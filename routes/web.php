<?php

Auth::routes(['register' => false]);


Route::get('/', function () {
    setup();
    return redirect('/login');
});





Route::get('/customer-register/{id}', 'OnlineOrderController@registerCustomerLayout');
Route::post('/customer-register', 'OnlineOrderController@registerCustomer');







/*
* Database Sync Routes
*/

Route::post('sync-orders', 'SyncController@syncOrders');




Auth::routes();




Route::group(['middleware'=>'language'],function ()  {


    Route::post('/change/lang', 'HomeController@changeLanguage');




    /*
    * profile and user routes
    */

    Route::get('/home', 'HomeController@index');
    Route::get('/dashboard', 'ProfileController@dashboard')->middleware('auth');
    Route::get('/profile', 'ProfileController@profile');
    Route::post('/edit/profile', 'ProfileController@editProfile');
    Route::post('/edit/password', 'ProfileController@editPassword')->middleware('admin');










    /*
    * Menu routes
    */

    Route::get('/menu', 'MenuController@index')->middleware('auth');
    Route::post('/add/menu', 'MenuController@addMenu')->middleware('auth');
    Route::post('/add/menu-item', 'MenuController@addMenuItem');
    Route::post('/view/menu-item', 'MenuController@viewMenuItem')->middleware('auth');
    Route::post('/delete/menu', 'MenuController@deleteMenu')->middleware('auth');
    Route::post('/edit/menu', 'MenuController@editMenu')->middleware('auth');
    Route::post('/delete/menu-item', 'MenuController@deleteMenuItem')->middleware('auth');
    Route::post('/edit/menu-item', 'MenuController@editMenuItem')->middleware('auth');

    Route::post('/add/setmenu-item', 'MenuController@addSetMenuItem')->middleware('auth');
    Route::post('/get/setmenu-items', 'MenuController@getSetMenuItem')->middleware('auth');









    /*
    * Modifier routes
    */

    Route::get('/modifiers', 'ModifierController@index')->middleware('auth');
    Route::post('/add/modifier', 'ModifierController@addModifier')->middleware('auth');
    Route::post('/edit/modifier', 'ModifierController@editModifier')->middleware('auth');
    Route::post('/delete/modifier', 'ModifierController@deleteModifier')->middleware('auth');









    /*
    * Printer routes
    */

    Route::get('/printers', 'PrinterController@index')->middleware('auth');
    Route::post('/add/printer', 'PrinterController@addPrinter')->middleware('auth');
    Route::post('/edit/printer', 'PrinterController@editPrinter')->middleware('auth');
    Route::post('/delete/printer', 'PrinterController@deletePrinter')->middleware('auth');









    /*
    * Recipe routes
    */

    Route::get('/recipes', 'RecipeController@index')->middleware('auth');
    Route::post('/view/recipe', 'RecipeController@viewRecipeItems')->middleware('auth');
    Route::post('/add/recipe', 'RecipeController@addRecipe')->middleware('auth');
    Route::post('/edit/recipe', 'RecipeController@editRecipe')->middleware('auth');
    Route::post('/delete/recipe', 'RecipeController@deleteRecipe')->middleware('auth');









    /*
    * Reservation routes
    */

    Route::get('/reservations', 'ReservationController@index')->middleware('auth');
    Route::get('/new-reservation', 'ReservationController@newReservationLayout')->middleware('auth');
    Route::post('/create/reservation', 'ReservationController@createReservation')->middleware('auth');
    Route::post('/check/availablity', 'ReservationController@checkAvailablity')->middleware('auth');
    Route::post('/delete/reservation', 'ReservationController@deleteReservation')->middleware('auth');

    Route::get('/reservation/receipt/{id}', 'ReservationController@receipt')->middleware('auth');










    /*
    * Order Instruction routes
    */

    Route::get('/order-instruction', 'InstructionController@index')->middleware('auth');
    Route::post('/add/order-instruction', 'InstructionController@addInstruction');
    Route::post('/edit/order-instruction', 'InstructionController@editInstruction')->middleware('auth');
    Route::post('/delete/order-instruction', 'InstructionController@deleteInstruction')->middleware('auth');










    /*
    * order routes
    */

//Route::get('/order', 'OrderController@index');
//Route::get('/order-history', 'OrderController@orderHistory');
    Route::post('/place/order', 'OrderController@placeOrder')->middleware('auth');
    Route::post('/get/order', 'OrderController@getOrder')->middleware('auth');
    Route::post('/get/order/history', 'OrderController@getOrderHistory')->middleware('auth');
    Route::post('/modify/order', 'OrderController@modifyOrder')->middleware('auth');

    Route::post('/confirm/payment', 'OrderController@confirmPayment')->middleware('auth');
    Route::post('/edit/order/info', 'OrderController@editOrder')->middleware('auth');

    Route::post('/change/table', 'OrderController@changeTable')->middleware('auth');
    Route::post('/change/waiter', 'OrderController@changeWaiter')->middleware('auth');
    Route::post('/change/customer', 'OrderController@changeCustomer')->middleware('auth');
    Route::post('/change/order-type', 'OrderController@changeOrderType')->middleware('auth');
    Route::post('/change/order-status', 'OrderController@changeOrderStatus')->middleware('auth');


    Route::get('/order', 'OrderController@index')->middleware('staff');
    Route::get('/settle/{id}', 'OrderController@settleLayout')->middleware('auth');



    Route::get('/table-order', 'TableController@index')->middleware('staff');
    Route::get('/table-order/{id}', 'TableController@order')->middleware('auth');
    Route::post('/place/table-order', 'TableController@placeOrder')->middleware('auth');

    Route::get('/kitchen-queue', 'KitchenController@index')->middleware('auth');
    Route::post('/order/served', 'KitchenController@orderServed')->middleware('auth');

    Route::get('/view-queue', 'KitchenController@viewQueue')->middleware('auth');
    Route::post('/update/order-queue', 'KitchenController@updateOrderQueue')->middleware('auth');
    Route::post('/clear/order-queue', 'KitchenController@clearOrderQueue')->middleware('auth');

    Route::get('/online-order', 'OnlineOrderController@index')->middleware('auth');
    Route::post('/place/online-order', 'OnlineOrderController@placeOrder')->middleware('auth');

    Route::get('/online/orders', 'OnlineOrderController@onlineOrders')->middleware('auth');
    Route::post('/change/online-order-status', 'OnlineOrderController@changeOrderStatus')->middleware('auth');
    Route::post('/get/online-order/history', 'OnlineOrderController@getOrderHistory')->middleware('auth');


    Route::get('/my-orders', 'OnlineOrderController@myOrders')->middleware('auth');
    Route::get('/online-order/receipt/{id}', 'OnlineOrderController@viewReceipt')->middleware('auth');








    /*
     * Void Routes
     */

    Route::post('/request/void', 'VoidController@voidRequest');
    Route::post('/request/void/item', 'VoidController@voidRequestItem');
    Route::post('/accept/void', 'VoidController@voidAccept');
    Route::post('/accept/void-item', 'VoidController@itemVoidAccept');

    Route::post('/get/void/history', 'VoidController@getVoidHistory');









    /*
     * Complimentary Routes
     */


    Route::post('/request/complimentary/', 'OrderController@requestComplimentaryItem');










    /*
     * Guest Routes
     */

    Route::get('/guests', 'CustomerController@index')->middleware('auth');
    Route::post('/add/client', 'CustomerController@addCustomer')->middleware('auth');
    Route::post('/edit/client', 'CustomerController@editCustomer')->middleware('auth');
    Route::post('/delete/client', 'CustomerController@deleteCustomer')->middleware('auth');


    Route::post('/get/clients', 'CustomerController@getCustomers')->middleware('auth');









    /*
    * Discount Routes
    */

    Route::get('/discount-circular', 'DiscountController@index')->middleware('auth');
    Route::post('/add/discount-circular', 'DiscountController@addDiscountCircular')->middleware('auth');
    Route::post('/discount-add', 'DiscountController@addDiscount')->middleware('auth');
    Route::post('/delete/circular', 'DiscountController@deleteCircular')->middleware('auth');









    /*
    * HR Setting Routes
    */

    Route::get('/user-setting', 'HRController@index')->middleware('auth');
    Route::post('/add/user', 'HRController@addUser')->middleware('auth');
    Route::get('/team', 'HRController@team')->middleware('auth');
    Route::post('/delete/user', 'HRController@deleteUser')->middleware('auth');
    Route::post('/edit/user', 'HRController@editUser')->middleware('auth');
    Route::post('/edit/user/password', 'HRController@editPassword')->middleware('admin');









    /*
    * App Setting Routes
    */

    Route::get('/application-setting', 'SettingsController@appSettingsLayout')->middleware('auth');
    Route::post('/save/restaurant/settings', 'SettingsController@saveAppSettings')->middleware('auth');
    Route::post('/login-settings', 'SettingsController@saveLoginSettings')->middleware('auth');










    /*
    * Restaurant Setting Routes
    */

    Route::get('/restaurant-settings', 'SettingsController@index')->middleware('auth');
    Route::post('/restaurant-settings', 'SettingsController@uploadRestaurantLogo')->middleware('auth');
    Route::post('/add/table', 'SettingsController@addTable')->middleware('auth');
    Route::post('/add/new-table', 'SettingsController@addNewTable')->middleware('auth');
    Route::post('/edit/charges', 'SettingsController@editCharges')->middleware('auth');
    Route::post('/edit/restaurant-info', 'SettingsController@editRestaurantInfo')->middleware('auth');
    Route::post('/delete/table', 'SettingsController@deleteTable')->middleware('auth');
    Route::post('/edit/restaurant-table', 'SettingsController@editTable')->middleware('auth');
    Route::get('/table-settings', 'SettingsController@tableSettings')->middleware('auth');

    Route::get('/permission-settings', 'SettingsController@permissionSettings')->middleware('auth');
    Route::post('/get/userPermissionDetails', 'SettingsController@getUserPermissionDetails')->middleware('auth');
    Route::post('/change/userReportPermission', 'SettingsController@changeUserPermission')->middleware('auth');
//Route::post('/change/menuPermission', 'SettingsController@changeMenuPermission')->middleware('auth');

    Route::post('/change/voidPermission', 'SettingsController@changeVoidPermission')->middleware('auth');
    Route::post('/change/userPermission', 'SettingsController@changeMenuPermission')->middleware('auth');
    Route::post('/change/orderTypesPermission', 'SettingsController@changeOrderTypesPermission')->middleware('auth');


    Route::post('/add/email', 'SettingsController@addEmail')->middleware('auth');
    Route::post('/edit/email', 'SettingsController@editEmail')->middleware('auth');
    Route::post('/delete/email', 'SettingsController@deleteEmail')->middleware('auth');










    /*
    * Restaurant Admin Routes
    */

    Route::post('/start/work-period', 'ProfileController@startWorkPeriod')->middleware('auth');
    Route::post('/end/work-period', 'ProfileController@endWorkPeriod')->middleware('auth');










    /*
    * Activity Routes
    */

    Route::get('/activity', 'ActivityController@index')->middleware('auth');










    /*
    * Sales Report Routes
    */

    Route::get('/sales-report', 'ReportController@index')->middleware('auth');
    Route::get('/sales-report-generation', 'ReportController@salesReportGeneration')->middleware('auth');
    Route::post('/generate/report', 'ReportController@generateReport')->middleware('auth');
    Route::post('/generate/orderDetails', 'ReportController@generateOrderDetails')->middleware('auth');

    Route::get('/report', 'ReportController@report')->middleware('auth');

    Route::post('/generate/stock-report', 'ReportController@currentStockReport')->middleware('auth');
    Route::post('/generate/purchase-history', 'ReportController@purchaseHistoryReport')->middleware('auth');
    Route::post('/generate/damage-history', 'ReportController@damageHistoryReport')->middleware('auth');










    /*
    * Void Report Routes
    */

    Route::get('/void-report', 'ReportController@voidReport')->middleware('auth');
    Route::post('/generate/void-report', 'ReportController@generateVoidReport')->middleware('auth');









    /*
    * Expense Report Routes
    */

    Route::get('/expense-report', 'ReportController@expenseReport')->middleware('auth');
    Route::post('/generate/expense-report', 'ReportController@generateExpenseReport')->middleware('auth');





    /*
    * Revenue Report Routes
    */

    Route::get('/revenue-report', 'ReportController@revenueReport')->middleware('auth');
    Route::post('/generate/revenue-report', 'ReportController@generateRevenueReport')->middleware('auth');









    /*
    * Reservation Report Routes
    */

    Route::get('/reservation-report', 'ReportController@reservationReport')->middleware('auth');
    Route::post('/generate/reservation-report', 'ReportController@generateReservationReport')->middleware('auth');




    /*
    * Recipe Report Routes
    */

    Route::get('/recipe-report', 'ReportController@recipeReport')->middleware('auth');
    Route::post('/recipe-report', 'ReportController@generateRecipeReport')->middleware('auth');










    /*
    * test Routes
    */
    Route::get('/test', 'HomeController@test');








    /*
    * Back-Up Routes
    */

    Route::get('/back-up', 'BackUpController@index')->middleware('auth');
    Route::post('/create/back-up', 'BackUpController@createBackup')->middleware('auth');
    Route::get('/download/back-up/{filename}', 'BackUpController@downloadBackup')->middleware('superadmin');
    Route::post('/delete/back-up', 'BackUpController@deleteBackup')->middleware('superadmin');









    /*
     * Email Routes
     */

    Route::get('viewEmailFormat','MailController@index');
    Route::get('sendbasicemail','MailController@basic_email');
    Route::get('/sendEmailReport/{id}','MailController@emailDailyReport');
    Route::get('sendattachmentemail','MailController@attachment_email');

    Route::post('/emailReport','MailController@emailReport');
    Route::post('/email/current-stock','MailController@emailCurrentStock');
    Route::post('/email/purchase-history','MailController@emailPurchaseHistory');
    Route::post('/email/damage-history','MailController@emailDamageHistory');









    /*
    * SuperAdmin Routes
    */

    Route::get('/sa-dashboard', 'SuperAdminController@index')->middleware('superadmin');

    Route::post('/add/bank-card', 'SuperAdminController@addBankCard')->middleware('superadmin');
    Route::post('/edit/bank-card', 'SuperAdminController@editBankCard')->middleware('superadmin');
    Route::post('/delete/card', 'SuperAdminController@deleteBankCard')->middleware('superadmin');

    Route::post('/add/payment-method', 'SuperAdminController@addPaymentMethod')->middleware('superadmin');
    Route::post('/edit/payment-method', 'SuperAdminController@editPaymentMethod')->middleware('superadmin');
    Route::post('/delete/payment-method', 'SuperAdminController@deletePaymentMethod')->middleware('superadmin');

    Route::post('/create/restaurant', 'SuperAdminController@createRestaurant')->middleware('superadmin');
    Route::post('/create/user', 'SuperAdminController@createUser')->middleware('superadmin');
    Route::post('/save/app/settings', 'SuperAdminController@saveAppSettings')->middleware('superadmin');
    Route::post('/add/lookup', 'SuperAdminController@addLookup')->middleware('superadmin');
    Route::post('/change/accounts/permission', 'SuperAdminController@changeAccountsPermission')->middleware('superadmin');
    Route::post('/change/inv/permission', 'SuperAdminController@changeInvPermission')->middleware('superadmin');
    Route::post('/change/kitchen-queue/permission', 'SuperAdminController@changeKitchenQueuePermission')->middleware('superadmin');
    Route::post('/get/restaurant-info', 'SuperAdminController@getRestaurantInfo')->middleware('superadmin');
    Route::post('/edit/user/role', 'SuperAdminController@editUserRole')->middleware('superadmin');
    Route::post('/edit/user/restaurant', 'SuperAdminController@editUserRestaurant')->middleware('superadmin');









    /*
    * SuperAdmin Settings Routes
    */

    Route::get('/create-restaurant', 'SuperAdminController@createRestaurantLayout')->middleware('superadmin');
    Route::get('/bank-cards', 'SuperAdminController@bankCardsLayout')->middleware('superadmin');

    Route::get('/restaurants', 'SuperAdminController@restaurants')->middleware('superadmin');
    Route::get('/restaurant/{id}', 'SuperAdminController@viewRestaurant')->middleware('superadmin');
    Route::post('/edit/restaurant-features', 'SuperAdminController@editRestaurantFeatures')->middleware('superadmin');
    Route::post('/edit/restaurant-settings', 'SuperAdminController@editRestaurantSettings')->middleware('superadmin');

    Route::get('/create-user', 'SuperAdminController@createUserLayout')->middleware('superadmin');

    Route::get('/app-settings', 'SuperAdminController@appSettingsLayout')->middleware('superadmin');

    Route::get('/look-up', 'SuperAdminController@lookUpLayout')->middleware('superadmin');
    Route::post('/add/order-type', 'SuperAdminController@addOrderType')->middleware('superadmin');
    Route::post('/edit/order-type', 'SuperAdminController@editOrderType')->middleware('superadmin');
    Route::post('/delete/order-type', 'SuperAdminController@deleteOrderType')->middleware('superadmin');


    Route::get('/inv-settings', 'SuperAdminController@invSettingsLayout')->middleware('superadmin');
    Route::get('/data-settings', 'SuperAdminController@dataSettingsLayout')->middleware('superadmin');
    Route::get('/schedule-scripts', 'SuperAdminController@scheduleScriptsLayout')->middleware('superadmin');

    Route::get('/application-menu', 'SuperAdminController@applicationMenu')->middleware('superadmin');
    Route::post('/save/application-menu', 'SuperAdminController@saveApplicationMenu')->middleware('superadmin');
    Route::post('/edit/application-menu', 'SuperAdminController@editApplicationMenu')->middleware('superadmin');
    Route::post('/delete/application-menu', 'SuperAdminController@deleteApplicationMenu')->middleware('superadmin');

    Route::get('/view-order', 'SuperAdminOrderController@index')->middleware('superadmin');
    Route::post('/view-order', 'SuperAdminOrderController@viewOrder')->middleware('superadmin');
    Route::get('/edit-order/{id}', 'SuperAdminOrderController@editOrderLayout')->middleware('superadmin');
    Route::post('/edit/order', 'SuperAdminOrderController@editOrder')->middleware('superadmin');
    Route::post('/delete/order', 'SuperAdminOrderController@deleteOrder')->middleware('superadmin');
    Route::post('/recalculate/order', 'SuperAdminOrderController@recalculateOrder')->middleware('superadmin');
    Route::post('/edit/order/payment', 'SuperAdminOrderController@editOrderPayment')->middleware('superadmin');

    Route::post('/edit/order/created-at', 'SuperAdminOrderController@editOrderCreatedAt')->middleware('superadmin');









    /*
    * SuperAdmin Scripts Routes
    */

    Route::post('/run-scripts', 'ScriptController@runScripts')->middleware('superadmin');
    Route::post('/db-fix-scripts', 'ScriptController@dbFixScript')->middleware('superadmin');
    Route::post('/db-fix-order-scripts', 'ScriptController@fixOrderData')->middleware('superadmin');
    Route::post('/load/demo-data', 'ScriptController@loadDemoData')->middleware('superadmin');









    /*
    * SuperAdmin Data Settings Routes
    */

    Route::post('/table/truncate', 'DataController@truncateTable')->middleware('superadmin');
    Route::post('/table/drop', 'DataController@dropTable')->middleware('superadmin');









    /*
     * SuperAdmin Licenses Routes
     */

    Route::get('/sa-licenses', 'LicenseController@saLicensesLayout')->middleware('superadmin');
    Route::get('/sa-licenses/{id}', 'LicenseController@saLicensesLayout')->middleware('superadmin');






    /*
    * License Routes
    */

    Route::post('/license/assign', 'LicenseController@licenseAssign')->middleware('superadmin');
    Route::post('/license/delete', 'LicenseController@deleteLicense')->middleware('superadmin');
    Route::post('/license/activate', 'LicenseController@activateLicense')->middleware('superadmin');









    /*
     * Accounts Routes
     */

    Route::get('/cash-opening', 'AccountsController@cashOpening')->middleware('auth');
    Route::post('/add/cash-opening', 'AccountsController@addCashOpening')->middleware('auth');

    Route::get('/daily-expense/', 'AccountsController@dailyExpense')->middleware('auth');
    Route::post('/add/daily-expense', 'AccountsController@addDailyExpense')->middleware('auth');
    Route::post('/add/cash-transfer', 'AccountsController@addCashTransfer')->middleware('auth');

    Route::post('/edit/daily-expense', 'AccountsController@editDailyExpense')->middleware('admin');
    Route::post('sendEmailReport', 'AccountsController@deleteDailyExpense')->middleware('admin');

    Route::get('/cash-closing', 'AccountsController@cashClosing')->middleware('auth');
    Route::post('/add/cash-closing', 'AccountsController@addCashClosing')->middleware('auth');

    Route::get('/loan-credit', 'AccountsController@loanCredit')->middleware('auth');
    Route::post('/add/owner', 'AccountsController@addOwner')->middleware('auth');
    Route::post('/add/loan', 'AccountsController@addLoan')->middleware('auth');

    Route::get('/loan-debit', 'AccountsController@loanDebit')->middleware('auth');
    Route::post('/add/loan-debit', 'AccountsController@addLoanDebit')->middleware('auth');












    /*
    * Inventory Section
    */



    /*
    * Supplier Routes
    */


    Route::get('/supplier', 'InventoryController@supplier')->middleware('auth');
    Route::post('/add/supplier', 'InventoryController@addSupplier')->middleware('auth');
    Route::post('/edit/supplier', 'InventoryController@editSupplier')->middleware('auth');
    Route::post('/delete/supplier', 'InventoryController@deleteSupplier')->middleware('auth');

    Route::post('/payment/supplier', 'InventoryController@paymentSupplier')->middleware('auth');




    /*
    * Products Routes
    */

    Route::get('/product-category', 'InventoryController@productCategory')->middleware('auth');
    Route::post('/add/category', 'InventoryController@addCategory')->middleware('auth');
    Route::post('/edit/category', 'InventoryController@editCategory')->middleware('auth');
    Route::post('/delete/category', 'InventoryController@deleteCategory')->middleware('auth');

    Route::get('/products', 'InventoryController@products')->middleware('auth');
    Route::post('/add/product', 'InventoryController@addProduct')->middleware('auth');
    Route::post('/edit/product', 'InventoryController@editProduct')->middleware('auth');
    Route::post('/delete/product', 'InventoryController@deleteProduct')->middleware('auth');

    Route::get('/product-unit', 'InventoryController@productUnitLayout')->middleware('auth');
    Route::post('/add/unit', 'InventoryController@addUnit')->middleware('auth');
    Route::post('/edit/unit', 'InventoryController@editUnit')->middleware('auth');
    Route::post('/delete/unit', 'InventoryController@deleteUnit')->middleware('auth');

    Route::post('/add/unit-conversion', 'InventoryController@addUnitConversion')->middleware('auth');



    /*
    * Purchase Routes
    */

    Route::get('/new-purchase', 'InventoryController@purchase')->middleware('auth');
    Route::post('/new-purchase', 'InventoryController@newPurchase')->middleware('auth');
    Route::get('/purchase-history', 'InventoryController@purchaseHistory')->middleware('auth');
    Route::post('/get/purchase-details', 'InventoryController@getPurchaseDetails')->middleware('auth');



    /*
    * Store Routes
    */


    Route::get('/store', 'InventoryController@store')->middleware('auth');
    Route::post('/add/store', 'InventoryController@addStore')->middleware('auth');
    Route::post('/edit/store', 'InventoryController@editStore')->middleware('auth');
    Route::post('/delete/store', 'InventoryController@deleteStore')->middleware('auth');




    /*
    * Stock Routes
    */


    Route::get('/current-stock', 'InventoryController@currentStock')->middleware('auth');

    Route::get('/transfer-stock', 'InventoryController@transferStock')->middleware('auth');
    Route::post('/transfer-stock', 'InventoryController@submitTransferStock')->middleware('auth');

    Route::get('/transfer-history', 'InventoryController@transferHistory')->middleware('auth');
    Route::post('/get/transferDetails', 'InventoryController@getTransferDetails')->middleware('auth');

    Route::get('/receive-stock', 'InventoryController@receiveStock')->middleware('auth');
    Route::post('/receive-stock', 'InventoryController@submitReceiveStock')->middleware('auth');

    Route::get('/damage-entry', 'InventoryController@damageEntry')->middleware('auth');
    Route::post('/damage-entry', 'InventoryController@submitDamageEntry')->middleware('auth');

    Route::get('/damage-history', 'InventoryController@damageHistory')->middleware('auth');
    Route::post('/get/damageDetails', 'InventoryController@getDamageDetails')->middleware('auth');


});





