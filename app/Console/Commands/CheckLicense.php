<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class CheckLicense extends Command {

    protected $signature = 'check:licenses';

    protected $description = 'Check For All The Licenses';


    public function __construct() {
        parent::__construct();
    }

    public function handle() {

        checkRstaurantLicense();

        DB::table('schedule_scripts')->insert([
            'script_name' => 'check:licenses'
            ]);

    }
}
