<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Psy\Command\Command;

class Kernel extends ConsoleKernel {

    protected $commands = [
        Commands\CheckLicense::class,
    ];

    protected function schedule(Schedule $schedule) {

        $schedule->call('App\Http\Controllers\MailController@emailDailyReport')->dailyAt('02:00');

        $schedule->command('check:licenses')->dailyAt('02:00')->timezone('Asia/Dhaka');


    }

    protected function commands() {
        require base_path('routes/console.php');
    }
}
