<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

class PrinterController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }


    public function index() {

        $restaurant_id = Auth::user()->restaurant_id;

        $printers = DB::table('printers')
            ->where('restaurant_id', $restaurant_id)
            ->get();

        return view('cupcake.menu.printers', [
            'printers' => $printers
        ]);
    }


    public function addPrinter(Request $request) {

        $data= $request->input('params');
        $restaurant_id = Auth::user()->restaurant_id;
        $created_by = Auth::user()->id;

        $printer_id = DB::table('printers')->insertGetId([
            'restaurant_id' => $restaurant_id,
            'printer_name' => $data['printer_name'],
            'port' => $data['port'],
            'created_by' => $created_by,
        ]);

        echo json_encode($printer_id);
    }


    public function editPrinter(Request $request) {

        $data = $request->input('params');
        $date = date('Y-m-d H:i:s');

        DB::table('printers')
            ->where('printer_id', $data['printer_id'])
            ->update([
                'printer_name' => $data['printer_name'],
                'port' => $data['port'],
                'updated_at' => $date,
            ]);

        echo json_encode("success");
    }


    public function deletePrinter(Request $request) {

        $data = $request->input('params');

        DB::table('printers')->where('printer_id', $data['printer_id'])->delete();

        echo json_encode("success");
    }

}
