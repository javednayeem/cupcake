<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Illuminate\Support\Facades\Schema;

ini_set('max_execution_time', 108000);
ini_set('memory_limit', '-1');

class ScriptController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }


    public function runScripts(Request $request) {

        checkRstaurantLicense();

        $this->createScriptLog('check:licenses');

        return json_encode('success');
    }


    public function dbFixScript(Request $request) {

        $user_id = Auth::user()->id;

        $this->setAppSettings($user_id);
        $this->setApplicationMenu();
        $this->setRestaurantSettings();
        $this->setOrderType();

        $this->createScriptLog('fix:db_scripts');

        return json_encode('success');
    }


    private function findKey($key, $app_settings) {

        foreach ($app_settings as $setting) {

            if ($setting->setting_key == $key) return true;
        }

        return false;

    }


    private function findRestaurantSettings($key, $restaurant_settings, $restaurant_id) {

        foreach ($restaurant_settings as $setting) {

            if ($setting->setting_key == $key && $setting->restaurant_id == $restaurant_id) return true;
        }

        return false;

    }


    private function findMenuKey($key, $application_menu) {

        foreach ($application_menu as $menu) {

            if ($menu->menu_name == $key) return true;
        }

        return false;

    }


    private function findOrderType($key, $order_types) {

        foreach ($order_types as $type) {

            if ($type->type_name == $key) return true;
        }

        return false;

    }


    public function fixOrderData() {

        $orders = DB::table('orders')
            ->where('order_status', 'paid')
            ->get();

        foreach ($orders as $order) {

            $order_id = $order->order_id;
            $created_at = $order->created_at;
            $updated_at = $order->updated_at;

            if ($order->order_status == 'paid') {

                DB::table('order_details')
                    ->where('order_id', $order_id)
                    ->update([
                        'settle' => 1
                    ]);

            }
        }

        $this->createScriptLog('fix:order_data');

    }


    private function createScriptLog($script_name) {

        date_default_timezone_set("Asia/Dhaka");

        DB::table('schedule_scripts')->insert([
            'script_name' => $script_name
        ]);

    }


    private function setAppSettings($user_id) {

        $app_settings = json_decode(json_encode(config('global.app_settings')));
        $prev_app_settings = DB::table('app_settings')->get();

        foreach ($app_settings as $setting) {

            if (! $this->findKey($setting->setting_key, $prev_app_settings)) {
                DB::table('app_settings')->insert([
                    'setting_key' => $setting->setting_key,
                    'setting_value' => $setting->setting_value,
                    'user_id' => $user_id,
                ]);
            }
        }
    }


    private function setApplicationMenu() {

        $application_menu = json_decode(json_encode(config('global.application_menu')));
        $prev_application_menu = DB::table('application_menu')->get();

        foreach ($application_menu as $key) {

            if (! $this->findMenuKey($key->menu_name, $prev_application_menu)) {

                DB::table('application_menu')->insert([
                    'menu_name' => $key->menu_name,
                    'url' => $key->url,
                    'parent_menu' => $key->parent_menu,
                    'class' => $key->class,
                ]);
            }
        }
    }


    private function setRestaurantSettings() {

        $restaurant_settings = json_decode(json_encode(config('global.restaurant_settings')));
        $prev_restaurant_settings = DB::table('restaurant_settings')->get();
        $restaurants = DB::table('restaurants')->get();

        foreach ($restaurants as $restaurant) {
            foreach ($restaurant_settings as $key) {
                if (! $this->findRestaurantSettings($key->setting_key, $prev_restaurant_settings, $restaurant->restaurants_id)) {

                    DB::table('restaurant_settings')->insert([
                        'setting_key' => $key->setting_key,
                        'setting_value' => $key->setting_value,
                        'restaurant_id' => $restaurant->restaurants_id,
                    ]);
                }
            }
        }
    }


    public function loadDemoData() {

        $count = DB::table('menus')->count();

        if ($count == 0) {

            $menus = json_decode(json_encode(config('global.menus')));

            foreach ($menus as $menu) {

                DB::table('menus')->insert([
                    'restaurants_id' => $menu->restaurants_id,
                    'menu_name' => $menu->menu_name,
                    'creator_id' => $menu->creator_id,
                ]);

            }


            $menu_items = json_decode(json_encode(config('global.menu_items')));

            foreach ($menu_items as $item) {

                DB::table('menu_items')->insert([
                    'menu_id' => $item->menu_id,
                    'item_name' => $item->item_name,
                    'ratio' => $item->ratio,
                    'price' => $item->price,
                    'item_vat' => $item->item_vat,
                    'set_menu' => $item->set_menu,
                    'printer_id' => $item->printer_id,
                ]);

            }



            $setmenu_items = json_decode(json_encode(config('global.setmenu_items')));

            foreach ($setmenu_items as $item) {

                DB::table('setmenu_items')->insert([
                    'item_name' => $item->item_name,
                    'menu_item_id' => $item->menu_item_id,
                    'restaurant_id' => $item->restaurant_id,
                ]);

            }


            $this->createScriptLog('load:demo_data');

        }



        return json_encode('success');
    }


    private function setOrderType() {

        $order_types = json_decode(json_encode(config('global.order_types')));
        $prev_order_types = DB::table('order_types')->get();

        foreach ($order_types as $key) {

            if (! $this->findOrderType($key->type_name, $prev_order_types)) {

                DB::table('order_types')->insert([
                    'type_name' => $key->type_name,
                ]);
            }
        }
    }



}
