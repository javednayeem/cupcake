<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;


class MenuController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }


    public function index() {

        $restaurant_id = Auth::user()->restaurant_id;

        $menu_data = DB::table('menus')
            ->select('menus.menu_id', 'menus.menu_name', 'menus.created_at', 'users.name' )
            ->where('restaurants_id', $restaurant_id)
            ->where('menus.status', '=', 1)
            ->leftJoin('users', 'users.id', 'menus.creator_id')
            ->orderBy('menu_name')
            ->get();

        $printers = DB::table('printers')
            ->where('restaurant_id', $restaurant_id)
            ->get();

        return view('cupcake.menu.index', [
            'menu_data' => $menu_data,
            'printers' => $printers,
        ]);
    }


    public function addMenu(Request $request) {
        $data= $request->input('params');

        $userId = Auth::user()->id;
        $restaurant_id = Auth::user()->restaurant_id;

        $menu_id = DB::table('menus')->insertGetId([
            'restaurants_id' => $restaurant_id,
            'menu_name' => $data['menu_name'],
            'creator_id' => $userId,
        ]);

        $activity = "Created menu: " . $data['menu_name'];
        createActivityLog($activity);


        echo json_encode($menu_id);
    }


    public function addMenuItem(Request $request) {

        $data= $request->input('params');
        $userId = Auth::user()->id;
        $restaurant_id = Auth::user()->restaurant_id;

        $item_id = DB::table('menu_items')->insertGetId([
            'menu_id' => $data['menu_id'],
            'item_name' => $data['item_name'],
            'ratio' => $data['ratio'],
            'price' => $data['price'],
            'cost_price' => $data['cost_price'],
            'item_vat' => $data['item_vat'],
            'set_menu' => $data['set_menu'],
            'discount_available' => $data['discount_available'],
            'printer_id' => $data['printer_id'],
        ]);

        $activity = "Created menu item: " . $data['item_name'];
        createActivityLog($activity);


        return json_encode($item_id);
    }


    public function viewMenuItem(Request $request) {
        $data= $request->input('params');

        $item_data = DB::table('menu_items')
            ->where('menu_id', $data['menu_id'])
            ->where('status', 1)
            ->orderBy('item_name')
            ->get();

        return json_encode($item_data);
    }


    public function deleteMenu(Request $request) {

        $data = $request->input('params');

        DB::table('menus')->where('menu_id', $data['menu_id'])->delete();


        $activity = "Deleted menu: " . $data['menu_id'];
        createActivityLog($activity);

//        DB::table('menus')
//            ->where('menu_id', $data['menu_id'])
//            ->update([
//                'status' => 0,
//            ]);
//
//        DB::table('activity')->insert([
//            'restaurant_id' => $restaurant_id,
//            'user_id' => $userId,
//            'lookup_activity' => 2,
//            'lookup_category' => 4,
//            'lookup_name_id' => $data['menu_id'],
//        ]);

        return json_encode("success");
    }


    public function editMenu(Request $request) {

        $data = $request->input('params');
        $date = date('Y-m-d H:i:s');

        DB::table('menus')
            ->where('menu_id', $data['menu_id'])
            ->update([
                'menu_name' => $data['menu_name'],
                'updated_at' => $date,
            ]);

        $activity = "Edited menu: " . $data['menu_name'];
        createActivityLog($activity);

        echo json_encode("success");
    }


    public function deleteMenuItem(Request $request) {

        $data = $request->input('params');
        $userId = Auth::user()->id;
        $restaurant_id = Auth::user()->restaurant_id;

        DB::table('menu_items')->where('menu_item_id', '=', $data['menu_item_id'])->delete();


        $activity = "Deleted menu item: " . $data['menu_item_id'];
        createActivityLog($activity);

//        DB::table('menu_items')
//            ->where('menu_item_id', $data['menu_item_id'])
//            ->update([
//                'status' => 0,
//            ]);
//
//        DB::table('activity')->insert([
//            'restaurant_id' => $restaurant_id,
//            'user_id' => $userId,
//            'lookup_activity' => 2,
//            'lookup_category' => 5,
//            'lookup_name_id' => $data['menu_item_id'],
//        ]);

        echo json_encode("success");
    }


    public function editMenuItem(Request $request) {

        $data = $request->input('params');
        $date = date('Y-m-d H:i:s');

        DB::table('menu_items')
            ->where('menu_item_id', $data['menu_item_id'])
            ->update([
                'item_name' => $data['item_name'],
                'ratio' => $data['ratio'],
                'price' => $data['price'],
                'cost_price' => $data['cost_price'],
                'item_discount' => $data['item_discount'],
                'item_vat' => $data['item_vat'],
                'status' => $data['status'],
                'printer_id' => $data['printer_id'],
                'menu_id' => $data['menu_id'],
                'updated_at' => $date,
            ]);

        $activity = "Edited menu item: " . $data['item_name'];
        createActivityLog($activity);

        return json_encode("success");
    }


    public function addSetMenuItem(Request $request) {

        $restaurant_id = Auth::user()->restaurant_id;

        $data= $request->input('params');

        $item_id = DB::table('setmenu_items')->insertGetId([
            'item_name' => $data['item_name'],
            'menu_item_id' => $data['menu_item_id'],
            'restaurant_id' => $restaurant_id
        ]);

        return json_encode($item_id);
    }


    public function getSetMenuItem(Request $request) {

        $data= $request->input('params');

        $setmenu_items = DB::table('setmenu_items')
            ->where('menu_item_id', $data['menu_item_id'])
            ->get();

        return json_encode($setmenu_items);
    }


}
