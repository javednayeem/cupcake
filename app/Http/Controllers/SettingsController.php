<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Illuminate\Support\Facades\Input;

class SettingsController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }


    public function index() {

        $restaurant_id = Auth::user()->restaurant_id;

        $emails = DB::table('emails')
            ->where('restaurant_id', $restaurant_id)
            ->get();

        $restaurant = DB::table('restaurants')
            ->where('restaurants_id', '=', $restaurant_id)
            ->first();


        $settings_data = DB::table('restaurant_settings')
            ->select('setting_key', 'setting_value')
            ->where('restaurant_id', $restaurant_id)
            ->get();

        $settings = array();
        foreach($settings_data as $row) $settings[$row->setting_key] = $row->setting_value;


        return view('cupcake.settings.index', [
            'emails' => $emails,
            'restaurant' => $restaurant,
            'settings' => $settings,
        ]);

    }


    public function tableSettings() {

        $restaurant_id = Auth::user()->restaurant_id;

        $table_data = DB::table('tables')
            ->where('restaurant_id', '=', $restaurant_id)
            ->get();

        $restaurant = DB::table('restaurants')
            ->where('restaurants_id', '=', $restaurant_id)
            ->first();

        return view('cupcake.settings.table', [
            'table_data' => $table_data,
            'restaurant' => $restaurant,
        ]);

    }


    public function addTable(Request $request) {

        $restaurant_id = Auth::user()->restaurant_id;
        $data = $request->input('params');

        for($i=1; $i<=$data['table_number']; $i++) {
            DB::table('tables')->insert([
                'restaurant_id' => $restaurant_id,
                'table_name' => $i,
                'capacity' => $data['capacity'],
            ]);
        }
        echo json_encode("success");
    }


    public function addNewTable(Request $request) {

        $restaurant_id = Auth::user()->restaurant_id;
        $data = $request->input('params');

        $count = DB::table('tables')
            ->where('restaurant_id', $restaurant_id)
            ->count();

        $count++;

        for($i=0; $i<$data['table_number']; $i++) {
            DB::table('tables')->insert([
                'restaurant_id' => $restaurant_id,
                'table_name' => $count++,
                'capacity' => $data['table_capacity'],
            ]);
        }
        return 'success';
    }


    public function editCharges(Request $request) {

        $restaurant_id = Auth::user()->restaurant_id;
        $data = $request->input('params');

        DB::table('restaurants')
            ->where('restaurants_id', $restaurant_id)
            ->update([
                'vat_no' => $data['vat_no'],
                'price_including_vat'=> $data['price_including_vat'],
                'vat_after_discount'=> $data['vat_after_discount'],
                'vat_percentage'=> $data['vat_percentage'],
                'service_charge'=> $data['service_charge'],
                'sd_percentage'=> $data['sd_percentage'],
                'service_charge_vat_percentage'=> $data['service_charge_vat_percentage'],
                'sd_vat_percentage'=> $data['sd_vat_percentage'],
            ]);



        echo json_encode("success");
    }


    public function editRestaurantInfo(Request $request) {

        $restaurant_id = Auth::user()->restaurant_id;
        $data = $request->input('params');

        DB::table('restaurants')
            ->where('restaurants_id', $restaurant_id)
            ->update([
                'restaurants_name' => $data['restaurants_name'],
                'address'=> $data['address'],
                'area'=> $data['area'],
                'city'=> $data['city'],
                'phone_number'=> $data['phone_number'],
                'email'=> $data['email'],
                'website'=> $data['website'],
                'petty_cash'=> $data['petty_cash'],
                'vat_no' => $data['vat_no'],
                'price_including_vat'=> $data['price_including_vat'],
                'vat_after_discount'=> $data['vat_after_discount'],
                'vat_percentage'=> $data['vat_percentage'],
                'service_charge'=> $data['service_charge'],
                'sd_percentage'=> $data['sd_percentage'],
                'service_charge_vat_percentage'=> $data['service_charge_vat_percentage'],
                'sd_vat_percentage'=> $data['sd_vat_percentage'],
            ]);
        echo json_encode("success");
    }


    public function deleteTable(Request $request) {

        $data = $request->input('params');

        DB::table('tables')->where('table_id', '=', $data['table_id'])->delete();

        echo json_encode("success");
    }


    public function editTable(Request $request) {

        $data = $request->input('params');
        $date = date('Y-m-d H:i:s');

        DB::table('tables')
            ->where('table_id', $data['table_id'])
            ->update([
                'table_name' => $data['table_name'],
                'capacity'=> $data['capacity'],
                'updated_at' => $date,
            ]);

        echo json_encode("success");
    }


    public function uploadRestaurantLogo(Request $request) {

        $file = $request->file('image');
        $restaurant_id = Input::get('restaurant_id');

        $this->validate($request, [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $file_name = time() . '.' .$file->getClientOriginalExtension();

        $destinationPath = 'images/restaurant-logo';
        $file->move($destinationPath,$file_name);

        DB::table('restaurants')
            ->where('restaurants_id', $restaurant_id)
            ->update([
                'restaurant_img' => $file_name
            ]);

        return redirect('/restaurant-settings');
    }


    public function appSettingsLayout() {

        $restaurant_id = Auth::user()->restaurant_id;

        $settings_data = DB::table('restaurant_settings')
            ->select('setting_key', 'setting_value')
            ->where('restaurant_id', $restaurant_id)
            ->get();

        $settings = array();
        foreach($settings_data as $row) $settings[$row->setting_key] = $row->setting_value;


        $order_types = DB::table('order_types')
            ->select('order_types.*',
                DB::raw("(SELECT permission FROM order_types_permission WHERE restaurant_id=$restaurant_id AND type_id=order_types.type_id) as permission"))
            ->get();


        return view('cupcake.settings.app-settings', [
            'settings' => $settings,
            'order_types' => $order_types,

        ]);
    }


    public function saveAppSettings(Request $request) {

        $data = $request->input('params');
        $restaurant_id = Auth::user()->restaurant_id;

        $keys = array_keys($data);

        for ($i=0; $i<count($keys); $i++) {

            DB::table('restaurant_settings')
                ->where('setting_key', $keys[$i])
                ->where('restaurant_id', $restaurant_id)
                ->update([
                    'setting_value' => $data[$keys[$i]]
                ]);

        }

        return json_encode('success');

    }


    public function permissionSettings() {

        $restaurant_id = Auth::user()->restaurant_id;
        $user_id = Auth::user()->id;

        $users = DB::table('users')
            ->select('id', 'name', 'role',
                DB::raw("(SELECT permission FROM void_permission WHERE user_id=users.id) as void_permission"))
            ->where('restaurant_id', $restaurant_id)
            ->where('status', 1)
            ->get();

        $report_type = DB::table('report_type')->get();

        $application_menu = DB::table('application_menu as a')
            ->select('a.*',
                DB::raw("(SELECT menu_name FROM application_menu as b WHERE b.menu_id=a.parent_menu) as parent_menu_name"),
                DB::raw("(SELECT COUNT(menu_id) FROM application_menu as c WHERE a.menu_id=c.parent_menu) as parent_count"),
                DB::raw("(SELECT permission FROM application_menu_permission as d WHERE a.menu_id=d.menu_id AND d.user_id=$user_id) as permission")
            )
            ->get();

        return view('cupcake.settings.permission-settings', [
            'users' => $users,
            'application_menu' => $application_menu,
            'report_type' => $report_type,
        ]);

    }


    public function getUserPermissionDetails(Request $request) {

        $data = $request->input('params');
        $user_id = $data['user_id'];

        $report_permission = DB::table('report_permission')
            ->where('user_id', $user_id)
            ->where('permission', 1)
            ->get();

        $menu_permission = DB::table('application_menu_permission')
            ->where('user_id', $user_id)
            ->where('permission', 1)
            ->get();



        $output =  array(
            'report_permission' => $report_permission,
            'menu_permission' => $menu_permission,
        );

        return json_encode($output);
    }


    public function changeUserPermission(Request $request) {

        $data = $request->input('params');
        $user_id = $data['user_id'];

        $permission = 1;
        $created_by = Auth::user()->id;

        DB::table('report_permission')->where('user_id', $user_id)->delete();

        $table_names = $data["table_names"];

        for ($i=0; $i<count($table_names); $i++) {

            $reportType_id = $table_names[$i];

            DB::table('report_permission')->insert([
                'reportType_id' => $reportType_id,
                'user_id' => $user_id,
                'permission' => $permission,
                'created_by' => $created_by,
            ]);

        }

        return json_encode('success');
    }


    public function addEmail(Request $request) {

        $restaurant_id = Auth::user()->restaurant_id;
        $created_by = Auth::user()->id;

        $data = $request->input('params');

        $email_id = DB::table('emails')->insertGetId([
            'email' => $data['email'],
            'created_by' => $created_by,
            'restaurant_id' => $restaurant_id,
        ]);

        return json_encode($email_id);
    }


    public function editEmail(Request $request) {

        $data = $request->input('params');

        DB::table('emails')
            ->where('email_id', $data['email_id'])
            ->update([
                'email' => $data['email']
            ]);

        return json_encode("success");
    }


    public function deleteEmail(Request $request) {

        $data = $request->input('params');

        DB::table('emails')->where('email_id', $data['email_id'])->delete();

        return json_encode("success");
    }


    public function changeVoidPermission(Request $request) {

        $data = $request->input('params');

        $permission = 1;

        $created_by = Auth::user()->id;
        $restaurant_id = Auth::user()->restaurant_id;

        DB::table('void_permission')->where('restaurant_id', $restaurant_id)->delete();



        if (isset($data["users"])) {

            $users = $data["users"];

            for ($i=0; $i<count($users); $i++) {

                $user_id = $users[$i];

                DB::table('void_permission')->insert([
                    'restaurant_id' => $restaurant_id,
                    'user_id' => $user_id,
                    'permission' => $permission,
                    'created_by' => $created_by,
                ]);

            }

        }



        return json_encode('success');
    }


    public function saveLoginSettings(Request $request) {

        $restaurant_id = Input::get('restaurant_id');
        $bg_type = Input::get('bg_type');
        $bg_color = Input::get('bg_color');

        if ($request->hasFile('bg_image')) {

            $file = $request->file('bg_image');
            $this->validate($request, [
                'bg_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1000',
            ]);

            $file_name = time() . '.' .$file->getClientOriginalExtension();
            $destinationPath = 'images/restaurant-logo';
            $file->move($destinationPath,$file_name);


            DB::table('restaurant_settings')
                ->where('setting_key', 'bg_image')
                ->where('restaurant_id', $restaurant_id)
                ->update([
                    'setting_value' => $file_name
                ]);

        }

        DB::table('restaurant_settings')
            ->where('setting_key', 'bg_type')
            ->where('restaurant_id', $restaurant_id)
            ->update([
                'setting_value' => $bg_type
            ]);

        DB::table('restaurant_settings')
            ->where('setting_key', 'bg_color')
            ->where('restaurant_id', $restaurant_id)
            ->update([
                'setting_value' => $bg_color
            ]);

        return back();


    }


    public function changeMenuPermission(Request $request) {

        $data = $request->input('params');
        $user_id = $data['user_id'];
        $permission = 1;
        $created_by = Auth::user()->id;

        DB::table('application_menu_permission')->where('user_id', $user_id)->delete();

        if (isset($data["menus"])) {

            $menus = $data["menus"];

            for ($i=0; $i<count($menus); $i++) {

                $menu_id = $menus[$i];

                DB::table('application_menu_permission')->insert([
                    'menu_id' => $menu_id,
                    'user_id' => $user_id,
                    'permission' => $permission,
                    'created_by' => $created_by,
                ]);
            }
        }

        return json_encode('success');
    }


    public function changeOrderTypesPermission(Request $request) {

        $data = $request->input('params');
        $permission = 1;
        $restaurant_id = Auth::user()->restaurant_id;

        DB::table('order_types_permission')->where('restaurant_id', $restaurant_id)->delete();

        if (isset($data["order_types"])) {

            $order_types = $data["order_types"];

            for ($i=0; $i<count($order_types); $i++) {

                $type_id = $order_types[$i];

                DB::table('order_types_permission')->insert([
                    'type_id' => $type_id,
                    'restaurant_id' => $restaurant_id,
                    'permission' => $permission,
                ]);
            }
        }

        return json_encode('success');
    }


}
