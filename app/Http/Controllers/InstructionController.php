<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

class InstructionController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }


    public function index() {

        $restaurant_id = Auth::user()->restaurant_id;

        $order_instructions = DB::table('order_instructions')
            ->where('restaurant_id', $restaurant_id)
            ->get();

        return view('cupcake.menu.order-instruction', [
            'order_instructions' => $order_instructions
        ]);
    }


    public function addInstruction(Request $request) {

        $data= $request->input('params');
        $restaurant_id = Auth::user()->restaurant_id;

        $instruction_id = DB::table('order_instructions')->insertGetId([
            'restaurant_id' => $restaurant_id,
            'instruction' => $data['instruction']
        ]);

        echo json_encode($instruction_id);
    }


    public function editInstruction(Request $request) {

        $data = $request->input('params');
        $date = date('Y-m-d H:i:s');

        DB::table('order_instructions')
            ->where('instruction_id', $data['instruction_id'])
            ->update([
                'instruction' => $data['instruction'],
                'updated_at' => $date,
            ]);

        echo json_encode("success");
    }


    public function deleteInstruction(Request $request) {

        $data = $request->input('params');

        DB::table('order_instructions')->where('instruction_id', $data['instruction_id'])->delete();

        echo json_encode("success");
    }

}
