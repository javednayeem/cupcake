<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

class ActivityController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {

        $restaurant_id = Auth::user()->restaurant_id;

        $activity_data = DB::table('activity')
            ->select('activity.*'
                , DB::raw("(SELECT lookup_name FROM lookup WHERE lookup_id= activity.lookup_activity limit 1) as lookup_activity_string")
                , DB::raw("(SELECT lookup_name FROM lookup WHERE lookup_id= activity.lookup_category limit 1) as lookup_category_string")
                , DB::raw("(SELECT name FROM users WHERE id= activity.user_id limit 1) as username")
                )
            ->where('restaurant_id', '=', $restaurant_id)
            ->get();


        return view('cupcake.activity.index', [
            'activity_data' => $activity_data
        ]);
    }

}
