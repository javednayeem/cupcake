<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Illuminate\Support\Facades\Hash;

class VoidController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }


    public function voidRequest(Request $request) {

        $restaurant_id = Auth::user()->restaurant_id;
        $cashier_id = Auth::user()->id;
        $date = date('Y-m-d H:i:s');
        $data = $request->input('params');

        $order_id = $data['order_id'];
        $username = $data['username'];
        $password = $data['password'];
        $reason = $data['reason'];
        $notes = $data['notes'];

        $void_status = 'accepted_void';

        $order = DB::table('orders')->where('order_id', $order_id)->first();
        $order_details = DB::table('order_details')->where('order_id', $order_id)->get();


        $user = DB::table('users')
            ->select('users.*'
                , DB::raw("(SELECT permission FROM void_permission WHERE user_id=$cashier_id) as permission"))
            ->where('email', $username)
            ->where('restaurant_id', $restaurant_id)
            ->first();


        $permission = 1;

        if ($user->permission == Null || $user->permission == 0) $permission = 0;

        if ($permission == 1 || $user->role == 'superadmin' || $user->role == 'admin') {

            $hashedPassword = $user->password;

            if (Hash::check($password, $hashedPassword)) {

                DB::table('void')->insert([
                    'restaurant_id' => $restaurant_id,
                    'order_id' => $order_id,
                    'waiter_id' => $order->waiter_id,
                    'cashier_id' => $cashier_id,
                    'approved_admin_id' => $user->id,
                    'void_status' => $void_status,
                    'reason' => $reason,
                    'notes' => $notes,
                ]);

                foreach ($order_details as $item) {

                    DB::table('void_item')->insert([
                        'restaurant_id' => $restaurant_id,
                        'order_details_id' => $item->order_details_id,
                        'order_id' => $order_id,
                        'void_quantity' => $item->item_quantity,
                        'waiter_id' => $order->waiter_id,
                        'cashier_id' => $cashier_id,
                        'approved_admin_id' => $user->id,
                        'void_status' => $void_status,
                        'reason' => $reason,
                        'notes' => $notes,
                    ]);

                }


                DB::table('orders')
                    ->where('order_id', $order_id)
                    ->update([
                        'order_status' => $void_status,
                        'updated_at' => $date
                    ]);

                $this->reCheckItemStatus($order_id);

                $response = [
                    "status"   => "success",
                    "message" => "Void Request Accepted"
                ];

            }

            else {

                $response = [
                    "status"   => "failure",
                    "message" => "Authentication Failed"
                ];

            }

        }

        else {

            $response = [
                "status"   => "failure",
                "message" => "User Not Found or Permission Denied"
            ];

        }

        return json_encode($response);

    }


    public function voidRequestItem(Request $request) {

        $restaurant_id = Auth::user()->restaurant_id;
        $cashier_id = Auth::user()->id;
        $date = date('Y-m-d H:i:s');
        $data = $request->input('params');

        $order_id = $data['order_id'];
        $order_details_id = $data['order_details_id'];
        $username = $data['username'];
        $password = $data['password'];
        $item_quantity = $data['item_quantity'];
        $reason = $data['reason'];
        $notes = $data['notes'];
        $waiter_id = $data['waiter_id'];

        $void_status = 'accepted_void';

        $user = DB::table('users')
            ->select('users.*'
                , DB::raw("(SELECT permission FROM void_permission WHERE user_id=$cashier_id) as permission"))
            ->where('email', $username)
            ->where('restaurant_id', $restaurant_id)
            ->first();


        $permission = 1;

        if ($user->permission == Null || $user->permission == 0) $permission = 0;


        if ($permission == 1 || $user->role == 'superadmin' || $user->role == 'admin') {

            $hashedPassword = $user->password;

            if (Hash::check($password, $hashedPassword)) {

                DB::table('void_item')->insert([
                    'restaurant_id' => $restaurant_id,
                    'order_details_id' => $order_details_id,
                    'order_id' => $order_id,
                    'void_quantity' => $item_quantity,
                    'waiter_id' => $waiter_id,
                    'cashier_id' => $cashier_id,
                    'approved_admin_id' => $user->id,
                    'void_status' => $void_status,
                    'reason' => $reason,
                    'notes' => $notes,
                ]);

                DB::table('order_details')
                    ->where('order_details_id', $order_details_id)
                    ->decrement('item_quantity', $item_quantity);

                $this->reCheckItemStatus($order_id);
                reCalculateBill($order_id);

                $response = [
                    "status"   => "success",
                    "message" => "Void Request Accepted"
                ];

            }

            else {

                $response = [
                    "status"   => "failure",
                    "message" => "Authentication Failed"
                ];

            }
        }

        else {

            $response = [
                "status"   => "failure",
                "message" => "User Not Found or Permission Denied"
            ];
        }


        reCalculateBill($order_id);

        return json_encode($response);
    }


    private function reCheckItemStatus($order_id) {

        $order_details = DB::table('order_details')
            ->where('order_id', $order_id)
            ->get();

        foreach ($order_details as $item) {

            $item_status = 1;

            if ($item->item_quantity == 0) $item_status = 0;

            DB::table('order_details')
                ->where('order_details_id', $item->order_details_id)
                ->update([
                    'item_status' => $item_status
                ]);

        }

    }


    public function getVoidHistory(Request $request) {

        $data = $request->input('params');
        $order_id = $data['order_id'];

        $void_item = DB::table('void_item')
            ->select('void_item.*', 'order_details.menu_item_name')
            ->where('void_item.order_id', $order_id)
            ->join('order_details', 'order_details.order_details_id', 'void_item.order_details_id')
            ->get();

        $output =  array(
            'void_item' => $void_item,
        );


        return json_encode($output);
    }


    /*
    public function voidAccept(Request $request) {
        $data = $request->input('params');
        $approved_admin_id = Auth::user()->id;
        $date = date('Y-m-d H:i:s');

        DB::table('void')
            ->where('void_id', $data['void_id'])
            ->update([
                'void_status' => 'accepted',
                'approved_admin_id' => $approved_admin_id,
                'updated_at' => $date
            ]);

        DB::table('orders')
            ->where('order_id', $data['order_id'])
            ->update([
                'order_status' => 'accepted_void',
                'updated_at' => $date
            ]);

        return json_encode("success");
    }


    public function itemVoidAccept(Request $request) {
        $data = $request->input('params');
        $approved_admin_id = Auth::user()->id;
        $date = date('Y-m-d H:i:s');

        DB::table('void_item')
            ->where('void_item_id', $data['void_item_id'])
            ->update([
                'void_status' => 'accepted',
                'approved_admin_id' => $approved_admin_id,
                'updated_at' => $date
            ]);

        return json_encode("success");
    }
*/
}
