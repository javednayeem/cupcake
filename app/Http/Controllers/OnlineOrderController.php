<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use DB;
use DateTime;
use Illuminate\Foundation\Auth\RegistersUsers;
use App;
use Session;


class OnlineOrderController extends Controller {

    use RegistersUsers;


    public function __construct() {
        //$this->middleware('auth');
    }


    public function index() {

        $restaurant_id = Auth::user()->restaurant_id;

        $table_data = DB::table('tables')
            ->select('table_id', 'restaurant_id', 'table_name', 'capacity', 'created_at', 'updated_at', 'reserved'
                , DB::raw("(SELECT order_id FROM `orders` WHERE table_id= tables.table_id ORDER BY order_id DESC limit 1) as order_id")
                , DB::raw("(SELECT customer_id FROM `orders` WHERE table_id= tables.table_id ORDER BY order_id DESC limit 1) as customer_id")
                , DB::raw("(SELECT waiter_id FROM `orders` WHERE table_id= tables.table_id ORDER BY order_id DESC limit 1) as waiter_id")
                , DB::raw("(SELECT order_type FROM `orders` WHERE table_id= tables.table_id ORDER BY order_id DESC limit 1) as order_type")
                , DB::raw("(SELECT discount FROM `orders` WHERE table_id= tables.table_id ORDER BY order_id DESC limit 1) as discount")
                , DB::raw("(SELECT discount_percentage FROM `orders` WHERE table_id= tables.table_id ORDER BY order_id DESC limit 1) as discount_percentage")
                , DB::raw("(SELECT discount_amount_type FROM `orders` WHERE table_id= tables.table_id ORDER BY order_id DESC limit 1) as discount_amount_type")
                , DB::raw("(SELECT discount_reference FROM `orders` WHERE table_id= tables.table_id ORDER BY order_id DESC limit 1) as discount_reference")
                , DB::raw("(SELECT order_status FROM `orders` WHERE table_id= tables.table_id order by order_id desc limit 1) as order_status")
                , DB::raw("(SELECT updated_at FROM `orders` WHERE table_id= tables.table_id order by order_id desc limit 1) as order_created"))
            ->where('restaurant_id', $restaurant_id)
            ->get();


        $menu_data = DB::table('menus')
            ->where('restaurants_id', $restaurant_id)
            ->where('status', 1)
            ->orderBy('menu_name')
            ->get();


        $menu_items = DB::table('menu_items')
            ->rightJoin('menus', 'menus.menu_id', '=', 'menu_items.menu_id')
            ->where('menus.restaurants_id', $restaurant_id)
            ->get();


        $modifiers = DB::table('modifiers')
            ->where('restaurant_id', $restaurant_id)
            ->get();


        $order_instructions = DB::table('order_instructions')
            ->where('restaurant_id', $restaurant_id)
            ->get();


        $setmenu_items = DB::table('setmenu_items')
            ->where('restaurant_id', $restaurant_id)
            ->get();


        $restaurant_data = DB::table('restaurants')
            ->select('restaurants.*',
                DB::raw("(SELECT COUNT(workperiod_id)FROM work_period WHERE restaurant_id=$restaurant_id AND status=1) as work_period_status"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='guest_bill') as guest_bill"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='waiter_order_void') as waiter_order_void"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='kitchen_type') as kitchen_type"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='receipt_footer') as receipt_footer"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='order_receipt_debug') as order_receipt_debug"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='menu_font_size') as menu_font_size"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='default_waiter') as default_waiter"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='pay_first') as pay_first"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='item_box_size') as item_box_size"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='view_settle_receipt') as view_settle_receipt"),
                DB::raw("(SELECT setting_value FROM app_settings WHERE setting_key='receipt_company') as receipt_company"),
                DB::raw("(SELECT setting_value FROM app_settings WHERE setting_key='receipt_phone') as receipt_phone")
            )
            ->where('restaurants_id', $restaurant_id)
            ->first();


        $printers = DB::table('printers')
            ->where('restaurant_id', $restaurant_id)
            ->get();


        $bank_cards = DB::table('bank_cards')->get();
        $payment_methods = DB::table('payment_methods')->get();
        $order_types = DB::table('order_types')
            ->select('order_types.*')
            ->where('order_types_permission.restaurant_id', $restaurant_id)
            ->where('order_types_permission.permission', 1)
            ->join('order_types_permission', 'order_types_permission.type_id', 'order_types.type_id')
            ->get();


        return view('cupcake.online-order.index', [
            'table_data' => $table_data,
            'menu_data' => $menu_data,
            'menu_items' => $menu_items,
            'restaurant_data' => $restaurant_data,
            'printers' => $printers,
            'setmenu_items' => $setmenu_items,
            'modifiers' => $modifiers,
            'order_instructions' => $order_instructions,
            'bank_cards' => $bank_cards,
            'payment_methods' => $payment_methods,
            'order_types' => $order_types,
        ]);
    }


    public function placeOrder(Request $request) {

        $restaurant_id = Auth::user()->restaurant_id;
        $customer_id = Auth::user()->id;

        $data = $request->input('params');

        $scheduled_date = $data['scheduled_date'];
        $scheduled_time = $data['scheduled_time'];
        $scheduled_at = $scheduled_date . ' ' . $scheduled_time;
        $order_notes = $data['order_notes'];


        $order_id = DB::table('online_orders')->insertGetId([
            'restaurant_id' => $restaurant_id,
            'customer_id' => $customer_id,
            'scheduled_at' => $scheduled_at,
            'order_notes' => $order_notes,
        ]);


        if (isset($data["order_items"])) {

            for ($i=0; $i<count($data["order_items"]); $i++) {

                $item_id = $data["order_items"][$i]['item_id'];
                $item_name = $data["order_items"][$i]['item_name'];
                $price = $data["order_items"][$i]['price'];
                $count = $data["order_items"][$i]['count'];
                $item_vat = $data["order_items"][$i]['item_vat'];

                DB::table('online_order_details')->insert([
                    'order_id' => $order_id,
                    'menu_item_id' => $item_id,
                    'menu_item_name' => $item_name,
                    'item_price' => $price,
                    'item_quantity' => $count,
                    'item_vat' => $item_vat,
                ]);
            }
        }

        reCalculateOnlineBill($order_id);

        return json_encode($order_id);
    }


    public function registerCustomerLayout($restaurant_id) {

        $data = DB::table('users')
            ->select('id', 'restaurant_id')
            ->where('restaurant_id', $restaurant_id)
            ->first();

        return view('auth.register', [
            'data' => $data
        ]);

    }


    public function registerCustomer(Request $request) {

        $restaurant_id = $request->input('restaurant_id');
        $creator_id = $request->input('creator_id');
        $name = $request->input('name');
        $email = $request->input('email');
        $password = $request->input('password');
        $role = 'customer';

        $rules = [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ];

        $validate = Validator::make($request->all(), $rules);

        if ($validate->passes()) {

            $user = new User;

            $user->name = $name;
            $user->email = $email;
            $user->password = Hash::make($password);
            $user->role = $role;
            $user->restaurant_id = $restaurant_id;
            $user->creator_id = $creator_id;
            $user->accounts = 0;

            $user->save();

            $this->guard()->login($user);

            return redirect('/online-order');

        }

        else {
            return back()->withErrors($validate)->withInput();
        }

    }


    public function onlineOrders() {

        $restaurant_id = Auth::user()->restaurant_id;

        $restaurant = DB::table('restaurants')
            ->where('restaurants_id', $restaurant_id)
            ->first();

        $orders = DB::table('online_orders')
            ->select('online_orders.*', 'users.name', 'users.email')
            ->where('online_orders.restaurant_id', $restaurant_id)
            ->join('users', 'users.id', 'online_orders.customer_id')
            ->orderBy('online_orders.order_id', 'DESC')
            ->get();


        return view('cupcake.online-order.orders', [
            'restaurant' => $restaurant,
            'orders' => $orders,
        ]);
    }


    public function changeOrderStatus(Request $request) {

        $data = $request->input('params');

        $order_id = $data['order_id'];
        $order_status = $data['order_status'];

        DB::table('online_orders')
            ->where('order_id', $order_id)
            ->update(['order_status' => $order_status]);


        return json_encode('success');
    }


    public function getOrderHistory(Request $request) {

        $data = $request->input('params');

        $order_id = $data['order_id'];

        $order = DB::table('online_orders')
            ->where('online_orders.order_id', $order_id)
            ->first();

        $order_details = DB::table('online_order_details')
            ->select('online_order_details.*', 'menu_items.ratio')
            ->where('online_order_details.order_id', $order_id)
            ->join('menu_items', 'menu_items.menu_item_id', 'online_order_details.menu_item_id')
            ->get();


        $output =  [
            'order' => $order,
            'order_details' => $order_details,
        ];

        return json_encode($output);
    }


    public function myOrders() {

        $restaurant_id = Auth::user()->restaurant_id;
        $customer_id = Auth::user()->id;

        $restaurant = DB::table('restaurants')
            ->where('restaurants_id', $restaurant_id)
            ->first();

        $orders = DB::table('online_orders')
            ->select('online_orders.*')
            ->where('online_orders.customer_id', $customer_id)
            ->orderBy('online_orders.order_id', 'DESC')
            ->paginate(20);


        return view('cupcake.online-order.my-orders', [
            'restaurant' => $restaurant,
            'orders' => $orders,
        ]);
    }


    public function viewReceipt($id) {

        $restaurant_id = Auth::user()->restaurant_id;

        $restaurant = DB::table('restaurants')
            ->select('restaurants.*',
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='receipt_footer') as receipt_footer"),
                DB::raw("(SELECT setting_value FROM app_settings WHERE setting_key='receipt_company') as receipt_company"),
                DB::raw("(SELECT setting_value FROM app_settings WHERE setting_key='receipt_phone') as receipt_phone"))
            ->where('restaurants_id', $restaurant_id)
            ->first();

        $orders = DB::table('online_orders')
            ->select('online_orders.*', 'users.name', 'users.email')
            ->where('online_orders.order_id', $id)
            ->join('users', 'users.id', 'online_orders.customer_id')
            ->first();

        $order_details = DB::table('online_order_details')
            ->where('online_order_details.order_id', $id)
            ->get();


        return view('cupcake.online-order.receipt', [
            'restaurant' => $restaurant,
            'orders' => $orders,
            'order_details' => $order_details,
        ]);
    }

}
