<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

class ModifierController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }


    public function index() {

        $restaurant_id = Auth::user()->restaurant_id;

        $modifiers = DB::table('modifiers')
            ->where('restaurant_id', $restaurant_id)
            ->get();

        return view('cupcake.menu.modifiers', [
            'modifiers' => $modifiers
        ]);
    }


    public function addModifier(Request $request) {

        $data= $request->input('params');
        $restaurant_id = Auth::user()->restaurant_id;

        $modifier_id = DB::table('modifiers')->insertGetId([
                'restaurant_id' => $restaurant_id,
                'modifier_name' => $data['modifier_name'],
                'price' => $data['price'],
                'status' => $data['status'],
                'modifier_vat' => $data['modifier_vat'],
            ]);

        echo json_encode($modifier_id);
    }


    public function editModifier(Request $request) {

        $data = $request->input('params');
        $date = date('Y-m-d H:i:s');

        DB::table('modifiers')
            ->where('modifier_id', $data['modifier_id'])
            ->update([
                'modifier_name' => $data['modifier_name'],
                'price' => $data['price'],
                'status' => $data['status'],
                'modifier_vat' => $data['modifier_vat'],
                'updated_at' => $date,
            ]);

        echo json_encode("success");
    }


    public function deleteModifier(Request $request) {

        $data = $request->input('params');

        DB::table('modifiers')->where('modifier_id', $data['modifier_id'])->delete();

        echo json_encode("success");
    }

}
