<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }


    public function dashboard() {

        $restaurant_id = Auth::user()->restaurant_id;

        $today_date = date("Y-m-d");
        $next_date = date('Y-m-d', strtotime("+1 day"));

        //$from = $today_date . ' 00:00:00';
        //$to = $today_date . ' 23:59:59';

        $workperiod = getCurrentWorkPeriod($restaurant_id);
        if ($workperiod == "0") $from = $today_date . ' 00:00:00';
        else $from = $workperiod . ' 00:00:00' ;
        $to = $today_date . ' 23:59:59';

        $licenses = "";

        $restaurant = DB::table('restaurants')
            ->where('restaurants_id', $restaurant_id)
            ->select('restaurants.*'
                , DB::raw("(SELECT SUM(amount)FROM order_payments WHERE restaurant_id=$restaurant_id AND (created_at BETWEEN '$from' AND '$to')) as today_sale")
                , DB::raw("(SELECT SUM(total_bill) FROM orders WHERE restaurant_id=$restaurant_id AND order_status='paid') as total_revenue")
                , DB::raw("(SELECT COUNT(customer_id)FROM customers WHERE restaurant_id=$restaurant_id) as customers")
                , DB::raw("(SELECT COUNT(id)FROM users WHERE restaurant_id=$restaurant_id) as stuffs"))
            ->first();


        $payment_methods = DB::table('payment_methods')
            ->select('payment_methods.method_name',
                DB::raw("(SELECT SUM(amount)FROM order_payments WHERE payment_method=payment_methods.method_name AND restaurant_id=$restaurant_id AND (order_payments.created_at BETWEEN '$from' AND '$to')) as amount"))
            ->get();



        if ($restaurant->license != 0) {
            $licenses = DB::table('licenses')->where('license_id', $restaurant->license_id)->first();
        }


        $work_period = DB::table('work_period')
            ->select('work_period.*'
                , DB::raw("(SELECT name FROM users WHERE users.id=work_period.start_time_creator) as start_time_creator_name")
                , DB::raw("(SELECT name FROM users WHERE users.id=work_period.end_time_creator) as end_time_creator_name")
            )
            ->where('work_period.restaurant_id', $restaurant_id)
            ->orderBy('workperiod_id', 'DESC')
            ->get();


        return view('cupcake.dashboard.index', [
            'restaurant' => $restaurant,
            'payment_methods' => $payment_methods,
            'work_period' => $work_period,
            'licenses' => $licenses,
        ]);
    }


    public function profile() {

        $user_id = Auth::user()->id;

        $user_data = DB::table('users')
            ->where('id', $user_id)
            ->first();

        return view('cupcake.dashboard.profile', [
            'user_data' => $user_data

        ]);

    }


    public function startWorkPeriod(Request $request) {

        $restaurant_id = Auth::user()->restaurant_id;
        $creator_id = Auth::user()->id;
        date_default_timezone_set('Asia/Dhaka');
        $date = date('Y-m-d H:i:s');

        DB::table('work_period')->insert([
            'restaurant_id' => $restaurant_id,
            'start_time' => $date,
            'start_time_creator' => $creator_id,
        ]);

        return json_encode('success');
    }


    public function endWorkPeriod() {

        $creator_id = Auth::user()->id;
        date_default_timezone_set('Asia/Dhaka');
        $date = date('Y-m-d H:i:s');
        $restaurant_id = Auth::user()->restaurant_id;

        $work_period = DB::table('work_period')
            ->select('work_period.*',
                DB::raw("(SELECT setting_value FROM `restaurant_settings` WHERE restaurant_id=$restaurant_id AND setting_key='check_order_before_end_workperiod') as check_order_before_end_workperiod"))
            ->where('work_period.restaurant_id', $restaurant_id)
            ->where('work_period.status', 1)
            ->first();

        $start_time = date('Y-m-d 00:00:00', strtotime( $work_period->start_time ));

        $pending_order = DB::table('orders')
            ->where('restaurant_id', $restaurant_id)
            ->where('order_status', 'placed')
            ->where('created_at', '>=', $start_time)
            ->count();

        if ($work_period->check_order_before_end_workperiod == 0) $pending_order = 0;

        if ($pending_order == 0) {

            DB::table('work_period')
                ->where('restaurant_id', $restaurant_id)
                ->where('status', 1)
                ->update([
                    'status' => 0,
                    'end_time' => $date,
                    'end_time_creator' => $creator_id,
                ]);

            //app('App\Http\Controllers\ScriptController')->fixOrderData();
            app('App\Http\Controllers\BackUpController')->createBackup();
            app('App\Http\Controllers\MailController')->emailDailyReport($restaurant_id);

            $response = [
                "status"   => "success",
                "message" => "Work Period Ended"
            ];

        }

        else {

            $response = [
                "status"   => "failure",
                "message" => "Please Settle All Placed Order Before Ending Work Period"
            ];

        }



        return json_encode($response);
    }


    public function editProfile(Request $request) {

        $file = $request->file('image');
        $name = Input::get('name');
        $user_id = Auth::user()->id;


        if ($request->hasFile('image')) {

            $file = $request->file('image');
            $this->validate($request, [
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:10000',
            ]);

            $file_name = time() . '.' .$file->getClientOriginalExtension();
            $destinationPath = 'images/users';
            $file->move($destinationPath,$file_name);


            DB::table('users')
                ->where('id', $user_id)
                ->update([
                    'user_img' => $file_name
                ]);
        }


        DB::table('users')
            ->where('id', $user_id)
            ->update([
                'name' => $name
            ]);

        return redirect('/profile');

    }


    public function editPassword(Request $request) {

        $data = $request->input('params');
        $user_id = Auth::user()->id;

        $old_password = $data['old_password'];
        $new_password = $data['new_password'];

//        $this->validate($request, [
//            'old' => 'required',
//            'password' => 'required|min:6|confirmed',
//        ]);

        $user = DB::table('users')->where('id', $user_id)->first();
        $hashedPassword = $user->password;

        if (Hash::check($old_password, $hashedPassword)) {

            DB::table('users')
                ->where('id', $user_id)
                ->update([
                    'password' => Hash::make($new_password)
                ]);

            return json_encode('success');
        }

        else return json_encode('failure');


    }


}
