<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use File;

use App\Model\Order;
use App\Model\OrderDetail;
use App\Model\OrderModifier;

class TableController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }


    public function index() {

        $restaurant_id = Auth::user()->restaurant_id;

        $table_data = DB::table('tables')
            ->select('table_id', 'restaurant_id', 'table_name', 'capacity', 'created_at', 'updated_at', 'reserved',
                DB::raw("(SELECT order_id FROM `orders` WHERE table_id= tables.table_id ORDER BY order_id DESC limit 1) as order_id"),
                DB::raw("(SELECT customer_id FROM `orders` WHERE table_id= tables.table_id ORDER BY order_id DESC limit 1) as customer_id"),
                DB::raw("(SELECT waiter_id FROM `orders` WHERE table_id= tables.table_id ORDER BY order_id DESC limit 1) as waiter_id"),
                DB::raw("(SELECT order_type FROM `orders` WHERE table_id= tables.table_id ORDER BY order_id DESC limit 1) as order_type"),
                DB::raw("(SELECT order_status FROM `orders` WHERE table_id= tables.table_id order by order_id desc limit 1) as order_status"),
                DB::raw("(SELECT created_at FROM `orders` WHERE table_id= tables.table_id order by order_id desc limit 1) as order_created"))
            ->where('restaurant_id', $restaurant_id)
            ->get();

        $restaurant_data = DB::table('restaurants')
            ->select('restaurants.*',
                DB::raw("(SELECT COUNT(workperiod_id)FROM work_period WHERE restaurant_id=$restaurant_id AND status=1) as work_period_status"))
            ->where('restaurants_id', $restaurant_id)
            ->first();


        return view('cupcake.order.table', [
            'restaurant_data' => $restaurant_data,
            'table_data' => $table_data,
        ]);
    }


    public function order($table_id) {

        //$this->checkReservedTable();

        $restaurant_id = Auth::user()->restaurant_id;

        $order_id = 0;
        $order_status = '';
        $order = [];

        $table_data = DB::table('tables')
            ->select('table_id', 'table_name', 'reserved'
                , DB::raw("(SELECT order_id FROM `orders` WHERE table_id= tables.table_id ORDER BY order_id DESC limit 1) as order_id")
                , DB::raw("(SELECT order_status FROM `orders` WHERE table_id= tables.table_id order by order_id desc limit 1) as order_status"))
            ->where('table_id', $table_id)
            ->first();


        if ($table_data->order_status == 'placed' || $table_data->order_status == 'guest_printed') {

            $order_id = $table_data->order_id;
            $order_status = $table_data->order_status;

            $order = DB::table('orders')
                ->where('order_id', $order_id)
                ->first();
        }

        $menu_data = DB::table('menus')
            ->where('restaurants_id', $restaurant_id)
            ->where('status', 1)
            ->orderBy('menu_name')
            ->get();


        $menu_items = DB::table('menu_items')
            ->rightJoin('menus', 'menus.menu_id', '=', 'menu_items.menu_id')
            ->where('menus.restaurants_id', '=', $restaurant_id)
            ->get();


        $modifiers = DB::table('modifiers')
            ->where('restaurant_id', $restaurant_id)
            ->get();


        $order_instructions = DB::table('order_instructions')
            ->where('restaurant_id', $restaurant_id)
            ->get();


        $setmenu_items = DB::table('setmenu_items')
            ->where('restaurant_id', $restaurant_id)
            ->get();


        $restaurant_data = DB::table('restaurants')
            ->select('restaurants.*',
                DB::raw("(SELECT COUNT(workperiod_id)FROM work_period WHERE restaurant_id=$restaurant_id AND status=1) as work_period_status"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='guest_bill') as guest_bill"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='waiter_order_void') as waiter_order_void"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='kitchen_type') as kitchen_type"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='receipt_footer') as receipt_footer"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='order_receipt_debug') as order_receipt_debug"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='menu_font_size') as menu_font_size"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='default_waiter') as default_waiter"),
                DB::raw("(SELECT setting_value FROM app_settings WHERE setting_key='receipt_company') as receipt_company"),
                DB::raw("(SELECT setting_value FROM app_settings WHERE setting_key='receipt_phone') as receipt_phone")
            )
            ->where('restaurants_id', $restaurant_id)
            ->first();


        $user_data = DB::table('users')
            ->where('restaurant_id', $restaurant_id)
            ->where('role', 'waiter')
            ->where('status', 1)
            ->get();


        $customer_data = DB::table('customers')
            ->where('restaurant_id', $restaurant_id)
            ->get();


        $bank_cards = DB::table('bank_cards')->get();


        return view('cupcake.order.order', [
            'order_id' => $order_id,
            'order_status' => $order_status,
            'order' => $order,
            'table_data' => $table_data,
            'menu_data' => $menu_data,
            'menu_items' => $menu_items,
            'user_data' => $user_data,
            'restaurant_data' => $restaurant_data,
            'customer_data' => $customer_data,
            'setmenu_items' => $setmenu_items,
            'modifiers' => $modifiers,
            'order_instructions' => $order_instructions,
            'bank_cards' => $bank_cards,
        ]);
    }


    public function placeOrder(Request $request) {

        $restaurant_id = Auth::user()->restaurant_id;
        $created_by = Auth::user()->id;
        $inventory = Auth::user()->inventory;

        $data = $request->input('params');
        $bill = 0;

        $created_at = getRestaurantTimestamp($restaurant_id);

        $customers = DB::table('customers')
            ->select('discount_percentage')
            ->where('customer_id', $data['customer_id'])
            ->first();

        $discount_percentage = $customers->discount_percentage;
        if ($data['discount_percentage'] > 0) $discount_percentage = $data['discount_percentage'];


        $order = new Order;

        $order->restaurant_id = $restaurant_id;
        $order->table_id = $data['table_id'];
        $order->waiter_id = $data['waiter_id'];
        $order->customer_id = $data['customer_id'];
        $order->order_type = $data['order_type'];
        $order->bill = $bill;
        $order->discount = $data['discount'];
        $order->discount_percentage = $discount_percentage;
        $order->discount_amount_type = $data['discount_amount_type'];
        $order->discount_reference = str_replace("'","", $data['discount_reference']);
        $order->total_bill = $bill;
        $order->order_notes = $data['order_notes'];
        $order->created_by = $created_by;
        $order->created_at = $created_at;

        $order->save();

        $order_id = $order->order_id;

        for ($i=0; $i<count($data["order_items"]); $i++) {

            $item = $data["order_items"][$i];

            $order_detail = new OrderDetail;

            $order_detail->order_id = $order_id;
            $order_detail->menu_item_id = $item['id'];
            $order_detail->menu_item_name = $item['item_name'];
            $order_detail->item_price = $item['price'];
            $order_detail->item_discount = $item['discount'];
            $order_detail->item_quantity = $item['count'];
            $order_detail->item_vat = $item['item_vat'];
            $order_detail->created_at = $created_at;

            $order_detail->save();

            $order_details_id = $order->order_details_id;



            if (isset($data["order_modifiers"])) {

                for ($j=0; $j<count($data["order_modifiers"]); $j++) {

                    $item = $data["order_modifiers"][$j];

                    $order_modifier = new OrderModifier;

//                    if ($menu_item_id == $item_id && $modifier_quantity>0) {
//
//                        DB::table('order_modifiers')->insert([
//                            'order_id' => $order_id,
//                            'modifier_id' => $modifier_id,
//                            'order_details_id' => $order_details_id,
//                            'quantity' => $modifier_quantity,
//                            'created_at' => $created_at,
//                        ]);
//                    }

                }

            }

        }

        if ($inventory) recipe($order_id);
        reCalculateBill($order_id);

        return json_encode($order_id);
    }


}
