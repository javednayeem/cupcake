<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Illuminate\Support\Facades\Schema;
use Storage;
use File;

use Mail;

class BackUpController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }


    public function index() {

        $this->checkBackupSchedule();

        date_default_timezone_set("Asia/Dhaka");

        $now = date("Y-m-d H:i:s");

        $on_queue = 1;
        $created_backup = 0;

        $backup_schedule = DB::table('backup_schedule')
            ->where('next_schedule', '>=' ,$now)
            ->where('on_queue', $on_queue)
            ->where('created_backup', $created_backup)
            ->first();

        $backup_log = DB::table('backup_log')
            ->orderBy('created_at', 'desc')
            ->paginate(20);


//        foreach ($backup_log as $log) {
//
//            $content = Storage::get($log->file_name);
//
//            echo "$content";
//
//            //$this->truncateDatabase();
//
//            //$results = DB::raw("$content");
//
//            $location = storage_path("app/$log->file_name");
//
//        }

        return view('cupcake.settings.backup', [
            'backup_schedule' => $backup_schedule,
            'backup_log' => $backup_log,
        ]);


    }


    public function createBackup() {

        $tables = DB::select('SHOW TABLES');
        $db_name = 'Tables_in_' . DB::connection()->getDatabaseName();
        $sql_query = '';


        foreach($tables as $table) {

            if($table->$db_name != 'migrations') {

                $table_name = $table->$db_name;

                //echo $table_name . "<br>";

                $columns = Schema::getColumnListing($table_name);
                $data = DB::table($table_name)->get();
                $sql = "INSERT INTO `$table_name` (";
                $count = 0;

                foreach ($columns as $column) {

                    if ($count++ > 0) $sql .= ", ";
                    $sql .= "`$column`";

                }

                $sql .= ") VALUES (";
                $line = 0;

                foreach ($data as $row) {

                    $count = 0;
                    $col_length = count($data);

                    foreach ($columns as $column) {

                        if ($count > 0) $sql .= ", ";
                        else if ($count > 1) $sql .= " (";

                        $key = $row->$column;

                        if ($line>0 && $count == 0) $sql .= "(";

                        $sql .= "'$key'";
                        $count++;

                    }

                    $sql .= ")";
                    $line++;

                    if ($line < $col_length) $sql .= ", ";
                }


                $sql .= "; ";

                if (count($data) == 0) $sql = '';
                else $sql_query .= $sql;
            }


        }

        //echo $sql_query;
        date_default_timezone_set("Asia/Dhaka");
        $file = date('d-m-Y_H-i-s') . '_' .time() . '.txt';

        DB::table('backup_log')->insert([
            'file_name' => $file
        ]);

        $this->createNextScheduleTime();

        Storage::put($file, $sql_query, 'public');

        //Storage::disk('local')->put($file, $sql_query);


/*
        $data = json_encode($sql_query);
        //$file = time() . '.txt';
        $destinationPath = public_path()."/backup/";
        if (!is_dir($destinationPath)) {  mkdir($destinationPath,0777,true);  }
        File::put($destinationPath.$file,$data);
*/



    }


    private function createNextScheduleTime() {

        $app_settings = DB::table('app_settings')
            ->where('setting_key', 'backup_interval')
            ->first();

        $backup_interval = $app_settings->setting_value;

        date_default_timezone_set("Asia/Dhaka");

        $next_schedule = date("Y-m-d H:i:s", strtotime("+$backup_interval hours"));

        $schedule_id = DB::table('backup_schedule')->insertGetId([
            'next_schedule' => $next_schedule
        ]);

        DB::table('backup_schedule')
            ->where('schedule_id', '<>', $schedule_id)
            ->update([
                'on_queue' => 0,
            ]);

    }


    public function checkBackupSchedule() {

        date_default_timezone_set("Asia/Dhaka");

        $now = date("Y-m-d H:i:s");

        $on_queue = 1;
        $created_backup = 0;

        $backup_schedule = DB::table('backup_schedule')
            ->where('next_schedule', '<=' ,$now)
            ->where('on_queue', $on_queue)
            ->where('created_backup', $created_backup)
            ->get();

        foreach ($backup_schedule as $schedule) {

            $this->createBackup();

            DB::table('backup_schedule')
                ->where('schedule_id', $schedule->schedule_id)
                ->update([
                    'on_queue' => 0,
                    'created_backup' => 1,
                ]);

        }

    }


    public function deleteBackup(Request $request) {

        $data = $request->input('params');

        $log_id = $data['log_id'];

        $backup_log = DB::table('backup_log')
            ->where('log_id', $log_id)
            ->first();

        Storage::delete($backup_log->file_name);

        DB::table('backup_log')->where('log_id', $log_id)->delete();

        return json_encode('success');



    }


    private function truncateDatabase() {

        $tables = DB::select('SHOW TABLES');
        $db_name = 'Tables_in_' . DB::connection()->getDatabaseName();

        foreach($tables as $table) {

            if ($table->$db_name != 'migrations') {

                $table_name = $table->$db_name;

                DB::table($table_name)->delete();

            }
        }


    }


    public function downloadBackup($filename) {
        return Storage::download($filename);
    }




}
