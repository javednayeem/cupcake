<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

class RecipeController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }


    public function index() {

        $restaurant_id = Auth::user()->restaurant_id;

        $menu_items = DB::table('menu_items')
            ->select('menu_items.*', 'menus.menu_name')
            ->where('menus.restaurants_id', $restaurant_id)
            ->join('menus', 'menus.menu_id', 'menu_items.menu_id')
            ->get();

        $inv_products = DB::table('inv_product')
            ->where('restaurant_id', $restaurant_id)
            ->orderBy('inv_product.product_name')
            ->get();

        $inv_stores = DB::table('inv_stores')
            ->where('restaurant_id', $restaurant_id)
            ->get();

        $product_category = DB::table('inv_product_category')
            ->where('restaurant_id', $restaurant_id)
            ->orderBy('inv_product_category.category_name')
            ->get();

        $units = DB::table('inv_units')
            ->where('restaurant_id', $restaurant_id)
            ->get();


        return view('cupcake.menu.recipes', [
            'menu_items' => $menu_items,
            'inv_products' => $inv_products,
            'inv_stores' => $inv_stores,
            'product_category' => $product_category,
            'units' => $units,
        ]);
    }


    public function viewRecipeItems(Request $request) {

        $data= $request->input('params');

        $recipes = DB::table('recipes')
            ->select('recipes.*', 'inv_product.product_name', 'inv_units.unit_name')
            ->where('recipes.item_id', $data['item_id'])
            ->join('inv_product', 'inv_product.product_id', 'recipes.product_id')
            ->join('inv_units', 'inv_units.unit_id', 'inv_product.unit_id')
            ->get();

        return json_encode($recipes);
    }


    public function addRecipe(Request $request) {

        $data= $request->input('params');

        $product_id = $data['product_id'];
        $unit_of_quantity = $data['unit_of_quantity'];
        $consumption_quantity = $quantity = $data['quantity'];

        $inv_product = DB::table('inv_product')
            ->where('inv_product.product_id', $product_id)
            ->first();

        $product_unit_id = $inv_product->unit_id;

        if ($product_unit_id != $unit_of_quantity) {

            $unit_conversion = DB::table('unit_conversion')
                ->where('from_unit_id', $product_unit_id)
                ->where('to_unit_id', $unit_of_quantity)
                ->first();

            $conversion_rate = $unit_conversion->conversion_rate;

            $consumption_quantity = round(($quantity/$conversion_rate), 2);

        }

        $recipe_id = DB::table('recipes')->insertGetId([
            'item_id' => $data['item_id'],
            'product_id' => $data['product_id'],
            'quantity' => $consumption_quantity,
            'store_id' => $data['store_id'],
        ]);

        return json_encode($recipe_id);
    }


    public function editRecipe(Request $request) {

        $data = $request->input('params');
        $date = date('Y-m-d H:i:s');

        DB::table('recipes')
            ->where('recipe_id', $data['recipe_id'])
            ->update([
                'quantity' => $data['quantity'],
                'updated_at' => $date,
            ]);

        return json_encode("success");
    }


    public function deleteRecipe(Request $request) {

        $data = $request->input('params');

        DB::table('recipes')->where('recipe_id', $data['recipe_id'])->delete();

        return json_encode("success");
    }

}
