<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use File;

class OrderController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        date_default_timezone_set("Asia/Dhaka");
    }


    public function index() {

        $this->checkReservedTable();

        $restaurant_id = Auth::user()->restaurant_id;

        $table_data = DB::table('tables')
            ->select('table_id', 'restaurant_id', 'table_name', 'capacity', 'created_at', 'updated_at', 'reserved'
                , DB::raw("(SELECT order_id FROM `orders` WHERE table_id= tables.table_id ORDER BY order_id DESC limit 1) as order_id")
                , DB::raw("(SELECT customer_id FROM `orders` WHERE table_id= tables.table_id ORDER BY order_id DESC limit 1) as customer_id")
                , DB::raw("(SELECT waiter_id FROM `orders` WHERE table_id= tables.table_id ORDER BY order_id DESC limit 1) as waiter_id")
                , DB::raw("(SELECT order_type FROM `orders` WHERE table_id= tables.table_id ORDER BY order_id DESC limit 1) as order_type")
                , DB::raw("(SELECT discount FROM `orders` WHERE table_id= tables.table_id ORDER BY order_id DESC limit 1) as discount")
                , DB::raw("(SELECT discount_percentage FROM `orders` WHERE table_id= tables.table_id ORDER BY order_id DESC limit 1) as discount_percentage")
                , DB::raw("(SELECT discount_amount_type FROM `orders` WHERE table_id= tables.table_id ORDER BY order_id DESC limit 1) as discount_amount_type")
                , DB::raw("(SELECT discount_reference FROM `orders` WHERE table_id= tables.table_id ORDER BY order_id DESC limit 1) as discount_reference")
                , DB::raw("(SELECT order_status FROM `orders` WHERE table_id= tables.table_id order by order_id desc limit 1) as order_status")
                , DB::raw("(SELECT updated_at FROM `orders` WHERE table_id= tables.table_id order by order_id desc limit 1) as order_created"))
            ->where('restaurant_id', $restaurant_id)
            ->get();


        $menu_data = DB::table('menus')
            ->where('restaurants_id', $restaurant_id)
            ->where('status', 1)
            ->orderBy('menu_name')
            ->get();


        $menu_items = DB::table('menu_items')
            ->rightJoin('menus', 'menus.menu_id', '=', 'menu_items.menu_id')
            ->where('menus.restaurants_id', $restaurant_id)
            ->get();


        $modifiers = DB::table('modifiers')
            ->where('restaurant_id', $restaurant_id)
            ->get();


        $order_instructions = DB::table('order_instructions')
            ->where('restaurant_id', $restaurant_id)
            ->get();


        $setmenu_items = DB::table('setmenu_items')
            ->where('restaurant_id', $restaurant_id)
            ->get();


        $restaurant_data = DB::table('restaurants')
            ->select('restaurants.*',
                DB::raw("(SELECT COUNT(workperiod_id)FROM work_period WHERE restaurant_id=$restaurant_id AND status=1) as work_period_status"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='guest_bill') as guest_bill"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='waiter_order_void') as waiter_order_void"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='kitchen_type') as kitchen_type"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='receipt_footer') as receipt_footer"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='order_receipt_debug') as order_receipt_debug"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='menu_font_size') as menu_font_size"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='default_waiter') as default_waiter"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='pay_first') as pay_first"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='item_box_size') as item_box_size"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='view_settle_receipt') as view_settle_receipt"),
                DB::raw("(SELECT setting_value FROM app_settings WHERE setting_key='receipt_company') as receipt_company"),
                DB::raw("(SELECT setting_value FROM app_settings WHERE setting_key='receipt_phone') as receipt_phone")
            )
            ->where('restaurants_id', $restaurant_id)
            ->first();


        $user_data = DB::table('users')
            ->where('restaurant_id', $restaurant_id)
            ->where('role', 'waiter')
            ->where('status', 1)
            ->get();


        $customer_data = DB::table('customers')
            ->where('restaurant_id', $restaurant_id)
            ->get();


        $printers = DB::table('printers')
            ->where('restaurant_id', $restaurant_id)
            ->get();


        $bank_cards = DB::table('bank_cards')->get();
        $payment_methods = DB::table('payment_methods')->get();
        $order_types = DB::table('order_types')
            ->select('order_types.*')
            ->where('order_types_permission.restaurant_id', $restaurant_id)
            ->where('order_types_permission.permission', 1)
            ->join('order_types_permission', 'order_types_permission.type_id', 'order_types.type_id')
            ->get();


        return view('cupcake.order.index', [
            'table_data' => $table_data,
            'menu_data' => $menu_data,
            'menu_items' => $menu_items,
            'user_data' => $user_data,
            'restaurant_data' => $restaurant_data,
            'customer_data' => $customer_data,
            'printers' => $printers,
            'setmenu_items' => $setmenu_items,
            'modifiers' => $modifiers,
            'order_instructions' => $order_instructions,
            'bank_cards' => $bank_cards,
            'payment_methods' => $payment_methods,
            'order_types' => $order_types,
        ]);
    }


    public function placeOrder(Request $request) {

        $restaurant_id = Auth::user()->restaurant_id;
        $created_by = Auth::user()->id;
        $data = $request->input('params');

        /*

        $workperiod = getCurrentWorkPeriod($restaurant_id);
        date_default_timezone_set("Asia/Dhaka");

        if ($workperiod == "0") $created_at = date("Y-m-d H:i:s");

        else {
            $now = date("H:i:s");
            $created_at = $workperiod . ' ' . $now;
        }
*/

        $created_at = getRestaurantTimestamp($restaurant_id);

        $card_name = $data['payment_method']=='card'? $data['card_name']:'0';

        $customers = DB::table('customers')
            ->select('discount_percentage')
            ->where('customer_id', $data['customer_id'])
            ->first();

        $discount_percentage = $customers->discount_percentage;

        if ($data['discount_percentage'] > 0) $discount_percentage = $data['discount_percentage'];

        $order_id = DB::table('orders')->insertGetId([
            'restaurant_id' => $restaurant_id,
            'table_id' => $data['table_id'],
            'waiter_id' => $data['waiter_id'],
            'customer_id' => $data['customer_id'],
            'order_type' => $data['order_type'],
            'bill' => $data['bill'],
            'vat' => $data['vat'],
            'service_charge' => $data['service_charge'],
            'discount' => $data['discount'],
            'discount_percentage' => $discount_percentage,
            'discount_amount_type' => $data['discount_amount_type'],
            'discount_reference' => str_replace("'","", $data['discount_reference']) ,
            'total_bill' => $data['total_bill'],
            'order_notes' => $data['order_notes'],
            'created_by' => $created_by,
            'created_at' => $created_at,
        ]);



        for ($i=0; $i<count($data["order_items"]); $i++) {

            $menu_item_id = $data["order_items"][$i][0];
            $quantity = $data["order_items"][$i][4];

            $order_details_id = DB::table('order_details')->insertGetId([
                'order_id' => $order_id,
                'menu_item_id' => $menu_item_id,
                'menu_item_name' => $data["order_items"][$i][1],
                'item_price' => $data["order_items"][$i][2],
                'item_discount' => $data["order_items"][$i][3],
                'item_quantity' => $quantity,
                'item_vat' => $data["order_items"][$i][5],
                'created_at' => $created_at,
            ]);



            if (isset($data["order_modifiers"])) {

                for ($j=0; $j<count($data["order_modifiers"]); $j++) {

                    $item_id = $data["order_modifiers"][$j][0];
                    $modifier_id = $data["order_modifiers"][$j][1];
                    $modifier_quantity = $data["order_modifiers"][$j][2];

                    if ($menu_item_id == $item_id && $modifier_quantity>0) {

                        DB::table('order_modifiers')->insert([
                            'order_id' => $order_id,
                            'modifier_id' => $modifier_id,
                            'order_details_id' => $order_details_id,
                            'quantity' => $modifier_quantity,
                            'created_at' => $created_at,
                        ]);
                    }

                }

            }


            //updateItemQuantity($menu_item_id, $quantity);
        }

        //recipe($order_id);
        reCalculateBill($order_id);

        echo json_encode($order_id);
    }


    public function settleLayout($order_id) {

        $restaurant_id = Auth::user()->restaurant_id;

        $restaurant_data = DB::table('restaurants')
            ->select('restaurants.*',
                DB::raw("(SELECT COUNT(workperiod_id)FROM work_period WHERE restaurant_id=$restaurant_id AND status=1) as work_period_status"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='guest_bill') as guest_bill"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='waiter_order_void') as waiter_order_void"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='kitchen_type') as kitchen_type"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='receipt_footer') as receipt_footer"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='order_receipt_debug') as order_receipt_debug"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='menu_font_size') as menu_font_size"),
                DB::raw("(SELECT setting_value FROM app_settings WHERE setting_key='receipt_company') as receipt_company"),
                DB::raw("(SELECT setting_value FROM app_settings WHERE setting_key='receipt_phone') as receipt_phone")
            )
            ->where('restaurants_id', $restaurant_id)
            ->first();


        $order = DB::table('orders')->where('order_id', $order_id)->first();

        $bank_cards = DB::table('bank_cards')->get();


        return view('cupcake.order.settle', [
            'order_id' => $order_id,
            'order' => $order,
            'restaurant_data' => $restaurant_data,
            'bank_cards' => $bank_cards,
        ]);
    }


    public function getOrder(Request $request) {

        $data = $request->input('params');

        $order_data = DB::table('order_details')
            ->where('order_id', '=', $data['order_id'])
            ->get();

        echo json_encode($order_data);
    }


    public function orderHistory() {

        $restaurant_id = Auth::user()->restaurant_id;

        $table_data = DB::table('tables')
            ->select('table_id', 'restaurant_id', 'table_name', 'capacity', 'created_at', 'updated_at'
                , DB::raw("(SELECT order_id FROM `orders` WHERE table_id= tables.table_id limit 1) as order_id")
                , DB::raw("(SELECT customer_id FROM `orders` WHERE table_id= tables.table_id limit 1) as customer_id")
                , DB::raw("(SELECT waiter_id FROM `orders` WHERE table_id= tables.table_id limit 1) as waiter_id")
                , DB::raw("(SELECT order_type FROM `orders` WHERE table_id= tables.table_id limit 1) as order_type")
                , DB::raw("(SELECT discount FROM `orders` WHERE table_id= tables.table_id limit 1) as discount")
                , DB::raw("(SELECT discount_reference FROM `orders` WHERE table_id= tables.table_id limit 1) as discount_reference")
                , DB::raw("(SELECT payment_method FROM `orders` WHERE table_id= tables.table_id limit 1) as payment_method")
                , DB::raw("(SELECT card_name FROM `orders` WHERE table_id= tables.table_id limit 1) as card_name")
                , DB::raw("(SELECT order_status FROM `orders` WHERE table_id= tables.table_id order by order_id desc limit 1) as order_status")
                , DB::raw("(SELECT created_at FROM `orders` WHERE table_id= tables.table_id order by order_id desc limit 1) as order_created"))
            ->where('restaurant_id', '=', $restaurant_id)
            ->get();

        $menu_data = DB::table('menus')
            ->where('restaurants_id', '=', $restaurant_id)
            ->get();

        $menu_items = DB::table('menu_items')
            ->rightJoin('menus', 'menus.menu_id', '=', 'menu_items.menu_id')
            ->where('menus.restaurants_id', '=', $restaurant_id)
            ->get();

        $restaurant_data = DB::table('restaurants')
            ->select('restaurants.*'
                , DB::raw("(SELECT COUNT(workperiod_id)FROM work_period WHERE restaurant_id=$restaurant_id AND status=1) as work_period_status")
            )
            ->where('restaurants_id', '=', $restaurant_id)
            ->first();



        $user_data = DB::table('users')
            ->where('restaurant_id', '=', $restaurant_id)
            ->get();

        $customer_data = DB::table('customers')
            ->where('restaurant_id', '=', $restaurant_id)
            ->get();


        return view('cupcake.order.order-history', [
            'table_data' => $table_data,
            'menu_data' => $menu_data,
            'menu_items' => $menu_items,
            'restaurant_data' => $restaurant_data,
            'user_data' => $user_data,
            'customer_data' => $customer_data,

        ]);

    }


    public function getOrderHistory(Request $request) {

        $data = $request->input('params');

        reCalculateBill($data['order_id']);

        $order_data = DB::table('orders')
            ->where('orders.order_id', $data['order_id'])
            ->select('orders.*', 'customers.customer_name', 'users.name as waiter_name', 'tables.table_name')
            ->join('customers', 'customers.customer_id', 'orders.customer_id')
            ->join('users', 'users.id', 'orders.waiter_id')
            ->join('tables', 'tables.table_id', 'orders.table_id')
            ->first();

        $order_details = DB::table('order_details')
            ->select('order_details.*', 'menu_items.ratio')
            ->where('order_details.order_id', $data['order_id'])
            ->join('menu_items', 'menu_items.menu_item_id', 'order_details.menu_item_id')
            ->get();

        $order_modifiers = DB::table('order_modifiers')
            ->where('order_modifiers.order_id', $data['order_id'])
            ->select('order_modifiers.*', 'modifiers.modifier_name', 'modifiers.price')
            ->join('modifiers', 'modifiers.modifier_id', 'order_modifiers.modifier_id')
            ->get();

        $order_payments = DB::table('order_payments')
            ->where('order_payments.order_id', $data['order_id'])
            ->select('order_payments.*'
                , DB::raw("(SELECT card_name FROM bank_cards WHERE bank_cards.card_id=order_payments.card_id) as card_name"))
            ->get();

        $output =  array(
            'order_data' => $order_data,
            'order_details' => $order_details,
            'order_modifiers' => $order_modifiers,
            'order_payments' => $order_payments,
        );

        //return response()->json(['order_data' => $order_data, 'order_details' => $order_details]);

        return json_encode($output);
    }


    public function modifyOrder(Request $request) {


        $restaurant_id = Auth::user()->restaurant_id;
        $data = $request->input('params');
        $date = date('Y-m-d H:i:s');
        $order_id = $data['order_id'];

//        $workperiod = getCurrentWorkPeriod($restaurant_id);
//        date_default_timezone_set("Asia/Dhaka");
//
//        if ($workperiod == "0") $created_at = date("Y-m-d H:i:s");
//
//        else {
//            $now = date("H:i:s");
//            $created_at = $workperiod . ' ' . $now;
//        }

        $created_at = getRestaurantTimestamp($restaurant_id);


        DB::table('orders')
            ->where('order_id', $order_id)
            ->update([
                'updated_at' => $date,
            ]);


        for ($i=0; $i<count($data["order_items"]); $i++) {

            $menu_item_id = $data["order_items"][$i][0];
            $quantity = $data["order_items"][$i][4];

            $order_details_id = DB::table('order_details')->insertGetId([
                'order_id' => $data['order_id'],
                'menu_item_id' => $menu_item_id,
                'menu_item_name' => $data["order_items"][$i][1],
                'item_price' => $data["order_items"][$i][2],
                'item_discount' => $data["order_items"][$i][3],
                'item_quantity' => $quantity,
                'item_vat' => $data["order_items"][$i][5],
                'created_at' => $created_at,
            ]);



            if (isset($data["order_modifiers"])) {

                for ($j=0; $j<count($data["order_modifiers"]); $j++) {

                    $item_id = $data["order_modifiers"][$j][0];
                    $modifier_id = $data["order_modifiers"][$j][1];
                    $modifier_quantity = $data["order_modifiers"][$j][2];

                    if ($menu_item_id == $item_id && $modifier_quantity>0) {

                        DB::table('order_modifiers')->insert([
                            'order_id' => $order_id,
                            'modifier_id' => $modifier_id,
                            'order_details_id' => $order_details_id,
                            'quantity' => $modifier_quantity
                        ]);
                    }
                }
            }
        }

        reCalculateBill($order_id);

        echo json_encode($order_id);
    }


    public function voidRequest(Request $request) {

        $restaurant_id = Auth::user()->restaurant_id;
        $cashier_id = Auth::user()->id;
        $data = $request->input('params');

        DB::table('void')->insert([
            'restaurant_id' => $restaurant_id,
            'order_id' => $data['order_id'],
            'waiter_id' => $data['waiter_id'],
            'cashier_id' => $cashier_id,
        ]);

        DB::table('orders')
            ->where('order_id',"=", $data["order_id"])
            ->update([
                'order_status' => 'requested_void'
            ]);

        echo json_encode("success");
    }


    public function voidRequestItem(Request $request) {

        $restaurant_id = Auth::user()->restaurant_id;
        $cashier_id = Auth::user()->id;
        $data = $request->input('params');

        $order_id = DB::table('order_details')
            ->select('order_id', 'menu_item_id', 'item_quantity')
            ->where('order_details_id', $data["order_details_id"])
            ->first();

        $order_id = $order_id->order_id;
        $menu_item_id = intval($order_id['menu_item_id']);
        $quantity = intval($order_id['item_quantity']);
        $quantity *= -1;

        DB::table('void_item')->insert([
            'restaurant_id' => $restaurant_id,
            'order_details_id' => $data['order_details_id'],
            'waiter_id' => $data['waiter_id'],
            'cashier_id' => $cashier_id,
        ]);

        DB::table('order_details')
            ->where('order_details_id', $data["order_details_id"])
            ->update([
                'item_status' => 0
            ]);

        reCalculateBill($order_id);
        //updateItemQuantity($menu_item_id, $quantity);

        return json_encode("success");
    }


    public function voidAccept(Request $request) {
        $data = $request->input('params');
        $approved_admin_id = Auth::user()->id;
        $date = date('Y-m-d H:i:s');

        DB::table('void')
            ->where('void_id', $data['void_id'])
            ->update([
                'void_status' => 'accepted',
                'approved_admin_id' => $approved_admin_id,
                'updated_at' => $date
            ]);

        DB::table('orders')
            ->where('order_id', $data['order_id'])
            ->update([
                'order_status' => 'accepted_void',
                'updated_at' => $date
            ]);

        return json_encode("success");
    }


    public function itemVoidAccept(Request $request) {
        $data = $request->input('params');
        $approved_admin_id = Auth::user()->id;
        $date = date('Y-m-d H:i:s');

        DB::table('void_item')
            ->where('void_item_id', $data['void_item_id'])
            ->update([
                'void_status' => 'accepted',
                'approved_admin_id' => $approved_admin_id,
                'updated_at' => $date
            ]);

        return json_encode("success");
    }


    public function confirmPayment(Request $request) {

        $restaurant_id = Auth::user()->restaurant_id;

        $data = $request->input('params');
        $date = date('Y-m-d H:i:s');

        $created_at = getRestaurantTimestamp($restaurant_id);

        $order_id = $data['order_id'];

        $total_bill = DB::table('orders')
            ->select('total_bill')
            ->where('order_id', $order_id)
            ->first();

        $total_bill = $total_bill->total_bill;

        DB::table('orders')
            ->where('order_id', $order_id)
            ->update([
                'given_amount' => $data['given_amount'],
                'order_status' => 'paid',
                'updated_at' => $date,
            ]);


        DB::table('order_details')
            ->where('order_id', $order_id)
            ->where('item_status', 1)
            ->update([
                'settle' => 1,
            ]);




        if (isset($data["payment_methods"])) {

            for ($i=0; $i<count($data["payment_methods"]); $i++) {

                $payment_method = $data["payment_methods"][$i]['payment_method'];
                $amount = $data["payment_methods"][$i]['paid_amount'];
                $bank_card = $data["payment_methods"][$i]['bank_card'];
                $card_number = $data["payment_methods"][$i]['card_number'];

                if ($amount>0) {

                    if ($payment_method == 'cash') {

                        DB::table('order_payments')->insert([
                            'order_id' => $order_id,
                            'payment_method' => $payment_method,
                            'amount' => $total_bill,
                            'card_id' => $bank_card,
                            'card_number' => $card_number,
                            'restaurant_id' => $restaurant_id,
                            'created_at' => $created_at,
                        ]);


                    }

                    else {

                        DB::table('order_payments')->insert([
                            'order_id' => $order_id,
                            'payment_method' => $payment_method,
                            'amount' => $amount,
                            'card_id' => $bank_card,
                            'card_number' => $card_number,
                            'restaurant_id' => $restaurant_id,
                            'created_at' => $created_at,
                        ]);

                    }



                }
            }
        }

        recipe($order_id);
        //app('App\Http\Controllers\BackUpController')->checkBackupSchedule();

        return json_encode('success');
    }


    public function editOrder(Request $request) {
        $data = $request->input('params');

        DB::table('orders')
            ->where('order_id', $data['order_id'])
            ->update([
                'table_id' => $data['table_id'],
                'waiter_id' => $data['waiter_id'],
                'customer_id' => $data['customer_id'],
                'order_type' => $data['order_type'],
                'discount' => $data['discount'],
                'discount_reference' => str_replace("'","", $data['discount_reference']),
                'payment_method' => $data['payment_method'],
                'card_name' => $data['card_name'],
            ]);
        return json_encode('success');
    }


    public function changeTable(Request $request) {
        $data = $request->input('params');

        $order_id = $data['order_id'];
        $table_id = $data['select_table'];

        DB::table('orders')
            ->where('order_id', $order_id)
            ->update([
                'table_id' => $table_id
            ]);

        return json_encode('success');
    }


    public function changeWaiter(Request $request) {
        $data = $request->input('params');

        $order_id = $data['order_id'];
        $waiter_id = $data['waiter_id'];

        DB::table('orders')
            ->where('order_id', $order_id)
            ->update([
                'waiter_id' => $waiter_id
            ]);

        return json_encode('success');
    }


    public function changeCustomer(Request $request) {
        $data = $request->input('params');

        $order_id = $data['order_id'];
        $customer_id = $data['customer_id'];

        $customers = DB::table('customers')
            ->select('discount_percentage')
            ->where('customer_id', $customer_id)
            ->first();

        $orders = DB::table('orders')
            ->select('discount_percentage')
            ->where('order_id', $order_id)
            ->first();

        $discount_percentage = $customer_discount_percentage = $customers->discount_percentage;
        $order_discount_percentage = $orders->discount_percentage;

        if ($order_discount_percentage > 0) $discount_percentage = $order_discount_percentage;

        DB::table('orders')
            ->where('order_id', $order_id)
            ->update([
                'customer_id' => $customer_id,
                'discount_percentage' => $discount_percentage,
            ]);


        reCalculateBill($order_id);

        return json_encode('success');
    }


    public function changeOrderType(Request $request) {
        $data = $request->input('params');

        $order_id = $data['order_id'];
        $order_type = $data['order_type'];

        DB::table('orders')
            ->where('order_id', $order_id)
            ->update([
                'order_type' => $order_type
            ]);

        return json_encode('success');
    }


    public function changeOrderStatus(Request $request) {
        $data = $request->input('params');

        $order_id = $data['order_id'];
        $order_status = $data['order_status'];

        DB::table('orders')
            ->where('order_id', $order_id)
            ->update([
                'order_status' => $order_status
            ]);

        return json_encode('success');
    }


    private function checkReservedTable() {

        $restaurant_id = Auth::user()->restaurant_id;

        $date = date('Y-m-d');
        $current_time = date('H:i:00');

        $reservations = DB::table('reservations')
            ->where('restaurant_id', $restaurant_id)
            ->where('reservation_date', $date)
            ->get();


        DB::table('tables')
            ->where('restaurant_id', $restaurant_id)
            ->update(['reserved' => 0]);


        foreach ($reservations as $reservation) {

            $tables = explode("|", $reservation->tables);
            $start_time = $reservation->start_time;
            $end_time = $reservation->end_time;

            for($i=0; $i<count($tables); $i++) {

                $table_name = $tables[$i];

                if ($current_time>=$start_time && $current_time<=$end_time) {

                    DB::table('tables')
                        ->where('restaurant_id', $restaurant_id)
                        ->where('table_name', $table_name)
                        ->update([
                            'reserved' => 1
                        ]);

                }

//                else {
//
//                    DB::table('tables')
//                        ->where('restaurant_id', $restaurant_id)
//                        ->where('table_name', $table_name)
//                        ->update([
//                            'reserved' => 0
//                        ]);
//
//                }
            }
        }

    }


    public function requestComplimentaryItem(Request $request) {

        $restaurant_id = Auth::user()->restaurant_id;
        $cashier_id = Auth::user()->id;
        $date = date('Y-m-d H:i:s');
        $data = $request->input('params');

        $order_id = $data['order_id'];
        $order_details_id = $data['order_details_id'];

        $item_quantity = $data['item_quantity'];
        $notes = $data['notes'];
        $waiter_id = $data['waiter_id'];


        $orders = DB::table('orders')
            ->where('order_id', $order_id)
            ->first();

        $order_details = DB::table('order_details')
            ->where('order_details_id', $order_details_id)
            ->first();


        DB::table('order_details')
            ->where('order_details_id', $order_details_id)
            ->delete();


        if ($order_details->item_quantity > $item_quantity) {

            $current_item_quantity = intval($order_details->item_quantity) - intval($item_quantity);

            DB::table('order_details')->insert([
                'order_id' => $order_details->order_id,
                'menu_item_id' => $order_details->menu_item_id,
                'menu_item_name' => $order_details->menu_item_name,
                'item_price' => $order_details->item_price,
                'item_discount' => $order_details->item_discount,
                'item_quantity' => $current_item_quantity,
                'item_vat' => $order_details->item_vat,
                'item_status' => $order_details->item_status,
                'sync' => $order_details->sync,
                'created_at' => $order_details->created_at,
                'updated_at' => $date,

            ]);

        }

        DB::table('order_details')->insert([
            'order_id' => $order_details->order_id,
            'menu_item_id' => $order_details->menu_item_id,
            'menu_item_name' => $order_details->menu_item_name . ' (Complimentary)',
            'item_price' => 0,
            'item_discount' => 0,
            'item_quantity' => $item_quantity,
            'item_vat' => $order_details->item_vat,
            'item_status' => $order_details->item_status,
            'sync' => $order_details->sync,
            'created_at' => $order_details->created_at,
            'updated_at' => $date,
        ]);


        DB::table('orders')
            ->where('order_id', $order_id)
            ->update([
                'discount_reference' => $orders->discount_reference . "<br>" . $notes,
            ]);


        reCalculateBill($order_id);

        return json_encode('success');
    }



}
