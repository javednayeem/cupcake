<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SyncController extends Controller {

    public function index() {

    }


    public function create() {

    }


    public function store(Request $request) {

    }


    public function show($id) {

    }


    public function edit($id) {

    }


    public function update(Request $request, $id) {

    }


    public function destroy($id) {

    }


    public function syncOrders(Request $request) {

        $data = $request->input('params');

        for ($i=0; $i<count($data["orders"]); $i++) {

            $order_id = $data["orders"][$i][0];
            $restaurant_id = $data["orders"][$i][1];
            $table_id = $data["orders"][$i][2];
            $waiter_id = $data["orders"][$i][3];
            $customer_id = $data["orders"][$i][4];
            $order_type = $data["orders"][$i][5];
            $order_status = $data["orders"][$i][6];
            $bill = $data["orders"][$i][7];
            $vat = $data["orders"][$i][8];
            $service_charge = $data["orders"][$i][9];
            $discount = $data["orders"][$i][10];
            $discount_reference = $data["orders"][$i][11];
            $payment_method = $data["orders"][$i][12];
            $card_name = $data["orders"][$i][13];
            $total_bill = $data["orders"][$i][14];
            $given_amount = $data["orders"][$i][15];
            $sync = $data["orders"][$i][16];
            $created_at = $data["orders"][$i][17];
            $updated_at = $data["orders"][$i][18];


            DB::table('orders')->insert([
                'order_id' => $order_id,
                'restaurant_id' => $restaurant_id,
                'table_id' => $table_id,
                'waiter_id' => $waiter_id,
                'customer_id' => $customer_id,
                'order_type' => $order_type,
                'order_status' => $order_status,
                'bill' => $bill,
                'vat' => $vat,
                'service_charge' => $service_charge,
                'discount' => $discount,
                'discount_reference' => $discount_reference,
                'payment_method' => $payment_method,
                'card_name' => $card_name,
                'total_bill' => $total_bill,
                'given_amount' => $given_amount,
                'sync' => $sync,
                'created_at' => $created_at,
                'updated_at' => $updated_at
            ]);


        }

        echo json_encode('success v2');


    }
}
