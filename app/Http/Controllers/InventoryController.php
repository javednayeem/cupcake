<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Illuminate\Support\Facades\Validator;

class InventoryController extends Controller {


    public function __construct() {
        $this->middleware('auth');
        $this->middleware('inventory');
    }


    public function supplier() {
        $restaurant_id = Auth::user()->restaurant_id;

        $supplier_data = DB::table('inv_supplier')
            ->where('restaurant_id', $restaurant_id)
            ->paginate(15);

        return view ('cupcake.inventory.supplier', [
            'supplier_data' => $supplier_data,
        ]);
    }


    public function addSupplier(Request $request) {
        $data = $request->input('params');
        $restaurant_id = Auth::user()->restaurant_id;

        $supplier_id = DB::table('inv_supplier')->insertGetId([
            'company_name' => $data['company_name'],
            'supplier_name' => $data['supplier_name'],
            'address' => $data['address'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'act_code' => $data['act_code'],
            'restaurant_id' => $restaurant_id,
        ]);

        return json_encode($supplier_id);
    }


    public function paymentSupplier(Request $request) {

        $restaurant_id = Auth::user()->restaurant_id;
        $created_by = Auth::user()->id;
        $data = $request->input('params');

        $supplier_id = $data['supplier_id'];
        $supplier_name = $data['supplier_name'];
        $debit_from = $data['debit_from'];
        $amount = $data['amount'];

        $purpose = 'Payment To ' . $supplier_name;

        DB::table('daily_expense')->insert([
            'restaurant_id' => $restaurant_id,
            'purpose' => $purpose,
            'debit_from' => $debit_from,
            'amount' => $amount,
            'created_by' => $created_by,
        ]);

        if ($debit_from == 'loan') {

            DB::table('restaurants')
                ->where('restaurants_id', $restaurant_id)
                ->decrement('loan', $amount);

        }

        DB::table('inv_supplier')
            ->where('supplier_id', $supplier_id)
            ->decrement('accounts_payable', $amount);

        return json_encode('success');
    }


    public function productCategory() {

        $restaurant_id = Auth::user()->restaurant_id;

        $product_category = DB::table('inv_product_category')->where('restaurant_id', $restaurant_id)->paginate(10);

        return view ('cupcake.inventory.product-category', [
            'product_category' => $product_category,
        ]);
    }


    public function addCategory(Request $request) {
        $data = $request->input('params');
        $restaurant_id = Auth::user()->restaurant_id;

        $category_id = DB::table('inv_product_category')->insertGetId([
            'category_name' => $data['category_name'],
            'restaurant_id' => $restaurant_id,
        ]);

        return json_encode($category_id);
    }


    public function editCategory(Request $request) {

        $data = $request->input('params');

        DB::table('inv_product_category')
            ->where('category_id', $data['category_id'])
            ->update([
                'category_name' => $data['category_name']

            ]);

        return json_encode('success');
    }


    public function products() {

        $restaurant_id = Auth::user()->restaurant_id;

        $products = DB::table('inv_product')
            ->where('inv_product.restaurant_id', $restaurant_id)
            ->select('inv_product.*', 'inv_product_category.category_name', 'inv_units.unit_name')
            ->join('inv_product_category', 'inv_product_category.category_id', 'inv_product.category_id')
            ->join('inv_units', 'inv_units.unit_id', 'inv_product.unit_id')
            ->get();


        $product_category = DB::table('inv_product_category')
            ->where('restaurant_id', $restaurant_id)
            ->get();

        $units = DB::table('inv_units')
            ->where('restaurant_id', $restaurant_id)
            ->get();

        return view ('cupcake.inventory.product', [
            'products' => $products,
            'product_category' => $product_category,
            'units' => $units,
        ]);
    }


    public function addProduct(Request $request) {

        $data = $request->input('params');
        $restaurant_id = Auth::user()->restaurant_id;

        $product_id = DB::table('inv_product')->insertGetId([
            'product_code' => $data['product_code'],
            'product_name' => $data['product_name'],
            'fixed_price' => $data['fixed_price'],
            'low_inv_alert' => $data['low_inv_alert'],
            'unit_id' => $data['unit_id'],
            'category_id' => $data['category_id'],
            'restaurant_id' => $restaurant_id,
        ]);

        return json_encode($product_id);
    }


    public function purchase() {

        $restaurant_id = Auth::user()->restaurant_id;

        $product_category = DB::table('inv_product_category')
            ->where('restaurant_id', $restaurant_id)
            ->orderBy('inv_product_category.category_name')
            ->get();

        $product_data = DB::table('inv_product')
            ->where('restaurant_id', $restaurant_id)
            ->orderBy('inv_product.product_name')
            ->get();

        $units = DB::table('inv_units')->where('restaurant_id', $restaurant_id)->get();
        $supplier_data = DB::table('inv_supplier')->where('restaurant_id', $restaurant_id)->get();


        return view ('cupcake.inventory.new-purchase', [
            'product_category' => $product_category,
            'product_data' => $product_data,
            'units' => $units,
            'supplier_data' => $supplier_data,
        ]);

    }


    public function addUnit(Request $request) {
        $data = $request->input('params');
        $restaurant_id = Auth::user()->restaurant_id;

        $unit_id = DB::table('inv_units')->insertGetId([
            'unit_name' => $data['unit_name'],
            'restaurant_id' => $restaurant_id,
        ]);

        return json_encode($unit_id);
    }


    public function editUnit(Request $request) {

        $data = $request->input('params');

        DB::table('inv_units')
            ->where('unit_id', $data['unit_id'])
            ->update([
                'unit_name' => $data['unit_name']

            ]);

        return json_encode('success');
    }


    public function deleteUnit(Request $request) {

        $data = $request->input('params');

        DB::table('inv_units')->where('unit_id', '=', $data['unit_id'])->delete();

        return json_encode("success");
    }


    public function newPurchase(Request $request) {

        $data = $request->input('params');
        $restaurant_id = Auth::user()->restaurant_id;
        $creator_id = Auth::user()->id;

        $main_store_id = getMainStoreId($restaurant_id);

        $purchase_id = DB::table('inv_purchase_invoice')->insertGetId([
            'invoice_no' => $data['invoice_no'],
            'supplier_id' => $data['supplier_id'],
            'purchase_date' => $data['purchase_date'],
            'delivery_date' => $data['delivery_date'],
            'sub_total' => $data['sub_total'],
            'discount' => $data['discount'],
            'shipping' => $data['shipping'],
            'grand_total' => $data['grand_total'],
            'created_by' => $creator_id,
            'updated_by' => $creator_id,
            'store_id' => $main_store_id,
            'restaurant_id' => $restaurant_id
        ]);

        for ($i=0; $i<count($data["invoice_orders"]); $i++) {

            $product_id = $data["invoice_orders"][$i][2];
            $quantity = floatval($data["invoice_orders"][$i][5]);
            $unit_price = $data["invoice_orders"][$i][4];
            $expire_date = $data["invoice_orders"][$i][7];

            if ($quantity > 0.0) {

                DB::table('inv_product_purchase_in')->insert([
                    'purchase_invoice_id' => $purchase_id,
                    'product_id' => $data["invoice_orders"][$i][2],
                    'quantity' => $data["invoice_orders"][$i][5],
                    'unit_price' => $unit_price,
                    'total_purchase_price' => $data["invoice_orders"][$i][6],
                    'expire_date' => $expire_date,
                    'created_by' => $creator_id,
                    'updated_by' => $creator_id
                ]);

                updateProductPrice($product_id, $unit_price);
                updateStoreProduct($product_id, $main_store_id, $quantity);
            }
        }


        if ($data['pay_type'] == 'payable') {

            DB::table('inv_supplier')
                ->where('supplier_id', $data['supplier_id'])
                ->increment('accounts_payable', $data['grand_total']);

        }

        else {

            if ($data['pay_type'] == 'cash_sale') $debit_from = 'sales';
            else if ($data['pay_type'] == 'petty_cash') $debit_from = 'petty_cash';
            else if ($data['pay_type'] == 'loan') $debit_from = 'loan';

            $purpose = 'Product purchased for inventory, with Invoice No. ' . $data['invoice_no'];

            DB::table('daily_expense')->insert([
                'restaurant_id' => $restaurant_id,
                'purpose' => $purpose,
                'debit_from' => $debit_from,
                'amount' => $data['grand_total'],
                'created_by' => $creator_id,
            ]);

            if ($debit_from == 'loan') {

                DB::table('restaurants')
                    ->where('restaurants_id', $restaurant_id)
                    ->decrement('loan', $data['grand_total']);

            }


        }

        return json_encode($purchase_id);
    }


    public function purchaseHistory() {

        $restaurant_id = Auth::user()->restaurant_id;

        /*

        $purchase_data = DB::table('inv_purchase_invoice')
            ->select('inv_purchase_invoice.*'
                , DB::raw("(SELECT supplier_name FROM inv_supplier WHERE supplier_id=inv_purchase_invoice.supplier_id) as supplier_name")
                , DB::raw("(SELECT name FROM users WHERE id=inv_purchase_invoice.created_by) as username")
            )
            ->where('restaurant_id', $restaurant_id)
            ->get();

        */

        $categories = DB::table('inv_product_category')
            ->select('inv_product_category.*',
                DB::raw("(SELECT SUM(product_id) FROM inv_product WHERE category_id=inv_product_category.category_id) as product_count"))
            ->where('restaurant_id', $restaurant_id)
            ->get();


        $inv_products = DB::table('inv_product')
            ->where('restaurant_id', $restaurant_id)
            ->get();

        $purchase_data = DB::table('inv_product_purchase_in')
            ->select('inv_product_purchase_in.*', 'inv_purchase_invoice.invoice_no', 'inv_purchase_invoice.purchase_date', 'inv_product.product_name', 'inv_product_category.category_name', 'inv_units.unit_name')
            ->join('inv_purchase_invoice', 'inv_purchase_invoice.invoice_id', 'inv_product_purchase_in.purchase_invoice_id')
            ->join('inv_product', 'inv_product.product_id', 'inv_product_purchase_in.product_id')
            ->join('inv_product_category', 'inv_product_category.category_id', 'inv_product.category_id')
            ->join('inv_units', 'inv_units.unit_id', 'inv_product.unit_id')
            ->where('inv_purchase_invoice.restaurant_id', $restaurant_id)
            ->get();


        $restaurant_data = DB::table('restaurants')
            ->where('restaurants_id', $restaurant_id)
            ->first();



        return view ('cupcake.inventory.purchase-history', [
            'categories' => $categories,
            'inv_products' => $inv_products,
            'purchase_data' => $purchase_data,
            'restaurant_data' => $restaurant_data,
        ]);

    }


    public function getPurchaseDetails(Request $request) {
        $data = $request->input('params');

        $products = DB::table('inv_product_purchase_in')

            ->select('inv_product_purchase_in.*'
                , DB::raw("(SELECT product_name FROM inv_product WHERE inv_product.product_id=inv_product_purchase_in.product_id) as product_name")

            )
            ->where('purchase_invoice_id', $data['invoice_id'])
            ->get();

        //return $products;
        echo json_encode($products);
    }


    public function productUnitLayout() {

        $restaurant_id = Auth::user()->restaurant_id;

        $units = DB::table('inv_units')
            ->where('restaurant_id', $restaurant_id)
            ->get();


        $unit_conversion = DB::table('unit_conversion')
            ->select('unit_conversion.*'
                , DB::raw("(SELECT unit_name FROM inv_units WHERE unit_id=unit_conversion.from_unit_id) as from_unit_name")
                , DB::raw("(SELECT unit_name FROM inv_units WHERE unit_id=unit_conversion.to_unit_id) as to_unit_name")
            )
            ->get();



        return view ('cupcake.inventory.product-unit', [
            'units' => $units,
            'unit_conversion' => $unit_conversion,
        ]);
    }


    public function store() {
        $restaurant_id = Auth::user()->restaurant_id;

        $stores = DB::table('inv_stores')
            ->where('restaurant_id', $restaurant_id)
            ->paginate(15);

        return view ('cupcake.inventory.store', [
            'stores' => $stores,
        ]);
    }


    public function addStore(Request $request) {

        $data = $request->input('params');
        $restaurant_id = Auth::user()->restaurant_id;

        $store_id = DB::table('inv_stores')->insertGetId([
            'store_name' => $data['store_name'],
            'store_address' => $data['store_address'],
            'store_phone' => $data['store_phone'],
            'store_email' => $data['store_email'],
            'restaurant_id' => $restaurant_id,
        ]);

        return $store_id;
    }


    public function currentStock() {

        $restaurant_id = Auth::user()->restaurant_id;

        $main_store_id = getMainStoreId($restaurant_id);


        $categories = DB::table('inv_product_category')
            ->select('inv_product_category.*',
                DB::raw("(SELECT SUM(product_id) FROM inv_product WHERE category_id=inv_product_category.category_id) as product_count"))
            ->where('restaurant_id', $restaurant_id)
            ->get();


        $inv_products = DB::table('inv_product')
            ->where('restaurant_id', $restaurant_id)
            ->get();


        $products = DB::table('inv_product_store')
            ->select('inv_product_store.quantity', 'inv_product.product_code','inv_product.product_name', 'inv_product.avg_price', 'inv_product_category.category_name', 'inv_units.unit_name',
                DB::raw("(SELECT SUM(quantity) FROM recipe_consumption WHERE product_id=inv_product_store.product_id) as consumption_quantity"),
                DB::raw("(SELECT SUM(quantity) FROM inv_damage_products WHERE product_id=inv_product_store.product_id) as damage_quantity")
            )
            ->where('store_id', $main_store_id)
            ->join('inv_product', 'inv_product.product_id', 'inv_product_store.product_id')
            ->join('inv_product_category', 'inv_product_category.category_id', 'inv_product.category_id')
            ->join('inv_units', 'inv_units.unit_id', 'inv_product.unit_id')
            ->get();

        $restaurant_data = DB::table('restaurants')
            ->where('restaurants_id', $restaurant_id)
            ->first();

        return view ('cupcake.inventory.current-stock', [
            'categories' => $categories,
            'inv_products' => $inv_products,
            'products' => $products,
            'restaurant_data' => $restaurant_data,
        ]);
    }


    public function transferStock() {

        $restaurant_id = Auth::user()->restaurant_id;


        $product_category = DB::table('inv_product_category')->where('restaurant_id', $restaurant_id)->get();

        $product_data = DB::table('inv_product')
            ->where('restaurant_id', $restaurant_id)
            ->select('inv_product.*', 'inv_product_store.quantity')
            ->join('inv_product_store', 'inv_product_store.product_id', '=', 'inv_product.product_id')
            ->get();

        $units = DB::table('inv_units')->where('restaurant_id', $restaurant_id)->get();
        //$supplier_data = DB::table('inv_supplier')->where('restaurant_id', $restaurant_id)->get();


        $stores = DB::table('inv_stores')
            ->where('restaurant_id', $restaurant_id)
            ->get();



        return view ('cupcake.inventory.transfer-stock', [
            'stores' => $stores,
            'product_category' => $product_category,
            'product_data' => $product_data,
            'units' => $units,
        ]);
    }


    public function submitTransferStock(Request $request) {

        $data = $request->input('params');
        $restaurant_id = Auth::user()->restaurant_id;
        $creator_id = Auth::user()->id;

        $main_store_id = $data['from_store_id'];
        $transfer_date = date("Y-m-d", strtotime($data['transfer_date']));


        $transfer_id = DB::table('inv_product_transfer')->insertGetId([
            'from_store_id' => $data['from_store_id'],
            'to_store_id' => $data['to_store_id'],
            'transfer_date' => $transfer_date,
            'created_by' => $creator_id,
            'updated_by' => $creator_id,
            'restaurant_id' => $restaurant_id
        ]);

        for ($i=0; $i<count($data["transfer_product"]); $i++) {

            $product_id = $data["transfer_product"][$i][1];
            $quantity = $data["transfer_product"][$i][3];

            if ($quantity > 0) {

                DB::table('inv_product_store_in')->insert([
                    'transfer_id' => $transfer_id,
                    'product_id' => $product_id,
                    'quantity' => $quantity,
                    'created_by' => $creator_id,
                    'updated_by' => $creator_id
                ]);

                $quantity *= -1;
                updateStoreProduct($product_id, $main_store_id, $quantity);

            }

        }

        return json_encode($transfer_id);
    }


    public function transferHistory() {

        $restaurant_id = Auth::user()->restaurant_id;

        $restaurant_data = DB::table('restaurants')
            ->where('restaurants_id', $restaurant_id)
            ->first();

        $transfers = DB::table('inv_product_transfer')
            ->select('inv_product_transfer.*', 'users.name',
                DB::raw("(SELECT store_name FROM `inv_stores` WHERE store_id= inv_product_transfer.from_store_id) as from_store_name"),
                DB::raw("(SELECT store_name FROM `inv_stores` WHERE store_id= inv_product_transfer.to_store_id) as to_store_name"),
                DB::raw("(SELECT COUNT(id)FROM inv_product_store_in WHERE transfer_id=inv_product_transfer.transfer_id) as items")
            )
            ->where('inv_product_transfer.restaurant_id', $restaurant_id)
            ->join('users', 'users.id', 'inv_product_transfer.created_by')
            ->orderBy('transfer_id', 'desc')
            ->get();

        return view ('cupcake.inventory.transfer-history', [
            'restaurant_data' => $restaurant_data,
            'transfers' => $transfers,
        ]);
    }


    public function getTransferDetails(Request $request) {
        $data = $request->input('params');

        $items = DB::table('inv_product_store_in')
            ->select('inv_product_store_in.quantity', 'inv_product.product_code', 'inv_product.product_name', 'inv_units.unit_name')
            ->where('inv_product_store_in.transfer_id', $data['transfer_id'])
            ->join('inv_product', 'inv_product.product_id', '=', 'inv_product_store_in.product_id')
            ->join('inv_units', 'inv_units.unit_id', '=', 'inv_product.unit_id')
            ->get();

        return json_encode($items);
    }


    public function receiveStock() {

        $restaurant_id = Auth::user()->restaurant_id;


        $product_category = DB::table('inv_product_category')->where('restaurant_id', $restaurant_id)->get();

        $product_data = DB::table('inv_product')
            ->where('restaurant_id', $restaurant_id)
            ->select('inv_product.*', 'inv_product_store.quantity')
            ->join('inv_product_store', 'inv_product_store.product_id', '=', 'inv_product.product_id')
            ->get();

        $units = DB::table('inv_units')->where('restaurant_id', $restaurant_id)->get();
        //$supplier_data = DB::table('inv_supplier')->where('restaurant_id', $restaurant_id)->get();


        $stores = DB::table('inv_stores')
            ->where('restaurant_id', $restaurant_id)
            ->get();



        return view ('cupcake.inventory.receive-stock', [
            'stores' => $stores,
            'product_category' => $product_category,
            'product_data' => $product_data,
            'units' => $units,
        ]);
    }


    public function submitReceiveStock(Request $request) {

        $data = $request->input('params');
        $restaurant_id = Auth::user()->restaurant_id;
        $creator_id = Auth::user()->id;

        $main_store_id = $data['to_store_id'];
        $transfer_date = date("Y-m-d", strtotime($data['transfer_date']));


        $transfer_id = DB::table('inv_product_transfer')->insertGetId([
            'from_store_id' => $data['from_store_id'],
            'to_store_id' => $data['to_store_id'],
            'transfer_date' => $transfer_date,
            'created_by' => $creator_id,
            'updated_by' => $creator_id,
            'restaurant_id' => $restaurant_id
        ]);

        for ($i=0; $i<count($data["transfer_product"]); $i++) {

            $product_id = $data["transfer_product"][$i][1];
            $quantity = $data["transfer_product"][$i][3];

            if ($quantity > 0) {

                DB::table('inv_product_store_in')->insert([
                    'transfer_id' => $transfer_id,
                    'product_id' => $product_id,
                    'quantity' => $quantity,
                    'created_by' => $creator_id,
                    'updated_by' => $creator_id
                ]);

                updateStoreProduct($product_id, $main_store_id, $quantity);

            }


        }

        return json_encode($transfer_id);
    }


    public function editSupplier(Request $request) {

        $data = $request->input('params');

        DB::table('inv_supplier')
            ->where('supplier_id', $data['supplier_id'])
            ->update([
                'company_name' => $data['company_name'],
                'supplier_name' => $data['supplier_name'],
                'address' => $data['address'],
                'email' => $data['email'],
                'phone' => $data['phone']

            ]);

        return json_encode('success');
    }


    public function deleteSupplier(Request $request) {

        $data = $request->input('params');

        DB::table('inv_supplier')->where('supplier_id', '=', $data['supplier_id'])->delete();

        return json_encode("success");
    }


    public function editStore(Request $request) {

        $data = $request->input('params');

        DB::table('inv_stores')
            ->where('store_id', $data['store_id'])
            ->update([
                'store_name' => $data['store_name'],
                'store_address' => $data['store_address'],
                'store_phone' => $data['store_phone'],
                'store_email' => $data['store_email']

            ]);

        return json_encode('success');
    }


    public function deleteStore(Request $request) {

        $data = $request->input('params');

        DB::table('inv_stores')->where('store_id', '=', $data['store_id'])->delete();

        return json_encode("success");
    }


    public function deleteCategory(Request $request) {

        $data = $request->input('params');

        DB::table('inv_product_category')->where('category_id', '=', $data['category_id'])->delete();

        return json_encode("success");
    }


    public function editProduct(Request $request) {

        $data = $request->input('params');

        DB::table('inv_product')
            ->where('product_id', $data['product_id'])
            ->update([
                'product_name' => $data['product_name'],
                'unit_id' => $data['unit_id'],
                'category_id' => $data['category_id'],
                'product_code' => $data['product_code'],
                'fixed_price' => $data['fixed_price'],
                'low_inv_alert' => $data['low_inv_alert']
            ]);

        return json_encode('success');
    }


    public function deleteProduct(Request $request) {

        $data = $request->input('params');

        DB::table('inv_product')->where('product_id', $data['product_id'])->delete();

        return json_encode("success");
    }


    public function damageEntry() {

        $restaurant_id = Auth::user()->restaurant_id;

        $product_category = DB::table('inv_product_category')->where('restaurant_id', $restaurant_id)->get();

        $product_data = DB::table('inv_product')
            ->where('restaurant_id', $restaurant_id)
            ->select('inv_product.*', 'inv_product_store.quantity')
            ->join('inv_product_store', 'inv_product_store.product_id', 'inv_product.product_id')
            ->get();

        $units = DB::table('inv_units')
            ->where('restaurant_id', $restaurant_id)
            ->get();

        $stores = DB::table('inv_stores')
            ->where('restaurant_id', $restaurant_id)
            ->get();


        return view ('cupcake.inventory.damage-entry', [
            'stores' => $stores,
            'product_category' => $product_category,
            'product_data' => $product_data,
            'units' => $units,
        ]);
    }


    public function submitDamageEntry(Request $request) {

        $data = $request->input('params');
        $restaurant_id = Auth::user()->restaurant_id;
        $creator_id = Auth::user()->id;
        $damage_cost = 0;

        $damage_id = DB::table('inv_damage_entry')->insertGetId([
            'store_id' => $data['store_id'],
            'created_by' => $creator_id,
            'restaurant_id' => $restaurant_id
        ]);


        for ($i=0; $i<count($data["damage_products"]); $i++) {

            $product_id = $data["damage_products"][$i][1];
            $quantity = $data["damage_products"][$i][3];
            $price = $data["damage_products"][$i][4];

            if ($quantity > 0) {

                $damage_cost += ($price * $quantity);

                DB::table('inv_damage_products')->insert([
                    'damage_id' => $damage_id,
                    'product_id' => $product_id,
                    'quantity' => $quantity,
                    'unit_price' => $price
                ]);

            }
        }

        DB::table('inv_damage_entry')
            ->where('damage_id', $damage_id)
            ->update([
                'damage_cost' => $damage_cost
            ]);


        return json_encode($damage_id);
    }


    public function damageHistory() {

        $restaurant_id = Auth::user()->restaurant_id;

        $categories = DB::table('inv_product_category')
            ->select('inv_product_category.*',
                DB::raw("(SELECT SUM(product_id) FROM inv_product WHERE category_id=inv_product_category.category_id) as product_count"))
            ->where('restaurant_id', $restaurant_id)
            ->get();

        $inv_products = DB::table('inv_product')
            ->where('restaurant_id', $restaurant_id)
            ->get();

        $restaurant_data = DB::table('restaurants')
            ->where('restaurants_id', $restaurant_id)
            ->first();

        $damages = DB::table('inv_damage_products')
            ->select('inv_damage_products.*', 'inv_product.product_name', 'inv_product_category.category_name', 'inv_units.unit_name')
            ->join('inv_damage_entry', 'inv_damage_entry.damage_id', 'inv_damage_products.damage_id')
            ->join('inv_product', 'inv_product.product_id', 'inv_damage_products.product_id')
            ->join('inv_product_category', 'inv_product_category.category_id', 'inv_product.category_id')
            ->join('inv_units', 'inv_units.unit_id', 'inv_product.unit_id')
            ->where('inv_damage_entry.restaurant_id', $restaurant_id)
            ->get();

//        $damages = DB::table('inv_damage_entry')
//            ->select('inv_damage_entry.*', 'inv_stores.store_name', 'users.name',
//                DB::raw("(SELECT COUNT(id) FROM inv_damage_products WHERE damage_id=inv_damage_entry.damage_id) as items"))
//            ->where('inv_damage_entry.restaurant_id', $restaurant_id)
//            ->join('inv_stores', 'inv_stores.store_id', 'inv_damage_entry.store_id')
//            ->join('users', 'users.id', 'inv_damage_entry.created_by')
//            ->orderBy('inv_damage_entry.damage_id', 'desc')
//            ->get();

        return view ('cupcake.inventory.damage-history', [
            'restaurant_data' => $restaurant_data,
            'damages' => $damages,
            'categories' => $categories,
            'inv_products' => $inv_products,
        ]);
    }


    public function getDamageDetails(Request $request) {

        $data = $request->input('params');
        $damage_id = $data['damage_id'];

        $items = DB::table('inv_damage_products')
            ->select('inv_damage_products.*', 'inv_product.product_name', 'inv_units.unit_name')
            ->where('inv_damage_products.damage_id', $damage_id)
            ->join('inv_product', 'inv_product.product_id', 'inv_damage_products.product_id')
            ->join('inv_units', 'inv_units.unit_id', 'inv_product.unit_id')
            ->get();

        return json_encode($items);
    }


    public function addUnitConversion(Request $request) {

        $data = $request->input('params');

        $conversion_id = DB::table('unit_conversion')->insertGetId([
            'from_unit_id' => $data['from_unit_id'],
            'to_unit_id' => $data['to_unit_id'],
            'conversion_rate' => $data['conversion_rate']
        ]);

        return json_encode($conversion_id);
    }

}
