<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;

class ApiController extends Controller {

    public function test () {

        $users = DB::table('users')->get();

        return response()->json($users, 200);

    }


    public function createBackUp(Request $request) {

        //$data = $request->input('customers');

        $customers = json_decode(stripslashes($_POST['data']));


        for ($i=0; $i<count($customers); $i++) {

            $customer_id = $customers[$i][0];
            $restaurant_id = $customers[$i][1];
            $customer_name = $customers[$i][2];
            $customer_email = $customers[$i][3];
            $customer_address = $customers[$i][4];
            $customer_phone = $customers[$i][5];
            $card_no = $customers[$i][6];
            $discount = $customers[$i][7];
            $discount_percentage = $customers[$i][8];
            $sync = $customers[$i][9];
            $created_at = $customers[$i][10];
            $updated_at = $customers[$i][11];

            DB::table('schedule_scripts')->insert([
                'script_name' => "create:backup-customers ===  $customer_name"
            ]);

//            DB::table('customers')->insert([
//                'customer_id' => $customer_id,
//                'restaurant_id' => $restaurant_id,
//                'customer_name' => $customer_name,
//                'customer_email' => $customer_email,
//                'customer_address' => $customer_address,
//                'customer_phone' => $customer_phone,
//                'card_no' => $card_no,
//                'discount' => $discount,
//                'discount_percentage' => $discount_percentage,
//                'sync' => $sync,
//                'created_at' => $created_at,
//                'updated_at' => $updated_at
//            ]);

        }

//        DB::table('schedule_scripts')->insert([
//            'script_name' => 'create:backup-customers'
//        ]);

        //return json_encode('success');
        return json_encode(count($customers));
    }

}
