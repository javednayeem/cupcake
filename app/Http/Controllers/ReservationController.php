<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

class ReservationController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }


    public function index() {

        $restaurant_id = Auth::user()->restaurant_id;

        $reservations = DB::table('reservations')
            ->select('reservations.*', 'customers.customer_name', 'users.name')
            ->where('reservations.restaurant_id', $restaurant_id)
            ->join('customers', 'customers.customer_id', 'reservations.customer_id')
            ->join('users', 'users.id', 'reservations.created_by')
            ->get();

        return view('cupcake.reservations.index', [
            'reservations' => $reservations
        ]);
    }


    public function newReservationLayout() {

        $restaurant_id = Auth::user()->restaurant_id;

        $customers = DB::table('customers')
            ->where('customers.restaurant_id', $restaurant_id)
            ->get();


        $tables = DB::table('tables')
            ->where('tables.restaurant_id', $restaurant_id)
            ->get();


        $menu_items = DB::table('menu_items')
            ->select('menu_items.*', 'menus.menu_name')
            ->where('menus.restaurants_id', $restaurant_id)
            ->join('menus', 'menus.menu_id', 'menu_items.menu_id')
            ->get();


        return view('cupcake.reservations.new-reservation', [
            'customers' => $customers,
            'tables' => $tables,
            'menu_items' => $menu_items,
        ]);
    }


    public function createReservation(Request $request) {

        $restaurant_id = Auth::user()->restaurant_id;
        $created_by = Auth::user()->id;
        $data = $request->input('params');

        $reservation_date = $data['reservation_date'];
        $reservation_date = date_create($reservation_date);
        $reservation_date = date_format($reservation_date,"Y-m-d");

        $reservation_id = DB::table('reservations')->insertGetId([
            'restaurant_id' => $restaurant_id,
            'customer_id' => $data['customer_id'],
            'reservation_date' => $reservation_date,
            'start_time' => $data['start_time'],
            'end_time' => $data['end_time'],
            'description' => $data['description'],
            'food_menu' => $data['food_menu'],
            'subtotal' => $data['subtotal'],
            'advance' => $data['advance'],
            'guest' => $data['guest'],
            'tables' => $data['tables'],
            'created_by' => $created_by
        ]);

        return json_encode($reservation_id);
    }


    public function checkAvailablity(Request $request) {

        $restaurant_id = Auth::user()->restaurant_id;
        $data = $request->input('params');

        $reservation_date = $data['reservation_date'];
        $reservation_date = date_create($reservation_date);
        $reservation_date = date_format($reservation_date,"Y-m-d");

        $reservations = DB::table('reservations')
            ->select('reservations.*', 'customers.customer_name')
            ->where('reservations.restaurant_id', $restaurant_id)
            ->where('reservations.reservation_date', $reservation_date)
            ->join('customers', 'customers.customer_id', 'reservations.customer_id')
            ->get();

        echo json_encode($reservations);
    }


    public function deleteReservation(Request $request) {

        $data = $request->input('params');

        DB::table('reservations')->where('reservation_id', $data['reservation_id'])->delete();

        return json_encode("success");
    }


    public function receipt($reservation_id) {

        $restaurant_id = Auth::user()->restaurant_id;

        $reservation = DB::table('reservations')
            ->select('reservations.*', 'customers.*', 'users.name')
            ->where('reservations.reservation_id', $reservation_id)
            ->join('customers', 'customers.customer_id', 'reservations.customer_id')
            ->join('users', 'users.id', 'reservations.created_by')
            ->first();


        $restaurant_data = DB::table('restaurants')
            ->select('restaurants.*',
                DB::raw("(SELECT COUNT(workperiod_id)FROM work_period WHERE restaurant_id=$restaurant_id AND status=1) as work_period_status"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='guest_bill') as guest_bill"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='waiter_order_void') as waiter_order_void"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='kitchen_type') as kitchen_type"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='receipt_footer') as receipt_footer"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='order_receipt_debug') as order_receipt_debug"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='menu_font_size') as menu_font_size"),
                DB::raw("(SELECT setting_value FROM app_settings WHERE setting_key='receipt_company') as receipt_company"),
                DB::raw("(SELECT setting_value FROM app_settings WHERE setting_key='receipt_phone') as receipt_phone")
            )
            ->where('restaurants_id', $restaurant_id)
            ->first();

        return view('cupcake.reservations.receipt', [
            'reservation' => $reservation,
            'restaurant_data' => $restaurant_data,
        ]);
    }

}
