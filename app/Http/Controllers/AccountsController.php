<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Illuminate\Support\Facades\Validator;

class AccountsController extends Controller {

    public function __construct() {
        date_default_timezone_set("Asia/Dhaka");
        $this->middleware('auth');
    }


    public function cashOpening() {

        $date = date('Y-m-d');

        $restaurant_id = Auth::user()->restaurant_id;
        $created_by = Auth::user()->id;

        $cash_opening = DB::table('cash_opening')
            ->where('restaurant_id', $restaurant_id)
            ->where('opening_date', $date)
            ->first();

        if ($cash_opening == NULL) {

            DB::table('cash_opening')->insert([
                'restaurant_id' => $restaurant_id,
                'opening_date' => $date,
                'created_by' => $created_by,
            ]);

            $cash_opening = DB::table('cash_opening')
                ->where('restaurant_id', $restaurant_id)
                ->where('opening_date', $date)
                ->first();

        }

        return view('cupcake.accounts.cash-opening', [
            'cash_opening' => $cash_opening
        ]);
    }


    public function addCashOpening(Request $request) {

        $restaurant_id = Auth::user()->restaurant_id;
        $date = date('Y-m-d');
        $created_by = Auth::user()->id;

        $data = $request->input('params');

        $note_1000 = $data['note_1000']!=''?intval($data['note_1000']):0;
        $note_500 = $data['note_500']!=''?intval($data['note_500']):0;
        $note_100 = $data['note_100']!=''?intval($data['note_100']):0;
        $note_50 = $data['note_50']!=''?intval($data['note_50']):0;
        $note_20 = $data['note_20']!=''?intval($data['note_20']):0;
        $note_10 = $data['note_10']!=''?intval($data['note_10']):0;
        $note_5 = $data['note_5']!=''?intval($data['note_5']):0;
        $note_2 = $data['note_2']!=''?intval($data['note_2']):0;
        $note_1 = $data['note_1']!=''?intval($data['note_1']):0;

        $total_amount = 0;

        $total_amount += ($note_1000*1000) + ($note_500*500) + ($note_100*100) + ($note_50*50) + ($note_20*20) + ($note_10*10)
            + ($note_5*5) + ($note_2*2) + ($note_1*1);

        DB::table('cash_opening')
            ->where('opening_date', $date)
            ->where('restaurant_id', $restaurant_id)
            ->update([
                'note_1000' => $data['note_1000'],
                'note_500' => $data['note_500'],
                'note_100' => $data['note_100'],
                'note_50' => $data['note_50'],
                'note_20' => $data['note_20'],
                'note_10' => $data['note_10'],
                'note_5' => $data['note_5'],
                'note_2' => $data['note_2'],
                'note_1' => $data['note_1'],
                'total_amount' => $total_amount,
                'set' => 1,
                'created_by' => $created_by,
            ]);

        return json_encode('success');
    }


    public function dailyExpense() {

        $restaurant_id = Auth::user()->restaurant_id;

        $date = date('Y-m-d');

        $workperiod = getCurrentWorkPeriod($restaurant_id);
        //$workperiod = 0;

        if ($workperiod == "0") $from = $date . ' 00:00:00';
        else $from = $workperiod . ' 00:00:00' ;

        //$from = $date . ' 00:00:00';
        $to = $date . ' 23:59:59';

        $daily_expense = DB::table('daily_expense')
            ->select('daily_expense.*', 'users.name')
            ->where('daily_expense.restaurant_id', $restaurant_id)
            ->join('users', 'users.id', 'daily_expense.created_by')
            ->whereBetween('daily_expense.created_at', array($from, $to))
            ->get();

        $expenses_details = DB::table('restaurants')
            ->where('restaurants_id', $restaurant_id)
            ->select(
                DB::raw("(SELECT SUM(total_bill)FROM orders WHERE restaurant_id=$restaurant_id AND order_status='paid' AND (created_at BETWEEN '$from' AND '$to')) as today_sale"),
                DB::raw("(SELECT SUM(amount)FROM order_payments WHERE payment_method='cash' AND (order_payments.created_at BETWEEN '$from' AND '$to')) as cash_sale"),
                DB::raw("(SELECT SUM(amount)FROM order_payments WHERE payment_method='card' AND (order_payments.created_at BETWEEN '$from' AND '$to')) as card_sale"),
                DB::raw("(SELECT total_amount FROM cash_opening WHERE restaurant_id=$restaurant_id AND opening_date='$date') as cash_opening_total_amount"),
                DB::raw("(SELECT total_amount FROM cash_closing WHERE restaurant_id=$restaurant_id AND closing_date='$date') as cash_closing_total_amount"),
                DB::raw("(SELECT SUM(amount) FROM daily_expense WHERE restaurant_id=$restaurant_id AND debit_from='sales' AND (created_at BETWEEN '$from' AND '$to')) as sales_expense"),
                DB::raw("(SELECT SUM(amount) FROM daily_expense WHERE restaurant_id=$restaurant_id AND debit_from='petty_cash' AND (created_at BETWEEN '$from' AND '$to')) as petty_cash_expense"),
                DB::raw("(SELECT SUM(amount) FROM daily_expense WHERE restaurant_id=$restaurant_id AND transfer_to='sales' AND (created_at BETWEEN '$from' AND '$to')) as transfer_sales"),
                DB::raw("(SELECT SUM(amount) FROM daily_expense WHERE restaurant_id=$restaurant_id AND transfer_to='petty_cash' AND (created_at BETWEEN '$from' AND '$to')) as transfer_petty_cash"),
                DB::raw("(SELECT loan FROM restaurants WHERE restaurants_id=$restaurant_id) as loan_amount"),
                DB::raw("(SELECT SUM(accounts_payable) FROM loans_from WHERE restaurant_id=$restaurant_id) as total_loan"),
                DB::raw("(SELECT SUM(accounts_payable) FROM inv_supplier WHERE restaurant_id=$restaurant_id) as total_payable")
            )
            ->first();


        $payment_methods = DB::table('payment_methods')
            ->select('payment_methods.method_name',
                DB::raw("(SELECT SUM(amount)FROM order_payments WHERE payment_method=payment_methods.method_name AND restaurant_id=$restaurant_id AND (order_payments.created_at BETWEEN '$from' AND '$to')) as amount"))
            ->get();

        return view('cupcake.accounts.daily-expenses', [
            'daily_expense' => $daily_expense,
            'expenses_details' => $expenses_details,
            'payment_methods' => $payment_methods,
        ]);

        //echo "from : $from<br>";
        //echo "to : $to<br>";
    }


    public function addDailyExpense(Request $request) {

        $restaurant_id = Auth::user()->restaurant_id;
        $created_by = Auth::user()->id;

        $created_at = getRestaurantTimestamp($restaurant_id);

        $data = $request->input('params');

        $expense_id = DB::table('daily_expense')->insertGetId([
            'restaurant_id' => $restaurant_id,
            'purpose' => $data['purpose'],
            'debit_from' => $data['debit_from'],
            'amount' => $data['amount'],
            'created_by' => $created_by,
            'created_at' => $created_at,
        ]);

        if ($data['debit_from'] == 'loan') {

            DB::table('restaurants')
                ->where('restaurants_id', $restaurant_id)
                ->decrement('loan', $data['amount']);

        }



        return json_encode($expense_id);
    }


    public function addCashTransfer(Request $request) {

        $restaurant_id = Auth::user()->restaurant_id;
        $created_by = Auth::user()->id;

        $data = $request->input('params');

        $expense_id = DB::table('daily_expense')->insertGetId([
            'restaurant_id' => $restaurant_id,
            'amount' => $data['amount'],
            'type' => $data['type'],
            'debit_from' => $data['debit_from'],
            'transfer_to' => $data['transfer_to'],
            'created_by' => $created_by,
        ]);

        return json_encode($expense_id);
    }


    public function editDailyExpense(Request $request) {

        $data = $request->input('params');
        $date = date('Y-m-d H:i:s');
        $restaurant_id = Auth::user()->restaurant_id;

        $daily_expense = DB::table('daily_expense')
            ->where('expense_id', $data['expense_id'])
            ->first();

        if ($daily_expense->debit_from == 'loan') {

            DB::table('restaurants')
                ->where('restaurants_id', $restaurant_id)
                ->increment('loan', $daily_expense->amount);

        }


        DB::table('daily_expense')
            ->where('expense_id', $data['expense_id'])
            ->update([
                'purpose' => $data['purpose'],
                'debit_from' => $data['debit_from'],
                'amount' => $data['amount'],
                'updated_at' => $date,
            ]);


        if ($data['debit_from'] == 'loan') {

            DB::table('restaurants')
                ->where('restaurants_id', $restaurant_id)
                ->decrement('loan', $data['amount']);

        }


        return json_encode("success");
    }


    public function deleteDailyExpense(Request $request) {

        $data = $request->input('params');
        $userId = Auth::user()->id;
        $restaurant_id = Auth::user()->restaurant_id;

        $daily_expense = DB::table('daily_expense')
            ->where('expense_id', $data['expense_id'])
            ->first();

        if ($daily_expense->debit_from == 'loan') {

            DB::table('restaurants')
                ->where('restaurants_id', $restaurant_id)
                ->increment('loan', $daily_expense->amount);

        }

        DB::table('daily_expense')->where('expense_id', $data['expense_id'])->delete();


        return json_encode("success");
    }


    public function cashClosing() {

        $date = date('Y-m-d');

        $restaurant_id = Auth::user()->restaurant_id;
        $created_by = Auth::user()->id;

        $cash_closing = DB::table('cash_closing')
            ->where('restaurant_id', $restaurant_id)
            ->where('closing_date', $date)
            ->first();

        if ($cash_closing == NULL) {

            DB::table('cash_closing')->insert([
                'restaurant_id' => $restaurant_id,
                'closing_date' => $date,
                'created_by' => $created_by,
            ]);

            $cash_closing = DB::table('cash_closing')
                ->where('restaurant_id', $restaurant_id)
                ->where('closing_date', $date)
                ->first();

        }

        return view('cupcake.accounts.cash-closing', [
            'cash_closing' => $cash_closing
        ]);
    }


    public function addCashClosing(Request $request) {

        $restaurant_id = Auth::user()->restaurant_id;
        $date = date('Y-m-d');
        $created_by = Auth::user()->id;

        $data = $request->input('params');

        $note_1000 = $data['note_1000']!=''?intval($data['note_1000']):0;
        $note_500 = $data['note_500']!=''?intval($data['note_500']):0;
        $note_100 = $data['note_100']!=''?intval($data['note_100']):0;
        $note_50 = $data['note_50']!=''?intval($data['note_50']):0;
        $note_20 = $data['note_20']!=''?intval($data['note_20']):0;
        $note_10 = $data['note_10']!=''?intval($data['note_10']):0;
        $note_5 = $data['note_5']!=''?intval($data['note_5']):0;
        $note_2 = $data['note_2']!=''?intval($data['note_2']):0;
        $note_1 = $data['note_1']!=''?intval($data['note_1']):0;

        $total_amount = 0;

        $total_amount += ($note_1000*1000) + ($note_500*500) + ($note_100*100) + ($note_50*50) + ($note_20*20) + ($note_10*10)
            + ($note_5*5) + ($note_2*2) + ($note_1*1);

        DB::table('cash_closing')
            ->where('closing_date', $date)
            ->where('restaurant_id', $restaurant_id)
            ->update([
                'note_1000' => $data['note_1000'],
                'note_500' => $data['note_500'],
                'note_100' => $data['note_100'],
                'note_50' => $data['note_50'],
                'note_20' => $data['note_20'],
                'note_10' => $data['note_10'],
                'note_5' => $data['note_5'],
                'note_2' => $data['note_2'],
                'note_1' => $data['note_1'],
                'total_amount' => $total_amount,
                'set' => 1,
                'created_by' => $created_by,
            ]);

        app('App\Http\Controllers\ProfileController')->endWorkPeriod();

        return json_encode('success');
    }


    public function loanCredit() {

        $date = date('Y-m-d');

        $restaurant_id = Auth::user()->restaurant_id;
        $created_by = Auth::user()->id;

        $loans_from = DB::table('loans_from')
            ->where('restaurant_id', $restaurant_id)
            ->get();


        $loans_history = DB::table('loans_history')
            ->select('loans_history.*', 'loans_from.name as owner_name', 'users.name')
            ->where('loans_history.restaurant_id', $restaurant_id)
            ->join('loans_from', 'loans_from.owner_id', 'loans_history.owner_id')
            ->join('users', 'users.id', 'loans_history.created_by')
            ->get();


        return view('cupcake.accounts.loans', [
            'loans_from' => $loans_from,
            'loans_history' => $loans_history,
        ]);
    }


    public function addOwner(Request $request) {

        $data = $request->input('params');

        $restaurant_id = Auth::user()->restaurant_id;
        $created_by = Auth::user()->id;

        $owner_id = DB::table('loans_from')->insertGetId([
            'name' => $data['name'],
            'address' => $data['address'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'ac' => $data['ac'],
            'restaurant_id' => $restaurant_id,
            'created_by' => $created_by,
        ]);

        return json_encode($owner_id);
    }


    public function addLoan(Request $request) {

        $restaurant_id = Auth::user()->restaurant_id;
        $created_by = Auth::user()->id;

        $data = $request->input('params');

        $history_id = DB::table('loans_history')->insertGetId([
            'owner_id' => $data['owner_id'],
            'amount' => $data['amount'],
            'transaction_type' => $data['transaction_type'],
            'created_by' => $created_by,
            'restaurant_id' => $restaurant_id,
        ]);

        if ($data['transaction_type'] == 'credit') {

            DB::table('restaurants')
                ->where('restaurants_id', $restaurant_id)
                ->increment('loan', $data['amount']);

            DB::table('loans_from')
                ->where('owner_id', $data['owner_id'])
                ->increment('accounts_payable', $data['amount']);

        }


        return json_encode($history_id);
    }


    public function loanDebit() {

        $restaurant_id = Auth::user()->restaurant_id;

        $loans_from = DB::table('loans_from')
            ->where('restaurant_id', $restaurant_id)
            ->get();

        $loans_history = DB::table('loans_history')
            ->select('loans_history.*', 'loans_from.name as owner_name', 'users.name')
            ->where('loans_history.restaurant_id', $restaurant_id)
            ->join('loans_from', 'loans_from.owner_id', 'loans_history.owner_id')
            ->join('users', 'users.id', 'loans_history.created_by')
            ->get();


        return view('cupcake.accounts.loan-debit', [
            'loans_from' => $loans_from,
            'loans_history' => $loans_history,
        ]);
    }


    public function addLoanDebit(Request $request) {

        $restaurant_id = Auth::user()->restaurant_id;
        $created_by = Auth::user()->id;

        $data = $request->input('params');

        $history_id = DB::table('loans_history')->insertGetId([
            'owner_id' => $data['owner_id'],
            'amount' => $data['amount'],
            'transaction_type' => $data['transaction_type'],
            'created_by' => $created_by,
            'restaurant_id' => $restaurant_id,
        ]);

        $purpose = 'Loan returned to ' . $data['owner_name'];

        $expense_id = DB::table('daily_expense')->insertGetId([
            'restaurant_id' => $restaurant_id,
            'purpose' => $purpose,
            'debit_from' => $data['debit_from'],
            'amount' => $data['amount'],
            'created_by' => $created_by,
        ]);

        DB::table('restaurants')
            ->where('restaurants_id', $restaurant_id)
            ->decrement('loan', $data['amount']);


        DB::table('loans_from')
            ->where('owner_id', $data['owner_id'])
            ->decrement('accounts_payable', $data['amount']);


        return json_encode($history_id);
    }

}
