<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class HRController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }


    public function index() {

        $restaurant_id = Auth::user()->restaurant_id;
        $role = Auth::user()->role;

        $role_lookup = DB::table('role_lookup')
            ->where('role_name', '<>', 'superadmin')
            ->get();

        if ($role == 'superadmin') {
            $user_data = DB::table('users')
                ->where('restaurant_id', $restaurant_id)
                ->where('status', 1)
                ->get();
        }

        else {
            $user_data = DB::table('users')
                ->where('restaurant_id', $restaurant_id)
                ->where('role', '<>', 'superadmin')
                ->where('status', 1)
                ->get();
        }

        return view('cupcake.hr.index', [
            'role_lookup' => $role_lookup,
            'user_data' => $user_data,
        ]);
    }


    public function addUser(Request $request) {

        $restaurant_id = Auth::user()->restaurant_id;
        $creator_id = Auth::user()->id;
        $data= $request->input('params');
        $date = date('Y-m-d H:i:s');

        $rules = array(
            'name' => 'required|string|max:255',
            'email' => 'required|string|max:255|unique:users',
            'password' => 'required|string|min:6',
        );

        $validator = Validator::make($data, $rules);
        if ($validator->passes()) {

            $restaurant = DB::table('restaurants')
                ->select('inventory', 'license')
                ->where('restaurants_id', $restaurant_id)
                ->first();

            $user_id = DB::table('users')->insertGetId([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'role' => $data['role'],
                'phone' => $data['phone'],
                'restaurant_id' => $restaurant_id,
                'creator_id' => $creator_id,
                'inventory' => $restaurant->inventory,
                'license' => $restaurant->license,
                'created_at' => $date,
                'updated_at' => $date,
            ]);


            return json_encode($user_id);
        }
        else {
            echo json_encode(array('errors' => $validator->getMessageBag()->toArray()));
        }

    }


    public function team() {

        $restaurant_id = Auth::user()->restaurant_id;

        $user_data = DB::table('users')
            ->where('restaurant_id', '=', $restaurant_id)
            ->get();

        return view('cupcake.hr.team', [
            'user_data' => $user_data,
        ]);
    }


    public function deleteUser(Request $request) {

        $data = $request->input('params');
        $userId = Auth::user()->id;
        $restaurant_id = Auth::user()->restaurant_id;

//        DB::table('users')->where('id', '=', $data['id'])->delete();

        DB::table('users')
            ->where('id', $data['id'])
            ->update([
                'password' => Hash::make(randomPassword()),
                'status' => 2
            ]);


        return json_encode("success");
    }


    public function editUser(Request $request) {

        $data= $request->input('params');
        $date = date('Y-m-d H:i:s');

        DB::table('users')
            ->where('id', $data['id'])
            ->update([
                'name' => $data['name'],
                'role' => $data['role'],
                'phone' => $data['phone'],
                'updated_at' => $date
            ]);

        return json_encode('success');
    }


    public function editPassword(Request $request) {

        $data = $request->input('params');

        $user_id = $data['user_id'];
        $new_password = $data['new_password'];

        DB::table('users')
            ->where('id', $user_id)
            ->update([
                'password' => Hash::make($new_password)
            ]);

        return json_encode('success');

    }

}
