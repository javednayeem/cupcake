<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

class ReportController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }


    public function index() {

        $restaurant_id = Auth::user()->restaurant_id;
        $year = date("Y");

        $user_data = DB::table('users')
            ->select('users.*'
                , DB::raw("(SELECT COUNT(order_id)FROM orders WHERE orders.waiter_id=users.id) as order_count")
            )
            ->where('restaurant_id', '=', $restaurant_id)
            ->get();

        $table_data = DB::table('tables')
            ->where('restaurant_id', '=', $restaurant_id)
            ->get();

        $reports = DB::select("SELECT MONTH(created_at) MONTH, COUNT(*) COUNT, SUM(total_bill) BILL FROM orders WHERE YEAR(created_at)=$year AND restaurant_id=$restaurant_id GROUP BY MONTH(created_at)");

        $week_reports = DB::select("SELECT WEEKDAY(created_at) WEEKDAY, COUNT(*) COUNT, SUM(total_bill) BILL FROM orders WHERE YEAR(created_at)=$year AND restaurant_id=$restaurant_id GROUP BY WEEKDAY(created_at)");

        $table_reports = DB::table('tables')
            ->select('tables.table_name'
                , DB::raw("(SELECT COUNT(order_id)FROM orders WHERE orders.table_id=tables.table_id) as table_order")
            )
            ->where('restaurant_id', $restaurant_id)
            ->get();

        /*
         *  Menu wise analytics
         */
        $menu_count = DB::table('menus')
            ->select('menus.menu_id', 'menus.menu_name')
            ->where('restaurants_id', $restaurant_id)
            ->where('status', 1)
            ->get();
        $menu_analytics = [];
        foreach ($menu_count as $menu) {

            $count = 0;

            $menu_items_count = DB::table('menu_items')
                ->select(
                    DB::raw("(SELECT SUM(item_quantity)FROM order_details WHERE order_details.menu_item_id=menu_items.menu_item_id) as menu_item_count")
                )
                ->where('menu_id', $menu->menu_id)
                ->get();

            foreach ($menu_items_count as $c) {
                $c_temp = ($c->menu_item_count==null? '0': $c->menu_item_count);
                $count += $c_temp;
            }
            //echo $menu->menu_name . " : " . ($menu_items_count->menu_item_count==null? 0: $menu_items_count->menu_item_count) . "<br>";
            //$count = ($menu_items_count->menu_item_count==null? '0': $menu_items_count->menu_item_count);
            $newdata =  array (
                'menu_name' => $menu->menu_name,
                'menu_count' => $count,
            );
            array_push($menu_analytics,$newdata);
        }

        $restaurant_data = DB::table('restaurants')
            ->where('restaurants_id', $restaurant_id)
            ->first();


        return view('cupcake.sales.index', [
            'user_data' => $user_data,
            'table_data' => $table_data,
            'reports' => $reports,
            'restaurant_data' => $restaurant_data,
            'week_reports' => $week_reports,
            'table_reports' => $table_reports,
            'menu_analytics' => $menu_analytics,
        ]);
    }


    public function salesReportGeneration() {

        $restaurant_id = Auth::user()->restaurant_id;
        $year = date("Y");

        $user_data = DB::table('users')
            ->select('users.*'
                , DB::raw("(SELECT COUNT(order_id)FROM orders WHERE orders.waiter_id=users.id) as order_count")
            )
            ->where('restaurant_id', '=', $restaurant_id)
            ->get();

        $table_data = DB::table('tables')
            ->where('restaurant_id', '=', $restaurant_id)
            ->get();


        $restaurant_data = DB::table('restaurants')
            ->where('restaurants_id', $restaurant_id)
            ->first();


        return view('cupcake.sales.sales-report-generation', [
            'user_data' => $user_data,
            'table_data' => $table_data,
            'restaurant_data' => $restaurant_data,
        ]);
    }


    public function generateReport(Request $request) {

        $data = $request->input('params');
        $restaurant_id = Auth::user()->restaurant_id;

        if ($data['select_report_type'] == 'waiter_wise_sale') {

            $from = $data['start_date'] . ' 00:00:00';
            $to = $data['end_date'] . ' 23:59:59';
            $waiter_id = $data['waiter_id'];

            if ($waiter_id != 0) {

                $report = DB::table('orders')
                    ->select('orders.*'
                        , DB::raw("(SELECT COUNT(order_details_id)FROM order_details WHERE order_id=orders.order_id AND item_status=1) as total_item")
                        , DB::raw("(SELECT name FROM users WHERE users.id=orders.waiter_id) as waiter_name")
                        , DB::raw("(SELECT customer_name FROM customers WHERE customers.customer_id=orders.customer_id) as customer_name")
                        , DB::raw("(SELECT table_name FROM tables WHERE tables.table_id=orders.table_id) as table_name"))
                    ->where('waiter_id', $data['waiter_id'])
                    ->where('order_status', 'paid')
                    ->whereBetween('created_at', array($from, $to))
                    ->orderBy('order_id', 'DESC')
                    ->get();

            }

            else {

                $report = DB::table('orders')
                    ->select('orders.*'
                        , DB::raw("(SELECT COUNT(order_details_id)FROM order_details WHERE order_id=orders.order_id) as total_item")
                        , DB::raw("(SELECT name FROM users WHERE users.id=orders.waiter_id) as waiter_name")
                        , DB::raw("(SELECT customer_name FROM customers WHERE customers.customer_id=orders.customer_id) as customer_name")
                        , DB::raw("(SELECT table_name FROM tables WHERE tables.table_id=orders.table_id) as table_name"))
                    ->where('order_status', 'paid')
                    ->whereBetween('created_at', array($from, $to))
                    ->orderBy('order_id', 'DESC')
                    ->get();

            }


        }


        else if ($data['select_report_type'] == 'table_wise_sale') {
            $report = DB::table('orders')
                ->select('orders.*'
                    , DB::raw("(SELECT COUNT(order_details_id)FROM order_details WHERE order_id=orders.order_id AND item_status=1) as total_item")
                    , DB::raw("(SELECT name FROM users WHERE users.id=orders.waiter_id) as waiter_name")
                    , DB::raw("(SELECT customer_name FROM customers WHERE customers.customer_id=orders.customer_id) as customer_name")
                    , DB::raw("(SELECT table_name FROM tables WHERE tables.table_id=orders.table_id) as table_name")

                )
                ->where('table_id', $data['table_id'])
                ->where('order_status', 'paid')
                ->orderBy('order_id', 'DESC')
                ->get();
        }


        else if ($data['select_report_type'] == 'invoice_wise_sale') {

            $from = $data['start_date'] . ' 00:00:00';
            $to = $data['end_date'] . ' 23:59:59';

            $report = DB::table('orders')
                ->select('orders.*'
                    , DB::raw("(SELECT COUNT(order_details_id)FROM order_details WHERE order_id=orders.order_id AND item_status=1) as total_item")
                    , DB::raw("(SELECT name FROM users WHERE users.id=orders.waiter_id) as waiter_name")
                    , DB::raw("(SELECT customer_name FROM customers WHERE customers.customer_id=orders.customer_id) as customer_name")
                    , DB::raw("(SELECT table_name FROM tables WHERE tables.table_id=orders.table_id) as table_name"))
                ->where('restaurant_id', $restaurant_id)
                ->where('order_status', 'paid')
                ->whereBetween('created_at', array($from, $to))
                ->orderBy('order_id', 'DESC')
                ->get();
        }


        else if ($data['select_report_type'] == 'order_summary') {
            $report = DB::table('orders')
                ->select('orders.*'
                    , DB::raw("(SELECT COUNT(order_details_id)FROM order_details WHERE order_id=orders.order_id AND item_status=1) as total_item")
                    , DB::raw("(SELECT name FROM users WHERE users.id=orders.waiter_id) as waiter_name")
                    , DB::raw("(SELECT customer_name FROM customers WHERE customers.customer_id=orders.customer_id) as customer_name")
                    , DB::raw("(SELECT table_name FROM tables WHERE tables.table_id=orders.table_id) as table_name")

                )
                ->where('order_id', $data['order_id'])
                ->where('order_status', 'paid')
                ->get();
        }


        else if ($data['select_report_type'] == 'month_wise_sale') {

            $from = $data['start_date'] . ' 00:00:00';
            $to = $data['end_date'] . ' 23:59:59';

            $report = DB::table('orders')
                ->select('orders.*'
                    , DB::raw("(SELECT COUNT(order_details_id)FROM order_details WHERE order_id=orders.order_id AND item_status=1) as total_item")
                    , DB::raw("(SELECT name FROM users WHERE users.id=orders.waiter_id) as waiter_name")
                    , DB::raw("(SELECT customer_name FROM customers WHERE customers.customer_id=orders.customer_id) as customer_name")
                    , DB::raw("(SELECT table_name FROM tables WHERE tables.table_id=orders.table_id) as table_name")

                )
                ->where('restaurant_id',$restaurant_id)
                ->where('order_status', 'paid')
                ->whereBetween('created_at', array($from, $to))
                ->get();
        }


        else if ($data['select_report_type'] == 'item_wise_sale') {

            $from = $data['start_date'] . ' 00:00:00';
            $to = $data['end_date'] . ' 23:59:59';
            $item_id = $data['item_id'];

            if ($item_id != 0) {

                $report = DB::table('menu_items')
                    ->select('menu_items.item_name', 'menu_items.ratio', 'order_details.*', 'orders.*', 'users.name as waiter_name')
                    ->where('menus.restaurants_id', $restaurant_id)
                    ->where('orders.restaurant_id', $restaurant_id)
                    ->where('orders.order_status', 'paid')
                    ->where('order_details.item_status', 1)
                    ->where('menu_items.menu_item_id', $item_id)
                    ->whereBetween('orders.created_at', array($from, $to))
                    ->orderBy('orders.order_id', 'DESC')
                    ->join('menus', 'menus.menu_id', 'menu_items.menu_id')
                    ->join('order_details', 'order_details.menu_item_id', 'menu_items.menu_item_id')
                    ->join('orders', 'orders.order_id', 'order_details.order_id')
                    ->join('users', 'users.id', 'orders.waiter_id')
                    ->get();

            }

            else {

                $report = DB::table('menu_items')
                    ->select('menu_items.item_name', 'menu_items.ratio', 'order_details.*', 'orders.*', 'users.name as waiter_name')
                    ->where('menus.restaurants_id', $restaurant_id)
                    ->where('orders.restaurant_id', $restaurant_id)
                    ->where('orders.order_status', 'paid')
                    ->where('order_details.item_status', 1)
                    ->whereBetween('orders.created_at', array($from, $to))
                    ->orderBy('orders.order_id', 'DESC')
                    ->join('menus', 'menus.menu_id', 'menu_items.menu_id')
                    ->join('order_details', 'order_details.menu_item_id', 'menu_items.menu_item_id')
                    ->join('orders', 'orders.order_id', 'order_details.order_id')
                    ->join('users', 'users.id', 'orders.waiter_id')
                    ->get();

            }

        }


        else if ($data['select_report_type'] == 'category_wise_sale') {

            $from = $data['start_date'] . ' 00:00:00';
            $to = $data['end_date'] . ' 23:59:59';
            $menu_id = $data['menu_id'];

            $menu_items = '';

            $reports = DB::table('menu_items')
                ->select('menu_items.menu_id', 'order_details.item_price', 'order_details.item_quantity', 'order_details.item_vat')
                ->where('menus.restaurants_id', $restaurant_id)
                ->where('orders.restaurant_id', $restaurant_id)
                ->where('orders.order_status', 'paid')
                ->where('order_details.item_status', 1)
                ->whereBetween('orders.created_at', array($from, $to))
                ->orderBy('orders.order_id', 'DESC')
                ->join('menus', 'menus.menu_id', 'menu_items.menu_id')
                ->join('order_details', 'order_details.menu_item_id', 'menu_items.menu_item_id')
                ->join('orders', 'orders.order_id', 'order_details.order_id')
                ->get();


            if ($menu_id != 0) {

                $menus = DB::table('menus')
                    ->where('menus.menu_id', $menu_id)
                    ->get();

                $menu_items = DB::table('menu_items')
                    ->select('menu_items.item_name', 'menu_items.price'
                        , DB::raw("(SELECT SUM(item_quantity) FROM order_details WHERE order_details.item_status=1 AND order_details.settle=1 AND menu_item_id=menu_items.menu_item_id AND (order_details.created_at BETWEEN '$from' AND '$to')) as item_quantity")

                        , DB::raw("(SELECT AVG(item_price) FROM order_details WHERE order_details.item_status=1 AND order_details.settle=1 AND menu_item_id=menu_items.menu_item_id AND (order_details.created_at BETWEEN '$from' AND '$to')) as item_price")

                        , DB::raw("(SELECT AVG(item_vat) FROM order_details WHERE order_details.item_status=1 AND order_details.settle=1 AND menu_item_id=menu_items.menu_item_id AND (order_details.created_at BETWEEN '$from' AND '$to')) as item_vat")
                    )
                    ->where('menu_id', $menu_id)
                    ->get();

            }

            else {

                $menus = DB::table('menus')
                    ->where('menus.restaurants_id', $restaurant_id)
                    ->get();

//                $menu_items = DB::table('menu_items')
//                    ->select('menu_items.item_name', 'menu_items.price'
//                        , DB::raw("(SELECT SUM(item_quantity) FROM order_details WHERE menu_item_id=menu_items.menu_item_id AND (order_details.created_at BETWEEN '$from' AND '$to')) as item_quantity")
//                    )
//                    ->get();

            }

            $report =  array(
                "menus"   => $menus,
                "reports" => $reports,
                "menu_items" => $menu_items,
            );


        }


        return json_encode($report);
    }


    public function generateOrderDetails(Request $request) {
        $data = $request->input('params');

        $orders = DB::table('order_details')
            ->select('menu_item_name', 'item_price', 'item_discount', 'item_quantity')
            ->where('order_id', $data['order_id'])
            ->where('item_status', 1)
            ->get();

        return json_encode($orders);
    }


    public function report() {

        $restaurant_id = Auth::user()->restaurant_id;
        $user_id = Auth::user()->id;
        $year = date("Y");
        $permission = 1;

        $user_data = DB::table('users')
            ->select('users.*'
                , DB::raw("(SELECT COUNT(order_id)FROM orders WHERE orders.waiter_id=users.id) as order_count"))
            ->where('restaurant_id', $restaurant_id)
            ->where('role', 'waiter')
            ->get();

        $table_data = DB::table('tables')
            ->where('restaurant_id', $restaurant_id)
            ->get();

        $menus = DB::table('menus')
            ->where('menus.restaurants_id', $restaurant_id)
            ->get();

        $menu_items = DB::table('menu_items')
            ->select('menu_items.*')
            ->where('menus.restaurants_id', $restaurant_id)
            ->join('menus', 'menus.menu_id', 'menu_items.menu_id')
            ->get();

        $restaurant_data = DB::table('restaurants')
            ->select('restaurants.*',
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='receipt_footer') as receipt_footer"),
                DB::raw("(SELECT setting_value FROM app_settings WHERE setting_key='receipt_company') as receipt_company"),
                DB::raw("(SELECT setting_value FROM app_settings WHERE setting_key='receipt_phone') as receipt_phone")
            )
            ->where('restaurants_id', $restaurant_id)
            ->first();


        $report_type = DB::table('report_type')
            ->select('report_type.*')
            ->where('report_type.report_type', 'sales')
            ->where('report_permission.user_id', $user_id)
            ->where('report_permission.permission', $permission)
            ->join('report_permission', 'report_permission.reportType_id', 'report_type.reportType_id')
            ->get();


        return view('cupcake.sales.sales-report', [
            'user_data' => $user_data,
            'table_data' => $table_data,
            'restaurant_data' => $restaurant_data,
            'menu_items' => $menu_items,
            'menus' => $menus,
            'report_type' => $report_type,
        ]);
    }


    public function voidReport() {

        $restaurant_id = Auth::user()->restaurant_id;
        $user_id = Auth::user()->id;
        $year = date("Y");
        $permission = 1;

        $user_data = DB::table('users')
            ->select('users.*'
                , DB::raw("(SELECT COUNT(order_id)FROM orders WHERE orders.waiter_id=users.id) as order_count"))
            ->where('restaurant_id', $restaurant_id)
            ->where('role', 'waiter')
            ->get();

        $table_data = DB::table('tables')
            ->where('restaurant_id', $restaurant_id)
            ->get();

        $menus = DB::table('menus')
            ->where('menus.restaurants_id', $restaurant_id)
            ->get();

        $menu_items = DB::table('menu_items')
            ->select('menu_items.*')
            ->where('menus.restaurants_id', $restaurant_id)
            ->join('menus', 'menus.menu_id', 'menu_items.menu_id')
            ->get();


        $restaurant_data = DB::table('restaurants')
            ->select('restaurants.*',
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='receipt_footer') as receipt_footer"),
                DB::raw("(SELECT setting_value FROM app_settings WHERE setting_key='receipt_company') as receipt_company"),
                DB::raw("(SELECT setting_value FROM app_settings WHERE setting_key='receipt_phone') as receipt_phone")
            )
            ->where('restaurants_id', $restaurant_id)
            ->first();


        $report_type = DB::table('report_type')
            ->select('report_type.*')
            ->where('report_type.report_type', 'void')
            ->where('report_permission.user_id', $user_id)
            ->where('report_permission.permission', $permission)
            ->join('report_permission', 'report_permission.reportType_id', 'report_type.reportType_id')
            ->get();


        return view('cupcake.sales.void-report', [
            'user_data' => $user_data,
            'table_data' => $table_data,
            'restaurant_data' => $restaurant_data,
            'menu_items' => $menu_items,
            'menus' => $menus,
            'report_type' => $report_type,
        ]);
    }


    public function generateVoidReport(Request $request) {

        $data = $request->input('params');
        $restaurant_id = Auth::user()->restaurant_id;

        if ($data['select_report_type'] == 'waiter_wise_void') {

            $from = $data['start_date'] . ' 00:00:00';
            $to = $data['end_date'] . ' 23:59:59';
            $waiter_id = $data['waiter_id'];

            if ($waiter_id != 0) {

                $report = DB::table('void_item')
                    ->select('void_item.*', 'users.name' ,'order_details.item_price',
                        DB::raw("(SELECT name FROM users WHERE users.id=void_item.approved_admin_id) as void_by"))
                    ->where('void_item.restaurant_id', $restaurant_id)
                    ->where('waiter_id', $waiter_id)
                    ->whereBetween('void_item.created_at', array($from, $to))
                    ->join('users', 'users.id', 'void_item.waiter_id')
                    ->join('order_details', 'order_details.order_details_id', 'void_item.order_details_id')
                    ->orderBy('void_item.void_item_id', 'DESC')
                    ->get();

            }

            else {

                $report = DB::table('void_item')
                    ->select('void_item.*', 'users.name' ,'order_details.item_price', 'order_details.item_quantity',
                        DB::raw("(SELECT name FROM users WHERE users.id=void_item.approved_admin_id) as void_by"))
                    ->where('void_item.restaurant_id', $restaurant_id)
                    ->whereBetween('void_item.created_at', array($from, $to))
                    ->join('users', 'users.id', 'void_item.waiter_id')
                    ->join('order_details', 'order_details.order_details_id', 'void_item.order_details_id')
                    ->orderBy('void_item.void_item_id', 'DESC')
                    ->get();

            }


        }

        else if ($data['select_report_type'] == 'invoice_wise_void' || $data['select_report_type'] == 'total_void') {

            $from = $data['start_date'] . ' 00:00:00';
            $to = $data['end_date'] . ' 23:59:59';

            $report = DB::table('void_item')
                ->select('void_item.*','users.name','orders.bill','order_details.item_price',
                    DB::raw("(SELECT name FROM users WHERE users.id=void_item.approved_admin_id) as void_by"))
                ->where('void_item.restaurant_id', $restaurant_id)
                ->whereBetween('void_item.created_at', array($from, $to))
                ->join('users', 'users.id', 'void_item.waiter_id')
                ->join('orders', 'orders.order_id', 'void_item.order_id')
                ->join('order_details', 'order_details.order_details_id', 'void_item.order_details_id')
                ->orderBy('void_item.void_item_id', 'DESC')
                ->get();


        }

        else if ($data['select_report_type'] == 'item_wise_void') {

            $from = $data['start_date'] . ' 00:00:00';
            $to = $data['end_date'] . ' 23:59:59';
            $item_id = $data['item_id'];

            if ($item_id != 0) {

                $report = DB::table('void_item')
                    ->select('void_item.*', 'users.name','order_details.menu_item_name','order_details.item_price', 'menus.menu_name',
                        DB::raw("(SELECT name FROM users WHERE users.id=void_item.approved_admin_id) as void_by"))
                    ->where('void_item.restaurant_id', $restaurant_id)
                    ->where('order_details.menu_item_id', $item_id)
                    ->whereBetween('void_item.created_at', array($from, $to))
                    ->join('users', 'users.id', 'void_item.waiter_id')
                    ->join('order_details', 'order_details.order_details_id', 'void_item.order_details_id')
                    ->join('menu_items', 'menu_items.menu_item_id', 'order_details.menu_item_id')
                    ->join('menus', 'menus.menu_id', 'menu_items.menu_id')
                    ->orderBy('void_item.void_item_id', 'DESC')
                    ->get();

            }

            else {

                $report = DB::table('void_item')
                    ->select('void_item.*', 'users.name','order_details.menu_item_name','order_details.item_price', 'menus.menu_name',
                        DB::raw("(SELECT name FROM users WHERE users.id=void_item.approved_admin_id) as void_by"))
                    ->where('void_item.restaurant_id', $restaurant_id)
                    ->whereBetween('void_item.created_at', array($from, $to))
                    ->join('users', 'users.id', 'void_item.waiter_id')
                    ->join('order_details', 'order_details.order_details_id', 'void_item.order_details_id')
                    ->join('menu_items', 'menu_items.menu_item_id', 'order_details.menu_item_id')
                    ->join('menus', 'menus.menu_id', 'menu_items.menu_id')
                    ->orderBy('void_item.void_item_id', 'DESC')
                    ->get();

            }

        }





        return json_encode($report);
    }


    public function expenseReport() {

        $restaurant_id = Auth::user()->restaurant_id;
        $user_id = Auth::user()->id;

        $date = date('Y-m-d');

        $from = $date . ' 00:00:00';
        $to = $date . ' 23:59:59';

        $restaurant_data = DB::table('restaurants')
            ->select('restaurants.*')
            ->where('restaurants_id', $restaurant_id)
            ->first();

        $user_data = DB::table('users')
            ->where('restaurant_id', $restaurant_id)
            ->get();

        $daily_expense = DB::table('daily_expense')
            ->select('daily_expense.*', 'users.name')
            ->where('daily_expense.restaurant_id', $restaurant_id)
            ->join('users', 'users.id', 'daily_expense.created_by')
            ->whereBetween('daily_expense.created_at', array($from, $to))
            ->get();





        return view('cupcake.sales.expense-report', [
            'restaurant_data' => $restaurant_data,
            'user_data' => $user_data,
            'daily_expense' => $daily_expense,
        ]);
    }


    public function generateExpenseReport(Request $request) {

        $restaurant_id = Auth::user()->restaurant_id;
        $data = $request->input('params');

        $from = $data['start_date'] . ' 00:00:00';
        $to = $data['end_date'] . ' 23:59:59';

        $report = DB::table('daily_expense')
            ->select('daily_expense.*', 'users.name')
            ->where('daily_expense.restaurant_id', $restaurant_id)
            ->join('users', 'users.id', 'daily_expense.created_by')
            ->whereBetween('daily_expense.created_at', array($from, $to))
            ->get();

        return json_encode($report);
    }


    public function reservationReport() {

        $restaurant_id = Auth::user()->restaurant_id;

        $restaurant_data = DB::table('restaurants')
            ->select('restaurants.*')
            ->where('restaurants_id', $restaurant_id)
            ->first();

        $customers = DB::table('customers')
            ->where('restaurant_id', $restaurant_id)
            ->get();

        $reservations = DB::table('reservations')
            ->select('reservations.*', 'customers.customer_name', 'users.name')
            ->where('reservations.restaurant_id', $restaurant_id)
            ->join('customers', 'customers.customer_id', 'reservations.customer_id')
            ->join('users', 'users.id', 'reservations.created_by')
            ->get();

        return view('cupcake.sales.reservation-report', [
            'restaurant_data' => $restaurant_data,
            'customers' => $customers,
            'reservations' => $reservations,
        ]);
    }


    public function generateReservationReport(Request $request) {

        $restaurant_id = Auth::user()->restaurant_id;
        $data = $request->input('params');

        $from = $data['start_date'];
        $to = $data['end_date'];

        $reservations = DB::table('reservations')
            ->select('reservations.*', 'customers.customer_name', 'users.name')
            ->where('reservations.restaurant_id', $restaurant_id)
            ->join('customers', 'customers.customer_id', 'reservations.customer_id')
            ->join('users', 'users.id', 'reservations.created_by')
            ->whereBetween('reservations.reservation_date', array($from, $to))
            ->get();


        return json_encode($reservations);
    }


    public function currentStockReport(Request $request) {

        $restaurant_id = Auth::user()->restaurant_id;
        $data = $request->input('params');

        $category_id = 0;
        $product_id = 0;

        $select_report_type = $data['select_report_type'];

        if ($select_report_type == 'category_wise_report') $category_id = $data['category_id'];
        else if ($select_report_type == 'product_wise_report') $product_id = $data['product_id'];


        $main_store_id = getMainStoreId($restaurant_id);

        if ($select_report_type == "0") {

            $products = DB::table('inv_product_store')
                ->select('inv_product_store.quantity', 'inv_product.product_code','inv_product.product_name', 'inv_product.avg_price', 'inv_product_category.category_name', 'inv_units.unit_name',
                    DB::raw("(SELECT SUM(quantity) FROM recipe_consumption WHERE product_id=inv_product_store.product_id) as consumption_quantity"),
                    DB::raw("(SELECT SUM(quantity) FROM inv_damage_products WHERE product_id=inv_product_store.product_id) as damage_quantity")
                )
                ->where('store_id', $main_store_id)
                ->join('inv_product', 'inv_product.product_id', 'inv_product_store.product_id')
                ->join('inv_product_category', 'inv_product_category.category_id', 'inv_product.category_id')
                ->join('inv_units', 'inv_units.unit_id', 'inv_product.unit_id')
                ->get();

        }

        else if ($select_report_type == 'product_wise_report') {

            $products = DB::table('inv_product_store')
                ->select('inv_product_store.quantity', 'inv_product.product_code','inv_product.product_name', 'inv_product.avg_price', 'inv_product_category.category_name', 'inv_units.unit_name',
                    DB::raw("(SELECT SUM(quantity) FROM recipe_consumption WHERE product_id=inv_product_store.product_id) as consumption_quantity"),
                    DB::raw("(SELECT SUM(quantity) FROM inv_damage_products WHERE product_id=inv_product_store.product_id) as damage_quantity")
                )
                ->where('inv_product_store.store_id', $main_store_id)
                ->where('inv_product_store.product_id', $product_id)
                ->join('inv_product', 'inv_product.product_id', 'inv_product_store.product_id')
                ->join('inv_product_category', 'inv_product_category.category_id', 'inv_product.category_id')
                ->join('inv_units', 'inv_units.unit_id', 'inv_product.unit_id')
                ->get();


        }

        else if ($select_report_type == 'category_wise_report') {

            $products = DB::table('inv_product_store')
                ->select('inv_product_store.quantity', 'inv_product.product_code','inv_product.product_name', 'inv_product.avg_price', 'inv_product_category.category_name', 'inv_units.unit_name',
                    DB::raw("(SELECT SUM(quantity) FROM recipe_consumption WHERE product_id=inv_product_store.product_id) as consumption_quantity"),
                    DB::raw("(SELECT SUM(quantity) FROM inv_damage_products WHERE product_id=inv_product_store.product_id) as damage_quantity")
                )
                ->where('inv_product_store.store_id', $main_store_id)
                ->where('inv_product.category_id', $category_id)
                ->join('inv_product', 'inv_product.product_id', 'inv_product_store.product_id')
                ->join('inv_product_category', 'inv_product_category.category_id', 'inv_product.category_id')
                ->join('inv_units', 'inv_units.unit_id', 'inv_product.unit_id')
                ->get();

        }

        return json_encode($products);
    }


    public function purchaseHistoryReport(Request $request) {

        $restaurant_id = Auth::user()->restaurant_id;

        $data = $request->input('params');

        $from = $data['start_date'];
        $to = $data['end_date'];

        $category_id = 0;
        $product_id = 0;

        $select_report_type = $data['select_report_type'];

        if ($select_report_type == 'category_wise_report') $category_id = $data['category_id'];
        else if ($select_report_type == 'product_wise_report') $product_id = $data['product_id'];

        if ($select_report_type == "0") {

            $purchase_data = DB::table('inv_product_purchase_in')
                ->select('inv_product_purchase_in.*', 'inv_purchase_invoice.invoice_no', 'inv_purchase_invoice.purchase_date', 'inv_product.product_name', 'inv_product_category.category_name', 'inv_units.unit_name')
                ->join('inv_purchase_invoice', 'inv_purchase_invoice.invoice_id', 'inv_product_purchase_in.purchase_invoice_id')
                ->join('inv_product', 'inv_product.product_id', 'inv_product_purchase_in.product_id')
                ->join('inv_product_category', 'inv_product_category.category_id', 'inv_product.category_id')
                ->join('inv_units', 'inv_units.unit_id', 'inv_product.unit_id')
                ->where('inv_purchase_invoice.restaurant_id', $restaurant_id)
                ->whereBetween('inv_purchase_invoice.purchase_date', array($from, $to))
                ->get();

        }

        else if ($select_report_type == 'product_wise_report') {

            $purchase_data = DB::table('inv_product_purchase_in')
                ->select('inv_product_purchase_in.*', 'inv_purchase_invoice.invoice_no', 'inv_purchase_invoice.purchase_date', 'inv_product.product_name', 'inv_product_category.category_name', 'inv_units.unit_name')
                ->join('inv_purchase_invoice', 'inv_purchase_invoice.invoice_id', 'inv_product_purchase_in.purchase_invoice_id')
                ->join('inv_product', 'inv_product.product_id', 'inv_product_purchase_in.product_id')
                ->join('inv_product_category', 'inv_product_category.category_id', 'inv_product.category_id')
                ->join('inv_units', 'inv_units.unit_id', 'inv_product.unit_id')
                ->where('inv_purchase_invoice.restaurant_id', $restaurant_id)
                ->where('inv_product_purchase_in.product_id', $product_id)
                ->whereBetween('inv_purchase_invoice.purchase_date', array($from, $to))
                ->get();


        }

        else if ($select_report_type == 'category_wise_report') {

            $purchase_data = DB::table('inv_product_purchase_in')
                ->select('inv_product_purchase_in.*', 'inv_purchase_invoice.invoice_no', 'inv_purchase_invoice.purchase_date', 'inv_product.product_name', 'inv_product_category.category_name', 'inv_units.unit_name')
                ->join('inv_purchase_invoice', 'inv_purchase_invoice.invoice_id', 'inv_product_purchase_in.purchase_invoice_id')
                ->join('inv_product', 'inv_product.product_id', 'inv_product_purchase_in.product_id')
                ->join('inv_product_category', 'inv_product_category.category_id', 'inv_product.category_id')
                ->join('inv_units', 'inv_units.unit_id', 'inv_product.unit_id')
                ->where('inv_purchase_invoice.restaurant_id', $restaurant_id)
                ->where('inv_product.category_id', $category_id)
                ->whereBetween('inv_purchase_invoice.purchase_date', array($from, $to))
                ->get();

        }

        return json_encode($purchase_data);

    }


    public function damageHistoryReport(Request $request) {

        $restaurant_id = Auth::user()->restaurant_id;

        $data = $request->input('params');

        $from = $data['start_date'] . ' 00:00:00';
        $to = $data['end_date'] . ' 23:59:59';

        $category_id = 0;
        $product_id = 0;

        $select_report_type = $data['select_report_type'];

        if ($select_report_type == 'category_wise_report') $category_id = $data['category_id'];
        else if ($select_report_type == 'product_wise_report') $product_id = $data['product_id'];

        if ($select_report_type == "0") {

            $damages = DB::table('inv_damage_products')
                ->select('inv_damage_products.*', 'inv_product.product_name', 'inv_product_category.category_name', 'inv_units.unit_name')
                ->join('inv_damage_entry', 'inv_damage_entry.damage_id', 'inv_damage_products.damage_id')
                ->join('inv_product', 'inv_product.product_id', 'inv_damage_products.product_id')
                ->join('inv_product_category', 'inv_product_category.category_id', 'inv_product.category_id')
                ->join('inv_units', 'inv_units.unit_id', 'inv_product.unit_id')
                ->where('inv_damage_entry.restaurant_id', $restaurant_id)
                ->whereBetween('inv_damage_products.created_at', array($from, $to))
                ->get();

        }

        else if ($select_report_type == 'product_wise_report') {


            $damages = DB::table('inv_damage_products')
                ->select('inv_damage_products.*', 'inv_product.product_name', 'inv_product_category.category_name', 'inv_units.unit_name')
                ->join('inv_damage_entry', 'inv_damage_entry.damage_id', 'inv_damage_products.damage_id')
                ->join('inv_product', 'inv_product.product_id', 'inv_damage_products.product_id')
                ->join('inv_product_category', 'inv_product_category.category_id', 'inv_product.category_id')
                ->join('inv_units', 'inv_units.unit_id', 'inv_product.unit_id')
                ->where('inv_damage_entry.restaurant_id', $restaurant_id)
                ->where('inv_damage_products.product_id', $product_id)
                ->whereBetween('inv_damage_products.created_at', array($from, $to))
                ->get();


        }

        else if ($select_report_type == 'category_wise_report') {

            $damages = DB::table('inv_damage_products')
                ->select('inv_damage_products.*', 'inv_product.product_name', 'inv_product_category.category_name', 'inv_units.unit_name')
                ->join('inv_damage_entry', 'inv_damage_entry.damage_id', 'inv_damage_products.damage_id')
                ->join('inv_product', 'inv_product.product_id', 'inv_damage_products.product_id')
                ->join('inv_product_category', 'inv_product_category.category_id', 'inv_product.category_id')
                ->join('inv_units', 'inv_units.unit_id', 'inv_product.unit_id')
                ->where('inv_damage_entry.restaurant_id', $restaurant_id)
                ->where('inv_product.category_id', $category_id)
                ->whereBetween('inv_damage_products.created_at', array($from, $to))
                ->get();

        }

        return json_encode($damages);

    }


    public function revenueReport() {

        $restaurant_id = Auth::user()->restaurant_id;

        $start_date = date("Y-m-01");
        $end_date = date("Y-m-d");

        $from = $start_date . ' 00:00:00';
        $to = $end_date . ' 23:59:59';

        $restaurant_data = DB::table('restaurants')
            ->select('restaurants.*')
            ->where('restaurants_id', $restaurant_id)
            ->first();


        $work_period = DB::table('work_period')
            ->select('work_period.*'
                , DB::raw("(SELECT SUM(amount) FROM order_payments WHERE payment_method='cash' AND (order_payments.created_at BETWEEN work_period.start_time AND work_period.end_time)) as cash_sale")
                , DB::raw("(SELECT SUM(amount) FROM order_payments WHERE payment_method='card' AND (order_payments.created_at BETWEEN work_period.start_time AND work_period.end_time)) as card_sale")
                , DB::raw("(SELECT SUM(amount) FROM daily_expense WHERE type='expense' AND restaurant_id='$restaurant_id' AND (daily_expense.created_at BETWEEN work_period.start_time AND work_period.end_time)) as total_expense")
                )
            ->where('work_period.restaurant_id', $restaurant_id)
            ->whereBetween('work_period.start_time', array($from, $to))
            ->get();


        return view('cupcake.sales.revenue-report', [
            'restaurant_data' => $restaurant_data,
            'work_period' => $work_period,
        ]);
    }


    public function generateRevenueReport(Request $request) {

        $restaurant_id = Auth::user()->restaurant_id;
        $data = $request->input('params');

        $from = $data['start_date'] . ' 00:00:00';
        $to = $data['end_date'] . ' 23:59:59';

        $report = DB::table('work_period')
            ->select('work_period.*'
                , DB::raw("(SELECT SUM(amount) FROM order_payments WHERE payment_method='cash' AND restaurant_id='$restaurant_id' AND (order_payments.created_at BETWEEN work_period.start_time AND work_period.end_time)) as cash_sale")
                , DB::raw("(SELECT SUM(amount) FROM order_payments WHERE payment_method='card' AND restaurant_id='$restaurant_id' AND (order_payments.created_at BETWEEN work_period.start_time AND work_period.end_time)) as card_sale")
                , DB::raw("(SELECT SUM(amount) FROM daily_expense WHERE type='expense' AND restaurant_id='$restaurant_id' AND (daily_expense.created_at BETWEEN work_period.start_time AND work_period.end_time)) as total_expense")
            )
            ->where('work_period.restaurant_id', $restaurant_id)
            ->whereBetween('work_period.start_time', array($from, $to))
            ->get();

        return json_encode($report);
    }


    public function recipeReport() {

        $restaurant_id = Auth::user()->restaurant_id;

        $restaurant_data = DB::table('restaurants')
            ->select('restaurants.*')
            ->where('restaurants_id', $restaurant_id)
            ->first();

        $menus = DB::table('menus')
            ->where('restaurants_id', $restaurant_id)
            ->get();

        $menu_items = DB::table('menu_items')
            ->select('menu_items.menu_item_id', 'menu_items.item_name', 'menu_items.ratio', 'menu_items.price', 'menus.menu_name')
            ->where('menus.restaurants_id', $restaurant_id)
            ->join('menus', 'menus.menu_id', 'menu_items.menu_id')
            ->get();



        return view('cupcake.sales.recipe-report', [
            'restaurant_data' => $restaurant_data,
            'menus' => $menus,
            'menu_items' => $menu_items,
        ]);
    }


    public function generateRecipeReport() {

        $restaurant_id = Auth::user()->restaurant_id;

        $restaurant_data = DB::table('restaurants')
            ->select('restaurants.*')
            ->where('restaurants_id', $restaurant_id)
            ->first();

        $menus = DB::table('menus')
            ->where('restaurants_id', $restaurant_id)
            ->get();

        $menu_items = DB::table('menu_items')
            ->select('menu_items.menu_item_id', 'menu_items.item_name', 'menu_items.ratio', 'menu_items.price', 'menus.menu_name')
            ->where('menus.restaurants_id', $restaurant_id)
            ->join('menus', 'menus.menu_id', 'menu_items.menu_id')
            ->get();



        return view('cupcake.sales.recipe-report', [
            'restaurant_data' => $restaurant_data,
            'menus' => $menus,
            'menu_items' => $menu_items,
        ]);
    }

}
