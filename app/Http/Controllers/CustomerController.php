<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use File;

class CustomerController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }


    public function index() {

        $restaurant_id = Auth::user()->restaurant_id;

        $customers = DB::table('customers')
            ->where('restaurant_id', $restaurant_id)
            ->get();

        return view('cupcake.customers.index', [
            'customers' => $customers
        ]);
    }


    public function addCustomer(Request $request) {

        $restaurant_id = Auth::user()->restaurant_id;
        $data = $request->input('params');

        $customer_id = DB::table('customers')->insertGetId([
                'restaurant_id' => $restaurant_id,
                'customer_name' => $data['customer_name'],
                'customer_email' => $data['customer_email'],
                'customer_address' => $data['customer_address'],
                'customer_phone' => $data['customer_phone'],
                'card_no' => $data['card_no'],
                'discount_percentage' => $data['discount_percentage'],
            ]);

        echo json_encode($customer_id);
    }


    public function editCustomer(Request $request) {

        $data = $request->input('params');
        $date = date('Y-m-d H:i:s');

        DB::table('customers')
            ->where('customer_id', $data['customer_id'])
            ->update([
                'customer_name' => $data['customer_name'],
                'customer_email' => $data['customer_email'],
                'customer_address' => $data['customer_address'],
                'customer_phone' => $data['customer_phone'],
                'card_no' => $data['card_no'],
                'discount_percentage' => $data['discount_percentage'],
                'updated_at' => $date,
            ]);

        return json_encode("success");
    }


    public function deleteCustomer(Request $request) {

        $data = $request->input('params');

        DB::table('customers')->where('customer_id', $data['customer_id'])->delete();

        return json_encode("success");
    }


    public function getCustomers() {

        $restaurant_id = Auth::user()->restaurant_id;

        $customers = DB::table('customers')
            ->where('restaurant_id', $restaurant_id)
            ->where('sync', 0)
            ->get();

        return json_encode($customers);




    }




}
