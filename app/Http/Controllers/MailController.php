<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use DB;

class MailController extends Controller {


    public function index() {

        $restaurant_id = 1;

        $date = date('Y-m-d');

        $from = $date . ' 00:00:00';
        $to = $date . ' 23:59:59';

        $expenses_details = DB::table('restaurants')
            ->where('restaurants_id', $restaurant_id)
            ->select(
                DB::raw("(SELECT SUM(total_bill)FROM orders WHERE restaurant_id=$restaurant_id AND order_status='paid' AND (created_at BETWEEN '$from' AND '$to')) as today_sale"),
                DB::raw("(SELECT COUNT(order_id)FROM orders WHERE restaurant_id=$restaurant_id AND order_status='paid' AND (created_at BETWEEN '$from' AND '$to')) as today_order"),
                DB::raw("(SELECT SUM(amount)FROM order_payments WHERE payment_method='cash' AND (order_payments.created_at BETWEEN '$from' AND '$to')) as cash_sale"),
                DB::raw("(SELECT SUM(amount)FROM order_payments WHERE payment_method='card' AND (order_payments.created_at BETWEEN '$from' AND '$to')) as card_sale"),
                DB::raw("(SELECT total_amount FROM cash_opening WHERE restaurant_id=$restaurant_id AND opening_date='$date') as cash_opening_total_amount"),
                DB::raw("(SELECT total_amount FROM cash_closing WHERE restaurant_id=$restaurant_id AND closing_date='$date') as cash_closing_total_amount"),
                DB::raw("(SELECT SUM(amount) FROM daily_expense WHERE restaurant_id=$restaurant_id AND debit_from='sales' AND (created_at BETWEEN '$from' AND '$to')) as sales_expense"),
                DB::raw("(SELECT SUM(amount) FROM daily_expense WHERE restaurant_id=$restaurant_id AND debit_from='petty_cash' AND (created_at BETWEEN '$from' AND '$to')) as petty_cash_expense"),
                DB::raw("(SELECT SUM(amount) FROM daily_expense WHERE restaurant_id=$restaurant_id AND transfer_to='sales' AND (created_at BETWEEN '$from' AND '$to')) as transfer_sales"),
                DB::raw("(SELECT SUM(amount) FROM daily_expense WHERE restaurant_id=$restaurant_id AND transfer_to='petty_cash' AND (created_at BETWEEN '$from' AND '$to')) as transfer_petty_cash"),
                DB::raw("(SELECT loan FROM restaurants WHERE restaurants_id=$restaurant_id) as loan_amount"),
                DB::raw("(SELECT SUM(accounts_payable) FROM loans_from WHERE restaurant_id=$restaurant_id) as total_loan"),
                DB::raw("(SELECT SUM(accounts_payable) FROM inv_supplier WHERE restaurant_id=$restaurant_id) as total_payable")
            )
            ->first();


        return view ('emails.report', [
            'expenses_details' => $expenses_details
        ]);
    }


    public function basic_email() {

        $data = array('name' => "Javed Nayeem");

        Mail::send(['text'=>'mail'], $data, function($message) {
            $message->to('abc@gmail.com', 'Tutorials Point')->subject
            ('Laravel Basic Testing Mail');
            $message->from('xyz@gmail.com','Virat Gandhi');
        });
        echo "Basic Email Sent. Check your inbox.";
    }


    public function emailDailyReport($restaurant_id=0) {

        if ($restaurant_id == 0) {

            $restaurants = DB::table('restaurants')->get();

            foreach ($restaurants as $restaurant) {

                $restaurant_id = $restaurant->restaurants_id;

                $this->emailDailyReport($restaurant_id);

            }

        }

        else {


            $emails = DB::table('emails')
                ->where('restaurant_id', $restaurant_id)
                ->get();

            $date = date('Y-m-d');

            $from = $date . ' 00:00:00';
            $to = $date . ' 23:59:59';

            $expenses_details = DB::table('restaurants')
                ->where('restaurants_id', $restaurant_id)
                ->select(
                    DB::raw("(SELECT SUM(total_bill)FROM orders WHERE restaurant_id=$restaurant_id AND order_status='paid' AND (created_at BETWEEN '$from' AND '$to')) as today_sale"),
                    DB::raw("(SELECT COUNT(order_id)FROM orders WHERE restaurant_id=$restaurant_id AND order_status='paid' AND (created_at BETWEEN '$from' AND '$to')) as today_order"),
                    DB::raw("(SELECT SUM(amount)FROM order_payments WHERE payment_method='cash' AND (order_payments.created_at BETWEEN '$from' AND '$to')) as cash_sale"),
                    DB::raw("(SELECT SUM(amount)FROM order_payments WHERE payment_method='card' AND (order_payments.created_at BETWEEN '$from' AND '$to')) as card_sale"),
                    DB::raw("(SELECT total_amount FROM cash_opening WHERE restaurant_id=$restaurant_id AND opening_date='$date') as cash_opening_total_amount"),
                    DB::raw("(SELECT total_amount FROM cash_closing WHERE restaurant_id=$restaurant_id AND closing_date='$date') as cash_closing_total_amount"),
                    DB::raw("(SELECT SUM(amount) FROM daily_expense WHERE restaurant_id=$restaurant_id AND debit_from='sales' AND (created_at BETWEEN '$from' AND '$to')) as sales_expense"),
                    DB::raw("(SELECT SUM(amount) FROM daily_expense WHERE restaurant_id=$restaurant_id AND debit_from='petty_cash' AND (created_at BETWEEN '$from' AND '$to')) as petty_cash_expense"),
                    DB::raw("(SELECT SUM(amount) FROM daily_expense WHERE restaurant_id=$restaurant_id AND transfer_to='sales' AND (created_at BETWEEN '$from' AND '$to')) as transfer_sales"),
                    DB::raw("(SELECT SUM(amount) FROM daily_expense WHERE restaurant_id=$restaurant_id AND transfer_to='petty_cash' AND (created_at BETWEEN '$from' AND '$to')) as transfer_petty_cash"),
                    DB::raw("(SELECT loan FROM restaurants WHERE restaurants_id=$restaurant_id) as loan_amount"),
                    DB::raw("(SELECT SUM(accounts_payable) FROM loans_from WHERE restaurant_id=$restaurant_id) as total_loan"),
                    DB::raw("(SELECT SUM(accounts_payable) FROM inv_supplier WHERE restaurant_id=$restaurant_id) as total_payable")
                )
                ->first();

            $today = date("F j, Y");
            $subject = 'CupCake - Daily Report: ' . $today;

            /*
                        $data = array( 'email' => 'javednayeemavi@gmail.com', 'subject' => $subject, 'expenses_details' => $expenses_details);

                        Mail::send('emails.report', $data, function($message) use ($data) {
                            $message->to($data['email'], 'CupCake POS')->subject($data['subject']);
                            $message->from('admin@cupcakepos.com','CupCake POS');
                            fedeisas/laravel-mail-css-inliner
                        });
            */


            foreach ($emails as $email) {

                $data = array( 'email' => $email->email, 'subject' => $subject, 'expenses_details' => $expenses_details);

                Mail::send('emails.report', $data, function($message) use ($data) {
                    $message->to($data['email'], 'CupCake POS')->subject($data['subject']);
                    $message->from('admin@cupcakepos.com','CupCake POS');
                });

            }

        }

    }


    public function attachment_email() {

        $data = array('name'=>"Virat Gandhi");

        Mail::send('mail', $data, function($message) {
            $message->to('abc@gmail.com', 'Tutorials Point')->subject
            ('Laravel Testing Mail with Attachment');
            $message->attach('C:\laravel-master\laravel\public\uploads\image.png');
            $message->attach('C:\laravel-master\laravel\public\uploads\test.txt');
            $message->from('xyz@gmail.com','Virat Gandhi');
        });
        echo "Email Sent with attachment. Check your inbox.";
    }


    public function emailReport(Request $request) {

        $data = $request->input('params');

        $restaurant_id = Auth::user()->restaurant_id;
        $report_data = [];


        $select_report_type = $data['select_report_type'];
        $start_date = $data['start_date'];
        $end_date = $data['end_date'];

        $from = $start_date . ' 00:00:00';
        $to = $end_date . ' 23:59:59';


        if ($select_report_type == 'invoice_wise_sale') {

            $report_data = [
                'report_type' => 'Invoice Wise Sales',
                'from' => $from,
                'to' => $to,
            ];

            $report = DB::table('orders')
                ->select('orders.*'
                    , DB::raw("(SELECT COUNT(order_details_id)FROM order_details WHERE order_id=orders.order_id AND item_status=1) as total_item")
                    , DB::raw("(SELECT name FROM users WHERE users.id=orders.waiter_id) as waiter_name")
                    , DB::raw("(SELECT customer_name FROM customers WHERE customers.customer_id=orders.customer_id) as customer_name")
                    , DB::raw("(SELECT table_name FROM tables WHERE tables.table_id=orders.table_id) as table_name"))
                ->where('restaurant_id', $restaurant_id)
                ->where('order_status', 'paid')
                ->whereBetween('created_at', array($from, $to))
                ->orderBy('order_id', 'DESC')
                ->get();

        }


        else if ($select_report_type == 'category_wise_sale') {

            $menu_id = $data['menu_id'];

            $report_data = [
                'report_type' => 'Category Wise Sale',
                'from' => $from,
                'to' => $to,
            ];

            if ($menu_id != 0) {

                $report = DB::table('menu_items')
                    ->select('menu_items.item_name', 'menu_items.price'
                        , DB::raw("(SELECT SUM(item_quantity) FROM order_details WHERE order_details.item_status=1 AND order_details.settle=1 AND menu_item_id=menu_items.menu_item_id AND (order_details.created_at BETWEEN '$from' AND '$to')) as item_quantity")
                    )
                    ->where('menu_id', $menu_id)
                    ->get();

            }

            else {

//                $report = DB::table('menu_items')
//                    ->select('menu_items.item_name', 'menu_items.price'
//                        , DB::raw("(SELECT SUM(item_quantity) FROM order_details WHERE menu_item_id=menu_items.menu_item_id AND (order_details.created_at BETWEEN '$from' AND '$to')) as item_quantity")
//                    )
//                    ->get();

                $report = DB::table('menus')
                    ->select('menus.menu_name as item_name'
                        , DB::raw("(SELECT SUM(item_quantity) FROM order_details WHERE order_details.item_status=1 AND order_details.settle=1 AND menu_item_id=menu_items.menu_item_id AND menu_items.menu_id=menus.menu_id AND(order_details.created_at BETWEEN '$from' AND '$to')) as item_quantity")
                    )
                    ->where('menus.restaurants_id', $restaurant_id)
                    ->join('menu_items', 'menu_items.menu_id', 'menus.menu_id')
                    ->get();
            }

        }


        else if ($select_report_type == 'item_wise_sale') {

            $item_id = $data['item_id'];

            $report_data = [
                'report_type' => 'Item Wise Sale',
                'from' => $from,
                'to' => $to,
            ];

            if ($item_id != 0) {

                $report = DB::table('menu_items')
                    ->select('menu_items.item_name', 'menu_items.ratio', 'order_details.*', 'orders.*', 'users.name as waiter_name')
                    ->where('menus.restaurants_id', $restaurant_id)
                    ->where('orders.restaurant_id', $restaurant_id)
                    ->where('orders.order_status', 'paid')
                    ->where('order_details.item_status', 1)
                    ->where('menu_items.menu_item_id', $item_id)
                    ->whereBetween('orders.created_at', array($from, $to))
                    ->orderBy('orders.order_id', 'DESC')
                    ->join('menus', 'menus.menu_id', 'menu_items.menu_id')
                    ->join('order_details', 'order_details.menu_item_id', 'menu_items.menu_item_id')
                    ->join('orders', 'orders.order_id', 'order_details.order_id')
                    ->join('users', 'users.id', 'orders.waiter_id')
                    ->get();

            }

            else {

                $report = DB::table('menu_items')
                    ->select('menu_items.item_name', 'menu_items.ratio', 'order_details.*', 'orders.*', 'users.name as waiter_name')
                    ->where('menus.restaurants_id', $restaurant_id)
                    ->where('orders.restaurant_id', $restaurant_id)
                    ->where('orders.order_status', 'paid')
                    ->where('order_details.item_status', 1)
                    ->whereBetween('orders.created_at', array($from, $to))
                    ->orderBy('orders.order_id', 'DESC')
                    ->join('menus', 'menus.menu_id', 'menu_items.menu_id')
                    ->join('order_details', 'order_details.menu_item_id', 'menu_items.menu_item_id')
                    ->join('orders', 'orders.order_id', 'order_details.order_id')
                    ->join('users', 'users.id', 'orders.waiter_id')
                    ->get();

            }

        }




        $restaurant_data = DB::table('restaurants')
            ->where('restaurants_id', $restaurant_id)
            ->first();


        $emails = DB::table('emails')
            ->where('restaurant_id', $restaurant_id)
            ->get();


        $subject = 'CupCake - ' . $report_data['report_type'] . ' ' . $start_date . ' To ' . $end_date;

        foreach ($emails as $email) {

            $data = array( 'email' => $email->email, 'subject' => $subject, 'report' => $report, 'report_data' => $report_data, 'restaurant_data' => $restaurant_data);

            Mail::send('emails.sales-report', $data, function($message) use ($data) {
                $message->to($data['email'], 'CupCake POS')->subject($data['subject']);
                $message->from('admin@cupcakepos.com','CupCake POS');
            });

        }



    }


    public function emailCurrentStock(Request $request) {

        $data = $request->input('params');

        $date = date('d/m/Y');

        $restaurant_id = Auth::user()->restaurant_id;
        $report_data = [];

        $category_id = 0;
        $product_id = 0;

        $select_report_type = $data['select_report_type'];

        if ($select_report_type == 'category_wise_report') $category_id = $data['category_id'];
        else if ($select_report_type == 'product_wise_report') $product_id = $data['product_id'];

        $main_store_id = getMainStoreId($restaurant_id);



        if ($select_report_type == "0") {

            $report_data = [
                'report_type' => 'All Current Stock'
            ];

            $report = DB::table('inv_product_store')
                ->select('inv_product_store.quantity', 'inv_product.product_code','inv_product.product_name', 'inv_product.avg_price', 'inv_product_category.category_name', 'inv_units.unit_name',
                    DB::raw("(SELECT SUM(quantity) FROM recipe_consumption WHERE product_id=inv_product_store.product_id) as consumption_quantity"),
                    DB::raw("(SELECT SUM(quantity) FROM inv_damage_products WHERE product_id=inv_product_store.product_id) as damage_quantity")
                )
                ->where('store_id', $main_store_id)
                ->join('inv_product', 'inv_product.product_id', 'inv_product_store.product_id')
                ->join('inv_product_category', 'inv_product_category.category_id', 'inv_product.category_id')
                ->join('inv_units', 'inv_units.unit_id', 'inv_product.unit_id')
                ->get();

        }

        else if ($select_report_type == 'product_wise_report') {

            $report_data = [
                'report_type' => 'Product Wise Stock'
            ];

            $report = DB::table('inv_product_store')
                ->select('inv_product_store.quantity', 'inv_product.product_code','inv_product.product_name', 'inv_product.avg_price', 'inv_product_category.category_name', 'inv_units.unit_name',
                    DB::raw("(SELECT SUM(quantity) FROM recipe_consumption WHERE product_id=inv_product_store.product_id) as consumption_quantity"),
                    DB::raw("(SELECT SUM(quantity) FROM inv_damage_products WHERE product_id=inv_product_store.product_id) as damage_quantity")
                )
                ->where('inv_product_store.store_id', $main_store_id)
                ->where('inv_product_store.product_id', $product_id)
                ->join('inv_product', 'inv_product.product_id', 'inv_product_store.product_id')
                ->join('inv_product_category', 'inv_product_category.category_id', 'inv_product.category_id')
                ->join('inv_units', 'inv_units.unit_id', 'inv_product.unit_id')
                ->get();


        }

        else if ($select_report_type == 'category_wise_report') {

            $report_data = [
                'report_type' => 'Category Wise Stock'
            ];

            $report = DB::table('inv_product_store')
                ->select('inv_product_store.quantity', 'inv_product.product_code','inv_product.product_name', 'inv_product.avg_price', 'inv_product_category.category_name', 'inv_units.unit_name',
                    DB::raw("(SELECT SUM(quantity) FROM recipe_consumption WHERE product_id=inv_product_store.product_id) as consumption_quantity"),
                    DB::raw("(SELECT SUM(quantity) FROM inv_damage_products WHERE product_id=inv_product_store.product_id) as damage_quantity")
                )
                ->where('inv_product_store.store_id', $main_store_id)
                ->where('inv_product.category_id', $category_id)
                ->join('inv_product', 'inv_product.product_id', 'inv_product_store.product_id')
                ->join('inv_product_category', 'inv_product_category.category_id', 'inv_product.category_id')
                ->join('inv_units', 'inv_units.unit_id', 'inv_product.unit_id')
                ->get();

        }


        $restaurant_data = DB::table('restaurants')
            ->where('restaurants_id', $restaurant_id)
            ->first();


        $emails = DB::table('emails')
            ->where('restaurant_id', $restaurant_id)
            ->get();


        $subject = 'CupCake - Inventory Report: ' . $report_data['report_type'] . ' - ' . $date;

        foreach ($emails as $email) {

            $data = array( 'email' => $email->email, 'subject' => $subject, 'report' => $report, 'report_data' => $report_data, 'restaurant_data' => $restaurant_data);

            Mail::send('emails.current-stock-report', $data, function($message) use ($data) {
                $message->to($data['email'], 'CupCake POS')->subject($data['subject']);
                $message->from('admin@cupcakepos.com','CupCake POS');
            });

        }



    }


    public function emailPurchaseHistory(Request $request) {

        $restaurant_id = Auth::user()->restaurant_id;
        $data = $request->input('params');

        $report_data = [];

        $from = $data['start_date'];
        $to = $data['end_date'];

        $category_id = 0;
        $product_id = 0;

        $select_report_type = $data['select_report_type'];

        if ($select_report_type == 'category_wise_report') $category_id = $data['category_id'];
        else if ($select_report_type == 'product_wise_report') $product_id = $data['product_id'];

        if ($select_report_type == "0") {

            $report_data = [
                'report_type' => 'All Purchase History',
                'from' => $from,
                'to' => $to,
            ];

            $report = DB::table('inv_product_purchase_in')
                ->select('inv_product_purchase_in.*', 'inv_purchase_invoice.invoice_no', 'inv_purchase_invoice.purchase_date', 'inv_product.product_name', 'inv_product_category.category_name', 'inv_units.unit_name')
                ->join('inv_purchase_invoice', 'inv_purchase_invoice.invoice_id', 'inv_product_purchase_in.purchase_invoice_id')
                ->join('inv_product', 'inv_product.product_id', 'inv_product_purchase_in.product_id')
                ->join('inv_product_category', 'inv_product_category.category_id', 'inv_product.category_id')
                ->join('inv_units', 'inv_units.unit_id', 'inv_product.unit_id')
                ->where('inv_purchase_invoice.restaurant_id', $restaurant_id)
                ->whereBetween('inv_purchase_invoice.purchase_date', array($from, $to))
                ->get();

        }

        else if ($select_report_type == 'product_wise_report') {

            $report_data = [
                'report_type' => 'Product Wise Purchase History',
                'from' => $from,
                'to' => $to,
            ];

            $report = DB::table('inv_product_purchase_in')
                ->select('inv_product_purchase_in.*', 'inv_purchase_invoice.invoice_no', 'inv_purchase_invoice.purchase_date', 'inv_product.product_name', 'inv_product_category.category_name', 'inv_units.unit_name')
                ->join('inv_purchase_invoice', 'inv_purchase_invoice.invoice_id', 'inv_product_purchase_in.purchase_invoice_id')
                ->join('inv_product', 'inv_product.product_id', 'inv_product_purchase_in.product_id')
                ->join('inv_product_category', 'inv_product_category.category_id', 'inv_product.category_id')
                ->join('inv_units', 'inv_units.unit_id', 'inv_product.unit_id')
                ->where('inv_purchase_invoice.restaurant_id', $restaurant_id)
                ->where('inv_product_purchase_in.product_id', $product_id)
                ->whereBetween('inv_purchase_invoice.purchase_date', array($from, $to))
                ->get();


        }

        else if ($select_report_type == 'category_wise_report') {

            $report_data = [
                'report_type' => 'Category Wise Purchase History',
                'from' => $from,
                'to' => $to,
            ];

            $report = DB::table('inv_product_purchase_in')
                ->select('inv_product_purchase_in.*', 'inv_purchase_invoice.invoice_no', 'inv_purchase_invoice.purchase_date', 'inv_product.product_name', 'inv_product_category.category_name', 'inv_units.unit_name')
                ->join('inv_purchase_invoice', 'inv_purchase_invoice.invoice_id', 'inv_product_purchase_in.purchase_invoice_id')
                ->join('inv_product', 'inv_product.product_id', 'inv_product_purchase_in.product_id')
                ->join('inv_product_category', 'inv_product_category.category_id', 'inv_product.category_id')
                ->join('inv_units', 'inv_units.unit_id', 'inv_product.unit_id')
                ->where('inv_purchase_invoice.restaurant_id', $restaurant_id)
                ->where('inv_product.category_id', $category_id)
                ->whereBetween('inv_purchase_invoice.purchase_date', array($from, $to))
                ->get();

        }

        $restaurant_data = DB::table('restaurants')
            ->where('restaurants_id', $restaurant_id)
            ->first();


        $emails = DB::table('emails')
            ->where('restaurant_id', $restaurant_id)
            ->get();

        $subject = 'CupCake - Inventory Report: ' . $report_data['report_type'] . ' - ' . $from . ' To ' . $to;

        foreach ($emails as $email) {

            $data = array( 'email' => $email->email, 'subject' => $subject, 'report' => $report, 'report_data' => $report_data, 'restaurant_data' => $restaurant_data);

            Mail::send('emails.purchase-history', $data, function($message) use ($data) {
                $message->to($data['email'], 'CupCake POS')->subject($data['subject']);
                $message->from('admin@cupcakepos.com','CupCake POS');
            });

        }



    }


    public function emailDamageHistory(Request $request) {

        $restaurant_id = Auth::user()->restaurant_id;
        $data = $request->input('params');

        $report_data = [];

        $from = $data['start_date'] . ' 00:00:00';
        $to = $data['end_date'] . ' 23:59:59';

        $category_id = 0;
        $product_id = 0;

        $select_report_type = $data['select_report_type'];

        if ($select_report_type == 'category_wise_report') $category_id = $data['category_id'];
        else if ($select_report_type == 'product_wise_report') $product_id = $data['product_id'];

        if ($select_report_type == "0") {

            $report_data = [
                'report_type' => 'All Damage History',
                'from' => $from,
                'to' => $to,
            ];

            $report = DB::table('inv_damage_products')
                ->select('inv_damage_products.*', 'inv_product.product_name', 'inv_product_category.category_name', 'inv_units.unit_name')
                ->join('inv_damage_entry', 'inv_damage_entry.damage_id', 'inv_damage_products.damage_id')
                ->join('inv_product', 'inv_product.product_id', 'inv_damage_products.product_id')
                ->join('inv_product_category', 'inv_product_category.category_id', 'inv_product.category_id')
                ->join('inv_units', 'inv_units.unit_id', 'inv_product.unit_id')
                ->where('inv_damage_entry.restaurant_id', $restaurant_id)
                ->whereBetween('inv_damage_products.created_at', array($from, $to))
                ->get();

        }

        else if ($select_report_type == 'product_wise_report') {

            $report_data = [
                'report_type' => 'Product Wise Damage History',
                'from' => $from,
                'to' => $to,
            ];

            $report = DB::table('inv_damage_products')
                ->select('inv_damage_products.*', 'inv_product.product_name', 'inv_product_category.category_name', 'inv_units.unit_name')
                ->join('inv_damage_entry', 'inv_damage_entry.damage_id', 'inv_damage_products.damage_id')
                ->join('inv_product', 'inv_product.product_id', 'inv_damage_products.product_id')
                ->join('inv_product_category', 'inv_product_category.category_id', 'inv_product.category_id')
                ->join('inv_units', 'inv_units.unit_id', 'inv_product.unit_id')
                ->where('inv_damage_entry.restaurant_id', $restaurant_id)
                ->where('inv_damage_products.product_id', $product_id)
                ->whereBetween('inv_damage_products.created_at', array($from, $to))
                ->get();


        }

        else if ($select_report_type == 'category_wise_report') {

            $report_data = [
                'report_type' => 'Category Wise Damage History',
                'from' => $from,
                'to' => $to,
            ];

            $report = DB::table('inv_damage_products')
                ->select('inv_damage_products.*', 'inv_product.product_name', 'inv_product_category.category_name', 'inv_units.unit_name')
                ->join('inv_damage_entry', 'inv_damage_entry.damage_id', 'inv_damage_products.damage_id')
                ->join('inv_product', 'inv_product.product_id', 'inv_damage_products.product_id')
                ->join('inv_product_category', 'inv_product_category.category_id', 'inv_product.category_id')
                ->join('inv_units', 'inv_units.unit_id', 'inv_product.unit_id')
                ->where('inv_damage_entry.restaurant_id', $restaurant_id)
                ->where('inv_product.category_id', $category_id)
                ->whereBetween('inv_damage_products.created_at', array($from, $to))
                ->get();

        }

        $restaurant_data = DB::table('restaurants')
            ->where('restaurants_id', $restaurant_id)
            ->first();


        $emails = DB::table('emails')
            ->where('restaurant_id', $restaurant_id)
            ->get();

        $subject = 'CupCake - Inventory Report: ' . $report_data['report_type'] . ' - ' . $data['start_date'] . ' To ' . $data['end_date'];

        foreach ($emails as $email) {

            $data = array( 'email' => $email->email, 'subject' => $subject, 'report' => $report, 'report_data' => $report_data, 'restaurant_data' => $restaurant_data);

            Mail::send('emails.damage-history', $data, function($message) use ($data) {
                $message->to($data['email'], 'CupCake POS')->subject($data['subject']);
                $message->from('admin@cupcakepos.com','CupCake POS');
            });

        }



    }

}