<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use DB;
use DateTime;

use App;
use Session;

error_reporting(0);

class HomeController extends Controller {

    public function __construct() {
        //$this->middleware('auth');
    }


    public function index() {
        //return view('home');
        //return view('cupcake.dashboard.index');
        return redirect('/dashboard');
    }


    public function changeLanguage(Request $request) {

        $user_id = Auth::user()->id;
        $data = $request->input('params');
        $locale = $data['locale'];
        Session::put('locale', $locale);

        DB::table('users')
            ->where('id', $user_id)
            ->update(['locale' => $data['locale']]);

        return json_encode('success');
    }


    public function test () {
//
//        $restaurant_id = Auth::user()->restaurant_id;
//
////        $setmenu_items = DB::table('setmenu_items')->get();
////
////        echo "'setmenu_items' => [<br>";
////        foreach ($setmenu_items as $item) {
////            echo "['item_name' => '$item->item_name', 'menu_item_id' => '$item->menu_item_id', 'restaurant_id' => '$item->restaurant_id'],<br>";
////        }
////        echo "],";
//
//
//        echo getRestaurantTimestamp($restaurant_id);


        return view('cupcake.test.index');

    }

}
