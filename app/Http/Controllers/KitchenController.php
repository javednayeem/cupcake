<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use File;
use phpDocumentor\Reflection\Types\Null_;

class KitchenController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }


    public function index() {

        $restaurant_id = Auth::user()->restaurant_id;

        $from = date("Y-m-d") . ' 00:00:00';
        $to = date("Y-m-d") . ' 23:59:59';

        $settings = getSettings($restaurant_id);

        $orders = DB::table('orders')
            ->select('orders.*', 'tables.table_name', 'users.name')
            ->where('orders.restaurant_id', $restaurant_id)
            ->where('orders.order_status', 'placed')
            ->where('orders.served', 0)
            ->whereBetween('orders.created_at', array($from, $to))
            ->join('tables', 'tables.table_id', 'orders.table_id')
            ->join('users', 'users.id', 'orders.waiter_id')
            ->paginate(50);


        $order = DB::table('order_queue')
            ->where('order_queue.restaurant_id', $restaurant_id)
            ->first();

        if ($order == Null) $count = 0;
        else $count = 1;

        if ($count == 0) {

            $order_data =  [
                'count' => $count,
                'order_id' => $count
            ];

        }
        else {

            $order_data =  [
                'count' => $count,
                'order_id' => $order->order_id
            ];

        }

        return view('cupcake.order.kitchen-queue', [
            'settings' => $settings,
            'orders' => $orders,
            'order_data' => $order_data,
        ]);
    }


    public function orderServed(Request $request) {

        $order_id = $request->input('order_id');
        $restaurant_id = Auth::user()->restaurant_id;

        DB::table('orders')
            ->where('order_id', $order_id)
            ->update(['served' => 1]);

        DB::table('order_queue')->where('restaurant_id', $restaurant_id)->delete();

        DB::table('order_queue')->insert([
            'order_id' => $order_id,
            'restaurant_id' => $restaurant_id,
        ]);

        return json_encode('success');
    }


    public function viewQueue() {

        $restaurant_id = Auth::user()->restaurant_id;

        $order = DB::table('order_queue')
            ->where('order_queue.restaurant_id', $restaurant_id)
            ->first();

        if ($order == Null) $count = 0;
        else $count = 1;

        if ($count == 0) {

            $order_data =  [
                'count' => $count,
                'order_id' => $count
            ];

        }
        else {

            $order_data =  [
                'count' => $count,
                'order_id' => $order->order_id
            ];

        }

        return view('cupcake.order.view-queue', [
            'order_data' => $order_data,
        ]);
    }


    public function updateOrderQueue() {

        $restaurant_id = Auth::user()->restaurant_id;

        $order = DB::table('order_queue')
            ->where('order_queue.restaurant_id', $restaurant_id)
            ->first();

        if ($order == Null) $count = 0;
        else $count = 1;

        if ($count == 0) {

            $order_data =  [
                'count' => $count,
                'order_id' => $count
            ];

        }
        else {

            $order_data =  [
                'count' => $count,
                'order_id' => $order->order_id
            ];

        }

        return json_encode($order_data);
    }


    public function clearOrderQueue() {

        $restaurant_id = Auth::user()->restaurant_id;
        DB::table('order_queue')->where('restaurant_id', $restaurant_id)->delete();
        return json_encode('success');

    }

}
