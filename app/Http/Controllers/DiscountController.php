<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

class DiscountController extends Controller {


    public function __construct() {
        $this->middleware('auth');
    }


    public function index() {

        $date = date('Y-m-d');

        $restaurant_id = Auth::user()->restaurant_id;
        $created_by = Auth::user()->id;

        $discount_circular = DB::table('discount_circular')
            ->select('discount_circular.*', 'users.name')
            ->where('discount_circular.restaurant_id', $restaurant_id)
            ->join('users', 'users.id', 'discount_circular.created_by')
            ->get();


        return view('cupcake.menu.discount-circular', [
            'discount_circular' => $discount_circular
        ]);
    }


    public function addDiscountCircular(Request $request) {

        $restaurant_id = Auth::user()->restaurant_id;
        $created_by = Auth::user()->id;

        $data = $request->input('params');


        $start_date = $data['start_date'];
        $start_date = date_create($start_date);
        $start_date = date_format($start_date,"Y-m-d");

        $end_date = $data['end_date'];
        $end_date = date_create($end_date);
        $end_date = date_format($end_date,"Y-m-d");

        $circular_id = DB::table('discount_circular')->insertGetId([
                'circular_name' => $data['circular_name'],
                'discount' => $data['discount'],
                'discount_type' => $data['discount_type'],
                'start_date' => $start_date,
                'end_date' => $end_date,
                'created_by' => $created_by,
                'restaurant_id' => $restaurant_id
            ]);

        return json_encode($circular_id);
    }


    public function addDiscount(Request $request) {
        $data = $request->input('params');

        DB::table('orders')
            ->where('order_id', $data['order_id'])
            ->update([
                'discount' => $data['discount'],
                'discount_percentage' => $data['discount_percentage'],
                'discount_amount_type' => $data['discount_amount_type'],
                'discount_reference' => $data['discount_reference']
            ]);

        return json_encode('success');
    }


    public function deleteCircular(Request $request) {

        $data = $request->input('params');

        DB::table('discount_circular')->where('circular_id', $data['circular_id'])->delete();

        return json_encode("success");

    }




}
