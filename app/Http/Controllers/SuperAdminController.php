<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Illuminate\Support\Facades\Validator;

class SuperAdminController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }


    public function index() {

        app_settings();

        $bank_cards = DB::table('bank_cards')->get();

        $users = DB::table('users')
            ->select('users.*', 'restaurants.restaurants_name')
            ->join('restaurants', 'restaurants.restaurants_id', '=', 'users.restaurant_id')
            ->get();

        $lookups = DB::table('lookup')->get();
        $license_periods = DB::table('license_periods')->get();
        $role_lookup = DB::table('role_lookup')->get();

        $restaurant_data = DB::table('restaurants')
            ->select('restaurants.*', 'users.name', 'users.user_img')
            ->rightJoin('users', 'users.id', '=', 'restaurants.creator_id')
            ->where('restaurants.restaurants_id', '>', 0)
            ->get();

        $settings_data = DB::table('app_settings')
            ->select('setting_key', 'setting_value')
            ->get();

        $settings = array();
        foreach($settings_data as $row) $settings[$row->setting_key] = $row->setting_value;

        $restaurants = DB::table('restaurants')
            ->select(DB::raw("(SELECT COUNT(restaurants_id)FROM restaurants) as total_restaurants")
                , DB::raw("(SELECT SUM(total_bill) FROM orders WHERE order_status='paid') as total_revenue")
                , DB::raw("(SELECT COUNT(customer_id)FROM customers) as customers")
                , DB::raw("(SELECT COUNT(id)FROM users) as stuffs"))
            ->first();



        return view('cupcake.superadmin.index', [
            'bank_cards' => $bank_cards,
            'restaurant_data' => $restaurant_data,
            'settings' => $settings,
            'restaurants' => $restaurants,
            'users' => $users,
            'lookups' => $lookups,
            'license_periods' => $license_periods,
            'role_lookup' => $role_lookup,

        ]);
    }


    public function addBankCard(Request $request) {
        $data = $request->input('params');

        $bank_card_id = DB::table('bank_cards')->insertGetId([
            'card_name' => $data['card_name'],
            'bank_name' => $data['card_name'],
        ]);
        return json_encode($bank_card_id);
    }


    public function editBankCard(Request $request) {

        $data = $request->input('params');

        DB::table('bank_cards')
            ->where('card_id', $data['card_id'])
            ->update([
                'card_name' => $data['card_name'],
            ]);

        return json_encode('success');
    }


    public function deleteBankCard(Request $request) {
        $data = $request->input('params');

        DB::table('bank_cards')->where('card_id', $data['card_id'])->delete();

        return json_encode("success");
    }


    public function addPaymentMethod(Request $request) {

        $data = $request->input('params');

        $method_id = DB::table('payment_methods')->insertGetId([
            'method_name' => $data['method_name'],
        ]);

        return json_encode($method_id);
    }


    public function editPaymentMethod(Request $request) {

        $data = $request->input('params');

        DB::table('payment_methods')
            ->where('method_id', $data['method_id'])
            ->update([
                'method_name' => $data['method_name'],
            ]);

        return json_encode('success');
    }


    public function deletePaymentMethod(Request $request) {
        $data = $request->input('params');

        DB::table('payment_methods')->where('method_id', $data['method_id'])->delete();

        return json_encode("success");
    }


    public function createRestaurant(Request $request) {
        $data = $request->input('params');
        $restaurants_code = check_restaurant_name($data['restaurants_name']);

        $restaurant_id = DB::table('restaurants')->insertGetId([
            'restaurants_name' => $data['restaurants_name'],
            'restaurants_code' => $restaurants_code,
            'address' => $data['address'],
            'area' => $data['area'],
            'city' => $data['city'],
            'phone_number' => $data['phone_number'],
            'email' => $data['email'],
            'website' => $data['website'],
            'license_id' => 0,
            'creator_id' => Auth::user()->id
        ]);

        setupNewRestaurant($restaurant_id);

        return json_encode($restaurant_id);
    }


    public function createUser(Request $request) {
        $creator_id = Auth::user()->id;
        $data= $request->input('params');
        $date = date('Y-m-d H:i:s');

        $rules = array(
            'name' => 'required|string|max:255',
            'email' => 'required|string|max:255|unique:users',
            'password' => 'required|string|min:6',
        );

        $validator = Validator::make($data, $rules);
        if ($validator->passes()) {

            $restaurant = DB::table('restaurants')
                ->select('inventory', 'license')
                ->where('restaurants_id', $data['restaurant_id'])
                ->first();

            $user_id = DB::table('users')->insertGetId([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'role' => $data['role'],
                'restaurant_id' => $data['restaurant_id'],
                'creator_id' => $creator_id,
                'inventory' => $restaurant->inventory,
                'license' => $restaurant->license,
                'created_at' => $date,
                'updated_at' => $date,
            ]);

            return json_encode($user_id);
        }
        else {
            echo json_encode(array('errors' => $validator->getMessageBag()->toArray()));
        }
    }


    public function saveAppSettings(Request $request) {

        $user_id = Auth::user()->id;

        if ($request->hasFile('bg_image')) {

            $file = $request->file('bg_image');
            $this->validate($request, [
                'bg_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:10000',
            ]);

            $bg_image = 'bg_image' . '.' .$file->getClientOriginalExtension();
            $destinationPath = 'images';
            $file->move($destinationPath, $bg_image);

            echo $bg_image;

            DB::table('app_settings')
                ->where('setting_key', 'bg_image')
                ->update([
                    'setting_value' => $bg_image,
                    'user_id' => $user_id
                ]);

        }

        $key_index = array_keys($request->all());
        $keys = $request->all();

        for ($i=0; $i<count($key_index); $i++) {

            DB::table('app_settings')
                ->where('setting_key', $key_index[$i])
                ->update([
                    'setting_value' => $keys[$key_index[$i]],
                    'user_id' => $user_id
                ]);

        }

        return redirect('/app-settings');

    }


    public function addLookup(Request $request) {
        $data = $request->input('params');
        $creator_id = Auth::user()->id;

        $lookup_id = DB::table('lookup')->insertGetId([
            'lookup_name' => $data['lookup_name'],
            'created_by' => $creator_id,
        ]);
        return json_encode($lookup_id);
    }


    public function changeAccountsPermission(Request $request) {
        $data = $request->input('params');

        DB::table('restaurants')
            ->where('restaurants_id', $data['restaurants_id'])
            ->update([
                'accounts' => $data['accounts']
            ]);

        DB::table('users')
            ->where('restaurant_id', $data['restaurants_id'])
            ->update([
                'accounts' => $data['accounts']
            ]);

        return json_encode('success');
    }


    public function changeInvPermission(Request $request) {
        $data = $request->input('params');

        DB::table('restaurants')
            ->where('restaurants_id', $data['restaurants_id'])
            ->update([
                'inventory' => $data['inventory']
            ]);

        DB::table('users')
            ->where('restaurant_id', $data['restaurants_id'])
            ->update([
                'inventory' => $data['inventory']
            ]);

        return json_encode('success');
    }


    public function changeKitchenQueuePermission(Request $request) {
        $data = $request->input('params');

        DB::table('restaurants')
            ->where('restaurants_id', $data['restaurants_id'])
            ->update([
                'kitchen_queue' => $data['kitchen_queue']
            ]);

        DB::table('users')
            ->where('restaurant_id', $data['restaurants_id'])
            ->update([
                'kitchen_queue' => $data['kitchen_queue']
            ]);

        return json_encode('success');
    }


    public function getRestaurantInfo(Request $request) {
        $data = $request->input('params');

        $restaurant = DB::table('restaurants')
            ->where('restaurants_id', $data['restaurants_id'])
            ->first();

        return json_encode($restaurant);
    }


    public function editUserRole(Request $request) {

        $data = $request->input('params');

        DB::table('users')
            ->where('id', $data['user_id'])
            ->update([
                'role' => $data['role']
            ]);

        return json_encode('success');
    }


    public function editUserRestaurant(Request $request) {

        $data = $request->input('params');

        DB::table('users')
            ->where('id', $data['user_id'])
            ->update([
                'restaurant_id' => $data['restaurant_id']
            ]);

        return json_encode('success');
    }


    public function createRestaurantLayout() {

        return view('cupcake.sa-settings.create-restaurant', [


        ]);
    }


    public function bankCardsLayout() {

        $bank_cards = DB::table('bank_cards')->get();
        $payment_methods = DB::table('payment_methods')->get();

        return view('cupcake.sa-settings.bank-cards', [
            'bank_cards' => $bank_cards,
            'payment_methods' => $payment_methods,


        ]);
    }


    public function restaurants() {

        $restaurants = DB::table('restaurants')->get();

        $license_periods = DB::table('license_periods')->get();

        return view('cupcake.sa-settings.restaurants', [
            'restaurants' => $restaurants,
            'license_periods' => $license_periods,


        ]);
    }


    public function viewRestaurant($restaurant_id) {

        $restaurants = DB::table('restaurants')->get();

        $restaurant_data = DB::table('restaurants')
            ->select('restaurants.*', 'users.name', 'users.user_img')
            ->join('users', 'users.id', 'restaurants.creator_id')
            ->where('restaurants.restaurants_id', $restaurant_id)
            ->first();

        $licenses = DB::table('licenses')
            ->where('licenses.restaurants_id', $restaurant_id)
            ->select('licenses.*', 'restaurants.restaurants_name', 'users.name')
            ->join('restaurants', 'restaurants.restaurants_id', 'licenses.restaurants_id')
            ->join('users', 'users.id', 'licenses.created_by')
            ->get();

        $license_periods = DB::table('license_periods')->get();


        $settings_data = DB::table('restaurant_settings')
            ->select('setting_key', 'setting_value')
            ->where('restaurant_id', $restaurant_id)
            ->get();

        $settings = array();
        foreach($settings_data as $row) $settings[$row->setting_key] = $row->setting_value;

        return view('cupcake.sa-settings.view-restaurant', [
            'restaurants' => $restaurants,
            'restaurant_data' => $restaurant_data,
            'licenses' => $licenses,
            'license_periods' => $license_periods,
            'settings' => $settings,
        ]);
    }


    public function createUserLayout() {

        $role_lookup = DB::table('role_lookup')->get();

        $restaurant_data = DB::table('restaurants')
            ->select('restaurants.*', 'users.name', 'users.user_img')
            ->join('users', 'users.id', '=', 'restaurants.creator_id')
            ->where('restaurants.restaurants_id', '>', 0)
            ->get();


        $users = DB::table('users')
            ->select('users.*', 'restaurants.restaurants_name')
            ->where('users.status', 1)
            ->join('restaurants', 'restaurants.restaurants_id', 'users.restaurant_id')
            ->get();

        return view('cupcake.sa-settings.create-user', [
            'role_lookup' => $role_lookup,
            'restaurant_data' => $restaurant_data,
            'users' => $users,


        ]);
    }


    public function appSettingsLayout() {

        $lookups = DB::table('lookup')->get();
        $role_lookup = DB::table('role_lookup')->get();
        $report_type = DB::table('report_type')->get();

        $settings_data = DB::table('app_settings')
            ->select('setting_key', 'setting_value')
            ->get();

        $settings = array();
        foreach($settings_data as $row) $settings[$row->setting_key] = $row->setting_value;



        return view('cupcake.sa-settings.app-settings', [
            'lookups' => $lookups,
            'role_lookup' => $role_lookup,
            'settings' => $settings,
            'report_type' => $report_type,

        ]);
    }


    public function lookUpLayout() {

        $lookups = DB::table('lookup')->get();
        $role_lookup = DB::table('role_lookup')->get();
        $report_type = DB::table('report_type')->get();
        $order_types = DB::table('order_types')->get();

        return view('cupcake.sa-settings.lookup', [
            'lookups' => $lookups,
            'role_lookup' => $role_lookup,
            'report_type' => $report_type,
            'order_types' => $order_types,
        ]);
    }


    public function invSettingsLayout() {

        $restaurant_data = DB::table('restaurants')
            ->select('restaurants.*', 'users.name', 'users.user_img')
            ->join('users', 'users.id', '=', 'restaurants.creator_id')
            ->where('restaurants.restaurants_id', '>', 0)
            ->get();

        return view('cupcake.sa-settings.inv-settings', [
            'restaurant_data' => $restaurant_data,


        ]);
    }


    public function dataSettingsLayout() {

        $tables = DB::select('SHOW TABLES');
        $db_name = 'Tables_in_' . DB::connection()->getDatabaseName();

        $users = DB::table('users')->get();
        $restaurants = DB::table('restaurants')->get();
        $licenses = DB::table('licenses')->get();
        $menu_items = DB::table('menu_items')->get();



//        echo '<pre>';
//        var_dump($tables);
//        echo '<pre>';

        return view('cupcake.sa-settings.data-settings', [
            'tables' => $tables,
            'db_name' => $db_name,
            'users' => $users,
            'restaurants' => $restaurants,
            'licenses' => $licenses,
            'menu_items' => $menu_items,
        ]);
    }


    public function scheduleScriptsLayout() {

        $schedule_scripts = DB::table('schedule_scripts')->orderBy('created_at', 'desc')->paginate(20);


        return view('cupcake.sa-settings.schedule-scripts', [
            'schedule_scripts' => $schedule_scripts
        ]);
    }


    public function applicationMenu() {

        $user_id = Auth::user()->id;

        $application_menu = DB::table('application_menu as a')
            ->select('a.*',
                DB::raw("(SELECT menu_name FROM application_menu as b WHERE b.menu_id=a.parent_menu) as parent_menu_name"),
                DB::raw("(SELECT COUNT(menu_id) FROM application_menu as c WHERE a.menu_id=c.parent_menu) as parent_count"),
                DB::raw("(SELECT permission FROM application_menu_permission as d WHERE a.menu_id=d.menu_id AND d.user_id=$user_id) as permission")
            )
            ->get();


//        echo '<pre>';
//        var_dump($application_menu);
//        echo '<pre>';

        return view('cupcake.sa-settings.application-menu', [
            'application_menu' => $application_menu,
        ]);
    }


    public function saveApplicationMenu(Request $request) {

        $data = $request->input('params');

        $menu_id = DB::table('application_menu')->insertGetId([
            'menu_name' => $data['menu_name'],
            'parent_menu' => $data['parent_menu'],
        ]);

        return json_encode($menu_id);
    }


    public function editApplicationMenu(Request $request) {

        $data = $request->input('params');

        DB::table('application_menu')
            ->where('menu_id', $data['menu_id'])
            ->update([
                'menu_name' => $data['menu_name'],
                'parent_menu' => $data['parent_menu'],
            ]);

        return json_encode('success');
    }


    public function deleteApplicationMenu(Request $request) {

        $data = $request->input('params');

        DB::table('application_menu')->where('menu_id', $data['menu_id'])->delete();

        return json_encode("success");
    }


    public function editRestaurantFeatures(Request $request) {

        $data = $request->input('params');

        DB::table('restaurants')
            ->where('restaurants_id', $data['restaurants_id'])
            ->update([
                'accounts' => $data['accounts'],
                'inventory' => $data['inventory'],
                'kitchen_queue' => $data['kitchen_queue'],
                'online_order' => $data['online_order'],
            ]);

        DB::table('users')
            ->where('restaurant_id', $data['restaurants_id'])
            ->update([
                'accounts' => $data['accounts'],
                'inventory' => $data['inventory'],
                'kitchen_queue' => $data['kitchen_queue'],
                'online_order' => $data['online_order'],
            ]);

        return json_encode('success');
    }


    public function editRestaurantSettings(Request $request) {

        $data = $request->input('params');
        $keys = array_keys($data);

        for ($i=0; $i<count($keys); $i++) {

            DB::table('restaurant_settings')
                ->where('setting_key', $keys[$i])
                ->where('restaurant_id', $data['restaurants_id'])
                ->update([
                    'setting_value' => $data[$keys[$i]]
                ]);

        }

        return json_encode('success');

    }



    public function addOrderType(Request $request) {

        $data = $request->input('params');

        $type_id = DB::table('order_types')->insertGetId([
            'type_name' => $data['type_name'],
        ]);

        return json_encode($type_id);
    }


    public function editOrderType(Request $request) {

        $data = $request->input('params');

        DB::table('order_types')
            ->where('type_id', $data['type_id'])
            ->update([
                'type_name' => $data['type_name']
            ]);

        return json_encode('success');
    }


    public function deleteOrderType(Request $request) {

        $data = $request->input('params');

        DB::table('order_types')->where('type_id', $data['type_id'])->delete();

        return json_encode("success");
    }




}
