<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Illuminate\Support\Facades\Validator;

class SuperAdminOrderController extends Controller {


    public function __construct() {
        $this->middleware('superadmin');
    }


    public function index($restaurant_id=0) {

        if ($restaurant_id == 0) {

            $orders = DB::table('orders')
                ->select('orders.*' , 'restaurants.restaurants_name'
                    , DB::raw("(SELECT COUNT(order_details_id)FROM order_details WHERE order_id=orders.order_id AND item_status=1) as total_item")
                    , DB::raw("(SELECT name FROM users WHERE users.id=orders.waiter_id) as waiter_name")
                    , DB::raw("(SELECT customer_name FROM customers WHERE customers.customer_id=orders.customer_id) as customer_name")
                    , DB::raw("(SELECT table_name FROM tables WHERE tables.table_id=orders.table_id) as table_name"))
                ->join('restaurants', 'restaurants.restaurants_id', 'orders.restaurant_id')
                ->orderBy('orders.order_id', 'DESC')
                ->paginate(20);

        }

        else {

            $orders = DB::table('orders')
                ->select('orders.*' , 'restaurants.restaurants_name'
                    , DB::raw("(SELECT COUNT(order_details_id)FROM order_details WHERE order_id=orders.order_id AND item_status=1) as total_item")
                    , DB::raw("(SELECT name FROM users WHERE users.id=orders.waiter_id) as waiter_name")
                    , DB::raw("(SELECT customer_name FROM customers WHERE customers.customer_id=orders.customer_id) as customer_name")
                    , DB::raw("(SELECT table_name FROM tables WHERE tables.table_id=orders.table_id) as table_name"))
                ->where('orders.restaurant_id', $restaurant_id)
                ->join('restaurants', 'restaurants.restaurants_id', 'orders.restaurant_id')
                ->orderBy('orders.order_id', 'DESC')
                ->paginate(20);

        }


        return view('cupcake.sa-settings.view-order', [
            'orders' => $orders,
        ]);
    }


    public function viewOrder(Request $request) {

        $order_status = $request->input('order_status');
        $start_date = $request->input('start_date');
        $end_date = $request->input('end_date');

        $from = $start_date . ' 00:00:00';
        $to = $end_date . ' 23:59:59';

        if ($order_status == "0") {

            $orders = DB::table('orders')
                ->select('orders.*' , 'restaurants.restaurants_name'
                    , DB::raw("(SELECT COUNT(order_details_id)FROM order_details WHERE order_id=orders.order_id AND item_status=1) as total_item")
                    , DB::raw("(SELECT name FROM users WHERE users.id=orders.waiter_id) as waiter_name")
                    , DB::raw("(SELECT customer_name FROM customers WHERE customers.customer_id=orders.customer_id) as customer_name")
                    , DB::raw("(SELECT table_name FROM tables WHERE tables.table_id=orders.table_id) as table_name"))
                ->join('restaurants', 'restaurants.restaurants_id', 'orders.restaurant_id')
                ->whereBetween('orders.created_at', array($from, $to))
                ->orderBy('orders.order_id', 'DESC')
                ->get();

        }

        else {

            $orders = DB::table('orders')
                ->select('orders.*' , 'restaurants.restaurants_name'
                    , DB::raw("(SELECT COUNT(order_details_id)FROM order_details WHERE order_id=orders.order_id AND item_status=1) as total_item")
                    , DB::raw("(SELECT name FROM users WHERE users.id=orders.waiter_id) as waiter_name")
                    , DB::raw("(SELECT customer_name FROM customers WHERE customers.customer_id=orders.customer_id) as customer_name")
                    , DB::raw("(SELECT table_name FROM tables WHERE tables.table_id=orders.table_id) as table_name"))
                ->join('restaurants', 'restaurants.restaurants_id', 'orders.restaurant_id')
                ->whereBetween('orders.created_at', array($from, $to))
                ->where('orders.order_status', $order_status)
                ->orderBy('orders.order_id', 'DESC')
                ->get();

        }


        return view('cupcake.sa-settings.view-order', [
            'orders' => $orders,
        ]);



    }


    public function editOrderLayout($order_id) {

        $order = DB::table('orders')
            ->select('orders.*'
                , DB::raw("(SELECT COUNT(order_details_id)FROM order_details WHERE order_id=orders.order_id AND item_status=1) as total_item")
                , DB::raw("(SELECT name FROM users WHERE users.id=orders.waiter_id) as waiter_name")
                , DB::raw("(SELECT customer_name FROM customers WHERE customers.customer_id=orders.customer_id) as customer_name")
                , DB::raw("(SELECT table_name FROM tables WHERE tables.table_id=orders.table_id) as table_name"))
            ->where('orders.order_id', $order_id)
            ->first();

        $order_details = DB::table('order_details')
            ->where('order_id', $order_id)
            ->get();

        $order_modifiers = DB::table('order_modifiers')
            ->select('order_modifiers.*', 'modifiers.modifier_name', 'modifiers.price')
            ->where('order_modifiers.order_id', $order_id)
            ->join('modifiers', 'modifiers.modifier_id', 'order_modifiers.modifier_id')
            ->get();

        $order_payments = DB::table('order_payments')
            ->select('order_payments.*'
                , DB::raw("(SELECT card_name FROM bank_cards WHERE card_id=order_payments.card_id) as card_name"))
            ->where('order_payments.order_id', $order_id)
            ->get();

        $restaurant_id = $order->restaurant_id;

        $restaurant_data = DB::table('restaurants')
            ->select('restaurants.*',
                DB::raw("(SELECT COUNT(workperiod_id)FROM work_period WHERE restaurant_id=$restaurant_id AND status=1) as work_period_status"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='guest_bill') as guest_bill"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='waiter_order_void') as waiter_order_void"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='kitchen_type') as kitchen_type"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='receipt_footer') as receipt_footer"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='order_receipt_debug') as order_receipt_debug"),
                DB::raw("(SELECT setting_value FROM restaurant_settings WHERE restaurant_id=$restaurant_id AND setting_key='menu_font_size') as menu_font_size"),
                DB::raw("(SELECT setting_value FROM app_settings WHERE setting_key='receipt_company') as receipt_company"),
                DB::raw("(SELECT setting_value FROM app_settings WHERE setting_key='receipt_phone') as receipt_phone")
            )
            ->where('restaurants_id', $restaurant_id)
            ->first();

        $user_data = DB::table('users')
            ->where('restaurant_id', $restaurant_id)
            ->where('role', 'waiter')
            ->where('status', 1)
            ->get();


        $customer_data = DB::table('customers')
            ->where('restaurant_id', $restaurant_id)
            ->get();


        $table_data = DB::table('tables')
            ->where('restaurant_id', $restaurant_id)
            ->get();


        $bank_cards = DB::table('bank_cards')->get();
        $order_types = DB::table('order_types')->get();
        $payment_methods = DB::table('payment_methods')->get();



        return view('cupcake.sa-settings.edit-order', [
            'order' => $order,
            'order_details' => $order_details,
            'order_modifiers' => $order_modifiers,
            'order_payments' => $order_payments,
            'restaurant_data' => $restaurant_data,
            'user_data' => $user_data,
            'customer_data' => $customer_data,
            'bank_cards' => $bank_cards,
            'table_data' => $table_data,
            'order_types' => $order_types,
            'payment_methods' => $payment_methods,
        ]);
    }


    public function editOrder(Request $request) {

        $data = $request->input('params');
        $order_id = $data['order_id'];
        $settle = 0;

        if ($data['order_status'] == 'paid') $settle = 1;

        DB::table('orders')
            ->where('order_id', $order_id)
            ->update([
                'waiter_id' => $data['waiter_id'],
                'customer_id' => $data['customer_id'],
                'table_id' => $data['table_id'],
                'order_type' => $data['order_type'],
                'order_status' => $data['order_status'],
                'discount' => $data['discount'],
                'discount_percentage' => $data['discount_percentage'],
                'discount_amount_type' => $data['discount_amount_type'],
                'discount_reference' => $data['discount_reference'],
                'created_at' => $data['created_at'] . ' ' . $data['created_time'],
            ]);

        if ($data['recalculate_bill'] == 1) reCalculateBill($order_id);


        DB::table('order_details')
            ->where('order_id', $order_id)
            ->update([
                'settle' => $settle
            ]);

        return json_encode($order_id);
    }


    public function recalculateOrder(Request $request) {

        $data = $request->input('params');
        $order_id = $data['order_id'];

        reCalculateBill($order_id);

        return json_encode($order_id);
    }


    public function deleteOrder(Request $request) {

        $data = $request->input('params');
        $order_id = $data['order_id'];

        DB::table('orders')->where('order_id', $order_id)->delete();
        DB::table('order_details')->where('order_id', $order_id)->delete();
        DB::table('order_modifiers')->where('order_id', $order_id)->delete();
        DB::table('order_payments')->where('order_id', $order_id)->delete();
        DB::table('recipe_consumption')->where('order_id', $order_id)->delete();

        $activity = "Delete order,  order id: $order_id";
        createActivityLog($activity);


        return json_encode('success');
    }


    public function editOrderPayment(Request $request) {

        $data = $request->input('params');
        $order_id = $data['order_id'];

        $order = DB::table('orders')->where('order_id', $order_id)->first();

        $restaurant_id = $order->restaurant_id;
        $created_at = $order->created_at;
        $updated_at = $order->updated_at;


        $this->deleteOrderPayments($order_id);

        if (isset($data["payment_methods"])) {

            for ($i = 0; $i < count($data["payment_methods"]); $i++) {

                $payment_method = $data["payment_methods"][$i][0];
                $amount = $data["payment_methods"][$i][1];
                $bank_card = $data["payment_methods"][$i][2]==''?0:$data["payment_methods"][$i][2];
                $bank_card_name = $data["payment_methods"][$i][3];
                $card_number = $data["payment_methods"][$i][4];

                $payment_id = DB::table('order_payments')->insertGetId([
                    'order_id' => $order_id,
                    'payment_method' => $payment_method,
                    'amount' => $amount,
                    'card_id' => $bank_card,
                    'card_number' => $card_number,
                    'restaurant_id' => $restaurant_id,
                    'created_at' => $created_at,
                    'updated_at' => $updated_at,
                ]);

            }
        }

        $activity = "Edited order payments for order id: $order_id";
        createActivityLog($activity);

        return json_encode($order_id);
    }


    private function deleteOrderPayments($order_id) {

        DB::table('order_payments')
            ->where('order_id', $order_id)
            ->delete();

        $activity = "Delete order payments for order id: $order_id";
        createActivityLog($activity);

    }


    public function editOrderCreatedAt(Request $request) {

        $data = $request->input('params');

        $start_order_id = $data['start_order_id'];
        $end_order_id = $data['end_order_id'];
        $created_at = $data['created_at'];




        /*
         * Edit orders table
         */


        $orders = DB::table('orders')
            ->where('order_id', '>=' , $start_order_id)
            ->where('order_id', '<=' , $end_order_id)
            ->get();

        foreach ($orders as $order) {

            $order_created_at = $order->created_at;
            $array = explode(' ', $order_created_at);

            $new_created_at = $created_at . ' ' . $array[1];

            DB::table('orders')
                ->where('order_id', $order->order_id)
                ->update([
                    'created_at' => $new_created_at
                ]);





            /*
             * Edit order_payments table
             */

            $order_payments = DB::table('order_payments')
                ->where('order_id' , $order->order_id)
                ->get();

            foreach ($order_payments as $payment) {

                $order_created_at = $payment->created_at;
                $array = explode(' ', $order_created_at);

                $new_created_at = $created_at . ' ' . $array[1];

                DB::table('order_payments')
                    ->where('payment_id', $payment->payment_id)
                    ->update([
                        'created_at' => $new_created_at
                    ]);

            }





            /*
             * Edit order_details table
             */


            $order_details = DB::table('order_details')
                ->where('order_id' , $order->order_id)
                ->get();

            foreach ($order_details as $item) {

                $order_created_at = $item->created_at;
                $array = explode(' ', $order_created_at);

                $new_created_at = $created_at . ' ' . $array[1];

                DB::table('order_details')
                    ->where('order_details_id', $item->order_details_id)
                    ->update([
                        'created_at' => $new_created_at
                    ]);

            }
        }

        $activity = "Fixed order work period from order id: $start_order_id to $end_order_id";
        createActivityLog($activity);

        return json_encode('success');
    }





}
