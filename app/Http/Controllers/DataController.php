<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Illuminate\Support\Facades\Schema;

class DataController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }


    public function truncateTable(Request $request) {

        $role = Auth::user()->role;
        $data = $request->input('params');

        if ($role == 'superadmin') {

            for ($i=0; $i<count($data["table_names"]); $i++) {
                DB::table($data["table_names"][$i])->truncate();
            }

        }

        return json_encode('success');
    }


    public function dropTable(Request $request) {

        $role = Auth::user()->role;
        $data = $request->input('params');

        if ($role == 'superadmin') {

            for ($i=0; $i<count($data["table_names"]); $i++) {
                Schema::dropIfExists($data["table_names"][$i]);
                //delete from mytable where myvalue like 'findme%';
                DB::delete("delete from migrations where migration like '%". $data['table_names'][$i] . "'");
            }

        }

        return json_encode('success');
    }

}
