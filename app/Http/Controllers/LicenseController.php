<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Illuminate\Support\Facades\Validator;

class LicenseController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }


    public function licenseAssign(Request $request) {
        $data = $request->input('params');
        $created_by = Auth::user()->id;

        $license_key = licenseKeyGenerate();

        $license_start_date = date("Y-m-d", strtotime($data['license_start_date']));
        $license_end_date = date("Y-m-d", strtotime($data['license_end_date']));

        $license_id = DB::table('licenses')->insertGetId([
            'restaurants_id' => $data['restaurants_id'],
            'license_key' => $license_key,
            'license_start_date' => $license_start_date,
            'license_end_date' => $license_end_date,
            'license_type' => $data['license_type'],
            'created_by' => $created_by,

        ]);

        DB::table('licenses')
            ->where('license_id', '<>', $license_id)
            ->where('restaurants_id', $data['restaurants_id'])
            ->update([
                'status' => 0
            ]);

        DB::table('restaurants')
            ->where('restaurants_id', $data['restaurants_id'])
            ->update([
                'license_id' => $license_id
            ]);

        checkRstaurantLicense($data['restaurants_id']);

        return json_encode($license_id);
    }


    public function saLicensesLayout($restaurants_id=0) {

        $license_periods = DB::table('license_periods')->get();

        $restaurants = DB::table('restaurants')
            ->where('restaurants_code', '<>', 'cupcake')
            ->get();

        if ($restaurants_id == 0) {

            $licenses = DB::table('licenses')
                ->select('licenses.*', 'restaurants.restaurants_name', 'users.name')
                ->join('restaurants', 'restaurants.restaurants_id', '=', 'licenses.restaurants_id')
                ->join('users', 'users.id', '=', 'licenses.created_by')
                ->get();

            if (count($licenses) == 0) $license_data = array('paid_license_count' => 0, 'trial_license_count' => 0);
            else {

                $license_data = DB::table('licenses')
                    ->select(
                        DB::raw("(SELECT COUNT(license_id) FROM licenses WHERE license_type='paid') as paid_license_count")
                        , DB::raw("(SELECT COUNT(license_id) FROM licenses WHERE license_type='trial') as trial_license_count"))
                    ->first();

                $license_data = array('paid_license_count' => $license_data->paid_license_count, 'trial_license_count' => $license_data->trial_license_count);
            }

        }

        else {

            $licenses = DB::table('licenses')
                ->where('licenses.restaurants_id', $restaurants_id)
                ->select('licenses.*', 'restaurants.restaurants_name', 'users.name')
                ->join('restaurants', 'restaurants.restaurants_id', '=', 'licenses.restaurants_id')
                ->join('users', 'users.id', '=', 'licenses.created_by')
                ->get();

            if (count($licenses) == 0) $license_data = array('paid_license_count' => 0, 'trial_license_count' => 0);
            else {

                $license_data = DB::table('licenses')
                    ->select(
                        DB::raw("(SELECT COUNT(license_id) FROM licenses WHERE license_type='paid' AND restaurants_id=$restaurants_id) as paid_license_count")
                        , DB::raw("(SELECT COUNT(license_id) FROM licenses WHERE license_type='trial' AND restaurants_id=$restaurants_id) as trial_license_count"))
                    ->first();

                $license_data = array('paid_license_count' => $license_data->paid_license_count, 'trial_license_count' => $license_data->trial_license_count);
            }

        }





        return view('cupcake.sa-settings.licenses', [
            'restaurants' => $restaurants,
            'licenses' => $licenses,
            'license_data' => $license_data,
            'license_periods' => $license_periods,
        ]);
    }


    public function deleteLicense(Request $request) {

        $data = $request->input('params');

        $license_data = DB::table('licenses')
            ->where('license_id', $data['license_id'])
            ->first();

        DB::table('licenses')->where('license_id', $data['license_id'])->delete();

        DB::table('restaurants')
            ->where('license_id', $data['license_id'])
            ->update([
                'license' => 0,
                'license_id' => 0,
            ]);

        if ($license_data->status == 1) {

            $restaurants_id = intval($license_data->restaurants_id);
            checkRstaurantLicense($restaurants_id);

        }



        return json_encode('success');
    }


    public function activateLicense(Request $request) {

        $data = $request->input('params');

        $restaurants_id = DB::table('licenses')
            ->where('license_id', $data['license_id'])
            ->first();

        $restaurants_id = $restaurants_id->restaurants_id;

        DB::table('restaurants')
            ->where('restaurants_id', $restaurants_id)
            ->update([
                'license_id' => $data['license_id'],
            ]);

        DB::table('licenses')
            ->where('license_id', '<>', $data['license_id'])
            ->where('restaurants_id', $restaurants_id)
            ->update([
                'status' => 0
            ]);

        DB::table('licenses')
            ->where('license_id', $data['license_id'])
            ->update([
                'status' => 1
            ]);

        checkRstaurantLicense($restaurants_id);

        return json_encode('success');
    }

}
