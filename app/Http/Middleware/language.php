<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;
use App;
use Session;

class language {

    public function handle($request, Closure $next) {

        if (Session::has('locale')) App::setLocale(Session::get('locale'));
        else {
            $locale = Auth::user()->locale;
            App::setLocale($locale);
        }

        return $next($request);
    }
}
