<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class inventory {

    public function handle($request, Closure $next) {
        if (Auth::user()->inventory == 1) return $next($request);
        else return redirect('/dashboard');
    }
}
