<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class staff {

    public function handle($request, Closure $next) {

        if (Auth::user()->role != "customer") return $next($request);
        else return redirect('/online-order');

    }
}
