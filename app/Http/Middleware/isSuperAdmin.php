<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;




class isSuperAdmin {

    public function handle($request, Closure $next) {



        if (Auth::user()->role == "superadmin") return $next($request);
        else if (Auth::user()->role == "admin") return redirect('/dashboard');
        else return redirect('/order');
    }
}
