<?php

$temp_bg_color_previous_color = -1;

function setup() {

    $count = DB::table('users')->count();

    if ($count == 0) {

        $date = date('Y-m-d H:i:s');

        $users = json_decode(json_encode(config('global.users')));
        $restaurants = json_decode(json_encode(config('global.restaurants')));

        $user_id = 1;
        $restaurant_id = 1;

        foreach ($users as $user) {

            if ($count++ == 0) {

                $user_id = DB::table('users')->insertGetId([
                    'name' => $user->name,
                    'email' => $user->email,
                    'password' => $user->password,
                    'role' => $user->role,
                    'restaurant_id' => $user->restaurant_id,
                    'creator_id' => $user->creator_id,
                    'inventory' => $user->inventory,
                    'license' => $user->license,
                    'created_at' => $date,
                    'updated_at' => $date
                ]);

                foreach ($restaurants as $restaurant) {

                    $restaurant_id = DB::table('restaurants')->insertGetId([
                        'restaurants_name' => $restaurant->restaurants_name,
                        'restaurants_code' => $restaurant->restaurants_code,
                        'creator_id' => $user_id,
                        'license_id' => 0,
                    ]);

                    DB::table('users')
                        ->where('id', $user_id)
                        ->update(['restaurant_id' => $restaurant_id]);

                }

            }

            else {

                DB::table('users')->insert([
                    'name' => $user->name,
                    'email' => $user->email,
                    'password' => bcrypt($user->password),
                    'role' => $user->role,
                    'restaurant_id' => $restaurant_id,
                    'creator_id' => $user_id,
                    'inventory' => $user->inventory,
                    'license' => $user->license,
                    'created_at' => $date,
                    'updated_at' => $date
                ]);

            }

        }


        app_settings($user_id);
        lookup_init($user_id);
        setupNewRestaurant($restaurant_id, $user_id);

        app('App\Http\Controllers\BackUpController')->createBackup();

    }


}


function app_settings($user_id=1) {

    $count = DB::table('app_settings')->count();

    if ($count == 0) {

        $app_settings = json_decode(json_encode(config('global.app_settings')));

        foreach ($app_settings as $setting) {
            DB::table('app_settings')->insert([
                'setting_key' => $setting->setting_key,
                'setting_value' => $setting->setting_value,
                'user_id' => $user_id,
            ]);
        }
    }
}


function lookup_init($user_id=1) {

    $count = DB::table('lookup')->count();
    if ($count == 0) {
        $lookup = json_decode(json_encode(config('global.lookup')));
        foreach ($lookup as $item) {
            DB::table('lookup')->insert([
                'lookup_name' => $item->lookup_name,
                'created_by' => $user_id,
            ]);
        }
    }


    $count = DB::table('license_periods')->count();
    if ($count == 0) {
        $license_periods = json_decode(json_encode(config('global.license_periods')));
        foreach ($license_periods as $item) {
            DB::table('license_periods')->insert([
                'days' => $item->days,
                'duration_str' => $item->duration_str,
                'created_by' => $user_id,
            ]);
        }
    }


    $count = DB::table('role_lookup')->count();
    if ($count == 0) {
        $role_lookup = json_decode(json_encode(config('global.role_lookup')));
        foreach ($role_lookup as $item) {
            DB::table('role_lookup')->insert([
                'role_name' => $item->role_name,
                'created_by' => $user_id,
            ]);
        }
    }


    $count = DB::table('bank_cards')->count();
    if ($count == 0) {
        $bank_cards = json_decode(json_encode(config('global.bank_cards')));
        foreach ($bank_cards as $item) {
            DB::table('bank_cards')->insert([
                'card_name' => $item->card_name,
                'bank_name' => $item->bank_name,
            ]);
        }
    }


    $count = DB::table('status_lookup')->count();
    if ($count == 0) {
        $status_lookup = json_decode(json_encode(config('global.status_lookup')));
        foreach ($status_lookup as $item) {
            DB::table('status_lookup')->insert([
                'status_name' => $item->status_name,
                'created_by' => $user_id,
            ]);
        }
    }


    $count = DB::table('report_type')->count();
    if ($count == 0) {
        $report_type = json_decode(json_encode(config('global.report_type')));
        foreach ($report_type as $item) {
            DB::table('report_type')->insert([
                'report_type' => $item->report_type,
                'report_value' => $item->report_value,
                'report_name' => $item->report_name,
            ]);
        }
    }


    $count = DB::table('application_menu')->count();
    if ($count == 0) {
        $application_menu = json_decode(json_encode(config('global.application_menu')));
        foreach ($application_menu as $item) {
            DB::table('application_menu')->insert([
                'menu_name' => $item->menu_name,
                'url' => $item->url,
                'parent_menu' => $item->parent_menu,
                'class' => $item->class,
            ]);
        }
    }


    $count = DB::table('payment_methods')->count();
    if ($count == 0) {
        $payment_methods = json_decode(json_encode(config('global.payment_methods')));
        foreach ($payment_methods as $method) {
            DB::table('payment_methods')->insert([
                'method_name' => $method->method_name,
            ]);
        }
    }


    $count = DB::table('order_types')->count();
    if ($count == 0) {
        $order_types = json_decode(json_encode(config('global.order_types')));
        foreach ($order_types as $type) {
            DB::table('order_types')->insert([
                'type_name' => $type->type_name,
            ]);
        }
    }
}


function randomPassword($length=10) {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < $length; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}


function licenseKeyGenerate($length=2) {
    $alphabet = "ABCDEFGHIJKLMNOPQRSTUWXYZ";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < $length; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass) . time(); //turn the array into a string
}


function getRstaurantName() {

    $restaurant_id = Auth::user()->restaurant_id;

    $restaurant_name = DB::table('restaurants')
        ->select('restaurants_name' )
        ->where('restaurants_id', '=', $restaurant_id)
        ->first();

    return ($restaurant_name->restaurants_name == "" || $restaurant_name->restaurants_name == Null) ? "CupCake" : $restaurant_name->restaurants_name;

}


function getRstaurantVAT() {

    $restaurant_id = Auth::user()->restaurant_id;

    $vat = DB::table('restaurants')
        ->select('vat_percentage' )
        ->where('restaurants_id', '=', $restaurant_id)
        ->first();

    return $vat->vat_percentage;

}


function getRstaurantLogo() {

    $restaurant_id = Auth::user()->restaurant_id;

    $restaurant_img = DB::table('restaurants')
        ->select('restaurant_img' )
        ->where('restaurants_id', '=', $restaurant_id)
        ->first();

    return ($restaurant_img->restaurant_img == "" || $restaurant_img->restaurant_img == Null) ? "default_restaurant.png" : $restaurant_img->restaurant_img;
}


function getRstaurantCode() {

    $restaurant_id = Auth::user()->restaurant_id;

    $restaurant_code = DB::table('restaurants')
        ->select('restaurants_code' )
        ->where('restaurants_id', '=', $restaurant_id)
        ->first();

    $restaurant_code = $restaurant_code->restaurants_code;
    return $restaurant_code;
}


function timeDifference($reserve_time) {
    date_default_timezone_set('Asia/Dhaka');
    $datetime1 = new DateTime($reserve_time);
    $t=time();
    $datetime2 = new DateTime(date('Y-m-d H:i:s', $t));
    $oDiff = $datetime1->diff($datetime2);
    $hours = ($oDiff->days * 24) + $oDiff->h;

    if ($hours == 0) return $oDiff->i . "m";
    else return $hours . "H:" . $oDiff->i . "m";
}


function check_restaurant_name ($restaurant_name) {
    $name = strtolower($restaurant_name);
    $restaurants_code = str_replace(' ', '', $name);
    return $restaurants_code;
}


function dateDifference($date_1 , $date_2 , $differenceFormat = '%d Days %h Hours %i Minutes' ) {
    $datetime1 = date_create($date_1);
    $datetime2 = date_create($date_2);
    $interval = date_diff($datetime1, $datetime2);
    return $interval->format($differenceFormat);
}


function duration ($date_1, $date_2, $differenceFormat = '%a Days') {
    $datetime1 = new DateTime($date_1);
    $datetime2 = new DateTime($date_2);
    $interval = $datetime1->diff($datetime2);
    return $interval->format($differenceFormat);
}


function checkLicenseDuration ($date_1, $date_2, $differenceFormat = '%a Days') {

    $today = date("Y-m-d");
    $flag =  (($today >= $date_1) && ($today <= $date_2));

    $datetime1 = new DateTime($date_1);
    $datetime2 = new DateTime($date_2);
    $interval = $datetime1->diff($datetime2);
    if ($flag == 0) return 0;
    else return $interval->format($differenceFormat);
}


function reCalculateBill($order_id) {

    checkDiscountCircular($order_id);

    $orders = DB::table('orders')
        ->where('orders.order_id', $order_id)
        ->select('orders.discount', 'orders.discount_percentage', 'orders.discount_amount_type', 'restaurants.*')
        ->join('restaurants', 'orders.restaurant_id', 'restaurants.restaurants_id')
        ->first();


    $order_details = DB::table('order_details')
        ->select('order_details.*', 'menu_items.discount_available')
        ->where('order_details.order_id', $order_id)
        ->where('order_details.item_status', 1)
        ->join('menu_items', 'menu_items.menu_item_id', 'order_details.menu_item_id')
        ->get();


    $order_modifiers = DB::table('order_modifiers')
        ->where('order_modifiers.order_id', $order_id)
        ->select('order_modifiers.*', 'modifiers.price', 'modifiers.modifier_vat')
        ->join('modifiers', 'modifiers.modifier_id', '=', 'order_modifiers.modifier_id')
        ->get();


    $order_discount = isset($orders->discount)?intval($orders->discount):0;
    $order_discount_percentage = isset($orders->discount_percentage)?$orders->discount_percentage:0;
    $discount_amount_type = $orders->discount_amount_type;

    if ($discount_amount_type == 'cash') $order_discount_percentage = 0;
    else $order_discount = 0;

    $price_including_vat = isset($orders->price_including_vat)?intval($orders->price_including_vat):0;
    $vat_after_discount = isset($orders->vat_after_discount)?intval($orders->vat_after_discount):0;
    $vat_percentage = isset($orders->vat_percentage)?$orders->vat_percentage:0;
    $service_charge = isset($orders->service_charge)?$orders->service_charge:0;
    $sd_percentage = isset($orders->sd_percentage)?$orders->sd_percentage:0;
    $service_charge_vat_percentage = isset($orders->service_charge_vat_percentage)?$orders->service_charge_vat_percentage:0;
    $sd_vat_percentage = isset($orders->sd_vat_percentage)?$orders->sd_vat_percentage:0;


    /*
        echo "price_including_vat: " . $price_including_vat . '<br><br>';
        echo "vat_after_discount: " . $vat_after_discount . '<br><br>';
        echo "vat_percentage: " . $vat_percentage . '<br><br>';
        echo "service_charge: " . $service_charge . '<br><br>';
        echo "sd_percentage: " . $sd_percentage . '<br><br>';
        echo "service_charge_vat_percentage: " . $service_charge_vat_percentage . '<br><br>';
        echo "sd_vat_percentage: " . $sd_vat_percentage . '<br><br>';
    */


    $order_bill = 0;
    $order_vat = 0;
    $order_service_charge = 0;
    $order_sd_charge = 0;
    $order_total_bill = 0;
    //$order_discount = 0;

    foreach ($order_details as $item) {

        $discount_available = isset($item->discount_available)?$item->discount_available:0;

        $this_item_price = intval($item->item_price - $item->item_discount) * intval($item->item_quantity);

        $this_item_discount = 0;

        if ($discount_amount_type == 'percentage') {

            $this_item_discount = ($this_item_price * floatval($order_discount_percentage))/100.0;
            $this_item_price_with_discount = intval($item->item_price - ($item->item_discount + $this_item_discount)) * intval($item->item_quantity);

        }

        //echo "this_item_discount: " . $this_item_discount . '<br><br>';

        //if ($vat_after_discount == 0) $order_bill += $this_item_price_with_discount;
        //else $order_bill += $this_item_price;

        $order_bill += $this_item_price;

        $this_item_vat = ($this_item_price * floatval($item->item_vat))/100.0;
        $order_vat += $this_item_vat;

        $order_discount += $this_item_discount;

        //echo "this_item_price_with_discount: " . $this_item_price_with_discount . '<br><br>';
        //echo "this_item_price_with_discount: " . $this_item_price_with_discount . '<br><br>';

    }


    foreach ($order_modifiers as $item) {

        $this_item_price = intval($item->price) * intval($item->quantity);
        $order_bill += $this_item_price;

        $this_item_vat = ($this_item_price * intval($item->modifier_vat))/100.0;
        $order_vat += $this_item_vat;
    }

    $order_service_charge = round(($order_bill * $service_charge)/100.0);
    $order_sd_charge = round(($order_bill * $sd_percentage)/100.0);
    $order_service_charge_vat_percentage = round(($order_service_charge * $service_charge_vat_percentage)/100.0);
    $order_sd_vat_percentage = round(($order_sd_charge * $sd_vat_percentage)/100.0);

    $order_total_bill = floatval((round($order_bill) - round($order_discount)) + $order_service_charge + $order_sd_charge + $order_service_charge_vat_percentage + $order_sd_vat_percentage);

    if (!$price_including_vat) $order_total_bill +=  round($order_vat);

    /*
        echo "order_bill: " . $order_bill . '<br><br>';
        echo "order_discount: " . $order_discount . '<br><br>';
        echo "order_vat: " . $order_vat . '<br><br>';
        echo "order_service_charge: " . $order_service_charge . '<br><br>';
        echo "order_sd_charge: " . $order_sd_charge . '<br><br>';
        echo "order_service_charge_vat_percentage: " . $order_service_charge_vat_percentage . '<br><br>';
        echo "order_sd_vat_percentage: " . $order_sd_vat_percentage . '<br><br>';
        echo "order_total_bill: " . $order_total_bill . '<br><br>';
    */

    DB::table('orders')
        ->where('order_id', $order_id)
        ->update([
            'bill' => $order_bill,
            'vat' => round($order_vat),
            'service_charge' => $order_service_charge,
            'sd_charge' => $order_sd_charge,
            'service_charge_vat' => $order_service_charge_vat_percentage,
            'sd_vat' => $order_sd_vat_percentage,
            'discount' => $order_discount,
            'total_bill' =>  round($order_total_bill)
        ]);

}


function reCalculateOnlineBill($order_id) {

    $orders = DB::table('online_orders')
        ->where('online_orders.order_id', $order_id)
        ->select('online_orders.discount', 'online_orders.discount_percentage', 'online_orders.discount_amount_type', 'restaurants.*')
        ->join('restaurants', 'online_orders.restaurant_id', 'restaurants.restaurants_id')
        ->first();


    $order_details = DB::table('online_order_details')
        ->select('online_order_details.*')
        ->where('online_order_details.order_id', $order_id)
        ->where('online_order_details.item_status', 1)
        ->join('menu_items', 'menu_items.menu_item_id', 'online_order_details.menu_item_id')
        ->get();

    $order_discount = isset($orders->discount)?intval($orders->discount):0;
    $order_discount_percentage = isset($orders->discount_percentage)?$orders->discount_percentage:0;
    $discount_amount_type = $orders->discount_amount_type;

    if ($discount_amount_type == 'cash') $order_discount_percentage = 0;
    else $order_discount = 0;

    $price_including_vat = isset($orders->price_including_vat)?intval($orders->price_including_vat):0;
    $vat_after_discount = isset($orders->vat_after_discount)?intval($orders->vat_after_discount):0;
    $vat_percentage = isset($orders->vat_percentage)?$orders->vat_percentage:0;
    $service_charge = isset($orders->service_charge)?$orders->service_charge:0;
    $sd_percentage = isset($orders->sd_percentage)?$orders->sd_percentage:0;
    $service_charge_vat_percentage = isset($orders->service_charge_vat_percentage)?$orders->service_charge_vat_percentage:0;
    $sd_vat_percentage = isset($orders->sd_vat_percentage)?$orders->sd_vat_percentage:0;


    /*
        echo "price_including_vat: " . $price_including_vat . '<br><br>';
        echo "vat_after_discount: " . $vat_after_discount . '<br><br>';
        echo "vat_percentage: " . $vat_percentage . '<br><br>';
        echo "service_charge: " . $service_charge . '<br><br>';
        echo "sd_percentage: " . $sd_percentage . '<br><br>';
        echo "service_charge_vat_percentage: " . $service_charge_vat_percentage . '<br><br>';
        echo "sd_vat_percentage: " . $sd_vat_percentage . '<br><br>';
    */


    $order_bill = 0;
    $order_vat = 0;
    $order_service_charge = 0;
    $order_sd_charge = 0;
    $order_total_bill = 0;
    //$order_discount = 0;

    foreach ($order_details as $item) {

        $this_item_price = intval($item->item_price) * intval($item->item_quantity);

        $this_item_discount = 0;

        if ($discount_amount_type == 'percentage') {

            $this_item_discount = ($this_item_price * floatval($order_discount_percentage))/100.0;
            $this_item_price_with_discount = intval($item->item_price - ($item->item_discount + $this_item_discount)) * intval($item->item_quantity);

        }


        $order_bill += $this_item_price;

        $this_item_vat = ($this_item_price * floatval($item->item_vat))/100.0;
        $order_vat += $this_item_vat;

        $order_discount += $this_item_discount;

    }




    $order_service_charge = round(($order_bill * $service_charge)/100.0);
    $order_sd_charge = round(($order_bill * $sd_percentage)/100.0);
    $order_service_charge_vat_percentage = round(($order_service_charge * $service_charge_vat_percentage)/100.0);
    $order_sd_vat_percentage = round(($order_sd_charge * $sd_vat_percentage)/100.0);

    $order_total_bill = floatval((round($order_bill) - round($order_discount)) + $order_service_charge + $order_sd_charge + $order_service_charge_vat_percentage + $order_sd_vat_percentage);

    if (!$price_including_vat) $order_total_bill +=  round($order_vat);

    /*
        echo "order_bill: " . $order_bill . '<br><br>';
        echo "order_discount: " . $order_discount . '<br><br>';
        echo "order_vat: " . $order_vat . '<br><br>';
        echo "order_service_charge: " . $order_service_charge . '<br><br>';
        echo "order_sd_charge: " . $order_sd_charge . '<br><br>';
        echo "order_service_charge_vat_percentage: " . $order_service_charge_vat_percentage . '<br><br>';
        echo "order_sd_vat_percentage: " . $order_sd_vat_percentage . '<br><br>';
        echo "order_total_bill: " . $order_total_bill . '<br><br>';
    */

    DB::table('online_orders')
        ->where('order_id', $order_id)
        ->update([
            'bill' => $order_bill,
            'vat' => round($order_vat),
            'service_charge' => $order_service_charge,
            'sd_charge' => $order_sd_charge,
            'service_charge_vat' => $order_service_charge_vat_percentage,
            'sd_vat' => $order_sd_vat_percentage,
            'discount' => $order_discount,
            'total_bill' =>  round($order_total_bill)
        ]);

}


function formatDate($date) {
    return date('F j, Y', strtotime( $date ));
}


function formatDateTime($date) {
    return date('F j, Y H:i:s', strtotime( $date ));
}


function getAppInfo() {

    $settings_data = DB::table('app_settings')
        ->select('setting_key', 'setting_value')
        ->get();

    $settings = array();
    foreach($settings_data as $row) $settings[$row->setting_key] = $row->setting_value;

    return $settings;
}


function getLookup($lookup_category, $lookup_name_id) {

    $key = "";

    if ($lookup_category == 'menu') {
        $menu = DB::table('menus')
            ->select('menu_name' )
            ->where('menu_id', '=', $lookup_name_id)
            ->first();

        $key = $menu->menu_name;
    }

    else if ($lookup_category == 'item') {
        $menu = DB::table('menu_items')
            ->select('item_name' )
            ->where('menu_item_id', '=', $lookup_name_id)
            ->first();

        $key = $menu->item_name;
    }

    return $key;
}


function randomColorGenerate() {

    $background_color = array(
        "#f0ad4e",
        "#337ab7",
        "#26B99A",
        "#5bc0de",
        "#d9534f"
    );

    global $temp_bg_color_previous_color;

    $index = rand(0,count($background_color)-1);

    if ($temp_bg_color_previous_color == $index) {
        $temp_bg_color_previous_color = $index;
        return randomColorGenerate();
    }
    else {
        $temp_bg_color_previous_color = $index;
        return $background_color[$index];
    }
}


function checkRstaurantLicense($restaurants_id = 0) {

    if ($restaurants_id == 0) {
        $restaurants = DB::table('restaurants')->select('restaurants_id')->get();
        foreach ($restaurants as $restaurant) {
            $restaurants_id = $restaurant->restaurants_id;
            checkRstaurantLicense($restaurants_id);
        }
    }

    else {
        $license_id = DB::table('restaurants')
            ->select('license_id')
            ->where('restaurants_id', $restaurants_id)
            ->first();

        $license_id = $license_id->license_id;

        if ($license_id == 0) $flag = 0;
        else {

            $license = DB::table('licenses')
                ->where('license_id', $license_id)
                ->first();

            $today = date("Y-m-d");

            $flag =  (($today >= $license->license_start_date) && ($today <= $license->license_end_date));
            $flag = $flag==1 ? 1:0;

        }



        DB::table('restaurants')
            ->where('restaurants_id', $restaurants_id)
            ->update([
                'license' => $flag
            ]);

        updateUserLicenseStatus($restaurants_id, $flag);

    }
}


function updateUserLicenseStatus($restaurants_id, $flag) {

    DB::table('users')
        ->where('restaurant_id', $restaurants_id)
        ->update(['license' => $flag]);

}


function setupNewRestaurant($restaurant_id, $user_id=0) {

    if ($user_id == 0) $user_id = Auth::user()->id;


    /*
     * Initialing Customers
     */

    $customers = json_decode(json_encode(config('global.customers')));

    foreach ($customers as $customer) {

        DB::table('customers')->insert([
            'restaurant_id' => $restaurant_id,
            'customer_name' => $customer->customer_name,
        ]);

    }




    /*
     * Initialing Inventory Store
     */

    $stores = json_decode(json_encode(config('global.inv_stores')));

    foreach ($stores as $store) {

        DB::table('inv_stores')->insertGetId([
            'store_name' => $store->store_name,
            'restaurant_id' => $restaurant_id,
            'main_store' => $store->main_store,
        ]);

    }




    /*
     * Initialing Restaurant Settings
     */

    $restaurant_settings = json_decode(json_encode(config('global.restaurant_settings')));

    foreach ($restaurant_settings as $settings) {

        DB::table('restaurant_settings')->insert([
            'setting_key' => $settings->setting_key,
            'setting_value' => $settings->setting_value,
            'restaurant_id' => $restaurant_id,
        ]);

    }




    /*
     * Initialing Printer
     */

    $printers = json_decode(json_encode(config('global.printers')));

    foreach ($printers as $printer) {

        DB::table('printers')->insert([
            'restaurant_id' => $restaurant_id,
            'printer_name' => $printer->printer_name,
            'created_by' => $user_id,
        ]);

    }





    /*
     * Initialing Order Types Permission
     */

    $order_types = DB::table('order_types')->get();

    foreach ($order_types as $type) {

        DB::table('order_types_permission')->insert([
            'type_id' => $type->type_id,
            'restaurant_id' => $restaurant_id,
        ]);

    }

}


function createStoreProduct($product_id, $store_id) {

    $ps_id = DB::table('inv_product_store')->insertGetId([
        'product_id' => $product_id,
        'store_id' => $store_id,
    ]);

    return $ps_id;

}


function updateStoreProduct($product_id, $store_id, $quantity) {

    $count = DB::table('inv_product_store')
        ->where('product_id', $product_id)
        ->where('store_id', $store_id)
        ->count();

    if ($count == 0) {
        createStoreProduct($product_id, $store_id);
    }

    $previous_quantity = DB::table('inv_product_store')
        ->where('product_id', $product_id)
        ->where('store_id', $store_id)
        ->select('quantity')
        ->first();

    $previous_quantity = floatval($previous_quantity->quantity);
    $new_quantity = $quantity + $previous_quantity;


    DB::table('inv_product_store')
        ->where('product_id', $product_id)
        ->where('store_id', $store_id)
        ->update(['quantity' => $new_quantity]);

}


function getMainStoreId ($restaurant_id) {

    $main_store_id = DB::table('inv_stores')
        ->where('restaurant_id', $restaurant_id)
        ->where('main_store', 1)
        ->select('store_id')
        ->first();

    return $main_store_id->store_id;

}


function updateProductPrice($product_id, $price) {

    $product_data = DB::table('inv_product')
        ->where('product_id', $product_id)
        ->select('first_price', 'last_price', 'avg_price')
        ->first();

    $first_price = floatval($product_data->first_price);
    $last_price = floatval($product_data->last_price);
    $avg_price = floatval($product_data->avg_price);

    if ($first_price == 0) {

        DB::table('inv_product')
            ->where('product_id', $product_id)
            ->update([
                'first_price' => $price,
                'last_price' => $price,
                'avg_price' => $price
            ]);
    }

    else {

        $avg_price = ($last_price + floatval($price))/2;

        DB::table('inv_product')
            ->where('product_id', $product_id)
            ->update([
                'last_price' => $price,
                'avg_price' => $avg_price
            ]);
    }
}


function updateItemQuantity($menu_item_id, $quantity) {

    $item = DB::table('menu_items')
        ->where('menu_item_id', $menu_item_id)
        ->select('readymade_item', 'item_quantity')
        ->first();

    $readymade_item = intval($item->readymade_item);
    $item_quantity = intval($item->item_quantity);
    $item_quantity -= intval($quantity);


    if ($readymade_item == 1) {

        DB::table('menu_items')
            ->where('menu_item_id', $menu_item_id)
            ->update([
                'item_quantity' => $item_quantity
            ]);
    }
}


function is_connected() {
    $connected = @fsockopen("www.example.com", 80);
    //website, port  (try 80 or 443)
    if ($connected) {
        $is_conn = true; //action when connected
        fclose($connected);
    }else {
        $is_conn = false; //action in connection failure
    }
    return $is_conn;
}


function test() {

    echo "hi";

    //$app_settings = Config::get('global.app_settings');

    $app_settings = config('global.app_settings');

    var_dump($app_settings);



//    foreach ($app_settings as $setting) {
//        echo $setting->setting_key . " ==== " . $setting->setting_value . "<br>";
//    }



}


function recipe($order_id) {

    $order_details = DB::table('order_details')
        ->where('order_id', $order_id)
        ->where('item_status', 1)
        ->get();


    foreach ($order_details as $item) {

        $item_id = $item->menu_item_id;
        $item_quantity = $item->item_quantity;

        $recipes = DB::table('recipes')
            ->where('item_id', $item->menu_item_id)
            ->get();


        foreach ($recipes as $recipe) {

            $total_quantity = intval($item_quantity) * floatval($recipe->quantity);

            DB::table('recipe_consumption')->insert([
                'order_id' => $order_id,
                'item_id' => $item_id,
                'product_id' => $recipe->product_id,
                'quantity' => $total_quantity,
                'store_id' => $recipe->store_id,
            ]);

            DB::table('inv_product_store')
                ->where('product_id', $recipe->product_id)
                ->where('store_id', $recipe->store_id)
                ->decrement('quantity', $total_quantity);

        }
    }

}


function changePassword($user_id, $new_password) {

    DB::table('users')
        ->where('id', $user_id)
        ->update([
            'password' => Hash::make($new_password)
        ]);
}


function searchMenu($key, $array) {

    foreach ($array as $menu) {
        if ($menu->menu_name == $key && $menu->permission == 1) {
            return true;
        }
    }
    return false;
}


function checkDiscountCircular($order_id) {

    $orders = DB::table('orders')
        ->where('order_id', $order_id)
        ->first();

    $customer = DB::table('customers')
        ->where('customer_id', $orders->customer_id)
        ->first();

    if ($customer->discount_percentage != 0) {

        DB::table('orders')
            ->where('order_id', $order_id)
            ->update([
                'discount' => $customer->discount,
                'discount_percentage' => $customer->discount_percentage,
                'discount_amount_type' => 'percentage',
                'discount_reference' => $customer->customer_name . ' - Discount cardholder guest',
            ]);

    }





    $order_date = date('Y-m-d', strtotime($orders->created_at));

    $restaurant_id = $orders->restaurant_id;

    $discount_circular = DB::table('discount_circular')
        ->where('start_date', '<=', $order_date)
        ->where('end_date', '>=', $order_date)
        ->where('restaurant_id', $restaurant_id)
        ->where('status', 1)
        ->get();

    foreach ($discount_circular as $circular) {

        DB::table('orders')
            ->where('order_id', $order_id)
            ->update([
                'discount' => $circular->discount,
                'discount_percentage' => $circular->discount,
                'discount_amount_type' => $circular->discount_type,
                'discount_reference' => $circular->circular_name,
            ]);

    }
}


function getCurrentWorkPeriod($restaurant_id) {

    //$restaurant_id = Auth::user()->restaurant_id;

    $work_period = DB::table('work_period')
        ->where('restaurant_id', $restaurant_id)
        ->where('status', 1)
        ->count();

    if ($work_period == 0) return "0";

    else {

        $work_period = DB::table('work_period')
            ->where('restaurant_id', $restaurant_id)
            ->where('status', 1)
            ->first();

        $created_at = explode(" ", $work_period->start_time);
        return $created_at[0];
    }


}


function generateInvoiceNumber($order_id, $created_at) {

    return date('Ymd', strtotime( $created_at )) .  str_pad($order_id, 6, "0", STR_PAD_LEFT);

}


function createActivityLog($activity) {

    $user_id = Auth::user()->id;
    $restaurant_id = Auth::user()->restaurant_id;

    DB::table('activity')->insert([
        'restaurant_id' => $restaurant_id,
        'user_id' => $user_id,
        'activity' => $activity,

    ]);

}


function getRestaurantTimestamp($restaurant_id) {

    date_default_timezone_set("Asia/Dhaka");
    $created_at = date("Y-m-d H:i:s");

    $settings_data = DB::table('restaurant_settings')
        ->select('setting_key', 'setting_value')
        ->where('restaurant_id', $restaurant_id)
        ->get();

    $settings = array();
    foreach($settings_data as $row) $settings[$row->setting_key] = $row->setting_value;

    $night_hour = $settings['night_hour'];

    if ($settings['order_timestamp'] == 'current_timestamp') {
        return $created_at;
    }

    else if ($settings['order_timestamp'] == 'workperiod_timestamp') {

        $workperiod = getCurrentWorkPeriod($restaurant_id);

        if ($workperiod == "0") return $created_at;

        else {
            $now = date("H:i:s");
            $created_at = $workperiod . ' ' . $now;
            return $created_at;
        }

    }


    else if ($settings['order_timestamp'] == 'nighthour_timestamp') {

        $start_time = "00:00:00";
        $end_time = date('H:i:00', mktime(0, $night_hour));

        if (time() >= strtotime($start_time) && time() <= strtotime($end_time)) {
            //within the night hour timestamp

            $yesterday = date('Y-m-d H:i:s',strtotime("-1 days"));
            return $yesterday;
        }

        else return $created_at;

    }





}


function getSettings($restaurant_id) {

    $settings_data = DB::table('restaurant_settings')
        ->select('setting_key', 'setting_value')
        ->where('restaurant_id', $restaurant_id)
        ->get();

    $settings = array();
    foreach($settings_data as $row) $settings[$row->setting_key] = $row->setting_value;

    return $settings;
}
