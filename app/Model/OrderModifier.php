<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrderModifier extends Model {

    protected $table = 'order_modifiers';
    protected $primaryKey = 'order_modifier_id';

    protected $guarded = [];


    public function order() {
        return $this->belongsTo('App\Model\Order', 'order_id','order_id');
    }

    public function orderDetail() {
        return $this->belongsTo('App\Model\OrderDetail', 'order_details_id','order_details_id');
    }
}
