<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Order extends Model {

    protected $table = 'orders';
    protected $primaryKey = 'order_id';

    protected $guarded = [];


    public function user() {
        return $this->belongsTo('App\User', 'id','created_by');
    }
}
