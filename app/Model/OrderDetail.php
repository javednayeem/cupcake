<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model {

    protected $table = 'order_details';
    protected $primaryKey = 'order_details_id';

    protected $guarded = [];


    public function order() {
        return $this->belongsTo('App\Model\Order', 'order_id','order_id');
    }

}
