$(document).ready(function() {

    $('#generate_report_button').click(function () {

        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();

        var params = {
            start_date: start_date,
            end_date: end_date
        };

        $.ajax({
            url: '/generate/expense-report',
            type: 'POST',
            format: 'JSON',
            data: {params:params, '_token': $('#token').val()},
            success: function (response) {

                var start_datetime = new Date(start_date);
                var formatted_start = start_datetime.getDate() + "-" + months[start_datetime.getMonth()] + "-" + start_datetime.getFullYear();

                var end_datetime = new Date(end_date);
                var formatted_end = end_datetime.getDate() + "-" + months[end_datetime.getMonth()] + "-" + end_datetime.getFullYear();

                $('#date_header').text(formatted_start + ' To ' + formatted_end);


                var json_message = $.parseJSON(response);
                var htmlstr = '';
                var amount_total = 0;

                for (var i=0; i<json_message.length; i++) {

                    var expense_id = json_message[i].expense_id;
                    var purpose = json_message[i].purpose;
                    var amount = json_message[i].amount;
                    var type = json_message[i].type;
                    var debit_from = json_message[i].debit_from;
                    var transfer_to = json_message[i].transfer_to;
                    var name = json_message[i].name;
                    var created_at = json_message[i].created_at;

                    amount_total += parseFloat(amount);

                    htmlstr += '<tr id="expense_'+expense_id+'">' +
                        '<th scope="row">'+ (i+1) +'</th>' +
                        '<td>'+ purpose +'</td>' +
                        '<td>'+ amount +'</td>' +
                        '<td>'+ debit_from+'</td>' +
                        '<td>'+ name+'</td>' +
                        '<td>'+ formatDate(created_at) +'</td>' +
                        '</tr>';

                }

                htmlstr += '<tr>' +
                    '<th></th>' +
                    '<td><b>Total</b></td>' +
                    '<td><b>'+amount_total+'</b></td>' +
                    '<td></td>' +
                    '<td></td>' +
                    '<td></td></tr>';

                $('#report_table_tbody').empty();
                $('#report_table_tbody').append(htmlstr);


            },
            error: function (error) {
                swal('Error!', 'Something went wrong', 'error');
            }
        });


    });

});
