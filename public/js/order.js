var menu_items = [];
var setmenu_items = [];
var modifiers = [];
var order_modifiers = [];
var kitchen_token = [];
var instructions = [];
var payment_methods = [];

var vat_no = "";
var vat_percentage = 0.0;
var service_charge = 0.0;
var discount = 0.0;
var bill_with_discount = 0.0;
var temp_bg_color_previous_color = -1;
var guest_bill=0;
var kitchen_type='single';
var instruction_count = 1;

var seconds = 0, minutes = 0, hours = 0, t, time;

var price_including_vat, vat_after_discount, sd_percentage, service_charge_vat_percentage, sd_vat_percentage, pay_first, item_box_size = 0;


var background_color = [
    "#26A65B",
    "#F89406",
    "#913D88",
    "#FF5A5E",
    "#46BFBD",
    "#34495e",
    "#947CB0"
];


$(document).ready(function() {



    $('#customer_id').select2();


    $('#order_table').DataTable({

        "paging":   false,
        "info":     false,
        "order": [[ 0, "desc" ]],
        "columnDefs": [
            { "orderable": false, "targets": 12 }
        ]

    });


    $('#cart_served_by_name').text($('#select_served_by').find(":selected").text());


    $('#select_served_by').change(function() {
        $('#cart_served_by_name').text($('#select_served_by').find(":selected").text());
    });


    $( ".select_payment_type" ).change(function() {
        if ($('.select_payment_type').val() == "card") $('#select_card_type_div').show();
        else $('#select_card_type_div').hide();
    });


    $("#order_1").click(function(){
        $('#cart_div').show();
        $('#menu_div').show();
        $('#item_div').show();
        $('#customer_details_div').show();
        $('#discount_details_div').show();
    });


    $(".place_order_button").click(function(){

        var served_by = $('#waiter_id').val();

        if (served_by != "0") {

            $('.place_order_button').prop('disabled', true);

            var order_type = $('#order_type').val();
            var table = $('#select_table_number').val();

            var customer_id = $('#customer_id').val();
            var payment_method = 'cash';
            //var payment_method = $('.select_payment_type').first().val();
            var card_name = $('#select_card_type').val();
            var order_notes = $('#order_notes').val();

            var bill = 0;
            var vat = 0;
            var service_charge = 0;
            var discount = $('#discount_amount').val()==''?0:$('#discount_amount').val();
            var discount_percentage = $('#discount_amount_percentage').val()==''?0:$('#discount_amount_percentage').val();
            var discount_amount_type = $('#discount_amount_type').val();
            var discount_reference = $('#discount_reference').val();
            var total_bill = 0;

            if (discount_reference == '') {
                discount = 0;
                discount_percentage = 0;
            }

            var order_items = [];

            for (var i = 0; i < menu_items.length; i++) {

                //redirect()

                var item_id = menu_items[i][0];
                var item_name = menu_items[i][2];
                var price = menu_items[i][4];
                var item_discount = menu_items[i][5];
                var count = menu_items[i][6];
                var item_vat = menu_items[i][7];

                var this_item_price = (price - item_discount) * count;

                if (count > 0) {
                    order_items.push([item_id, item_name, price, item_discount, count, item_vat]);
                }
            }

            var params = {
                order_type: order_type,
                table_id: table,
                waiter_id: served_by,
                customer_id: customer_id,
                bill: bill,
                vat: vat,
                service_charge: service_charge,
                discount: discount,
                discount_percentage: discount_percentage,
                discount_amount_type: discount_amount_type,
                discount_reference: discount_reference,
                total_bill: total_bill,
                payment_method: payment_method,
                card_name: card_name,
                order_notes: order_notes,
                order_items: order_items,
                order_modifiers: order_modifiers
            };

            $.ajax({
                url: '/place/order',
                type: 'POST',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {

                    $('#order_id').val(response);

                    $('.new_order_id').text('Order No: ' + response);
                    showSuccessNotification('Order #' + response);
                    generateReceipt(response);

                    if (kitchen_type == 'single') {
                        printTokenReceipt();
                    }

                    else {
                        $('.print_button_div').show();
                    }


                    /*
                     if pay first is true, then it will stay on the page after creating order
                     otherwise, it will reload the page
                     */

                    if (pay_first == 1) {
                        $('#guest_print_button').prop('disabled', false);
                        $('.order_payment_button').prop('disabled', false);
                    }

                    else {
                        reloadCurrentPage();
                    }



                    // if (guest_bill != '1') reloadCurrentPage();
                    //
                    // else {
                    //     $('#guest_print_button').prop('disabled', false);
                    //     $('.order_payment_button').prop('disabled', false);
                    // }


                },
                error: function (error) {
                    showErrorNotification();
                }
            });

        }

        else showWarningNotification('Please Select Waiter');


    });


    $("#confirm_order_button").click(function(){

        swal({
            title: 'Are you sure to place this order?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, confirm!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function () {

            $('#confirm_order_button').prop('disabled', true);

            var scheduled_date = $('#basic-datepicker').val();
            var scheduled_time = $('#24hours-timepicker').val();
            var order_notes = $('#online_order_notes').val();
            var order_items = [];

            for (var i = 0; i < menu_items.length; i++) {

                var item_id = menu_items[i][0];
                var item_name = menu_items[i][2];
                var price = menu_items[i][4];
                var item_discount = menu_items[i][5];
                var count = menu_items[i][6];
                var item_vat = menu_items[i][7];

                if (count > 0) {

                    var item = {
                        item_id: item_id,
                        item_name: item_name,
                        price: price,
                        item_discount: item_discount,
                        count: count,
                        item_vat: item_vat
                    };

                    order_items.push(item);
                }
            }

            var params = {
                scheduled_date: scheduled_date,
                scheduled_time: scheduled_time,
                order_notes: order_notes,
                order_items: order_items,
                order_modifiers: order_modifiers
            };

            $.ajax({
                url: '/place/online-order',
                type: 'POST',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {

                    showSuccessNotification("Order Placed Successfully");
                    redirect('/online-order/receipt/' + response, 1500);

                },
                error: function (error) {
                    showErrorNotification();
                }
            });


        });














    });


    $("#print_cart_button").click(function () {

        //$('.main_container').hide();
        //$('#print_r').show();

        printCartReceipt();

        //window.print();

        /* var divToPrint=document.getElementById('view_cart_modal');

         var newWin=window.open('','Print-Window');

         newWin.document.open();

         newWin.document.write('<html><head>' +
         '<link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css" />' +
         '<style>' +
         'html{font-size: 2px;}'+
         '</style>'+
         '</head>' +
         '<body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

         newWin.document.close();

         setTimeout(function(){newWin.close();},10);*/
    });


    $("#add_customer_button").click(function(){

        var customer_name = $('#customer_name').val();
        var customer_phone = $('#customer_phone').val();
        var customer_email = $('#customer_email').val();
        var customer_address = $('#customer_address').val();
        var card_no = $('#card_no').val();
        var discount_percentage = $('#discount_percentage').val();

        if (customer_name != "" && customer_phone != "") {

            var params = {
                customer_name: customer_name,
                customer_phone: customer_phone,
                customer_email: customer_email,
                customer_address: customer_address,
                card_no: card_no,
                discount_percentage: discount_percentage
            };

            $.ajax({
                url: '/add/client',
                type: 'POST',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {
                    var htmlstr = '<option value="'+response+'">'+customer_name+'</option>';
                    $( "#customer_id" ).append(htmlstr);

                    swal("", customer_name + " Added To Database", "success");
                },
                error: function (error) {
                    console.log("Error! Something went wrong.");
                }
            });
        }

        else {
            swal({
                type: 'warning',
                html: $('<div>')
                    .addClass('some-class')
                    .text('Input Customer Name and Phone Number'),
                animation: false,
                customClass: 'animated bounceIn'
            });
        }
    });


    $("#modify_order_button").click(function(){

        $('#modify_order_button').prop('disabled', true);

        var order_items = [];

        for (var i = 0; i < menu_items.length; i++) {

            var order_id = $('#order_id').val();
            var item_id = menu_items[i][0];
            var item_name = menu_items[i][2];
            var price = menu_items[i][4];
            var item_discount = menu_items[i][5];
            var count = menu_items[i][6];
            var item_vat = menu_items[i][7];

            if (count > 0) {
                order_items.push([item_id, item_name, price, item_discount, count, item_vat]);
            }
        }

        var params = {
            order_id: order_id,
            order_items: order_items,
            order_modifiers: order_modifiers
        };

        $.ajax({
            url: '/modify/order',
            type: 'POST',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {

                $('.new_order_id').text('Order No: ' + order_id);
                $('#order_id').val(order_id);
                generateReceipt(order_id);

                if (kitchen_type == 'single') {
                    printTokenReceipt();
                }

                else {
                    $('.print_button_div').show();
                }

                showSuccessNotification('Items Added to Order');
                reloadCurrentPage();
            },
            error: function (error) {
                showErrorNotification();
            }
        });

    });


    $("#history_back_button").click(function () {

        clearCart();

        var pathname = window.location.pathname;

        window.location.href = pathname;



    });


    $(".order_payment_button").click(function () {

        var order_id = $("#order_id").val();
        generateReceipt(order_id);

        $('.bill_type').text('Bill');

        var total_amount = $('.receipt_total_amount').first().text();
        $('#paid_amount').val(total_amount);

        $("#order_payment_modal").modal('show');


    });


    $("#request_void_button").click(function () {

        var order_id = $('#order_id').val();
        var username = $('#username').val();
        var password = $('#password').val();
        var reason = $('#reason').val();
        var notes = $('#notes').val();

        if (username != '' && reason != '') {

            var params = {
                order_id: order_id,
                username: username,
                password: password,
                reason: reason,
                notes: notes
            };

            $.ajax({
                url: '/request/void',
                type: 'POST',
                format: 'JSON',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {

                    var json_message = $.parseJSON(response);


                    if (json_message.status == 'success') {

                        swal('Done!', json_message.message, 'success');

                        generateVoidReceipt(order_id);



                    }

                    else {
                        swal('Warning!', json_message.message , 'warning');
                    }


                },
                error: function (error) {
                    swal('Error!', 'Something went wrong.', 'error');
                }
            });

        }

    });





    $('#confirm_payment_button').click(function () {

        var order_id = $('#order_id').val();
        var given_amount = $('#total_paid_amount').val();

        if (order_id > 0) {

            var params = {
                order_id: order_id,
                given_amount: given_amount,
                payment_methods: payment_methods
            };

            $.ajax({
                url: '/confirm/payment',
                type: 'POST',
                format: 'JSON',
                data: {params: params, '_token': $('#token').val()},
                success: function (response) {

                    $('#order_receipt_div').show();

                    printOrderPaymentReceipt('paid');

                    showSuccessNotification('Payment confirmed');
                    redirect('/order', 1500);
                },
                error: function (error) {
                    showErrorNotification();
                }
            });
        }
    });


    $('#payment_method').change(function () {

        var payment_method = $("#payment_method").val();

        if (payment_method == 'cash') {
            $('#bank_card_span').hide();
            $('#card_number_span').hide();
        }

        else if(payment_method == 'card') {

            $('#bank_card_span').show();
            $('#card_number_span').show();

            $('#card_no_label').text(uc_first(payment_method) + ' No.');

            // $('#confirm_payment_button').prop('disabled', false);
            // $('#select_bank_card_div').show();
        }

        else {

            $('#bank_card_span').hide();
            $('#card_number_span').show();

            $('#card_no_label').text(uc_first(payment_method) + ' No.');
        }

    });


    $("#add_discount_button").click(function(){

        var order_id = $('#order_id').val();

        var discount_amount = parseFloat($('#discount_amount').val());
        var discount_amount_percentage = parseFloat($('#discount_amount_percentage').val());
        var discount_amount_type = $('#discount_amount_type').val();
        var discount_reference = $('#discount_reference').val();

        if (discount_reference == '') {
            swal('Warning!', 'Discount Reference Is Mandatory', 'warning');
        }

        if (order_id != 0 && discount_reference != '') {

            var params = {
                order_id: order_id,
                discount: discount_amount,
                discount_percentage: discount_amount_percentage,
                discount_amount_type: discount_amount_type,
                discount_reference: discount_reference
            };


            $.ajax({
                url: '/discount-add',
                type: 'POST',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {

                    generateReceipt(order_id);

                    swal('Done!', 'Discount Has Been Added', 'success');
                },
                error: function (error) {
                    swal('Error!', 'Something Went Wrong', 'error');
                }
            });

        }

    });


    $('#discount_amount').keyup(function() {

        var cart_net_amaount = parseFloat($( ".cart_net_amaount" ).first().text());
        var receipt_total_amount = parseFloat($( ".receipt_subtotal" ).first().text());
        //var receipt_total_amount = parseFloat($('#order_amount_total').text());
        //var receipt_total_amount = parseFloat($('#receipt_total_amount').text());
        var amount_total = cart_net_amaount + receipt_total_amount;
        //var amount_total = cart_net_amaount;
        var discount_amount = parseFloat($('#discount_amount').val());

        var discount_amount_percentage = (discount_amount * 100) / amount_total;
        discount_amount_percentage = discount_amount_percentage.toFixed(2);

        $('#discount_amount_percentage').val(discount_amount_percentage);
        $('#discount_amount_type').val('cash');

        $('#order_amount_total').text(amount_total);
        $('.discount_amount').text(discount_amount);
        $('.order_net_amaount').text(amount_total - discount_amount);

    });


    $('#discount_amount_percentage').keyup(function() {

        var cart_net_amaount = parseFloat($( ".cart_net_amaount" ).first().text());
        var receipt_total_amount = parseFloat($( ".receipt_subtotal" ).first().text());

        var amount_total = cart_net_amaount + receipt_total_amount;
        //var amount_total = cart_net_amaount;
        var discount_amount_percentage = parseFloat($('#discount_amount_percentage').val());

        var discount_amount = amount_total * ((discount_amount_percentage * 1.0)/100.0);
        discount_amount = discount_amount.toFixed(2);

        $('#discount_amount').val(discount_amount);
        $('#discount_amount_type').val('percentage');

        $('#order_amount_total').text(amount_total);
        $('.discount_amount').text(discount_amount);
        $('.order_net_amaount').text(amount_total - discount_amount);

    });


    $('#change_table_button').click(function () {

        var order_id = $('#order_id').val();
        var select_table = $('#select_table').val();

        if (order_id != 0) {
            var params = {
                order_id: order_id,
                select_table: select_table
            };

            $.ajax({
                url: '/change/table',
                type: 'POST',
                format: 'JSON',
                data: {params: params, '_token': $('#token').val()},
                success: function (response) {
                    reloadCurrentPage();
                },
                error: function (error) {
                    swal('Error!', 'Something went wrong', 'error');
                }
            })
        }
    });


    $('#add_modifier_button').click(function () {

        var item_id = $('#modifier_item_id').val();
        var modifier_id = $('#modifier_id').val();
        var modifier_quantity = $('#modifier_quantity').val();

        order_modifiers.push([item_id, modifier_id, modifier_quantity]);

        printCart();
    });


    $( "#modifier_id" ).change(function() {
        displayModifierPrice();
    });


    $( "#modifier_quantity" ).keyup(function() {
        displayModifierPrice();
    });


    $('#order_instruction_button').click(function () {



        $('#order_instruction_modal').modal('show');
    });


    $('#add_instruction_button').click(function () {

        var instruction_id = $('#instruction_id').val();
        var instruction = $("#instruction_id option:selected").text();

        addInstruction(instruction);

    });


    $('#textarea_instruction_button').click(function () {

        var instruction = $("#textarea_instruction").val();

        if (instruction != '') {

            var params = {
                instruction: instruction
            };

            $.ajax({
                url: '/add/order-instruction',
                type: 'POST',
                format: 'JSON',
                data: {params: params, '_token': $('#token').val()},

                success: function (response) {

                    var htmlstr = '<option value="'+response+'">'+instruction+'</option>';
                    $('#instruction_id').append(htmlstr);

                },
                error: function (error) {
                    // swal("Error!", "Something went wrong", "error");
                }
            });

            addInstruction(instruction);
            $("#textarea_instruction").val('');

        }



    });


    $('#waiter_id').change(function () {

        var order_id = $('#order_id').val();
        var waiter_id = $('#waiter_id').val();

        var waiter_name = $('#waiter_id option:selected').text();
        $('.token_waiter_name').text(waiter_name);

        if (order_id != 0) {
            var params = {
                order_id: order_id,
                waiter_id: waiter_id
            };

            $.ajax({
                url: '/change/waiter',
                type: 'POST',
                format: 'JSON',
                data: {params: params, '_token': $('#token').val()},
                success: function (response) {
                    generateReceipt(order_id);
                    swal('Done!', 'Waiter Changed', 'success');
                },
                error: function (error) {
                    swal('Error!', 'Something went wrong', 'error');
                }
            })
        }

    });


    $('#customer_id').change(function () {

        var order_id = $('#order_id').val();
        var customer_id = $('#customer_id').val();

        if (order_id != 0) {
            var params = {
                order_id: order_id,
                customer_id: customer_id
            };

            $.ajax({
                url: '/change/customer',
                type: 'POST',
                format: 'JSON',
                data: {params: params, '_token': $('#token').val()},
                success: function (response) {
                    generateReceipt(order_id);
                    swal('Done!', 'Customer Changed', 'success');
                },
                error: function (error) {
                    swal('Error!', 'Something went wrong', 'error');
                }
            })
        }

    });


    $('#order_type').change(function () {

        var order_id = $('#order_id').val();
        var order_type = $('#order_type').val();

        if (order_id != 0) {
            var params = {
                order_id: order_id,
                order_type: order_type
            };

            $.ajax({
                url: '/change/order-type',
                type: 'POST',
                format: 'JSON',
                data: {params: params, '_token': $('#token').val()},
                success: function (response) {
                    generateReceipt(order_id);
                },
                error: function (error) {
                    swal('Error!', 'Something went wrong', 'error');
                }
            })
        }

    });


    $('#modal_payment_method').change(function() {

        var payment_method = $('#modal_payment_method').val();

        if (payment_method == "cash") {
            $('#modal_bank_div').hide();
        }
        else {

            $('#modal_bank_div').show();

            if (payment_method != 'card') {

                $('#modal_bank_card_span').hide();
                $('#modal_card_number_label').text(uc_first(payment_method) +  ' No.');

            }

            else {
                $('#modal_bank_card_span').show();
                $('#modal_card_number_label').text('Card No.');
            }
        }

    });


    $("#request_void_item_button").click(function () {

        var order_id = $('#order_id').val();

        var order_details_id = $('#void_order_details_id').val();
        var item_quantity_max = $('#void_item_quantity_max').val();

        var username = $('#item_username').val();
        var password = $('#item_password').val();
        var item_quantity = $('#void_item_quantity').val();
        var reason = $('#item_reason').val();
        var notes = $('#item_notes').val();
        var waiter_id = $('#waiter_id').val();

        if (username != '' && reason != '' && (item_quantity>0 && item_quantity<=item_quantity_max)) {

            var params = {
                order_id: order_id,
                order_details_id: order_details_id,
                username: username,
                password: password,
                item_quantity: item_quantity,
                reason: reason,
                notes: notes,
                waiter_id: waiter_id
            };

            $.ajax({
                url: '/request/void/item',
                type: 'POST',
                format: 'JSON',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {

                    var json_message = $.parseJSON(response);

                    if (json_message.status == 'success') {

                        swal('Done!', json_message.message, 'success');

                        //generateReceipt(order_id);
                        $('#order_receipt_div').show();
                        generateVoidReceipt(order_id);

                        //reloadCurrentPage();

                        //printVoidReceipt();

                    }

                    else {
                        swal('Warning!', json_message.message , 'warning');
                    }


                },
                error: function (error) {
                    swal('Error!', 'Something went wrong.', 'error');
                }
            });

        }

    });


    $("#set_item_quantity").click(function () {

        var item_id = $('#item_id').val();
        var item_quantity = $('#item_amount').val();

        if (item_quantity != '' && item_quantity>0) setItemQuantity(item_id, item_quantity);

    });


    $("#add_item_button").click(function () {

        var menu_id = $('#open_food_menu_id').val();
        var item_name = $('#open_food_item_name').val();
        var price = $('#open_food_price').val();
        var item_vat = $('#open_food_item_vat').val();
        var printer_id = $('#open_food_printer_id').val();

        var set_menu = 0;
        var ratio = '';
        var category = '';
        var discount_available = 1;


        if (item_name != '' && price != '' ) {

            var params = {
                menu_id: menu_id,
                item_name: item_name,
                ratio: ratio,
                price: price,
                item_vat: item_vat,
                set_menu: set_menu,
                discount_available: discount_available,
                category: category,
                printer_id: printer_id
            };

            $.ajax({
                url: '/add/menu-item',
                type: 'POST',
                format: 'JSON',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {

                    pushToItemArray(response, menu_id, item_name, ratio, price, 0, item_vat, 0, 0, set_menu, printer_id);

                    loadItems2(menu_id, 'Open Food');

                },
                error: function (error) {
                    swal('Error!', 'Something went wrong.', 'error');
                }
            });

        }

    });


    $("#request_complimentary_button").click(function () {

        var order_id = $('#order_id').val();

        var order_details_id = $('#complimentary_order_details_id').val();
        var item_quantity_max = $('#complimentary_item_quantity_max').val();

        var item_quantity = $('#complimentary_item_quantity').val();
        var notes = $('#complimentary_notes').val();
        var waiter_id = $('#waiter_id').val();

        if (notes != '' && (item_quantity>0 && item_quantity<=item_quantity_max)) {

            var params = {
                order_id: order_id,
                order_details_id: order_details_id,
                item_quantity: item_quantity,
                notes: notes,
                waiter_id: waiter_id
            };

            $.ajax({
                url: '/request/complimentary/',
                type: 'POST',
                format: 'JSON',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {

                    showSuccessNotification('Item Added To Complimentary');
                    generateReceipt(order_id);

                },
                error: function (error) {
                    swal('Error!', 'Something went wrong.', 'error');
                }
            });

        }

    });

});


function addInstruction(instruction) {

    var printer_id = $('#instruction_printer_id').val();
    var order_notes = $('#order_notes').val();

    instructions.push(instruction + '_' + printer_id);

    if (order_notes != '') $('#order_notes').val(order_notes + '|' + instruction + '_' + printer_id);
    else $('#order_notes').val(instruction + '_' + printer_id);

    printInstruction();
}


function editInstruction(index) {

    var cooking_instruction = instructions[index];
    cooking_instruction = cooking_instruction.split("_")[0];
    // var printer_id = cooking_instruction.split("_")[1];

    // $('#edit_instruction_id').val(index);
    $('#textarea_instruction').val(cooking_instruction);
    deleteInstruction(index);


}


function deleteInstruction(index) {
    instructions.splice(index, 1);
    printInstruction();
}


function printInstruction() {

    var printer_id = $('#instruction_printer_id').val();
    var htmlstr = '';
    var htmlstr_token = '';

    for (var i=0; i < instructions.length; i++) {

        var cooking_instruction = instructions[i];
        cooking_instruction = cooking_instruction.split("_")[0];

        htmlstr += '<tr id="instruction_'+(i+1)+'">' +
            '<td scope="row">'+(i+1)+'</td>' +
            '<td>'+cooking_instruction+'</td>' +
            '<td>' +
            '<button type="button" class="btn btn-dark" onclick="editInstruction('+i+')">' +
            '<i class="glyphicon glyphicon-pencil"></i>' +
            '</button>' +
            '<button type="button" class="btn btn-danger" onclick="deleteInstruction('+i+')">' +
            '<i class="glyphicon glyphicon-remove"></i>' +
            '</button>' +
            '</td>' +
            '</tr>';

        htmlstr_token += '<li>' +
            '<p class="excerpt">'+cooking_instruction+'</p>' +
            '</li>';

    }

    $('#instruction_table').empty();
    $('#token_instruction_' + printer_id).empty();


    $('#instruction_table').append(htmlstr);
    $('#token_instruction_' + printer_id).append(htmlstr_token);
    instruction_count++;
}


function pushToItemArray(id, menu_id, item_name, ratio, price, discount, item_vat, readymade_item, item_quantity, set_menu, printer_id) {
    var count=0;
    menu_items.push([id, menu_id, item_name, ratio, price, discount, count, item_vat, readymade_item, item_quantity, set_menu, printer_id]);
}


function pushToSetMenuItemArray(item_id, item_name, menu_item_id) {

    var item = {
        item_id: item_id,
        item_name: item_name,
        menu_item_id: menu_item_id
    };

    setmenu_items.push(item);
    // setmenu_items.push([item_id, item_name, menu_item_id]);
}


function pushToModifiersArray(modifier_id, modifier_name, price, modifier_vat) {
    modifiers.push([modifier_id, modifier_name, price, modifier_vat]);
}


function pushToKitchenToken(printer_id) {

    var htmlstr_token = '';
    kitchen_token.push([printer_id, htmlstr_token]);
}


function clearKitchenToken() {

    for (var i=0; i < kitchen_token.length; i++) {
        kitchen_token[i][1] = '';
    }

}


function printKitchenToken() {

    clearKitchenToken();

    var waiter_name = $('#waiter_id option:selected').text();
    $('.token_waiter_name').text(waiter_name);

    var htmlstr_token = '';

    for (var i = 0; i < menu_items.length; i++) {

        var item_id = menu_items[i][0];
        //var temp_item_menu_id = menu_items[i][1];
        var temp_item_name = menu_items[i][2];
        var temp_item_ratio = menu_items[i][3];
        var temp_item_count = menu_items[i][6];
        var printer_id = parseInt(menu_items[i][11]);

        if (temp_item_count > 0) {

            htmlstr_token = '<tr>' +
                '<td><h3>'+ temp_item_name + ' ' + temp_item_ratio +'</h3></td>' +
                '<td><h3 class="text-right">'+ temp_item_count +'</h3></td>' +
                '</tr>';

            for (var j = 0; j < order_modifiers.length; j++) {

                var order_modifiers_item_id = order_modifiers[j][0];
                var modifier_id = order_modifiers[j][1];
                var modifier_quantity = order_modifiers[j][2];

                if (modifier_quantity > 0 && order_modifiers_item_id == item_id) {

                    var modifier = getModifierData(modifier_id);
                    var modifier_name = modifier.modifier_name;

                    htmlstr_token += '<tr>' +
                        '<td><h3>'+ modifier_name + '</h3></td>' +
                        '<td><h3 class="text-right">'+ modifier_quantity +'</h3></td>' +
                        '</tr>';

                }
            }


            for (var j = 0; j < kitchen_token.length; j++) {

                if (kitchen_token[j][0] == printer_id) {
                    //var previous_token = kitchen_token[i][1];
                    kitchen_token[j][1] += htmlstr_token;
                }
            }

        }
    }


    for (var i=0; i < kitchen_token.length; i++) {

        var printer_id = kitchen_token[i][0];
        var token = kitchen_token[i][1];

        $( "#token_div_table_" + printer_id ).empty();
        $( "#token_div_table_" + printer_id ).append(token);
    }

}


function loadItems(menu_id) {
    var item_count = 1;

    var htmlstr = '<tbody id="menu_item_div">';

    for (var i = 0; i < menu_items.length; i++) {

        if (menu_items[i][1] == menu_id) {
            var item_id = menu_items[i][0];
            var item_name = menu_items[i][2];
            var ratio = menu_items[i][3];
            var price = menu_items[i][4];
            var discount = menu_items[i][5];

            htmlstr += '<tr class="pointer" onclick="addToCart('+item_id+')"><th scope="row">'+ item_count +'</th>' +
                '<td>'+ item_name +'</td>' +
                '<td>'+ ratio +'</td>' +
                '<td>'+ price +'</td>' +
                '<td>'+ discount +'</td>' +
                '<td onclick="addToCart('+item_id+')"><span class="pointer">Add Item To Cart</span></td></tr>';

            item_count++;
        }
    }

    htmlstr += '</tbody>';

    $( "#menu_item_div" ).remove();
    $( "#menu_item_table" ).append(htmlstr);
}


function loadItems2(menu_id, menu_name) {

    var item_count = 1;
    $('#menu_item_header').text(menu_name);

    var htmlstr = '';

    for (var i = 0; i < menu_items.length; i++) {

        if (menu_items[i][1] == menu_id) {

            var item_id = menu_items[i][0];
            var item_name = menu_items[i][2];
            var ratio = menu_items[i][3];
            var price = menu_items[i][4];
            var discount = menu_items[i][5];
            var readymade_item = menu_items[i][8];
            var item_quantity = menu_items[i][9];

            var box_width = parseInt(item_box_size/50);

            htmlstr += '<div class="col-md-'+box_width+' pointer" onclick="addToCart('+item_id+')">' +
                '<div class="panel panel-default text-center text-white slider-bg m-b-0" style="height: '+item_box_size+'px; background-color: '+background_color[getRandomNumber(0, background_color.length-1)]+'">' +
                '<div class="panel-body p-0">' +
                '<div id="owl-slider-2" class="owl-carousel owl-loaded owl-drag">' +
                '<div class="owl-nav" style="color: white">' +
                '<h4><strong>'+ item_name +'</strong></h4>' +
                '<h5>'+ ratio +'</h5>' +
                '<h5><strong>'+ price +'Tk</strong></h5>' +
                '</div></div></div></div></div>';

            item_count++;


        }
    }

    if (menu_name == 'Open Food' || menu_name == 'open food' || menu_name == 'Open food') {

        htmlstr += '<div class="col-md-'+box_width+' pointer" id="addOpenFoodDiv" onclick="addOpenFoodModal('+menu_id+')">' +
            '<div class="panel panel-default text-center text-white slider-bg m-b-0" style="height: '+item_box_size+'px; background-color: '+background_color[getRandomNumber(0, background_color.length-1)]+'">' +
            '<div class="panel-body p-0">' +
            '<div id="owl-slider-2" class="owl-carousel owl-loaded owl-drag">' +
            '<div class="owl-nav" style="color: white">' +
            '<h2><strong>Add Open Food</strong></h2>' +
            '<span><i class="fa fa-plus"></i></span>';

    }


    $( "#item_div_span" ).empty();
    $( "#item_div_span" ).append(htmlstr);




}


function loadItems3(menu_id) {

    var item_count = 1;

    htmlstr = '<div class="row">';

    for (var i = 0; i < menu_items.length; i++) {

        if (menu_items[i][1] == menu_id) {
            var item_id = menu_items[i][0];
            var item_name = menu_items[i][2];
            var ratio = menu_items[i][3];
            var price = menu_items[i][4];
            var discount = menu_items[i][5];

            htmlstr += '<div class="col-md-4 pointer" onclick="addToCart('+item_id+')">' +
                '<div class="panel panel-default text-center text-white slider-bg m-b-0" style="height: 200px; background-color: '+background_color[getRandomNumber(0, background_color.length-1)]+'">' +
                '<div class="panel-body p-0">' +
                '<div id="owl-slider-2" class="owl-carousel owl-loaded owl-drag">' +
                '<div class="owl-nav" style="color: white">' +
                '<h3>'+ item_name +'</h3>' +
                '<h4>'+ ratio +'</h4>' +
                '<h5>'+ price +'Tk</h5>' +
                '</div></div></div></div></div>';

            item_count++;
        }
    }

    htmlstr += '</div>';

    $( "#view_item_modal_span" ).empty();
    $( "#view_item_modal_span" ).append(htmlstr);

    $('#view_item_modal').modal('show');
}


function addToCart(item_id) {


    for (var i = 0; i < menu_items.length; i++) {

        if (menu_items[i][0] == item_id) {

            var readymade_item = menu_items[i][8];
            var item_quantity = menu_items[i][9];

            if (readymade_item == 1 && item_quantity>0) {
                menu_items[i][6]++;
                menu_items[i][9] -= 1;
                $('#item_q_' + item_id).text('Available: ' + menu_items[i][9]);
            }

            else if (readymade_item == 1 && item_quantity<=0) swal('Warning!', 'No Available Item', 'warning');

            else menu_items[i][6]++;

            break;
        }
    }

    printCart();

}


function getSetMenuItems(given_menu_item_id) {

    var htmlstr = '';

    for (var i = 0; i < setmenu_items.length; i++) {

        // var item_id = setmenu_items[i][0];
        // var item_name = setmenu_items[i][1];
        // var menu_item_id = setmenu_items[i][2];

        var item = setmenu_items[i];

        var item_id = item.item_id;
        var item_name = item.item_name;
        var menu_item_id = item.menu_item_id;

        if (given_menu_item_id == menu_item_id) {
            htmlstr += '<tr><td>&nbsp;&nbsp;&nbsp;'+ item_name +'</td><td></td></tr>';
        }
    }

    return htmlstr;
}


function printCart() {

    var htmlstr = '';
    var htmlstr_modal = '';
    var htmlstr_token = '';


    var total_bill = 0;
    discount = 0.0;
    var item_count = 1;
    var flag = false;
    var discount_amount = $('#discount_amount').val();
    var vat = 0;

    for (var i = 0; i < menu_items.length; i++) {

        var item_id = menu_items[i][0];
        var temp_item_menu_id = menu_items[i][1];
        var temp_item_name = menu_items[i][2];
        var temp_item_ratio = menu_items[i][3];
        var temp_item_price = menu_items[i][4];
        var temp_item_discount = menu_items[i][5];
        var temp_item_count = menu_items[i][6];
        var temp_item_vat = parseFloat(menu_items[i][7]);
        var temp_set_menu = parseInt(menu_items[i][10]);
        var printer_id = parseInt(menu_items[i][11]);

        if (temp_item_count > 0) {

            var this_item_price = (temp_item_price - temp_item_discount) * temp_item_count;
            if (temp_item_vat > 0) vat += parseFloat(this_item_price * (temp_item_vat/100.00));

            var this_item_modifier = '';
            var this_item_modifier_price = 0;
            var this_item_modifier_vat = 0;
            var modifier_price = 0;
            var order_modifiers_modifier_quantity = 0;
            var modifier_id = 0;
            var htmlstr_modifiers = '';
            var htmlstr_token_modifiers = '';

            for (var j = 0; j < order_modifiers.length; j++) {

                var order_modifiers_item_id = order_modifiers[j][0];
                modifier_id = order_modifiers[j][1];
                order_modifiers_modifier_quantity = order_modifiers[j][2];
                var mod_index = j;

                if (order_modifiers_modifier_quantity > 0) {

                    if (order_modifiers_item_id == item_id) {

                        var modifier = getModifierData(modifier_id);

                        var modifier_name = modifier.modifier_name;
                        modifier_price = parseInt(modifier.price);
                        var modifier_vat = parseInt(modifier.modifier_vat);

                        this_item_modifier_price = (modifier_price * order_modifiers_modifier_quantity);
                        this_item_modifier_vat += parseFloat(modifier_price * (modifier_vat/100.00));
                        vat += this_item_modifier_vat;

                        this_item_modifier += ', ' + modifier_name + ' x' + order_modifiers_modifier_quantity;


                        htmlstr_modifiers += '<tr class="even pointer" id="item_row_'+ item_id +'">' +
                            '<td></td>' +
                            '<td>Add '+ modifier_name +'</td>' +
                            '<td>'+ modifier_price +'</td>' +
                            '<td><i class="fa fa-minus" onclick="modifierDec('+mod_index+')"></i>&nbsp;&nbsp;'+order_modifiers_modifier_quantity+'&nbsp;&nbsp;' +
                            '<i class="fa fa-plus" onclick="modifierInc('+mod_index+')"></i></td>' +
                            '<td>'+ (modifier_price * order_modifiers_modifier_quantity) +'</td>' +
                            '<td>' +
                            '<span class="pointer" onclick="removeModifier('+mod_index+')" title="Remove Item">' +
                            '<i class="glyphicon glyphicon-trash"></i>' +
                            '</span>' +
                            '</td></tr>';

                        htmlstr_token_modifiers += '<tr>' +
                            '<td>Add '+ modifier_name + '</td>' +
                            '<td>'+ order_modifiers_modifier_quantity +'</td>' +
                            '</tr>';

                    }
                }
            }

            flag = true;
            total_bill += (this_item_price + this_item_modifier_price);
            discount += parseFloat(temp_item_discount * temp_item_count);

            htmlstr += '<tr class="even pointer" id="item_row_'+ item_id +'">' +
                '<td>'+ item_count +'</td>' +
                '<td>'+ temp_item_name + ' ' + temp_item_ratio +'</td>' +
                '<td>'+ parseInt(temp_item_price) +'</td>' +
                '<td><i class="fa fa-minus" onclick="itemDec('+item_id+')"></i>&nbsp;&nbsp;'+temp_item_count+'&nbsp;&nbsp;' +
                '<i class="fa fa-plus" onclick="itemInc('+item_id+')"></i>&nbsp;&nbsp;' +
                '<span class="pointer" onclick="addItemQty('+item_id+')"><i class="fa fa-forward"></i></span></td>' +
                '<td>'+ this_item_price +'</td>' +
                '<td>' +
                '<button type="button" class="btn btn-success btn-xs pointer" onclick="addModifier('+item_id+')" title="Add Modifier">' +
                'Modifier</button> &nbsp;&nbsp;' +
                '<span class="pointer" onclick="removeCartItem('+item_id+')" title="Remove Item">' +
                '<i class="glyphicon glyphicon-trash"></i>' +
                '</span>' +
                '</td></tr>';

            htmlstr += htmlstr_modifiers;


            htmlstr_token = '<tr>' +
                '<td>'+ temp_item_name +'</td>' +
                '<td class="text-right">'+ temp_item_count +'</td>' +
                '</tr>';

            printKitchenToken();

            //$( "#token_div_table_" + printer_id ).empty();
            //$( "#token_div_table_" + printer_id ).append(htmlstr_token);

            htmlstr_token += htmlstr_token_modifiers;

            if (temp_set_menu) htmlstr_token += getSetMenuItems(item_id);

            item_count++;
        }
    }


    vat = Math.ceil(vat);
    vat = parseInt(vat, 10);


    var order_charges = getCharges(total_bill);

    var order_service_charge = order_charges['service_charge'];
    var order_sd_percentage = order_charges['sd_percentage'];
    var order_service_charge_vat_percentage = order_charges['service_charge_vat_percentage'];
    var order_sd_vat_percentage = order_charges['sd_vat_percentage'];
    var order_discount = order_charges['discount'];

    var net_amaount = 0.0;

    if (price_including_vat == 1) {
        $('#receipt_vat_header').text('(Prices are inclusive of VAT)');
        net_amaount = parseFloat(total_bill + order_service_charge + order_sd_percentage + order_service_charge_vat_percentage + order_sd_vat_percentage);
    }

    else {
        $('#receipt_vat_header').text('(Prices are exclusive of VAT)');
        net_amaount = parseFloat(total_bill + order_service_charge + vat + order_sd_percentage + order_service_charge_vat_percentage + order_sd_vat_percentage);
    }


    var service = parseFloat(total_bill * (service_charge/100.0)).toFixed(2);
    net_amaount = net_amaount.toFixed(2);


    $( ".cart_bill" ).text(total_bill.toFixed(2));
    $( ".grand_total" ).text(total_bill.toFixed(2));
    $( ".cart_vat" ).text(vat.toFixed(2));
    $( ".cart_service_charge" ).text(service);
    $( ".cart_sd_percentage" ).text(order_sd_percentage.toFixed(2));
    $( ".cart_service_charge_vat" ).text(order_service_charge_vat_percentage.toFixed(2));
    $( ".cart_discount" ).text(discount);
    $( ".cart_net_amaount" ).text(net_amaount);


    $( "#cart_item_div" ).empty();
    $( "#cart_table" ).append(htmlstr);

    $( "#cart_item_modal" ).empty();
    $( "#cart_table_modal" ).append(htmlstr_modal);

    //$( "#token_div_table" ).empty();
    //$( "#token_div_table" ).append(htmlstr_token);

    if (flag) $('.place_order_button').prop('disabled', false);
    else $('.place_order_button').prop('disabled', true);

    if (flag) $('#modify_order_button').prop('disabled', false);
    else $('#modify_order_button').prop('disabled', true);

    if (flag) $('#order_instruction_button').prop('disabled', false);
    else $('#order_instruction_button').prop('disabled', true);

    if (flag) $('#confirm_order_span').prop('disabled', false);
    else $('#confirm_order_span').prop('disabled', true);


    //console.log(kitchen_token);

}


function removeCartItem(item_id) {

    for (var i = 0; i < menu_items.length; i++) {
        if (menu_items[i][0] == item_id) {

            //var readymade_item = menu_items[i][8];
            //var item_quantity = menu_items[i][9];

            // if (readymade_item == 1) {
            //     menu_items[i][9] += menu_items[i][6];
            //     $('#item_q_' + item_id).text('Available: ' + menu_items[i][9]);
            // }

            menu_items[i][6] = 0;
            removeModifierItem(item_id);
            break;
        }
    }

    printCart();
}


function removeModifierItem(item_id) {

    for (var i = 0; i < order_modifiers.length; i++) {

        var order_modifiers_item_id = order_modifiers[i][0];

        if (order_modifiers_item_id == item_id) {
            order_modifiers.splice( i, 1 );
        }
    }
}


function clearCart() {
    for (var i = 0; i < menu_items.length; i++) {

        var item_id = menu_items[i][0];
        // var readymade_item = menu_items[i][8];
        // var item_quantity = menu_items[i][9];
        //
        // if (readymade_item == 1) {
        //     menu_items[i][9] += parseInt(menu_items[i][6]);
        //     $('#item_q_' + item_id).text('Available: ' + menu_items[i][9]);
        // }

        menu_items[i][6] = 0;
    }

    order_modifiers = [];
    printCart();
}


function itemDec(item_id) {

    for (var i = 0; i < menu_items.length; i++) {
        if (menu_items[i][0] == item_id) {
            if (menu_items[i][6]>1) {

                var readymade_item = menu_items[i][8];
                var item_quantity = menu_items[i][9];

                if (readymade_item == 1) {
                    menu_items[i][6]--;
                    menu_items[i][9] += 1;
                    $('#item_q_' + item_id).text('Available: ' + menu_items[i][9]);
                }


                else menu_items[i][6]--;

                break;
            }
        }
    }
    printCart();
}


function itemInc(item_id) {

    for (var i = 0; i < menu_items.length; i++) {
        if (menu_items[i][0] == item_id) {

            var readymade_item = menu_items[i][8];
            var item_quantity = menu_items[i][9];

            if (readymade_item == 1 && item_quantity>0) {
                menu_items[i][6]++;
                menu_items[i][9] -= 1;
                $('#item_q_' + item_id).text('Available: ' + menu_items[i][9]);
            }

            else if (readymade_item == 1 && item_quantity<=0) swal('Warning!', 'No Available Item', 'warning');

            else menu_items[i][6]++;


            break;
        }
    }
    printCart();
}


function setItemQuantity(item_id, item_quantity) {

    for (var i = 0; i < menu_items.length; i++) {

        if (menu_items[i][0] == item_id) {

            menu_items[i][6] = item_quantity;

            break;
        }
    }

    printCart();
}


function getItemQuantity(item_id) {

    for (var i = 0; i < menu_items.length; i++) {
        if (menu_items[i][0] == item_id) return menu_items[i][6];
    }

}


function pushChargesInfo(vat_number, vat, service, guest_bill_value, piv, vad, sd, scvp, sdvp, kitchenType, pay_first, item_box_size) {

    vat_no = vat_number;
    vat_percentage = isNaN(vat) ? 0.0 : vat;
    service_charge = isNaN(service) ? 0.0 : service;
    guest_bill = guest_bill_value;
    kitchen_type = kitchenType;

    this.price_including_vat = isNaN(piv)?0:piv;
    this.vat_after_discount = isNaN(vad)?0:vad;
    this.sd_percentage = isNaN(sd)?0.0:sd;
    this.service_charge_vat_percentage = isNaN(scvp)?0.0:scvp;
    this.sd_vat_percentage = isNaN(sdvp)?0.0:sdvp;
    this.pay_first = isNaN(pay_first)?0:pay_first;
    this.item_box_size = isNaN(item_box_size)?0:item_box_size;


    /*
     console.log("vat_no : " + vat_no);
     console.log("vat_percentage : " + vat_percentage);
     console.log("service_charge : " + service_charge);
     */
}


function get_order_details(order_no) {

    var params = {
        order_id: order_no,
    };

    $.ajax({
        url: '/get/order',
        type: 'POST',
        data: {params: params, "_token": $('#token').val()},

        success: function (response) {
            var json_message = $.parseJSON(response);

            var htmlstr_modal = '<tbody id="cart_item_modal">';

            for (var i=0; i<json_message.length; i++) {

                htmlstr_modal += '<tr><th scope="row">'+ (i+1) +'</th>' +
                    '<td>'+ json_message[i].menu_item_name +'</td>' +
                    '<td>'+ json_message[i].item_price +'</td>' +
                    '<td>'+ json_message[i].item_quantity +'</td>' +
                    '<td>'+ json_message[i].item_price * json_message[i].item_quantity +'</td></tr>';
            }

            htmlstr_modal += '</tbody>';
            $( "#cart_item_modal" ).remove();
            $( "#cart_table_modal" ).append(htmlstr_modal);


        },
        error: function (error) {
            console.log("Error! Something went wrong.");
        }
    });

    $('#order_number').text('order #' + order_no);

    $('#view_order_modal').modal('show');
}


function generateReceipt(order_id) {

    var params = {
        order_id: order_id
    };

    $.ajax({
        url: '/get/order/history',
        type: 'POST',
        format: 'JSON',
        data: {params: params, "_token": $('#token').val()},

        success: function (response) {

            var json_message = JSON.parse(response);

            var order_data = json_message.order_data;
            var order_details = json_message.order_details;
            var item_modifiers = json_message.order_modifiers;

            var discount = order_data.discount>0.0? '-' + order_data.discount:'0';

            var discount_amount_type;
            var discount_percentage;

            if (order_data.discount_amount_type=='cash') {

                discount_percentage = order_data.discount;
                discount_amount_type = 'Tk';

            }

            else {

                discount_percentage = order_data.discount_percentage;
                discount_amount_type = '%';

            }

            var payment_modal_count = 1;
            var htmlstr = '';
            var htmlstr_modal = '';


            //console.log(order_details);

            for (var i=0; i<order_details.length; i++) {

                var order_details_id = order_details[i].order_details_id;
                var menu_item_id = order_details[i].menu_item_id;
                var menu_item_name = order_details[i].menu_item_name;
                var ratio = order_details[i].ratio != null?order_details[i].ratio:'';
                var item_price = parseInt(order_details[i].item_price);
                var item_discount = parseInt(order_details[i].item_discount);
                var item_quantity = parseInt(order_details[i].item_quantity);
                var unit_price = ((item_price - item_discount));
                var item_status = parseInt(order_details[i].item_status);
                var total_cost = (unit_price * item_quantity);

                var modifiers_str = '';
                var item_modifier_price = 0;


                for (var j=0; j<item_modifiers.length; j++) {

                    var modifier_order_details_id = item_modifiers[j].order_details_id;
                    var modifier_name = item_modifiers[j].modifier_name;
                    var modifier_price = item_modifiers[j].price;
                    var modifier_quantity = item_modifiers[j].quantity;

                    if (order_details_id == modifier_order_details_id) {

                        modifiers_str += ', ' + modifier_name + ' x' + modifier_quantity;
                        item_modifier_price += (parseInt(modifier_price) * parseInt(modifier_quantity));
                        unit_price += parseInt(modifier_price);

                    }

                }


                /*
                 previous_cart_table
                 */

                if (item_quantity > 0) {

                    htmlstr += '<tr class="even pointer';
                    if (item_status == 0) htmlstr += ' strike';
                    htmlstr += '" id="order_details_'+ order_details_id +'">' +
                        '<td>'+ menu_item_name + ' ' + ratio + modifiers_str +'</td>' +
                        '<td>'+ item_quantity +'</td>' +
                        '<td>' +
                        '<button type="button" class="btn btn-danger btn-xs pointer" ';
                    if (item_status != 0) htmlstr += 'onclick="void_item(\'' + order_details_id + '\', \''+item_quantity+'\')"';
                    htmlstr += '>Void</button> ';

                    if (item_status != 0 && item_price>0) {

                        htmlstr += '<button type="button" class="btn btn-primary btn-xs pointer" ' +
                            'onclick="complimentary_item(\''+order_details_id+'\', \''+item_quantity+'\')">Complimentary</button>';

                    }

                    htmlstr += '</td></tr>';

                }



                /*
                 receipt_order_table
                 */

                if (item_quantity > 0) {

                    htmlstr_modal += '<tr>' +
                        '<th scope="row">'+ payment_modal_count++ +'</th>' +
                        '<td>'+ menu_item_name + ' ' + ratio + modifiers_str +'</td>' +
                        '<td>'+ unit_price +'</td>' +
                        '<td>'+ item_quantity +'</td>' +
                        '<td class="text-right">'+ (total_cost + item_modifier_price) +'</td></tr>';

                }



            }


            $( "#previous_cart_table" ).empty();
            $( "#previous_cart_table" ).append(htmlstr);

            $( ".receipt_order_table" ).empty();
            $( ".receipt_order_table" ).append(htmlstr_modal);


            $( ".receipt_customer_name" ).text(order_data.customer_name);
            $( ".receipt_waiter_name" ).text(order_data.waiter_name);
            $( ".receipt_created_at" ).text(formatDateTime(order_data.created_at));
            $( ".receipt_table_no" ).text(order_data.table_name);
            $( ".receipt_order_no" ).text(formatInvoiceDate(order_data.created_at) + numberPadding(order_data.order_id));
            $( ".receipt_token_no" ).text(order_data.order_id);
            $( ".receipt_order_type" ).text(uc_first(order_data.order_type));


            $( ".receipt_subtotal" ).text(order_data.bill);
            $( ".receipt_vat_total" ).text(order_data.vat);
            $( ".receipt_service_charge" ).text(order_data.service_charge);
            $( ".receipt_sd_percentage" ).text(order_data.sd_charge);
            $( ".receipt_service_charge_vat_percentage" ).text(order_data.service_charge_vat);
            $( ".receipt_sd_vat_percentage" ).text(order_data.sd_vat);

            $( ".receipt_discount_percentage" ).text(discount_percentage + discount_amount_type);

            $( ".receipt_discount_amount" ).text(discount);
            $( ".receipt_total_amount" ).text(order_data.total_bill);

            $( "#order_amount_total" ).text(order_data.total_bill);
            $( ".discount_amount" ).text(order_data.discount);
            $( ".order_net_amaount" ).text(order_data.total_bill - order_data.discount);


            printCart();




        },
        error: function (error) {
            showErrorNotification();
        }
    });

}


function select_table_order(order_id, table_id, customer_id, waiter_id, order_type, discount, discount_percentage, discount_amount_type, discount_reference, table_name, order_created) {

    clearCart();

    time = timeDifference(order_created);
    setOrderCountdown();


    /*
     Hide Div
     */

    $('#order_1_view').hide();
    $('.place_order_button').hide();
    $("#table_div").hide();





    /*
     Show Div
     */

    $('#order_2_view').show();
    $('#modify_order_button').show();
    $("#menu_div").show();
    $("#item_div").show();
    $("#payment_div").show();
    $("#customer_details_div").show();
    $("#previous_cart_div").show();
    $("#cart_div").show();
    $("#slip_div").show();
    $("#token_div").show();
    $("#settle_div").show();





    $("#table_header").text("Table - " + table_name);
    $(".new_table_id").text("Table - " + table_name);


    $("#select_table_number").val(table_id);

    $("#customer_id").val(customer_id).change();
    $("#waiter_id").val(waiter_id).change();
    $("#order_type").val(order_type).change();

    $("#discount_amount").val(discount);
    $("#discount_amount_percentage").val(discount_percentage);
    $("#discount_amount_type").val(discount_amount_type);
    $("#discount_reference").val(discount_reference);



    //$(".select_payment_type").val(payment_method).change();
    //$("#select_card_type").val(card_name).change();



    $("#order_id").val(order_id);

    $("#order_settle").attr("href", "/settle/" + order_id);

    generateReceipt(order_id);

}


function select_table(table_id, table_name) {

    $('#order_2_view').hide();
    $('#order_1_view').show();


    clearCart();

    // $( "#customer_details_div" ).removeClass( "col-md-4 col-sm-4 col-xs-12" ).addClass( "col-md-6 col-sm-6 col-xs-12" );
    //$( "#cart_div" ).removeClass( "col-md-5 col-sm-5 col-xs-12" ).addClass( "col-md-5 col-sm-5 col-xs-12" );
    $('#payment_button_div').hide();
    $('#modify_order_button').hide();
    $("#previous_cart_div").hide();
    $('.place_order_button').show();

    $("#select_table").val(table_id);
    $("#select_table_number").val(table_id);
    $("#table_header").text("Table - " + table_name);
    $(".new_table_id").text("Table - " + table_name);

    $("#table_div").hide();

    $("#customer_details_div").show();
    $("#cart_div").show();
    $("#menu_div").show();
    $("#item_div").show();
    $("#payment_div").show();
    $("#token_div").show();
    $("#settle_div").show();

    $('.receipt_customer_name').text('Walk in Customer');
    $('.receipt_waiter_name').text(getUserName());
    $('.receipt_table_no').text(table_name);

}


function timeDifference(timestamp1) {

    var timestamp2  = getFormattedDate();

    /*
     console.log("timestamp1: " + timestamp1);
     console.log("timestamp2: " + timestamp2);
     */

    const today_timestamp1 = new Date(timestamp1);
    const today_timestamp2 = new Date(timestamp2);

    const days = parseInt((today_timestamp2 - today_timestamp1) / (1000 * 60 * 60 * 24));
    const hours = parseInt(Math.abs(today_timestamp2 - today_timestamp1) / (1000 * 60 * 60) % 24);
    const minutes = parseInt(Math.abs(today_timestamp2.getTime() - today_timestamp1.getTime()) / (1000 * 60) % 60);
    const seconds = parseInt(Math.abs(today_timestamp2.getTime() - today_timestamp1.getTime()) / (1000) % 60);

    /*
     console.log("days: " + days);
     console.log("hours: " + hours);
     console.log("minutes: " + minutes);
     console.log("seconds: " + seconds);
     */

    var time = {
        days: days,
        hours: hours,
        minutes: minutes,
        seconds: seconds
    };

    return time;
}


function getFormattedDate(){

    var d = new Date();

    d = d.getFullYear() + "-" + ('0' + (d.getMonth() + 1)).slice(-2) + "-" + ('0' + d.getDate()).slice(-2) + " " + ('0' + d.getHours()).slice(-2) + ":" + ('0' + d.getMinutes()).slice(-2) + ":" + ('0' + d.getSeconds()).slice(-2);

    return d;
}


function setOrderCountdown() {

    hours = time.hours + (time.days * 24);
    minutes = time.minutes;
    seconds = time.seconds;

    add();
}


function add() {
    seconds++;
    if (seconds >= 60) {
        seconds = 0;
        minutes++;
        if (minutes >= 60) {
            minutes = 0;
            hours++;
        }
    }



    var textContent = (hours ? (hours > 9 ? hours : "0" + hours) : "00") + ":" + (minutes ? (minutes > 9 ? minutes : "0" + minutes) : "00") + ":" + (seconds > 9 ? seconds : "0" + seconds);

    $('#clock_span').text(textContent);

    timer();
}


function timer() {
    t = setTimeout(add, 1000);
}


function void_item(order_details_id, item_quantity) {


    $('#void_order_details_id').val(order_details_id);
    $('#void_item_quantity_max').val(item_quantity);
    $('#void_item_quantity').val(item_quantity);

    $('#void_item_modal').modal('show');

}


function complimentary_item(order_details_id, item_quantity) {

    $('#complimentary_order_details_id').val(order_details_id);
    $('#complimentary_item_quantity_max').val(item_quantity);
    $('#complimentary_item_quantity').val(item_quantity);

    $('#complimentary_item_modal').modal('show');

}


function getRandomNumber(min, max) {
    var number = Math.floor(Math.random() * (max - min + 1)) + min;
    if (temp_bg_color_previous_color == number) {
        temp_bg_color_previous_color = number;
        return getRandomNumber(min, max);
    }
    else {
        temp_bg_color_previous_color = number;
        return number;
    }
}


function printTokenReceipt() {

    console.log('printTokenReceipt function');
    console.log('kitchen_token length: ' + kitchen_token.length);

    w = window.open();
    w.document.write($('#kitchen_token').html());
    w.print();
    w.close();

    // for (var i=0; i < kitchen_token.length; i++) {
    //
    //     var printer_id = kitchen_token[i][0];
    //     var token = kitchen_token[i][1];
    //
    //     if (token != '') {
    //         printToken(printer_id);
    //     }
    // }

}


function printToken(printer_id) {

    //console.log('printToken function');

    $('#printTokenButton_' + printer_id).hide();

    w = window.open();
    // w.document.write($('#print_r').html());
    //w.document.write($('#kitchen_token_' + printer_id).html());
    w.document.write($('#kitchen_token').html());
    w.print();
    w.close();

    $('#printTokenButton_' + printer_id).show();

}


function printOrderPaymentReceipt(order_status) {

    var order_id = $('#order_id').val();

    if (order_status != 'paid' && order_id != 0) {

        var params = {
            order_id: order_id,
            order_status: order_status
        };

        $.ajax({

            url: '/change/order-status',
            type: 'POST',
            format: 'JSON',
            data: {'_token': $('#token').val(), params: params},

            success: function (response) {
                $('.bill_type').text('Guest Print');
                $('#order_receipt_div').show();
                window.print();
                reloadCurrentPage();
            },

            error: function (error) {
                showErrorNotification();
            }

        });

    }

    else {
        window.print();
    }




}


function getCharges(order_total) {

    var order_service_charge = order_total * (this.service_charge/100.0);
    var order_sd_percentage = order_total * (this.sd_percentage/100.0);
    var order_service_charge_vat_percentage = order_service_charge * (this.service_charge_vat_percentage/100.0);
    var order_sd_vat_percentage = order_sd_percentage * (this.sd_vat_percentage/100.0);
    var order_discount = this.discount;

    var order_charges = {
        service_charge: order_service_charge,
        sd_percentage: order_sd_percentage,
        service_charge_vat_percentage: order_service_charge_vat_percentage,
        sd_vat_percentage: order_sd_vat_percentage,
        discount: order_discount
    };

    return order_charges;

}


function addModifier(item_id) {

    $('#modifier_item_id').val(item_id);
    $('#modifier_quantity').val(1);
    displayModifierPrice();

    $('#add_modifier_modal').modal('show');

}


function getModifierData(modifier_id) {

    // console.log('hit the function');
    // console.log('modifier_id: ' + modifier_id);

    for (var i = 0; i < modifiers.length; i++) {

        var this_modifier_id = modifiers[i][0];
        var this_modifier_name = modifiers[i][1];
        var this_price = modifiers[i][2];
        var this_modifier_vat = modifiers[i][3];

        //console.log('this_modifier_id: ' + this_modifier_id);


        if (modifier_id == this_modifier_id) {

            // console.log('this_modifier_id: ' + this_modifier_id);
            // console.log('this_modifier_name: ' + this_modifier_name);
            // console.log('this_price: ' + this_price);

            var modifier = {
                modifier_name: this_modifier_name,
                price: this_price,
                modifier_vat: this_modifier_vat,
            };

            return modifier;
        }

    }
}


function displayModifierPrice() {

    var modifier_id = $('#modifier_id').val();
    var modifier_quantity = $('#modifier_quantity').val();

    var modifier = getModifierData(modifier_id);

    var modifier_price = parseInt(modifier.price);
    var modifier_vat = parseInt(modifier.modifier_vat);


    $('#modifier_unit_price').val(modifier_price);
    $('#modifier_total_price').val(modifier_price * modifier_quantity);

}


function modifierDec(index) {

    var item_id = order_modifiers[index][0];
    var modifier_id = order_modifiers[index][1];
    var modifier_quantity = order_modifiers[index][2];

    if (modifier_quantity>0) order_modifiers[index][2]--;

    printCart();
}


function modifierInc(index) {

    var item_id = order_modifiers[index][0];
    var modifier_id = order_modifiers[index][1];
    var modifier_quantity = order_modifiers[index][2];

    order_modifiers[index][2]++;

    printCart();
}


function removeModifier(index) {

    order_modifiers[index][2] = 0;

    printCart();
}


function guestPrint() {

    w = window.open();
    // w.document.write($('#print_r').html());
    w.document.write($('#settle_div').html());
    w.print();
    w.close();

}


function addPayment() {

    var payment_method = $('#payment_method').val();
    var paid_amount = $('#paid_amount').val();
    var bank_card = $('#bank_card').val();
    var bank_card_name = $('#bank_card option:selected').text();
    var card_number = $('#card_number').val();

    if (payment_method != 'cash' && payment_method != 'card') {
        bank_card = '';
        bank_card_name = '';
    }

    else if (payment_method == 'cash') {
        bank_card = '';
        bank_card_name = '';
        card_number = '';
    }


    if (paid_amount != '' && paid_amount>0) {

        var payment = {
            payment_method: payment_method,
            paid_amount: paid_amount,
            bank_card: bank_card,
            bank_card_name: bank_card_name,
            card_number: card_number
        };

        payment_methods.push(payment);

        $('#paid_amount').val('');
        $('#card_number').val('');

        printPaymentMethod();

    }


}


function deletePayment(index) {
    payment_methods.splice(index, 1);
    printPaymentMethod();
}


function printPaymentMethod() {

    var htmlstr = '';
    var htmlstr_payment = '';
    var total_amount = 0;

    for (var i=0; i < payment_methods.length; i++) {

        var payment = payment_methods[i];

        var payment_method = payment.payment_method;
        var paid_amount = payment.paid_amount;
        var bank_card = payment.bank_card;
        var bank_card_name = payment.bank_card_name;
        var card_number = payment.card_number;

        total_amount += parseFloat(paid_amount);

        htmlstr += '<tr id="payment_'+(i+1)+'">' +
            '<td>'+payment_method+'</td>' +
            '<td>'+bank_card_name+'</td>' +
            '<td>'+card_number+'</td>' +
            '<td class="text-right">'+paid_amount+'</td>' +
            '<td>' +
            '<button type="button" class="btn btn-danger" onclick="deletePayment('+i+')">' +
            '<i class="glyphicon glyphicon-remove"></i>' +
            '</button>' +
            '</td>' +
            '</tr>';

        htmlstr_payment += '<tr>' +
            '<td>'+ payment_method +'</td>' +
            '<td>'+ bank_card_name +'</td>' +
            '<td>'+ card_number +'</td>' +
            '<td class="text-right">'+ paid_amount +'</td></tr>';
    }

    htmlstr += '<tr>' +
        '<td></td>' +
        '<td></td>' +
        '<td></td>' +
        '<td class="text-right">Total: <b id="payment_total_amount">'+total_amount+'</b></td>' +
        '<td></td>' +
        '</tr>';

    htmlstr_payment += '<tr>' +
        '<td></td>' +
        '<td></td>' +
        '<td></td>' +
        '<td class="text-right"><b>'+ total_amount +'</b></td></tr>';

    $('#payment_table').empty();
    $('#payment_table').append(htmlstr);

    $( ".payment_table" ).empty();
    $( ".payment_table" ).append(htmlstr_payment);

    var receipt_total_amount = parseInt($( ".receipt_total_amount" ).first().text());
    var rest_amount = total_amount - receipt_total_amount;


    $( "#total_paid_amount" ).val(total_amount);
    $( "#change_amount" ).text(rest_amount);

    $( ".receipt_given_amount" ).text(total_amount);
    $( ".receipt_return_amount" ).text(rest_amount);
    $( "#paid_amount" ).val(Math.abs(rest_amount));

    if (rest_amount>=0) $('#confirm_payment_button').prop('disabled', false);
    else $('#confirm_payment_button').prop('disabled', true);

}


function addItemQty(item_id) {

    $('#item_id').val(item_id);
    $('#item_amount').val(getItemQuantity(item_id));

    $('#item_quantity_modal').modal('show');

}


function generateVoidReceipt(order_id) {

    var params = {
        order_id: order_id
    };

    $.ajax({
        url: '/get/void/history',
        type: 'POST',
        format: 'JSON',
        data: {params: params, "_token": $('#token').val()},

        success: function (response) {

            var json_message = JSON.parse(response);

            var void_item = json_message.void_item;

            var htmlstr = '';
            var htmlstr_header = '<th width="80%">Item Name</th>' +
                '<th>Qty</th>';

            //console.log(order_details);

            for (var i=0; i < void_item.length; i++) {

                var menu_item_name = void_item[i].menu_item_name;
                var void_quantity = void_item[i].void_quantity;

                /*
                 receipt_order_table
                 */

                if (void_quantity > 0) {

                    htmlstr += '<tr class="strike">' +
                        '<td>'+ menu_item_name +'</td>' +
                        '<td>'+ void_quantity +'</td>' +
                        '</tr>';

                }



            }

            $( "#receipt_table_header" ).empty();
            $( "#receipt_table_header" ).append(htmlstr_header);

            $( ".receipt_order_table" ).empty();
            $( ".receipt_order_table" ).append(htmlstr);

            $('#receipt_charges_div').hide();
            $('#receipt_vat_div').hide();
            $('#receipt_payment_div').hide();
            $('#receipt_footer_div').hide();
            $('#receipt_address_div').hide();
            $('#tr_receipt_customer_name').hide();

            $('.bill_type').text('Void');
            $('#tr_receipt_kot').text('KOT');
            $( ".receipt_order_no" ).text(order_id);


            window.print();

            printCart();

            reloadCurrentPage();

        },
        error: function (error) {
            showErrorNotification();
        }
    });

}


function printVoidReceipt() {
    window.print();
}


function addOpenFoodModal(menu_id) {

    $('#open_food_menu_id').val(menu_id);

    $('#add_item_modal').modal('show');

}