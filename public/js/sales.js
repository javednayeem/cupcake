var sales_report_waiter_array = [];
var sales_by_month_array = [];
var sales_by_week_array = [];
var sales_by_table_array = [];
var sales_by_menu_array = [];
var temp_bg_color_previous_color = -1;

var background_color = [
    "#26A65B",
    "#F89406",
    "#913D88",
    "#FF5A5E",
    "#46BFBD",
    "#34495e",
    "#947CB0",
];


for (var i = 0; i < 13; i++) sales_by_month_array.push([i, 0, 0]);
for (var i = 0; i < 7; i++) sales_by_week_array.push([i, 0, 0]);


$(document).ready(function() {

    var sales_by_waiter = document.getElementById("chart_sales_by_waiter");
    var temp_label = [];
    var temp_data = [];
    var temp_bg_color = [];

    for (var i = 0; i < sales_report_waiter_array.length; i++) {
        temp_label.push([sales_report_waiter_array[i][0]]);
        temp_data.push([sales_report_waiter_array[i][1]]);
        temp_bg_color = temp_bg_color.concat([background_color[getRandomNumber(0, background_color.length-1)]]);
    }

    var sales_by_waiter_data = {

        labels: temp_label,
        datasets: [{
            data: temp_data,
            backgroundColor: temp_bg_color
        }]
    };

    var pieChart = new Chart(sales_by_waiter, {
        type: 'doughnut',
        data: sales_by_waiter_data
    });


    var sales_by_month_orders = document.getElementById('sales_by_month_orders').getContext('2d');
    var sales_by_month_bills = document.getElementById('sales_by_month_bills').getContext('2d');
    var sales_by_week_orders = document.getElementById('sales_by_week_orders').getContext('2d');
    var sales_by_table_orders = document.getElementById('sales_by_table_orders').getContext('2d');
    var sales_by_menu_orders = document.getElementById('sales_by_menu_orders').getContext('2d');

    var temp_label_orders = [];
    var temp_data_bills = [];


    for (var i = 1; i < sales_by_month_array.length; i++) {
        temp_label_orders = temp_label_orders.concat([sales_by_month_array[i][1]]);
        temp_data_bills = temp_data_bills.concat([sales_by_month_array[i][2]]);
    }

    var myChart = new Chart(sales_by_month_orders, {
        type: 'line',
        data: {
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            datasets: [{
                label: "Orders",
                data: temp_label_orders,
                backgroundColor: "rgba(75, 192, 192, 0.4)"

            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });

    myChart = new Chart(sales_by_month_bills, {
        type: 'bar',
        data: {
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            datasets: [{
                label: "Orders",
                data: temp_data_bills,
                backgroundColor: "rgba(52, 73, 94, 0.8)"
            }]
        }
    });




    temp_label_orders = [];
    temp_data_bills = [];
    var temp_bg_color = [];

    for (var i = 0; i < sales_by_week_array.length; i++) {
        temp_label_orders = temp_label_orders.concat([sales_by_week_array[i][1]]);
        temp_data_bills = temp_data_bills.concat([sales_by_week_array[i][2]]);
        temp_bg_color = temp_bg_color.concat([background_color[getRandomNumber(0, background_color.length-1)]]);
    }

    myChart = new Chart(sales_by_week_orders, {
        type: 'bar',
        data: {
            labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
            datasets: [{
                label: "Sales By Week",
                data: temp_label_orders,
                backgroundColor: temp_bg_color
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });



    /*
     Table Analytics
     */

    var temp_table_name = [];
    var temp_table_order = [];
    var temp_bg_color = [];

    for (var i = 0; i < sales_by_table_array.length; i++) {
        temp_table_name = temp_table_name.concat('Table-' + [sales_by_table_array[i][0]]);
        temp_table_order = temp_table_order.concat([sales_by_table_array[i][1]]);
        temp_bg_color = temp_bg_color.concat([background_color[getRandomNumber(0, background_color.length-1)]]);
    }

    myChart = new Chart(sales_by_table_orders, {
        type: 'pie',
        data: {
            labels: temp_table_name,
            datasets: [{
                data: temp_table_order,
                backgroundColor: temp_bg_color
            }]
        }
    });


    /*
     Menu Analytics
     */

    var temp_menu_name = [];
    var temp_menu_count = [];
    var temp_bg_color = [];

    for (var i = 0; i < sales_by_menu_array.length; i++) {
        temp_menu_name = temp_menu_name.concat([sales_by_menu_array[i][0]]);
        temp_menu_count = temp_menu_count.concat([sales_by_menu_array[i][1]]);
        temp_bg_color = temp_bg_color.concat([background_color[getRandomNumber(0, background_color.length-1)]]);
    }

    myChart = new Chart(sales_by_menu_orders, {
        type: 'doughnut',
        data: {
            labels: temp_menu_name,
            datasets: [{
                data: temp_menu_count,
                backgroundColor: temp_bg_color
            }]
        }
    });






    $('#select_report_type').change(function () {
        var select_report_type = $('#select_report_type').val();

        if (select_report_type == 'waiter_wise_sale') {
            $('#select_table_div').hide();
            $('#select_order_id').hide();
            $('#select_waiter_div').show();
            $('#select_start_date').hide();
            $('#select_end_date').hide();
        }

        else if (select_report_type == 'table_wise_sale') {
            $('#select_waiter_div').hide();
            $('#select_order_id').hide();
            $('#select_table_div').show();
            $('#select_start_date').hide();
            $('#select_end_date').hide();
        }

        else if (select_report_type == 'invoice_wise_sale') {
            $('#select_waiter_div').hide();
            $('#select_table_div').hide();
            $('#select_order_id').hide();
            $('#select_start_date').hide();
            $('#select_end_date').hide();
        }

        else if (select_report_type == 'order_summary') {
            $('#select_waiter_div').hide();
            $('#select_table_div').hide();
            $('#select_order_id').show();
            $('#select_start_date').hide();
            $('#select_end_date').hide();
        }

        else if (select_report_type == 'month_wise_sale') {
            $('#select_waiter_div').hide();
            $('#select_table_div').hide();
            $('#select_order_id').hide();

            $('#select_start_date').show();
            $('#select_end_date').show();
        }

    });


    $('#generate_report_button').click(function () {
        var select_report_type = $('#select_report_type').val();

        if (select_report_type == '0') {
            swal('', 'Please Select Any Report Type', 'warning');
        }

        else {

            var params;

            if (select_report_type == 'waiter_wise_sale') {
                var waiter_id = $('#select_waiter').val();
                params = {
                    select_report_type: 'waiter_wise_sale',
                    waiter_id: waiter_id,
                }
            }

            else if (select_report_type == 'table_wise_sale') {
                var table_id = $('#select_table').val();
                params = {
                    select_report_type: 'table_wise_sale',
                    table_id: table_id,
                }
            }

            else if (select_report_type == 'invoice_wise_sale') {
                params = {
                    select_report_type: 'invoice_wise_sale',
                };
            }

            else if (select_report_type == 'order_summary') {
                var order_id = $('#order_id_input').val();
                params = {
                    select_report_type: 'order_summary',
                    order_id: order_id,
                }
            }

            else if (select_report_type == 'month_wise_sale') {
                var start_date = $('#start_date').val();
                var end_date = $('#end_date').val();
                params = {
                    select_report_type: 'month_wise_sale',
                    start_date: start_date,
                    end_date: end_date,
                }
            }

            $.ajax({
                url: '/generate/report',
                type: 'POST',
                format: 'JSON',
                data: {params:params, '_token': $('#token').val()},
                success: function (response) {
                    var json_message = $.parseJSON(response);

                    var htmlstr = '';

                    for (var i=0; i<json_message.length; i++) {

                        var order_id = json_message[i].order_id;
                        var waiter_name = json_message[i].waiter_name;
                        var time = json_message[i].updated_at;
                        var customer_name = json_message[i].customer_name;
                        var table_name = json_message[i].table_name;
                        var bill = json_message[i].bill;
                        var vat = json_message[i].vat;
                        var service_charge = json_message[i].service_charge;
                        var total_bill = json_message[i].total_bill;
                        var payment_method = json_message[i].payment_method;
                        var given_amount = json_message[i].given_amount==null ? '': json_message[i].given_amount;



                        var t_total_bill = (json_message[i].bill + json_message[i].service_charge + json_message[i].vat - json_message[i].discount);
                        var t_discount = json_message[i].discount == null ? '':json_message[i].discount;
                        var t_discount_reference = json_message[i].discount_reference==null? '':json_message[i].discount_reference;


                        htmlstr += '<tr class="even pointer" onclick="orderDetails('+order_id+', \''+waiter_name+'\', \''+time+'\', \''+customer_name+'\', \''+table_name+'\', \''+bill+'\', \''+vat+'\', \''+service_charge+'\', \''+total_bill+'\', \''+payment_method+'\', \''+given_amount+'\')">' +
                            '<td>'+ order_id +'</td>' +
                            '<td>'+ time +'</td>' +
                            '<td>'+ waiter_name +'</td>' +
                            '<td>'+ json_message[i].total_item +'</td>' +
                            '<td>'+ json_message[i].bill +'</td>' +
                            '<td>'+ json_message[i].service_charge +'</td>' +
                            '<td>'+ json_message[i].vat +'</td>' +
                            '<td>'+ t_discount +'</td>' +
                            '<td>'+ t_discount_reference +'</td>' +
                            '<td>'+ json_message[i].total_bill +'</td>' +
                            '<td>'+ json_message[i].payment_method +'</td></td></tr>';
                    }

                    $('#report_table_tbody').empty();
                    $( "#report_table_tbody" ).append(htmlstr);

                    $('#report_table').DataTable();

                },
                error: function (error) {
                    swal('Error!', 'Something went wrong', 'error');
                }
            });
        }

    });

});


function sales_array_push(name, number) {
    sales_report_waiter_array.push([name, number]);
}


function sales_by_month_push(month, orders, bill) {
    sales_by_month_array[month][1] = orders;
    sales_by_month_array[month][2] = bill;
}


function sales_by_week_push(week, orders, bill) {
    sales_by_week_array[week][1] = orders;
    sales_by_week_array[week][2] = bill;
}


function sales_by_table_array_push(table_name, table_order) {
    sales_by_table_array.push([table_name, table_order]);
}


function sales_by_menu_push(menu_name, count) {
    sales_by_menu_array.push([menu_name, count]);
}


function orderDetails(order_id, waiter, time, customer, table, bill, vat, service_charge, total_bill, payment_method, given_amount) {

    $('#payment_modal_order_no').text(order_id);
    $('#payment_modal_served_by').text(waiter);
    $('#payment_modal_time').text(time);
    $('#payment_modal_customer_name').text(customer);
    $('#payment_modal_table').text(table);
    $('#payment_modal_order_total').text(bill);
    $('#payment_modal_service_charge').text(service_charge);
    $('#payment_modal_vat_total').text(vat);
    $('#payment_modal_total_amount').text(total_bill);
    $('#payment_modal_payment_method').text(payment_method);
    $('#payment_modal_paid_amount').text(given_amount);

    var params = {
        order_id: order_id
    };

    $.ajax({
        url: '/generate/orderDetails',
        type: 'POST',
        format: 'JSON',
        data: {params: params, '_token': $('#token').val()},

        success: function (response) {
            var json_message = $.parseJSON(response);
            var htmlstr = '';

            for (var i=0; i<json_message.length; i++) {

                var discount = json_message[i].item_discount==null? 0: parseInt(json_message[i].item_discount);
                var u_price = (parseInt(json_message[i].item_price) - discount);
                var total = (parseInt(json_message[i].item_quantity) * u_price);

                htmlstr += '<tr><th scope="row">'+ (i+1) +'</th>' +
                    '<td>'+ json_message[i].menu_item_name +'</td>' +
                    '<td>'+ u_price +'</td>' +
                    '<td>'+ json_message[i].item_quantity +'</td>' +
                    '<td>'+ total +'</td>' +
                    '</tr>';
            }

            $('#payment_modal_order_table').empty();
            $('#payment_modal_order_table').append(htmlstr);

        },
        error: function (error) {
            swal('Error!', 'Something went wrong', 'error');
        }
    });

    $('#order_details_modal').modal('show');
}


function getRandomNumber(min, max) {
    var number = Math.floor(Math.random() * (max - min + 1)) + min;
    if (temp_bg_color_previous_color == number) {
        temp_bg_color_previous_color = number;
        return getRandomNumber(min, max);
    }
    else {
        temp_bg_color_previous_color = number;
        return number;
    }
}
