
$(document).ready(function() {


    $("#add_modifier_button").click(function () {

        var modifier_name = $('#modifier_name').val();
        var price = $('#price').val()==''?0:$('#price').val();
        var modifier_vat = $('#modifier_vat').val();
        var status = $("#status").is(':checked')?1:0;

        if (modifier_name != '') {

            var params = {
                modifier_name: modifier_name,
                price: price,
                modifier_vat: modifier_vat,
                status: status
            };

            $.ajax({
                url: '/add/modifier',
                type: 'POST',
                format: 'JSON',
                data: {params: params, '_token': $('#token').val()},

                success: function (response) {

                    var htmlstr = '<tr class="even" id="modifier_'+response+'">' +
                        '<td>#</td>' +
                        '<td id="modifier_name_'+response+'">'+modifier_name+'</td>' +
                        '<td id="price_'+response+'">'+price+'</td>' +
                        '<td id="modifier_vat_'+response+'">'+modifier_vat+'</td>' +
                        '<td>'+status+'</td>' +
                        '<td>'+getCurrentDate()+'</td>' +
                        '<td>' +
                        '<button type="button" class="btn btn-dark" onclick="editModifier(\''+response+'\', \''+modifier_name+'\', \''+price+'\', \''+modifier_vat+'\', \''+status+'\')">' +
                        '<i class="glyphicon glyphicon-pencil"></i></button> ' +
                        '<button type="button" class="btn btn-danger" onclick="deleteModifier('+response+')">' +
                        '<i class="glyphicon glyphicon-remove"></i></button></td>' +
                        '</tr>';


                    $('#modifier_table').append(htmlstr);
                    //swal("Done!", "Successfully Added Modifier To Database", "success");

                },
                error: function (error) {
                    swal("Error!", "Something went wrong", "error");
                }
            });
        }

    });


    $('#edit_modifier_button').click(function () {

        var modifier_id = $('#edit_modifier_id').val();
        var modifier_name = $('#edit_modifier_name').val();
        var price = $('#edit_price').val();
        var modifier_vat = $('#edit_modifier_vat').val();
        var status = $("#edit_status").is(':checked')?1:0;

        if (modifier_name != "") {

            var params = {
                modifier_id: modifier_id,
                modifier_name: modifier_name,
                price: price,
                modifier_vat: modifier_vat,
                status: status
            };

            $.ajax({
                url: '/edit/modifier',
                type: 'POST',
                format: 'JSON',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {

                    $('#modifier_name_' + modifier_id).text(modifier_name);
                    $('#price_' + modifier_id).text(price);
                    $('#modifier_vat_' + modifier_id).text(modifier_vat);

                    //swal('Done!', 'Successfully Updated Modifier', 'success');

                },
                error: function (error) {
                    swal('Error!', 'Something went wrong.', 'error');
                }
            });
        }
    });


});



function editModifier(modifier_id, modifier_name, price, modifier_vat, status) {

    $("#edit_modifier_id").val(modifier_id);
    $("#edit_modifier_name").val(modifier_name);
    $("#edit_price").val(price);
    $("#edit_modifier_vat").val(modifier_vat);

    if (status) $('#edit_status').prop('checked', true);
    else $('#edit_status').attr('checked', false);

    $('#edit_modifier_modal').modal('show');
}


function deleteModifier(modifier_id) {

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-danger',
        cancelButtonClass: 'btn btn-success',
        buttonsStyling: false
    }).then(function () {

        var params = {
            modifier_id: modifier_id
        };

        $.ajax({
            url: '/delete/modifier',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {

                $("#modifier_" + modifier_id).remove();

                swal('Done', 'Modifier Has Been Deleted', 'success');
            },
            error: function (error) {
                console.log("Error! Something went wrong.");
            }
        });

    });
}









