var menu_items = [];
var setmenu_items = [];
var modifiers = [];
var order_modifiers = [];
var kitchen_token = [];
var instructions = [];
var payment_methods = [];

var vat_no = "";
var vat_percentage = 0.0;
var service_charge = 0.0;
var discount = 0.0;
var bill_with_discount = 0.0;
var temp_bg_color_previous_color = -1;
var guest_bill=0;
var kitchen_type='single';
var instruction_count = 1;

var seconds = 0, minutes = 0, hours = 0, t, time;

var paid_amount = 0;


var background_color = [
    "#26A65B",
    "#F89406",
    "#913D88",
    "#FF5A5E",
    "#46BFBD",
    "#34495e",
    "#947CB0"
];


$(document).ready(function() {









});


function giveNote(amount) {

    paid_amount += parseInt(amount);
    displayPaidAmount();


}


function addNumber(number) {

    var paid_amount_str = paid_amount==0?'':paid_amount.toString();

    var output = paid_amount_str + number.toString();
    paid_amount = parseFloat(output);

    displayPaidAmount();

}


function displayPaidAmount() {

    var total_amount = parseFloat($('#total_amount').val());

    $('#paid_amount').val(paid_amount);
    $('#change_amount').val(parseFloat(paid_amount) - total_amount);
}


function clearPaidAmount() {

    paid_amount = 0;
    displayPaidAmount();

}


function generateReceipt(order_id) {

    var params = {
        order_id: order_id
    };

    $.ajax({
        url: '/get/order/history',
        type: 'POST',
        format: 'JSON',
        data: {params: params, "_token": $('#token').val()},

        success: function (response) {

            var json_message = JSON.parse(response);

            var order_data = json_message.order_data;
            var order_details = json_message.order_details;
            var item_modifiers = json_message.order_modifiers;

            var discount = order_data.discount>0.0? '-' + order_data.discount:'0';

            var discount_amount_type;
            var discount_percentage;

            if (order_data.discount_amount_type=='cash') {

                discount_percentage = order_data.discount;
                discount_amount_type = 'Tk';

            }

            else {

                discount_percentage = order_data.discount_percentage;
                discount_amount_type = '%';

            }

            var payment_modal_count = 1;
            var htmlstr = '';
            var htmlstr_modal = '';


            //console.log(order_details);

            for (var i=0; i<order_details.length; i++) {

                var order_details_id = order_details[i].order_details_id;
                var menu_item_id = order_details[i].menu_item_id;
                var menu_item_name = order_details[i].menu_item_name;
                var ratio = order_details[i].ratio != null?order_details[i].ratio:'';
                var item_price = parseInt(order_details[i].item_price);
                var item_discount = parseInt(order_details[i].item_discount);
                var item_quantity = parseInt(order_details[i].item_quantity);
                var unit_price = ((item_price - item_discount));
                var item_status = parseInt(order_details[i].item_status);
                var total_cost = (unit_price * item_quantity);

                var modifiers_str = '';
                var item_modifier_price = 0;


                for (var j=0; j<item_modifiers.length; j++) {

                    var modifier_order_details_id = item_modifiers[j].order_details_id;
                    var modifier_name = item_modifiers[j].modifier_name;
                    var modifier_price = item_modifiers[j].price;
                    var modifier_quantity = item_modifiers[j].quantity;

                    if (order_details_id == modifier_order_details_id) {

                        modifiers_str += ', ' + modifier_name + ' x' + modifier_quantity;
                        item_modifier_price += (parseInt(modifier_price) * parseInt(modifier_quantity));
                        unit_price += parseInt(modifier_price);

                    }

                }


                /*
                 previous_cart_table
                 */

                if (item_quantity > 0) {

                    htmlstr += '<tr class="even pointer';
                    if (item_status == 0) htmlstr += ' strike';
                    htmlstr += '" id="order_details_'+ order_details_id +'">' +
                        '<td>'+ menu_item_name + ' ' + ratio + modifiers_str +'</td>' +
                        '<td>'+ item_quantity +'</td>' +
                        '<td>' +
                        '<button type="button" class="btn btn-danger btn-xs pointer" ';
                    if (item_status != 0) htmlstr += 'onclick="void_item(\'' + order_details_id + '\', \''+item_quantity+'\')"';
                    htmlstr += '>Void</button> ';

                    if (item_status != 0 && item_price>0) {

                        htmlstr += '<button type="button" class="btn btn-primary btn-xs pointer" ' +
                            'onclick="complimentary_item(\''+order_details_id+'\', \''+item_quantity+'\')">Complimentary</button>';

                    }

                    htmlstr += '</td></tr>';

                }



                /*
                 receipt_order_table
                 */

                if (item_quantity > 0) {

                    htmlstr_modal += '<tr>' +
                        '<th scope="row">'+ payment_modal_count++ +'</th>' +
                        '<td>'+ menu_item_name + ' ' + ratio + modifiers_str +'</td>' +
                        '<td>'+ unit_price +'</td>' +
                        '<td>'+ item_quantity +'</td>' +
                        '<td class="text-right">'+ (total_cost + item_modifier_price) +'</td></tr>';

                }



            }


            $( "#previous_cart_table" ).empty();
            $( "#previous_cart_table" ).append(htmlstr);

            $( ".receipt_order_table" ).empty();
            $( ".receipt_order_table" ).append(htmlstr_modal);


            $( ".receipt_customer_name" ).text(order_data.customer_name);
            $( ".receipt_waiter_name" ).text(order_data.waiter_name);
            $( ".receipt_created_at" ).text(formatDate(order_data.updated_at));
            $( ".receipt_table_no" ).text(order_data.table_name);
            $( ".receipt_order_no" ).text(formatInvoiceDate(order_data.created_at) + numberPadding(order_data.order_id));
            $( ".receipt_order_type" ).text(uc_first(order_data.order_type));


            $( ".receipt_subtotal" ).text(order_data.bill);
            $( ".receipt_vat_total" ).text(order_data.vat);
            $( ".receipt_service_charge" ).text(order_data.service_charge);
            $( ".receipt_sd_percentage" ).text(order_data.sd_charge);
            $( ".receipt_service_charge_vat_percentage" ).text(order_data.service_charge_vat);
            $( ".receipt_sd_vat_percentage" ).text(order_data.sd_vat);

            $( ".receipt_discount_percentage" ).text(discount_percentage + discount_amount_type);

            $( ".receipt_discount_amount" ).text(discount);
            $( ".receipt_total_amount" ).text(order_data.total_bill);

            $( "#order_amount_total" ).text(order_data.total_bill);
            $( ".discount_amount" ).text(order_data.discount);
            $( ".order_net_amaount" ).text(order_data.total_bill - order_data.discount);


            printCart();




        },
        error: function (error) {
            swal('Error!', 'Something went wrong', 'error');
        }
    });

}


