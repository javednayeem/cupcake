var temp_response = [];

var price_including_vat, vat_percentage = 0;

$(document).ready(function() {


    $('#select_report_type').change(function () {
        var select_report_type = $('#select_report_type').val();

        if (select_report_type == 'waiter_wise_sale') {

            $('#select_table_div').hide();
            $('#select_order_id').hide();
            $('#select_item_div').hide();
            $('#select_menu_div').hide();


            $('#select_waiter_div').show();


            $('#report_header').text('Waiter Wise Sale');
        }

        else if (select_report_type == 'table_wise_sale') {
            $('#select_waiter_div').hide();
            $('#select_order_id').hide();
            $('#select_item_div').hide();
            $('#select_menu_div').hide();
            $('#select_table_div').show();
            // $('#select_start_date').hide();
            // $('#select_end_date').hide();
        }

        else if (select_report_type == 'invoice_wise_sale') {

            $('#select_waiter_div').hide();
            $('#select_table_div').hide();
            $('#select_order_id').hide();
            $('#select_item_div').hide();
            $('#select_menu_div').hide();


            $('#report_header').text('Invoice Wise Sales');
        }


        else if (select_report_type == 'item_wise_sale') {

            $('#select_waiter_div').hide();
            $('#select_table_div').hide();
            $('#select_order_id').hide();
            $('#select_menu_div').hide();


            $('#select_item_div').show();


            $('#report_header').text('Item Wise Sale');
        }


        else if (select_report_type == 'category_wise_sale') {

            $('#select_waiter_div').hide();
            $('#select_table_div').hide();
            $('#select_order_id').hide();
            $('#select_item_div').hide();


            $('#select_menu_div').show();


            $('#report_header').text('Category Wise Sale');
        }

        else if (select_report_type == 'order_summary') {
            $('#select_waiter_div').hide();
            $('#select_table_div').hide();
            $('#select_item_div').hide();
            $('#select_menu_div').hide();
            $('#select_order_id').show();
            //$('#select_start_date').hide();
            //$('#select_end_date').hide();
        }

        else if (select_report_type == 'month_wise_sale') {
            $('#select_waiter_div').hide();
            $('#select_table_div').hide();
            $('#select_order_id').hide();
            $('#select_item_div').hide();
            $('#select_menu_div').hide();

            $('#select_start_date').show();
            $('#select_end_date').show();
        }

    });


    $('#generate_report_button').click(function () {

        var select_report_type = $('#select_report_type').val();
        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();

        if (select_report_type == '0') {
            showWarningNotification('Please Select Any Report Type');
        }

        else {

            var params;

            if (select_report_type == 'waiter_wise_sale') {
                var waiter_id = $('#select_waiter').val();
                params = {
                    select_report_type: select_report_type,
                    waiter_id: waiter_id,
                    start_date: start_date,
                    end_date: end_date
                }
            }

            else if (select_report_type == 'table_wise_sale') {
                var table_id = $('#select_table').val();
                params = {
                    select_report_type: select_report_type,
                    table_id: table_id
                }
            }

            else if (select_report_type == 'invoice_wise_sale') {
                params = {
                    select_report_type: select_report_type,
                    start_date: start_date,
                    end_date: end_date
                };
            }

            else if (select_report_type == 'order_summary') {
                var order_id = $('#order_id_input').val();
                params = {
                    select_report_type: select_report_type,
                    order_id: order_id
                }
            }

            else if (select_report_type == 'month_wise_sale') {

                params = {
                    select_report_type: select_report_type,
                    start_date: start_date,
                    end_date: end_date
                }
            }

            else if (select_report_type == 'item_wise_sale') {

                params = {
                    select_report_type: select_report_type,
                    item_id: $('#select_item').val(),
                    start_date: start_date,
                    end_date: end_date
                }
            }

            else if (select_report_type == 'category_wise_sale') {

                params = {
                    select_report_type: select_report_type,
                    menu_id: $('#menu_id').val(),
                    start_date: start_date,
                    end_date: end_date
                }
            }

            $.ajax({
                url: '/generate/report',
                type: 'POST',
                format: 'JSON',
                data: {params:params, '_token': $('#token').val()},
                success: function (response) {
                    temp_response = response;
                    print_table(response, 'all', select_report_type);

                    var start_datetime = new Date(start_date);
                    var formatted_start = start_datetime.getDate() + "-" + months[start_datetime.getMonth()] + "-" + start_datetime.getFullYear();

                    var end_datetime = new Date(end_date);
                    var formatted_end = end_datetime.getDate() + "-" + months[end_datetime.getMonth()] + "-" + end_datetime.getFullYear();

                    $('#date_header').text(formatted_start + ' To ' + formatted_end);
                },
                error: function (error) {
                    showErrorNotification();
                }
            });
        }

    });


    $('#payment_method').change(function () {

        var payment_method = $('#payment_method').val();
        print_table(temp_response, payment_method);



    });


    $('#print_receipt_button').click(function () {

        w = window.open();
        w.document.write($('#settle_div').html());
        w.print();
        w.close();

    });


    $('#email_report_button').click(function () {

        var select_report_type = $('#select_report_type').val();
        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();

        if (select_report_type != 0) {

            var params;

            if (select_report_type == 'waiter_wise_sale') {
                var waiter_id = $('#select_waiter').val();
                params = {
                    select_report_type: select_report_type,
                    waiter_id: waiter_id,
                    start_date: start_date,
                    end_date: end_date
                }
            }

            else if (select_report_type == 'table_wise_sale') {
                var table_id = $('#select_table').val();
                params = {
                    select_report_type: select_report_type,
                    table_id: table_id
                }
            }

            else if (select_report_type == 'invoice_wise_sale') {
                params = {
                    select_report_type: select_report_type,
                    start_date: start_date,
                    end_date: end_date
                };
            }

            else if (select_report_type == 'order_summary') {
                var order_id = $('#order_id_input').val();
                params = {
                    select_report_type: select_report_type,
                    order_id: order_id
                }
            }

            else if (select_report_type == 'month_wise_sale') {

                params = {
                    select_report_type: select_report_type,
                    start_date: start_date,
                    end_date: end_date
                }
            }

            else if (select_report_type == 'item_wise_sale') {

                params = {
                    select_report_type: select_report_type,
                    item_id: $('#select_item').val(),
                    start_date: start_date,
                    end_date: end_date
                }
            }

            else if (select_report_type == 'category_wise_sale') {

                params = {
                    select_report_type: select_report_type,
                    menu_id: $('#menu_id').val(),
                    start_date: start_date,
                    end_date: end_date
                }
            }



            $.ajax({
                url: '/emailReport',
                type: 'POST',
                format: 'JSON',
                data: {'_token': $('#token').val(), 'params': params},

                success: function (response) {
                    showSuccessNotification('Email Has Been Sent!');
                },

                error: function (error) {
                    showErrorNotification();
                }
            });
        }


    });


});



function pushChargesInfo(price_including_vat, vat_percentage) {

    this.price_including_vat = isNaN(price_including_vat)?0:price_including_vat;
    this.vat_percentage = isNaN(vat_percentage)?0:vat_percentage;

     //console.log("price_including_vat : " + price_including_vat);

}


function calculatePercentage(amount, percentage) {

    var result = parseFloat(amount * (percentage/100.00));
    return result;

}


function print_table(response, payment, select_report_type) {

    var json_message = $.parseJSON(response);

    var total_sales = 0;
    var total_vat = 0;
    var total_service_charge = 0;
    var total_service_charge_VAT = 0;
    var total_sd = 0;
    var total_discount = 0;
    var total = 0;
    var htmlstr = '';
    var htmlstr_header = '';

    if (select_report_type == 'invoice_wise_sale') {

        htmlstr_header = '<th class="column-title text-center">Invoice #</th>' +
            '<th class="column-title text-center">Served By</th>' +
            '<th class="column-title text-center">Item Qty</th>' +
            '<th class="column-title text-center">Item Amount</th>' +
            '<th class="column-title text-center">VAT</th>' +
            '<th class="column-title text-center">Service Charge</th>' +
            '<th class="column-title text-center">Service Charge VAT</th>' +
            '<th class="column-title text-center">SD</th>' +
            '<th class="column-title text-center">Discount</th>' +
            '<th class="column-title text-center">Discount Ref.</th>' +
            '<th class="column-title text-center">Net Amount</th>';

    }


    else if (select_report_type == 'item_wise_sale') {

        htmlstr_header = '<th class="column-title text-center">Invoice #</th>' +
            '<th class="column-title text-center">Served By</th>' +
            '<th class="column-title text-center">Item Name</th>' +
            '<th class="column-title text-center">Item Qty</th>';

    }


    else if (select_report_type == 'category_wise_sale') {

        var temp_header = '';
        //if (price_including_vat == 1) temp_header = '(Prices are VAT included)';

        htmlstr_header = '<th class="column-title text-center">Category Name</th>' +
            '<th class="text-center">Item Qty</th>' +
            '<th class="text-center">Item Price</th>' +
            '<th class="text-center">Item VAT '+ temp_header +'</th>' +
            '<th class="text-center">Total Price</th>';

    }


    else if (select_report_type == 'waiter_wise_sale') {

        htmlstr_header = '<th class="column-title text-center">Invoice #</th>' +
            '<th class="column-title text-center">Waiter Name</th>' +
            '<th class="column-title text-center">Total Amount</th>';

    }


    $('#report_table_header').empty();
    $( "#report_table_header" ).append(htmlstr_header);

    if (select_report_type == 'category_wise_sale') {

        var menu_array = [];

        json_message = JSON.parse(response);

        var menus = json_message.menus;
        var reports = json_message.reports;
        var menu_items = json_message.menu_items;

        var total_item_quantity = 0;
        var total_price = 0;
        var total_item_vat = 0;
        var total_item_price = 0;


        for (var i=0; i<menus.length; i++) {

            var menu = {
                menu_id: menus[i].menu_id,
                menu_name: menus[i].menu_name,
                quantity: 0,
                price: 0,
                vat: 0
            };

            menu_array.push(menu);

        }



        for (var i = 0; i < reports.length; i++) {

            var item = reports[i];

            var item_menu_id = item.menu_id;
            var item_price = item.item_price;
            var item_quantity = item.item_quantity;
            var item_vat = item.item_vat;


            for (var j=0; j<menu_array.length; j++) {

                var menu = menu_array[j];
                var menu_id = menu.menu_id;

                if (menu_id == item_menu_id) {

                    var this_item_price = (item_price * item_quantity);
                    var this_item_vat = ((this_item_price * item_vat)/100.0);

                    menu_array[j].price += this_item_price;
                    menu_array[j].quantity += item_quantity;
                    menu_array[j].vat += this_item_vat;

                }

            }


        }


        for (var i = 0; i < menu_array.length; i++) {

            var menu = menu_array[i];

            var menu_id = menu.menu_id;
            var menu_name = menu.menu_name;
            var quantity = menu.quantity;
            var price = menu.price;
            var vat = menu.vat;

            total_item_quantity += parseInt(quantity);
            total_price += price;


            if (price_including_vat == 0) total_item_price += (price + vat);
            else total_item_price += (price);

            if (quantity>0) {

                var temp_price = price;
                var temp_total_price = price;

                if (price_including_vat == 0) temp_total_price += vat;
                else {
                    temp_price = Math.round((temp_price * 100.0)/(100.0 + parseFloat(vat_percentage)));
                    vat = Math.round((temp_price * vat_percentage)/100.0);
                }


                total_item_vat += vat;

                htmlstr += '<tr class="even pointer">' +
                    '<td class="text-center">'+ menu_name +'</td>' +
                    '<td class="text-center">'+ quantity +'</td>' +
                    '<td class="text-center">'+ temp_price +'</td>' +
                    '<td class="text-center">'+ vat +'</td>' +
                    '<td class="text-center">'+ (temp_total_price) +'</td>' +
                    '</tr>';

            }

        }


        for (var i=0; i<menu_items.length; i++) {

            var item_name = menu_items[i].item_name;
            var item_quantity = menu_items[i].item_quantity;
            var item_price = menu_items[i].item_price;
            var item_vat = menu_items[i].item_vat;

            var this_item_price = (item_quantity * item_price);
            var this_item_vat = ((this_item_price * item_vat)/100.0);




            if (item_quantity>0) {

                var this_item_price_vat = 0;

                if (price_including_vat == 1) {
                    item_price = Math.round((item_price * 100.0)/(100.0 + parseFloat(vat_percentage)));
                    this_item_vat = Math.round((item_price * vat_percentage)/100.0);
                    this_item_price_vat = (item_price * item_quantity) + this_item_vat;
                }

                else {

                    this_item_price_vat = this_item_price + this_item_vat;

                }

                this_item_price = (item_quantity * item_price);

                htmlstr += '<tr class="even pointer">' +
                    '<td class="text-center">'+ item_name +'</td>' +
                    '<td class="text-center">'+ item_quantity +'</td>' +
                    '<td class="text-center">'+ this_item_price +'</td>' +
                    '<td class="text-center">'+ Math.round(this_item_vat) +'</td>' +
                    '<td class="text-center">'+ Math.round(this_item_price_vat) +'</td>' +
                    '</tr>';

            }


        }



        htmlstr += '<tr class="even pointer">' +
            '<td class="text-center"><b>Total</b></td>' +
            '<td class="text-center"><b>'+total_item_quantity+'</b></td>' +
            '<td class="text-center"><b>'+total_price+'</b></td>' +
            '<td class="text-center"><b>'+total_item_vat+'</b></td>' +
            '<td class="text-center"><b>'+total_item_price+'</b></td>' +
            '</tr>';




    }

    else {

        for (var i=0; i<json_message.length; i++) {

            var order_id = json_message[i].order_id;
            var waiter_name = json_message[i].waiter_name;
            var time = json_message[i].updated_at;
            var customer_name = json_message[i].customer_name;
            var table_name = json_message[i].table_name;
            var bill = parseFloat(json_message[i].bill);
            var vat = parseFloat(json_message[i].vat);
            var total_item = parseInt(json_message[i].total_item);
            var service_charge = parseFloat(json_message[i].service_charge);
            var service_charge_vat = parseFloat(json_message[i].service_charge_vat);
            var sd_charge = parseFloat(json_message[i].sd_charge);
            var total_bill = json_message[i].total_bill;
            //var payment_method = json_message[i].payment_method;
            var given_amount = json_message[i].given_amount==null ? '': json_message[i].given_amount;

            // var invoice_no = replaceAll(time.split(" ")[0], "-", "") + order_id.pad(5 - order_id.toString().length);
            //var invoice_no = replaceAll(time.split(" ")[0], "-", "") + order_id;
            var invoice_no = formatInvoiceDate(time) + numberPadding(order_id);

            var t_total_bill = (json_message[i].bill + json_message[i].service_charge + json_message[i].vat - json_message[i].discount);
            var discount = json_message[i].discount == null ? 0:parseFloat(json_message[i].discount);
            var discount_reference = json_message[i].discount_reference==null? '':json_message[i].discount_reference;





            if (select_report_type == 'invoice_wise_sale') {

                htmlstr += '<tr class="even pointer" onclick="generateReceipt('+order_id+')">' +
                    '<td class="text-center">'+ invoice_no +'</td>' +
                    '<td class="text-center">'+ waiter_name +'</td>' +
                    '<td class="text-center">'+ total_item +'</td>' +
                    '<td class="text-center">'+ bill +'</td>' +
                    '<td class="text-center">'+ vat +'</td>' +
                    '<td class="text-center">'+ service_charge +'</td>' +
                    '<td class="text-center">'+ service_charge_vat +'</td>' +
                    '<td class="text-center">'+ sd_charge +'</td>' +
                    '<td class="text-center">'+ discount +'</td>' +
                    '<td class="text-center">'+ discount_reference +'</td>' +
                    '<td class="text-center">'+ json_message[i].total_bill +'</td>';

                total_sales += bill;
                total_vat += vat;
                total_service_charge += service_charge;
                total_service_charge_VAT += service_charge_vat;
                total_sd += sd_charge;
                total_discount += discount;
                total += parseFloat(json_message[i].total_bill);

            }


            else if (select_report_type == 'item_wise_sale') {

                var item_name = json_message[i].item_name;
                var ratio = json_message[i].ratio;
                var item_quantity = json_message[i].item_quantity;
                var item_price = json_message[i].item_price;
                var item_discount = json_message[i].item_discount;
                var item_vat = json_message[i].item_vat;
                var total_item_price = parseFloat(item_price) * parseFloat(item_quantity);

                htmlstr += '<tr class="even pointer">' +
                    '<td class="text-center">'+ invoice_no +'</td>' +
                    '<td class="text-center">'+ waiter_name +'</td>' +
                    '<td class="text-center">'+ item_name + ' ' + ratio +'</td>' +
                    '<td class="text-center">'+ item_quantity +'</td>' +
                    '</tr>';

                total_sales += total_item_price;
                total_vat += calculatePercentage(total_item_price, item_vat);
                total_service_charge += service_charge;
                total_discount += parseFloat(item_discount) * parseFloat(item_quantity);

            }


            else if (select_report_type == 'waiter_wise_sale') {

                htmlstr += '<tr class="even pointer">' +
                    '<td class="text-center">'+ invoice_no +'</td>' +
                    '<td class="text-center">'+ waiter_name +'</td>' +
                    '<td class="text-center">'+ total_bill +'</td>' +
                    '</tr>';

                total_sales += bill;
                total_vat += vat;
                total_service_charge += service_charge;
                total_discount += discount;

            }

        }

        if (select_report_type == 'invoice_wise_sale') {

            htmlstr += '<tr class="even pointer">' +
                '<td class="text-center"><b>Total</b></td>' +
                '<td></td>' +
                '<td></td>' +
                '<td class="text-center"><b>' + total_sales +'</b></td>' +
                '<td class="text-center"><b>' + total_vat +'</b></td>' +
                '<td class="text-center"><b>' + total_service_charge +'</b></td>' +
                '<td class="text-center"><b>' + total_service_charge_VAT +'</b></td>' +
                '<td class="text-center"><b>' + total_sd +'</b></td>' +
                '<td class="text-center"><b>' + total_discount +'</b></td>' +
                '<td></td>' +
                '<td class="text-center"><b>'+ total +'</b></td>' +
                '<td></td></tr>';



        }


    }


    $('#report_table_tbody').empty();
    $('#report_table_tbody').append(htmlstr);

    //$('#report_table').DataTable();

    $( "#total_sales" ).text(total_sales + '৳');
    $( "#total_vat" ).text(total_vat + '৳');
    $( "#total_service_charge" ).text(total_service_charge + '৳');
    $( "#total_discount" ).text(total_discount + '৳');



}


Number.prototype.pad = function(n) {
    return new Array(n).join('0').slice((n || 2) * -1) + this;
};


function escapeRegExp(str) {
    return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}


function replaceAll(str, find, replace) {
    return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}


function printReport() {
    window.print();
}


function generateReceipt(order_id) {

    var params = {
        order_id: order_id
    };

    $.ajax({
        url: '/get/order/history',
        type: 'POST',
        format: 'JSON',
        data: {params: params, "_token": $('#token').val()},

        success: function (response) {

            var json_message = JSON.parse(response);

            var order_data = json_message.order_data;
            var order_details = json_message.order_details;
            var item_modifiers = json_message.order_modifiers;
            var order_payments = json_message.order_payments;

            var payment_modal_count = 1;
            var htmlstr_modal = '';
            var htmlstr_payment = '';
            var total_amount = 0;


            //console.log(order_details);

            for (var i=0; i<order_details.length; i++) {

                var order_details_id = order_details[i].order_details_id;
                var menu_item_id = order_details[i].menu_item_id;
                var menu_item_name = order_details[i].menu_item_name;
                var item_price = parseInt(order_details[i].item_price);
                var item_discount = parseInt(order_details[i].item_discount);
                var item_quantity = parseInt(order_details[i].item_quantity);
                var unit_price = ((item_price - item_discount));
                var item_status = parseInt(order_details[i].item_status);
                var total_cost = (unit_price * item_quantity);

                var modifiers_str = '';
                var item_modifier_price = 0;


                for (var j=0; j<item_modifiers.length; j++) {

                    var modifier_order_details_id = item_modifiers[j].order_details_id;
                    var modifier_name = item_modifiers[j].modifier_name;
                    var modifier_price = item_modifiers[j].price;
                    var modifier_quantity = item_modifiers[j].quantity;

                    if (order_details_id == modifier_order_details_id) {

                        modifiers_str += ', ' + modifier_name + ' x' + modifier_quantity;
                        item_modifier_price += (parseInt(modifier_price) * parseInt(modifier_quantity));
                        unit_price += parseInt(modifier_price);

                    }

                }


                /*
                 receipt_order_table
                 */

                if (item_quantity > 0) {

                    htmlstr_modal += '<tr>' +
                        '<th scope="row">'+ payment_modal_count++ +'</th>' +
                        '<td>'+ menu_item_name + modifiers_str +'</td>' +
                        '<td>'+ unit_price +'</td>' +
                        '<td>'+ item_quantity +'</td>' +
                        '<td>'+ (total_cost + item_modifier_price) +'</td></tr>';

                }




            }


            for (var i=0; i<order_payments.length; i++) {

                var payment_method = order_payments[i].payment_method;
                var amount = order_payments[i].amount;
                var card_name = order_payments[i].card_name==null?'':order_payments[i].card_name;
                var card_number = order_payments[i].card_number==null?'':order_payments[i].card_number;
                total_amount += parseFloat(amount);

                /*
                 Payment Method Table
                 */

                htmlstr_payment += '<tr>' +
                    '<td>'+ payment_method +'</td>' +
                    '<td>'+ card_name +'</td>' +
                    '<td>'+ card_number +'</td>' +
                    '<td class="text-right">'+ amount +'</td></tr>';
            }

            htmlstr_payment += '<tr>' +
                '<td></td>' +
                '<td></td>' +
                '<td></td>' +
                '<td class="text-right"><b>'+ total_amount +'</b></td></tr>';




            $( ".receipt_order_table" ).empty();
            $( ".receipt_order_table" ).append(htmlstr_modal);


            $( ".payment_table" ).empty();
            $( ".payment_table" ).append(htmlstr_payment);


            $( ".receipt_customer_name" ).text(order_data.customer_name);
            $( ".receipt_waiter_name" ).text(order_data.waiter_name);
            $( ".receipt_created_at" ).text(formatDate(order_data.created_at));
            $( ".receipt_table_no" ).text(order_data.table_name);
            $( ".receipt_order_no" ).text(formatInvoiceDate(order_data.created_at) + numberPadding(order_data.order_id));
            $( ".receipt_order_type" ).text(uc_first(order_data.order_type));


            $( ".receipt_subtotal" ).text(order_data.bill);
            $( ".receipt_vat_total" ).text(order_data.vat);
            $( ".receipt_service_charge" ).text(order_data.service_charge);
            $( ".receipt_sd_percentage" ).text(order_data.sd_charge);
            $( ".receipt_service_charge_vat_percentage" ).text(order_data.service_charge_vat);
            $( ".receipt_sd_vat_percentage" ).text(order_data.sd_vat);
            $( ".receipt_discount_amount" ).text(order_data.discount);
            $( ".receipt_total_amount" ).text(order_data.total_bill);


            $('#order_details_modal').modal('show');




        },
        error: function (error) {
            swal('Error!', 'Something went wrong', 'error');
        }
    });

}