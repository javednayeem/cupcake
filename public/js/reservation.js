$(document).ready(function() {

    $('#item_id').select2();


    $('.date').datetimepicker({
        format: 'HH:mm'
    });


    $('#customer_id').select2();


    $('#create_reservation_button').click(function () {

        var customer_id = $('#customer_id').val();
        var reservation_date = $('#single_cal2').val();
        var start_time = $('#start_time').val();
        var end_time = $('#end_time').val();
        var description = $('#description').val();
        var food_menu = $('#food_menu').val();
        var subtotal = $('#subtotal').val();
        var advance = $('#advance').val();
        var guest = $('#guest').val();
        var tables = getTablesId();

        if (tables != '') {

            var params = {
                customer_id: customer_id,
                reservation_date: reservation_date,
                start_time: start_time,
                end_time: end_time,
                description: description,
                food_menu: food_menu,
                subtotal: subtotal,
                advance: advance,
                guest: guest,
                tables: tables
            };

            $.ajax({

                url: '/create/reservation',
                type: 'POST',
                format: 'JSON',
                data: { '_token': $('#token').val(), params: params},

                success: function (response) {

                    showSuccessNotification('Reservation has been created successfully');

                    redirect('/reservation/receipt/' + response, 1);

                },

                error: function (error) {

                    swal('Error!', 'Something Went Wrong', 'error');

                }

            });
        }

    });


    $('.check_availablity').click(function () {

        var reservation_date = $('#single_cal2').val();

        if (reservation_date != '') {

            var params = {
                reservation_date: reservation_date
            };

            $.ajax({
                url: '/check/availablity',
                type: 'POST',
                format: 'JSON',
                data: {'_token': $('#token').val(), params: params},

                success: function (response) {

                    var json_message = $.parseJSON(response);

                    var htmlstr = '';
                    var htmlstr_status = '';

                    var reservation_count = json_message.length;

                    if (reservation_count == 0) {

                        htmlstr_status = '<div class="alert alert-success alert-dismissible fade in" role="alert">' +
                            '<h3>There are no reservations on '+reservation_date+'</h3></div>';

                        $('#create_reservation_button').prop('disabled', false);


                    }

                    else {

                        $('#create_reservation_button').prop('disabled', true);

                        htmlstr_status = '<div class="alert alert-success alert-dismissible fade in" role="alert">' +
                            '<h3>There are '+reservation_count+' reservations on '+reservation_date+'</h3></div>';

                        for (var i=0; i<json_message.length; i++) {

                            var customer_name = json_message[i].customer_name;
                            var start_time = json_message[i].start_time;
                            var end_time = json_message[i].end_time;
                            var description = json_message[i].description;
                            var guest = json_message[i].guest;
                            var tables = json_message[i].tables;
                            var htmlstr_tables = '';

                            tables = tables.split("|");

                            checkConflictingTables(tables);

                            for (var j=0; j<tables.length; j++) {
                                htmlstr_tables += '<p class="excerpt">Table '+tables[j]+'</p>';
                            }


                            htmlstr += '<li>' +
                                '<div class="block"><div class="tags">' +
                                '<a href="" class="tag"><span>Booking '+(i+1)+'</span></a>' +
                                '</div>' +
                                '<div class="block_content"><h2 class="title">' +
                                '<a>'+description+'</a></h2>' +
                                '<div class="byline">' +
                                '<span>From '+start_time+' To '+end_time+' </span>' +
                                '<a>By '+customer_name+'</a></div>' +
                                htmlstr_tables +
                                '</div></div></li>';

                        }

                    }


                    $( "#availablity_status_span" ).empty();
                    $( "#availablity_status_span" ).append(htmlstr_status);

                    $( "#reservations_table" ).empty();
                    $( "#reservations_table" ).append(htmlstr);

                },

                error: function (error) {

                }

            });
        }


    });


    $("#add_customer_button").click(function(){

        var customer_name = $('#customer_name').val();
        var customer_phone = $('#customer_phone').val();
        var customer_email = $('#customer_email').val();
        var customer_address = $('#customer_address').val();
        var card_no = $('#card_no').val();
        var discount_percentage = $('#discount_percentage').val();

        if (customer_name != "") {

            var params = {
                customer_name: customer_name,
                customer_phone: customer_phone,
                customer_email: customer_email,
                customer_address: customer_address,
                card_no: card_no,
                discount_percentage: discount_percentage
            };

            $.ajax({
                url: '/add/customer',
                type: 'POST',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {

                    var htmlstr = '<option value="'+response+'">'+customer_name+'</option>';

                    $( "#customer_id" ).append(htmlstr);

                },
                error: function (error) {
                    console.log("Error! Something went wrong.");
                }
            });
        }

        else {
            swal({
                type: 'warning',
                html: $('<div>')
                    .addClass('some-class')
                    .text('Input Customer Name and Phone Number'),
                animation: false,
                customClass: 'animated bounceIn'
            });
        }
    });


    $('#generate_report_button').click(function () {

        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();

        var params = {
            start_date: start_date,
            end_date: end_date
        };

        $.ajax({
            url: '/generate/reservation-report',
            type: 'POST',
            format: 'JSON',
            data: {params:params, '_token': $('#token').val()},
            success: function (response) {

                var start_datetime = new Date(start_date);
                var formatted_start = start_datetime.getDate() + "-" + months[start_datetime.getMonth()] + "-" + start_datetime.getFullYear();

                var end_datetime = new Date(end_date);
                var formatted_end = end_datetime.getDate() + "-" + months[end_datetime.getMonth()] + "-" + end_datetime.getFullYear();

                $('#date_header').text(formatted_start + ' To ' + formatted_end);

                var json_message = $.parseJSON(response);
                var htmlstr = '';

                for (var i=0; i<json_message.length; i++) {

                    var reservation_id = json_message[i].reservation_id;
                    var customer_name = json_message[i].customer_name;
                    var description = json_message[i].description;
                    var reservation_date = json_message[i].reservation_date;
                    var guest = json_message[i].guest;
                    var start_time = json_message[i].start_time;
                    var end_time = json_message[i].end_time;
                    var name = json_message[i].name;
                    var created_at = json_message[i].created_at;

                    htmlstr += '<tr id="reservation_'+reservation_id+'">' +
                        '<th scope="row">'+ (i+1) +'</th>' +
                        '<td>'+ customer_name +'</td>' +
                        '<td>'+ description +'</td>' +
                        '<td>'+ formatDate(reservation_date) +'</td>' +
                        '<td>'+ guest+'</td>' +
                        '<td>'+ start_time+'</td>' +
                        '<td>'+ end_time+'</td>' +
                        '<td>'+ name+'</td>' +
                        '<td>'+ formatDate(created_at) +'</td>' +
                        '</tr>';

                }


                $('#reservation_table').empty();
                $('#reservation_table').append(htmlstr);


            },
            error: function (error) {
                showErrorNotification();
            }
        });


    });


    $("#add_menu_button").click(function(){

        var item_id = $('#item_id').val();
        var item_name = $('#item_id:option selected').text();

        alert(item_name);



    });


});


function getTablesId() {

    var tables = [];

    $.each($("input[name='table_id']:checked"), function(){
        tables.push($(this).val());
    });

    return tables.join("|");

}


function deleteReservation(reservation_id) {

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-danger',
        cancelButtonClass: 'btn btn-success',
        buttonsStyling: false
    }).then(function () {

        var params = {
            reservation_id: reservation_id
        };

        $.ajax({
            url: '/delete/reservation',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {

                $("#reservation_" + reservation_id).remove();

                swal('Done', 'Reservation Has Been Deleted', 'success');
            },
            error: function (error) {
                console.log("Error! Something went wrong.");
            }
        });

    });
}


function checkConflictingTables(tables) {

    var current_tables = [];

    $.each($("input[name='table_id']:checked"), function(){
        current_tables.push($(this).val());
    });


    for (var j=0; j<tables.length; j++) {
        var reserved_table = tables[j];

        console.log('reserved_table: ' + reserved_table);
    }



}













