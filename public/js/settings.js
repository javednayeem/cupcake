var flag = true;
var application_menu = [];
var menu_permission = [];

$(document).ready(function() {


    $("#create_table_button").click(function(){

        var table_number = $("#table_no").val();
        var capacity = $("#select_table_capacity").val();


        if (table_number>0) {

            var params = {
                table_number: table_number,
                capacity: capacity
            };

            $.ajax({
                url: '/add/table',
                type: 'POST',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {
                    swal("", table_number + " Tables Created Successfully", "success");

                    setTimeout(function(){
                        window.location.href = "/table-settings";
                    },2000);

                },
                error: function (error) {
                    console.log("Error! Something went wrong.");
                }
            });
        }
        else {
            swal("", "Input field empty", "warning");
        }
    });


    $("#save_charge_button").click(function(){

        var vat_no = $("#vat_no").val();
        var price_including_vat = ( $("#price_including_vat").is(':checked') ) ? 1 : 0;
        var vat_after_discount = ( $("#vat_after_discount").is(':checked') ) ? 1 : 0;
        var vat_percentage = $("#vat_percentage").val();
        var service_charge = $("#service_charge").val();
        var sd_percentage = $("#sd_percentage").val();
        var service_charge_vat_percentage = $("#service_charge_vat_percentage").val();
        var sd_vat_percentage = $("#sd_vat_percentage").val();

        var params = {
            vat_no: vat_no,
            price_including_vat: price_including_vat,
            vat_after_discount: vat_after_discount,
            vat_percentage: vat_percentage,
            service_charge: service_charge,
            sd_percentage: sd_percentage,
            service_charge_vat_percentage: service_charge_vat_percentage,
            sd_vat_percentage: sd_vat_percentage
        };

        $.ajax({
            url: '/edit/charges',
            type: 'POST',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                //swal("", "Restaurant Charges Updated Successfully", "success");
                swal({
                    type: 'success',
                    html: $('<div>')
                        .addClass('some-class')
                        .text('Restaurant Charges Updated Successfully'),
                    animation: false,
                    customClass: 'animated bounceIn'
                });
            },
            error: function (error) {
                console.log("Error! Something went wrong.");
            }
        });
    });


    $("#save_restaurant_info_button").click(function(){

        var restaurants_name = $("#restaurant_name").val();
        var address = $("#restaurant_address").val();
        var area = $("#restaurant_area").val();
        var city = $("#restaurant_city").val();
        var phone_number = $("#restaurant_phone").val();
        var email = $("#restaurant_email").val();
        var website = $("#restaurant_website").val();
        var petty_cash = $("#petty_cash").val()!=''?$("#petty_cash").val():0;

        var vat_no = $("#vat_no").val();
        var price_including_vat = ( $("#price_including_vat").is(':checked') ) ? 1 : 0;
        var vat_after_discount = ( $("#vat_after_discount").is(':checked') ) ? 1 : 0;
        var vat_percentage = $("#vat_percentage").val();
        var service_charge = $("#service_charge").val();
        var sd_percentage = $("#sd_percentage").val();
        var service_charge_vat_percentage = $("#service_charge_vat_percentage").val();
        var sd_vat_percentage = $("#sd_vat_percentage").val();

        if (restaurants_name != "") {

            var params = {
                restaurants_name: restaurants_name,
                address: address,
                area: area,
                city: city,
                phone_number: phone_number,
                email: email,
                website: website,
                petty_cash: petty_cash,
                vat_no: vat_no,
                price_including_vat: price_including_vat,
                vat_after_discount: vat_after_discount,
                vat_percentage: vat_percentage,
                service_charge: service_charge,
                sd_percentage: sd_percentage,
                service_charge_vat_percentage: service_charge_vat_percentage,
                sd_vat_percentage: sd_vat_percentage
            };

            $.ajax({
                url: '/edit/restaurant-info',
                type: 'POST',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {
                    //swal("", "Restaurant Info Updated Successfully", "success");
                    swal({
                        type: 'success',
                        html: $('<div>')
                            .addClass('some-class')
                            .text('Restaurant Info Updated Successfully'),
                        animation: false,
                        customClass: 'animated bounceIn'
                    });
                },
                error: function (error) {
                    console.log("Error! Something went wrong.");
                }
            });

        }

    });


    $("#edit_table_button").click(function(){

        var table_id = $("#edit_table_id").val();
        var table_name = $("#modal_table_name").val();
        var capacity = $("#modal_table_capacity").val();


        if (table_name != "") {

            var params = {
                table_id: table_id,
                table_name: table_name,
                capacity: capacity,
            };

            $.ajax({
                url: '/edit/restaurant-table',
                type: 'POST',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {

                    $("#table_name_" + table_id).text(table_name);
                    $("#table_capacity_" + table_id).text(capacity);

                    showSuccessNotification('Table Info Updated Successfully');
                },
                error: function (error) {
                    showErrorNotification();
                }
            });

        }

    });


    $('#add_table_button').click(function () {

        var table_number = $('#modal_table_number').val();
        var table_capacity = $('#modal_new_table_capacity').val();

        if (table_number > 0 && table_capacity>0) {
            var params = {
                table_number:table_number,
                table_capacity: table_capacity
            };

            $.ajax({
                url: '/add/new-table',
                type: 'POST',
                format : 'JSON',
                data : {params: params, "_token": $('#token').val()},

                success: function (response) {
                    swal("", table_number + " Tables Created Successfully", "success");

                    var htmlstr = '';

                    for (var i=0; i<table_number; i++) {

                        htmlstr += '<tr class="even pointer">' +
                            '<td>#</td>' +
                            '<td>'+ 'New Table-' + (i+1) +'</td>' +
                            '<td>'+ table_capacity +'</td>' +
                            '<td>'+ getCurrentDate() +'</td>' +
                            '<td>'+ getCurrentDate() +'</td>' +
                            '<td>' +
                            '<button type="button" class="btn btn-dark pointer"><i class="glyphicon glyphicon-pencil"></i></button>' +
                            '<button type="button" class="btn btn-danger pointer" onclick="deleteTable('+ response +')">' +
                            '<i class="glyphicon glyphicon-remove"></i></button>' +
                            '</td></tr>';
                    }

                    $('#restaurant_table_list').append(htmlstr);
                    $('#modal_table_number').val('');
                    $('#modal_new_table_capacity').val('');
                },
                error: function (error) {
                    swal("", "Error! Something went wrong", "error");
                }

            });
        }

    });





    $('#table_sales_toggle').click(function () {

        $('.table_sales').each(function () {
            this.checked = flag;
        });

        flag = !flag;

    });


    $('#table_void_toggle').click(function () {

        $('.table_void').each(function () {
            this.checked = flag;
        });

        flag = !flag;

    });


    $('#table_void_permission_toggle').click(function () {

        $('.users').each(function () {
            this.checked = flag;
        });

        flag = !flag;

    });


    $('#change_sales_permission').click(function () {

        var user_id = $('#select_user_id').val();

        if (user_id != 0) {

            var table_names = [];
            var count = 0;

            $('.table_sales:checked').each(function () {
                table_names[count++] = $(this).val();
            });

            $('.table_void:checked').each(function () {
                table_names[count++] = $(this).val();
            });

            //console.log(table_names);

            var params = {
                user_id: user_id,
                table_names: table_names
            };

            $.ajax({

                url: '/change/userReportPermission',
                type: 'POST',
                format: 'JSON',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {
                    swal('Done!', 'User Report Permission Changed!', 'success');

                },
                error: function (error) {
                    swal('Error!', 'Something Went Wrong', 'error');
                }

            });

        }



    });


    $('#change_void_report_permission').click(function () {

        var user_id = $('#select_user_id').val();

        if (user_id != 0) {

            var table_names = [];
            var count = 0;

            $('.table_sales:checked').each(function () {
                table_names[count++] = $(this).val();
            });

            $('.table_void:checked').each(function () {
                table_names[count++] = $(this).val();
            });

            var params = {
                user_id: user_id,
                table_names: table_names
            };

            $.ajax({

                url: '/change/userReportPermission',
                type: 'POST',
                format: 'JSON',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {
                    swal('Done!', 'User Report Permission Changed!', 'success');

                },
                error: function (error) {
                    swal('Error!', 'Something Went Wrong', 'error');
                }

            });

        }



    });


    $('#select_user_id').change(function () {

        unCheckEverything();
        unCheckAllMenu();

        var user_id = $('#select_user_id').val();

        if (user_id != 0) {

            var params = {
                user_id: user_id
            };

            $.ajax({
                url: '/get/userPermissionDetails',
                type: 'POST',
                format: 'JSON',
                data: {'_token': $('#token').val(), params: params},

                success: function (response) {

                    var json_message = JSON.parse(response);

                    menu_permission = json_message.menu_permission;

                    //console.log(menu_permission);
                    var report_permission = json_message.report_permission;

                    for (var i=0; i<report_permission.length; i++) {

                        var reportType_id = report_permission[i].reportType_id;

                        $('#reportType_' + reportType_id).prop('checked', true);

                    }

                    viewUserMenuPermission();


                },

                error: function (error) {
                    swal('Error!', 'Something Went Wrong', 'error');
                }
            });

        }



    });


    $("#add_email_button").click(function(){

        var email = $("#email").val();

        if (email != '') {

            var params = {
                email: email
            };

            $.ajax({
                url: '/add/email',
                type: 'POST',
                format: 'JSON',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {

                    var htmlstr = '<tr class="even pointer" id="email_'+response+'">' +
                        '<td>#</td>' +
                        '<td id="email_address_'+response+'">'+email+'</td>' +
                        '<td>' +
                        '<button type="button" class="btn btn-dark pointer" onclick="editEmail(\''+response+'\', \''+email+'\')">' +
                        '<i class="glyphicon glyphicon-pencil"></i>' +
                        '</button> ' +
                        '<button type="button" class="btn btn-danger pointer" onclick="deleteEmail('+response+')">' +
                        '<i class="glyphicon glyphicon-remove"></i></button></td></tr>';


                    $('#emails_table').append(htmlstr);

                    showSuccessNotification('Reporting Email Has Been Added!');



                },
                error: function (error) {
                    showErrorNotification();
                }
            });
        }
        else {
            showWarningNotification('Email Input Field Is Empty');
        }
    });


    $("#edit_email_button").click(function(){

        var email_id = $("#email_id").val();
        var email = $("#edit_email").val();

        if (email != "") {

            var params = {
                email_id: email_id,
                email: email
            };

            $.ajax({
                url: '/edit/email',
                type: 'POST',
                format: 'JSON',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {

                    $("#email_address_" + email_id).text(email);

                    showSuccessNotification('Reporting Email Has Been Edited!');

                },
                error: function (error) {
                    showErrorNotification();
                }
            });

        }

    });


    $('#change_void_permission').click(function () {

        var users = [];
        var count = 0;

        $('.users:checked').each(function () {
            users[count++] = $(this).val();
        });


        var params = {
            users: users
        };

        $.ajax({

            url: '/change/voidPermission',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                swal('Done!', 'User Void Permission Changed!', 'success');

            },
            error: function (error) {
                swal('Error!', 'Something Went Wrong', 'error');
            }

        });





    });


    $('.save_permission').click(function(event){

        var user_id = $('#select_user_id').val();

        var menus = [];

        $("input[name='menu[]']:checked").each(function() {
            menus.push($(this).val());
        });

        if (user_id != 0) {

            var params = {
                user_id: user_id,
                menus: menus
            };

            $.ajax({

                url: '/change/userPermission',
                type: 'POST',
                format: 'JSON',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {
                    showSuccessNotification('User Permission Changed!')

                },
                error: function (error) {
                    showErrorNotification();
                }

            });

        }

        else showWarningNotification('Select A User First');


    });


    $('#save_order_types_permission').click(function(event){

        var order_types = [];

        $("input[name='order_types[]']:checked").each(function() {
            order_types.push($(this).val());
        });

        var params = {
            order_types: order_types
        };

        $.ajax({
            url: '/change/orderTypesPermission',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                showSuccessNotification('User Permission Changed!')

            },
            error: function (error) {
                showErrorNotification();
            }

        });





    });



});


function deleteTable(table_id) {

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-danger',
        cancelButtonClass: 'btn btn-success',
        buttonsStyling: false
    }).then(function () {

        var params = {
            table_id: table_id,
        };

        $.ajax({
            url: '/delete/table',
            type: 'POST',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                $("#table_" + table_id).remove();
                swal({
                    type: 'success',
                    html: $('<div>')
                        .addClass('some-class')
                        .text('Table has been deleted'),
                    animation: false,
                    customClass: 'animated bounceIn'
                });
            },
            error: function (error) {
                console.log("Error! Something went wrong.");
            }
        });

    });
}


function editTable(table_id, table_name, table_capacity) {

    $("#edit_table_title").text("Edit Table : " + table_name);
    $("#modal_table_name").val(table_name);
    $("#modal_table_capacity").val(table_capacity);
    $("#edit_table_id").val(table_id);

    $("#edit_table_modal").modal('show');
}


function unCheckEverything() {

    $('.table_sales').each(function () { this.checked = false });
    $('.table_void').each(function () { this.checked = false });
    $('.table_menu').each(function () { this.checked = false });


}


function pushToMenuArray(menu_id, menu_name, parent_menu) {
    application_menu.push([menu_id, menu_name, parent_menu]);
}


function loadSubMenu(menu_id, title_name) {

    $('#submenu_title').text('Of ' + title_name);

    //console.log(application_menu);

    var item_count = 1;

    htmlstr = '';

    for (var i = 0; i < application_menu.length; i++) {

        var submenu_id = application_menu[i][0];
        var menu_name = application_menu[i][1];
        var parent_menu = application_menu[i][2];



        if (parent_menu == menu_id) {

            htmlstr += '<tr>' +
                '<td scope="row">'+ item_count++ +'</td>' +
                '<td>' +
                '<input class="table_menu" onclick="changeBox('+submenu_id+')" id="menu_'+submenu_id+'" value="'+submenu_id+'-'+parent_menu+'" type="checkbox">' +
                '</td>' +
                '<td>'+menu_name+'</td>' +
                '</tr>';
        }
    }


    $( "#submenu_table" ).empty();
    $( "#submenu_table" ).append(htmlstr);

    viewUserMenuPermission();
}


function changeBox(menu_id) {

    var value = $('#menu_' + menu_id).val();

    var checkedValue = 0;
    if ($('#menu_'+ menu_id).prop("checked") == true) checkedValue=1;

    var token = value.split("-");
    var menu_id = token[0];
    var parent_menu = token[1];

    var user_id = $('#select_user_id').val();

    var params = {
        user_id: user_id,
        menu_id: menu_id,
        parent_menu: parent_menu,
        checkedValue: checkedValue
    };

    if (user_id != 0) {

        $.ajax({
            url: '/change/menuPermission',
            type: 'POST',
            format: 'JSON',
            data: {'_token': $('#token').val(), params: params},

            success: function (response) {
                // showSuccessNotification('Permission Has Been Approved!');
            },

            error: function (error) {
                showErrorNotification();
            }
        });

    }





}


function viewUserMenuPermission() {

    for (var i=0; i<menu_permission.length; i++) {

        var menu_id = menu_permission[i].menu_id;

        $('#menu_' + menu_id).prop('checked', true);

    }

}


function editEmail(email_id, email) {

    $("#email_id").val(email_id);
    $("#edit_email").val(email);

    $("#edit_email_modal").modal('show');
}


function deleteEmail(email_id) {

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-danger',
        cancelButtonClass: 'btn btn-success',
        buttonsStyling: false
    }).then(function () {

        var params = {
            email_id: email_id
        };

        $.ajax({
            url: '/delete/email',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {

                $("#email_" + email_id).remove();

            },
            error: function (error) {
                console.log("Error! Something went wrong.");
            }
        });

    });
}


function checkAllMenu() {
    $("input[name='menu[]']:checkbox").prop('checked',true);
}


function unCheckAllMenu() {
    $("input[name='menu[]']:checkbox").prop('checked',false);
}


function checkAllOrderTypes() {
    $("input[name='order_types[]']:checkbox").prop('checked',true);
}


function unCheckAllOrderTypes() {
    $("input[name='order_types[]']:checkbox").prop('checked',false);
}








