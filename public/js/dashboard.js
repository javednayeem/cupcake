$(document).ready(function() {

    $('#start_work_period_button').click(function () {

        $.ajax({
            url: '/start/work-period',
            type: 'POST',
            format: 'JSON',
            data: {'_token': $('#token').val()},
            success: function (response) {
                swal('', 'Work Period Started', 'success');
                setTimeout(function(){
                    window.location.href = "/dashboard";
                },1000);
            },
            error: function (error) {
                swal('Error!', 'Something went wrong', 'error');
            }
        });
    });


    $('#end_work_period_button').click(function () {

        swal({
            title: "",
            text: 'Please Wait Few Moments',
            type: 'info',
            showCancelButton: false,
            showConfirmButton: false,
            timer: 53000
        });

        $.ajax({
            url: '/end/work-period',
            type: 'POST',
            format: 'JSON',
            data: {'_token': $('#token').val()},
            success: function (response) {

                var json_message = $.parseJSON(response);

                if (json_message.status == 'success') {

                    showSuccessNotification(json_message.message);

                    setTimeout(function(){
                        window.location.href = "/dashboard";
                    },1);

                }

                else {
                    showWarningNotification(json_message.message);
                }

            },
            error: function (error) {
                showErrorNotification();
            }
        });
    });


    $('#email_report_button').click(function () {

        var restaurant_id = $('#restaurant_id').val();

        $.ajax({
            url: '/sendEmailReport/' + restaurant_id,
            type: 'GET',
            format: 'JSON',
            data: {'_token': $('#token').val()},
            success: function (response) {
                showSuccessNotification('Email Has Been Sent!');
            },
            error: function (error) {
                showErrorNotification();
            }
        });
    });

});


function accept_void_request(void_id, order_id) {
    $("#void_button_" + void_id ).removeClass("btn-warning");
    $("#void_button_" + void_id).addClass("btn-danger");
    $("#void_button_" + void_id).text("Accepting...");

    var params = {
        void_id: void_id,
        order_id: order_id
    };

    $.ajax({
        url: '/accept/void',
        type: 'POST',
        format: 'JSON',
        data: {params: params, "_token": $("#token").val()},

        success: function (response) {

            setTimeout(function(){
                swal({
                    type: 'success',
                    html: $('<div>')
                        .addClass('some-class')
                        .text('Void Request Accepted'),
                    animation: false,
                    customClass: 'animated bounceIn'
                });
            },2000);

            $("#void_button_" + void_id ).removeClass("btn-danger");
            $("#void_button_" + void_id).addClass("btn-success");
            $("#void_button_" + void_id).text("Accepted");
        },
        error: function (error) {
            console.log("Something went wrong");
        }

    });
}


function get_time_diff( datetime ) {

    var datetime = typeof datetime !== 'undefined' ? datetime : "2014-01-01 01:02:03.123456";
    var datetime = new Date( datetime ).getTime();
    var now = new Date().getTime();
    var difference = '';

    if (datetime < now) var milisec_diff = now - datetime;
    else var milisec_diff = datetime - now;

    var days = Math.floor(milisec_diff / 1000 / 60 / (60 * 24));
    var date_diff = new Date( milisec_diff );
    difference = days + " Days "+ (date_diff.getHours()-6) + " Hours " + date_diff.getMinutes() + " Minutes " + date_diff.getSeconds() + " Seconds";

    $('#time_difference_span').text(difference);
}


function accept_item_void_request(void_item_id) {

    $("#void_item_button_" + void_item_id ).removeClass("btn-warning");
    $("#void_item_button_" + void_item_id).addClass("btn-danger");
    $("#void_item_button_" + void_item_id).text("Accepting...");

    var params = {
        void_item_id: void_item_id
    };

    $.ajax({
        url: '/accept/void-item',
        type: 'POST',
        format: 'JSON',
        data: {params: params, "_token": $("#token").val()},

        success: function (response) {

            setTimeout(function(){
                swal({
                    type: 'success',
                    html: $('<div>')
                        .addClass('some-class')
                        .text('Item Void Request Accepted'),
                    animation: false,
                    customClass: 'animated bounceIn'
                });
            },2000);

            $("#void_item_button_" + void_item_id ).removeClass("btn-danger");
            $("#void_item_button_" + void_item_id).addClass("btn-success");
            $("#void_item_button_" + void_item_id).text("Accepted");
        },
        error: function (error) {
            console.log("Something went wrong");
        }

    });
}












