$(document).ready(function() {

    generatePassword();

    $( "#user_email" ).keyup(function() {
        var $th = $(this);
        $th.val( $th.val().replace(/[@ ."<>&]/g, function(str) {  return ''; } ) );
    });


    $("#add_user_button").click(function(){

        var name = $("#employee_name").val();
        var email = $("#user_email").val();
        var role = $("#select_user_role").val();
        var password = $("#user_password").val();

        if (name != "" && email != "" && role != "0" && password.length>5) {

            //var email = email + $("#rstaurantCode").val();
            var phone = $("#user_phone").val();

            var params = {
                name: name,
                email: email,
                password: password,
                role: role,
                phone: phone,
            };

            $.ajax({
                url: '/add/user',
                type: 'POST',
                format: 'JSON',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {
                    var json_message = $.parseJSON(response);

                    if (!isNaN(json_message)) {
                        swal("", name + " added To " + role, "success");
                        htmlstr = '<tr class="even pointer">' +
                            '<td>#</td>' +
                            '<td>'+name+'</td>' +
                            '<td>'+email+'</td>' +
                            '<td>'+phone+'</td>' +
                            '<td>'+role+'</td>' +
                            '<td>1</td>' +
                            '<td>'+ getCurrentDate() +'</td>' +
                            '<td>' +
                            '<button type="button" class="btn btn-dark" onclick="editUser(\''+ response+'\', \''+ name +'\', \''+ role +'\', \''+ phone +'\')">' +
                            '<i class="glyphicon glyphicon-pencil"></i></button> ' +
                            '<button type="button" class="btn btn-danger" onclick="deleteUser('+ response +')">' +
                            '<i class="glyphicon glyphicon-remove"></i></button></td>';

                        $( "#user_table" ).append(htmlstr);


                    }
                    else {

                    }



                },
                error: function (error) {
                    console.log("Error! Something went wrong.");
                }
            });
        }
        else {
            swal("", "Input field empty", "warning");
        }
    });


    $('#edit_user_button').click(function () {

        var name = $("#modal_user_name").val();
        var role = $("#modal_user_role").val();
        var phone = $("#modal_user_phone").val();

        if (name != "" && role != "0") {

            var params = {
                id: $("#edit_user_id").val(),
                name: name,
                role: role,
                phone: phone
            };

            $.ajax({
                url: '/edit/user',
                type: 'POST',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {
                    swal('Done!', 'Successfully Updated User Info', 'success');

                },
                error: function (error) {
                    console.log("Error! Something went wrong.");
                }
            });
        }
    });


    $("#edit_password_button").click(function(){

        var user_id = $("#edit_password_id").val();
        var new_password = $("#new_password").val();
        var retype_password = $("#retype_password").val();

        if (new_password != retype_password) swal("", "Password Mismatched", "warning");

        else {

            var params = {
                user_id: user_id,
                new_password: new_password
            };

            $.ajax({
                url: '/edit/user/password',
                type: 'POST',
                format: 'JSON',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {
                    showSuccessNotification('Successfully Changed Password!');

                    reloadCurrentPage();
                },
                error: function (error) {
                    showErrorNotification();
                }
            });
        }

    });
});


function randomPassword(length) {
    var chars = "abcdefghijklmnopqrstuvwxyz!@#$%ABCDEFGHIJKLMNOP1234567890";
    var pass = "";
    for (var x = 0; x < length; x++) {
        var i = Math.floor(Math.random() * chars.length);
        pass += chars.charAt(i);
    }
    return pass;
}


function generatePassword() {
    var length = 15;
    $("#user_password").val(randomPassword(length));
}


function deleteUser(id) {

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function () {

        var params = {
            id: id,
        };

        $.ajax({
            url: '/delete/user',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                $("#user_" + id).remove();
                swal({
                    type: 'success',
                    html: $('<div>')
                        .addClass('some-class')
                        .text('User has been deleted'),
                    animation: false,
                    customClass: 'animated bounceIn'
                });
            },
            error: function (error) {
                console.log("Error! Something went wrong.");
            }
        });

    }, function (dismiss) {
        if (dismiss === 'cancel') {
            swal({
                title: 'Cancelled',
                type: 'error',
                html: $('<div>')
                    .addClass('some-class')
                    .text('Your User is safe :)'),
                animation: false,
                customClass: 'animated bounceIn'
            });
        }
    });
}


function editUser(id, name, role, phone) {

    $("#edit_user_id").val(id);
    $("#modal_user_name").val(name);
    $("#modal_user_role").val(role);
    $("#modal_user_phone").val(phone);

    $("#edit_user_modal").modal('show');
}


function editPassword(id) {

    $("#edit_password_id").val(id);

    $("#edit_password_modal").modal('show');
}







