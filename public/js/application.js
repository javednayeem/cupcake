var start_time = Date.now();
var render_time = 0;
var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];


var customers = [];

window.onload = function(){
    // render_time = parseInt((Date.now()) - start_time);
    $('#render_time').text(((Date.now()) - start_time)/1000.0 + 's');
};


function toggleFullScreen() {
    if (!document.fullscreenElement &&    // alternative standard method
        !document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement ) {  // current working methods
        if (document.documentElement.requestFullscreen) {
            document.documentElement.requestFullscreen();
        } else if (document.documentElement.msRequestFullscreen) {
            document.documentElement.msRequestFullscreen();
        } else if (document.documentElement.mozRequestFullScreen) {
            document.documentElement.mozRequestFullScreen();
        } else if (document.documentElement.webkitRequestFullscreen) {
            document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
        }
    } else {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        }
    }
}


function getCurrentDate() {

    var now = new Date();
    var thisMonth = months[now.getMonth()];
    var date = now.getDate();
    var year = now.getFullYear();

    var today = thisMonth + ' ' + date + ', ' + year;
    return today;
}


function getUserName() {
    return $('#user_name').val();
}


function randomString(length) {
    var text = "";
    var d = new Date();
    var time = d.getTime();
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    for (var i = 0; i < length; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text + '-' + time ;
}


function showSuccessNotification(message) {
    //$.Notification.notify('success','top left','Done!', message);

    swal({
        title: "Done!",
        text: message,
        type: 'success',
        showCancelButton: false,
        showConfirmButton: false,
        timer: 1500
    });


}


function showWarningNotification(message) {
    //$.Notification.notify('warning','top left','Warning!', message);

    swal({
        title: "Warning!",
        text: message,
        type: 'warning',
        showCancelButton: false,
        showConfirmButton: false
    });
}


function showErrorNotification(message) {
    message = typeof message !== 'undefined' ? message : 'Something Went Wrong!';
    //$.Notification.notify('error','top left','Error!', message);

    swal({
        title: "Error!",
        text: message,
        type: 'error',
        showCancelButton: false,
        showConfirmButton: false
    });
}


function reloadCurrentPage() {
    window.location = window.location.pathname;
}


function redirect(url, time) {

    setTimeout(function(){
        window.location.href = url;
    }, 1);

}



$(document).ready(function() {


    $('#select_en').click(function(){

        var params = {
            locale: 'en'
        };

        $.ajax({
            url: '/change/lang',
            type: 'POST',
            format: 'JSON',
            data: {'_token': $('#token').val(), params: params},

            success: function(response) {
                reloadCurrentPage();
            },
            error: function(error) {
                showErrorNotification();
            }

        });
    });


    $('#select_bn').click(function(){

        var params = {
            locale: 'bn'
        };

        $.ajax({
            url: '/change/lang',
            type: 'POST',
            format: 'JSON',
            data: {'_token': $('#token').val(), params: params},

            success: function(response) {
                reloadCurrentPage();
            },
            error: function(error) {
                showErrorNotification();
            }

        });
    });


    $('#myDatepicker').datetimepicker();


    $('#myDatepicker2').datetimepicker({
        format: 'YYYY-MM-DD'
    });


    $('#myDatepicker3').datetimepicker({
        format: 'YYYY-MM-DD'
    });


    $('#myDatepicker4').datetimepicker({
        ignoreReadonly: true,
        allowInputToggle: true
    });


    $('#datetimepicker6').datetimepicker();


    $('#datetimepicker7').datetimepicker({
        useCurrent: false
    });


    $("#datetimepicker6").on("dp.change", function(e) {
        $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
    });


    $("#datetimepicker7").on("dp.change", function(e) {
        $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
    });


    $('.ui-pnotify').remove();


    $("#restaurant_settings_button").click(function(){

        var guest_bill = ( $("#guest_bill").is(':checked') ) ? 1 : 0;
        var waiter_order_void = ( $("#waiter_order_void").is(':checked') ) ? 1 : 0;
        var order_receipt_debug = ( $("#order_receipt_debug").is(':checked') ) ? 1 : 0;
        var pay_first = ( $("#pay_first").is(':checked') ) ? 1 : 0;
        var kitchen_type = $("#kitchen_type").val();
        var view_settle_receipt = $("#view_settle_receipt").val();
        var receipt_footer = $("#receipt_footer").val();
        var menu_font_size = $("#menu_font_size").val();
        var default_waiter = ( $("#default_waiter").is(':checked') ) ? 1 : 0;
        var item_box_size = $("#item_box_size").val();
        var order_timestamp = $("#order_timestamp").val();
        var night_hour = $("#night_hour").val()!=''?$("#night_hour").val():0;
        var check_order_before_end_workperiod = $("#check_order_before_end_workperiod").val();
        var refresh_timeout = $("#refresh_timeout").val()!=''?$("#refresh_timeout").val():-1;

        var params = {
            guest_bill: guest_bill,
            waiter_order_void: waiter_order_void,
            order_receipt_debug: order_receipt_debug,
            pay_first: pay_first,
            kitchen_type: kitchen_type,
            view_settle_receipt: view_settle_receipt,
            receipt_footer: receipt_footer,
            menu_font_size: menu_font_size,
            default_waiter: default_waiter,
            item_box_size: item_box_size,
            order_timestamp: order_timestamp,
            night_hour: night_hour,
            check_order_before_end_workperiod: check_order_before_end_workperiod,
            refresh_timeout: refresh_timeout,
        };

        $.ajax({
            url: '/save/restaurant/settings',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                showSuccessNotification('Restaurant Settings Updated Successfully');

            },
            error: function (error) {
                showErrorNotification();
            }
        });
    });

});



function databaseBackup() {

    $.ajax({
        url: '/create/back-up',
        type: 'POST',
        format: 'JSON',
        data: {"_token": $('#token').val()},

        success: function (response) {
            showSuccessNotification('Successfully Created Database Backup');
        },
        error: function (error) {
            showErrorNotification();
        }
    });

}


function clearInputValue(div_name) {
    $('#' + div_name).find('input:text').val('');
}


function reload(duration, path) {

    var duration = typeof duration !== 'undefined' ? duration : 0;
    var pathname = typeof path !== 'undefined' ? path : window.location.pathname;

    setTimeout(function(){
        window.location.href = pathname;
    },duration);

}


function uc_first(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}


function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear(),
        hour = '' + d.getHours(),
        minute = '' + d.getMinutes();

    if (minute.length < 2) minute = '0' + minute;
    if (day.length < 2) day = '0' + day;

    return months[d.getMonth()] + ' ' + day + ', ' + year;
}


function formatDateTime(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear(),
        hour = '' + d.getHours(),
        minute = '' + d.getMinutes();

    if (minute.length < 2) minute = '0' + minute;
    if (day.length < 2) day = '0' + day;

    return months[d.getMonth()] + ' ' + day + ', ' + year + ' ' + hour + ':' + minute;
}


function onlineOrderDateTime(date) {

    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear(),
        hour = '' + d.getHours(),
        minute = '' + d.getMinutes();

    var AmOrPm = hour >= 12 ? 'pm' : 'am';
    hour = (hour % 12) || 12;

    if (minute.length < 2) minute = '0' + minute;
    if (day.length < 2) day = '0' + day;

    return hour + ':' + minute + ' ' + AmOrPm + ', ' + months[d.getMonth()] + ' ' + day + ', ' + year;

    //return months[d.getMonth()] + ' ' + day + ', ' + year + ' ' + hour + ':' + minute;
}


function numberPadding(num) {

    var number = num.toString();
    var padding = '';
    var length = 6;

    var number_length = number.length;

    for (var i=0; i<(length-number_length); i++) padding += '0';

    return (padding + number);

}


function formatInvoiceDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return (year  + month + day);
}













