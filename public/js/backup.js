
$(document).ready(function() {


    $("#db_backup_button").click(function(){

        $.ajax({
            url: '/create/back-up',
            type: 'POST',
            format: 'JSON',
            data: {"_token": $('#token').val()},

            success: function (response) {
                showSuccessNotification('Successfully Created Database Backup');
                reloadCurrentPage();

            },
            error: function (error) {
                showErrorNotification();
            }
        });



    });

});





function deleteBackup(log_id) {

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-danger',
        cancelButtonClass: 'btn btn-success',
        buttonsStyling: false
    }).then(function () {

        var params = {
            log_id: log_id
        };

        $.ajax({
            url: '/delete/back-up',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {

                $("#log_" + log_id).remove();

                showSuccessNotification('Database Backup Has Been Deleted');

            },
            error: function (error) {
                showErrorNotification();
            }
        });

    });
}







