var payment_methods = [];

$(document).ready(function() {

    printPaymentMethod();

    generatePassword();

    $('#add_card_button').click(function () {

        var card_name = $('#card_name').val();

        if (card_name != '') {

            var params = {
                card_name: card_name
            };

            $.ajax({
                url: '/add/bank-card',
                type: 'POST',
                format: 'JSON',
                data: {params:params, '_token': $('#token').val()},

                success: function (response) {
                    showSuccessNotification('Successfully Added The Card To List');
                    reloadCurrentPage();
                },
                error: function (error) {
                    showErrorNotification();
                }
            });
        }

    });


    $('#edit_card_button').click(function () {

        var card_id = $('#card_id').val();
        var card_name = $('#edit_card_name').val();

        if (card_name != '') {

            var params = {
                card_id: card_id,
                card_name: card_name
            };

            $.ajax({
                url: '/edit/bank-card',
                type: 'POST',
                format: 'JSON',
                data: {params:params, '_token': $('#token').val()},

                success: function (response) {
                    showSuccessNotification('Successfully Edited The Card To List');
                    reloadCurrentPage();
                },
                error: function (error) {
                    showErrorNotification();
                }
            });
        }

    });


    $('#add_payment_method_button').click(function () {

        var method_name = $('#method_name').val();

        if (method_name != '') {

            var params = {
                method_name: method_name
            };

            $.ajax({
                url: '/add/payment-method',
                type: 'POST',
                format: 'JSON',
                data: {params:params, '_token': $('#token').val()},

                success: function (response) {
                    showSuccessNotification('Successfully Added The Payment To List');
                    reloadCurrentPage();
                },
                error: function (error) {
                    showErrorNotification();
                }
            });
        }

    });


    $('#edit_payment_method_button').click(function () {

        var method_id = $('#method_id').val();
        var method_name = $('#edit_method_name').val();

        if (method_name != '') {

            var params = {
                method_id: method_id,
                method_name: method_name
            };

            $.ajax({
                url: '/edit/payment-method',
                type: 'POST',
                format: 'JSON',
                data: {params:params, '_token': $('#token').val()},

                success: function (response) {
                    showSuccessNotification('Successfully Edited The Payment To List');
                    reloadCurrentPage();
                },
                error: function (error) {
                    showErrorNotification();
                }
            });
        }

    });


    $("#create_restaurant_button").click(function () {

        var restaurant_name = $("#restaurant_name").val();
        var restaurant_address = $("#restaurant_address").val();
        var restaurant_area = $("#restaurant_area").val();
        var restaurant_city = $("#restaurant_city").val();
        var restaurant_phone = $("#restaurant_phone").val();
        var restaurant_email = $("#restaurant_email").val();
        var restaurant_website = $("#restaurant_website").val();

        if (restaurant_name != "") {

            var params = {
                restaurants_name: restaurant_name,
                address: restaurant_address,
                area: restaurant_area,
                city: restaurant_city,
                phone_number: restaurant_phone,
                email: restaurant_email,
                website: restaurant_website
            };

            $.ajax({
                url: '/create/restaurant',
                type: 'POST',
                format: 'JSON',
                data: {params:params, '_token': $('#token').val()},

                success: function (response) {
                    showSuccessNotification('Successfully Created Restaurant');
                    redirect('/restaurants', 1500);
                },
                error: function (error) {
                    showWarningNotification('Unable To Create, Conflict Restaurant Name');
                }


            });

        }
    });


    $('#select_restaurant').change(function() {
        var restaurant_code = $('#select_restaurant option:selected').text();
        $('#span_rstaurantCode').text(restaurant_code);
        $('#rstaurantCode').val(restaurant_code);
    });


    $('#add_user_button').click(function () {

        var name = $('#new_user_name').val();
        var email = $('#user_email').val();
        var password = $('#user_password').val();
        var role = $('#select_user_role').val();
        var restaurant_id = $('#select_restaurant').val();

        if (name != "" && email != "" && role != "0" && password.length>5) {

            //email = email + '@' + $("#select_restaurant option:selected").text() + '.com';
            var phone = $("#user_phone").val();

            var params = {
                name: name,
                email: email,
                password: password,
                role: role,
                restaurant_id: restaurant_id,
            };

            $.ajax({
                url: '/create/user',
                type: 'POST',
                format: 'JSON',
                data: {params: params, '_token': $('#token').val()},

                success: function (response) {

                    swal('Done!', 'Created User for ' + $("#select_restaurant option:selected").text(), 'success');
                    $('#user_name').val('');
                    $('#user_email').val('');
                    $('#user_password').val('');

                    setTimeout(function(){
                        window.location.href = "/create-user";
                    },2000);

                },
                error: function (error) {
                    swal('Error!', 'Something went wrong', 'error');
                }
            });
        }
    });


    $('#app_settings_button').click(function () {

        var app_name_1 = $('#app_name_1').val();
        var app_name_2 = $('#app_name_2').val();
        var app_title = $('#app_title').val();
        var app_login_footer = $('#app_login_footer').val();
        var receipt_company = $('#receipt_company').val();
        var receipt_phone = $('#receipt_phone').val();
        var backup_interval = $('#backup_interval').val();
        var bg_type = $('#bg_type').val();
        var bg_type = $('#bg_type').val();
        var bg_color = $('#bg_color').val();

        if (app_name_1 != '' && app_login_footer != '') {

            var params = {
                app_name_1: app_name_1,
                app_name_2: app_name_2,
                app_title: app_title,
                app_login_footer: app_login_footer,
                receipt_company: receipt_company,
                receipt_phone: receipt_phone,
                backup_interval: backup_interval,
                bg_type: bg_type,
                bg_color: bg_color
            };

            console.log(params);

            $.ajax({
                url: '/save/app/settings',
                type: 'POST',
                format: 'JSON',
                data: {params: params, '_token': $('#token').val()},
                success: function (response) {
                    swal('Done!', 'Settings Saved Successfully', 'success');
                },
                error: function (error) {
                    swal('Error!', 'Something went wrong', 'error');
                }
            });

        }

    });


    $('#add_lookup_button').click(function () {

        var lookup_name = $('#lookup_name').val();

        if (lookup_name != '') {

            var params = {
                lookup_name: lookup_name,
            };

            $.ajax({
                url: '/add/lookup',
                type: 'POST',
                format: 'JSON',
                data: {params:params, '_token': $('#token').val()},

                success: function (response) {
                    swal("", "Successfully Added Lookup", "success");
                },
                error: function (error) {
                    console.log("Error! Something went wrong");
                }
            });
        }

    });


    $('.acc_permission').on('change', function() {

        var restaurants_id = this.value;
        var accounts = this.checked;
        accounts = accounts==true?1:0;

        var params = {
            restaurants_id: restaurants_id,
            accounts: accounts
        };

        $.ajax({
            url: '/change/accounts/permission',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                swal('Done!', 'Accounts Permission Changed', 'success');
            },
            error: function (error) {
                showErrorNotification();
            }
        });
    });


    $('.inv_permission').on('change', function() {

        var restaurants_id = this.value;
        var inventory = this.checked;
        inventory = inventory==true?1:0;

        var params = {
            restaurants_id: restaurants_id,
            inventory: inventory
        };

        $.ajax({
            url: '/change/inv/permission',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                swal('Done!', 'Inventory Permission Changed', 'success');
            },
            error: function (error) {
                showErrorNotification();
            }
        });
    });



    $('.kitchen_queue_permission').on('change', function() {

        var restaurants_id = this.value;
        var kitchen_queue = this.checked;
        kitchen_queue = kitchen_queue==true?1:0;

        var params = {
            restaurants_id: restaurants_id,
            kitchen_queue: kitchen_queue
        };

        $.ajax({
            url: '/change/kitchen-queue/permission',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                showSuccessNotification('Kitchen Queue Permission Changed');
            },
            error: function (error) {
                showErrorNotification();
            }
        });
    });


    $('#license_duration').change(function() {
        var license_start_date = $('#single_cal1').val();
        var duration = $('#license_duration').val();

        var license_end_date = addDays(license_start_date, duration);
        $('#single_cal2').val(formatDate(license_end_date));
    });


    $('#assign_license_button').click(function() {

        var restaurants_id = $('#restaurants_id').val();
        var license_start_date = $('#single_cal1').val();
        var license_end_date = $('#single_cal2').val();
        var license_duration = $('#license_duration').val();
        var license_type = $('#license_type').val();

        if (license_duration == '0') swal('', 'Please Select A Minimum Duration', 'warning');
        else {

            var params = {
                restaurants_id: restaurants_id,
                license_start_date: license_start_date,
                license_end_date: license_end_date,
                license_duration: license_duration,
                license_type: license_type
            };

            $.ajax({

                url: '/license/assign',
                type: 'POST',
                format: 'JSON',
                data: {params: params, "_token": $('#token').val()},

                success: function(response) {
                    showSuccessNotification('Successfully Created License');
                    reloadCurrentPage();
                },
                error: function(error) {
                    showErrorNotification();
                }

            });


        }



    });


    $('.user-role').on('change', function() {

        var str = this.value;
        var key = str.split("-");

        var params = {
            user_id: key[1],
            role: key[0]
        };

        $.ajax({
            url: '/edit/user/role',
            type: 'POST',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                swal('Done!', 'User Role Has Been Changed', 'success');
            },
            error: function (error) {
                swal('Error!', 'Something Went Wrong', 'error');
            }
        });
    });


    $('.user_restaurant_id').on('change', function() {


        var str = $('select option:selected').attr('class');
        //var str = this.val();
        var key = str.split("-");

        alert(str);

        var params = {
            user_id: key[1],
            restaurant_id: key[0]
        };

        $.ajax({
            url: '/edit/user/restaurant',
            type: 'POST',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                swal('Done!', 'User Restaurant Has Been Changed', 'success');
            },
            error: function (error) {
                swal('Error!', 'Something Went Wrong', 'error');
            }
        });
    });


    $('#table_truncate_button').click(function () {

        var table_names = [];
        var count = 0;

        $('.table_truncate:checked').each(function () {
            table_names[count++] = $(this).val();
        });

        var params = {
            table_names: table_names
        };

        $.ajax({

            url: '/table/truncate',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                swal('Done!', 'Table Truncated', 'success');

                setTimeout(function(){
                    window.location.href = "/data-settings";
                },2000);
            },
            error: function (error) {
                swal('Error!', 'Something Went Wrong', 'error');
            }

        });

    });


    $('#table_truncate_toggle').click(function () {

        $('.table_truncate').each(function () { this.checked = !this.checked; });

    });


    $('#table_drop_button').click(function () {

        var table_names = [];
        var count = 0;

        $('.table_drop:checked').each(function () {
            table_names[count++] = $(this).val();
        });

        var params = {
            table_names: table_names
        };

        $.ajax({

            url: '/table/drop',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                swal('Done!', 'Table Dropped', 'success');

                setTimeout(function(){
                    window.location.href = "/data-settings";
                },2000);
            },
            error: function (error) {
                swal('Error!', 'Something Went Wrong', 'error');
            }

        });

    });


    $('#table_drop_toggle').click(function () {

        $('.table_drop').each(function () { this.checked = !this.checked; });

    });


    $('#edit_user_button').click(function () {

        var name = $("#modal_user_name").val();
        var role = $("#modal_user_role").val();
        var phone = $("#modal_user_phone").val();

        if (name != "" && role != "0") {

            var params = {
                id: $("#edit_user_id").val(),
                name: name,
                role: role,
                phone: phone
            };

            $.ajax({
                url: '/edit/user',
                type: 'POST',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {
                    swal('Done!', 'Successfully Updated User Info', 'success');

                    setTimeout(function(){
                        window.location.href = "/create-user";
                    },2000);

                },
                error: function (error) {
                    console.log("Error! Something went wrong.");
                }
            });
        }
    });


    $('#view_license_button').click(function () {

        var select_restaurant_id = $('#select_restaurant_id').val();
        if (select_restaurant_id == 0) window.location.href = "/sa-licenses";
        else window.location.href = "/sa-licenses/" + select_restaurant_id;

    });


    $('#add_app_menu_button').click(function () {

        var menu_name = $('#menu_name').val();

        var parent_menu = $('#parent_menu').val();

        if (menu_name != '') {

            var params = {
                menu_name: menu_name,
                parent_menu: parent_menu
            };

            $.ajax({
                url: '/save/application-menu',
                type: 'POST',
                format: 'JSON',
                data: {params: params, '_token': $('#token').val()},
                success: function (response) {

                    showSuccessNotification('Application Menu Saved Successfully');
                    reloadCurrentPage();
                },
                error: function (error) {
                    showErrorNotification();
                }
            });

        }

    });


    $('#edit_app_menu_button').click(function () {

        var menu_id = $('#menu_id').val();
        var menu_name = $('#edit_menu_name').val();
        var parent_menu = $('#edit_parent_menu').val();

        if (menu_name != '') {

            var params = {
                menu_id: menu_id,
                menu_name: menu_name,
                parent_menu: parent_menu
            };

            $.ajax({
                url: '/edit/application-menu',
                type: 'POST',
                format: 'JSON',
                data: {params: params, '_token': $('#token').val()},
                success: function (response) {

                    showSuccessNotification('Application Menu Edited Successfully');
                    reloadCurrentPage();
                },
                error: function (error) {
                    showErrorNotification();
                }
            });

        }

    });


    $("#edit_order_button").click(function () {

        var order_id = $("#order_id").val();
        var waiter_id = $("#waiter_id").val();
        var customer_id = $("#customer_id").val();
        var table_id = $("#table_id").val();
        var order_type = $("#order_type").val();
        var order_status = $("#order_status").val();
        var discount = $("#discount").val();
        var discount_percentage = $("#discount_percentage").val();
        var discount_amount_type = $("#discount_amount_type").val();
        var discount_reference = $("#discount_reference").val();
        var payment_method = $("#payment_method").val();

        var created_at = $("#created_at").val();
        var created_time = $("#created_time").val();

        var recalculate_bill = $("#recalculate_bill").is(':checked')?1:0;

        if (order_id != "") {

            var params = {
                order_id: order_id,
                waiter_id: waiter_id,
                customer_id: customer_id,
                table_id: table_id,
                order_type: order_type,
                order_status: order_status,
                discount: discount,
                discount_percentage: discount_percentage,
                discount_amount_type: discount_amount_type,
                discount_reference: discount_reference,
                payment_method: payment_method,
                created_at: created_at,
                created_time: created_time,
                recalculate_bill: recalculate_bill
            };

            $.ajax({
                url: '/edit/order',
                type: 'POST',
                format: 'JSON',
                data: {params:params, '_token': $('#token').val()},

                success: function (response) {

                    swal("", "Successfully Edited Order Information", "success");
                    reloadCurrentPage();

                },
                error: function (error) {
                    showErrorNotification();
                }


            });

        }
    });


    $("#delete_order_button").click(function () {

        var order_id = $("#order_id").val();


        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function () {

            var params = {
                order_id: order_id
            };

            $.ajax({
                url: '/delete/order',
                type: 'POST',
                format: 'JSON',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {
                    redirect('/view-order', 1);
                },
                error: function (error) {
                    showErrorNotification();
                }
            });

        });

    });


    $("#recalculate_order_button").click(function () {

        var order_id = $("#order_id").val();

        var params = {
            order_id: order_id
        };

        $.ajax({
            url: '/recalculate/order',
            type: 'POST',
            format: 'JSON',
            data: {params:params, '_token': $('#token').val()},

            success: function (response) {

                swal("", "Re-Calculated Order Billing Calculation", "success");
                reloadCurrentPage();

            },
            error: function (error) {
                showErrorNotification();
            }


        });


    });


    $('#modal_payment_method').change(function() {
        if ($('#modal_payment_method').val() == "card") $('#modal_bank_div').show();
        else $('#modal_bank_div').hide();
    });


    $('#edit_order_payment').click(function() {

        var order_id = $('#order_id').val();


        var params = {
            order_id: order_id,
            payment_methods: payment_methods
        };

        $.ajax({
            url: '/edit/order/payment',
            type: 'POST',
            format: 'JSON',
            data: {params: params, '_token': $('#token').val()},
            success: function (response) {

                showSuccessNotification('Successfully Edited Order Payment Methods');

                reloadCurrentPage();
            },
            error: function (error) {
                swal('Error!', 'Something went wrong', 'error');
            }
        });

    });


    $('#fix_date_button').click(function() {

        var start_order_id = $('#start_order_id').val();
        var end_order_id = $('#end_order_id').val();
        var created_at = $('#created_at').val();

        var params = {
            start_order_id: start_order_id,
            end_order_id: end_order_id,
            created_at: created_at
        };

        $.ajax({
            url: '/edit/order/created-at',
            type: 'POST',
            format: 'JSON',
            data: {params: params, '_token': $('#token').val()},
            success: function (response) {
                showSuccessNotification('Successfully Edited Order Date');
            },
            error: function (error) {
                showErrorNotification();
            }
        });

    });


    $('#save_features_button').click(function() {

        var accounts = $("#feature_accounts").is(':checked')?1:0;
        var inventory = $("#feature_inventory").is(':checked')?1:0;
        var kitchen_queue = $("#feature_kitchen_queue").is(':checked')?1:0;
        var online_order = $("#feature_online_order").is(':checked')?1:0;


        var params = {
            restaurants_id: $('#restaurants_id').val(),
            accounts: accounts,
            inventory: inventory,
            kitchen_queue: kitchen_queue,
            online_order: online_order
        };

        $.ajax({
            url: '/edit/restaurant-features',
            type: 'POST',
            format: 'JSON',
            data: {params: params, '_token': $('#token').val()},
            success: function (response) {
                showSuccessNotification('Successfully Edited Restaurant Features');
            },
            error: function (error) {
                showErrorNotification();
            }
        });

    });




    $('#add_order_type_button').click(function () {

        var type_name = $('#type_name').val();

        if (type_name != '') {

            var params = {
                type_name: type_name,
            };

            $.ajax({
                url: '/add/order-type',
                type: 'POST',
                format: 'JSON',
                data: {params:params, '_token': $('#token').val()},

                success: function (response) {

                    $('#type_name').val('');

                    var html_str = '<tr id="type_'+response+'">' +
                        '<td>#</td>' +
                        '<td id="type_name_'+response+'">'+type_name+'</td>' +
                        '<td><button type="button" class="btn btn-dark" onclick="editOrderType(\''+response+'\', \''+type_name+'\')">' +
                        '<i class="glyphicon glyphicon-pencil"></i>' +
                        '</button> ' +
                        '<button type="button" class="btn btn-danger" onclick="deleteOrderType('+response+')">' +
                        '<i class="glyphicon glyphicon-remove"></i></button></td></tr>';

                    $('#order_type_table').append(html_str);

                    showSuccessNotification('Order Type Has Been Added Successfully');
                },
                error: function (error) {
                    showErrorNotification();
                }
            });
        }

    });



    $('#edit_order_type_button').click(function() {

        var type_id = $('#edit_type_id').val();
        var type_name = $('#edit_type_name').val();

        var params = {
            type_id: type_id,
            type_name: type_name
        };

        $.ajax({
            url: '/edit/order-type',
            type: 'POST',
            format: 'JSON',
            data: {params: params, '_token': $('#token').val()},
            success: function (response) {

                $('#type_name_' + type_id).text(type_name);
                showSuccessNotification('Successfully Edited Order Type');
            },
            error: function (error) {
                showErrorNotification();
            }
        });

    });






});


function editMenu(menu_id, menu_name, parent_menu) {

    $('#menu_id').val(menu_id);
    $('#edit_parent_menu').val(parent_menu);
    $('#edit_menu_name').val(menu_name);

    $("#edit_application_menu_modal").modal('show');

}


function deleteMenu(menu_id) {

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function () {

        var params = {
            menu_id: menu_id
        };

        $.ajax({
            url: '/delete/application-menu',
            type: 'POST',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                $("#menu_" + menu_id).remove();

                showSuccessNotification('Deleted Application Menu');
            },
            error: function (error) {
                showErrorNotification();
            }
        });

    }, function (dismiss) {

    });

}


function changeRestaurant(user_id) {

    var restaurant_id = $('#restaurant_' + user_id).val();

    var params = {
        user_id: user_id,
        restaurant_id: restaurant_id
    };

    $.ajax({
        url: '/edit/user/restaurant',
        type: 'POST',
        data: {params: params, "_token": $('#token').val()},

        success: function (response) {
            showSuccessNotification('User Restaurant Has Been Changed');
        },
        error: function (error) {
            showErrorNotification();
        }
    });



}


function editCard(card_id, card_name) {

    $('#card_id').val(card_id);
    $('#edit_card_name').val(card_name);

    $("#edit_card_modal").modal('show');

}


function deleteCard(card_id) {

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function () {

        var params = {
            card_id: card_id
        };

        $.ajax({
            url: '/delete/card',
            type: 'POST',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                $("#bank_card_" + card_id).remove();
                showSuccessNotification('Bank Card Has Been Deleted');
            },
            error: function (error) {
                showErrorNotification();
            }
        });

    });

}


function editPaymentMethod(method_id, method_name) {

    $('#method_id').val(method_id);
    $('#edit_method_name').val(method_name);

    $("#edit_method_modal").modal('show');

}


function deletePaymentMethod(method_id) {

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function () {

        var params = {
            method_id: method_id
        };

        $.ajax({
            url: '/delete/payment-method',
            type: 'POST',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                $("#method_" + method_id).remove();
                showSuccessNotification('Payment Method Has Been Deleted');
            },
            error: function (error) {
                showErrorNotification();
            }
        });

    });

}


function viewRestaurant(restaurant_id) {
    redirect('/restaurant/' + restaurant_id, 1);
}


function deleteRestaurant(restaurant_id) {
    alert("delete : " + restaurant_id);
}


function randomPassword(length) {
    var chars = "abcdefghijklmnopqrstuvwxyz!@#$%ABCDEFGHIJKLMNOP1234567890";
    var pass = "";
    for (var x = 0; x < length; x++) {
        var i = Math.floor(Math.random() * chars.length);
        pass += chars.charAt(i);
    }
    return pass;
}


function generatePassword() {
    var length = 15;
    $("#user_password").val(randomPassword(length));
}


function addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + parseInt(days));
    return result;
}


function formatDate(value) {
    return value.getMonth()+1 + "/" + value.getDate() + "/" + value.getFullYear();
}


function editUser(id, name, role, phone) {

    $("#edit_user_id").val(id);
    $("#modal_user_name").val(name);
    $("#modal_user_role").val(role);
    $("#modal_user_phone").val(phone);

    $("#edit_user_modal").modal('show');
}


function deleteUser(id) {

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function () {

        var params = {
            id: id
        };

        $.ajax({
            url: '/delete/user',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                $("#user_" + id).remove();
                swal({
                    type: 'success',
                    html: $('<div>')
                        .addClass('some-class')
                        .text('User has been deleted'),
                    animation: false,
                    customClass: 'animated bounceIn'
                });
            },
            error: function (error) {
                console.log("Error! Something went wrong.");
            }
        });

    }, function (dismiss) {
        if (dismiss === 'cancel') {
            swal({
                title: 'Cancelled',
                type: 'error',
                html: $('<div>')
                    .addClass('some-class')
                    .text('Your User is safe :)'),
                animation: false,
                customClass: 'animated bounceIn'
            });
        }
    });
}


function deleteLicense(license_id) {

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-danger',
        cancelButtonClass: 'btn btn-success',
        buttonsStyling: false
    }).then(function () {

        var params = {
            license_id: license_id
        };

        $.ajax({
            url: '/license/delete',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                $("#license_" + license_id).remove();
                swal('Done!', 'License Has Been Deleted', 'success');
            },
            error: function (error) {
                swal('Error!', 'Something went wrong.', 'error');
            }
        });

    });
}


function activateLicense(license_id) {

    var params = {
        license_id: license_id
    };

    $.ajax({

        url: '/license/activate',
        type: 'POST',
        format: 'JSON',
        data: {params: params, "_token": $('#token').val()},

        success: function (response) {

            swal('Done!', 'License Activated', 'success');

            setTimeout(function(){
                var pathname = window.location.pathname;
                window.location.href = pathname;
            },1000);



        },
        error: function (error) {
            swal('Error!', 'Something Went Wrong', 'error');

        }

    });
}


function addPayment() {

    var payment_method = $('#modal_payment_method').val();
    var amount = $('#modal_amount').val();
    var bank_card = $('#modal_bank_card').val();
    var bank_card_name = $('#modal_bank_card option:selected').text();
    var card_number = $('#modal_card_number').val();

    if (payment_method == 'cash') {
        bank_card = '';
        bank_card_name = '';
        card_number = '';
    }

    if (amount != '') {



        payment_methods.push([payment_method, amount, bank_card, bank_card_name, card_number]);

        $('#modal_amount').val('');
        $('#modal_card_number').val('');

        printPaymentMethod();

    }


}


function printPaymentMethod() {

    var htmlstr = '';
    var htmlstr_payment = '';
    var total_amount = 0;

    for (var i=0; i < payment_methods.length; i++) {

        var payment_method = payment_methods[i][0];
        var amount = payment_methods[i][1];
        var bank_card = payment_methods[i][2];
        var bank_card_name = payment_methods[i][3];
        var card_number = payment_methods[i][4];

        total_amount += parseFloat(amount);

        htmlstr += '<tr id="payment_'+(i+1)+'">' +
            '<td>'+payment_method+'</td>' +
            '<td>'+bank_card_name+'</td>' +
            '<td>'+card_number+'</td>' +
            '<td class="text-right">'+amount+'</td>' +
            '<td>' +
            '<button type="button" class="btn btn-danger" onclick="deletePayment('+i+')">' +
            '<i class="glyphicon glyphicon-remove"></i>' +
            '</button>' +
            '</td>' +
            '</tr>';


    }

    htmlstr += '<tr>' +
        '<td></td>' +
        '<td></td>' +
        '<td></td>' +
        '<td class="text-right">Total: <b id="payment_total_amount">'+total_amount+'</b></td>' +
        '<td></td>' +
        '</tr>';



    $('#payment_table').empty();
    $('#payment_table').append(htmlstr);


    var receipt_total_amount = parseFloat($( "#total_bill" ).val());
    var payment_total_amount = parseFloat($( "#payment_total_amount" ).text());

    var rest_amount = receipt_total_amount - payment_total_amount;
    $( "#modal_amount" ).val(rest_amount);

}


function deletePayment(index) {
    payment_methods.splice(index, 1);
    printPaymentMethod();
}


function editOrderType(type_id, type_name) {

    $("#edit_type_id").val(type_id);
    $("#edit_type_name").val(type_name);

    $("#edit_order_type_modal").modal('show');
}


function deleteOrderType(type_id) {

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function () {

        var params = {
            type_id: type_id
        };

        $.ajax({
            url: '/delete/order-type',
            type: 'POST',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                $("#type_" + type_id).remove();
                showSuccessNotification('Order Type Has Been Deleted');
            },
            error: function (error) {
                showErrorNotification();
            }
        });

    });

}
