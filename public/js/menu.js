$(document).ready(function() {

    $('#menu_table_span').DataTable();


    $("#add_menu_button").click(function(){

        var menu_name = $("#menu_name").val();

        if (menu_name != "") {
            var params = {
                menu_name: menu_name,
            };
            $.ajax({
                url: '/add/menu',
                type: 'POST',
                data: {params: params, "_token": $('#token').val()},  //send these values to controller

                success: function (response) {
                    //console.log(response);
                    $("#menu_name").val("");

                    var htmlstr = '<tr id="menu_'+response+'" onclick="view_menu_items(\''+response+'\', \''+menu_name+'\')">' +
                        '<td scope="row">#</td>' +
                        '<td id="menu_name_'+response+'">'+menu_name+'</td><td>' +
                        '<button type="button" onclick="editMenu(\''+response+'\', \''+menu_name+'\')" class="btn btn-dark">' +
                        '<i class="glyphicon glyphicon-pencil"></i></button> ' +
                        '<button type="button" class="btn btn-danger" onclick="deleteMenu('+response+')">' +
                        '<i class="glyphicon glyphicon-remove"></i>' +
                        '</button></td></tr>';

                    $( "#menu_table" ).append(htmlstr);

                    htmlstr = '<option value="'+ response +'">'+ menu_name +'</option>';
                    $( "#select_menu_id" ).append(htmlstr);

                    //swal("", menu_name + " Added to Menu", "success")

                    showSuccessNotification(menu_name + " Added to Menu");
                },
                error: function (error) {
                    console.log("Error! Something went wrong.");
                }
            });
        }
    });


    $("#add_food_item_button").click(function(){

        var menu_id = $("#select_menu_id").val();
        var item_name = $("#item_name").val();
        var price = $("#item_price").val();
        var cost_price = $("#cost_price").val();
        var item_vat = $("#item_vat").val();
        var set_menu = $("#set_menu").is(':checked')?1:0;
        var discount_available = $("#discount_available").is(':checked')?1:0;


        if (menu_id != 0 && item_name != "" && item_vat != "" && price != '') {

            var params = {
                menu_id: menu_id,
                item_name: item_name,
                ratio: $("#item_ratio").val(),
                price: price,
                cost_price: cost_price,
                item_vat: item_vat,
                set_menu: set_menu,
                discount_available: discount_available,
                category: $("#item_category").val(),
                printer_id: $("#printer_id").val()
            };

            $.ajax({
                url: '/add/menu-item',
                type: 'POST',
                format: 'JSON',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {

                    $("#item_name").val("");
                    $("#item_ratio").val("");
                    $("#item_price").val("");
                    $("#cost_price").val("0");
                    $("#item_category").val("");
                    $("#set_menu"). prop("checked", false);

                    showSuccessNotification(item_name + " Saved to Menu Item");
                },
                error: function (error) {
                    showErrorNotification();
                }
            });

        }
        else {
            showWarningNotification('Please select a Menu or Item name required');
        }
    });


    $("#edit_menu_button").click(function(){

        var menu_id = $("#edit_menu_id").val();
        var menu_name = $("#modal_menu_name").val();


        if (menu_name != "") {

            var params = {
                menu_id: menu_id,
                menu_name: menu_name,
            };

            $.ajax({
                url: '/edit/menu',
                type: 'POST',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {

                    $("#menu_name_" + menu_id).text(menu_name);

                    showSuccessNotification("Menu Updated Successfully");

                },
                error: function (error) {
                    console.log("Error! Something went wrong.");
                }
            });

        }

    });


    $("#edit_item_button").click(function(){

        var menu_item_id = $("#edit_item_id").val();

        var item_name = $("#modal_item_name").val();
        var ratio = $("#modal_item_ratio").val();
        var price = $("#modal_item_price").val();
        var cost_price = $("#modal_cost_price").val();
        var item_discount = $("#modal_item_discount").val();
        var item_vat = $("#modal_item_vat").val();
        var status = ( $("#modal_item_status").is(':checked') ) ? 1 : 0;
        var printer_id = $("#modal_printer_id").val();
        var menu_id = $("#modal_menu_id").val();


        if (item_name != "" && price>0) {

            var params = {
                menu_item_id: menu_item_id,
                item_name: item_name,
                ratio: ratio,
                price: price,
                cost_price: cost_price,
                item_discount: item_discount,
                item_vat: item_vat,
                status: status,
                printer_id: printer_id,
                menu_id: menu_id
            };

            $.ajax({
                url: '/edit/menu-item',
                type: 'POST',
                format: 'JSON',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {

                    $("#item_name_" + menu_item_id).text(item_name);
                    $("#item_ratio_" + menu_item_id).text(ratio);
                    $("#item_price_" + menu_item_id).text(price);
                    $("#item_discount_" + menu_item_id).text(item_discount);

                    showSuccessNotification("Menu Item Updated Successfully");


                },
                error: function (error) {
                    console.log("Error! Something went wrong.");
                }
            });

        }

    });


    $("#edit_ready_item_button").click(function(){

        var menu_item_id = $("#edit_item_id").val();
        var readymade_item = ( $("#readymade_item").is(':checked') ) ? 1 : 0;
        var item_quantity = $("#item_quantity").val();


        if (item_quantity != "") {

            var params = {
                menu_item_id: menu_item_id,
                readymade_item: readymade_item,
                item_quantity: item_quantity
            };

            $.ajax({
                url: '/edit/readymade-item',
                type: 'POST',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {

                    readymade_item = readymade_item==1?'Yes':'No';

                    $('#readymade_item_' + menu_item_id).text(readymade_item);
                    $('#item_quantity_' + menu_item_id).text(item_quantity);

                    swal('Done!', 'Readymade Item Updated Successfully', 'success');
                },
                error: function (error) {
                    console.log("Error! Something went wrong.");
                }
            });

        }

    });


    $("#add_setmenu_item_button").click(function(){

        var menu_item_id = $("#modal_menu_item_id").val();
        var item_name = $("#modal_setmenu_item_name").val();

        if (item_name != "") {

            var params = {
                menu_item_id: menu_item_id,
                item_name: item_name
            };

            $.ajax({
                url: '/add/setmenu-item',
                type: 'POST',
                format: 'JSON',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {

                    $("#modal_setmenu_item_name").val("");

                    var htmlstr = '<tr>' +
                        '<th scope="row">#</th>' +
                        '<td>'+ item_name +'</td>' +
                        '<td width="25%">' +
                        '<button type="button" class="btn btn-dark">' +
                        '<i class="glyphicon glyphicon-pencil"></i></button> ' +
                        '<button type="button" class="btn btn-danger">' +
                        '<i class="glyphicon glyphicon-remove"></i></button></td></tr>';

                    $( "#setmenu_item_table" ).append(htmlstr);
                },
                error: function (error) {
                    console.log("Error! Something went wrong.");
                }
            });
        }
    });


    $("#save_discount_circular").click(function(){

        var circular_name = $("#circular_name").val();
        var discount = $("#discount").val();
        var discount_type = $("#discount_type").val();
        var start_date = $("#single_cal1").val();
        var end_date = $("#single_cal3").val();

        if (circular_name != '' && discount != '') {

            var params = {
                circular_name: circular_name,
                discount: discount,
                discount_type: discount_type,
                start_date: start_date,
                end_date: end_date
            };

            $.ajax({
                url: '/add/discount-circular',
                type: 'POST',
                format: 'JSON',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {
                    reloadCurrentPage();
                },
                error: function (error) {
                    console.log("Error! Something went wrong.");
                }
            });
        }
    });


});


function view_menu_items(menu_id, menu_name) {

    var params = {
        menu_id: menu_id
    };

    $.ajax({
        url: '/view/menu-item',
        type: 'POST',
        data: {params: params, "_token": $('#token').val()},

        success: function (response) {
            var json_message = $.parseJSON(response);
            var htmlstr = '<tbody id="menu_item_list">';

            for (var i=0; i<json_message.length; i++) {
                var now = Date.parse(json_message[i].created_at)/1000;

                var item_id = json_message[i].menu_item_id;
                var item_name = json_message[i].item_name;
                var ratio = "";
                var price = json_message[i].price;
                var cost_price = json_message[i].cost_price;
                var item_discount = json_message[i].item_discount;
                var item_vat = json_message[i].item_vat;
                var readymade_item = json_message[i].readymade_item;
                var set_menu = parseInt(json_message[i].set_menu);
                var status = json_message[i].status==1?'Active':'Inactive';
                var printer_id = json_message[i].printer_id;

                if (json_message[i].ratio != null) ratio = json_message[i].ratio;

                htmlstr += '<tr id="menu_item_'+ item_id +'"><td scope="row">'+ (i+1)+'</td>' +
                    '<td id="item_name_'+ item_id +'">'+ item_name +'</td>' +
                    '<td id="item_ratio_'+ item_id +'">'+ ratio +'</td>' +
                    '<td id="item_price_'+ item_id +'">'+ price +'</td>' +
                    '<td id="item_cost_price_'+ item_id +'">'+ cost_price +'</td>' +
                    '<td id="item_discount_'+ item_id +'">'+ item_discount +'</td>' +
                    '<td id="item_vat_'+ item_id +'">'+ item_vat +'</td>' +
                    '<td>'+ status +'</td>' +
                    '<td>';


                if (set_menu == 1) {

                    htmlstr += '<button type="button" class="btn btn-dark" onclick="viewSetMenu(\''+item_id+'\')">' +
                        '<i class="glyphicon glyphicon-eye-open"></i></button> ';

                }



                htmlstr += '<button type="button" class="btn btn-dark pointer" title="Edit Item" onclick="editItem(\''+item_id+'\', \''+item_name+'\', \''+ratio+'\', \''+price+'\', \''+readymade_item+'\', \''+item_discount+'\', \''+status+'\', \''+printer_id+'\', \''+menu_id+'\', \''+item_vat+'\', \''+cost_price+'\')"><i class="glyphicon glyphicon-pencil"></i></button> ' +
                    '<button type="button" class="btn btn-danger" title="Delete Item" onclick="deleteItem(\''+json_message[i].menu_item_id+'\')"><i class="glyphicon glyphicon-remove"></i></button></td></tr>';
            }

            htmlstr += '</tbody>';

            $( "#menu_item_title" ).text(menu_name + " : " + json_message.length + " items");


            $( "#menu_item_list" ).remove();
            $( "#menu_item_table" ).append(htmlstr);

            $('#menu_item_table').DataTable();

        },
        error: function (error) {
            console.log("Error! Something went wrong.");
        }
    });

}


function view_readymade_items(menu_id, menu_name) {

    var params = {
        menu_id: menu_id
    };

    $.ajax({
        url: '/view/menu-item',
        type: 'POST',
        data: {params: params, "_token": $('#token').val()},

        success: function (response) {
            var json_message = $.parseJSON(response);
            var htmlstr = '';

            for (var i=0; i<json_message.length; i++) {
                var now = Date.parse(json_message[i].created_at)/1000;

                var item_id = json_message[i].menu_item_id;
                var item_name = json_message[i].item_name;
                var ratio = "";
                var price = json_message[i].price;
                var item_discount = json_message[i].item_discount;
                var item_vat = json_message[i].item_vat;
                var readymade_item = json_message[i].readymade_item;
                var readymade_item_str = json_message[i].readymade_item==1?'Yes':'No';
                var item_quantity = json_message[i].item_quantity;
                var status = json_message[i].status;
                var status_str = json_message[i].status==1?'Active':'Inactive';

                if (json_message[i].ratio != null) ratio = json_message[i].ratio;

                htmlstr += '<tr id="menu_item_id_'+ item_id +'"><td scope="row">'+ (i+1)+'</td>' +
                    '<td id="item_name_'+ item_id+'">'+ item_name +'</td>' +
                    '<td id="readymade_item_'+ item_id+'">'+ readymade_item_str +'</td>' +
                    '<td id="item_quantity_'+ item_id+'">'+ item_quantity +'</td>' +
                    '<td>'+ status_str +'</td>' +
                    '<td>' +
                    '<button type="button" class="btn btn-dark pointer" onclick="editReadymadeItem(\''+item_id+'\', \''+readymade_item+'\', \''+item_quantity+'\')"><i class="glyphicon glyphicon-pencil"></i></button> ' +
                    '</td>' +
                    '</tr>';
            }



            $( "#menu_item_title" ).text(menu_name + " : " + json_message.length + " items");


            $( "#menu_item_list" ).empty();
            $( "#menu_item_list" ).append(htmlstr);

        },
        error: function (error) {
            console.log("Error! Something went wrong.");
        }
    });

}


function deleteMenu(menu_id) {

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function () {

        var params = {
            menu_id: menu_id,
        };

        $.ajax({
            url: '/delete/menu',
            type: 'POST',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                $("#menu_" + menu_id).remove();
                swal({
                    type: 'success',
                    html: $('<div>')
                        .addClass('some-class')
                        .text('Menu has been deleted'),
                    animation: false,
                    customClass: 'animated bounceIn'
                });
            },
            error: function (error) {
                console.log("Error! Something went wrong.");
            }
        });

    }, function (dismiss) {
        if (dismiss === 'cancel') {
            swal({
                title: 'Cancelled',
                type: 'error',
                html: $('<div>')
                    .addClass('some-class')
                    .text('Your Menu is safe :)'),
                animation: false,
                customClass: 'animated bounceIn'
            });
        }
    });
}


function editMenu(menu_id, menu_name) {

    $("#edit_menu_title").text("Edit Menu : " + menu_name);
    $("#modal_menu_name").val(menu_name);
    $("#edit_menu_id").val(menu_id);

    $("#edit_menu_modal").modal('show');
}


function deleteItem(item_id) {

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-danger',
        cancelButtonClass: 'btn btn-success',
        buttonsStyling: false
    }).then(function () {

        var params = {
            menu_item_id: item_id,
        };

        $.ajax({
            url: '/delete/menu-item',
            type: 'POST',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                $("#menu_item_" + item_id).remove();
                swal({
                    type: 'success',
                    html: $('<div>')
                        .addClass('some-class')
                        .text('Item has been deleted'),
                    animation: false,
                    customClass: 'animated bounceIn'
                });
            },
            error: function (error) {
                console.log("Error! Something went wrong.");
            }
        });

    });
}


function editItem(item_id, item_name, item_ratio, item_price, readymade_item, item_discount, item_status, printer_id, menu_id, item_vat, cost_price) {

    $("#edit_item_title").text("Edit Item : " + item_name);
    $("#edit_item_id").val(item_id);

    $("#modal_item_name").val(item_name);
    $("#modal_item_ratio").val(item_ratio);
    $("#modal_item_price").val(item_price);
    $("#modal_cost_price").val(cost_price);
    if (readymade_item == "1") $('#readymade_item').prop('checked', true);
    $("#modal_item_discount").val(item_discount);
    $("#modal_item_vat").val(item_vat);
    if (item_status == "1") $('#modal_item_status').prop('checked', true);

    $("#modal_menu_id").val(menu_id);
    $("#modal_printer_id").val(printer_id);

    $("#edit_item_modal").modal('show');
}


function editReadymadeItem(item_id, readymade_item, item_quantity) {

    $("#edit_item_id").val(item_id);

    if (readymade_item == "1") $('#readymade_item').prop('checked', true);
    else $('#readymade_item').prop('checked', false);

    $("#item_quantity").val(item_quantity);


    $("#edit_ready_item_modal").modal('show');
}


function viewSetMenu(item_id) {

    $("#modal_menu_item_id").val(item_id);

    var params = {
        menu_item_id: item_id
    };

    $.ajax({
        url: '/get/setmenu-items',
        type: 'POST',
        format: 'JSON',
        data: {params: params, "_token": $('#token').val()},

        success: function (response) {
            var json_message = $.parseJSON(response);
            var htmlstr = '';

            for (var i=0; i<json_message.length; i++) {

                var item_id = json_message[i].item_id;
                var item_name = json_message[i].item_name;

                htmlstr += '<tr>' +
                    '<th scope="row">'+ (i+1) +'</th>' +
                    '<td>'+ item_name +'</td>' +
                    '<td width="25%">' +
                    '<button type="button" class="btn btn-dark">' +
                    '<i class="glyphicon glyphicon-pencil"></i></button> ' +
                    '<button type="button" class="btn btn-danger">' +
                    '<i class="glyphicon glyphicon-remove"></i></button></td></tr>';


            }

            $( "#setmenu_item_table" ).empty();
            $( "#setmenu_item_table" ).append(htmlstr);

        },
        error: function (error) {
            console.log("Error! Something went wrong.");
        }
    });




    $("#edit_setmenu_modal").modal('show');
}


function deleteCircular(circular_id) {

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function () {

        var params = {
            circular_id: circular_id,
        };

        $.ajax({
            url: '/delete/circular',
            type: 'POST',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                $("#circular_" + circular_id).remove();

            },
            error: function (error) {
                console.log("Error! Something went wrong.");
            }
        });

    }, function (dismiss) {
        if (dismiss === 'cancel') {
            swal({
                title: 'Cancelled',
                type: 'error',
                html: $('<div>')
                    .addClass('some-class')
                    .text('Your Menu is safe :)'),
                animation: false,
                customClass: 'animated bounceIn'
            });
        }
    });
}




