$(document).ready(function() {

    $('#generate_report_button').click(function () {

        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();

        var params = {
            start_date: start_date,
            end_date: end_date
        };

        $.ajax({
            url: '/generate/revenue-report',
            type: 'POST',
            format: 'JSON',
            data: {params:params, '_token': $('#token').val()},
            success: function (response) {

                var start_datetime = new Date(start_date);
                var formatted_start = start_datetime.getDate() + "-" + months[start_datetime.getMonth()] + "-" + start_datetime.getFullYear();

                var end_datetime = new Date(end_date);
                var formatted_end = end_datetime.getDate() + "-" + months[end_datetime.getMonth()] + "-" + end_datetime.getFullYear();

                $('#date_header').text(formatted_start + ' To ' + formatted_end);


                var json_message = $.parseJSON(response);
                var htmlstr = '';

                var cash_sale = 0;
                var card_sale = 0;
                var total_expense = 0;
                var cash_in_hand = 0;

                for (var i=0; i<json_message.length; i++) {

                    var cash = json_message[i].cash_sale==null?0:json_message[i].cash_sale;
                    var card = json_message[i].card_sale==null?0:json_message[i].card_sale;
                    var expense = json_message[i].total_expense==null?0:json_message[i].total_expense;
                    var cash_in_hand_temp = parseFloat(cash) - parseFloat(expense);
                    var start_time = json_message[i].start_time;

                    cash_sale += parseFloat(cash);
                    card_sale += parseFloat(card);
                    total_expense += parseFloat(expense);
                    cash_in_hand += parseFloat(cash_in_hand_temp);

                    htmlstr += '<tr>' +
                        '<td>'+ (i+1) +'</td>' +
                        '<td>'+ formatDate(start_time) +'</td>' +
                        '<td>'+ cash+'</td>' +
                        '<td>'+ card+'</td>' +
                        '<td>'+ expense+'</td>' +
                        '<td>'+ cash_in_hand_temp+'</td></tr>';

                }

                htmlstr += '<tr>' +
                    '<th></th>' +
                    '<td><b>Total</b></td>' +
                    '<td><b>'+ cash_sale +'</b></td>' +
                    '<td><b>'+ card_sale +'</b></td>' +
                    '<td><b>'+ total_expense+'</b></td>' +
                    '<td><b>'+ cash_in_hand +'</b></td></tr>';

                $('#report_table_tbody').empty();
                $('#report_table_tbody').append(htmlstr);


            },
            error: function (error) {
                showErrorNotification();
            }
        });


    });

});
