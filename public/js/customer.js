
$(document).ready(function() {


    $("#add_customer_button").click(function(){

        var customer_name = $('#customer_name').val();
        var customer_phone = $('#customer_phone').val();
        var customer_email = $('#customer_email').val();
        var customer_address = $('#customer_address').val();
        var card_no = $('#card_no').val();
        var discount_percentage = $('#discount_percentage').val();

        if (customer_name != '' && customer_phone != '' && customer_address != '') {

            var params = {
                customer_name: customer_name,
                customer_phone: customer_phone,
                customer_email: customer_email,
                customer_address: customer_address,
                card_no: card_no,
                discount_percentage: discount_percentage
            };

            $.ajax({
                url: '/add/client',
                type: 'POST',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {

                    var htmlstr = '<tr class="even" id="customer_'+response+'">' +
                        '<td>#</td>' +
                        '<td id="customer_name_'+response+'">'+customer_name+'</td>' +
                        '<td id="customer_email_'+response+'">'+customer_email+'</td>' +
                        '<td id="customer_address_'+response+'">'+customer_address+'</td>' +
                        '<td id="customer_phone_'+response+'">'+customer_phone+'</td>' +
                        '<td id="card_no_'+response+'">'+card_no+'</td>' +
                        '<td id="discount_percentage_'+response+'">'+discount_percentage+'%</td>' +
                        '<td>'+getCurrentDate()+'</td>' +
                        '<td>' +
                        '<button type="button" class="btn btn-dark" onclick="editCustomer(\''+response+'\', \''+customer_name+'\', \''+customer_email+'\', \''+customer_address+'\', \''+customer_phone+'\', \''+card_no+'\', \''+discount_percentage+'\')">' +
                        '<i class="glyphicon glyphicon-pencil"></i>' +
                        '</button> ' +
                        '<button type="button" class="btn btn-danger" onclick="deleteCustomer('+response+')">' +
                        '<i class="glyphicon glyphicon-remove"></i>' +
                        '</button></td></tr>';

                    $( "#customer_table" ).append(htmlstr);

                    //swal("Done!", customer_name + " Added To Database", "success");
                },
                error: function (error) {
                    console.log("Error! Something went wrong.");
                }
            });
        }

        else {
            swal({
                type: 'warning',
                html: $('<div>')
                    .addClass('some-class')
                    .text('Input Customer Name and Phone Number'),
                animation: false,
                customClass: 'animated bounceIn'
            });
        }
    });


    $('#edit_customer_button').click(function () {

        var customer_id = $('#edit_customer_id').val();
        var customer_name = $('#edit_customer_name').val();
        var customer_phone = $('#edit_customer_phone').val();
        var customer_email = $('#edit_customer_email').val();
        var customer_address = $('#edit_customer_address').val();
        var card_no = $('#edit_card_no').val();
        var discount_percentage = $('#edit_discount_percentage').val();

        if (customer_name != "") {

            var params = {
                customer_id: customer_id,
                customer_name: customer_name,
                customer_phone: customer_phone,
                customer_email: customer_email,
                customer_address: customer_address,
                card_no: card_no,
                discount_percentage: discount_percentage
            };

            $.ajax({
                url: '/edit/client',
                type: 'POST',
                format: 'JSON',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {

                    $('#customer_name_' + customer_id).text(customer_name);
                    $('#customer_email_' + customer_id).text(customer_email);
                    $('#customer_address_' + customer_id).text(customer_address);
                    $('#customer_phone_' + customer_id).text(customer_phone);
                    $('#card_no_' + customer_id).text(card_no);
                    $('#discount_percentage_' + customer_id).text(discount_percentage + '%');

                    //swal('Done!', 'Successfully Updated Modifier', 'success');

                },
                error: function (error) {
                    swal('Error!', 'Something went wrong.', 'error');
                }
            });
        }
    });


});



function editCustomer(customer_id, customer_name, customer_email, customer_address, customer_phone, card_no, discount_percentage) {

    $("#edit_customer_id").val(customer_id);
    $("#edit_customer_name").val(customer_name);
    $("#edit_customer_phone").val(customer_phone);
    $("#edit_customer_address").val(customer_address);
    $("#edit_customer_email").val(customer_email);
    $("#edit_card_no").val(card_no);
    $("#edit_discount_percentage").val(discount_percentage);


    $('#edit_customer_modal').modal('show');
}


function deleteCustomer(customer_id) {

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-danger',
        cancelButtonClass: 'btn btn-success',
        buttonsStyling: false
    }).then(function () {

        var params = {
            customer_id: customer_id
        };

        $.ajax({
            url: '/delete/client',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {

                $("#customer_" + customer_id).remove();

                swal('Done', 'Customer Has Been Deleted', 'success');
            },
            error: function (error) {
                console.log("Error! Something went wrong.");
            }
        });

    });
}









