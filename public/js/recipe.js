$(document).ready(function() {

    //$('#product_id').select2();

    $('#item_table').DataTable();


    $('#add_recipe_button').click(function () {

        var item_id = $('#item_id').val();
        var product_id = $('#product').val().split("-");
        product_id = product_id[0];
        var product_name = $('#product :selected').text();

        var unit_of_quantity = $('#unit_of_quantity').val();
        var unit_of_quantity_text = $('#unit_of_quantity :selected').text();

        var quantity = $('#quantity').val();
        var store_id = $('#store_id').val();

        var unit_name = $('#quantity_span').text();

        if (item_id > 0) {

            var params = {
                item_id: item_id,
                product_id :product_id,
                unit_of_quantity :unit_of_quantity,
                quantity: quantity,
                store_id: store_id
            };

            $.ajax({
                url: '/add/recipe',
                type: 'POST',
                format: 'JSON',
                data: { '_token': $('#token').val(), params: params},

                success: function (recipe_id) {

                    $('#product_category').val('0');
                    $('#product').val('0');
                    $('#quantity').val('');

                    var htmlstr = '<tr class="pointer" id="recipe_'+recipe_id+'">' +
                        '<td scope="row">#</td>' +
                        '<td>'+ product_name +'</td>' +
                        '<td>'+ quantity + ' ' + unit_of_quantity_text+'</td>' +
                        '<td>' +
                        '<button type="button" class="btn btn-dark">' +
                        '<i class="glyphicon glyphicon-pencil"></i>' +
                        '</button> ' +
                        '<button type="button" class="btn btn-danger" onclick="deleteRecipe(\''+recipe_id+'\')">' +
                        '<i class="glyphicon glyphicon-remove"></i>' +
                        '</button>' +
                        '</td></tr>';

                    $( "#recipe_table" ).append(htmlstr);

                },
                error: function (error) {

                    showErrorNotification();

                }

            });


        }


    });


    $("#quantity").keyup(function(){

        var product_id = $('#product').val().split("-");
        product_id = product_id[0];
        var quantity = $('#quantity').val();

        if (product_id != '') {

            for (var i = 0; i < products.length; i++) {

                var id = products[i][0];
                var price = products[i][6];

                if (product_id == id) $('#cost').val(price * quantity);
            }

        }

    });


    $('#edit_recipe_button').click(function () {

        var recipe_id = $('#recipe_id').val();
        var quantity = $('#edit_quantity').val();

        if (quantity > 0 && quantity != '') {

            var params = {
                recipe_id: recipe_id,
                quantity: quantity
            };

            $.ajax({
                url: '/edit/recipe',
                type: 'POST',
                format: 'JSON',
                data: { '_token': $('#token').val(), params: params},

                success: function (response) {

                    showSuccessNotification('Successfully Edited Recipe!');

                    var item_id = $('#edit_item_id').val();
                    var item_name = $('#edit_item_name').val();

                    view_recipe(item_id, item_name);

                },
                error: function (error) {

                }

            });


        }


    });


});


function view_recipe(item_id, item_name) {

    $( "#button_recipe_modal" ).removeClass( "hidden" );
    $( "#item_id" ).val(item_id);

    var params = {
        item_id: item_id
    };

    $.ajax({
        url: '/view/recipe',
        type: 'POST',
        format: 'JSON',
        data: {params: params, "_token": $('#token').val()},

        success: function (response) {

            var json_message = $.parseJSON(response);
            var htmlstr = '';

            for (var i=0; i<json_message.length; i++) {

                var recipe_id = json_message[i].recipe_id;
                var item_id = json_message[i].item_id;
                var product_id = json_message[i].product_id;
                var product_name = json_message[i].product_name;
                var quantity = json_message[i].quantity;
                var unit_name = json_message[i].unit_name;
                var store_id = json_message[i].store_id;
                var status = json_message[i].status;

                htmlstr += '<tr class="pointer" id="recipe_'+recipe_id+'">' +
                    '<td scope="row">'+ (i+1) +'</td>' +
                    '<td>'+ product_name +'</td>' +
                    '<td>'+ quantity + ' ' + unit_name+'</td>' +
                    '<td>' +
                    '<button type="button" class="btn btn-dark" onclick="editRecipe(\''+recipe_id+'\', \''+product_id+'\', \''+product_name+'\', \''+quantity+'\', \''+unit_name+'\', \''+item_id+'\', \''+item_name+'\')">' +
                    '<i class="glyphicon glyphicon-pencil"></i>' +
                    '</button> ' +
                    '<button type="button" class="btn btn-danger" onclick="deleteRecipe(\''+recipe_id+'\')">' +
                    '<i class="glyphicon glyphicon-remove"></i>' +
                    '</button>' +
                    '</td></tr>';


            }

            $( "#item_title" ).text(item_name + " : " + json_message.length + " recipe items");
            $( "#modal_item_title" ).text(item_name);

            $( "#recipe_table" ).empty();
            $( "#recipe_table" ).append(htmlstr);

        },
        error: function (error) {
            console.log("Error! Something went wrong.");
        }
    });

}


function editRecipe(recipe_id, product_id, product_name, quantity, unit_name, item_id, item_name) {

    $('#recipe_id').val(recipe_id);
    $('#edit_quantity').val(quantity);

    $('#edit_item_id').val(item_id);
    $('#edit_item_name').val(item_name);

    $('#edit_quantity_span').text(unit_name);

    var htmlstr = '<option value="'+product_id+'">'+product_name+'</option>';
    $('#edit_product').empty();
    $('#edit_product').append(htmlstr);

    $('#edit_modal_item_title').text(product_name);

    $('#edit_recipe_modal').modal('show');

}


function deleteRecipe(recipe_id) {

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function () {

        var params = {
            recipe_id: recipe_id
        };

        $.ajax({
            url: '/delete/recipe',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {

                $("#recipe_" + recipe_id).remove();

                showSuccessNotification('Recipe Has Been Deleted');

            },
            error: function (error) {
                console.log("Error! Something went wrong.");
            }
        });

    }, function (dismiss) {
        if (dismiss === 'cancel') {
            swal({
                title: 'Cancelled',
                type: 'error',
                html: $('<div>')
                    .addClass('some-class')
                    .text('Your Recipe is safe :)'),
                animation: false,
                customClass: 'animated bounceIn'
            });
        }
    });
}










