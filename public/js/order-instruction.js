
$(document).ready(function() {


    $("#add_instruction_button").click(function () {

        var instruction = $('#instruction').val();

        if (instruction != '') {

            var params = {
                instruction: instruction
            };

            $.ajax({
                url: '/add/order-instruction',
                type: 'POST',
                format: 'JSON',
                data: {params: params, '_token': $('#token').val()},

                success: function (response) {

                    $('#instruction').val('');

                    var htmlstr = '<tr class="even" id="order_instruction_'+response+'">' +
                        '<td>#</td>' +
                        '<td id="instruction_'+response+'">'+instruction+'</td>' +
                        '<td>'+ getCurrentDate() +'</td>' +
                        '<td>' +
                        '<button type="button" class="btn btn-dark" onclick="editInstruction(\''+response+'\', \''+instruction+'\')">' +
                        '<i class="glyphicon glyphicon-pencil"></i>' +
                        '</button>' +
                        '<button type="button" class="btn btn-danger" onclick="deleteInstruction('+response+')">' +
                        '<i class="glyphicon glyphicon-remove"></i>' +
                        '</button>' +
                        '</td>' +
                        '</tr>';


                    $('#instruction_table').append(htmlstr);
                    //swal("Done!", "Successfully Added Order Instruction", "success");

                },
                error: function (error) {
                    swal("Error!", "Something went wrong", "error");
                }
            });
        }

    });


    $('#edit_instruction_button').click(function () {

        var instruction_id = $('#edit_instruction_id').val();
        var instruction = $('#edit_instruction').val();

        if (instruction != "") {

            var params = {
                instruction_id: instruction_id,
                instruction: instruction
            };

            $.ajax({
                url: '/edit/order-instruction',
                type: 'POST',
                format: 'JSON',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {

                    $('#instruction_' + instruction_id).text(instruction);

                    //swal('Done!', 'Successfully Updated Modifier', 'success');

                },
                error: function (error) {
                    swal('Error!', 'Something went wrong.', 'error');
                }
            });
        }
    });


});



function editInstruction(instruction_id, instruction) {

    $("#edit_instruction_id").val(instruction_id);
    $("#edit_instruction").val(instruction);

    $('#edit_instruction_modal').modal('show');
}


function deleteInstruction(instruction_id) {

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-danger',
        cancelButtonClass: 'btn btn-success',
        buttonsStyling: false
    }).then(function () {

        var params = {
            instruction_id: instruction_id
        };

        $.ajax({
            url: '/delete/order-instruction',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {

                $("#order_instruction_" + instruction_id).remove();

                swal('Done', 'Order Instruction Has Been Deleted', 'success');
            },
            error: function (error) {
                console.log("Error! Something went wrong.");
            }
        });

    });
}









