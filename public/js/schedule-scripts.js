$(document).ready(function() {


    $("#run_scripts_button").click(function(){

        $.ajax({
            url: '/run-scripts',
            type: 'POST',
            format: 'JSON',
            data: {"_token": $('#token').val()},

            success: function (response) {

                swal('Done!', 'You Just Run All The Scripts Manually', 'success');

                var pathname = window.location.pathname;
                setTimeout(function(){
                    window.location.href = pathname;
                },2000);

            },
            error: function (error) {
                swal('Error!', 'Something Went Wrong', 'error');
            }
        });


    });


    $("#db_fix_scripts_button").click(function(){

        $.ajax({
            url: '/db-fix-scripts',
            type: 'POST',
            format: 'JSON',
            data: {"_token": $('#token').val()},

            success: function (response) {

                swal('Done!', 'Database Fixing Scripts', 'success');

                var pathname = window.location.pathname;
                setTimeout(function(){
                    window.location.href = pathname;
                },1000);

            },
            error: function (error) {
                swal('Error!', 'Something Went Wrong', 'error');
            }
        });


    });


    $("#fix_order_button").click(function(){

        $.ajax({
            url: '/db-fix-order-scripts',
            type: 'POST',
            format: 'JSON',
            data: {"_token": $('#token').val()},

            success: function (response) {

                swal('Done!', 'Database Fixing Scripts', 'success');

                var pathname = window.location.pathname;
                setTimeout(function(){
                    window.location.href = pathname;
                },1000);

            },
            error: function (error) {
                swal('Error!', 'Something Went Wrong', 'error');
            }
        });

    });


    $("#demo_data_button").click(function(){

        $.ajax({
            url: '/load/demo-data',
            type: 'POST',
            format: 'JSON',
            data: {"_token": $('#token').val()},

            success: function (response) {
                showSuccessNotification('Demo Data Loaded');
            },
            error: function (error) {
                showErrorNotification();
            }
        });


    });


});










