var temp_response = [];

$(document).ready(function() {

    $('#select_report_type').change(function () {

        var select_report_type = $('#select_report_type').val();

        if (select_report_type == 'waiter_wise_void') {

            $('#select_table_div').hide();
            $('#select_order_id').hide();
            $('#select_item_div').hide();
            $('#select_menu_div').hide();


            $('#select_waiter_div').show();


            $('#report_header').text('Waiter Wise Void');
        }


        else if (select_report_type == 'invoice_wise_void') {

            $('#select_waiter_div').hide();
            $('#select_table_div').hide();
            $('#select_order_id').hide();
            $('#select_item_div').hide();
            $('#select_menu_div').hide();


            $('#report_header').text('Invoice Wise Void');
        }


        else if (select_report_type == 'item_wise_void') {

            $('#select_waiter_div').hide();
            $('#select_table_div').hide();
            $('#select_order_id').hide();
            $('#select_menu_div').hide();


            $('#select_item_div').show();


            $('#report_header').text('Item Wise Void');
        }




    });


    $('#generate_report_button').click(function () {

        var select_report_type = $('#select_report_type').val();
        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();

        if (select_report_type == '0') {
            swal('', 'Please Select Any Report Type', 'warning');
        }

        else {

            var params;

            if (select_report_type == 'waiter_wise_void') {
                var waiter_id = $('#select_waiter').val();
                params = {
                    select_report_type: select_report_type,
                    waiter_id: waiter_id,
                    start_date: start_date,
                    end_date: end_date
                }
            }

            else if (select_report_type == 'invoice_wise_void' || select_report_type == 'total_void') {
                params = {
                    select_report_type: select_report_type,
                    start_date: start_date,
                    end_date: end_date
                };
            }

            else if (select_report_type == 'item_wise_void') {

                params = {
                    select_report_type: select_report_type,
                    item_id: $('#select_item').val(),
                    start_date: start_date,
                    end_date: end_date
                }
            }



            $.ajax({
                url: '/generate/void-report',
                type: 'POST',
                format: 'JSON',
                data: {params:params, '_token': $('#token').val()},
                success: function (response) {
                    temp_response = response;
                    print_table(response, 'all', select_report_type);

                    var start_datetime = new Date(start_date);
                    var formatted_start = start_datetime.getDate() + "-" + months[start_datetime.getMonth()] + "-" + start_datetime.getFullYear();

                    var end_datetime = new Date(end_date);
                    var formatted_end = end_datetime.getDate() + "-" + months[end_datetime.getMonth()] + "-" + end_datetime.getFullYear();

                    $('#date_header').text(formatted_start + ' To ' + formatted_end);
                },
                error: function (error) {
                    swal('Error!', 'Something went wrong', 'error');
                }
            });
        }

    });


    $('#payment_method').change(function () {

        var payment_method = $('#payment_method').val();
        print_table(temp_response, payment_method);



    });




});




function calculatePercentage(amount, percentage) {

    var result = parseFloat(amount * (percentage/100.00));
    return result;

}


function print_table(response, payment, select_report_type) {

    var json_message = $.parseJSON(response);

    var htmlstr = '';
    var htmlstr_header = '';

    if (select_report_type == 'invoice_wise_void') {

        htmlstr_header = '<th class="column-title text-center">KOT #</th>' +
            '<th class="column-title text-center">Waiter Name</th>' +
            '<th class="column-title text-center">Total Item Amount</th>' +
            '<th class="column-title text-center">Void Amount</th>' +
            '<th class="column-title text-center">Reason</th>' +
            '<th class="column-title text-center">Notes</th>' +
            '<th class="column-title text-center">Void By</th>';

    }


    if (select_report_type == 'total_void') {

        htmlstr_header = '<th class="column-title text-center">KOT #</th>' +
            '<th class="column-title text-center">Total Item Amount</th>' +
            '<th class="column-title text-center">Void Amount</th>' +
            '<th class="column-title text-center">Reason</th>' +
            '<th class="column-title text-center">Notes</th>' +
            '<th class="column-title text-center">Void By</th>';

    }


    else if (select_report_type == 'item_wise_void') {

        htmlstr_header = '<th class="column-title text-center">KOT #</th>' +
            '<th class="column-title text-center">Waiter Name</th>' +
            '<th class="column-title text-center">Item Name</th>' +
            '<th class="column-title text-center">Category</th>' +
            '<th class="column-title text-center">Void Amount</th>' +
            '<th class="column-title text-center">Reason</th>' +
            '<th class="column-title text-center">Notes</th>' +
            '<th class="column-title text-center">Void By</th>';

    }


    else if (select_report_type == 'waiter_wise_void') {

        htmlstr_header = '<th class="column-title text-center">KOT #</th>' +
            '<th class="column-title text-center">Waiter Name</th>' +
            '<th class="column-title text-center">Void Amount</th>' +
            '<th class="column-title text-center">Reason</th>' +
            '<th class="column-title text-center">Notes</th>' +
            '<th class="column-title text-center">Void By</th>';

    }



    $('#report_table_header').empty();
    $( "#report_table_header" ).append(htmlstr_header);



    for (var i=0; i<json_message.length; i++) {

        var order_id = json_message[i].order_id;
        var waiter_name = json_message[i].name;
        var reason = json_message[i].reason;
        var notes = json_message[i].notes;
        var void_by = json_message[i].void_by;



        if (select_report_type == 'invoice_wise_void') {

            var bill = json_message[i].bill;
            var item_price = json_message[i].item_price;
            var void_quantity = json_message[i].void_quantity;
            var total = parseFloat(bill) + parseFloat(item_price * void_quantity);

            htmlstr += '<tr class="even pointer" onclick="generateReceipt('+order_id+')">' +
                '<td>'+ order_id +'</td>' +
                '<td>'+ waiter_name +'</td>' +
                '<td>'+ total +'</td>' +
                '<td>'+ (item_price * void_quantity) +'</td>' +
                '<td>'+ reason +'</td>' +
                '<td>'+ notes +'</td>' +
                '<td>'+ void_by +'</td>' +
                '</tr>';

        }


        if (select_report_type == 'total_void') {

            var bill = json_message[i].bill;
            var item_price = json_message[i].item_price;
            var void_quantity = json_message[i].void_quantity;
            var total = parseFloat(bill) + parseFloat(item_price * void_quantity);

            htmlstr += '<tr class="even pointer" onclick="generateReceipt('+order_id+')">' +
                '<td>'+ order_id +'</td>' +
                '<td>'+ total +'</td>' +
                '<td>'+ (item_price * void_quantity) +'</td>' +
                '<td>'+ reason +'</td>' +
                '<td>'+ notes +'</td>' +
                '<td>'+ void_by +'</td>' +
                '</tr>';

        }


        else if (select_report_type == 'item_wise_void') {

            var item_name = json_message[i].menu_item_name;
            var menu_name = json_message[i].menu_name;
            var item_price = json_message[i].item_price;
            var item_quantity = json_message[i].void_quantity;


            htmlstr += '<tr class="even pointer">' +
                '<td>'+ order_id +'</td>' +
                '<td>'+ waiter_name +'</td>' +
                '<td>'+ item_name +'</td>' +
                '<td>'+ menu_name +'</td>' +
                '<td>'+ (item_price * item_quantity) +'</td>' +
                '<td>'+ reason +'</td>' +
                '<td>'+ notes +'</td>' +
                '<td>'+ void_by +'</td>' +
                '</tr>';


        }


        else if (select_report_type == 'waiter_wise_void') {

            var item_price = json_message[i].item_price;
            var item_quantity = json_message[i].void_quantity;


            htmlstr += '<tr class="even pointer">' +
                '<td>'+ order_id +'</td>' +
                '<td>'+ waiter_name +'</td>' +
                '<td>'+ (item_price * item_quantity) +'</td>' +
                '<td>'+ reason +'</td>' +
                '<td>'+ notes +'</td>' +
                '<td>'+ void_by +'</td>' +
                '</tr>';


        }

    }

    $('#report_table_tbody').empty();
    $('#report_table_tbody').append(htmlstr);

}


function printReport() {

    window.print();

}


function generateReceipt(order_id) {

    var params = {
        order_id: order_id
    };

    $.ajax({
        url: '/get/order/history',
        type: 'POST',
        format: 'JSON',
        data: {params: params, "_token": $('#token').val()},

        success: function (response) {

            var json_message = JSON.parse(response);

            var order_data = json_message.order_data;
            var order_details = json_message.order_details;
            var item_modifiers = json_message.order_modifiers;
            var order_payments = json_message.order_payments;

            var payment_modal_count = 1;
            var htmlstr_modal = '';
            var htmlstr_payment = '';
            var total_amount = 0;


            //console.log(order_details);

            for (var i=0; i<order_details.length; i++) {

                var order_details_id = order_details[i].order_details_id;
                var menu_item_id = order_details[i].menu_item_id;
                var menu_item_name = order_details[i].menu_item_name;
                var item_price = parseInt(order_details[i].item_price);
                var item_discount = parseInt(order_details[i].item_discount);
                var item_quantity = parseInt(order_details[i].item_quantity);
                var unit_price = ((item_price - item_discount));
                var item_status = parseInt(order_details[i].item_status);
                var total_cost = (unit_price * item_quantity);

                var modifiers_str = '';
                var item_modifier_price = 0;


                for (var j=0; j<item_modifiers.length; j++) {

                    var modifier_order_details_id = item_modifiers[j].order_details_id;
                    var modifier_name = item_modifiers[j].modifier_name;
                    var modifier_price = item_modifiers[j].price;
                    var modifier_quantity = item_modifiers[j].quantity;

                    if (order_details_id == modifier_order_details_id) {

                        modifiers_str += ', ' + modifier_name + ' x' + modifier_quantity;
                        item_modifier_price += (parseInt(modifier_price) * parseInt(modifier_quantity));
                        unit_price += parseInt(modifier_price);

                    }

                }


                /*
                 receipt_order_table
                 */

                htmlstr_modal += '<tr>' +
                    '<th scope="row">'+ payment_modal_count++ +'</th>' +
                    '<td class="text-center">'+ menu_item_name + modifiers_str +'</td>' +
                    '<td class="text-center">'+ unit_price +'</td>' +
                    '<td class="text-center">'+ item_quantity +'</td>' +
                    '<td class="text-center">'+ (total_cost + item_modifier_price) +'</td></tr>';


            }


            for (var i=0; i<order_payments.length; i++) {

                var payment_method = order_payments[i].payment_method;
                var amount = order_payments[i].amount;
                var card_name = order_payments[i].card_name==null?'':order_payments[i].card_name;
                var card_number = order_payments[i].card_number==null?'':order_payments[i].card_number;
                total_amount += parseFloat(amount);

                /*
                 Payment Method Table
                 */

                htmlstr_payment += '<tr>' +
                    '<td class="text-center">'+ payment_method +'</td>' +
                    '<td class="text-center">'+ card_name +'</td>' +
                    '<td class="text-center">'+ card_number +'</td>' +
                    '<td class="text-right text-center">'+ amount +'</td></tr>';
            }

            htmlstr_payment += '<tr>' +
                '<td></td>' +
                '<td></td>' +
                '<td></td>' +
                '<td class="text-right text-center"><b>'+ total_amount +'</b></td></tr>';




            $( ".receipt_order_table" ).empty();
            $( ".receipt_order_table" ).append(htmlstr_modal);


            $( ".payment_table" ).empty();
            $( ".payment_table" ).append(htmlstr_payment);


            $( ".receipt_customer_name" ).text(order_data.customer_name);
            $( ".receipt_waiter_name" ).text(order_data.waiter_name);
            $( ".receipt_created_at" ).text(formatDate(order_data.created_at));
            $( ".receipt_table_no" ).text(order_data.table_name);
            $( ".receipt_order_no" ).text(formatInvoiceDate(order_data.created_at) + numberPadding(order_data.order_id));
            $( ".receipt_order_type" ).text(uc_first(order_data.order_type));


            $( ".receipt_subtotal" ).text(order_data.bill);
            $( ".receipt_vat_total" ).text(order_data.vat);
            $( ".receipt_service_charge" ).text(order_data.service_charge);
            $( ".receipt_sd_percentage" ).text(order_data.sd_charge);
            $( ".receipt_service_charge_vat_percentage" ).text(order_data.service_charge_vat);
            $( ".receipt_sd_vat_percentage" ).text(order_data.sd_vat);
            $( ".receipt_discount_amount" ).text(order_data.discount);
            $( ".receipt_total_amount" ).text(order_data.total_bill);


            $('#order_details_modal').modal('show');




        },
        error: function (error) {
            swal('Error!', 'Something went wrong', 'error');
        }
    });

}