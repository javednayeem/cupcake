$(document).ready(function() {



});


function changeOrderStatus(order_id, order_status) {

    var params = {
        order_id: order_id,
        order_status: order_status,
    };

    $.ajax({
        url: '/change/online-order-status',
        type: 'POST',
        data: {params: params, "_token": $('#token').val()},

        success: function (response) {
            reloadCurrentPage();
        },
        error: function (error) {
            showErrorNotification();
        }
    });

}


function generateToken(order_id) {

    var params = {
        order_id: order_id
    };

    $.ajax({
        url: '/get/online-order/history',
        type: 'POST',
        format: 'JSON',
        data: {params: params, "_token": $('#token').val()},

        success: function (response) {

            var json_message = JSON.parse(response);

            var order = json_message.order;
            var order_details = json_message.order_details;

            var token_html = '';

            var order_id = order.order_id;
            var scheduled_at = onlineOrderDateTime(order.scheduled_at);
            var order_notes = order.order_notes;

            for (var i=0; i<order_details.length; i++) {

                var menu_item_name = order_details[i].menu_item_name;
                var ratio = order_details[i].ratio != null?order_details[i].ratio:'';
                var item_quantity = parseInt(order_details[i].item_quantity);

                if (item_quantity > 0) {

                    token_html += '<tr>' +
                        '<td>'+menu_item_name+ ' ' + ratio +'</td>' +
                        '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>' +
                        '<td class="text-right">'+item_quantity+'</td>' +
                        '</tr>';

                }
            }

            $( "#token_div_table" ).empty();
            $( "#token_div_table" ).append(token_html);

            $( "#scheduled_at" ).append(scheduled_at);

            $('#order_id').text(order_id);

            if (order_notes != '' && order_notes != null) {

                $("#notes_span").show();

                var html = '<li>'+order_notes+'</li>';

                $('#token_instruction').empty();
                $('#token_instruction').append(html);
            }

            else $("#notes_span").hide();

            printTokenReceipt();

        },
        error: function (error) {
            showErrorNotification();
        }
    });

}


function printTokenReceipt() {
    w = window.open();
    w.document.write($('#kitchen_token').html());
    w.print();
    w.close();
}

