$(document).ready(function() {

    $('#clear_queue').click(function () {

        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, clear it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-danger',
            cancelButtonClass: 'btn btn-success',
            buttonsStyling: false
        }).then(function () {

            $.ajax({
                url: '/clear/order-queue',
                type: 'POST',
                data: {"_token": $('#token').val()},

                success: function (response) {
                    reloadCurrentPage();
                },
                error: function (error) {
                    showErrorNotification();
                }
            });
        });
    });



});


function order_served(order_id) {

    $.ajax({
        url: '/order/served',
        type: 'POST',
        data: {order_id: order_id, "_token": $('#token').val()},

        success: function (response) {
            reloadCurrentPage();
        },
        error: function (error) {
            showErrorNotification();
        }
    });

}


function updateOrderQueue() {

    $.ajax({
        url: '/update/order-queue',
        type: 'POST',
        data: {"_token": $('#token').val()},

        success: function (response) {

            var json_message = JSON.parse(response);

            var count = json_message.count;
            var order_id = json_message.order_id;

            if (count == 0) $('#queue_span').text('No Order In Queue');
            else $('#queue_span').text('Token #' + order_id);

            setTimeout(updateOrderQueue, 5000);

        },
        error: function (error) {
            showErrorNotification();
        }
    });


}


function queueTimer() {
    setTimeout(updateOrderQueue, 5000);
}

