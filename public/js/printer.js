
$(document).ready(function() {


    $("#add_printer_button").click(function () {

        var printer_name = $('#printer_name').val();
        var port = $('#port').val();

        if (printer_name != '') {

            var params = {
                printer_name: printer_name,
                port: port
            };

            $.ajax({
                url: '/add/printer',
                type: 'POST',
                format: 'JSON',
                data: {params: params, '_token': $('#token').val()},

                success: function (response) {

                    var htmlstr = '<tr class="even" id="printer_'+response+'">' +
                        '<td>#</td>' +
                        '<td id="printer_name_'+response+'">'+printer_name+'</td>' +
                        '<td id="port_'+response+'">'+port+'</td>' +
                        '<td>'+ getCurrentDate() +'</td>' +
                        '<td>' +
                        '<button type="button" class="btn btn-dark" ' +
                        'onclick="editPrinter(\''+response+'\', \''+printer_name+'\', \''+port+'\')">' +
                        '<i class="glyphicon glyphicon-pencil"></i>' +
                        '</button>' +
                        '<button type="button" class="btn btn-danger" onclick="deletePrinter('+response+')">' +
                        '<i class="glyphicon glyphicon-remove"></i>' +
                        '</button>' +
                        '</td></tr>';


                    $('#printer_table').append(htmlstr);

                    $('#printer_name').val('');
                    $('#port').val('');

                    //swal("Done!", "Successfully Added Modifier To Database", "success");

                },
                error: function (error) {
                    swal("Error!", "Something went wrong", "error");
                }
            });
        }

    });


    $('#edit_printer_button').click(function () {

        var printer_id = $('#edit_printer_id').val();
        var printer_name = $('#edit_printer_name').val();
        var port = $('#edit_port').val();

        if (printer_name != "") {

            var params = {
                printer_id: printer_id,
                printer_name: printer_name,
                port: port
            };

            $.ajax({
                url: '/edit/printer',
                type: 'POST',
                format: 'JSON',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {

                    $('#printer_name_' + printer_id).text(printer_name);
                    $('#port_' + printer_id).text(port);

                    //swal('Done!', 'Successfully Updated Modifier', 'success');

                },
                error: function (error) {
                    swal('Error!', 'Something went wrong.', 'error');
                }
            });
        }
    });


});



function editPrinter(printer_id, printer_name, port) {

    $("#edit_printer_id").val(printer_id);
    $("#edit_printer_name").val(printer_name);
    $("#edit_port").val(port);

    $('#edit_printer_modal').modal('show');
}


function deletePrinter(printer_id) {

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-danger',
        cancelButtonClass: 'btn btn-success',
        buttonsStyling: false
    }).then(function () {

        var params = {
            printer_id: printer_id
        };

        $.ajax({
            url: '/delete/printer',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {

                $("#printer_" + printer_id).remove();

                swal('Done', 'Printer Has Been Deleted', 'success');
            },
            error: function (error) {
                console.log("Error! Something went wrong.");
            }
        });

    });
}









