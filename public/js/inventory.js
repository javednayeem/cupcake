var products = [];
var units = [];
var invoice_orders = [];
var transfer_product = [];
var invoice_table_no = 1;


$(document).ready(function() {

    $('#category_id').select2();
    $('#product_id').select2();


    $('#inv_product_store_table').DataTable({
        "lengthChange": false
    });


    $('#inventory_products_table').DataTable({
        "lengthChange": false
    });


    $("#add_supplier_button").click(function () {

        var company_name = $('#company_name').val();
        var supplier_name = $('#supplier_name').val();
        var address = $('#address').val();
        var email = $('#email').val();
        var phone = $('#phone').val();
        var act_code = $('#act_code').val();

        if (supplier_name == '') {
            swal('', 'Supplier Name Missing', 'warning');
        }

        else if (company_name == '') {
            swal('', 'Company Name Missing', 'warning');
        }

        else if (address == '') {
            swal('', 'Address Missing', 'warning');
        }

        else if (phone == '') {
            swal('', 'Phone Number Missing', 'warning');
        }

        else {
            var params = {
                company_name: company_name,
                supplier_name: supplier_name,
                address: address,
                email: email,
                phone: phone,
                act_code: act_code
            };

            $.ajax({
                url: '/add/supplier',
                type: 'POST',
                format: 'JSON',
                data: {params: params, '_token': $('#token').val()},
                success: function (response) {

                    var htmlstr = '<tr class="even" id="supplier_'+ response+'">' +
                        '<td>#</td>' +
                        '<td id="company_name_'+response+'">'+ company_name +'</td>' +
                        '<td id="supplier_name_'+ response +'">'+ supplier_name +'</td>' +
                        '<td>'+ address +'</td>' +
                        '<td>'+ email +'</td>' +
                        '<td>'+ phone +'</td>' +
                        '<td>'+ getCurrentDate() +'</td>' +
                        '<td><button type="button" class="btn btn-dark" title="Edit Supplier" onclick="edit_supplier()">' +
                        '<i class="glyphicon glyphicon-pencil"></i></button> ' +
                        '<button type="button" class="btn btn-danger" title="Delete Supplier" >' +
                        '<i class="glyphicon glyphicon-remove"></i></button></td></tr>';

                    $('#supplier_table').append(htmlstr);
                    clearInputValue('add_supplier_modal');

                    swal("Done!", "Successfully Added Supplier To Database", "success");

                },
                error: function (error) {
                    swal("Error!", "Something went wrong", "error");
                }
            });
        }

    });


    $('#add_category_button').click(function () {

        var category_name = $('#modal_category_name').val();

        if (category_name != '') {
            var params = {
                category_name:category_name
            };

            $.ajax({
                url: '/add/category',
                type: 'POST',
                format: 'JSON',
                data: {params: params, '_token': $('#token').val()},

                success: function (response) {
                    $('#modal_category_name').val('');

                    var htmlstr = '<tr class="even" id="category_'+ response +'">' +
                        '<td>#</td>' +
                        '<td id="category_name_'+ response +'">'+ category_name +'</td>' +
                        '<td>'+ getCurrentDate() +'</td>' +
                        '<td>' +
                        '<button type="button" class="btn btn-dark" onclick="editCategory(\''+ response +'\')">' +
                        '<i class="glyphicon glyphicon-pencil"></i></button> ' +
                        '<button type="button" class="btn btn-danger" onclick="deleteCategory(\''+ response +'\')">' +
                        '<i class="glyphicon glyphicon-remove"></i>' +
                        '</button></td></tr>';

                    $('#category_table').append(htmlstr);

                    swal('Done!', 'Successfully Added Category To Database', 'success');


                },
                error: function (error) {
                    swal('Error!', 'Something went wrong', 'error');
                }

            });
        }
    });


    $('#add_product_button').click(function () {

        var product_name = $('#product_name').val();
        var product_code = $('#product_code').val();
        var category_id = $('#category_id').val();
        var fixed_price = $('#fixed_price').val();
        var low_inv_alert = $('#low_inv_alert').val();
        if (low_inv_alert<=0 || low_inv_alert == '') low_inv_alert = 5;
        var unit_id = $('#unit_id').val();

        var product_category_name = $('#category_id option:selected').text();
        var product_unit_name = $('#unit_id option:selected').text();

        if (product_name != '') {
            var params = {
                product_name: product_name,
                product_code: product_code,
                category_id: category_id,
                fixed_price: fixed_price,
                low_inv_alert: low_inv_alert,
                unit_id: unit_id
            };

            $.ajax({
                url: '/add/product',
                type: 'POST',
                format: 'JSON',
                data: {params: params, '_token': $('#token').val()},

                success: function (response) {

                    $('#product_name').val('');
                    $('#product_code').val('');
                    $('#category_id').val('');
                    $('#low_inv_alert').val('5');
                    $('#unit_id').val('');

                    var htmlstr = '<tr class="even" id="product_'+ response +'">' +
                        '<td>#</td>' +
                        '<td id="product_code_'+ response +'">'+ product_code +'</td>' +
                        '<td id="product_name_'+response+'">'+ product_name +'</td>' +
                        '<td id="product_cat_'+response+'">'+product_category_name+'</td>' +
                        '<td id="product_unit_'+response+'">'+product_unit_name+'</td>' +
                        '<td>'+fixed_price+'৳</td>' +
                        '<td>0 ৳</td>' +
                        '<td>'+ getCurrentDate() +'</td><td>' +
                        '<button type="button" class="btn btn-dark" onclick="editProduct(\''+ response +'\')">' +
                        '<i class="glyphicon glyphicon-pencil"></i></button> ' +
                        '<button type="button" class="btn btn-danger" onclick="deleteProduct(\''+ response +'\')">' +
                        '<i class="glyphicon glyphicon-remove"></i></button></td></tr>';

                    $('#product_table').append(htmlstr);

                    swal('Done!', 'Successfully Added Product To Database', 'success');
                },
                error: function (error) {
                    swal('Error!', 'Something went wrong', 'error');
                }

            });
        }

    });


    $('#add_supplier_info_button').click(function () {
        var invoice_no = $('#invoice_no').val();
        var supplier = $('#supplier').val();
        var purchase_date = $('#purchase_date').val();
        var delivery_date = $('#delivery_date').val();

        if (invoice_no != '' && supplier>0 && purchase_date != '' && delivery_date != '') {

            var supplier_name = $('#supplier option:selected').text();
            var supplier_id = $('#supplier option:selected').val();

            $('#supplier_span').text(supplier_name);
            $('#supplier_id').val(supplier_id);
            $('#purchase_date_span').text(purchase_date);
            $('#delivery_date_span').text(delivery_date);
            $('#invoice_no_span').text(invoice_no);

            $("#add_supplier_info_button").text("Update Supplier Info");



        }
        else {
        }
    });


    $('#product_category').change(function() {

        var product_category = $('#product_category').val();

        if (product_category != 0) {
            var htmlstr = '<option value="0-0">Select Product</option>';
            for (var i = 0; i < products.length; i++) {

                var product_id = products[i][0];
                var product_name = products[i][1];
                var status = products[i][2];
                var unit_id = products[i][3];
                var category_id = products[i][4];
                var count = products[i][5];

                if (category_id == product_category) {

                    htmlstr += '<option value="'+ product_id +'-'+ unit_id +'">'+ product_name +'</option>';
                }
            }

            $('#product').empty();
            $('#product').append(htmlstr);
        }

    });


    $('#product').change(function() {

        var product = $('#product').val();
        var token = product.split("-");
        var product_id = token[0];

        if (product_id != 0) {

            var product_unit_id = token[1];

            for (var i = 0; i < units.length; i++) {

                var unit_id = units[i][0];
                var unit_name = units[i][1];

                if (unit_id == product_unit_id) {
                    $('#quantity_span').text(unit_name);
                    $('#product_unit_name').val(unit_name);

                    var product_quantity = getProductQuantity(product_id);
                    $('#max_product_quantity').val(product_quantity);
                    $('#product_quantity_span').text('(Available: '+ product_quantity+' '+ unit_name +')');

                    break;
                }
            }
        }

    });


    $('#add_product_order_button').click(function () {

        var product_name = $('#product option:selected').text();
        var product_category_name = $('#product_category option:selected').text();
        var unit_price = $('#unit_price').val();
        var quantity = parseFloat($('#quantity').val());
        var expire_date = $('#expire_date').val();

        var product_category_id = $('#product_category').val();
        var product_id = $('#product').val();
        var token = product_id.split("-");
        product_id = token[0];

        if (product_category_id >0 && unit_price != '' && quantity>0.0) {

            var total = (parseFloat(unit_price) * parseFloat(quantity));
            total = total.toFixed(2);

            invoice_orders.push([product_category_id, product_category_name, product_id, product_name, unit_price, quantity, total, expire_date]);

            order_table_print();

            $('#unit_price').val('');
            $('#quantity').val('');
        }


    });


    $('#submit_payment_button').click(function () {

        inventoryPurchase();

    });


    $('#add_unit_button').click(function () {
        var unit_name = $('#modal_unit_name').val();

        if (unit_name != '') {
            var params = {
                unit_name:unit_name
            };

            $.ajax({
                url: '/add/unit',
                type: 'POST',
                format: 'JSON',
                data: {params: params, '_token': $('#token').val()},

                success: function (response) {
                    $('#modal_unit_name').val('');

                    var htmlstr = '<tr class="even" id="unit_'+ response +'">' +
                        '<td>#</td>' +
                        '<td id="unit_name_'+ response +'">'+ unit_name +'</td>' +
                        '<td>'+ getCurrentDate() +'</td>' +
                        '<td>' +
                        '<button type="button" class="btn btn-dark" title="Edit Unit"><i class="glyphicon glyphicon-pencil"></i></button> ' +
                        '<button type="button" class="btn btn-danger" title="Delete Unit" ><i class="glyphicon glyphicon-remove"></i></button>' +
                        '</td></tr>';

                    $('#unit_table').append(htmlstr);

                    swal('Done!', 'Successfully Added Unit To Database', 'success');


                },
                error: function (error) {
                    swal('Error!', 'Something went wrong', 'error');
                }

            });
        }
    });


    $('.extra-cost').keyup(function () {
        var discount_input = $('#discount_input').val();
        var shipping_input = $('#shipping_input').val();
        var subtotal_input = $('#subtotal_input').val();
        var amount = (parseInt(subtotal_input) - parseInt(discount_input) + parseInt(shipping_input));
        $('#grand_total_input').val(amount);
    });


    $('#edit_category_button').click(function () {

        var category_id = $("#edit_category_id").val();
        var category_name = $("#edit_category_name").val();

        if (category_name != "") {

            var params = {
                category_id: category_id,
                category_name: category_name
            };

            $.ajax({
                url: '/edit/category',
                type: 'POST',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {

                    $('#category_name_' + category_id).text(category_name);

                    swal('Done!', 'Successfully Updated Product Category', 'success');
                },
                error: function (error) {
                    swal('Error!', 'Something went wrong.', 'error');
                }
            });
        }
    });


    $('#edit_unit_button').click(function () {

        var unit_id = $("#edit_unit_id").val();
        var unit_name = $("#edit_unit_name").val();

        if (unit_name != "") {

            var params = {
                unit_id: unit_id,
                unit_name: unit_name
            };

            $.ajax({
                url: '/edit/unit',
                type: 'POST',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {
                    swal('Done!', 'Successfully Updated Product Unit', 'success');
                },
                error: function (error) {
                    swal('Error!', 'Something went wrong.', 'error');
                }
            });
        }
    });


    $("#addNewProd").click(function (e) {
        $('#product_code').val(randomString(2));
    });


    $("#add_store_button").click(function () {

        var store_name = $('#store_name').val();
        var store_address = $('#store_address').val();
        var store_phone = $('#store_phone').val();
        var store_email = $('#store_email').val();

        if (store_name != '') {

            var params = {
                store_name: store_name,
                store_address: store_address,
                store_phone: store_phone,
                store_email: store_email
            };

            $.ajax({
                url: '/add/store',
                type: 'POST',
                format: 'JSON',
                data: {params: params, '_token': $('#token').val()},

                success: function (response) {

                    var htmlstr = '<tr class="even" id="store_'+ response +'">' +
                        '<td>#</td>' +
                        '<td>'+ store_name +'</td>' +
                        '<td>'+ store_address +'</td>' +
                        '<td>'+ store_phone +'</td>' +
                        '<td>'+ store_email +'</td>' +
                        '<td>'+ getCurrentDate() +'</td>' +
                        '<td><button type="button" class="btn btn-dark" onclick="edit_store(\''+ response +'\')">' +
                        '<i class="glyphicon glyphicon-pencil"></i></button> ' +
                        '<button type="button" class="btn btn-danger" onclick="delete_store(\''+ response +'\')">' +
                        '<i class="glyphicon glyphicon-remove"></i></button></td></tr>';

                    $('#store_table').append(htmlstr);
                    clearInputValue('add_store_modal');

                    swal("Done!", "Successfully Added Store To Database", "success");

                },
                error: function (error) {
                    swal("Error!", "Something went wrong", "error");
                }
            });
        }

    });


    $('#add_product_transfer_button').click(function () {

        var product_name = $('#product option:selected').text();
        var product_category_name = $('#product_category option:selected').text();
        var quantity = $('#quantity').val();

        var product_category_id = $('#product_category').val();
        var product_id = $('#product').val();
        var token = product_id.split("-");
        product_id = token[0];

        var max_product_quantity = $('#max_product_quantity').val();

        product_category_id = parseInt(product_category_id);
        quantity = parseFloat(quantity);
        max_product_quantity = parseInt(max_product_quantity);

        if (product_category_id >0 && quantity>-1 &&quantity<=max_product_quantity) {

            $('#quantity').val('');
            $('#product_category').val(0);
            $('#product').val(0);
            $('#product_quantity_span').text('');

            var price = getProductPrice(product_id);

            transfer_product.push([product_category_name, product_id, product_name, quantity, price]);
            updateProductQuantity(product_id, quantity);
            transfer_table_print();
        }
        else if (quantity>max_product_quantity) {

            var max_product_quantity = $('#max_product_quantity').val();
            var product_unit_name = $('#product_unit_name').val();


            $('#product_quantity_span').text('(Can\'t exceed '+ max_product_quantity +' '+ product_unit_name +')');

        }

    });


    $('#add_transfer_info_button').click(function () {

        var store_name = $('#store_id option:selected').text();
        var store_id = $('#store_id').val();
        var transfer_date = $('#single_cal1').val();

        if (store_id != 0) {

            $('#transfer_date_span').text(transfer_date);
            $('#store_name_span').text(store_name);

        }

    });


    $('#submit_transfer_button').click(function () {

        var from_store_id = $('#main_store_id').val();
        var to_store_id = $('#store_id').val();
        var transfer_date = $('#single_cal1').val();

        var params = {
            from_store_id: from_store_id,
            to_store_id: to_store_id,
            transfer_date: transfer_date,
            transfer_product :transfer_product
        };


        $.ajax({
            url: '/transfer-stock',
            type: 'POST',
            format: 'JSON',
            data: {params: params, '_token': $('#token').val()},

            success: function (response) {
                swal('Done!', 'Successfully Transferred Stock Products', 'success');

                setTimeout(function(){
                    window.location.href = "/transfer-history";
                },1000);
            },
            error: function (error) {
                swal('Error!', 'Something went wrong', 'error');
            }

        });
    });


    $('#add_product_receive_button').click(function () {

        var product_name = $('#product option:selected').text();
        var product_category_name = $('#product_category option:selected').text();
        var quantity = $('#quantity').val();

        var product_category_id = $('#product_category').val();
        var product_id = $('#product').val();
        var token = product_id.split("-");
        product_id = token[0];

        product_category_id = parseInt(product_category_id);
        quantity = parseInt(quantity);


        $('#quantity').val('');
        $('#product_category').val(0);
        $('#product').val(0);

        transfer_product.push([product_category_name, product_id, product_name, quantity]);
        transfer_table_print();

    });


    $('#submit_receive_button').click(function () {

        var from_store_id = $('#store_id').val();
        var to_store_id = $('#main_store_id').val();

        var transfer_date = $('#single_cal1').val();

        var params = {
            from_store_id: from_store_id,
            to_store_id: to_store_id,
            transfer_date: transfer_date,
            transfer_product :transfer_product
        };


        $.ajax({
            url: '/receive-stock',
            type: 'POST',
            format: 'JSON',
            data: {params: params, '_token': $('#token').val()},

            success: function (response) {
                swal('Done!', 'Successfully Received Stock Products', 'success');

                setTimeout(function(){
                    window.location.href = "/transfer-history";
                },1000);
            },
            error: function (error) {
                swal('Error!', 'Something went wrong', 'error');
            }

        });
    });


    $('#edit_supplier_button').click(function () {

        var supplier_id = $("#edit_supplier_id").val();
        var company_name = $("#edit_company_name").val();
        var supplier_name = $("#edit_supplier_name").val();
        var address = $("#edit_address").val();
        var email = $("#edit_email").val();
        var phone = $("#edit_phone").val();

        if (supplier_name != "") {

            var params = {
                supplier_id: supplier_id,
                company_name: company_name,
                supplier_name: supplier_name,
                address: address,
                email: email,
                phone: phone
            };

            $.ajax({
                url: '/edit/supplier',
                type: 'POST',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {

                    $('#company_name_' + supplier_id).text(company_name);
                    $('#supplier_name_' + supplier_id).text(supplier_name);
                    $('#address_' + supplier_id).text(address);
                    $('#email_' + supplier_id).text(email);
                    $('#phone_' + supplier_id).text(phone);

                    swal('Done!', 'Successfully Updated Supplier', 'success');
                },
                error: function (error) {
                    swal('Error!', 'Something went wrong.', 'error');
                }
            });
        }
    });


    $('#edit_store_button').click(function () {

        var store_id = $("#edit_store_id").val();
        var store_name = $("#edit_store_name").val();
        var store_address = $("#edit_store_address").val();
        var store_phone = $("#edit_store_phone").val();
        var store_email = $("#edit_store_email").val();

        if (store_name != "") {

            var params = {
                store_id: store_id,
                store_name: store_name,
                store_address: store_address,
                store_phone: store_phone,
                store_email: store_email
            };

            $.ajax({
                url: '/edit/store',
                type: 'POST',
                format: 'JSON',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {

                    $('#store_name_' + store_id).text(store_name);
                    $('#store_address_' + store_id).text(store_address);
                    $('#store_phone_' + store_id).text(store_phone);
                    $('#store_email_' + store_id).text(store_email);

                    swal('Done!', 'Successfully Updated Store Information', 'success');
                },
                error: function (error) {
                    swal('Error!', 'Something went wrong.', 'error');
                }
            });
        }
    });


    $('#edit_product_button').click(function () {

        var product_id = $("#edit_product_id").val();
        var category_id = $("#edit_category_id").val();
        var product_name = $("#edit_product_name").val();
        var unit_id = $("#edit_unit_id").val();

        var product_code = $("#edit_product_code").val();
        var fixed_price = $("#edit_fixed_price").val();
        var low_inv_alert = $("#edit_low_inv_alert").val();

        //var product_category_name = $('#category_id option:selected').text();
        //var product_unit_name = $('#unit_id option:selected').text();

        if (product_name != "") {

            var params = {
                product_id: product_id,
                category_id: category_id,
                product_name: product_name,
                unit_id: unit_id,
                product_code: product_code,
                fixed_price: fixed_price,
                low_inv_alert: low_inv_alert
            };

            $.ajax({
                url: '/edit/product',
                type: 'POST',
                format: 'JSON',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {

                    //$('#product_name_' + product_id).text(product_name);
                    //$('#category_name_' + product_id).text(product_category_name);
                    //$('#unit_name_' + product_id).text(product_unit_name);

                    swal('Done!', 'Successfully Updated Product Information', 'success');
                    reloadCurrentPage();

                },
                error: function (error) {
                    swal('Error!', 'Something went wrong.', 'error');
                }
            });
        }
    });


    $('#submit_damage_button').click(function () {

        var store_id = $('#store_id').val();

        var params = {
            store_id: store_id,
            damage_products :transfer_product
        };

        $.ajax({
            url: '/damage-entry',
            type: 'POST',
            format: 'JSON',
            data: {params: params, '_token': $('#token').val()},

            success: function (response) {
                swal('Done!', 'Successfully Added To Damage Entry', 'success');

                setTimeout(function(){
                    window.location.href = "/damage-history";
                },1000);
            },
            error: function (error) {
                swal('Error!', 'Something went wrong', 'error');
            }

        });
    });


    $('#payment_supplier_button').click(function () {

        var supplier_id = $("#payment_supplier_id").val();
        var supplier_name = $("#payment_supplier_name").val();
        var debit_from = $("#debit_from").val();
        var amount = $("#amount").val();

        if (amount != "") {

            var params = {
                supplier_id: supplier_id,
                supplier_name: supplier_name,
                debit_from: debit_from,
                amount: amount
            };

            $.ajax({
                url: '/payment/supplier',
                type: 'POST',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {

                    showSuccessNotification('Payment Has Been Completed!');
                },
                error: function (error) {
                    swal('Error!', 'Something went wrong.', 'error');
                }
            });
        }
    });


    $('#add_conversion_button').click(function () {

        var from_unit_id = $('#from_unit_id').val();
        var to_unit_id = $('#to_unit_id').val();
        var conversion_rate = $('#conversion_rate').val();

        if (conversion_rate != '') {

            var params = {
                from_unit_id: from_unit_id,
                to_unit_id: to_unit_id,
                conversion_rate: conversion_rate
            };

            $.ajax({
                url: '/add/unit-conversion',
                type: 'POST',
                format: 'JSON',
                data: {params: params, '_token': $('#token').val()},

                success: function (response) {

                    $('#conversion_rate').val('');

                    var htmlstr = '';

                    $('#unit_conversion_table').append(htmlstr);

                    swal('Done!', 'Successfully Added Unit To Database', 'success');


                },
                error: function (error) {
                    swal('Error!', 'Something went wrong', 'error');
                }

            });
        }
    });


    $('#select_report_type').change(function() {

        var select_report_type = $('#select_report_type').val();

        if (select_report_type == '0') {

            $('#select_category_div').hide();
            $('#select_product_div').hide();

        }

        else if (select_report_type == 'category_wise_report') {

            $('#select_category_div').show();
            $('#select_product_div').hide();

        }

        else if (select_report_type == 'product_wise_report') {

            $('#select_category_div').hide();
            $('#select_product_div').show();

        }

        $('#category_id').select2();
        $('#product_id').select2();




    });


    $('#generate_report_button').click(function () {

        var select_report_type = $('#select_report_type').val();
        var params;


        if (select_report_type == '0') {

            params = {
                select_report_type: select_report_type
            };


        }

        else if (select_report_type == 'category_wise_report') {

            params = {
                select_report_type: select_report_type,
                category_id: $('#category_id').val()
            };


        }

        else if (select_report_type == 'product_wise_report') {

            params = {
                select_report_type: select_report_type,
                product_id: $('#product_id').val()
            };


        }

        $.ajax({
            url: '/generate/stock-report',
            type: 'POST',
            format: 'JSON',
            data: {params:params, '_token': $('#token').val()},

            success: function (response) {

                var json_message = $.parseJSON(response);

                var t_stock_price = 0;
                var htmlstr = '';

                for (var i=0; i<json_message.length; i++) {

                    var product_code = json_message[i].product_code;
                    var product_name = json_message[i].product_name;
                    var category_name = json_message[i].category_name;
                    var quantity = parseFloat(json_message[i].quantity!=null?json_message[i].quantity:0);
                    var unit_name = json_message[i].unit_name;
                    var avg_price = parseFloat(json_message[i].avg_price!=null?json_message[i].avg_price:0);
                    var consumption_quantity = parseFloat(json_message[i].consumption_quantity!=null?json_message[i].consumption_quantity:0);
                    var damage_quantity = parseFloat(json_message[i].damage_quantity!=null?json_message[i].damage_quantity:0);

                    var actual_stock = quantity - damage_quantity;
                    t_stock_price += (actual_stock * avg_price);

                    htmlstr += '<tr>' +
                        '<td>'+product_code+'</td>' +
                        '<td>'+product_name+'</td>' +
                        '<td>'+category_name+'</td>' +
                        '<td>'+ quantity + ' ' + unit_name+'</td>' +
                        '<td>'+ consumption_quantity + ' ' + unit_name +'</td>' +
                        '<td>'+ damage_quantity + ' ' + unit_name +'</td>' +
                        '<td>'+ actual_stock + ' ' + unit_name +'</td>' +
                        '<td>'+ (actual_stock * avg_price) +'&#2547;</td></tr>';
                }

                $('#current_stock_table').empty();
                $('#current_stock_table').append(htmlstr);

            },
            error: function (error) {
                swal('Error!', 'Something went wrong', 'error');
            }
        });


    });


    $('#email_current_stock').click(function () {

        var select_report_type = $('#select_report_type').val();
        var params;


        if (select_report_type == '0') {

            params = {
                select_report_type: select_report_type
            };


        }

        else if (select_report_type == 'category_wise_report') {

            params = {
                select_report_type: select_report_type,
                category_id: $('#category_id').val()
            };


        }

        else if (select_report_type == 'product_wise_report') {

            params = {
                select_report_type: select_report_type,
                product_id: $('#product_id').val()
            };


        }


        $.ajax({
            url: '/email/current-stock',
            type: 'POST',
            format: 'JSON',
            data: {'_token': $('#token').val(), 'params': params},

            success: function (response) {
                showSuccessNotification('Email Has Been Sent!');
            },

            error: function (error) {
                showErrorNotification();
            }
        });



    });


    $('#generate_purchase_history').click(function () {

        var select_report_type = $('#select_report_type').val();
        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();
        var params;


        if (select_report_type == '0') {

            params = {
                select_report_type: select_report_type,
                start_date: start_date,
                end_date: end_date
            };


        }

        else if (select_report_type == 'category_wise_report') {

            params = {
                select_report_type: select_report_type,
                category_id: $('#category_id').val(),
                start_date: start_date,
                end_date: end_date
            };


        }

        else if (select_report_type == 'product_wise_report') {

            params = {
                select_report_type: select_report_type,
                product_id: $('#product_id').val(),
                start_date: start_date,
                end_date: end_date
            };


        }


        $.ajax({
            url: '/generate/purchase-history',
            type: 'POST',
            format: 'JSON',
            data: {'_token': $('#token').val(), 'params': params},

            success: function (response) {

                var json_message = $.parseJSON(response);

                var start_datetime = new Date(start_date);
                var formatted_start = start_datetime.getDate() + "-" + months[start_datetime.getMonth()] + "-" + start_datetime.getFullYear();

                var end_datetime = new Date(end_date);
                var formatted_end = end_datetime.getDate() + "-" + months[end_datetime.getMonth()] + "-" + end_datetime.getFullYear();

                $('#date_header').text(formatted_start + ' To ' + formatted_end);

                var total_quantity = 0;
                var total = 0;
                var htmlstr = '';

                for (var i=0; i<json_message.length; i++) {

                    var invoice_no = json_message[i].invoice_no;
                    var product_name = json_message[i].product_name;
                    var category_name = json_message[i].category_name;

                    var quantity = parseFloat(json_message[i].quantity!=null?json_message[i].quantity:0);
                    var unit_name = json_message[i].unit_name;
                    var unit_price = parseFloat(json_message[i].unit_price!=null?json_message[i].unit_price:0);

                    var purchase_date = json_message[i].purchase_date;

                    var grand_total = (quantity * unit_price);

                    total_quantity += quantity;
                    total += grand_total;


                    htmlstr += '<tr class="even pointer">' +
                        '<td>'+(i+1)+'</td>' +
                        '<td>'+invoice_no+'</td>' +
                        '<td>'+category_name+'</td>' +
                        '<td>'+product_name+'</td>' +
                        '<td>'+quantity + ' ' + unit_name +'</td>' +
                        '<td>'+unit_price+'&#2547;</td>' +
                        '<td>'+grand_total+'&#2547;</td>' +
                        '<td>'+formatDate(purchase_date)+'</td></tr>';
                }

                htmlstr += '<tr class="even pointer">' +
                    '<td></td>' +
                    '<th>Total</th>' +
                    '<td></td>' +
                    '<td></td>' +
                    '<th>'+total_quantity+'</th>' +
                    '<td></td>' +
                    '<th>'+total+'&#2547;</th>' +
                    '<td></td></tr>';

                $('#purchase_history_table').empty();
                $('#purchase_history_table').append(htmlstr);

            },

            error: function (error) {
                showErrorNotification();
            }
        });



    });


    $('#email_purchase_history').click(function () {

        var select_report_type = $('#select_report_type').val();
        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();
        var params;


        if (select_report_type == '0') {

            params = {
                select_report_type: select_report_type,
                start_date: start_date,
                end_date: end_date
            };


        }

        else if (select_report_type == 'category_wise_report') {

            params = {
                select_report_type: select_report_type,
                category_id: $('#category_id').val(),
                start_date: start_date,
                end_date: end_date
            };


        }

        else if (select_report_type == 'product_wise_report') {

            params = {
                select_report_type: select_report_type,
                product_id: $('#product_id').val(),
                start_date: start_date,
                end_date: end_date
            };


        }


        $.ajax({
            url: '/email/purchase-history',
            type: 'POST',
            format: 'JSON',
            data: {'_token': $('#token').val(), 'params': params},

            success: function (response) {
                showSuccessNotification('Email Has Been Sent!');
            },

            error: function (error) {
                showErrorNotification();
            }
        });



    });


    $('#generate_damage_history').click(function () {

        var select_report_type = $('#select_report_type').val();
        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();
        var params;


        if (select_report_type == '0') {

            $('#report_header').text('All Damage Report');

            params = {
                select_report_type: select_report_type,
                start_date: start_date,
                end_date: end_date
            };


        }

        else if (select_report_type == 'category_wise_report') {

            $('#report_header').text('Category Wise Damage Report');

            params = {
                select_report_type: select_report_type,
                category_id: $('#category_id').val(),
                start_date: start_date,
                end_date: end_date
            };


        }

        else if (select_report_type == 'product_wise_report') {

            $('#report_header').text('Product Wise Damage Report');

            params = {
                select_report_type: select_report_type,
                product_id: $('#product_id').val(),
                start_date: start_date,
                end_date: end_date
            };


        }


        $.ajax({
            url: '/generate/damage-history',
            type: 'POST',
            format: 'JSON',
            data: {'_token': $('#token').val(), 'params': params},

            success: function (response) {

                var json_message = $.parseJSON(response);

                var start_datetime = new Date(start_date);
                var formatted_start = start_datetime.getDate() + "-" + months[start_datetime.getMonth()] + "-" + start_datetime.getFullYear();

                var end_datetime = new Date(end_date);
                var formatted_end = end_datetime.getDate() + "-" + months[end_datetime.getMonth()] + "-" + end_datetime.getFullYear();

                $('#date_header').text(formatted_start + ' To ' + formatted_end);

                var total_quantity = 0;
                var total = 0;
                var htmlstr = '';

                for (var i=0; i<json_message.length; i++) {

                    var product_name = json_message[i].product_name;
                    var category_name = json_message[i].category_name;

                    var quantity = parseFloat(json_message[i].quantity!=null?json_message[i].quantity:0);
                    var unit_name = json_message[i].unit_name;
                    var unit_price = parseFloat(json_message[i].unit_price!=null?json_message[i].unit_price:0);

                    var created_at = json_message[i].created_at;

                    var grand_total = (quantity * unit_price);

                    total_quantity += quantity;
                    total += grand_total;

                    htmlstr += '<tr class="pointer">' +
                        '<td>'+(i+1)+'</td>' +
                        '<td>'+category_name+'</td>' +
                        '<td>'+product_name+'</td>' +
                        '<td>'+quantity + ' ' + unit_name +'</td>' +
                        '<td>'+unit_price+'&#2547;</td>' +
                        '<td>'+grand_total+'&#2547;</td>' +
                        '<td>'+formatDate(created_at)+'</td></tr>';
                }

                htmlstr += '<tr class="even pointer">' +
                    '<td></td>' +
                    '<th>Total</th>' +
                    '<td></td>' +
                    '<th>'+total_quantity+'</th>' +
                    '<td></td>' +
                    '<th>'+total+'&#2547;</th>' +
                    '<td></td></tr>';

                $('#damage_history_table').empty();
                $('#damage_history_table').append(htmlstr);

            },

            error: function (error) {
                showErrorNotification();
            }
        });



    });


    $('#email_damage_history').click(function () {

        var select_report_type = $('#select_report_type').val();
        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();
        var params;


        if (select_report_type == '0') {

            params = {
                select_report_type: select_report_type,
                start_date: start_date,
                end_date: end_date
            };


        }

        else if (select_report_type == 'category_wise_report') {

            params = {
                select_report_type: select_report_type,
                category_id: $('#category_id').val(),
                start_date: start_date,
                end_date: end_date
            };


        }

        else if (select_report_type == 'product_wise_report') {

            params = {
                select_report_type: select_report_type,
                product_id: $('#product_id').val(),
                start_date: start_date,
                end_date: end_date
            };


        }


        $.ajax({
            url: '/email/damage-history',
            type: 'POST',
            format: 'JSON',
            data: {'_token': $('#token').val(), 'params': params},

            success: function (response) {
                showSuccessNotification('Email Has Been Sent!');
            },

            error: function (error) {
                showErrorNotification();
            }
        });



    });





});


function print_table(response, payment, select_report_type) {

    var json_message = $.parseJSON(response);

    var total_sales = 0;
    var total_vat = 0;
    var total_service_charge = 0;
    var total_service_charge_VAT = 0;
    var total_sd = 0;
    var total_discount = 0;
    var total = 0;
    var htmlstr = '';
    var htmlstr_header = '';

    if (select_report_type == 'invoice_wise_sale') {

        htmlstr_header = '<th class="column-title text-center">Invoice #</th>' +
            '<th class="column-title text-center">Served By</th>' +
            '<th class="column-title text-center">Item Qty</th>' +
            '<th class="column-title text-center">Item Amount</th>' +
            '<th class="column-title text-center">VAT</th>' +
            '<th class="column-title text-center">Service Charge</th>' +
            '<th class="column-title text-center">Service Charge VAT</th>' +
            '<th class="column-title text-center">SD</th>' +
            '<th class="column-title text-center">Discount</th>' +
            '<th class="column-title text-center">Discount Ref.</th>' +
            '<th class="column-title text-center">Net Amount</th>' +
            '<th class="column-title text-center">Payment Type</th>';

    }


    else if (select_report_type == 'item_wise_sale') {

        htmlstr_header = '<th class="column-title text-center">Invoice #</th>' +
            '<th class="column-title text-center">Served By</th>' +
            '<th class="column-title text-center">Item Name</th>' +
            '<th class="column-title text-center">Item Qty</th>';

    }


    else if (select_report_type == 'category_wise_sale') {

        htmlstr_header = '<th class="column-title text-center">Category Name</th>' +
            '<th class="column-title text-center">Item Qty</th>' +
            '<th class="column-title text-center">Total Price</th>';

    }


    else if (select_report_type == 'waiter_wise_sale') {

        htmlstr_header = '<th class="column-title text-center">Invoice #</th>' +
            '<th class="column-title text-center">Waiter Name</th>' +
            '<th class="column-title text-center">Total Amount</th>';

    }


    $('#report_table_header').empty();
    $( "#report_table_header" ).append(htmlstr_header);

    if (select_report_type == 'category_wise_sale') {

        var menu_array = [];

        json_message = JSON.parse(response);

        var menus = json_message.menus;
        var reports = json_message.reports;
        var menu_items = json_message.menu_items;

        for (var i=0; i<menus.length; i++) {

            var menu_id = menus[i].menu_id;
            var menu_name = menus[i].menu_name;
            var count = 0;

            menu_array.push([menu_id, menu_name, count]);

        }

        for (var i=0; i<reports.length; i++) {

            var menu_id = reports[i].menu_id;
            var item_quantity = reports[i].item_quantity;

            for (var j=0; j<menu_array.length; j++) {

                if (menu_array[j][0] == menu_id) menu_array[j][2] += parseInt(item_quantity);

            }


        }

        var total_item_quantity = 0;
        var total_price = 0;

        for (var j=0; j<menu_array.length; j++) {

            var menu_id = menu_array[j][0];
            var menu_name = menu_array[j][1];
            var count = menu_array[j][2];

            if (count > 0 && $('#menu_id').val() == 0) {

                total_item_quantity += count;

                htmlstr += '<tr class="even pointer">' +
                    '<td class="text-center">'+ menu_name +'</td>' +
                    '<td class="text-center">'+ count +'</td>' +
                    '<td class="text-center"></td>' +
                    '</tr>';

            }

            else if (count > 0) {

                htmlstr += '<tr class="even pointer">' +
                    '<td class="text-center">'+ menu_name +'</td>' +
                    '<td class="text-center"></td>' +
                    '<td class="text-center"></td>' +
                    '</tr>';


            }
        }




        for (var i=0; i<menu_items.length; i++) {

            var item_name = menu_items[i].item_name;
            var price = parseFloat(menu_items[i].price);
            var item_quantity = menu_items[i].item_quantity==null?0:parseInt(menu_items[i].item_quantity);

            total_item_quantity += item_quantity;
            total_price += (item_quantity * price);

            if (item_quantity > 0 && $('#menu_id').val() != 0) {

                htmlstr += '<tr class="even pointer">' +
                    '<td class="text-center">'+ item_name +'</td>' +
                    '<td class="text-center">'+ item_quantity +'</td>' +
                    '<td class="text-center">'+ (item_quantity * price) +'</td>' +
                    '</tr>';

            }
        }

        htmlstr += '<tr class="even pointer">' +
            '<td class="text-center"><b>Total</b></td>' +
            '<td class="text-center"><b>'+ total_item_quantity +'</b></td>' +
            '<td class="text-center"><b>'+ total_price +'</b></td>' +
            '</tr>';




    }

    else {

        for (var i=0; i<json_message.length; i++) {

            var order_id = json_message[i].order_id;
            var waiter_name = json_message[i].waiter_name;
            var time = json_message[i].updated_at;
            var customer_name = json_message[i].customer_name;
            var table_name = json_message[i].table_name;
            var bill = parseFloat(json_message[i].bill);
            var vat = parseFloat(json_message[i].vat);
            var total_item = parseInt(json_message[i].total_item);
            var service_charge = parseFloat(json_message[i].service_charge);
            var service_charge_vat = parseFloat(json_message[i].service_charge_vat);
            var sd_charge = parseFloat(json_message[i].sd_charge);
            var total_bill = json_message[i].total_bill;
            var payment_method = json_message[i].payment_method;
            var given_amount = json_message[i].given_amount==null ? '': json_message[i].given_amount;

            // var invoice_no = replaceAll(time.split(" ")[0], "-", "") + order_id.pad(5 - order_id.toString().length);
            //var invoice_no = replaceAll(time.split(" ")[0], "-", "") + order_id;
            var invoice_no = formatInvoiceDate(time) + numberPadding(order_id);

            var t_total_bill = (json_message[i].bill + json_message[i].service_charge + json_message[i].vat - json_message[i].discount);
            var discount = json_message[i].discount == null ? 0:parseFloat(json_message[i].discount);
            var discount_reference = json_message[i].discount_reference==null? '':json_message[i].discount_reference;





            if (select_report_type == 'invoice_wise_sale') {

                htmlstr += '<tr class="even pointer" onclick="generateReceipt('+order_id+')">' +
                    '<td class="text-center">'+ invoice_no +'</td>' +
                    '<td class="text-center">'+ waiter_name +'</td>' +
                    '<td class="text-center">'+ total_item +'</td>' +
                    '<td class="text-center">'+ bill +'</td>' +
                    '<td class="text-center">'+ vat +'</td>' +
                    '<td class="text-center">'+ service_charge +'</td>' +
                    '<td class="text-center">'+ service_charge_vat +'</td>' +
                    '<td class="text-center">'+ sd_charge +'</td>' +
                    '<td class="text-center">'+ discount +'</td>' +
                    '<td class="text-center">'+ discount_reference +'</td>' +
                    '<td class="text-center">'+ json_message[i].total_bill +'</td>' +
                    '<td class="text-center">'+ json_message[i].payment_method.toUpperCase() +'</td></tr>';

                total_sales += bill;
                total_vat += vat;
                total_service_charge += service_charge;
                total_service_charge_VAT += service_charge_vat;
                total_sd += sd_charge;
                total_discount += discount;
                total += parseFloat(json_message[i].total_bill);

            }


            else if (select_report_type == 'item_wise_sale') {

                var item_name = json_message[i].item_name;
                var ratio = json_message[i].ratio;
                var item_quantity = json_message[i].item_quantity;
                var item_price = json_message[i].item_price;
                var item_discount = json_message[i].item_discount;
                var item_vat = json_message[i].item_vat;
                var total_item_price = parseFloat(item_price) * parseFloat(item_quantity);

                htmlstr += '<tr class="even pointer">' +
                    '<td class="text-center">'+ invoice_no +'</td>' +
                    '<td class="text-center">'+ waiter_name +'</td>' +
                    '<td class="text-center">'+ item_name + ' ' + ratio +'</td>' +
                    '<td class="text-center">'+ item_quantity +'</td>' +
                    '</tr>';

                total_sales += total_item_price;
                total_vat += calculatePercentage(total_item_price, item_vat);
                total_service_charge += service_charge;
                total_discount += parseFloat(item_discount) * parseFloat(item_quantity);

            }


            else if (select_report_type == 'waiter_wise_sale') {

                htmlstr += '<tr class="even pointer">' +
                    '<td class="text-center">'+ invoice_no +'</td>' +
                    '<td class="text-center">'+ waiter_name +'</td>' +
                    '<td class="text-center">'+ total_bill +'</td>' +
                    '</tr>';

                total_sales += bill;
                total_vat += vat;
                total_service_charge += service_charge;
                total_discount += discount;

            }

        }

        if (select_report_type == 'invoice_wise_sale') {

            htmlstr += '<tr class="even pointer">' +
                '<td class="text-center"><b>Total</b></td>' +
                '<td></td>' +
                '<td></td>' +
                '<td class="text-center"><b>' + total_sales +'</b></td>' +
                '<td class="text-center"><b>' + total_vat +'</b></td>' +
                '<td class="text-center"><b>' + total_service_charge +'</b></td>' +
                '<td class="text-center"><b>' + total_service_charge_VAT +'</b></td>' +
                '<td class="text-center"><b>' + total_sd +'</b></td>' +
                '<td class="text-center"><b>' + total_discount +'</b></td>' +
                '<td></td>' +
                '<td class="text-center"><b>'+ total +'</b></td>' +
                '<td></td></tr>';



        }


    }




    $('#report_table_tbody').empty();
    $('#report_table_tbody').append(htmlstr);

    //$('#report_table').DataTable();

    $( "#total_sales" ).text(total_sales + '৳');
    $( "#total_vat" ).text(total_vat + '৳');
    $( "#total_service_charge" ).text(total_service_charge + '৳');
    $( "#total_discount" ).text(total_discount + '৳');



}


function transfer_table_print() {

    var htmlstr = '';

    for (var i = 0; i < transfer_product.length; i++) {

        var product_category_name = transfer_product[i][0];
        var product_id = transfer_product[i][1];
        var product_name = transfer_product[i][2];
        var quantity = transfer_product[i][3];

        if (quantity > -1) {

            htmlstr += '<tr id="transfer_row_'+ i +'"><td>'+ (i+1) +'</td>' +
                '<td>'+ product_category_name +'</td>' +
                '<td>'+ product_name +'</td>' +
                '<td>'+ quantity +'</td>' +
                '<td><button type="button" class="btn btn-danger" onclick="removeTransferProduct('+i+')">' +
                '<i class="glyphicon glyphicon-remove"></i></button></td></tr>';
        }
    }


    $('#transfer_table_tbody').empty();
    $('#transfer_table_tbody').append(htmlstr);

}


function removeTransferProduct(product_no) {

    var product_id = transfer_product[product_no][1];
    var quantity = parseInt(transfer_product[product_no][3]) * (-1);

    transfer_product[product_no][3] = 0;

    updateProductQuantity(product_id, quantity);
    transfer_table_print();

}


function order_table_print() {

    var sub_total = 0;
    var grand_total = 0;

    var discount = $('#discount_input').val();
    var shipping = $('#shipping_input').val();

    var html = '';

    for (var i = 0; i < invoice_orders.length; i++) {

        var product_category_id = invoice_orders[i][0];
        var product_category_name = invoice_orders[i][1];
        var product_id = invoice_orders[i][2];
        var product_name = invoice_orders[i][3];
        var unit_price = parseFloat(invoice_orders[i][4]);
        var quantity = parseFloat(invoice_orders[i][5]);
        var total = parseFloat(invoice_orders[i][6]);

        if (quantity > 0.0) {

            html += '<tr id="inv_order_row_'+ i +'"><td>'+ (i+1) +'</td>' +
                '<td>'+ product_category_name +'</td>' +
                '<td>'+ product_name +'</td>' +
                '<td>'+ unit_price +'</td>' +
                '<td>'+ quantity +'</td>' +
                '<td>'+ total +'</td>' +
                '<td><button type="button" class="btn btn-danger" onclick="removeCartProduct('+i+')">' +
                '<i class="glyphicon glyphicon-remove"></i></button></td></tr>';

            sub_total += total;
        }


    }

    grand_total = sub_total - parseFloat(discount) + parseFloat(shipping);


    $('#invoice_table_tbody').empty();
    $('#invoice_table_tbody').append(html);

    $('#subtotal_input').val(sub_total);
    $('#grand_total_input').val(grand_total);


}


function removeCartProduct(product_no) {

    invoice_orders[product_no][5] = 0;
    order_table_print();

}


function pushToProductArray(product_id, product_name, status, unit_id, category_id, price) {
    var count=0;
    products.push([product_id, product_name, status, unit_id, category_id, count, price]);
}


function getProductPrice(product_id) {

    var price = 0;

    for (var i = 0; i < products.length; i++) {

        var id = products[i][0];
        price = products[i][6];

        if (product_id == id) return price;
    }

}


function pushToUnitArray(unit_id, unit_name) {
    units.push([unit_id, unit_name]);
}


function viewPurchaseDetails(invoice_id, invoice_no, supplier_name, purchase_date, delivery_date, sub_total, discount, shipping, grand_total) {

    $('#modal_invoice_no').text(invoice_no);
    $('#modal_supplier_name').text(supplier_name);
    $('#modal_purchase_date').text(purchase_date);
    $('#modal_delivery_date').text(delivery_date);
    $('#modal_sub_total').text(sub_total);
    $('#modal_discount').text(discount);
    $('#modal_shipping').text(shipping);
    $('#modal_grand_total').text(grand_total);

    var params = {
        invoice_id: invoice_id
    };

    $.ajax({
        url: '/get/purchase-details',
        type: 'POST',
        format: 'JSON',
        data: {params: params, '_token': $('#token').val()},

        success: function (response) {

            var json_message = $.parseJSON(response);
            var htmlstr = '';

            for (var i=0; i<json_message.length; i++) {

                htmlstr += '<tr>' +
                    '<th scope="row">'+ (i+1) +'</th>' +
                    '<td>'+ json_message[i].product_name +'</td>' +
                    '<td>'+ json_message[i].unit_price +'</td>' +
                    '<td>'+ json_message[i].quantity +'</td>' +
                    '<td>'+ json_message[i].total_purchase_price +'</td>' +
                    '</tr>';
            }

            $( "#purchase_details_table" ).empty();
            $( "#purchase_details_table" ).append(htmlstr);

        },
        error: function (error) {

        }
    });

    $('#purchase_details_modal').modal('show');
}


function edit_supplier(supplier_id) {

    var company_name = $('#company_name_' + supplier_id).text();
    var supplier_name = $('#supplier_name_' + supplier_id).text();
    var address = $('#address_' + supplier_id).text();
    var email = $('#email_' + supplier_id).text();
    var phone = $('#phone_' + supplier_id).text();

    $("#edit_supplier_id").val(supplier_id);
    $("#edit_company_name").val(company_name);
    $("#edit_supplier_name").val(supplier_name);
    $("#edit_address").val(address);
    $("#edit_email").val(email);
    $("#edit_phone").val(phone);

    $('#edit_supplier_modal').modal('show');
}


function payment_supplier(supplier_id) {

    var supplier_name = $('#supplier_name_' + supplier_id).text();

    $("#payment_supplier_id").val(supplier_id);
    $("#payment_supplier_name").val(supplier_name);

    $('#payment_supplier_modal').modal('show');
}


function delete_supplier(supplier_id) {

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-danger',
        cancelButtonClass: 'btn btn-success',
        buttonsStyling: false
    }).then(function () {

        var params = {
            supplier_id: supplier_id
        };

        $.ajax({
            url: '/delete/supplier',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {

                $("#supplier_" + supplier_id).remove();

                swal('Done', 'Supplier Has Been Deleted', 'success');
            },
            error: function (error) {
                console.log("Error! Something went wrong.");
            }
        });

    });
}


function editCategory(category_id) {


    var category_name = $('#category_name_' + category_id).text();

    $("#edit_category_id").val(category_id);
    $("#edit_category_name").val(category_name);


    $("#edit_category_modal").modal('show');
}


function editUnit(unit_id) {

    var unit_name = $('#unit_name_' + unit_id).text();

    $("#edit_unit_id").val(unit_id);
    $("#edit_unit_name").val(unit_name);


    $("#edit_unit_modal").modal('show');
}


function deleteUnit(unit_id) {

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-danger',
        cancelButtonClass: 'btn btn-success',
        buttonsStyling: false
    }).then(function () {

        var params = {
            unit_id: unit_id
        };

        $.ajax({
            url: '/delete/unit',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {

                $("#unit_" + unit_id).remove();

                swal('Done', 'Unit Has Been Deleted', 'success');
            },
            error: function (error) {
                console.log("Error! Something went wrong.");
            }
        });

    });
}


function getProductQuantity(product_id) {

    var quantity = 0;

    for (var i = 0; i < products.length; i++) {
        var id = products[i][0];
        quantity = products[i][2];

        if (product_id == id) return quantity;
    }

    return quantity;


}


function updateProductQuantity(product_id, cart_q) {

    for (var i = 0; i < products.length; i++) {
        var id = products[i][0];

        if (product_id == id) {
            products[i][2] -= cart_q;
            return;
        }
    }
}


function setMainStoreId(store_id) {
    $('#main_store_id').val(store_id);
}


function transferDetails(transfer_id, m_from, m_to, m_transfer_date, m_created_by) {

    $('#m_transfer_date').text(m_transfer_date);
    $('#m_from').text(m_from);
    $('#m_to').text(m_to);
    $('#m_created_by').text(m_created_by);


    var params = {
        transfer_id: transfer_id
    };

    $.ajax({
        url: '/get/transferDetails',
        type: 'POST',
        format: 'JSON',
        data: {params: params, '_token': $('#token').val()},

        success: function (response) {
            var json_message = $.parseJSON(response);
            var htmlstr = '';

            for (var i=0; i<json_message.length; i++) {

                htmlstr += '<tr><th scope="row">'+ (i+1) +'</th>' +
                    '<td>'+ json_message[i].product_code +'</td>' +
                    '<td>'+ json_message[i].product_name +'</td>' +
                    '<td>'+ json_message[i].quantity + ' ' + json_message[i].unit_name +'</td>' +
                    '</tr>';
            }

            $('#modal_transfer_table').empty();
            $('#modal_transfer_table').append(htmlstr);

        },
        error: function (error) {
            swal('Error!', 'Something went wrong', 'error');
        }
    });

    $('#transfer_details_modal').modal('show');
}


function setStockValue(value) {

    $('#t_stock_price').text(value);

}


function edit_store(store_id) {

    var store_name = $('#store_name_' + store_id).text();
    var store_address = $('#store_address_' + store_id).text();
    var store_phone = $('#store_phone_' + store_id).text();
    var store_email = $('#store_email_' + store_id).text();

    $("#edit_store_id").val(store_id);
    $("#edit_store_name").val(store_name);
    $("#edit_store_address").val(store_address);
    $("#edit_store_phone").val(store_phone);
    $("#edit_store_email").val(store_email);


    $('#edit_store_modal').modal('show');
}


function delete_store(store_id) {

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-danger',
        cancelButtonClass: 'btn btn-success',
        buttonsStyling: false
    }).then(function () {

        var params = {
            store_id: store_id
        };

        $.ajax({
            url: '/delete/store',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {

                $("#store_" + store_id).remove();

                swal('Done', 'Store Has Been Deleted', 'success');
            },
            error: function (error) {
                console.log("Error! Something went wrong.");
            }
        });

    });
}


function deleteCategory(category_id) {

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-danger',
        cancelButtonClass: 'btn btn-success',
        buttonsStyling: false
    }).then(function () {

        var params = {
            category_id: category_id
        };

        $.ajax({
            url: '/delete/category',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {

                $("#category_" + category_id).remove();

                swal('Done', 'Category Has Been Deleted', 'success');
            },
            error: function (error) {
                console.log("Error! Something went wrong.");
            }
        });

    });
}


function editProduct(product_id, product_code, product_name, unit_id, category_id, fixed_price, low_inv_alert) {

    $("#edit_product_id").val(product_id);
    $("#edit_category_id").val(category_id);
    $("#edit_product_name").val(product_name);
    $("#edit_unit_id").val(unit_id);
    $("#edit_product_code").val(product_code);
    $("#edit_fixed_price").val(fixed_price);
    $("#edit_low_inv_alert").val(low_inv_alert);

    $('#edit_product_modal').modal('show');
}


function deleteProduct(product_id) {

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-danger',
        cancelButtonClass: 'btn btn-success',
        buttonsStyling: false
    }).then(function () {

        var params = {
            product_id: product_id
        };

        $.ajax({
            url: '/delete/product',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {

                $("#product_" + product_id).remove();

                swal('Done', 'Product Has Been Deleted', 'success');
            },
            error: function (error) {
                console.log("Error! Something went wrong.");
            }
        });

    });
}


function damageDetails(damage_id, date, store, username) {

    $('#m_damage_date').text(date);
    $('#m_from').text(store);
    $('#m_created_by').text(username);


    var params = {
        damage_id: damage_id
    };

    $.ajax({
        url: '/get/damageDetails',
        type: 'POST',
        format: 'JSON',
        data: {params: params, '_token': $('#token').val()},

        success: function (response) {
            var json_message = $.parseJSON(response);
            var htmlstr = '';

            for (var i=0; i<json_message.length; i++) {

                var unit_price = json_message[i].unit_price;
                var quantity = json_message[i].quantity;

                var total_cost = (parseFloat(unit_price) * parseFloat(quantity));

                htmlstr += '<tr><th scope="row">'+ (i+1) +'</th>' +
                    '<td>'+ json_message[i].product_name +'</td>' +
                    '<td>'+ quantity + ' ' + json_message[i].unit_name +'</td>' +
                    '<td>'+ unit_price +'</td>' +
                    '<td>'+ total_cost +'</td>' +
                    '</tr>';
            }

            $('#modal_damage_table').empty();
            $('#modal_damage_table').append(htmlstr);

            $('#damage_details_modal').modal('show');

        },
        error: function (error) {
            swal('Error!', 'Something went wrong', 'error');
        }
    });


}


function inventoryPurchase() {

    var invoice_no = $('#invoice_no').val();
    var supplier_id = $('#supplier').val();
    var purchase_date = $('#purchase_date').val();
    var delivery_date = $('#delivery_date').val();
    var pay_type = $('#pay_type').val();

    var params = {
        invoice_no :invoice_no,
        supplier_id :supplier_id,
        purchase_date :purchase_date,
        delivery_date :delivery_date,
        pay_type :pay_type,
        sub_total :$('#subtotal_input').val(),
        discount :$('#discount_input').val(),
        shipping :$('#shipping_input').val(),
        grand_total :$('#grand_total_input').val(),
        invoice_orders :invoice_orders
    };


    if (invoice_no == '' || supplier_id == '0') swal('Warning!', 'Please Provide Supplier Information', 'warning');
    else if (invoice_orders.length == 0) swal('Warning!', 'Please Add Product To Cart', 'warning');
    else {
        $.ajax({
            url: '/new-purchase',
            type: 'POST',
            format: 'JSON',
            data: {params: params, '_token': $('#token').val()},

            success: function (response) {
                swal('Done!', 'Order Placed Successfully', 'success');

                setTimeout(function(){
                    window.location.href = "/purchase-history";
                },1000);
            },
            error: function (error) {
                swal('Error!', 'Something went wrong', 'error');
            }

        });
    }

}


function payType(payment_type) {

    $('#pay_type').val(payment_type);

    inventoryPurchase();

}









