
$(document).ready(function() {


    $("#save_cash_opening").click(function(){

        var note_1000 = $('#note_1000').val();
        var note_500 = $('#note_500').val();
        var note_100 = $('#note_100').val();
        var note_50 = $('#note_50').val();
        var note_20 = $('#note_20').val();
        var note_10 = $('#note_10').val();
        var note_5 = $('#note_5').val();
        var note_2 = $('#note_2').val();
        var note_1 = $('#note_1').val();

        var params = {
            note_1000: note_1000,
            note_500: note_500,
            note_100: note_100,
            note_50: note_50,
            note_20: note_20,
            note_10: note_10,
            note_5: note_5,
            note_2: note_2,
            note_1: note_1
        };

        $.ajax({
            url: '/add/cash-opening',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {

                showSuccessNotification('Added To Cash Opening');

                reloadCurrentPage();


            },
            error: function (error) {
                console.log("Error! Something went wrong.");
            }
        });



    });


    $( "#note_1000" ).keyup(function() {

        var note = $('#note_1000').val();

        if (note == '') {
            note = 0;
            //$('#note_1000').val(note);
        }

        var amount = parseInt(note) * 1000;
        $('#note_1000_total').text(amount);
        calculateTotalCash();


    });


    $( "#note_500" ).keyup(function() {

        var note = $('#note_500').val();

        if (note == '') {
            note = 0;
            //$('#note_1000').val(note);
        }

        var amount = parseInt(note) * 500;
        $('#note_500_total').text(amount);
        calculateTotalCash();


    });


    $( "#note_100" ).keyup(function() {

        var note = $('#note_100').val();

        if (note == '') {
            note = 0;
            //$('#note_1000').val(note);
        }

        var amount = parseInt(note) * 100;
        $('#note_100_total').text(amount);
        calculateTotalCash();


    });


    $( "#note_50" ).keyup(function() {

        var note = $('#note_50').val();

        if (note == '') {
            note = 0;
            //$('#note_1000').val(note);
        }

        var amount = parseInt(note) * 50;
        $('#note_50_total').text(amount);
        calculateTotalCash();


    });


    $( "#note_20" ).keyup(function() {

        var note = $('#note_20').val();

        if (note == '') {
            note = 0;
            //$('#note_1000').val(note);
        }

        var amount = parseInt(note) * 20;
        $('#note_20_total').text(amount);
        calculateTotalCash();


    });


    $( "#note_10" ).keyup(function() {

        var note = $('#note_10').val();

        if (note == '') {
            note = 0;
            //$('#note_1000').val(note);
        }

        var amount = parseInt(note) * 10;
        $('#note_10_total').text(amount);
        calculateTotalCash();


    });


    $( "#note_5" ).keyup(function() {

        var note = $('#note_5').val();

        if (note == '') {
            note = 0;
            //$('#note_1000').val(note);
        }

        var amount = parseInt(note) * 5;
        $('#note_5_total').text(amount);
        calculateTotalCash();


    });


    $( "#note_2" ).keyup(function() {

        var note = $('#note_2').val();

        if (note == '') {
            note = 0;
            //$('#note_1000').val(note);
        }

        var amount = parseInt(note) * 2;
        $('#note_2_total').text(amount);
        calculateTotalCash();


    });


    $( "#note_1" ).keyup(function() {

        var note = $('#note_1').val();

        if (note == '') {
            note = 0;
            //$('#note_1000').val(note);
        }

        var amount = parseInt(note) * 1;
        $('#note_1_total').text(amount);
        calculateTotalCash();


    });


    $("#add_expense_button").click(function(){

        var purpose = $('#purpose').val();
        var debit_from = $('#debit_from').val();
        var amount = $('#amount').val();

        var cash_sale_amount = parseFloat($('#cash_sale_amount').text());
        var petty_cash_amount = parseFloat($('#petty_cash_amount').text());
        var loan_amount = parseFloat($('#loan_amount').text());


        var params = {
            purpose: purpose,
            debit_from: debit_from,
            amount: amount
        };

        $.ajax({
            url: '/add/daily-expense',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {
                showSuccessNotification('Added To Expense');
                reloadCurrentPage();
            },
            error: function (error) {
                showErrorNotification();
            }
        });



    });


    $("#save_cash_closing").click(function(){

        var note_1000 = $('#note_1000').val();
        var note_500 = $('#note_500').val();
        var note_100 = $('#note_100').val();
        var note_50 = $('#note_50').val();
        var note_20 = $('#note_20').val();
        var note_10 = $('#note_10').val();
        var note_5 = $('#note_5').val();
        var note_2 = $('#note_2').val();
        var note_1 = $('#note_1').val();

        var params = {
            note_1000: note_1000,
            note_500: note_500,
            note_100: note_100,
            note_50: note_50,
            note_20: note_20,
            note_10: note_10,
            note_5: note_5,
            note_2: note_2,
            note_1: note_1
        };

        $.ajax({
            url: '/add/cash-closing',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {

                showSuccessNotification('Added To Cash Closing');

                reloadCurrentPage();


            },
            error: function (error) {
                console.log("Error! Something went wrong.");
            }
        });



    });


    $("#add_transfer_button").click(function(){


        var transfer_to = $('#transfer_to').val();
        var amount = $('#amount').val();

        var type = 'transfer';
        var debit_from = transfer_to=='sales'?'petty_cash':'sales';

        var params = {
            amount: amount,
            type: type,
            debit_from: debit_from,
            transfer_to: transfer_to
        };

        $.ajax({
            url: '/add/cash-transfer',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {

                showSuccessNotification('Added To Expense');

                reloadCurrentPage();



            },
            error: function (error) {
                console.log("Error! Something went wrong.");
            }
        });



    });


    $("#add_owner_button").click(function () {

        var name = $('#name').val();
        var address = $('#address').val();
        var email = $('#email').val();
        var phone = $('#phone').val();
        var ac = $('#ac').val();

        if (name == '') {
            swal('', 'Owner Name Missing', 'warning');
        }

        else {
            var params = {
                name: name,
                address: address,
                email: email,
                phone: phone,
                ac: ac
            };

            $.ajax({
                url: '/add/owner',
                type: 'POST',
                format: 'JSON',
                data: {params: params, '_token': $('#token').val()},
                success: function (response) {

                    showSuccessNotification('Successfully Added Owner To Database');

                    reloadCurrentPage();

                },
                error: function (error) {
                    swal("Error!", "Something went wrong", "error");
                }
            });
        }

    });


    $("#add_loan_button").click(function(){

        var owner_id = $('#owner_id').val();
        var amount = $('#amount').val();
        var transaction_type = 'credit';

        var params = {
            owner_id: owner_id,
            transaction_type: transaction_type,
            amount: amount
        };

        $.ajax({
            url: '/add/loan',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {

                showSuccessNotification('Added To Loan');

                reloadCurrentPage();



            },
            error: function (error) {
                console.log("Error! Something went wrong.");
            }
        });



    });


    $("#add_loan_debit_button").click(function(){

        var owner_id = $('#owner_id').val();
        var owner_name = $('#owner_id option:selected').text();
        var amount = $('#amount').val();
        var debit_from = $('#debit_from').val();
        var transaction_type = 'debit';

        var params = {
            owner_id: owner_id,
            owner_name: owner_name,
            transaction_type: transaction_type,
            amount: amount,
            debit_from: debit_from
        };

        $.ajax({
            url: '/add/loan-debit',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {

                showSuccessNotification('Added To Loan Debit');

                reloadCurrentPage();



            },
            error: function (error) {
                console.log("Error! Something went wrong.");
            }
        });



    });


    $('#edit_expense_button').click(function () {

        var expense_id = $('#expense_id').val();
        var purpose = $('#edit_purpose').val();
        var debit_from = $('#edit_debit_from').val();
        var amount = $('#edit_amount').val();

        var params = {
            expense_id: expense_id,
            purpose: purpose,
            debit_from: debit_from,
            amount: amount
        };

        $.ajax({
            url: '/edit/daily-expense',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {

                showSuccessNotification('Expense information has been edited');

                reloadCurrentPage();

            },
            error: function (error) {
                showErrorNotification();
            }
        });



    });


});


function calculateTotalCash() {

    var note_1000 = $('#note_1000_total').text();
    var note_500 = $('#note_500_total').text();
    var note_100 = $('#note_100_total').text();
    var note_50 = $('#note_50_total').text();
    var note_20 = $('#note_20_total').text();
    var note_10 = $('#note_10_total').text();
    var note_5 = $('#note_5_total').text();
    var note_2 = $('#note_2_total').text();
    var note_1 = $('#note_1_total').text();

    var total = parseInt(note_1000) + parseInt(note_500) + parseInt(note_100) + parseInt(note_50) + parseInt(note_20) + parseInt(note_10) + parseInt(note_5) + parseInt(note_2) + parseInt(note_1);

    $('#note_total').text(total);



}


function editExpense(expense_id, purpose, amount, debit_from) {

    $("#expense_id").val(expense_id);
    $("#edit_purpose").val(purpose);
    $("#edit_amount").val(amount);
    $("#edit_debit_from").val(debit_from);

    $('#edit_expense_modal').modal('show');
}


function deleteExpense(expense_id) {

    swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-danger',
        cancelButtonClass: 'btn btn-success',
        buttonsStyling: false
    }).then(function () {

        var params = {
            expense_id: expense_id
        };

        $.ajax({
            url: '/delete/daily-expense',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {

                $("#expense_" + expense_id).remove();

                showSuccessNotification('Expense Has Been Deleted');

                reloadCurrentPage();
            },
            error: function (error) {
                //console.log("Error! Something went wrong.");
                showErrorNotification();
            }
        });

    });
}