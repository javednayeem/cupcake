var menu_items = [];
var setmenu_items = [];
var modifiers = [];
var order_modifiers = [];
var kitchen_token = [];
var instructions = [];
var payment_methods = [];

var vat_no = "";
var vat_percentage = 0.0;
var service_charge = 0.0;
var discount = 0.0;
var bill_with_discount = 0.0;
var temp_bg_color_previous_color = -1;
var guest_bill=0;
var kitchen_type='single';
var instruction_count = 1;

var seconds = 0, minutes = 0, hours = 0, t, time;

var price_including_vat, vat_after_discount, sd_percentage, service_charge_vat_percentage, sd_vat_percentage = 0;

var background_color = [
    "#26A65B",
    "#F89406",
    "#913D88",
    "#FF5A5E",
    "#46BFBD",
    "#34495e",
    "#947CB0"
];

$(document).ready(function() {

    var order_id = $('#order_id').val();

    generateReceipt(order_id);


    $('#send_kot').click(function () {

        var served_by = $('#waiter_id').val();

        if (served_by != "0") {

            $('#send_kot').prop('disabled', true);

            var order_id = $('#order_id').val();
            var order_type = $('#order_type').val();
            var table = $('#table_id').val();

            var customer_id = $('#customer_id').val();
            var order_notes = $('#order_notes').val();

            var discount = $('#discount_amount').val()==''?0:$('#discount_amount').val();
            var discount_percentage = $('#discount_amount_percentage').val()==''?0:$('#discount_amount_percentage').val();
            var discount_amount_type = $('#discount_amount_type').val();
            var discount_reference = $('#discount_reference').val();

            var order_items = [];

            for (var i = 0; i < menu_items.length; i++) {

                var item = menu_items[i];
                var count = item.count;

                if (count > 0) order_items.push(item);
            }

            var params = {
                order_id: order_id,
                order_type: order_type,
                table_id: table,
                waiter_id: served_by,
                customer_id: customer_id,
                discount: discount,
                discount_percentage: discount_percentage,
                discount_amount_type: discount_amount_type,
                discount_reference: discount_reference,
                order_notes: order_notes,
                order_items: order_items,
                order_modifiers: order_modifiers
            };

            $.ajax({
                url: '/place/table-order',
                type: 'POST',
                format: 'JSON',
                data: {params: params, "_token": $('#token').val()},

                success: function (response) {

                    $('#order_id').val(response);
                    printTokenReceipt();
                    //reloadCurrentPage();
                },
                error: function (error) {
                    showErrorNotification();
                }
            });



        }

    });

});


function pushToSetMenuItemArray(item_id, item_name, menu_item_id) {

    var setmenu = {
        item_id: item_id,
        item_name: item_name,
        menu_item_id: menu_item_id,
    };

    setmenu_items.push(setmenu);
}


function pushToItemArray(id, menu_id, item_name, ratio, price, discount, item_vat, set_menu) {

    var count=0;

    var item = {
        id: id,
        menu_id: menu_id,
        item_name: item_name,
        ratio: ratio,
        price: price,
        discount: discount,
        item_vat: item_vat,
        set_menu: set_menu,
        count: count,
    };

    menu_items.push(item);
}


function pushToModifiersArray(modifier_id, modifier_name, price, modifier_vat) {

    var modifier = {
        modifier_id: modifier_id,
        modifier_name: modifier_name,
        price: price,
        modifier_vat: modifier_vat,
    };

    modifiers.push(modifier);
}


function pushToKitchenToken(printer_id) {
    var htmlstr_token = '';
    kitchen_token.push([printer_id, htmlstr_token]);
}


function pushChargesInfo(vat_number, vat, service, guest_bill_value, piv, vad, sd, scvp, sdvp, kitchenType) {

    vat_no = vat_number;
    vat_percentage = isNaN(vat) ? 0.0 : vat;
    service_charge = isNaN(service) ? 0.0 : service;
    guest_bill = guest_bill_value;
    kitchen_type = kitchenType;

    this.price_including_vat = isNaN(piv)?0:piv;
    this.vat_after_discount = isNaN(vad)?0:vad;
    this.sd_percentage = isNaN(sd)?0.0:sd;
    this.service_charge_vat_percentage = isNaN(scvp)?0.0:scvp;
    this.sd_vat_percentage = isNaN(sdvp)?0.0:sdvp;


    /*
     console.log("vat_no : " + vat_no);
     console.log("vat_percentage : " + vat_percentage);
     console.log("service_charge : " + service_charge);
     */
}


function loadItems(menu_id, menu_name) {

    var item_count = 1;
    $('#menu_item_header').text(menu_name);

    var htmlstr = '';

    for (var i = 0; i < menu_items.length; i++) {

        var item = menu_items[i];

        var item_id = item.id;
        var this_menu_id = item.menu_id;
        var item_name = item.item_name;
        var ratio = item.ratio;
        var price = item.price;
        /*
        var discount = item.discount;
        var item_vat = item.item_vat;
        var set_menu = item.set_menu;
        var count = item.count;
        */
        if (this_menu_id == menu_id) {

            htmlstr += '<div class="col-md-4 pointer" onclick="addToCart('+item_id+')">' +
                '<div class="panel panel-default text-center text-white slider-bg m-b-0" style="height: 200px; background-color: '+background_color[getRandomNumber(0, background_color.length-1)]+'">' +
                '<div class="panel-body p-0">' +
                '<div id="owl-slider-2" class="owl-carousel owl-loaded owl-drag">' +
                '<div class="owl-nav" style="color: white">' +
                '<h4><strong>'+ item_name +'</strong></h4>' +
                '<h5>'+ ratio +'</h5>' +
                '<h5><strong>'+ price +'Tk</strong></h5>' +
                '</div></div></div></div></div>';

            item_count++;
        }
    }

    if (menu_name == 'Open Food' || menu_name == 'open food' || menu_name == 'Open food') {

        htmlstr += '<div class="col-md-4 pointer" id="addOpenFoodDiv" onclick="addOpenFoodModal('+menu_id+')">' +
            '<div class="panel panel-default text-center text-white slider-bg m-b-0" style="height: 200px; background-color: '+background_color[getRandomNumber(0, background_color.length-1)]+'">' +
            '<div class="panel-body p-0">' +
            '<div id="owl-slider-2" class="owl-carousel owl-loaded owl-drag">' +
            '<div class="owl-nav" style="color: white">' +
            '<h2><strong>Add Open Food</strong></h2>' +
            '<span><i class="fa fa-plus"></i></span>';

    }


    $( "#item_div_span" ).empty();
    $( "#item_div_span" ).append(htmlstr);

}


function addToCart(item_id) {

    for (var i = 0; i < menu_items.length; i++) {

        var item = menu_items[i];
        var id = item.id;

        if (id == item_id) {
            menu_items[i].count++;
            break;
        }
    }


    // for (var i = 0; i < menu_items.length; i++) {
    //
    //     if (menu_items[i][0] == item_id) {
    //
    //         var readymade_item = menu_items[i][8];
    //         var item_quantity = menu_items[i][9];
    //
    //         if (readymade_item == 1 && item_quantity>0) {
    //             menu_items[i][6]++;
    //             menu_items[i][9] -= 1;
    //             $('#item_q_' + item_id).text('Available: ' + menu_items[i][9]);
    //         }
    //
    //         else if (readymade_item == 1 && item_quantity<=0) swal('Warning!', 'No Available Item', 'warning');
    //
    //         else menu_items[i][6]++;
    //
    //         break;
    //     }
    // }

    printCart();

}


function getRandomNumber(min, max) {
    var number = Math.floor(Math.random() * (max - min + 1)) + min;
    if (temp_bg_color_previous_color == number) {
        temp_bg_color_previous_color = number;
        return getRandomNumber(min, max);
    }
    else {
        temp_bg_color_previous_color = number;
        return number;
    }
}


function printCart() {

    var htmlstr = '';
    var htmlstr_modal = '';
    var htmlstr_token = '';


    var total_bill = 0;
    discount = 0.0;
    var item_count = 1;
    var flag = false;
    var discount_amount = $('#discount_amount').val();
    var vat = 0;

    for (var i = 0; i < menu_items.length; i++) {

        var item = menu_items[i];

        var item_id = item.id;
        var menu_id = item.menu_id;
        var item_name = item.item_name;
        var ratio = item.ratio;
        var price = item.price;
        var discount = item.discount;
        var item_vat = item.item_vat;
        var set_menu = item.set_menu;
        var count = item.count;

        if (count > 0) {

            var this_item_price = (price - discount) * count;
            if (item_vat > 0) vat += parseFloat(this_item_price * (item_vat/100.00));

            var this_item_modifier = '';
            var this_item_modifier_price = 0;
            var this_item_modifier_vat = 0;
            var modifier_price = 0;
            var order_modifiers_modifier_quantity = 0;
            var modifier_id = 0;
            var htmlstr_modifiers = '';
            var htmlstr_token_modifiers = '';

            for (var j = 0; j < order_modifiers.length; j++) {

                var modifier_data = order_modifiers[j];

                var this_item_id = modifier_data.item_id;
                modifier_id = modifier_data.modifier_id;
                var modifier_quantity = modifier_data.modifier_quantity;

                //var order_modifiers_item_id = order_modifiers[j][0];
                //modifier_id = order_modifiers[j][1];
                //order_modifiers_modifier_quantity = order_modifiers[j][2];
                var mod_index = j;

                if (modifier_quantity > 0) {

                    if (this_item_id == item_id) {

                        var modifier = getModifierData(modifier_id);

                        var modifier_name = modifier.modifier_name;
                        modifier_price = parseInt(modifier.price);
                        var modifier_vat = parseInt(modifier.modifier_vat);

                        this_item_modifier_price = (modifier_price * order_modifiers_modifier_quantity);
                        this_item_modifier_vat += parseFloat(modifier_price * (modifier_vat/100.00));
                        vat += this_item_modifier_vat;

                        this_item_modifier += ', ' + modifier_name + ' x' + order_modifiers_modifier_quantity;

                        htmlstr_modifiers += '<tr class="even pointer" id="item_row_'+ item_id +'">' +
                            '<td></td>' +
                            '<td>Add '+ modifier_name +'</td>' +
                            '<td>'+ modifier_price +'</td>' +
                            '<td><i class="fa fa-minus" onclick="modifierDec('+mod_index+')"></i>&nbsp;&nbsp;'+order_modifiers_modifier_quantity+'&nbsp;&nbsp;' +
                            '<i class="fa fa-plus" onclick="modifierInc('+mod_index+')"></i></td>' +
                            '<td>'+ (modifier_price * order_modifiers_modifier_quantity) +'</td>' +
                            '<td>' +
                            '<span class="pointer" onclick="removeModifier('+mod_index+')" title="Remove Item">' +
                            '<i class="glyphicon glyphicon-trash"></i>' +
                            '</span>' +
                            '</td></tr>';

                        htmlstr_token_modifiers += '<tr>' +
                            '<td>Add '+ modifier_name + '</td>' +
                            '<td>'+ order_modifiers_modifier_quantity +'</td>' +
                            '</tr>';

                    }
                }
            }

            flag = true;
            total_bill += (this_item_price + this_item_modifier_price);
            discount += parseFloat(discount * count);

            htmlstr += '<tr class="even pointer" id="item_row_'+ item_id +'">' +
                '<td>'+ item_count +'</td>' +
                '<td>'+ item_name + ' ' + ratio +'</td>' +
                '<td>'+ parseInt(price) +'</td>' +
                '<td><i class="fa fa-minus" onclick="itemDec('+item_id+')"></i>&nbsp;&nbsp;'+count+'&nbsp;&nbsp;' +
                '<i class="fa fa-plus" onclick="itemInc('+item_id+')"></i>&nbsp;&nbsp;' +
                '<span class="pointer" onclick="addItemQty('+item_id+')"><i class="fa fa-forward"></i></span></td>' +
                '<td>'+ this_item_price +'</td>' +
                '<td>' +
                '<button type="button" class="btn btn-success btn-xs pointer" onclick="addModifier('+item_id+')" title="Add Modifier">' +
                'Modifier</button> &nbsp;&nbsp;' +
                '<span class="pointer" onclick="removeCartItem('+item_id+')" title="Remove Item">' +
                '<i class="glyphicon glyphicon-trash"></i>' +
                '</span>' +
                '</td></tr>';

            htmlstr += htmlstr_modifiers;


            htmlstr_token = '<tr>' +
                '<td>'+ item_name +'</td>' +
                '<td class="text-right">'+ count +'</td>' +
                '</tr>';

            printKitchenToken();

            //$( "#token_div_table_" + printer_id ).empty();
            //$( "#token_div_table_" + printer_id ).append(htmlstr_token);

            htmlstr_token += htmlstr_token_modifiers;

            if (set_menu) htmlstr_token += getSetMenuItems(item_id);

            item_count++;
        }
    }


    vat = Math.ceil(vat);
    vat = parseInt(vat, 10);


    var order_charges = getCharges(total_bill);

    var order_service_charge = order_charges['service_charge'];
    var order_sd_percentage = order_charges['sd_percentage'];
    var order_service_charge_vat_percentage = order_charges['service_charge_vat_percentage'];
    var order_sd_vat_percentage = order_charges['sd_vat_percentage'];
    var order_discount = order_charges['discount'];

    var net_amaount = 0.0;

    if (price_including_vat == 1) {
        $('#receipt_vat_header').text('(Prices are inclusive of VAT)');
        net_amaount = parseFloat(total_bill + order_service_charge + order_sd_percentage + order_service_charge_vat_percentage + order_sd_vat_percentage);
    }

    else {
        $('#receipt_vat_header').text('(Prices are exclusive of VAT)');
        net_amaount = parseFloat(total_bill + order_service_charge + vat + order_sd_percentage + order_service_charge_vat_percentage + order_sd_vat_percentage);
    }


    var service = parseFloat(total_bill * (service_charge/100.0)).toFixed(2);
    net_amaount = net_amaount.toFixed(2);


    $( ".cart_bill" ).text(total_bill.toFixed(2));
    $( ".grand_total" ).text(total_bill.toFixed(2));
    $( ".cart_vat" ).text(vat.toFixed(2));
    $( ".cart_service_charge" ).text(service);
    $( ".cart_sd_percentage" ).text(order_sd_percentage.toFixed(2));
    $( ".cart_service_charge_vat" ).text(order_service_charge_vat_percentage.toFixed(2));
    $( ".cart_discount" ).text(discount);
    $( ".cart_net_amaount" ).text(net_amaount);


    $( "#cart_item_div" ).empty();
    $( "#cart_table" ).append(htmlstr);

    $( "#cart_item_modal" ).empty();
    $( "#cart_table_modal" ).append(htmlstr_modal);

    //$( "#token_div_table" ).empty();
    //$( "#token_div_table" ).append(htmlstr_token);

    if (flag) {
        $('#send_kot').prop('disabled', false);
        $('#order_instruction_button').prop('disabled', false);
        $('#add_discount_button').prop('disabled', false);
    }

    else {
        $('#send_kot').prop('disabled', true);
        $('#order_instruction_button').prop('disabled', true);
        $('#add_discount_button').prop('disabled', true);
    }


}


function getModifierData(modifier_id) {

    // console.log('hit the function');
    // console.log('modifier_id: ' + modifier_id);

    for (var i = 0; i < modifiers.length; i++) {

        var this_modifier_id = modifiers[i][0];
        var this_modifier_name = modifiers[i][1];
        var this_price = modifiers[i][2];
        var this_modifier_vat = modifiers[i][3];

        //console.log('this_modifier_id: ' + this_modifier_id);


        if (modifier_id == this_modifier_id) {

            // console.log('this_modifier_id: ' + this_modifier_id);
            // console.log('this_modifier_name: ' + this_modifier_name);
            // console.log('this_price: ' + this_price);

            var modifier = {
                modifier_name: this_modifier_name,
                price: this_price,
                modifier_vat: this_modifier_vat,
            };

            return modifier;
        }

    }
}


function modifierInc(index) {

    var item_id = order_modifiers[index][0];
    var modifier_id = order_modifiers[index][1];
    var modifier_quantity = order_modifiers[index][2];

    order_modifiers[index][2]++;

    printCart();
}


function modifierDec(index) {

    var item_id = order_modifiers[index][0];
    var modifier_id = order_modifiers[index][1];
    var modifier_quantity = order_modifiers[index][2];

    if (modifier_quantity>0) order_modifiers[index][2]--;

    printCart();
}


function removeModifier(index) {

    order_modifiers[index][2] = 0;

    printCart();
}


function itemDec(item_id) {

    for (var i = 0; i < menu_items.length; i++) {
        if (menu_items[i][0] == item_id) {
            if (menu_items[i][6]>1) {

                var readymade_item = menu_items[i][8];
                var item_quantity = menu_items[i][9];

                if (readymade_item == 1) {
                    menu_items[i][6]--;
                    menu_items[i][9] += 1;
                    $('#item_q_' + item_id).text('Available: ' + menu_items[i][9]);
                }


                else menu_items[i][6]--;

                break;
            }
        }
    }
    printCart();
}


function itemInc(item_id) {

    for (var i = 0; i < menu_items.length; i++) {
        if (menu_items[i][0] == item_id) {

            var readymade_item = menu_items[i][8];
            var item_quantity = menu_items[i][9];

            if (readymade_item == 1 && item_quantity>0) {
                menu_items[i][6]++;
                menu_items[i][9] -= 1;
                $('#item_q_' + item_id).text('Available: ' + menu_items[i][9]);
            }

            else if (readymade_item == 1 && item_quantity<=0) swal('Warning!', 'No Available Item', 'warning');

            else menu_items[i][6]++;


            break;
        }
    }
    printCart();
}


function addItemQty(item_id) {

    $('#item_id').val(item_id);
    $('#item_amount').val(getItemQuantity(item_id));

    $('#item_quantity_modal').modal('show');

}


function addModifier(item_id) {

    $('#modifier_item_id').val(item_id);
    $('#modifier_quantity').val(1);
    displayModifierPrice();

    $('#add_modifier_modal').modal('show');

}


function displayModifierPrice() {

    var modifier_id = $('#modifier_id').val();
    var modifier_quantity = $('#modifier_quantity').val();

    var modifier = getModifierData(modifier_id);

    var modifier_price = parseInt(modifier.price);
    var modifier_vat = parseInt(modifier.modifier_vat);


    $('#modifier_unit_price').val(modifier_price);
    $('#modifier_total_price').val(modifier_price * modifier_quantity);



}


function removeCartItem(item_id) {

    for (var i = 0; i < menu_items.length; i++) {
        if (menu_items[i][0] == item_id) {

            menu_items[i][6] = 0;
            removeModifierItem(item_id);
            break;
        }
    }

    printCart();
}


function removeModifierItem(item_id) {

    for (var i = 0; i < order_modifiers.length; i++) {

        var order_modifiers_item_id = order_modifiers[i][0];

        if (order_modifiers_item_id == item_id) {
            order_modifiers.splice( i, 1 );
        }
    }
}


function printKitchenToken() {

    var waiter_name = $('#waiter_id option:selected').text();
    $('.token_waiter_name').text(waiter_name);

    var htmlstr_token = '';

    for (var i = 0; i < menu_items.length; i++) {

        var item = menu_items[i];

        var item_id = item.id;
        var item_name = item.item_name;
        var ratio = item.ratio;
        var count = item.count;

        if (count>0) {

            htmlstr_token += '<tr>' +
                '<td><h3>'+ item_name + ' ' + ratio +'</h3></td>' +
                '<td><h3 class="text-right">'+ count +'</h3></td>' +
                '</tr>';

            for (var j = 0; j < order_modifiers.length; j++) {

                var order_modifiers_item_id = order_modifiers[j][0];
                var modifier_id = order_modifiers[j][1];
                var modifier_quantity = order_modifiers[j][2];

                if (modifier_quantity > 0 && order_modifiers_item_id == item_id) {

                    var modifier = getModifierData(modifier_id);
                    var modifier_name = modifier.modifier_name;

                    htmlstr_token += '<tr>' +
                        '<td><h3>'+ modifier_name + '</h3></td>' +
                        '<td><h3 class="text-right">'+ modifier_quantity +'</h3></td>' +
                        '</tr>';

                }
            }

        }
    }


    $( "#kitchen_token").empty();
    $( "#kitchen_token").append(htmlstr_token);


}


function getSetMenuItems(given_menu_item_id) {

    var htmlstr = '';

    for (var i = 0; i < setmenu_items.length; i++) {

        var item_id = setmenu_items[i][0];
        var item_name = setmenu_items[i][1];
        var menu_item_id = setmenu_items[i][2];


        if (given_menu_item_id == menu_item_id) {
            htmlstr += '<tr><td>&nbsp;&nbsp;&nbsp;'+ item_name +'</td><td></td></tr>';
        }
    }

    return htmlstr;

}


function getCharges(order_total) {

    var order_service_charge = order_total * (this.service_charge/100.0);
    var order_sd_percentage = order_total * (this.sd_percentage/100.0);
    var order_service_charge_vat_percentage = order_service_charge * (this.service_charge_vat_percentage/100.0);
    var order_sd_vat_percentage = order_sd_percentage * (this.sd_vat_percentage/100.0);
    var order_discount = this.discount;

    var order_charges = {
        service_charge: order_service_charge,
        sd_percentage: order_sd_percentage,
        service_charge_vat_percentage: order_service_charge_vat_percentage,
        sd_vat_percentage: order_sd_vat_percentage,
        discount: order_discount
    };

    return order_charges;

}


function printOrderPaymentReceipt(order_status) {

    $("#order_span").remove();

    var order_id = $('#order_id').val();

    if (order_status != 'paid' && order_id != 0) {

        var params = {
            order_id: order_id,
            order_status: order_status
        };

        $.ajax({

            url: '/change/order-status',
            type: 'POST',
            format: 'JSON',
            data: {'_token': $('#token').val(), params: params},

            success: function (response) {
                $('.bill_type').text('Guest Print');
                window.print();
                reloadCurrentPage();

                //$( "#order_span" ).removeClass("div-hide");
            },

            error: function (error) {
                showErrorNotification();
            }

        });

    }

    else {
        window.print();
    }




}


function clearCart() {

    for (var i = 0; i < menu_items.length; i++) {
        var item_id = menu_items[i][0];
        menu_items[i][6] = 0;
    }

    order_modifiers = [];
    printCart();
}


function generateReceipt(order_id) {

    if (order_id != 0) {

        var params = {
            order_id: order_id
        };

        $.ajax({
            url: '/get/order/history',
            type: 'POST',
            format: 'JSON',
            data: {params: params, "_token": $('#token').val()},

            success: function (response) {

                var json_message = JSON.parse(response);

                var order_data = json_message.order_data;
                var order_details = json_message.order_details;
                var item_modifiers = json_message.order_modifiers;

                var discount = order_data.discount>0.0? '-' + order_data.discount:'0';

                var discount_amount_type;
                var discount_percentage;

                if (order_data.discount_amount_type=='cash') {

                    discount_percentage = order_data.discount;
                    discount_amount_type = 'Tk';

                }

                else {

                    discount_percentage = order_data.discount_percentage;
                    discount_amount_type = '%';

                }

                var payment_modal_count = 1;
                var htmlstr = '';
                var htmlstr_modal = '';


                //console.log(order_details);

                for (var i=0; i<order_details.length; i++) {

                    var order_details_id = order_details[i].order_details_id;
                    var menu_item_id = order_details[i].menu_item_id;
                    var menu_item_name = order_details[i].menu_item_name;
                    var ratio = order_details[i].ratio != null?order_details[i].ratio:'';
                    var item_price = parseInt(order_details[i].item_price);
                    var item_discount = parseInt(order_details[i].item_discount);
                    var item_quantity = parseInt(order_details[i].item_quantity);
                    var unit_price = ((item_price - item_discount));
                    var item_status = parseInt(order_details[i].item_status);
                    var total_cost = (unit_price * item_quantity);

                    var modifiers_str = '';
                    var item_modifier_price = 0;


                    for (var j=0; j<item_modifiers.length; j++) {

                        var modifier_order_details_id = item_modifiers[j].order_details_id;
                        var modifier_name = item_modifiers[j].modifier_name;
                        var modifier_price = item_modifiers[j].price;
                        var modifier_quantity = item_modifiers[j].quantity;

                        if (order_details_id == modifier_order_details_id) {

                            modifiers_str += ', ' + modifier_name + ' x' + modifier_quantity;
                            item_modifier_price += (parseInt(modifier_price) * parseInt(modifier_quantity));
                            unit_price += parseInt(modifier_price);

                        }

                    }


                    /*
                     previous_cart_table
                     */

                    if (item_quantity > 0) {

                        htmlstr += '<tr class="even pointer';
                        if (item_status == 0) htmlstr += ' strike';
                        htmlstr += '" id="order_details_'+ order_details_id +'">' +
                            '<td>'+ menu_item_name + ' ' + ratio + modifiers_str +'</td>' +
                            '<td>'+ item_quantity +'</td>' +
                            '<td>' +
                            '<button type="button" class="btn btn-danger btn-xs pointer" ';
                        if (item_status != 0) htmlstr += 'onclick="void_item(\'' + order_details_id + '\', \''+item_quantity+'\')"';
                        htmlstr += '>Void</button> ';

                        if (item_status != 0 && item_price>0) {

                            htmlstr += '<button type="button" class="btn btn-primary btn-xs pointer" ' +
                                'onclick="complimentary_item(\''+order_details_id+'\', \''+item_quantity+'\')">Complimentary</button>';

                        }

                        htmlstr += '</td></tr>';

                    }



                    /*
                     receipt_order_table
                     */

                    if (item_quantity > 0) {

                        htmlstr_modal += '<tr>' +
                            '<th scope="row">'+ payment_modal_count++ +'</th>' +
                            '<td>'+ menu_item_name + ' ' + ratio + modifiers_str +'</td>' +
                            '<td>'+ unit_price +'</td>' +
                            '<td>'+ item_quantity +'</td>' +
                            '<td class="text-right">'+ (total_cost + item_modifier_price) +'</td></tr>';

                    }



                }


                $( "#previous_cart_table" ).empty();
                $( "#previous_cart_table" ).append(htmlstr);

                $( ".receipt_order_table" ).empty();
                $( ".receipt_order_table" ).append(htmlstr_modal);


                $( ".receipt_customer_name" ).text(order_data.customer_name);
                $( ".receipt_waiter_name" ).text(order_data.waiter_name);
                $( ".receipt_created_at" ).text(formatDate(order_data.updated_at));
                $( ".receipt_table_no" ).text(order_data.table_name);
                $( ".receipt_order_no" ).text(formatInvoiceDate(order_data.created_at) + numberPadding(order_data.order_id));
                $( ".receipt_order_type" ).text(uc_first(order_data.order_type));


                $( ".receipt_subtotal" ).text(order_data.bill);
                $( ".receipt_vat_total" ).text(order_data.vat);
                $( ".receipt_service_charge" ).text(order_data.service_charge);
                $( ".receipt_sd_percentage" ).text(order_data.sd_charge);
                $( ".receipt_service_charge_vat_percentage" ).text(order_data.service_charge_vat);
                $( ".receipt_sd_vat_percentage" ).text(order_data.sd_vat);

                $( ".receipt_discount_percentage" ).text(discount_percentage + discount_amount_type);

                $( ".receipt_discount_amount" ).text(discount);
                $( ".receipt_total_amount" ).text(order_data.total_bill);

                $( "#order_amount_total" ).text(order_data.total_bill);
                $( ".discount_amount" ).text(order_data.discount);
                $( ".order_net_amaount" ).text(order_data.total_bill - order_data.discount);


                printCart();




            },
            error: function (error) {
                swal('Error!', 'Something went wrong', 'error');
            }
        });


    }

}


function printTokenReceipt() {

    w = window.open();
    w.document.write($('#kitchen_token_receipt').html());
    w.print();
    w.close();

}










