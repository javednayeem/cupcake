<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Venus POS</title>


    <meta name="author" content="Javed Nayeem">
    <meta name="description" content="fully integrated Intuitive Restaurant POS Software suitable to work in restaurants"/>

    <link rel="shortcut icon" href="/logo.png">

    @include("cupcake.partials.pre-scriprts")

</head>



<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="/" class="site_title text-center">
                        <img src="/logo.png" id="logo-icon">
                        <span>Venus POS</span>
                    </a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
            @include('cupcake.partials.menu-profile-quick-info')
            <!-- /menu profile quick info -->

                <br />

                <!-- sidebar menu -->
            @include('cupcake.partials.menu')
            <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
            @include('cupcake.partials.menu-footer')
            <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
    @include('cupcake.partials.top-navigation')
    <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

                <div class="page-title hidden-print">
                    <div class="title_left hidden-print">
                        <h3>@yield('title')</h3>
                    </div>

                    {{--<div class="title_right">--}}
                    {{--<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">--}}
                    {{--<div class="input-group">--}}
                    {{--<input type="text" class="form-control" placeholder="Search for...">--}}
                    {{--<span class="input-group-btn">--}}
                    {{--<button class="btn btn-default" type="button">Go!</button>--}}
                    {{--</span>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                </div>

                <div class="clearfix"></div>

                <input type="hidden" id="user_name" value="{{ Auth::user()->name }}">
                <input type="hidden" id="token" value="{{ csrf_token() }}">

                @yield('content')
            </div>

        </div>
    </div>
    <!-- /page content -->

    <form id="logout-form" action="/logout" method="POST" style="display: none;">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
    </form>

    <!-- footer content -->
@include('cupcake.partials.footer')
<!-- /footer content -->
</div>

@include("cupcake.partials.post-scripts")

</body>
</html>
