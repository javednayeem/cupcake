<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CupCake</title>

    <!-- Styles -->
    {{--<link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}
</head>
<body>

<script src="/js/jquery.min.js"></script>

<div id="app">
    <input type="hidden" id="token" value="{{ csrf_token() }}">
    @yield('content')
</div>

<script src="/js/jquery.min.js"></script>
</body>
</html>
