<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CupCake</title>
    <link rel="shortcut icon" href="/cupcake.png">

    @include("cupcake.partials.pre-scriprts")

</head>



<body class="nav-md">
<div class="container body">



    <div class="right_col" role="main">
        <div class="">
            @yield('content')
        </div>
    </div>



</div>

@include("cupcake.partials.post-scripts")




</body>
</html>
