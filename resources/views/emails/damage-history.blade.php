<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CupCake</title>
    <link rel="shortcut icon" href="/cupcake.png">
</head>



<body class="nav-md">
<div class="container body">



    <div class="right_col" role="main">
        <div class="">
            <h2 style="text-align: center;"><strong>{{ $restaurant_data->restaurants_name }}</strong></h2>
            <h2 style="text-align: center;"><strong>{{ $restaurant_data->address }}</strong></h2>
            <h2 style="text-align: center;"><strong>{{ $restaurant_data->area }} {{ $restaurant_data->city }}</strong></h2>
            <h2 style="text-align: center;"><strong>{{ $report_data['report_type'] }}</strong></h2>
            <p style="text-align: center;">&nbsp;</p>


            <table style="margin-left: auto; margin-right: auto;">

                <thead>

                <tr>
                    <th class="column-title">#</th>
                    <th class="column-title">Category</th>
                    <th class="column-title">Product</th>
                    <th class="column-title">Quantity</th>
                    <th class="column-title">Unit Price</th>
                    <th class="column-title">Grand Total</th>
                    <th class="column-title">Damaged Date</th>
                </tr>

                </thead>

                <tbody>

                @php $damage_count=1; @endphp
                @php $total_quantity=0; @endphp
                @php $total=0; @endphp

                @foreach($report as $damage)

                    @php $grand_total = ($damage->quantity * $damage->unit_price); @endphp
                    @php $total_quantity += $damage->quantity; @endphp
                    @php $total += $grand_total; @endphp

                    <tr class="pointer">
                        <td>{{ $damage_count++ }}</td>
                        <td>{{ $damage->category_name }}</td>
                        <td>{{ $damage->product_name }}</td>
                        <td>{{ $damage->quantity }} {{ $damage->unit_name }}</td>
                        <td>{{ $damage->unit_price }}&#2547;</td>
                        <td>{{ $grand_total }}&#2547;</td>
                        <td>{{ date('F j, Y', strtotime( $damage->created_at )) }}</td>
                    </tr>
                @endforeach

                <tr class="even pointer">
                    <td></td>
                    <th>Total</th>
                    <td></td>
                    <th>{{ $total_quantity }}</th>
                    <td></td>
                    <th>{{ $total }}&#2547;</th>
                    <td></td>
                </tr>

                </tbody>
            </table>
        </div>
    </div>



</div>

</body>
</html>
