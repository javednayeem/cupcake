<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CupCake</title>
    <link rel="shortcut icon" href="/cupcake.png">
</head>



<body class="nav-md">
<div class="container body">



    <div class="right_col" role="main">
        <div class="">
            <h2 style="text-align: center;"><strong>{{ $restaurant_data->restaurants_name }}</strong></h2>
            <h2 style="text-align: center;"><strong>{{ $restaurant_data->address }}</strong></h2>
            <h2 style="text-align: center;"><strong>{{ $restaurant_data->area }} {{ $restaurant_data->city }}</strong></h2>
            <h2 style="text-align: center;"><strong>{{ $report_data['report_type'] }}</strong></h2>
            <h2 style="text-align: center;"><strong>{{ date('j-F-Y', strtotime( $report_data['from'] )) }} To {{ date('j-F-Y', strtotime( $report_data['to'] )) }}</strong></h2>
            <p style="text-align: center;">&nbsp;</p>


            <table style="margin-left: auto; margin-right: auto;">

                <thead>

                @if($report_data['report_type'] == 'Invoice Wise Sales')
                    <tr>
                        <th>#</th>
                        <th>Invoice #</th>
                        <th>Served By</th>
                        <th>Item Qty</th>
                        <th>Item Amount</th>
                        <th>VAT</th>
                        <th>Service Charge</th>
                        <th>Service Charge VAT</th>
                        <th>SD</th>
                        <th>Discount</th>
                        <th>Discount Ref.</th>
                        <th>Net Amount</th>
                    </tr>

                @elseif($report_data['report_type'] == 'Category Wise Sale')
                    <tr>
                        <th>#</th>
                        <th>Category Name</th>
                        <th>Item Qty</th>
                        <th>Total Price</th>
                    </tr>

                @elseif($report_data['report_type'] == 'Item Wise Sale')
                    <tr>
                        <th>#</th>
                        <th>Invoice #</th>
                        <th>Served By</th>
                        <th>Item Name</th>
                        <th>Item Qty</th>
                    </tr>
                @endif

                </thead>

                <tbody>

                @php $total_price = 0; @endphp
                @php $total_quantity = 0; @endphp
                @php $total_vat = 0; @endphp
                @php $total_service_charge = 0; @endphp
                @php $total_service_charge_vat = 0; @endphp
                @php $total_sd = 0; @endphp
                @php $total_discount = 0; @endphp
                @php $total_net_amount = 0; @endphp
                @php $count = 1; @endphp

                @foreach($report as $item)

                    @if($report_data['report_type'] == 'Invoice Wise Sales')

                        @php $total_price = floatval($item->bill); @endphp
                        @php $total_quantity += floatval($item->total_item); @endphp
                        @php $total_vat += floatval($item->vat); @endphp
                        @php $total_service_charge += floatval($item->service_charge); @endphp
                        @php $total_service_charge_vat += floatval($item->service_charge_vat); @endphp
                        @php $total_sd += floatval($item->sd_charge); @endphp
                        @php $total_discount += floatval($item->discount); @endphp
                        @php $total_net_amount += floatval($item->total_bill); @endphp

                        <tr style="text-align: center">
                            <th>{{ $count++ }}</th>
                            <th>{{ generateInvoiceNumber($item->order_id, $item->created_at) }}</th>
                            <td>{{ $item->waiter_name }}</td>
                            <td>{{ $item->total_item }}</td>
                            <td>{{ $item->bill }}</td>
                            <td>{{ $item->vat }}</td>
                            <td>{{ $item->service_charge }}</td>
                            <td>{{ $item->service_charge_vat }}</td>
                            <td>{{ $item->sd_charge }}</td>
                            <td>{{ $item->discount }}</td>
                            <td>{{ $item->discount_reference }}</td>
                            <td>{{ $item->total_bill }}</td>
                        </tr>


                    @elseif($report_data['report_type'] == 'Category Wise Sale')

                        @php
                            if (isset($item->price)) $price = intval($item->price);
                            else $price=0;
                        @endphp
                        @php $total_quantity += $quantity = intval($item->item_quantity); @endphp
                        @php $total_price += ($quantity * $price); @endphp

                        @if($quantity > 0)

                            <tr style="text-align: center">
                                <th>{{ $count++ }}</th>
                                <th>{{ $item->item_name }}</th>
                                <td>{{ $quantity }}</td>
                                <td>{{ ($quantity * $price) }}</td>
                            </tr>

                        @endif


                    @elseif($report_data['report_type'] == 'Item Wise Sale')

                        @php $total_quantity += $quantity = intval($item->item_quantity); @endphp

                        @if($quantity > 0)

                            <tr style="text-align: center">
                                <th>{{ $count++ }}</th>
                                <th>{{ generateInvoiceNumber($item->order_id, $item->created_at) }}</th>
                                <td>{{ $item->waiter_name }}</td>
                                <td>{{ $item->item_name }}</td>
                                <td>{{ $quantity }}</td>
                            </tr>

                        @endif

                    @endif

                @endforeach


















                @if($report_data['report_type'] == 'Invoice Wise Sales')

                    <tr style="text-align: center">
                        <th>Total</th>
                        <th></th>
                        <td></td>
                        <th>{{ $total_quantity }}</th>
                        <th>{{ $total_price }}</th>
                        <th>{{ $total_vat }}</th>
                        <th>{{ $total_service_charge }}</th>
                        <th>{{ $total_service_charge_vat }}</th>
                        <th>{{ $total_sd }}</th>
                        <th>{{ $total_discount }}</th>
                        <td></td>
                        <th>{{ $total_net_amount }}</th>
                    </tr>

                @endif


                @if($report_data['report_type'] == 'Category Wise Sale')

                    <tr style="text-align: center">
                        <th></th>
                        <th>Total</th>
                        <th>{{ $total_quantity }}</th>
                        <th>{{ $total_price }}</th>
                        <th>{{ $total_price }}</th>
                    </tr>

                @endif


                @if($report_data['report_type'] == 'Item Wise Sale')

                    <tr style="text-align: center">
                        <th></th>
                        <th>Total</th>
                        <th></th>
                        <th></th>
                        <th>{{ $total_quantity }}</th>
                    </tr>

                @endif





                </tbody>
            </table>
        </div>
    </div>



</div>

</body>
</html>
