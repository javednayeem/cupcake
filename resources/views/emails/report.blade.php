<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CupCake</title>
    <link rel="shortcut icon" href="/cupcake.png">



</head>



<body class="nav-md">
<div class="container body">



    <div class="right_col" role="main">
        <div class="">
            <h2 style="text-align: center;"><strong>Today's Sales Report</strong></h2>
            <p style="text-align: center;">&nbsp;</p>
            <table style="height: 177px; margin-left: auto; margin-right: auto;" width="697">
                <tbody>

                @php
                    $cash_sale = $expenses_details->cash_sale!=''?$expenses_details->cash_sale:0;
                    $current_cash_sale = $cash_sale + $expenses_details->transfer_sales - $expenses_details->sales_expense;

                    $card_sale = $expenses_details->card_sale!=''?$expenses_details->card_sale:0;
                    $sale = $expenses_details->today_sale + $expenses_details->transfer_sales - $expenses_details->sales_expense;
                    $petty_cash = $expenses_details->cash_opening_total_amount + $expenses_details->transfer_petty_cash -  $expenses_details->petty_cash_expense;
                    $loan = $expenses_details->loan_amount;
                    $total = $current_cash_sale + $petty_cash;

                    $total_loan = $expenses_details->total_loan!=''?$expenses_details->total_loan:0;
                @endphp

                <tr>

                    <td style="width: 230px;">
                        <h3><strong>Cash Sale: {{ $cash_sale }} ৳</strong></h3>
                    </td>

                    <td style="width: 230px;">
                        <h3><strong>Card Sale: {{ $card_sale }} ৳</strong></h3>
                    </td>

                    <td style="width: 230px;">
                        <h3><strong>Total Order: {{ $expenses_details->today_order }}</strong></h3>
                    </td>

                </tr>

                <tr>

                    <td style="width: 230px;">
                        <h3><strong>Expenses: {{ ($expenses_details->sales_expense + $expenses_details->petty_cash_expense) }} ৳</strong></h3>
                    </td>

                    <td style="width: 230px;">
                        <h3><strong>Petty Cash: {{ $petty_cash }} ৳</strong></h3>
                    </td>
                    <td style="width: 230px;">
                        <h3><strong>Loan: {{ $loan }} ৳</strong></h3>
                    </td>

                </tr>


                <tr>
                    <td style="width: 230px;">

                        <h3><strong></strong></h3>
                    </td>
                    <td style="width: 230px;">
                        <h3><strong>Cash In Hand: {{ $total }} ৳</strong></h3>
                    </td>
                    <td style="width: 230px;">
                        <h3><strong></strong></h3>
                    </td>
                </tr>

                </tbody>
            </table>
        </div>
    </div>



</div>






</body>
</html>
