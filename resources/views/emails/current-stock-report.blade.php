<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CupCake</title>
    <link rel="shortcut icon" href="/cupcake.png">
</head>



<body class="nav-md">
<div class="container body">



    <div class="right_col" role="main">
        <div class="">
            <h2 style="text-align: center;"><strong>{{ $restaurant_data->restaurants_name }}</strong></h2>
            <h2 style="text-align: center;"><strong>{{ $restaurant_data->address }}</strong></h2>
            <h2 style="text-align: center;"><strong>{{ $restaurant_data->area }} {{ $restaurant_data->city }}</strong></h2>
            <h2 style="text-align: center;"><strong>{{ $report_data['report_type'] }}</strong></h2>
            <p style="text-align: center;">&nbsp;</p>


            <table style="margin-left: auto; margin-right: auto;">

                <thead>

                <tr>
                    <th class="column-title">Code</th>
                    <th class="column-title">Product Name</th>
                    <th class="column-title">Category</th>
                    <th class="column-title">Quantity</th>
                    <th class="column-title">Consumption</th>
                    <th class="column-title">Damage</th>
                    <th class="column-title">Actual Stock</th>
                    <th class="column-title">Stock Price</th>
                </tr>

                </thead>

                <tbody>

                @php $t_stock_price=0; @endphp

                @foreach($report as $product)

                    @php $quantity = floatval($product->quantity) @endphp
                    @php $consumption_quantity = floatval($product->consumption_quantity) @endphp
                    @php $damage_quantity = floatval($product->damage_quantity) @endphp
                    @php $actual_stock = $quantity - ($damage_quantity) @endphp
                    @php $t_stock_price+= ($actual_stock * $product->avg_price) @endphp

                    <tr>
                        <td>{{ $product->product_code }}</td>
                        <td>{{ $product->product_name }}</td>
                        <td>{{ $product->category_name }}</td>
                        <td>{{ $product->quantity . ' ' . $product->unit_name}}</td>
                        <td>{{ $consumption_quantity==null?0:$consumption_quantity}} {{ $product->unit_name }}</td>
                        <td>{{ $damage_quantity==null?0:$damage_quantity}} {{ $product->unit_name }}</td>
                        <td>{{ $actual_stock . ' ' . $product->unit_name}}</td>
                        <td>{{ $actual_stock * $product->avg_price }} &#2547;</td>
                    </tr>

                @endforeach


                <tr>
                    <th>Total</th>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <th>{{ $t_stock_price }} &#2547;</th>
                </tr>

                </tbody>
            </table>
        </div>
    </div>



</div>

</body>
</html>
