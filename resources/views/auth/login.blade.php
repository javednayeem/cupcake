<!DOCTYPE html>
<html lang="en">
<head>
    <title>Venus POS</title>

    <meta name="author" content="Javed Nayeem">
    <meta name="description" content="fully integrated Intuitive Restaurant POS Software suitable to work in restaurants"/>


    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="/logo.png">

    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css">

    <link rel="stylesheet" type="text/css" href="/css/util.css">
    <link rel="stylesheet" type="text/css" href="/css/main.css">

</head>
<body>

<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">


            <form role="form" action="{{ route('login') }}" method="post" class="login100-form validate-form" autocomplete="off">

                @csrf

                <span class="login100-form-title p-b-34"><img src="/logo.png" id="logo-icon" width="30">enus POS</span>

                <div class="wrap-input100 rs1-wrap-input100 validate-input m-b-20" data-validate="Type user name">
                    <input type="text" name="email" placeholder="Username" class="input100 form-username form-control" id="email">
                    <span class="focus-input100"></span>
                </div>


                <div class="wrap-input100 rs2-wrap-input100 validate-input m-b-20" data-validate="Type password">
                    <input type="password" name="password" placeholder="Password" class="input100 form-password form-control" id="password">
                    <span class="focus-input100"></span>
                </div>

                @if ($errors->has('email'))
                    <span class="help-block" style="color: red">** <strong>{{ $errors->first('email') }}</strong></span>
                @endif

                <div class="container-login100-form-btn">
                    <button type="submit" class="login100-form-btn">Sign in</button>
                </div>

                <div class="w-full text-center p-t-27 p-b-239">
                    {{--<span class="txt1">Forgot</span>--}}
                    {{--<a href="#" class="txt2">User name / password?</a>--}}
                </div>

                {{--<div class="w-full text-center">--}}
                {{--<a href="#" class="txt3">--}}
                {{--Sign Up--}}
                {{--</a>--}}
                {{--</div>--}}
            </form>

            <div class="login100-more" style="background-image: url('/images/login_sidebar.jpg');"></div>
        </div>
    </div>
</div>




</body>
</html>