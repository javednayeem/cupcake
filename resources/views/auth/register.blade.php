<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CupCake</title>
    <link rel="shortcut icon" href="/cupcake.png">

    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/css/form-elements.css">
    <link rel="stylesheet" href="/assets/css/style.css">



</head>

<body >

@php
    $settings = getAppInfo();
@endphp

<div class="top-content" >

    <div class="inner-bg"
         @if($settings['bg_type'] == 'color')
         style="background-color: {{ $settings['bg_color'] }}"
         @else
         style="background-image: url('/images/bg_image.jpg'); background-repeat: no-repeat; background-size: cover;"
            @endif
    >
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 text">
                    <img src="/cupcake.png" style="width: 15%">
                    <h1>{{ $settings['app_name_1'] }}{{ $settings['app_name_2'] }}</h1>
                    <div class="description">
                        <p>{{ $settings['app_title'] }}</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 form-box">
                    <div class="form-top">
                        <div class="form-top-left">
                            <h3>Customer Register</h3>
                            <p>Enter your Username and Password to register</p>
                        </div>
                    </div>
                    <div class="form-bottom" style="padding-top: 0px">

                        {!! Form::open(['url' => 'customer-register', 'autocomplete' => 'off']) !!}

                        <input type="hidden" name="restaurant_id" value="{{ $data->restaurant_id }}">
                        <input type="hidden" name="creator_id" value="{{ $data->id }}">



                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" id="name" name="name" value="{{ old('name') }}" class="form-username form-control" required autocomplete="off" autofocus>
                        </div>

                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif



                        <div class="form-group">
                            <label class="" for="form-username">Phone No</label>
                            <input type="text" name="email" class="form-username form-control" value="{{ old('email') }}" id="email" required autocomplete="off">
                        </div>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif



                        <div class="form-group">
                            <label class="" for="form-password">Password</label>
                            <input type="password" name="password" placeholder="Password" class="form-password form-control" id="password">
                        </div>

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif

                        <div class="form-group">
                            <label class="" for="form-password">Retype Password</label>
                            <input type="password" name="password_confirmation" required autocomplete="off" class="form-password form-control" id="password-confirm">
                        </div>


                        <div style="margin-top: 30px">
                            <button type="submit" class="btn">Sign up!</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 social-login">

                    <div class="social-login-buttons">
                        <a class="btn btn-link-2" href="#">
                            {{ $settings['app_login_footer'] }}
                        </a>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div>


<!-- Javascript -->
<script src="assets/js/jquery-1.11.1.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/js/jquery.backstretch.min.js"></script>
<script src="assets/js/scripts.js"></script>

<!--[if lt IE 10]>
<script src="assets/js/placeholder.js"></script>
<![endif]-->

</body>

</html>