@extends('layouts.main')

@section('title', 'Profile')

@section('content')

    <script src="/js/profile.js"></script>


    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>User Details</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="col-md-4 col-sm-4 col-xs-12 profile_left">
                        <div class="profile_img">
                            <div id="crop-avatar">
                                <!-- Current avatar -->
                                <img class="img-responsive avatar-view" src="/images/users/{{ $user_data->user_img }}" alt="Avatar" title="Change the avatar">
                            </div>
                        </div>
                        <h3>{{ $user_data->name }}</h3>

                        <ul class="list-unstyled user_data">

                            <li><i class="fa fa-briefcase user-profile-icon"></i> {{ ucfirst(Auth::user()->role) }}</li>

                            <li><i class="fa fa-envelope"></i> {{ $user_data->email }}</li>

                            <li><i class="fa  fa-plus-circle"></i> {{  date('F j, Y', strtotime( $user_data->created_at )) }}</li>

                        </ul>

                        <button data-toggle="modal" data-target="#edit_profile_modal" class="btn btn-primary"><i class="fa fa-edit m-right-xs"></i> Edit Profile</button>
                        <button data-toggle="modal" data-target="#edit_password_modal" class="btn btn-warning"><i class="fa fa-edit m-right-xs"></i> Change Password</button>
                        <br />

                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="edit_profile_modal" role="dialog">
        <div class="modal-dialog">
            <form class="modal-content form-horizontal" action="/edit/profile" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title" id="edit_menu_title">Edit Profile</h4>
                    <input type="hidden" id="edit_menu_id" value="0">
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control" name="name" value="{{ $user_data->name }}">
                    </div>

                    <div class="form-group">
                        <label class="">Display Picture</label>
                        <input type="file" class="form-control" name="image" accept="image/*">
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-success">Done</button>
                </div>
            </form>

        </div>
    </div>


    <div class="modal fade" id="edit_password_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Edit Password</h4>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label>Old Password</label>
                        <input type="password" class="form-control" id="old_password">
                    </div>

                    <div class="form-group">
                        <label>New Password</label>
                        <input type="password" class="form-control" id="new_password">
                    </div>

                    <div class="form-group">
                        <label>Retype Password</label>
                        <input type="password" class="form-control" id="retype_password">
                    </div>


                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="edit_password_button" class="btn btn-success" data-dismiss="modal">Done</button>
                </div>
            </div>

        </div>
    </div>



@endsection
