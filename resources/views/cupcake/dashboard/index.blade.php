@extends('layouts.main')

@section('title', 'Dashboard')

@section('content')

    <script src="/js/dashboard.js"></script>

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">

                <div class="x_content bs-example-popovers">

                    @php
                        $today = date("Y-m-d");
                    @endphp

                    @if($restaurant->license == 0)
                        <div class="alert alert-danger alert-dismissible fade in" role="alert">
                            <h1><strong>No Active License</strong></h1>
                            <h3><strong>Please Contact Your System Administrator For Further Information</strong></h3>
                        </div>
                    @else

                        @php $remaining_days = duration($today, $licenses->license_end_date, '%a'); @endphp

                        @if($remaining_days<=15)
                            <div class="alert alert-warning alert-dismissible fade in" role="alert">
                                <h1><strong>Active License</strong></h1>
                                <h3><strong>Remaining {{  $remaining_days }} {{ $remaining_days>1? "Days":"Day" }}</strong></h3>
                                <h3><strong>Expires on {{ date('F j, Y', strtotime( $licenses->license_end_date )) }}</strong></h3>
                            </div>
                        @else
                            <div class="alert alert-success alert-dismissible fade in" role="alert">
                                <h1><strong>Active License</strong></h1>
                                <h3><strong>Remaining {{  $remaining_days }} {{ $remaining_days>1? "Days":"Day" }}</strong></h3>
                                <h3><strong>Expires on {{ date('F j, Y', strtotime( $licenses->license_end_date )) }}</strong></h3>
                            </div>
                        @endif

                    @endif

                </div>
            </div>
        </div>


        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_content form-group">


                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-edit"></i>
                            </div>
                            <div class="count">{{ $restaurant->today_sale!=null?$restaurant->today_sale:0 }}<span class="taka-icon">&#2547;</span></div>
                            <h3>Today's Sale</h3>
                            <p></p>
                        </div>
                    </div>


                    {{--<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">--}}
                        {{--<div class="tile-stats">--}}
                            {{--<div class="icon"><i class="fa fa-money"></i>--}}
                            {{--</div>--}}
                            {{--<div class="count">{{ $restaurant->total_revenue!=null?$restaurant->total_revenue:0 }}<span class="taka-icon">&#2547;</span></div>--}}
                            {{--<h3>Total Revenue</h3>--}}
                            {{--<p></p>--}}
                        {{--</div>--}}
                    {{--</div>--}}


                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-user"></i>
                            </div>
                            <div class="count">{{ $restaurant->customers }}</div>
                            <h3>Customers</h3>
                            <p></p>
                        </div>
                    </div>


                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-users"></i>
                            </div>
                            <div class="count">{{ $restaurant->stuffs }}</div>
                            <h3>Stuffs</h3>
                            <p></p>
                        </div>
                    </div>


                    @foreach($payment_methods as $payment)

                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="icon"><i class="fa fa-edit"></i>
                                </div>
                                <div class="count">{{ $payment->amount!=null?$payment->amount:0 }}<span class="taka-icon">&#2547;</span></div>
                                <h3>{{ ucwords($payment->method_name) }} Sale</h3>
                                <p></p>
                            </div>
                        </div>

                    @endforeach



                </div>
            </div>
        </div>


        <div class="animated pulse col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Admin Panel</h2>
                    <div class="clearfix"></div>
                </div>


                <div class="lms_default_tab">

                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#work_period_tab" role="tab" data-toggle="tab" aria-expanded="true">Work Periods</a>
                        </li>

                        <li role="presentation" class="">
                            <a href="#email_report_tab" role="tab" data-toggle="tab" aria-expanded="false">E-Mail Report</a>
                        </li>



                    </ul>


                    <div id="myTabContent" class="tab-content">

                        <div class="tab-pane active" id="work_period_tab">
                            <div class="animated pulse col-md-4 col-sm-4 col-xs-12" >
                                <div class="form-horizontal top-margin-20">

                                    <table class="table table-striped">
                                        @php
                                            $flag = true;
                                            if(count($work_period) == 0) $flag = false;
                                            if(isset($work_period[0])) {
                                                if ($work_period[0]->status == 0) $flag = false;
                                            }
                                        @endphp
                                        <tbody>
                                        <tr>
                                            <td width="40%">Work Period Start Date</td>
                                            <td>{{ $flag? date('F j, Y', strtotime( $work_period[0]->start_time )) : 'Work Period Not Started' }}</td>
                                        </tr>
                                        <tr>
                                            <td>Work Period Start Time</td>
                                            <td>{{ $flag? date('h:i:s A', strtotime( $work_period[0]->start_time )) : '' }}</td>
                                        </tr>
                                        <tr>
                                            <td>Time Span</td>
                                            {{--<td id="test_canvas">43 Days 3 Hours 34 Minutes 50 Seconds</td>--}}
                                            @if($flag)
                                                <td id="time_difference_span"><script> get_time_diff('{{ $work_period[0]->start_time }}')</script></td>
                                            @else
                                                <td></td>
                                            @endif
                                        </tr>
                                        </tbody>
                                    </table>

                                    <div class="form-group top-margin-30">
                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">
                                            @if($flag)
                                                <input type="hidden" value="{{$work_period[0]->workperiod_id}}" id="work_period_id">
                                                <button class="btn btn-success" id="end_work_period_button">End Work Period</button>
                                            @else
                                                <button class="btn btn-success" id="start_work_period_button">Start Work Period</button>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="animated pulse col-md-8 col-sm-8 col-xs-12">
                                <div class="x_panel">
                                    <div class="">
                                        <h2>Work Period History</h2>
                                        <div class="clearfix"></div>
                                    </div>

                                    <div class="x_content">
                                        <table class="table table-striped jambo_table">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Start Date</th>
                                                <th>End Date</th>
                                                <th>Time Span</th>
                                                <th>Started By</th>
                                                <th>Ended By</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @php $work_period_count=1; @endphp

                                            @foreach($work_period as $period)
                                                <tr id="{{ $period->workperiod_id }}">
                                                    <td scope="row">{{ $work_period_count++ }}</td>
                                                    <td> {{ date('F j, Y - h:i:s A', strtotime($period->start_time))  }}</td>
                                                    <td> {{ $period->end_time==Null? '':date('F j, Y - h:i:s A', strtotime($period->end_time))  }}</td>
                                                    <td>{{ $period->end_time==Null? '':dateDifference($period->start_time, $period->end_time) }}</td>
                                                    <td>{{ $period->start_time_creator_name }}</td>
                                                    <td>{{ $period->end_time==Null? '':$period->end_time_creator_name }}</td>
                                                </tr>
                                            @endforeach

                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="email_report_tab">
                            <div class="animated pulse col-md-12 col-sm-12 col-xs-12">

                                <input type="hidden" id="restaurant_id" value="{{ Auth::user()->restaurant_id }}">

                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5 top-margin-30" style="margin-bottom: 50px">
                                        <button class="btn btn-primary btn-lg" id="email_report_button">Email Report Now</button>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
                <div class="clearfix"></div>

            </div>
        </div>


    </div>



@endsection
