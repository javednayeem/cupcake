@extends('layouts.main')
@section('title', 'Online Orders')

@section('content')

    {{--<meta http-equiv="refresh" content="10" />--}}

    <script src="/js/online-order.js"></script>

    <div class="row hidden-print">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><button type="button" class="btn btn-primary" onclick="reloadCurrentPage();">Refresh Queue</button></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">

                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr class="headings">
                                <th>Order #</th>
                                <th>Customer</th>
                                <th>Phone</th>
                                <th>Created At</th>
                                <th>Schedule At</th>
                                <th>Total Bill</th>
                                <th>Notes</th>
                                <th>Order Status</th>
                                <th class="text-center" width="20%">Order Items</th>
                                <th class="text-center" width="15%">Action</th>
                            </tr>
                            </thead>

                            <tbody>

                            @php $count=1; @endphp

                            @foreach($orders as $order)

                                @php
                                    $order_details = DB::table('online_order_details')->where('order_id', $order->order_id)->get();
                                @endphp

                                <tr id="order_{{ $order->order_id }}">
                                    <td>{{ $order->order_id }}</td>
                                    <td>{{ $order->name }}</td>
                                    <td>{{ $order->email }}</td>
                                    <td>{{ formatDateTime($order->created_at) }}</td>
                                    <td>{{ formatDateTime($order->scheduled_at) }}</td>
                                    <td>{{ $order->total_bill }}</td>
                                    <td>{{ $order->order_notes }}</td>
                                    <td>

                                        <button type="button" class="btn @if($order->order_status == 'placed')
                                                btn-primary
                                                @elseif($order->order_status == 'confirmed')
                                                btn-info
                                                @elseif($order->order_status == 'processing')
                                                btn-warning
                                                @elseif($order->order_status == 'completed')
                                                btn-success
                                                @elseif($order->order_status == 'cancelled')
                                                btn-danger
                                                @endif">{{ ucwords($order->order_status) }}
                                        </button>

                                    </td>

                                    <td>
                                        @foreach($order_details as $detail)
                                            <li><b>{{ $detail->menu_item_name }}: {{ $detail->item_quantity }}</b></li>
                                        @endforeach

                                        @if($order->order_status == 'confirmed' || $order->order_status == 'processing')
                                            <br>
                                            <button type="button" class="btn btn-primary" onclick="generateToken({{ $order->order_id }})">Print KOT</button>
                                        @endif
                                    </td>
                                    <td class="text-center">

                                        <a type="button" href="/online-order/receipt/{{ $order->order_id }}" class="btn btn-primary">View Receipt</a>

                                        @if($order->order_status == 'placed')
                                            <button type="button" class="btn btn-info" onclick="changeOrderStatus('{{ $order->order_id }}', 'confirmed')">Confirmed</button>
                                            <button type="button" class="btn btn-warning" onclick="changeOrderStatus('{{ $order->order_id }}', 'processing')">Processing</button>
                                            <button type="button" class="btn btn-success" onclick="changeOrderStatus('{{ $order->order_id }}', 'completed')">Completed</button>
                                        @endif

                                        @if($order->order_status == 'confirmed')
                                            <button type="button" class="btn btn-warning" onclick="changeOrderStatus('{{ $order->order_id }}', 'processing')">Processing</button>
                                            <button type="button" class="btn btn-success" onclick="changeOrderStatus('{{ $order->order_id }}', 'completed')">Completed</button>
                                        @endif

                                        @if($order->order_status == 'processing')
                                            <button type="button" class="btn btn-success" onclick="changeOrderStatus('{{ $order->order_id }}', 'completed')">Completed</button>
                                        @endif

                                        @if($order->order_status == 'completed')
                                            <button type="button" class="btn btn-success">Completed</button>
                                        @elseif($order->order_status == 'cancelled')
                                            <button type="button" class="btn btn-danger">Cancelled</button>
                                        @else
                                            <button type="button" class="btn btn-danger" onclick="changeOrderStatus('{{ $order->order_id }}', 'cancelled')">Cancel</button>
                                        @endif

                                    </td>
                                </tr>
                            @endforeach

                            </tbody>

                        </table>
                    </div>


                </div>
            </div>
        </div>
    </div>


    <div class="row div-hide">
        <div class="col-md-3 col-sm-2 col-xs-12" id="kitchen_token">
            <div class="x_panel">

                @php date_default_timezone_set("Asia/Dhaka") @endphp

                <div class="text-center">
                    <h4>{{ $restaurant->restaurants_name }}</h4>
                    <h2>Order #<span id="order_id">12</span></h2>
                    <h4>Time : @php echo date('g:i a, F j, Y', time()); @endphp</h4>
                    <h4>Pickup: <span id="scheduled_at"></span></h4>
                    <div class="clearfix"></div>
                </div>

                <b>==========================</b>

                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                            <tr>
                                <th>Item Name</th>
                                <th></th>
                                <th class="text-right">Qty</th>
                            </tr>
                            </thead>

                            <tbody id="token_div_table" style="height: 100px;overflow-y: auto;"></tbody>
                        </table>
                    </div>
                </div>

                <b>==========================</b>

                <br><br>
                <div class="x_content" id="notes_span">
                    <div class="dashboard-widget-content">

                        <b>Order Notes</b>

                        <ul id="token_instruction"></ul>
                    </div>
                </div>


            </div>

        </div>
    </div>


@endsection
