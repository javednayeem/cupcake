@extends('layouts.main')
@section('title', 'Receipt')

@section('content')

    <link href="/css/order.css" rel="stylesheet">


    <div class="row" id="settle_div">
        <div class="col-md-12">
            <div class="x_panel">

                <div class="x_content">

                    <section class="content invoice" >

                        <div class="row">
                            <div class="col-xs-12 invoice-header text-center">
                                <img src="/images/restaurant-logo/{{ $restaurant->restaurant_img }}" width="100">
                                <br><br>
                                <h3><b>{{ $restaurant->restaurants_name==''? "Cupcake":$restaurant->restaurants_name }}</b></h3>
                                <h5>
                                    <b>
                                        {{ $restaurant->address}}
                                        {{ $restaurant->area==null? '':', '.$restaurant->area }}
                                        {{ $restaurant->city==null? '':', '.$restaurant->city }}
                                    </b>
                                </h5>
                                <h5><b>Phone : {{ $restaurant->phone_number==null? '':$restaurant->phone_number }}</b></h5>
                                <h5 id="receipt_vat_div"><b>VAT Reg. No : {{ $restaurant->vat_no==null? 'N/A':$restaurant->vat_no }}</b></h5>
                                <br>
                                <h3><b>Online Order Receipt</b></h3>
                                <b>==========================================</b>
                            </div>
                        </div>

                        <div class="form-group font-bold">
                            <table class="table top-margin-40">
                                <tbody>
                                <tr><td><strong>Guest: {{ $orders->name }}</strong></td></tr>
                                <tr><td><strong>Time: {{ formatDateTime($orders->created_at) }}</strong></td></tr>
                                <tr><td><strong>Schedule: {{ formatDateTime($orders->scheduled_at) }}</strong></td></tr>
                                <tr><td><strong>Invoice No: {{ generateInvoiceNumber($orders->order_id, $orders->created_at) }}</strong></td></tr>
                                <tr><td><strong>Order Status: {{ ucfirst($orders->order_status) }}</strong></td></tr>
                                <tr><td><strong>Token No: {{ $orders->order_id }}</strong></td></tr>
                                </tbody>
                            </table>
                        </div>

                        <b>==========================================</b>
                        <div class="row">
                            <div class="col-xs-12 table">
                                <table class="table table-striped">

                                    <thead>
                                    <tr>
                                        <th width="5%">#</th>
                                        <th width="50%">Item Name</th>
                                        <th>U.Price</th>
                                        <th>Qty</th>
                                        <th class="text-right">Total</th>
                                    </tr>
                                    </thead>

                                    <tbody>

                                    @php $count=1 @endphp

                                    @foreach($order_details as $detail)

                                        <tr>
                                            <td>{{ $count++ }}</td>
                                            <td>{{ $detail->menu_item_name }}</td>
                                            <td>{{ $detail->item_price }}</td>
                                            <td>{{ $detail->item_quantity }}</td>
                                            <td class="text-right">{{ ($detail->item_price * $detail->item_quantity) }}</td>
                                        </tr>

                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>


                        <div class="row">
                            <b>==========================================</b>
                            <div class="col-xs-8 pull-right">
                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody>

                                        <tr>
                                            <th style="width:50%">SubTotal:</th>
                                            <td class="text-right">{{ $orders->total_bill }}</td>
                                        </tr>

                                        <tr>
                                            <th>VAT ({{$restaurant->vat_percentage}}%):</th>
                                            <td class="text-right">{{ $orders->vat }}</td>
                                        </tr>


                                        @if($restaurant->service_charge>0)
                                            <tr>
                                                <th>Service Charge ({{$restaurant->service_charge}}%) : </th>
                                                <td class="text-right">{{ $orders->service_charge }}</td>
                                            </tr>
                                        @endif


                                        @if($restaurant->sd_percentage>0)
                                            <tr>
                                                <th>SD ({{$restaurant->sd_percentage}}%):</th>
                                                <td class="text-right">{{ $orders->sd_charge }}</td>
                                            </tr>
                                        @endif


                                        @if($restaurant->service_charge_vat_percentage>0)
                                            <tr>
                                                <th>Service Charge VAT ({{$restaurant->service_charge_vat_percentage}}%):</th>
                                                <td class="text-right">{{ $orders->service_charge_vat }}</td>
                                            </tr>
                                        @endif


                                        @if($restaurant->sd_vat_percentage>0)
                                            <tr>
                                                <th>SD VAT ({{$restaurant->sd_vat_percentage}}%):</th>
                                                <td class="text-right">{{ $orders->sd_vat }}</td>
                                            </tr>
                                        @endif



                                        <tr>
                                            <th>Discount ({{ $orders->discount_percentage }}):</th>
                                            <td class="text-right">{{ $orders->discount }}</td>
                                        </tr>



                                        <tr>
                                            <th>Total Amount: (Rounded)</th>
                                            <td class="text-right">{{ $orders->total_bill }}</td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <b>==========================================</b>

                            <h6 class="text-center">{{ $restaurant->receipt_footer }}</h6>
                            <h6 class="text-center">{{ $restaurant->receipt_company }}</h6>
                            <h6 class="text-center">{{ $restaurant->receipt_phone }}</h6>

                        </div>








                        <div class="row hidden-print">
                            <div class="col-xs-12">
                                <button class="btn btn-primary pull-right" style="margin-right: 5px;" onclick="window.print();"><i class="fa fa-print"></i> Print Receipt</button>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>


@endsection
