@extends('layouts.main')
@section('title', 'My Orders')

@section('content')


    <script src="/js/online-order.js"></script>

    <div class="row hidden-print">

        <div class="col-md-12 col-sm-12 col-xs-12 top-margin-20">
            <div class="x_panel">

                <div class="x_content">

                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr class="headings">
                                <th>Order #</th>
                                <th>Created At</th>
                                <th>Schedule At</th>
                                <th>Total Bill</th>
                                <th>Notes</th>
                                <th>Order Status</th>
                                <th class="text-center" width="20%">Order Items</th>
                                <th class="text-center" width="15%">Action</th>
                            </tr>
                            </thead>

                            <tbody>

                            @php $count=1; @endphp

                            @foreach($orders as $order)

                                @php
                                    $order_details = DB::table('online_order_details')->where('order_id', $order->order_id)->get();
                                @endphp

                                <tr id="order_{{ $order->order_id }}">
                                    <td>{{ $order->order_id }}</td>
                                    <td>{{ formatDateTime($order->created_at) }}</td>
                                    <td>{{ formatDateTime($order->scheduled_at) }}</td>
                                    <td>{{ $order->total_bill }}</td>
                                    <td>{{ $order->order_notes }}</td>
                                    <td>

                                        <button type="button" class="btn @if($order->order_status == 'placed')
                                                btn-primary
                                                @elseif($order->order_status == 'confirmed')
                                                btn-info
                                                @elseif($order->order_status == 'processing')
                                                btn-warning
                                                @elseif($order->order_status == 'completed')
                                                btn-success
                                                @elseif($order->order_status == 'cancelled')
                                                btn-danger
                                                @endif">{{ ucwords($order->order_status) }}
                                        </button>

                                    </td>

                                    <td>
                                        @foreach($order_details as $detail)
                                            <li><b>{{ $detail->menu_item_name }}: {{ $detail->item_quantity }}</b></li>
                                        @endforeach
                                    </td>
                                    <td class="text-center">
                                        <a type="button" href="/online-order/receipt/{{ $order->order_id }}" class="btn btn-primary" {{ $order->order_status == 'cancelled'?'disabled':'' }}>View Receipt</a>

                                        @if($order->order_status != 'confirmed' && $order->order_status != 'processing' && $order->order_status != 'completed' && $order->order_status != 'cancelled')
                                            <button type="button" class="btn btn-danger" onclick="changeOrderStatus('{{ $order->order_id }}', 'cancelled')">Cancel</button>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>

                        </table>

                        {{ $orders->links() }}
                    </div>


                </div>
            </div>
        </div>
    </div>

@endsection
