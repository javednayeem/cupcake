@extends('layouts.main')

@section('title', 'Online Order')

@section('content')

    <script src="/js/order.js"></script>


    <div class="row">

        @if (Auth::user()->license == 0)
            @include('cupcake.partials.expired-license')
        @else

            @foreach($setmenu_items as $item)
                <script type="application/javascript">
                    @php echo 'pushToSetMenuItemArray("'.$item->item_id.'", "'.$item->item_name.'", "'.$item->menu_item_id.'");' @endphp
                </script>
            @endforeach


            @foreach($menu_items as $item)
                <script type="application/javascript">
                    @php echo 'pushToItemArray("'.$item->menu_item_id.'", "'.$item->menu_id.'", "'.$item->item_name.'", "'.$item->ratio.'", "'.$item->price.'", "'.$item->item_discount.'", "'.$item->item_vat.'", "0", "0", "'.$item->set_menu.'", "'.$item->printer_id.'");' @endphp
                </script>
            @endforeach


            @foreach($modifiers as $modifier)
                <script type="application/javascript">
                    @php echo 'pushToModifiersArray("'.$modifier->modifier_id.'", "'.$modifier->modifier_name.'", "'.$modifier->price.'", "'.$modifier->modifier_vat.'");' @endphp
                </script>
            @endforeach


            <script type="application/javascript">
                pushChargesInfo('{{$restaurant_data->vat_no}}', '{{$restaurant_data->vat_percentage}}', '{{$restaurant_data->service_charge}}', '{{$restaurant_data->guest_bill}}', '{{$restaurant_data->price_including_vat}}' ,'{{$restaurant_data->vat_after_discount}}', '{{$restaurant_data->sd_percentage}}', '{{$restaurant_data->service_charge_vat_percentage}}', '{{$restaurant_data->sd_vat_percentage}}', '{{$restaurant_data->kitchen_type}}', '{{$restaurant_data->pay_first}}', '{{$restaurant_data->item_box_size}}');
            </script>

            @if ($restaurant_data->work_period_status == 0)

                <div class="col-md-12 col-sm-12 col-xs-12 hidden-print">
                    <div class="x_panel">
                        <div class="x_content">

                            <div class="bs-example top-margin-30" data-example-id="simple-jumbotron">
                                <div class="jumbotron">
                                    <h1>This restaurant isn't taking orders currently</h1>
                                    <p>Please Contact Restaurant Authority For Details</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            @else


                <div class="col-md-2 col-sm-2 col-xs-12 hidden-print">
                    <div class="x_panel" style="padding: 0px;">
                        <div class="x_title top-margin-10">
                            <h2 style="padding-left: 10px">Menu</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">


                            <table class="table text-center">

                                @php $color=0; @endphp

                                <div class="ranges">
                                    <ul id="">
                                        @foreach($menu_data as $menu)
                                            <li style="font-size: {{ $restaurant_data->menu_font_size }}px;background-color: {{ randomColorGenerate() }}" onclick="loadItems2('{{ $menu->menu_id }}', '{{ $menu->menu_name }}')">
                                                {{ $menu->menu_name }}
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>

                            </table>

                        </div>
                    </div>
                </div>


                <div class="col-md-6 col-sm-6 col-xs-12 hidden-print" id="item_div">
                    <div class="x_panel">

                        <div class="x_title">
                            <h2 id="menu_item_header">Menu Items</h2>
                            <div class="clearfix"></div>
                        </div>

                        <div id="item_div_span">
                        </div>
                    </div>

                </div>


                <div class="col-md-4 col-sm-4 col-xs-12 hidden-print" id="cart_div">


                    <div class="x_panel hidden-print">

                        <div class="x_content">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <td class="active">Total</td>
                                    <td class="whiteBg grand_total">0</td>
                                </tr>

                                @if($restaurant_data->vat_percentage > 0)
                                    <tr>
                                        <td class="active">VAT ({{ $restaurant_data->vat_percentage }}%)</td>
                                        <td class="whiteBg cart_vat">0</td>
                                    </tr>
                                @endif

                                @if($restaurant_data->service_charge > 0)
                                    <tr>
                                        <td class="active">Service Charge ({{ $restaurant_data->service_charge }}%)</td>
                                        <td class="whiteBg cart_service_charge">0</td>
                                    </tr>
                                @endif

                                @if($restaurant_data->sd_percentage > 0)
                                    <tr>
                                        <td class="active">SD ({{ $restaurant_data->sd_percentage }}%)</td>
                                        <td class="whiteBg cart_sd_percentage">0</td>
                                    </tr>
                                @endif

                                @if($restaurant_data->service_charge_vat_percentage > 0)
                                    <tr>
                                        <td class="active">Service Charge VAT ({{ $restaurant_data->service_charge_vat_percentage }}%)</td>
                                        <td class="whiteBg cart_service_charge_vat">0</td>
                                    </tr>
                                @endif


                                <tr>
                                    <td class="active">Grand Total</td>
                                    <td class="whiteBg"><b class="cart_net_amaount">0</b></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="text-center">
                            <td><button type="button" id="confirm_order_span" class="btn btn-primary" data-toggle="modal" data-target="#confirm_order_modal" disabled>Confirm & Place Order</button></td>
                        </div>

                    </div>

                    <div class="x_panel hidden-print">
                        <div class="x_title">
                            <h2>Cart</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li>
                                    <button type="button" class="btn btn-danger" onclick="clearCart();">
                                        Clear Cart
                                    </button>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>

                        <div class="x_content">
                            <div class="table-responsive">
                                <table class="table table-striped jambo_table bulk_action" id="cart_table">
                                    <thead>
                                    <tr class="headings">
                                        <th>#</th>
                                        <th>Item Name</th>
                                        <th>Unit Price</th>
                                        <th width="20%">Quantity</th>
                                        <th>Item Price</th>
                                        <th width="23%">Action</th>
                                    </tr>
                                    </thead>

                                    <tbody id="cart_item_div" style="height: 100px;overflow-y: auto;">

                                    </tbody>
                                </table>
                            </div>

                            {{--<div class="form-group">--}}
                            {{--<div class="col-md-12 col-sm-12 col-xs-12">--}}
                            {{--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#view_cart_modal">View Cart</button>--}}

                            {{--</div>--}}
                            {{--</div>--}}
                        </div>
                    </div>


                </div>


                <div class="clearfix"></div>




            @endif

        @endif


    </div>






    <div class="modal fade" id="add_modifier_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Add Modifier</h4>
                </div>
                <div class="modal-body">

                    <input type="hidden" id="modifier_item_id">

                    <div class="form-group">
                        <label for="course_name">Modifier Name</label>
                        <select class="form-control radius-10" id="modifier_id">

                            @foreach($modifiers as $modifier)
                                <option value="{{ $modifier->modifier_id }}">{{ $modifier->modifier_name }}</option>
                            @endforeach
                        </select>
                    </div>



                    <div class="row">

                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="form-group">
                                <label for="course_name">Quantity</label>
                                <input type="number" class="form-control" id="modifier_quantity" value="1" min="1">
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="form-group">
                                <label for="course_name">Unit Price</label>
                                <input type="text" class="form-control" id="modifier_unit_price" readonly>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="form-group">
                                <label for="course_name">Total Price</label>
                                <input type="text" class="form-control" id="modifier_total_price" readonly>
                            </div>
                        </div>

                    </div>



                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="add_modifier_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                </div>
            </div>

        </div>
    </div>


    <div class="modal fade" id="order_instruction_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Cooking Instructions</h4>
                </div>
                <div class="modal-body">

                    <input type="hidden" id="order_notes">
                    {{--<input type="hidden" id="edit_instruction_id" value="0">--}}

                    <div class="row">

                        <div class="col-md-7 col-sm-7 col-xs-7">
                            <div class="form-group">
                                <label for="instruction_id">Select Printer</label>
                                <select class="form-control radius-10" id="instruction_printer_id">

                                    @foreach($printers as $printer)
                                        <option value="{{ $printer->printer_id }}">{{ $printer->printer_name }}</option>
                                    @endforeach
                                </select>

                            </div>
                        </div>



                    </div>

                    <div class="row">

                        <div class="col-md-7 col-sm-7 col-xs-7">
                            <div class="form-group">
                                <label for="instruction_id">Instruction</label>
                                <select class="form-control radius-10" id="instruction_id">

                                    @foreach($order_instructions as $instruction)
                                        <option value="{{ $instruction->instruction_id }}">{{ $instruction->instruction }}</option>
                                    @endforeach
                                </select>

                            </div>
                        </div>

                        <div class="col-md-3 col-sm-3 col-xs-3" style="margin-top: 24px;">
                            <div class="form-group">
                                <button type="button" id="add_instruction_button" class="btn btn-success">Add Instruction</button>
                            </div>
                        </div>

                    </div>


                    <div class="row">

                        <div class="col-md-7 col-sm-7 col-xs-7">
                            <div class="form-group">
                                <label for="course_name">Text Instruction</label>
                                <textarea class="form-control radius-5" id="textarea_instruction"></textarea>
                            </div>
                        </div>



                        <div class="col-md-3 col-sm-3 col-xs-3 top-margin-30">
                            <div class="form-group">
                                <button type="button" id="textarea_instruction_button" class="btn btn-success">Add Instruction</button>
                            </div>
                        </div>

                    </div>


                    <div class="row top-margin-40">
                        <div class="x_content">
                            <div class="">
                                <table class="table table-striped jambo_table">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Instruction</th>
                                        <th width="20%">Action</th>
                                    </tr>
                                    </thead>

                                    <tbody id="instruction_table">



                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
                </div>
            </div>

        </div>
    </div>


    <div class="modal fade" id="item_quantity_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Item Quantity</h4>
                </div>

                <div class="modal-body">

                    <input type="hidden" id="item_id">

                    <div class="form-group">
                        <label for="username">Item Quantity</label>
                        <input type="number" class="form-control" id="item_amount">
                    </div>

                </div>




                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="set_item_quantity" class="btn btn-success" data-dismiss="modal">Ok</button>
                </div>
            </div>

        </div>
    </div>


    <div class="modal fade" id="add_item_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Open Food Item Add</h4>
                </div>

                <div class="modal-body">

                    <input type="hidden" id="open_food_menu_id">

                    <div class="form-group">
                        <label for="username">Item Name</label>
                        <input type="text" class="form-control" id="open_food_item_name">
                    </div>

                    <div class="form-group">
                        <label for="username">Price</label>
                        <input type="text" class="form-control" id="open_food_price">
                    </div>


                    <div class="form-group">
                        <label for="username">Item VAT</label>
                        <input type="text" class="form-control" id="open_food_item_vat">
                    </div>


                    <div class="form-group">
                        <label for="username">Select Printer</label>
                        <select class="form-control" id="open_food_printer_id">
                            @foreach($printers as $printer)
                                <option value="{{ $printer->printer_id }}">{{ $printer->printer_name }}</option>
                            @endforeach
                        </select>
                    </div>

                </div>




                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="add_item_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                </div>
            </div>

        </div>
    </div>


    <div class="modal fade" id="confirm_order_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Confirm Order</h4>
                </div>
                <div class="modal-body">

                    @php date_default_timezone_set("Asia/Dhaka") @endphp

                    <div class="form-group">
                        <label for="username">Schedule Date</label>
                        <input type="text" id="basic-datepicker" class="form-control" value="{{ date('Y-m-d') }}" disabled>
                    </div>


                    <div class="form-group">
                        <label for="username">Pickup Time</label>
                        <input type="text" id="24hours-timepicker" class="form-control" value="{{ date('H:i') }}">
                    </div>

                    <div class="form-group">
                        <label for="username">Order Notes</label>
                        <textarea class="form-control" id="online_order_notes"></textarea>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="confirm_order_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                </div>
            </div>

        </div>
    </div>



@endsection
