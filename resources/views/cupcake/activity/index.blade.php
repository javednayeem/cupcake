@extends('layouts.main')

@section('title', 'Activity')

@section('content')

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Users Activity</h2>
                <ul class="nav navbar-right panel_toolbox col-md-3 col-sm-3 col-xs-3">
                    <div class="input-group">
                        <input type="text" class="form-control">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-primary">Search Activity</button>
                        </span>
                    </div>


                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <ul class="list-unstyled timeline">

                    @foreach($activity_data as $activity)
                        <li>
                            <div class="block">
                                <div class="tags">
                                    <a href="" class="tag">
                                        <span>{{ ucfirst($activity->lookup_activity_string) }}d</span>
                                    </a>
                                </div>
                                <div class="block_content">
                                    <h2 class="title">
                                        <a>{{ ucfirst($activity->lookup_category_string) }} : {{ getLookup($activity->lookup_category_string, $activity->lookup_name_id) }}</a>
                                    </h2>
                                    <div class="byline">
                                        <span>{{ date('F j, Y h:m:s A', strtotime( $activity->created_at )) }}</span> by <a>{{ $activity->username }}</a>
                                    </div>
                                    </p>
                                </div>
                            </div>
                        </li>
                    @endforeach

                </ul>

            </div>
        </div>
    </div>


@endsection
