@extends('layouts.main')

@section('title', 'Stores')

@section('content')

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">

                    <ul class="nav navbar-right panel_toolbox">
                        <li><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_store_modal">Add Store</button></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">

                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                            <tr class="headings">
                                <th class="column-title">#</th>
                                <th class="column-title">Store Name</th>
                                <th class="column-title">Address</th>
                                <th class="column-title">Phone No.</th>
                                <th class="column-title">Email</th>
                                <th class="column-title">Created At</th>
                                <th class="column-title no-link last" width="11%">Action</th>
                            </tr>
                            </thead>

                            <tbody id="store_table">

                            @php $store_count=1; @endphp

                            @foreach($stores as $store)

                                <tr class="even" id="store_{{ $store->store_id }}">
                                    <td> {{ $store_count++ }}</td>
                                    <td id="store_name_{{ $store->store_id }}">{{ $store->store_name }}</td>
                                    <td id="store_address_{{ $store->store_id }}">{{ $store->store_address }}</td>
                                    <td id="store_phone_{{ $store->store_id }}">{{ $store->store_phone }}</td>
                                    <td id="store_email_{{ $store->store_id }}">{{ $store->store_email }}</td>
                                    <td>{{ date('F j, Y', strtotime( $store->created_at )) }}</td>
                                    <td>
                                        <button type="button" class="btn btn-dark" onclick="edit_store('{{ $store->store_id }}')">
                                            <i class="glyphicon glyphicon-pencil"></i>
                                        </button>

                                        <button type="button" class="btn btn-danger" onclick="delete_store('{{ $store->store_id }}')">
                                            <i class="glyphicon glyphicon-remove"></i>
                                        </button>
                                    </td>
                                </tr>

                            @endforeach

                            </tbody>
                        </table>
                        {{ $stores->links() }}
                    </div>


                </div>
            </div>
        </div>

    </div>



    <div class="modal fade" id="add_store_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Add Store</h4>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label>Store Name</label>
                        <input type="text" class="form-control" id="store_name">
                    </div>


                    <div class="form-group">
                        <label>Address</label>
                        <input type="text" class="form-control" id="store_address">
                    </div>

                    <div class="form-group">
                        <label>Phone</label>
                        <input type="text" class="form-control" id="store_phone">
                    </div>

                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" class="form-control" id="store_email">
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="add_store_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                </div>
            </div>

        </div>
    </div>


    <div class="modal fade" id="edit_store_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Add Store</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="edit_store_id">
                    <div class="form-group">
                        <label>Store Name</label>
                        <input type="text" class="form-control" id="edit_store_name">
                    </div>


                    <div class="form-group">
                        <label>Address</label>
                        <input type="text" class="form-control" id="edit_store_address">
                    </div>

                    <div class="form-group">
                        <label>Phone</label>
                        <input type="text" class="form-control" id="edit_store_phone">
                    </div>

                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" class="form-control" id="edit_store_email">
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="edit_store_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                </div>
            </div>

        </div>
    </div>




    <script src="/js/inventory.js"></script>

@endsection
