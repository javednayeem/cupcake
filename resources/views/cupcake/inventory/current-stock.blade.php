@extends('layouts.main')

@section('title', 'Current Stock')

@section('content')

    <script src="/js/inventory.js"></script>
    <link href="/css/report.css" rel="stylesheet">


    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12 top-margin-30">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Inventory Report Generation</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br>
                    <div class="form-horizontal form-label-left">


                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4 col-xs-12">Select Report Type</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control" id="select_report_type">
                                    <option value="0">Choose option</option>
                                    <option value="category_wise_report">Category Wise Report</option>
                                    <option value="product_wise_report">Product Wise Report</option>
                                </select>
                            </div>
                        </div>


                        <div class="form-group div-hide" id="select_category_div">
                            <label class="control-label col-md-4 col-sm-4 col-xs-12">Category</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control" id="category_id">
                                    <option value="0">All Categories</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->category_id }}">{{ $category->category_name }} ({{ $category->product_count!=Null?$category->product_count:0 }} Products)</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="form-group div-hide" id="select_product_div">
                            <label class="control-label col-md-4 col-sm-4 col-xs-12">Products</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control" id="product_id">
                                    <option value="0">All Products</option>
                                    @foreach($inv_products as $product)
                                        <option value="{{ $product->product_id }}">{{ $product->product_name }} ({{ $product->product_code }})</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">
                                <button class="btn btn-success" id="generate_report_button">Generate Report</button>
                            </div>
                        </div>

                        <div id="printDiv"></div>

                    </div>
                </div>
            </div>
        </div>


        <div class="col-md-12 col-sm-12 col-xs-12" id="report_div">
            <div class="x_panel">

                <div class="x_content form-group">
                    <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="tile-stats text-center">
                            <img src="/images/restaurant-logo/{{ getRstaurantLogo() }}" width="5%">
                            <div class="count">{{ $restaurant_data->restaurants_name==''? "Cupcake":$restaurant_data->restaurants_name }}</div>
                            <h3>
                                <b>
                                    {{ $restaurant_data->address}}
                                    {{ $restaurant_data->area==null? '':', '.$restaurant_data->area }}
                                    {{ $restaurant_data->city==null? '':', '.$restaurant_data->city }}
                                </b>
                            </h3>
                            <div class="count" id="report_header">Report</div>
                            <h3 id="date_header"></h3>
                            <div class="form-group text-center top-margin-20 ">
                                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 20px">
                                    <button class="btn btn-primary hidden-print" onclick="window.print()">Print Report</button>
                                    <button class="btn btn-dark hidden-print" id="email_current_stock">Email Report</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div >
                    <h2><strong>Total Stock Price: <span id="t_stock_price">0</span> &#2547;</strong></h2>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table top-margin-20" id="inv_product_store_table">
                            <thead>
                            <tr class="headings">
                                <th class="column-title">Code</th>
                                <th class="column-title">Product Name</th>
                                <th class="column-title">Category</th>
                                <th class="column-title">Quantity</th>
                                <th class="column-title">Consumption</th>
                                <th class="column-title">Damage</th>
                                <th class="column-title">Actual Stock</th>
                                <th class="column-title">Stock Price</th>
                            </tr>
                            </thead>

                            <tbody id="current_stock_table">

                            @php $t_stock_price=0; @endphp

                            @foreach($products as $product)

                                @php $quantity = floatval($product->quantity) @endphp
                                @php $consumption_quantity = floatval($product->consumption_quantity) @endphp
                                @php $damage_quantity = floatval($product->damage_quantity) @endphp
                                @php $actual_stock = $quantity - ($damage_quantity) @endphp
                                @php $t_stock_price+= ($actual_stock * $product->avg_price) @endphp

                                {{--<h1>quantity: {{ $quantity }}</h1>--}}
                                {{--<h1>damage_quantity: {{ $damage_quantity }}</h1>--}}
                                {{--<h1>consumption_quantity: {{ $consumption_quantity }}</h1>--}}
                                {{--<h1>actual_stock: {{ $actual_stock }}</h1>--}}

                                <tr>
                                    <td>{{ $product->product_code }}</td>
                                    <td>{{ $product->product_name }}</td>
                                    <td>{{ $product->category_name }}</td>
                                    <td>{{ $product->quantity . ' ' . $product->unit_name}}</td>
                                    <td>{{ $consumption_quantity==null?0:$consumption_quantity}} {{ $product->unit_name }}</td>
                                    <td>{{ $damage_quantity==null?0:$damage_quantity}} {{ $product->unit_name }}</td>
                                    <td>{{ $actual_stock . ' ' . $product->unit_name}}</td>
                                    <td>{{ $actual_stock * $product->avg_price }} &#2547;</td>
                                </tr>

                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>

                <script type="application/javascript">
                    <?php echo 'setStockValue('.$t_stock_price.');'; ?>
                </script>

            </div>
        </div>
    </div>


@endsection
