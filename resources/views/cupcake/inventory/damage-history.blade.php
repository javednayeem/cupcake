@extends('layouts.main')

@section('title', 'Damage History')

@section('content')

    <script src="/js/inventory.js"></script>
    <link href="/css/report.css" rel="stylesheet">

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 top-margin-30">
            <div class="x_panel">

                <div class="x_content">
                    <br>
                    <div class="form-horizontal form-label-left">


                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4 col-xs-12">Select Report Type</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control" id="select_report_type">
                                    <option value="0">All Report</option>
                                    <option value="category_wise_report">Category Wise Report</option>
                                    <option value="product_wise_report">Product Wise Report</option>
                                </select>
                            </div>
                        </div>


                        <div class="form-group div-hide" id="select_category_div">
                            <label class="control-label col-md-4 col-sm-4 col-xs-12">Category</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control" id="category_id">
                                    <option value="0">All Categories</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->category_id }}">{{ $category->category_name }} ({{ $category->product_count!=Null?$category->product_count:0 }} Products)</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="form-group div-hide" id="select_product_div">
                            <label class="control-label col-md-4 col-sm-4 col-xs-12">Products</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control" id="product_id">
                                    <option value="0">All Products</option>
                                    @foreach($inv_products as $product)
                                        <option value="{{ $product->product_id }}">{{ $product->product_name }} ({{ $product->product_code }})</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="form-group top-margin-20">
                            <label class="control-label col-md-4 col-sm-4 col-xs-12">Start Date</label>
                            <div class="col-md-6 col-sm-6 col-xs-12 input-group date" id='myDatepicker2'>
                                <input type='text' class="form-control" id="start_date" value="{{ date("Y-m-01") }}"/>
                                <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4 col-xs-12">End Date</label>
                            <div class="col-md-6 col-sm-6 col-xs-12 input-group date" id='myDatepicker3'>
                                <input type='text' class="form-control" id="end_date" value="{{ date("Y-m-d") }}"/>
                                <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">
                                <button class="btn btn-success" id="generate_damage_history">Generate Report</button>
                            </div>
                        </div>

                        <div id="printDiv"></div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12 top-margin-30" id="report_div">
            <div class="x_panel">

                <div class="x_content form-group">
                    <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="tile-stats text-center">
                            <img src="/images/restaurant-logo/{{ getRstaurantLogo() }}" width="5%">
                            <div class="count">{{ $restaurant_data->restaurants_name==''? "Cupcake":$restaurant_data->restaurants_name }}</div>
                            <h3>
                                <b>
                                    {{ $restaurant_data->address}}
                                    {{ $restaurant_data->area==null? '':', '.$restaurant_data->area }}
                                    {{ $restaurant_data->city==null? '':', '.$restaurant_data->city }}
                                </b>
                            </h3>
                            <div class="count" id="report_header">All Damage Report</div>
                            <h3 id="date_header"></h3>
                            <div class="form-group text-center top-margin-20 ">
                                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 20px">
                                    <button class="btn btn-primary hidden-print" onclick="window.print()">Print Report</button>
                                    <button class="btn btn-dark hidden-print" id="email_damage_history">Email Report</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table top-margin-20">
                            <thead>
                            <tr class="headings">
                                <th class="column-title">#</th>
                                <th class="column-title">Category</th>
                                <th class="column-title">Product</th>
                                <th class="column-title">Quantity</th>
                                <th class="column-title">Unit Price</th>
                                <th class="column-title">Grand Total</th>
                                <th class="column-title">Damaged Date</th>
                            </tr>
                            </thead>

                            <tbody id="damage_history_table">

                            @php $damage_count=1; @endphp
                            @php $total_quantity=0; @endphp
                            @php $total=0; @endphp

                            @foreach($damages as $damage)

                                @php $grand_total = ($damage->quantity * $damage->unit_price); @endphp
                                @php $total_quantity += $damage->quantity; @endphp
                                @php $total += $grand_total; @endphp

                                <tr class="pointer">
                                    <td>{{ $damage_count++ }}</td>
                                    <td>{{ $damage->category_name }}</td>
                                    <td>{{ $damage->product_name }}</td>
                                    <td>{{ $damage->quantity }} {{ $damage->unit_name }}</td>
                                    <td>{{ $damage->unit_price }}&#2547;</td>
                                    <td>{{ $grand_total }}&#2547;</td>
                                    <td>{{ date('F j, Y', strtotime( $damage->created_at )) }}</td>
                                </tr>
                            @endforeach

                            <tr class="even pointer">
                                <td></td>
                                <th>Total</th>
                                <td></td>
                                <th>{{ $total_quantity }}</th>
                                <td></td>
                                <th>{{ $total }}&#2547;</th>
                                <td></td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="damage_details_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                </div>
                <div class="modal-body" id="order_details_modal_print">

                    <div class="x_title text-center">
                        <img src="/images/restaurant-logo/{{ $restaurant_data->restaurant_img }}" id="logo-icon" >
                        <h4>{{ $restaurant_data->restaurants_name==''? "Cupcake":$restaurant_data->restaurants_name }}</h4>
                        <h5>Address :
                            {{ $restaurant_data->address}}
                            {{ $restaurant_data->area==null? '':', '.$restaurant_data->area }}
                            {{ $restaurant_data->city==null? '':', '.$restaurant_data->city }}
                        </h5>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_title form-group">
                        <h5>Date : <a id="m_damage_date"></a></h5>
                        <h5>From : <a id="m_from"></a></h5>
                        <h5>Created By : <a id="m_created_by"></a></h5>
                    </div>


                    <div class="x_title table-responsive">
                        <table class="table">
                            <thead>

                            <tr>
                                <th width="5%">#</th>
                                <th width="50%">Item Name</th>
                                <th>Qty</th>
                                <th>U.Cost</th>
                                <th>T.Cost</th>
                            </tr>

                            </thead>
                            <tbody id="modal_damage_table">

                            </tbody>
                        </table>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-success" data-dismiss="modal" onclick="window.print();">Print</button>
                </div>
            </div>

        </div>
    </div>







@endsection
