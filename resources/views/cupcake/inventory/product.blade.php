@extends('layouts.main')

@section('title', 'Products')

@section('content')

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12 top-margin-30">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Product List</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><button id="addNewProd" type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_product_modal">Add Product</button></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">

                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action" id="inventory_products_table">
                            <thead>
                            <tr class="headings">
                                <th class="column-title">#</th>
                                <th class="column-title">Product Code</th>
                                <th class="column-title">Product Name</th>
                                <th class="column-title">Product Category</th>
                                <th class="column-title">Unit</th>
                                <th class="column-title">Fixed Price</th>
                                <th class="column-title">Avg Price</th>
                                <th class="column-title">Created At</th>
                                <th class="column-title no-link last" width="12%">Action</th>
                            </tr>
                            </thead>

                            @php $product_count=1; @endphp

                            <tbody id="product_table">

                            @foreach($products as $product)

                                <tr class="even" id="product_{{ $product->product_id }}">
                                    <td>{{ $product_count++ }}</td>
                                    <td>{{ $product->product_code }}</td>
                                    <td id="product_name_{{ $product->product_id }}">{{ $product->product_name }}</td>
                                    <td id="category_name_{{ $product->product_id }}">{{ $product->category_name }}</td>
                                    <td id="unit_name_{{ $product->product_id }}">{{ $product->unit_name }}</td>
                                    <td>{{ $product->fixed_price }} &#2547;</td>
                                    <td>{{ $product->avg_price }} &#2547;</td>
                                    <td>{{ date('F j, Y', strtotime( $product->created_at )) }}</td>
                                    <td>
                                        <button type="button" class="btn btn-dark" onclick="editProduct('{{ $product->product_id }}', '{{ $product->product_code }}', '{{ $product->product_name }}', '{{ $product->unit_id }}', '{{ $product->category_id }}', '{{ $product->fixed_price }}', '{{ $product->low_inv_alert }}')">
                                            <i class="glyphicon glyphicon-pencil"></i>
                                        </button>

                                        <button type="button" class="btn btn-danger" onclick="deleteProduct('{{ $product->product_id }}')">
                                            <i class="glyphicon glyphicon-remove"></i>
                                        </button>
                                    </td>
                                </tr>

                                <input type="hidden" id="unit_id_{{ $product->product_id }}" value="{{ $product->unit_id }}">
                                <input type="hidden" id="category_id_{{ $product->product_id }}" value="{{ $product->category_id }}">


                            @endforeach

                            </tbody>
                        </table>
                        {{--{{ $products->links() }}--}}
                    </div>


                </div>
            </div>
        </div>

    </div>


    <div class="modal fade" id="add_product_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Add Product</h4>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label>Category</label>
                        <select class="form-control" id="category_id">
                            @foreach($product_category as $category)
                                <option value="{{ $category->category_id }}">{{ $category->category_name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Product Name</label>
                        <input type="text" class="form-control" id="product_name" placeholder="">
                    </div>

                    <div class="form-group">
                        <label>Product Code</label>
                        <input type="text" class="form-control" id="product_code">
                    </div>

                    <div class="form-group">
                        <label>Product Price</label>
                        <input type="number" class="form-control" id="fixed_price">
                    </div>

                    <div class="form-group">
                        <label>Low Inventory Alert</label>
                        <input type="number" class="form-control" id="low_inv_alert" value="5">
                    </div>

                    <div class="form-group">
                        <label>Unit</label>
                        <select class="form-control" id="unit_id">
                            @foreach($units as $unit)
                                <option value="{{ $unit->unit_id }}">{{ $unit->unit_name }}</option>
                            @endforeach
                        </select>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="add_product_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                </div>
            </div>

        </div>
    </div>


    <div class="modal fade" id="edit_product_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Edit Product</h4>
                </div>
                <div class="modal-body">

                    <input type="hidden" id="edit_product_id">

                    <div class="form-group">
                        <label>Category</label>
                        <select class="form-control" id="edit_category_id">
                            @foreach($product_category as $category)
                                <option value="{{ $category->category_id }}">{{ $category->category_name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Product Name</label>
                        <input type="text" class="form-control" id="edit_product_name">
                    </div>

                    <div class="form-group">
                        <label>Product Code</label>
                        <input type="text" class="form-control" id="edit_product_code">
                    </div>

                    <div class="form-group">
                        <label>Product Price</label>
                        <input type="number" class="form-control" id="edit_fixed_price">
                    </div>

                    <div class="form-group">
                        <label>Low Inventory Alert</label>
                        <input type="number" class="form-control" id="edit_low_inv_alert">
                    </div>

                    <div class="form-group">
                        <label>Unit</label>
                        <select class="form-control" id="edit_unit_id">
                            @foreach($units as $unit)
                                <option value="{{ $unit->unit_id }}">{{ $unit->unit_name }}</option>
                            @endforeach
                        </select>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="edit_product_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                </div>
            </div>

        </div>
    </div>





    <script src="/js/inventory.js"></script>

@endsection
