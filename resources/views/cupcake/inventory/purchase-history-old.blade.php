@extends('layouts.main')

@section('title', 'Purchase History')

@section('content')

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12 top-margin-30">
            <div class="x_panel">
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table top-margin-20">
                            <thead>
                            <tr class="headings">
                                <th class="column-title">#</th>
                                <th class="column-title">Invoice No</th>
                                <th class="column-title">Supplier</th>
                                <th class="column-title">Purchase Date</th>
                                <th class="column-title">Delivery Date</th>
                                <th class="column-title">Sub Total</th>
                                <th class="column-title">Discount</th>
                                <th class="column-title">Shipping</th>
                                <th class="column-title">Grand Total</th>
                                <th class="column-title">Created By</th>
                                <th class="column-title">Created At</th>
                            </tr>
                            </thead>

                            <tbody id="supplier_table">

                            @php $purchase_count=1; @endphp

                            @foreach($purchase_data as $purchase)
                                <tr class="even pointer" id="supplier_{{ $purchase->invoice_id }}" onclick="viewPurchaseDetails('{{ $purchase->invoice_id }}', '{{ $purchase->invoice_no }}', '{{ $purchase->supplier_name }}', '{{ date('d/m/Y', strtotime( $purchase->purchase_date )) }}', '{{ date('d/m/Y', strtotime( $purchase->delivery_date )) }}', '{{ $purchase->sub_total }}', '{{ $purchase->discount }}', '{{ $purchase->shipping }}', '{{ $purchase->grand_total }}')">
                                    <td> {{ $purchase_count++ }}</td>
                                    <td>{{ $purchase->invoice_no }}</td>
                                    <td>{{ $purchase->supplier_name }}</td>
                                    <td>{{ date('d/m/Y', strtotime( $purchase->purchase_date )) }}</td>
                                    <td>{{ date('d/m/Y', strtotime( $purchase->delivery_date )) }}</td>
                                    <td>{{ $purchase->sub_total }}</td>
                                    <td>{{ $purchase->discount }}</td>
                                    <td>{{ $purchase->shipping }}</td>
                                    <td>{{ $purchase->grand_total }}</td>
                                    <td>{{ $purchase->username }}</td>
                                    <td>{{ date('F j, Y', strtotime( $purchase->created_at )) }}</td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!--Purchase Details Modal -->
    <div class="modal fade" id="purchase_details_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                </div>
                <div class="modal-body">

                    <div class="x_title text-center">
                        <h4>{{ getRstaurantName() }}</h4>
                        <h5>test address</h5>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_title form-group font-bold">
                        <h4>Invoice No : <a id="modal_invoice_no"></a></h4>
                        <h4>Supplier : <a id="modal_supplier_name"></a></h4>
                        <h4>Purchase Date : <a id="modal_purchase_date"></a></h4>
                        <h4>Delivery Date : <a id="modal_delivery_date"></a></h4>
                    </div>


                    <div class="x_title table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th width="5%">#</th>
                                <th width="50%">Item Name</th>
                                <th>U.Price</th>
                                <th>Qty</th>
                                <th>Total</th>
                            </tr>
                            </thead>

                            <tbody id="purchase_details_table">



                            </tbody>
                        </table>
                    </div>

                    <div class="x_title form-group font-bold">
                        <a>Sub Total : <a id="modal_sub_total">0</a></a><br>
                        <a>Discount :<a id="modal_discount">0</a></a><br>
                        <a>Shipping : <a id="modal_shipping">0</a></a><br>
                        <p>Grand Total : <a id="modal_grand_total">0</a></p>
                    </div>



                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="confirm_payment_button" class="btn btn-success" data-dismiss="modal">OK</button>
                </div>
            </div>

        </div>
    </div>


    <script src="/js/inventory.js"></script>
@endsection
