@extends('layouts.main')

@section('title', 'Product Unit')

@section('content')

    <div class="row top-margin-30">

        <div class="col-md-6 col-sm-6 col-xs-6">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Units</h2>

                    <ul class="nav navbar-right panel_toolbox">
                        <li><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_unit_modal">Add Unit</button></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">

                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                            <tr class="headings">
                                <th class="column-title">#</th>
                                <th class="column-title">Unit Name</th>
                                <th class="column-title">Created At</th>
                                <th class="column-title no-link last" width="23%">Action
                                </th>
                            </tr>
                            </thead>

                            <tbody id="unit_table">
                            @php $unit_count=1; @endphp

                            @foreach($units as $unit)
                                <tr class="even pointer" id="unit_{{ $unit->unit_id }}">
                                    <td> {{ $unit_count++ }}</td>
                                    <td id="unit_name_{{ $unit->unit_id }}">{{ $unit->unit_name }}</td>
                                    <td>{{ date('F j, Y', strtotime( $unit->created_at )) }}</td>
                                    <td>
                                        <button type="button" class="btn btn-dark" onclick="editUnit('{{ $unit->unit_id }}')">
                                            <i class="glyphicon glyphicon-pencil"></i>
                                        </button>

                                        <button type="button" class="btn btn-danger" onclick="deleteUnit('{{ $unit->unit_id }}')">
                                            <i class="glyphicon glyphicon-remove"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>


                </div>
            </div>
        </div>


        <div class="col-md-6 col-sm-6 col-xs-6">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Unit Conversion Rate</h2>

                    <ul class="nav navbar-right panel_toolbox">
                        <li><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_conversion_modal">Add Unit Conversion</button></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">

                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                            <tr class="headings">
                                <th class="column-title">From</th>
                                <th class="column-title">To</th>
                                <th class="column-title">Rate</th>
                                <th class="column-title no-link last" width="23%">Action</th>
                            </tr>
                            </thead>

                            <tbody id="unit_conversion_table">
                            @php $unit_count=1; @endphp

                            @foreach($unit_conversion as $conversion)
                                <tr class="even pointer" id="unit_{{ $conversion->conversion_id }}">
                                    <td>{{ $conversion->from_unit_name }}</td>
                                    <td>{{ $conversion->to_unit_name }}</td>
                                    <td>1 {{ $conversion->from_unit_name }} = {{ $conversion->conversion_rate . ' ' .$conversion->to_unit_name }}</td>
                                    <td>
                                        <button type="button" class="btn btn-dark">
                                            <i class="glyphicon glyphicon-pencil"></i>
                                        </button>

                                        <button type="button" class="btn btn-danger">
                                            <i class="glyphicon glyphicon-remove"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>


                </div>
            </div>
        </div>

    </div>


    <div class="modal fade" id="add_unit_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Add Unit</h4>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label>Unit Name</label>
                        <input type="text" class="form-control" id="modal_unit_name" placeholder="">
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="add_unit_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                </div>
            </div>

        </div>
    </div>


    <div class="modal fade" id="edit_unit_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Add Unit</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="edit_unit_id">
                    <div class="form-group">
                        <label>Unit Name</label>
                        <input type="text" class="form-control" id="edit_unit_name">
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="edit_unit_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                </div>
            </div>

        </div>
    </div>


    <div class="modal fade" id="add_conversion_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Add Unit Conversion</h4>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label>From Unit</label>
                        <select class="form-control" id="from_unit_id">
                            @foreach($units as $unit)
                                <option value="{{ $unit->unit_id }}">{{ $unit->unit_name }}</option>
                            @endforeach
                        </select>
                    </div>


                    <div class="form-group">
                        <label>To Unit</label>
                        <select class="form-control" id="to_unit_id">
                            @foreach($units as $unit)
                                <option value="{{ $unit->unit_id }}">{{ $unit->unit_name }}</option>
                            @endforeach
                        </select>
                    </div>


                    <div class="form-group">
                        <label>Conversion Rate</label>
                        <input type="text" class="form-control" id="conversion_rate">
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="add_conversion_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                </div>
            </div>

        </div>
    </div>


    <script src="/js/inventory.js"></script>

@endsection
