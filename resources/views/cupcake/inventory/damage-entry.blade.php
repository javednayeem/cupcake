@extends('layouts.main')

@section('title', 'Damage Entry')

@section('content')

    <script src="/js/inventory.js"></script>


    @foreach($product_data as $product)
        <script type="application/javascript">
            @php
                echo 'pushToProductArray("'.$product->product_id.'", "'.$product->product_name.'", "'.$product->quantity.'", "'.$product->unit_id.'", "'.$product->category_id.'", "'.$product->avg_price.'");'
            @endphp
        </script>
    @endforeach

    @foreach($units as $unit)
        <script type="application/javascript">
            <?php echo 'pushToUnitArray("'.$unit->unit_id.'", "'.$unit->unit_name.'");'; ?>
        </script>
    @endforeach


    <input type="hidden" id="max_product_quantity" value="0">
    <input type="hidden" id="product_unit_name" value="0">
    <input type="hidden" id="main_store_id" value="0">


    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 top-margin-30">
            <div class="x_panel">
                <div class="">
                    <h2>Store Information</h2>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">

                    <div class="top-margin-20">

                        <div class="col-md-3 col-sm-3 col-xs-12 form-group">
                            <label for="store_id">Store</label>
                            <select id="store_id" class="form-control">
                                @foreach($stores as $store)
                                    <option value="{{ $store->store_id }}">{{ $store->store_name }}</option>
                                @endforeach
                            </select>
                        </div>


                    </div>


                </div>

            </div>
        </div>
    </div>


    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="">
                    <h2>Product Information</h2>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">

                    <div class="top-margin-20" id="transfer_product_info_div">
                        <div class="col-md-3 col-sm-3 col-xs-12 form-group">
                            <label for="product_category">Product Category</label>
                            <select id="product_category" class="form-control">
                                <option value="0">Select Category</option>
                                @foreach($product_category as $category)
                                    <option value="{{ $category->category_id }}">{{ $category->category_name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-md-3 col-sm-3 col-xs-12 form-group" id="select_product_div">
                            <label for="product">Product Name</label>
                            <select id="product" class="form-control">

                            </select>
                        </div>


                        <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                            <label for="quantity">Quantity <a class="red-text" id="product_quantity_span"></a></label>
                            <input type="number" id="quantity" class="form-control" />
                            <span class="form-control-feedback right" aria-hidden="true" id="quantity_span"></span>
                        </div>

                        <div class="col-md-1 col-sm-1 col-xs-12 form-group">
                            <label for="email">&nbsp;</label>
                            <button class="btn btn-primary" id="add_product_transfer_button">Add Product To Damage Entry</button>
                        </div>
                    </div>


                </div>

            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="">
                    <h2>Damage Details</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <section class="content invoice">


                        <div class="row top-margin-30">
                            <div class="col-xs-12 table">
                                <table class="table table-striped jambo_table bulk_action" id="invoice_table">
                                    <thead >
                                    <tr>
                                        <th>#</th>
                                        <th>Product Category</th>
                                        <th width="40%">Product Name</th>
                                        <th>Quantity</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>

                                    <tbody id="transfer_table_tbody">


                                    </tbody>
                                </table>
                            </div>

                        </div>



                        <div class="row no-print">
                            <div class="col-xs-12">
                                {{--<button class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i> Print</button>--}}
                                <button class="btn btn-success pull-right" id="submit_damage_button">
                                    <i class="fa fa-credit-card"></i>
                                    Submit Damage Entry
                                </button>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>





@endsection
