@extends('layouts.main')

@section('title', 'Product Category')

@section('content')

    <div class="row top-margin-30">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Category</h2>

                    <ul class="nav navbar-right panel_toolbox">
                        <li><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_category_modal">Add Category</button></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">

                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                            <tr class="headings">
                                <th class="column-title">#</th>
                                <th class="column-title">Category Name</th>
                                <th class="column-title">Created At</th>
                                <th class="column-title no-link last" width="11%">Action</th>
                            </tr>
                            </thead>

                            <tbody id="category_table">

                            @php $category_count=1; @endphp

                            @foreach($product_category as $category)
                                <tr class="even" id="category_{{ $category->category_id }}">
                                    <td> {{ $category_count++ }}</td>
                                    <td id="category_name_{{ $category->category_id }}">{{ $category->category_name }}</td>
                                    <td>{{ date('F j, Y', strtotime( $category->created_at )) }}</td>
                                    <td>
                                        <button type="button" class="btn btn-dark" onclick="editCategory('{{ $category->category_id }}')">
                                            <i class="glyphicon glyphicon-pencil"></i>
                                        </button>

                                        <button type="button" class="btn btn-danger" onclick="deleteCategory({{ $category->category_id }})">
                                            <i class="glyphicon glyphicon-remove"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        {{ $product_category->links() }}
                    </div>


                </div>
            </div>
        </div>

    </div>


    <div class="modal fade" id="add_category_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Add Category</h4>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label>Category Name</label>
                        <input type="text" class="form-control" id="modal_category_name" placeholder="">
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="add_category_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                </div>
            </div>

        </div>
    </div>


    <div class="modal fade" id="edit_category_modal" role="dialog" style="margin-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Add Category</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="edit_category_id">
                    <div class="form-group">
                        <label>Category Name</label>
                        <input type="text" class="form-control" id="edit_category_name">
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="edit_category_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                </div>
            </div>

        </div>
    </div>


    <script src="/js/inventory.js"></script>

@endsection
