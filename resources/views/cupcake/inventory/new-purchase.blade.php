@extends('layouts.main')

@section('title', 'New Purchase')

@section('content')

    <script src="/js/inventory.js"></script>

    @foreach($product_data as $product)
        <script type="application/javascript">
            <?php echo 'pushToProductArray("'.$product->product_id.'", "'.$product->product_name.'", "'.$product->status.'", "'.$product->unit_id.'", "'.$product->category_id.'");'; ?>
        </script>
    @endforeach

    @foreach($units as $unit)
        <script type="application/javascript">
            <?php echo 'pushToUnitArray("'.$unit->unit_id.'", "'.$unit->unit_name.'");'; ?>
        </script>
    @endforeach

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Supplier Information</h2>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">

                    <input type="hidden" id="pay_type" value="payable">

                    <div data-parsley-validate>
                        <div class="col-md-3 col-sm-3 col-xs-12 form-group">
                            <label for="invoice_no">Invoice No:</label>
                            <input type="text" id="invoice_no" class="form-control" value="{{ licenseKeyGenerate() }}">
                        </div>

                        <div class="col-md-3 col-sm-3 col-xs-12 form-group">
                            <label for="supplier">Supplier:</label>
                            <select id="supplier" class="form-control">
                                <option value="0">Choose Supplier</option>
                                @foreach($supplier_data as $supplier)
                                    <option value="{{ $supplier->supplier_id }}">{{ $supplier->supplier_name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-md-2 col-sm-2 col-xs-12 form-group" id="myDatepicker2" >
                            <label for="purchase_date">Purchase Date:</label>
                            <input type="date" id="purchase_date" class="form-control" value="{{ date('Y-m-d') }}"/>
                        </div>

                        <div class="col-md-2 col-sm-2 col-xs-12 form-group">
                            <label for="delivery_date">Delivery Date:</label>
                            <input type="date" id="delivery_date" class="form-control" value="{{ date('Y-m-d') }}"/>
                        </div>

                        <div class="col-md-1 col-sm-1 col-xs-12 form-group">
                            <label for="email">&nbsp;</label>
                            <button class="btn btn-primary" id="add_supplier_info_button">Add Supplier Information</button>
                        </div>
                    </div>


                </div>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Product Information</h2>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">

                    <div data-parsley-validate>

                        <div class="row">

                            <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                <label for="product_category">Product Category</label>
                                <select id="product_category" class="form-control">
                                    <option value="0">Select Category</option>
                                    @foreach($product_category as $category)
                                        <option value="{{ $category->category_id }}">{{ $category->category_name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 form-group" id="select_product_div">
                                <label for="product">Product Name</label>
                                <select id="product" class="form-control">

                                </select>
                            </div>

                        </div>


                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                <label for="unit_price">Unit Price</label>
                                <input type="number" class="form-control" id="unit_price" min="0">
                                <span class="form-control-feedback right" aria-hidden="true">&#2547;</span>
                            </div>

                            <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                <label for="quantity">Quantity</label>
                                <input type="number" id="quantity" class="form-control" />
                                <span class="form-control-feedback right" aria-hidden="true" id="quantity_span"></span>
                            </div>

                            <div class="col-md-3 col-sm-3 col-xs-12 form-group has-feedback">
                                <label for="quantity">Expire Date</label>
                                <input type="date" class="form-control has-feedback-left" id="expire_date">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                            </div>

                            {{--<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">--}}
                                {{--<input type="text" class="form-control has-feedback-left" id="single_cal2">--}}
                                {{--<span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>--}}
                                {{--<span id="inputSuccess2Status2" class="sr-only">(success)</span>--}}
                            {{--</div>--}}

                            <div class="col-md-1 col-sm-1 col-xs-12 form-group">
                                <label></label>
                                <button class="btn btn-primary" id="add_product_order_button">Add Product To Order</button>
                            </div>
                        </div>



                    </div>


                </div>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Purchase Order</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <section class="content invoice">
                        <!-- title row -->
                        <div class="row">
                            <div class="col-xs-12 invoice-header">
                                <h1>
                                    <i class="fa fa-globe"></i> Invoice
                                </h1>
                            </div>

                        </div>


                        <div class="row invoice-info">
                            <div class="col-sm-4 invoice-col">

                                <div>
                                    <h5><strong>Invoice No: <span id="invoice_no_span"></span></strong></h5>
                                    <h5><strong>Supplier: <span id="supplier_span"></span></strong></h5>
                                    <input type="hidden" id="supplier_id" value="0">
                                    <h5><strong>Purchase Date: <span id="purchase_date_span"></span></strong></h5>
                                    <h5><strong>Delivery Date: <span id="delivery_date_span"></span></strong></h5>
                                </div>
                            </div>


                        </div>



                        <div class="row top-margin-30">
                            <div class="col-xs-12 table">
                                <table class="table table-striped jambo_table bulk_action" id="invoice_table">
                                    <thead >
                                    <tr>
                                        <th>#</th>
                                        <th>Product Category</th>
                                        <th width="40%">Product Name</th>
                                        <th>Unit Price</th>
                                        <th>Quantity</th>
                                        <th>Total</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>

                                    <tbody id="invoice_table_tbody">


                                    </tbody>
                                </table>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->

                        <div class="row">
                            <div class="col-xs-6">

                            </div>

                            <div class="col-xs-6">

                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                            <th style="width:50%">Subtotal:</th>
                                            <td><input type="text" id="subtotal_input" class="form-control" readonly/></td>
                                        </tr>
                                        <tr>
                                            <th>Discount (Taka):</th>
                                            <td><input type="number" id="discount_input" class="form-control extra-cost" value="0" min="0"/></td>
                                        </tr>
                                        <tr>
                                            <th>Shipping:</th>
                                            <td><input type="number" id="shipping_input" class="form-control extra-cost" value="0" min="0"/></td>
                                        </tr>
                                        <tr>
                                            <th>Grand Total:</th>
                                            <td><input type="text" id="grand_total_input" class="form-control" readonly/></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="row no-print">
                            <div class="col-xs-12">
                                <button class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i> Print</button>

                                <div class="btn-group pull-right">
                                    <button type="button" class="btn btn-success">Cash Paid</button>
                                    <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="javascript:void(0);" onclick="payType('cash_sale')">Cash Sale</a></li>
                                        {{--<li><a href="javascript:void(0);" onclick="payType('bank_sale')">Bank Sale</a>--}}
                                        <li><a href="javascript:void(0);" onclick="payType('petty_cash')">Petty Cash</a>
                                        <li><a href="javascript:void(0);" onclick="payType('loan')">Loan</a>


                                        </li>
                                    </ul>
                                </div>

                                <button class="btn btn-success pull-right" id="submit_payment_button"><i class="fa fa-credit-card"></i> Payable</button>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>



@endsection
