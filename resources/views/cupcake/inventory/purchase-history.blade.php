@extends('layouts.main')

@section('title', 'Purchase History')

@section('content')

    <link href="/css/report.css" rel="stylesheet">

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12 top-margin-30">
            <div class="x_panel">

                <div class="x_content">
                    <br>
                    <div class="form-horizontal form-label-left">


                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4 col-xs-12">Select Report Type</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control" id="select_report_type">
                                    <option value="0">All Report</option>
                                    <option value="category_wise_report">Category Wise Report</option>
                                    <option value="product_wise_report">Product Wise Report</option>
                                </select>
                            </div>
                        </div>


                        <div class="form-group div-hide" id="select_category_div">
                            <label class="control-label col-md-4 col-sm-4 col-xs-12">Category</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control" id="category_id">
                                    <option value="0">All Categories</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->category_id }}">{{ $category->category_name }} ({{ $category->product_count!=Null?$category->product_count:0 }} Products)</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="form-group div-hide" id="select_product_div">
                            <label class="control-label col-md-4 col-sm-4 col-xs-12">Products</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control" id="product_id">
                                    <option value="0">All Products</option>
                                    @foreach($inv_products as $product)
                                        <option value="{{ $product->product_id }}">{{ $product->product_name }} ({{ $product->product_code }})</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="form-group top-margin-20">
                            <label class="control-label col-md-4 col-sm-4 col-xs-12">Start Date</label>
                            <div class="col-md-6 col-sm-6 col-xs-12 input-group date" id='myDatepicker2'>
                                <input type='text' class="form-control" id="start_date" value="{{ date("Y-m-01") }}"/>
                                <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4 col-xs-12">End Date</label>
                            <div class="col-md-6 col-sm-6 col-xs-12 input-group date" id='myDatepicker3'>
                                <input type='text' class="form-control" id="end_date" value="{{ date("Y-m-d") }}"/>
                                <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">
                                <button class="btn btn-success" id="generate_purchase_history">Generate Report</button>
                            </div>
                        </div>

                        <div id="printDiv"></div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12 top-margin-30" id="report_div">
            <div class="x_panel">

                <div class="x_content form-group">
                    <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="tile-stats text-center">
                            <img src="/images/restaurant-logo/{{ getRstaurantLogo() }}" width="5%">
                            <div class="count">{{ $restaurant_data->restaurants_name==''? "Cupcake":$restaurant_data->restaurants_name }}</div>
                            <h3>
                                <b>
                                    {{ $restaurant_data->address}}
                                    {{ $restaurant_data->area==null? '':', '.$restaurant_data->area }}
                                    {{ $restaurant_data->city==null? '':', '.$restaurant_data->city }}
                                </b>
                            </h3>
                            <div class="count" id="report_header">All Purchase Report</div>
                            <h3 id="date_header"></h3>
                            <div class="form-group text-center top-margin-20 ">
                                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 20px">
                                    <button class="btn btn-primary hidden-print" onclick="window.print()">Print Report</button>
                                    <button class="btn btn-dark hidden-print" id="email_purchase_history">Email Report</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>


                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table top-margin-20">
                            <thead>
                            <tr class="headings">
                                <th class="column-title">#</th>
                                <th class="column-title">Invoice No</th>
                                <th class="column-title">Category</th>
                                <th class="column-title">Product</th>
                                <th class="column-title">Quantity</th>
                                <th class="column-title">Unit Price</th>
                                <th class="column-title">Grand Total</th>
                                <th class="column-title">Purchase Date</th>
                            </tr>
                            </thead>

                            <tbody id="purchase_history_table">

                            @php $purchase_count=1; @endphp
                            @php $total_quantity=0; @endphp
                            @php $total=0; @endphp

                            @foreach($purchase_data as $purchase)

                                @php $grand_total = ($purchase->quantity * $purchase->unit_price); @endphp
                                @php $total_quantity += $purchase->quantity; @endphp
                                @php $total += $grand_total; @endphp

                                <tr class="even pointer">
                                    <td>{{ $purchase_count++ }}</td>
                                    <td>{{ $purchase->invoice_no }}</td>
                                    <td>{{ $purchase->category_name }}</td>
                                    <td>{{ $purchase->product_name }}</td>
                                    <td>{{ $purchase->quantity }} {{ $purchase->unit_name }}</td>
                                    <td>{{ $purchase->unit_price }}&#2547;</td>
                                    <td>{{ $grand_total }}&#2547;</td>
                                    <td>{{ date('F j, Y', strtotime( $purchase->purchase_date )) }}</td>
                                </tr>
                            @endforeach

                            <tr class="even pointer">
                                <td></td>
                                <th>Total</th>
                                <td></td>
                                <td></td>
                                <th>{{ $total_quantity }}</th>
                                <td></td>
                                <th>{{ $total }}&#2547;</th>
                                <td></td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!--Purchase Details Modal -->
    <div class="modal fade" id="purchase_details_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                </div>
                <div class="modal-body">

                    <div class="x_title text-center">
                        <h4>{{ getRstaurantName() }}</h4>
                        <h5>test address</h5>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_title form-group font-bold">
                        <h4>Invoice No : <a id="modal_invoice_no"></a></h4>
                        <h4>Supplier : <a id="modal_supplier_name"></a></h4>
                        <h4>Purchase Date : <a id="modal_purchase_date"></a></h4>
                        <h4>Delivery Date : <a id="modal_delivery_date"></a></h4>
                    </div>


                    <div class="x_title table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th width="5%">#</th>
                                <th width="50%">Item Name</th>
                                <th>U.Price</th>
                                <th>Qty</th>
                                <th>Total</th>
                            </tr>
                            </thead>

                            <tbody id="purchase_details_table">



                            </tbody>
                        </table>
                    </div>

                    <div class="x_title form-group font-bold">
                        <a>Sub Total : <a id="modal_sub_total">0</a></a><br>
                        <a>Discount :<a id="modal_discount">0</a></a><br>
                        <a>Shipping : <a id="modal_shipping">0</a></a><br>
                        <p>Grand Total : <a id="modal_grand_total">0</a></p>
                    </div>



                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="confirm_payment_button" class="btn btn-success" data-dismiss="modal">OK</button>
                </div>
            </div>

        </div>
    </div>


    <script src="/js/inventory.js"></script>
@endsection
