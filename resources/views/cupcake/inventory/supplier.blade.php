@extends('layouts.main')

@section('title', 'Supplier')

@section('content')

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12 top-margin-30">
            <div class="x_panel">
                <div class="top-margin-10">

                    <ul class="nav navbar-right panel_toolbox">
                        <li><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_supplier_modal">Add Supplier</button></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">

                    <div class="table-responsive top-margin-20">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                            <tr class="headings">
                                <th>#</th>
                                <th>Company Name</th>
                                <th>Supplier Name</th>
                                <th>Address</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Payable</th>
                                <th>Receivable</th>
                                <th>Joined</th>
                                <th width="17%">Action</th>
                            </tr>
                            </thead>

                            <tbody id="supplier_table">

                            @php $supplier_count=1; @endphp

                            @foreach($supplier_data as $supplier)

                                <tr class="even" id="supplier_{{ $supplier->supplier_id }}">
                                    <td> {{ $supplier_count++ }}</td>
                                    <td id="company_name_{{ $supplier->supplier_id }}">{{ $supplier->company_name }}</td>
                                    <td id="supplier_name_{{ $supplier->supplier_id }}">{{ $supplier->supplier_name }}</td>
                                    <td id="address_{{ $supplier->supplier_id }}">{{ $supplier->address }}</td>
                                    <td id="email_{{ $supplier->supplier_id }}">{{ $supplier->email }}</td>
                                    <td id="phone_{{ $supplier->supplier_id }}">{{ $supplier->phone }}</td>
                                    <td>{{ $supplier->accounts_payable }}</td>
                                    <td>{{ $supplier->accounts_receivable }}</td>
                                    <td>{{ date('F j, Y', strtotime( $supplier->created_at )) }}</td>
                                    <td>

                                        <button type="button" class="btn btn-primary" onclick="payment_supplier('{{ $supplier->supplier_id }}')">
                                            <i class="glyphicon glyphicon-send"></i>
                                        </button>

                                        <button type="button" class="btn btn-dark" onclick="edit_supplier('{{ $supplier->supplier_id }}')">
                                            <i class="glyphicon glyphicon-pencil"></i>
                                        </button>

                                        <button type="button" class="btn btn-danger" onclick="delete_supplier('{{ $supplier->supplier_id }}')">
                                            <i class="glyphicon glyphicon-remove"></i>
                                        </button>
                                    </td>
                                </tr>

                            @endforeach

                            </tbody>
                        </table>
                        {{ $supplier_data->links() }}
                    </div>


                </div>
            </div>
        </div>

    </div>


    <div class="modal fade" id="add_supplier_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title" id="edit_table_title">Add Supplier</h4>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label>Company Name</label>
                        <input type="text" class="form-control" id="company_name">
                    </div>

                    <div class="form-group">
                        <label>Supplier Name</label>
                        <input type="text" class="form-control" id="supplier_name">
                    </div>

                    <div class="form-group">
                        <label>Address</label>
                        <input type="text" class="form-control" id="address">
                    </div>

                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" class="form-control" id="email">
                    </div>

                    <div class="form-group">
                        <label>Phone</label>
                        <input type="text" class="form-control" id="phone">
                    </div>

                    <div class="form-group">
                        <label>Act Code</label>
                        <input type="text" class="form-control" id="act_code">
                    </div>


                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="add_supplier_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                </div>
            </div>

        </div>
    </div>


    <div class="modal fade" id="edit_supplier_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Edit Supplier</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="edit_supplier_id">
                    <div class="form-group">
                        <label>Company Name</label>
                        <input type="text" class="form-control" id="edit_company_name" placeholder="">
                    </div>

                    <div class="form-group">
                        <label>Supplier Name</label>
                        <input type="text" class="form-control" id="edit_supplier_name" placeholder="">
                    </div>

                    <div class="form-group">
                        <label>Address</label>
                        <input type="text" class="form-control" id="edit_address" placeholder="">
                    </div>

                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" class="form-control" id="edit_email" placeholder="">
                    </div>

                    <div class="form-group">
                        <label>Phone</label>
                        <input type="text" class="form-control" id="edit_phone" placeholder="">
                    </div>


                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="edit_supplier_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                </div>
            </div>

        </div>
    </div>


    <div class="modal fade" id="payment_supplier_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Edit Supplier</h4>
                </div>
                <div class="modal-body">

                    <input type="hidden" id="payment_supplier_id">
                    <input type="hidden" id="payment_supplier_name">

                    <div class="form-group">
                        <label>Debit From</label>
                        <select class="form-control" id="debit_from">
                            <option value="sales">Sales</option>
                            <option value="petty_cash">Petty Cash</option>
                            <option value="loan">Loan</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Payment Amount</label>
                        <input type="number" class="form-control" id="amount">
                    </div>



                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="payment_supplier_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                </div>
            </div>

        </div>
    </div>


    <script src="/js/inventory.js"></script>

@endsection
