@extends('layouts.main')

@section('title', 'Transfer History')

@section('content')

    <script src="/js/inventory.js"></script>

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12 top-margin-30">
            <div class="x_panel">
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table top-margin-20">
                            <thead>
                            <tr class="headings">
                                <th class="column-title">#</th>
                                <th class="column-title">Items</th>
                                <th class="column-title">From Store</th>
                                <th class="column-title">To Store</th>
                                <th class="column-title">Transfer Date</th>
                                <th class="column-title">Created By</th>
                                <th class="column-title">Created At</th>
                            </tr>
                            </thead>

                            <tbody>

                            @foreach($transfers as $transfer)
                                <tr class="pointer" onclick="transferDetails('{{ $transfer->transfer_id }}', '{{ $transfer->from_store_name }}', '{{ $transfer->to_store_name }}', '{{ $transfer->transfer_date }}', '{{ $transfer->name }}')">
                                    <td>Transfer-{{ $transfer->transfer_id }}</td>
                                    <td>{{ $transfer->items }}</td>
                                    <td>{{ $transfer->from_store_name }}</td>
                                    <td>{{ $transfer->to_store_name }}</td>
                                    <td>{{ date('F j, Y', strtotime( $transfer->transfer_date )) }}</td>
                                    <td>{{ $transfer->name }}</td>
                                    <td>{{ date('F j, Y H:i a', strtotime( $transfer->created_at )) }}</td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="transfer_details_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                </div>
                <div class="modal-body" id="order_details_modal_print">

                    <div class="x_title text-center">
                        <img src="/images/restaurant-logo/{{ $restaurant_data->restaurant_img }}" id="logo-icon" >
                        <h4>{{ $restaurant_data->restaurants_name==''? "Cupcake":$restaurant_data->restaurants_name }}</h4>
                        <h5>Address :
                            {{ $restaurant_data->address}}
                            {{ $restaurant_data->area==null? '':', '.$restaurant_data->area }}
                            {{ $restaurant_data->city==null? '':', '.$restaurant_data->city }}
                        </h5>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_title form-group">
                        <h5>Transfer Date : <a id="m_transfer_date"></a></h5>
                        <h5>From : <a id="m_from"></a></h5>
                        <h5>To : <a id="m_to"></a></h5>
                        <h5>Created By : <a id="m_created_by"></a></h5>
                    </div>


                    <div class="x_title table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th width="5%">#</th>
                                <th width="30%">Code</th>
                                <th width="50%">Item Name</th>
                                <th>Qty</th>
                            </tr>
                            </thead>
                            <tbody id="modal_transfer_table">

                            </tbody>
                        </table>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-success" data-dismiss="modal" onclick="window.print();">Print</button>
                </div>
            </div>

        </div>
    </div>







@endsection
