@extends('layouts.main')

@section('title', 'Users')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Add New User</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <div class="form-horizontal form-label-left">

                        <div class="form-group">
                            <label class="control-label col-md-3">Employee Name <span class="red-text">*</span>
                            </label>
                            <div class="col-md-6">
                                <input type="text" id="employee_name" required="required" class="form-control" placeholder="John Snow">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Role <span class="red-text">*</span></label>
                            <div class="col-md-6">
                                <select class="form-control" id="select_user_role">
                                    <option value="0">Choose option</option>
                                    @foreach($role_lookup as $role)
                                        <option value="{{ $role->role_name }}">{{ ucfirst($role->role_name) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>



                        <div class="form-group">
                            <label class="control-label col-md-3">Username <span class="red-text">*</span></label>
                            <div class="col-md-3">
                                <input type="text" id="user_email" class="form-control">
                            </div>
                            {{--<div class="col-md-3">--}}
                            {{--<label class="control-label col-md-3">&#64;{{ getRstaurantCode() }}.com</label>--}}
                            {{--<input type="hidden" value="&#64;{{ getRstaurantCode() }}.com" id="rstaurantCode">--}}
                            {{--</div>--}}
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Password</label>
                            <div class="col-md-3">
                                <input type="text" id="user_password" class="form-control">
                            </div>
                            <div class="col-md-3">
                                <button class="btn btn-success" onclick="generatePassword()">Generate Password</button>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Phone Number</label>
                            <div class="col-md-6">
                                <input type="number" id="user_phone" class="form-control" placeholder="">
                            </div>
                        </div>

                        <div class="form-group top-margin-50">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button class="btn btn-success" id="add_user_button">Add User</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Employee List</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        {{--<button type="button" class="btn btn-success btn-lg">New Employee</button>--}}
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">


                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                            <tr class="headings">
                                <th>#</th>
                                <th>Name</th>
                                <th>Username</th>
                                <th>Phone</th>
                                <th>Role</th>
                                <th>Status</th>
                                <th>Joined</th>
                                <th>Action</th>
                            </tr>
                            </thead>

                            <tbody id="user_table">

                            @php $user_count=0; @endphp
                            @foreach($user_data as $user)
                                <tr class="even pointer" id="user_{{ $user->id }}">
                                    <td>{{ ++$user_count }}</td>
                                    <td>{{ $user->name }} {{ $user->id==Auth::user()->id?' (Me)': '' }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->phone }}</td>
                                    <td>{{ ucfirst($user->role) }}</td>
                                    <td>{{ $user->status }}</td>
                                    <td>{{ date('F j, Y', strtotime( $user->created_at )) }}</td>
                                    <td>
                                        @if($user->id != Auth::user()->id)

                                            @if(Auth::user()->role == 'superadmin' || Auth::user()->role == 'admin')
                                                <button type="button" class="btn btn-warning" onclick="editPassword({{ $user->id }})" {{ $user->email=='superuser@cupcake.com'?'disabled':'' }}>
                                                    <i class="fa fa-lock"></i>
                                                </button>
                                            @endif

                                            <button type="button" class="btn btn-dark" onclick="editUser('{{ $user->id }}', '{{ $user->name }}', '{{ $user->role }}', '{{ $user->phone }}')" {{ $user->email=='superuser@cupcake.com'?'disabled':'' }}>
                                                <i class="glyphicon glyphicon-pencil"></i>
                                            </button>

                                            <button type="button" class="btn btn-danger" onclick="deleteUser({{ $user->id }})" {{ $user->email=='superuser@cupcake.com'?'disabled':'' }}>
                                                <i class="glyphicon glyphicon-remove"></i>
                                            </button>

                                        @else
                                            <button type="button" class="btn btn-dark pointer">
                                                <i class="glyphicon glyphicon-user"></i>
                                            </button>
                                        @endif

                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>


                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="edit_user_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title" id="edit_table_title">Edit User</h4>
                    <input type="hidden" id="edit_user_id" value="0">
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label>User Name</label>
                        <input type="text" class="form-control" id="modal_user_name">
                    </div>

                    <div class="form-group">
                        <label>Role</label>
                        <select class="form-control" id="modal_user_role">
                            <option value="0">Choose option</option>
                            @foreach($role_lookup as $role)
                                <option value="{{ $role->role_name }}">{{ ucfirst($role->role_name) }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Phone Number</label>
                        <input type="text" class="form-control" id="modal_user_phone">
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="edit_user_button" class="btn btn-success" data-dismiss="modal">Done</button>
                </div>
            </div>

        </div>
    </div>


    <div class="modal fade" id="edit_password_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Edit Password</h4>
                </div>
                <div class="modal-body">

                    <input type="hidden" id="edit_password_id" value="0">

                    <div class="form-group">
                        <label>New Password</label>
                        <input type="text" class="form-control" id="new_password">
                    </div>

                    <div class="form-group">
                        <label>Retype Password</label>
                        <input type="text" class="form-control" id="retype_password">
                    </div>


                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="edit_password_button" class="btn btn-success" data-dismiss="modal">Done</button>
                </div>
            </div>

        </div>
    </div>


    <script src="/js/user.js"></script>

@endsection
