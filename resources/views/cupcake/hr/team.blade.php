@extends('layouts.main')

@section('title', 'Team')

@section('content')


    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_content">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 text-center">

                        </div>

                        <div class="clearfix"></div>

                        @foreach($user_data as $user)
                            <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                                <div class="well profile_view">
                                    <div class="col-sm-12">
                                        <h4 class="brief"><i>{{ ucfirst($user->role) }}</i></h4>
                                        <div class="left col-xs-7">
                                            <h2>{{ ucfirst($user->name) }}</h2>
                                            <ul class="list-unstyled">
                                                <li><i class="fa fa-building"></i> Status: {{ $user->status==1? 'Active':'Inactive' }}</li>
                                                <li><i class="fa fa-phone"></i> Phone #: {{ $user->phone }}</li>
                                            </ul>
                                        </div>
                                        <div class="right col-xs-5 text-center">
                                            <img src="images/{{ $user->user_img }}" alt="{{ $user->name }}" class="img-circle img-responsive" width="85%">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 bottom">
                                        <div class="col-xs-12 col-sm-6">
                                            <button type="button" class="btn btn-primary">
                                                <i class="fa fa-user"> </i> View Profile
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach




                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
