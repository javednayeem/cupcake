@extends('layouts.main')

@section('title', 'Recipe')

@section('content')

    <script src="/js/recipe.js"></script>
    <script src="/js/inventory.js"></script>

    @if (Auth::user()->license == 0)
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">

                        <div class="bs-example top-margin-30">
                            <div class="jumbotron">
                                <h1>License Has Been Expired</h1>
                                <p>Please Contact Your System Administrator For Further Information</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    @else


        @foreach($inv_products as $product)
            <script type="application/javascript">
                @php
                    echo 'pushToProductArray("'.$product->product_id.'", "'.$product->product_name.'", "'.$product->status.'", "'.$product->unit_id.'", "'.$product->category_id.'", "'.$product->fixed_price.'");'
                @endphp
            </script>
        @endforeach

        @foreach($units as $unit)
            <script type="application/javascript">
                @php echo 'pushToUnitArray("'.$unit->unit_id.'", "'.$unit->unit_name.'");' @endphp
            </script>
        @endforeach




        <div class="row top-margin-40">

            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="x_panel">
                    <div class="">
                        <h2>Menu Items</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table class="table table-striped jambo_table bulk_action" id="item_table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Menu Name</th>
                                <th>Item Name</th>
                            </tr>
                            </thead>
                            <tbody id="menu_table">

                            @php $item_count=1; @endphp

                            @foreach($menu_items as $item)
                                <tr class="pointer" onclick="view_recipe('{{ $item->menu_item_id }}', '{{ $item->item_name }}')">
                                    <td scope="row">{{ $item_count++ }}</td>
                                    <td>{{ $item->menu_name }}</td>
                                    <td>{{ $item->item_name }}</td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

            <div class="col-md-8 col-sm-8 col-xs-12">
                <div class="x_panel">

                    <div class="">
                        <h2>Item Recipe</h2>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content" id="menu_item_div">

                        <ul class="nav navbar-right panel_toolbox">
                            <li>
                                <button type="button" id="button_recipe_modal" class="btn btn-primary hidden" data-toggle="modal" data-target="#add_recipe_modal">
                                    Add Recipe Item
                                </button>
                            </li>
                        </ul>

                        <h2 id="item_title">No Menu Selected or No Items</h2>

                        <table class="table table-striped jambo_table top-margin-30">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Product Name</th>
                                <th>Quantity</th>
                                <th width="15%">Action</th>
                            </tr>
                            </thead>

                            <tbody id="recipe_table">



                            </tbody>

                        </table>

                    </div>
                </div>
            </div>
        </div>

    @endif



    <div class="modal fade" id="add_recipe_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Add New Recipe: <span id="modal_item_title"></span></h4>
                </div>
                <div class="modal-body">

                    <input type="hidden" id="item_id" value="0">


                    <div class="form-group">
                        <label>Product Category</label>
                        <select id="product_category" class="form-control">
                            <option value="0">Select Category</option>
                            @foreach($product_category as $category)
                                <option value="{{ $category->category_id }}">{{ $category->category_name }}</option>
                            @endforeach
                        </select>
                    </div>


                    <div class="form-group">
                        <label>Product Item</label>
                        <select id="product" class="form-control">

                        </select>
                    </div>


                    <div class="form-group">
                        <label>Unit Of Quantity</label>
                        <select id="unit_of_quantity" class="form-control">
                            @foreach($units as $unit)
                                <option value="{{ $unit->unit_id }}">{{ $unit->unit_name }}</option>
                            @endforeach
                        </select>
                    </div>


                    <div class="form-group has-feedback">
                        <label for="quantity">Quantity</label>
                        <input type="number" id="quantity" class="form-control" />
                        {{--<span class="form-control-feedback right" aria-hidden="true" id="quantity_span"></span>--}}
                    </div>


                    <div class="form-group has-feedback hidden">
                        <label for="cost">Cost</label>
                        <input type="number" id="cost" class="form-control" disabled/>
                        <span class="form-control-feedback right" aria-hidden="true">&#2547;</span>
                    </div>


                    <div class="form-group">
                        <label>Store</label>
                        <select id="store_id" class="form-control">
                            @foreach($inv_stores as $store)
                                <option value="{{ $store->store_id }}">{{ $store->store_name }}</option>
                            @endforeach
                        </select>
                    </div>


                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="add_recipe_button" class="btn btn-success" data-dismiss="modal">Done</button>
                </div>
            </div>

        </div>
    </div>


    <div class="modal fade" id="edit_recipe_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Edit Recipe: <span id="edit_modal_item_title"></span></h4>
                </div>
                <div class="modal-body">

                    <input type="hidden" id="recipe_id" value="0">
                    <input type="hidden" id="edit_item_id" value="0">
                    <input type="hidden" id="edit_item_name" value="0">

                    <div class="form-group">
                        <label for="edit_product">Product Item</label>
                        <select id="edit_product" class="form-control">

                        </select>
                    </div>


                    <div class="form-group has-feedback">
                        <label for="edit_quantity">Quantity</label>
                        <input type="number" id="edit_quantity" class="form-control" />
                        <span class="form-control-feedback right" aria-hidden="true" id="edit_quantity_span"></span>
                    </div>


                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="edit_recipe_button" class="btn btn-success" data-dismiss="modal">Done</button>
                </div>
            </div>

        </div>
    </div>







@endsection
