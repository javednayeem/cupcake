@extends('layouts.main')
@section('title', 'Menu')

@section('content')

    @if (Auth::user()->license == 0)
        @include('cupcake.partials.expired-license')
    @else


        <script src="/js/menu.js"></script>


        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Add New Menu</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <div class="form-horizontal form-label-left">

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Menu Name <span class="text-danger">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="menu_name" required="required" class="form-control col-md-7 col-xs-12" placeholder="APPETIZER’S">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button class="btn btn-success" id="add_menu_button">Add Menu</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Add Food Item to Menu</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <div class="form-horizontal form-label-left">

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Select Menu</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select class="form-control" id="select_menu_id">
                                        <option value="0">Choose option</option>
                                        @foreach($menu_data as $menu)
                                            <option value="{{ $menu->menu_id }}">{{ $menu->menu_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Item Name <span class="text-danger">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="item_name" required="required" class="form-control col-md-7 col-xs-12" placeholder="Golden Fried Prawn">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Ratio</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="item_ratio" class="form-control col-md-7 col-xs-12" placeholder="1:3 or 100gm">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Price
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="number" id="item_price" class="form-control col-md-7 col-xs-12" placeholder="450">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Cost Price</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="number" id="cost_price" class="form-control col-md-7 col-xs-12" placeholder="120" value="0">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Vat</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="item_vat" class="form-control col-md-7 col-xs-12" value="{{ getRstaurantVAT() }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Set Menu</label>
                                <div class="col-md-6 col-sm-6 col-xs-12 form-group" style="margin-top: 7px">
                                    <input type="checkbox" class="" id="set_menu">
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Discount Applicable</label>
                                <div class="col-md-6 col-sm-6 col-xs-12 form-group" style="margin-top: 7px">
                                    <input type="checkbox" class="" id="discount_available" checked>
                                </div>
                            </div>

                            <div class="form-group div-hide">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Category</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="item_category" class="form-control col-md-7 col-xs-12" placeholder="Lobster">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Select Printer</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select class="form-control" id="printer_id">
                                        @foreach($printers as $printer)
                                            <option value="{{ $printer->printer_id }}">{{ $printer->printer_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button class="btn btn-success" id="add_food_item_button">Add Food Item</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">

            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="x_panel">
                    <div class="">
                        <h2>Menu</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table class="table table-striped jambo_table bulk_action" id="menu_table_span">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Menu Name</th>
                                <th width="40%">Action</th>
                            </tr>
                            </thead>
                            <tbody id="menu_table">

                            @php $menu_count=0; @endphp

                            @foreach($menu_data as $menu)
                                <tr id="menu_{{ $menu->menu_id }}" onclick="view_menu_items('{{ $menu->menu_id }}', '{{ $menu->menu_name }}')">
                                    <td scope="row">{{ ++$menu_count }}</td>
                                    <td id="menu_name_{{ $menu->menu_id }}">{{ $menu->menu_name }}</td>
                                    <td>

                                        <button type="button" onclick="editMenu('{{ $menu->menu_id }}', '{{ $menu->menu_name }}')" class="btn btn-dark">
                                            <i class="glyphicon glyphicon-pencil"></i>
                                        </button>

                                        <button type="button" class="btn btn-danger" onclick="deleteMenu({{ $menu->menu_id }})">
                                            <i class="glyphicon glyphicon-remove"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

            <div class="col-md-8 col-sm-8 col-xs-12">
                <div class="x_panel">

                    <div class="">
                        <h2>Menu Items</h2>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content" id="menu_item_div">
                        <h2 id="menu_item_title">No Menu Selected or No Items</h2>

                        <table class="table table-striped jambo_table" id="menu_item_table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Item Name</th>
                                <th>Ratio</th>
                                <th>Price</th>
                                <th>Cost Price</th>
                                <th>Discount</th>
                                <th>VAT</th>
                                <th>Status</th>
                                <th width="20%">Action</th>
                            </tr>
                            </thead>

                        </table>

                    </div>
                </div>
            </div>
        </div>

    @endif



    <div class="modal fade" id="edit_menu_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title" id="edit_menu_title">Edit Menu</h4>
                    <input type="hidden" id="edit_menu_id" value="0">
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label>Menu Name</label>
                        <input type="text" class="form-control" id="modal_menu_name" placeholder="Salad">
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="edit_menu_button" class="btn btn-success" data-dismiss="modal">Done</button>
                </div>
            </div>

        </div>
    </div>


    <div class="modal fade" id="edit_item_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title" id="edit_item_title">Edit Menu</h4>
                    <input type="hidden" id="edit_item_id" value="0">
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label>Item Name</label>
                        <input type="text" class="form-control" id="modal_item_name">
                    </div>

                    <div class="form-group">
                        <label>Ratio</label>
                        <input type="text" class="form-control" id="modal_item_ratio">
                    </div>

                    <div class="form-group">
                        <label>Price</label>
                        <input type="number" class="form-control" id="modal_item_price">
                    </div>

                    <div class="form-group">
                        <label>Cost Price</label>
                        <input type="number" class="form-control" id="modal_cost_price">
                    </div>

                    <div class="form-group">
                        <label>Discount</label>
                        <input type="number" class="form-control" id="modal_item_discount">
                    </div>

                    <div class="form-group">
                        <label>VAT</label>
                        <input type="number" class="form-control" id="modal_item_vat">
                    </div>


                    <div class="form-group">
                        <label>Menu</label>
                        <select class="form-control" id="modal_menu_id">
                            @foreach($menu_data as $menu)
                                <option value="{{ $menu->menu_id }}">{{ $menu->menu_name }}</option>
                            @endforeach
                        </select>
                    </div>


                    <div class="form-group">
                        <label>Printer</label>
                        <select class="form-control" id="modal_printer_id">
                            @foreach($printers as $printer)
                                <option value="{{ $printer->printer_id }}">{{ $printer->printer_name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <input type="checkbox" id="modal_item_status">&nbsp; Tick If Active
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="edit_item_button" class="btn btn-success" data-dismiss="modal">Done</button>
                </div>
            </div>

        </div>
    </div>


    <div class="modal fade" id="edit_setmenu_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Set Menu</h4>
                    <input type="hidden" id="modal_menu_item_id" value="0">
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label>Item Name</label>
                        <input type="text" class="form-control" id="modal_setmenu_item_name" placeholder="Salad">
                    </div>

                    <div class="form-group">
                        <button type="button" id="add_setmenu_item_button" class="btn btn-success">Done</button>
                    </div>


                    <table class="table table-hover m-t-10">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Item Name</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                        <tbody id="setmenu_item_table">

                        </tbody>
                    </table>


                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>



@endsection
