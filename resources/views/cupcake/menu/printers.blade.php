@extends('layouts.main')

@section('title', 'Printers')

@section('content')

    <script src="/js/printer.js"></script>

    @if (Auth::user()->license == 0)
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">

                        <div class="bs-example top-margin-30">
                            <div class="jumbotron">
                                <h1>License Has Been Expired</h1>
                                <p>Please Contact Your System Administrator For Further Information</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    @else



        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12 top-margin-30">
                <div class="x_panel">
                    <div class="top-margin-10">

                        <ul class="nav navbar-right panel_toolbox">
                            <li>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_printer_modal">
                                    Add Printer
                                </button>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">

                        <div class="table-responsive top-margin-20">
                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                <tr class="headings">
                                    <th class="column-title">#</th>
                                    <th class="column-title">Printer Name</th>
                                    <th class="column-title">Port</th>
                                    <th class="column-title">Created At</th>
                                    <th class="column-title no-link last" width="11%">
                                        <span class="nobr">Action</span>
                                    </th>
                                </tr>
                                </thead>

                                <tbody id="printer_table">

                                @php $printer_count=1; @endphp

                                @foreach($printers as $printer)

                                    <tr class="even" id="printer_{{ $printer->printer_id }}">
                                        <td> {{ $printer_count++ }}</td>
                                        <td id="printer_name_{{ $printer->printer_id }}">{{ $printer->printer_name }}</td>
                                        <td id="port_{{ $printer->printer_id }}">{{ $printer->port }}</td>
                                        <td>{{ date('F j, Y', strtotime( $printer->created_at )) }}</td>
                                        <td>
                                            <button type="button" class="btn btn-dark" onclick="editPrinter('{{ $printer->printer_id }}', '{{ $printer->printer_name }}', '{{ $printer->port }}')">
                                                <i class="glyphicon glyphicon-pencil"></i>
                                            </button>
                                            <button type="button" class="btn btn-danger" onclick="deletePrinter({{ $printer->printer_id }})">
                                                <i class="glyphicon glyphicon-remove"></i>
                                            </button>
                                        </td>
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>


                    </div>
                </div>
            </div>

        </div>


        <div class="modal fade" id="add_printer_modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">x</button>
                        <h4 class="modal-title" id="edit_table_title">Add Printer</h4>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label>Printer Name</label>
                            <input type="text" class="form-control" id="printer_name">
                        </div>

                        <div class="form-group">
                            <label>Port</label>
                            <input type="text" class="form-control" id="port">
                        </div>


                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                        <button type="button" id="add_printer_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                    </div>
                </div>

            </div>
        </div>


        <div class="modal fade" id="edit_printer_modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">x</button>
                        <h4 class="modal-title" id="edit_table_title">Edit Printer</h4>
                    </div>
                    <div class="modal-body">

                        <input type="hidden" id="edit_printer_id" value="0">

                        <div class="form-group">
                            <label>Printer Name</label>
                            <input type="text" class="form-control" id="edit_printer_name">
                        </div>

                        <div class="form-group">
                            <label>Port</label>
                            <input type="text" class="form-control" id="edit_port">
                        </div>


                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                        <button type="button" id="edit_printer_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                    </div>
                </div>

            </div>
        </div>


    @endif








@endsection
