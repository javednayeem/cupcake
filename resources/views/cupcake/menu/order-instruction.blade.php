@extends('layouts.main')

@section('title', 'Order Instruction')

@section('content')

    <script src="/js/order-instruction.js"></script>

    @if (Auth::user()->license == 0)
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">

                        <div class="bs-example top-margin-30">
                            <div class="jumbotron">
                                <h1>License Has Been Expired</h1>
                                <p>Please Contact Your System Administrator For Further Information</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    @else



        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12 top-margin-30">
                <div class="x_panel">
                    <div class="top-margin-10">

                        <ul class="nav navbar-right panel_toolbox">
                            <li>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_instruction_modal">Add Instruction</button>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">

                        <div class="table-responsive top-margin-20">
                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                <tr class="headings">
                                    <th class="column-title">#</th>
                                    <th class="column-title">Instruction</th>
                                    <th class="column-title">Created At</th>
                                    <th class="column-title no-link last" width="11%">
                                        <span class="nobr">Action</span>
                                    </th>
                                </tr>
                                </thead>

                                <tbody id="instruction_table">

                                @php $instruction_count=1; @endphp

                                @foreach($order_instructions as $instruction)

                                    <tr class="even" id="order_instruction_{{ $instruction->instruction_id }}">
                                        <td> {{ $instruction_count++ }}</td>
                                        <td id="instruction_{{ $instruction->instruction_id }}">{{ $instruction->instruction }}</td>

                                        <td>{{ date('F j, Y', strtotime( $instruction->created_at )) }}</td>
                                        <td>
                                            <button type="button" class="btn btn-dark" onclick="editInstruction('{{ $instruction->instruction_id }}', '{{ $instruction->instruction }}')">
                                                <i class="glyphicon glyphicon-pencil"></i>
                                            </button>
                                            <button type="button" class="btn btn-danger" onclick="deleteInstruction({{ $instruction->instruction_id }})">
                                                <i class="glyphicon glyphicon-remove"></i>
                                            </button>
                                        </td>
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>


                    </div>
                </div>
            </div>

        </div>


        <div class="modal fade" id="add_instruction_modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">x</button>
                        <h4 class="modal-title" id="edit_table_title">Add Order Instruction</h4>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label>Order Instruction</label>
                            <input type="text" class="form-control" id="instruction">
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                        <button type="button" id="add_instruction_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                    </div>
                </div>

            </div>
        </div>


        <div class="modal fade" id="edit_instruction_modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">x</button>
                        <h4 class="modal-title" id="edit_table_title">Edit Order Instruction</h4>
                    </div>
                    <div class="modal-body">

                        <input type="hidden" class="form-control" id="edit_instruction_id">

                        <div class="form-group">
                            <label>Order Instruction</label>
                            <input type="text" class="form-control" id="edit_instruction">
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                        <button type="button" id="edit_instruction_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                    </div>
                </div>

            </div>
        </div>


    @endif








@endsection
