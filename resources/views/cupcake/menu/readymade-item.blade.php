@extends('layouts.main')

@section('title', 'ReadyMade Items')

@section('content')

    @if (Auth::user()->license == 0)
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">

                        <div class="bs-example top-margin-30">
                            <div class="jumbotron">
                                <h1>License Has Been Expired</h1>
                                <p>Please Contact Your System Administrator For Further Information</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    @else





        <div class="row top-margin-30">

            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="x_panel">
                    <div class="">
                        <h2>Menu</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Menu Name</th>
                            </tr>
                            </thead>
                            <tbody id="menu_table">

                            @php $menu_count=0; @endphp
                            @foreach($menu_data as $menu)
                                <tr class="pointer" onclick="view_readymade_items('{{ $menu->menu_id }}', '{{ $menu->menu_name }}')">
                                    <td scope="row">{{ ++$menu_count }}</td>
                                    <td id="menu_name_{{ $menu->menu_id }}">{{ $menu->menu_name }}</td>
                                </tr>
                            @endforeach



                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

            <div class="col-md-8 col-sm-8 col-xs-12">
                <div class="x_panel">

                    <div class="">
                        <h2>Menu Items</h2>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content" id="menu_item_div">
                        <h2 id="menu_item_title">No Menu Selected or No Items</h2>

                        <table class="table table-striped jambo_table" id="menu_item_table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Item Name</th>
                                <th>Readymade</th>
                                <th>Quantity</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>

                            <tbody id="menu_item_list">
                            </tbody>

                        </table>

                    </div>
                </div>
            </div>
        </div>

    @endif





    <!--edit Item Modal -->
    <div class="modal fade" id="edit_ready_item_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content" style="margin-top: 200px; ">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title" id="edit_item_title">Edit Readymade Item</h4>
                    <input type="hidden" id="edit_item_id" value="0">
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <input type="checkbox" id="readymade_item">&nbsp; Readymade Item
                    </div>

                    <div class="form-group">
                        <label>Quantity</label>
                        <input type="number" class="form-control" id="item_quantity" min="0">
                    </div>


                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="edit_ready_item_button" class="btn btn-success" data-dismiss="modal">Done</button>
                </div>
            </div>

        </div>
    </div>
    <!-- end Modal -->


    <script src="/js/menu.js"></script>
@endsection
