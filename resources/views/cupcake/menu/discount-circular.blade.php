@extends('layouts.main')

@section('title', 'Discount Circular')

@section('content')

    <script src="/js/menu.js"></script>

    <div class="row">
        <div class="col-md-12 top-margin-20">
            <div class="x_panel">

                <div class="x_content">

                    <div class="form-horizontal form-label-left top-margin-20">


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Discount Circular Name</label>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="text" class="form-control has-feedback-left" id="circular_name">
                                <span class="fa fa-home form-control-feedback left" aria-hidden="true"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Discount Amount</label>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="text" class="form-control has-feedback-left" id="discount">
                                <span class="fa fa-home form-control-feedback left" aria-hidden="true"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Discount Type</label>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <select class="form-control has-feedback-left" id="discount_type">
                                    <option value="cash">Cash Discount</option>
                                    <option value="percentage">Percentage Discount</option>
                                </select>
                                <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Discount Start Date</label>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="text" class="form-control has-feedback-left" id="single_cal1">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                <span id="inputSuccess2Status" class="sr-only">(success)</span>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Discount End Date</label>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="text" class="form-control has-feedback-left" id="single_cal3">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                <span id="inputSuccess2Status" class="sr-only">(success)</span>
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5 ">
                                <button class="btn btn-success btn-lg" id="save_discount_circular">Save Discount Circular</button>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12 top-margin-30">
            <div class="x_panel">

                <div class="x_content">

                    <div class="form-horizontal">


                        <div class="animated pulse col-md-12 col-sm-12 col-xs-12">
                            <div class="">
                                <div class="x_title">
                                    <h2>Lookup List</h2>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="x_content">
                                    <table class="table table-striped jambo_table bulk_action">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Discount Circular Name</th>
                                            <th>Discount Amount</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th>Created By</th>
                                            <th>Created At</th>
                                            <th width="15%">Action</th>
                                        </tr>
                                        </thead>

                                        <tbody id="circular_table">

                                        @php $circular_count=1; @endphp

                                        @foreach($discount_circular as $circular)
                                            <tr id="circular_{{ $circular->circular_id }}">
                                                <td scope="row"> {{ $circular_count++ }}</td>
                                                <td>{{ $circular->circular_name }}</td>
                                                <td>{{ $circular->discount }}{{ $circular->discount_type=='cash'?'Tk':'%' }}</td>
                                                <td>{{ date('F j, Y', strtotime( $circular->start_date )) }}</td>
                                                <td>{{ date('F j, Y', strtotime( $circular->end_date )) }}</td>
                                                <td>{{ $circular->name }}</td>
                                                <td>{{ date('F j, Y', strtotime( $circular->created_at )) }}</td>
                                                <td>
                                                    <button type="button" class="btn btn-dark">
                                                        <i class="glyphicon glyphicon-pencil"></i>
                                                    </button>

                                                    <button type="button" class="btn btn-danger" onclick="deleteCircular({{ $circular->circular_id }})">
                                                        <i class="glyphicon glyphicon-remove"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>





@endsection
