@extends('layouts.main')

@section('title', 'Modifiers')

@section('content')

    <script src="/js/modifier.js"></script>

    @if (Auth::user()->license == 0)
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">

                        <div class="bs-example top-margin-30">
                            <div class="jumbotron">
                                <h1>License Has Been Expired</h1>
                                <p>Please Contact Your System Administrator For Further Information</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    @else



        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12 top-margin-30">
                <div class="x_panel">
                    <div class="top-margin-10">

                        <ul class="nav navbar-right panel_toolbox">
                            <li><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_modifier_modal">Add Modifier</button></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">

                        <div class="table-responsive top-margin-20">
                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                <tr class="headings">
                                    <th class="column-title">#</th>
                                    <th class="column-title">Modifier Name</th>
                                    <th class="column-title">Price</th>
                                    <th class="column-title">VAT</th>
                                    <th class="column-title">Status</th>
                                    <th class="column-title">Created At</th>
                                    <th class="column-title no-link last" width="11%">
                                        <span class="nobr">Action</span>
                                    </th>
                                </tr>
                                </thead>

                                <tbody id="modifier_table">

                                @php $modifier_count=1; @endphp

                                @foreach($modifiers as $modifier)

                                    <tr class="even" id="modifier_{{ $modifier->modifier_id }}">
                                        <td> {{ $modifier_count++ }}</td>
                                        <td id="modifier_name_{{ $modifier->modifier_id }}">{{ $modifier->modifier_name }}</td>
                                        <td id="price_{{ $modifier->modifier_id }}">{{ $modifier->price }}</td>
                                        <td id="modifier_vat_{{ $modifier->modifier_id }}">{{ $modifier->modifier_vat }}</td>
                                        <td>{{ $modifier->status }}</td>
                                        <td>{{ date('F j, Y', strtotime( $modifier->created_at )) }}</td>
                                        <td>
                                            <button type="button" class="btn btn-dark" onclick="editModifier('{{ $modifier->modifier_id }}', '{{ $modifier->modifier_name }}', '{{ $modifier->price }}', '{{ $modifier->modifier_vat }}', '{{ $modifier->status }}')">
                                                <i class="glyphicon glyphicon-pencil"></i>
                                            </button>
                                            <button type="button" class="btn btn-danger" onclick="deleteModifier({{ $modifier->modifier_id }})">
                                                <i class="glyphicon glyphicon-remove"></i>
                                            </button>
                                        </td>
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>


                    </div>
                </div>
            </div>

        </div>


        <div class="modal fade" id="add_modifier_modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">x</button>
                        <h4 class="modal-title" id="edit_table_title">Add Modifier</h4>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label>Modifier Name</label>
                            <input type="text" class="form-control" id="modifier_name">
                        </div>

                        <div class="form-group">
                            <label>Price</label>
                            <input type="number" class="form-control" id="price" placeholder="50">
                        </div>

                        <div class="form-group">
                            <label>VAT</label>
                            <input type="number" class="form-control" id="modifier_vat" value="{{ getRstaurantVAT() }}" min="0">
                        </div>

                        <div class="form-group">
                            <input type="checkbox" id="status" checked>&nbsp; Tick If Active
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                        <button type="button" id="add_modifier_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                    </div>
                </div>

            </div>
        </div>


        <div class="modal fade" id="edit_modifier_modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">x</button>
                        <h4 class="modal-title">Edit Modifier</h4>
                    </div>
                    <div class="modal-body">

                        <input type="hidden" id="edit_modifier_id" value="0">

                        <div class="form-group">
                            <label>Modifier Name</label>
                            <input type="text" class="form-control" id="edit_modifier_name">
                        </div>

                        <div class="form-group">
                            <label>Price</label>
                            <input type="number" class="form-control" id="edit_price" min="0">
                        </div>

                        <div class="form-group">
                            <label>VAT</label>
                            <input type="number" class="form-control" id="edit_modifier_vat" min="0">
                        </div>

                        <div class="form-group">
                            <input type="checkbox" id="edit_status" checked>&nbsp; Tick If Active
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                        <button type="button" id="edit_modifier_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                    </div>
                </div>

            </div>
        </div>

    @endif








@endsection
