@extends('layouts.main')

@section('title', 'Order')

@section('content')
    <style>
        @media print {
            body * {
                visibility: hidden;
            }
            #print_r, #print_r * {
                visibility: visible;
            }
            #print_r {
                position: absolute;
                left: 0;
                top: 0;
            }
        }
    </style>
    <script src="/js/order.js"></script>

    <input type="hidden" id="order_id" value="0">

    <div class="row text-center">
        <h3 style="font-family: 'Kaushan Script', cursive;" id="table_header"></h3>
    </div>

    <div class="row">

        @foreach($menu_items as $item)
            <script type="application/javascript">
                <?php echo 'pushToItemArray("'.$item->menu_item_id.'", "'.$item->menu_id.'", "'.$item->item_name.'", "'.$item->ratio.'", "'.$item->price.'", "'.$item->item_discount.'");'; ?>
            </script>
        @endforeach

        <script type="application/javascript">
            pushChargesInfo("{{$restaurant_data->vat_no}}", '{{$restaurant_data->vat_percentage}}', '{{$restaurant_data->service_charge}}');
        </script>

        @if ($restaurant_data->work_period_status == 0)

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">

                        <div class="bs-example top-margin-30" data-example-id="simple-jumbotron">
                            <div class="jumbotron">
                                <h1>Work Period Not Started!</h1>
                                <p>Please Contact Your Restaurant Administrator</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        @else

            <div class="col-md-12 col-sm-12 col-xs-12" id="table_div">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Select Table</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content" id="table_div_x_content">

                        @foreach($table_data as $table)

                            <div class="col-sm-2 col-xs-4" >
                                <div class="tableList">
                                    @if($table->order_status != "placed")
                                        <p>&nbsp;</p>
                                        <img src="/images/table_empty.png" class="table-box-100 pointer" onclick="select_table('{{ $table->table_id }}', '{{ $table->table_name }}')">
                                    @else
                                        <p>{{ date('g:i A', strtotime( $table->order_created )) }} / {{timeDifference($table->order_created)}}</p>
                                        <img src="/images/table_reserved.png" class="table-box-100 pointer" onclick="select_table_order('{{ $table->order_id }}', '{{ $table->table_id }}', '{{ $table->customer_id }}', '{{ $table->waiter_id }}', '{{ $table->order_type }}', '{{ $table->discount }}', '{{ $table->discount_reference }}', '{{ $table->payment_method }}', '{{ $table->card_name }}', '{{ $table->table_name }}')">
                                    @endif

                                    <h2> Table-{{ $table->table_name }}</h2>
                                </div>
                            </div>

                        @endforeach


                    </div>
                </div>
            </div>


            <div class="col-md-3 col-sm- col-xs-12 div-hide" id="menu_div">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Menus</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">


                        <table class="table text-center">

                            @php $color=0; @endphp

                            @foreach($menu_data as $menu)
                                <tr>
                                    @if($color%5 == 0)
                                        <td class="btn-warning btn-lg pointer" onclick="loadItems({{ $menu->menu_id }})">{{ $menu->menu_name }}</td>
                                    @elseif($color%5 == 1)
                                        <td class="btn-primary btn-lg pointer" onclick="loadItems({{ $menu->menu_id }})">{{ $menu->menu_name }}</td>
                                    @elseif($color%5 == 2)
                                        <td class="btn-success btn-lg pointer" onclick="loadItems({{ $menu->menu_id }})">{{ $menu->menu_name }}</td>
                                    @elseif($color%5 == 3)
                                        <td class="btn-info btn-lg pointer" onclick="loadItems({{ $menu->menu_id }})">{{ $menu->menu_name }}</td>
                                    @elseif($color%5 == 4)
                                        <td class="btn-danger btn-lg pointer" onclick="loadItems({{ $menu->menu_id }})">{{ $menu->menu_name }}</td>
                                    @endif
                                </tr>
                                @php $color++; @endphp
                            @endforeach


                        </table>

                    </div>
                </div>
            </div>


            <div class="col-md-6 col-sm-6 col-xs-12 div-hide" id="item_div">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Menu Items</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table class="table table-hover" id="menu_item_table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Item Name</th>
                                <th>Quantity/Ratio</th>
                                <th>Price</th>
                                <th>Discount</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody id="menu_item_div">


                            </tbody>
                        </table>

                    </div>
                </div>
            </div>


            <div class="col-md-3 col-sm-3 col-xs-12 div-hide" id="payment_div">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Payment Details</h2>
                        <ul class="nav navbar-right panel_toolbox">

                            <li>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#view_cart_modal">View Cart</button>
                            </li>

                        </ul>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">
                        <table class="table">
                            <tbody>
                            <tr>
                                <td class="active">SubTotal</td>
                                <td class="whiteBg cart_bill">0</td>
                            </tr>
                            <tr>
                                <td class="active">Discount</td>
                                <td class="whiteBg cart_discount">0</td>
                            </tr>
                            <tr>
                                <td class="active">Total</td>
                                <td class="whiteBg grand_total">0</td>
                            </tr>
                            <tr>
                                <td class="active">VAT ({{ $restaurant_data->vat_percentage }}%)</td>
                                <td class="whiteBg cart_vat">0</td>
                            </tr>
                            <tr>
                                <td class="active">Service Charge ({{ $restaurant_data->service_charge }}%)</td>
                                <td class="whiteBg cart_service_charge">0</td>
                            </tr>
                            <tr>
                                <td class="active">Grand Total</td>
                                <td class="whiteBg"><b class="cart_net_amaount">0</b></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="x_content col-lg-offset-1 div-hide" id="payment_button_div">

                        <td><button type="button" class="btn btn-warning btn-lg" id="history_back_button">Cancel</button></td>
                        <td><button type="button" class="btn btn-danger btn-lg" id="void_button">Void</button></td>
                        <td><button type="button" class="btn btn-success btn-lg" id="order_payment_button">Payment</button></td>

                    </div>
                </div>
            </div>


            <div class="clearfix"></div>


            <div class="div-hide" id="customer_details_div">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Customer Details</h2>
                        <ul class="nav navbar-right panel_toolbox">


                            <li>
                            <span class="fa-stack fa-lg" data-toggle="tooltip" data-placement="top" title="" data-original-title="Show last Receipt">
                                <i class="fa fa-square fa-stack-2x grey"></i>
                                <i class="fa fa-ticket fa-stack-1x fa-inverse dark-blue"></i>
                            </span>
                            </li>


                            <li>
                            <span data-toggle="modal" data-target="#add_customer_modal">
                            <span class="fa-stack fa-lg" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add New Customer">
                                <i class="fa fa-square fa-stack-2x grey"></i>
                                <i class="fa fa-user-plus fa-stack-1x fa-inverse dark-blue"></i>
                            </span>
                            </span>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">
                        <br />
                        <div class="form-horizontal form-label-left input_mask">

                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <select class="form-control radius-10" id="select_customer">
                                    @foreach($customer_data as $customer)
                                        <option value="{{ $customer->customer_id }}">{{ $customer->customer_name }}</option>
                                    @endforeach
                                </select>
                            </div>


                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <select class="form-control radius-10" id="select_served_by">
                                    @foreach($user_data as $user)
                                        <option value="{{ $user->id }}"
                                            {{ Auth::user()->id == $user->id? 'selected': '' }} >{{ $user->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <select class="form-control" id="select_order_type">
                                    <option value="dine-in">Dine In</option>
                                    <option value="take-away">Take Away</option>
                                </select>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <select class="form-control radius-10" id="select_table_number">
                                    @foreach($table_data as $table)
                                        <option value="{{ $table->table_id }}" >{{ $table->table_name }}</option>
                                    @endforeach
                                </select>
                            </div>



                            <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                                <input type="text" class="form-control has-feedback-left" id="inputSuccess4" placeholder="Bar Code">
                                <span class="fa fa-barcode form-control-feedback left" aria-hidden="true"></span>
                            </div>


                            <div>
                                <h4><strong>Discount Details</strong></h4>
                                <div class="clearfix"></div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Discount Amount</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <input type="text" class="form-control" placeholder="500" id="discount_amount">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Discount Reference</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <input type="text" class="form-control" placeholder="Victory Day" id="discount_reference">
                                </div>
                            </div>



                            <div>
                                <h4><strong>Payment Status</strong></h4>
                                <div class="clearfix"></div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Payment Method</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <select class="form-control" id="select_payment_type">
                                        <option value="cash">Cash</option>
                                        <option value="card">Card</option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group div-hide" id="select_card_type_div">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Card Type</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <select class="form-control" id="select_card_type">
                                        <option value="">--</option>
                                        <option value="Amex">Amex Card</option>
                                        <option value="BRAC">BRAC Bank</option>
                                        <option value="SCB">SCB</option>
                                        <option value="EBL">EBL</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4 ">
                                <button type="button" class="btn btn-primary" id="update_order_button" >Update Order Info</button>
                            </div>




                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-3 col-sm-2 col-xs-12 div-hide" id="previous_cart_div">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Previous Orders</h2>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">
                        <div class="table-responsive">
                            <table class="table table-striped jambo_table bulk_action" id="previous_cart_table" >
                                <thead>
                                <tr class="headings">
                                    <th class="column-title">Item Name</th>
                                    <th class="column-title">Qty</th>
                                    <th class="column-title"></th>
                                    </th>

                                </tr>
                                </thead>

                                <tbody id="previous_cart_item_div">


                                </tbody>
                            </table>
                        </div>


                    </div>
                </div>



            </div>


            <div class="div-hide" id="cart_div">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Cart</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li>
                            <span class="fa-stack fa-lg" data-toggle="tooltip" data-placement="top" data-original-title="Clear Cart" onclick="clearCart()">
                                <i class="fa fa-square fa-stack-2x grey"></i>
                                <i class="glyphicon glyphicon-trash fa-stack-1x fa-inverse dark-blue"></i>
                            </span>
                            </li>

                        </ul>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">
                        <div class="table-responsive">
                            <table class="table table-striped jambo_table bulk_action" id="cart_table" >
                                <thead>
                                <tr class="headings">
                                    <th class="column-title">#</th>
                                    <th class="column-title">Item Name</th>
                                    <th class="column-title">Unit Price</th>
                                    <th class="column-title">Quantity</th>
                                    <th class="column-title">Item Price</th>
                                    <th class="column-title no-link last"><span class="nobr">Action</span>
                                    </th>

                                </tr>
                                </thead>

                                <tbody id="cart_item_div" style="height: 100px;overflow-y: auto;">


                                </tbody>
                            </table>
                        </div>



                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4 ">
                                <button type="button" class="btn btn-danger" onclick="clearCart()" >Clear Cart</button>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#view_cart_modal">View Cart</button>

                            </div>
                        </div>

                    </div>
                </div>



            </div>


            <div class="clearfix"></div>

        @endif


    </div>


    <!--view cart Modal -->
    <div class="modal fade" id="view_cart_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content" style="margin-top: 200px; width: 120%">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">View Cart</h4>
                </div>
                <div class="modal-body">


                    <div class="form-group">
                        <h5><strong>Served By</strong></h5>
                        <p id="cart_served_by_name"></p>
                    </div>

                    <div class="form-group top-padding-10">
                        <h5><strong>Order Details</strong></h5>

                        <table class="table table-bordered" id="cart_table_modal">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Item Name</th>
                                <th>Price</th>
                                <th>Qty</th>
                                <th>Total</th>
                            </tr>
                            </thead>

                            <tbody id="cart_item_modal">

                            </tbody>
                        </table>

                    </div>


                    <div class="form-group top-padding-10">
                        <h5><strong>Billing Details</strong></h5>
                        <table class="table table-striped" style="width: 50%;">
                            <tbody>
                            <tr>
                                <td>Total</td>
                                <td class="cart_bill">0</td>
                            </tr>

                            <tr>
                                <td>Discount</td>
                                <td class="cart_discount">0</td>
                            </tr>

                            <tr>
                                <td>VAT({{ $restaurant_data->vat_percentage }}%)</td>
                                <td class="cart_vat">0</td>
                            </tr>

                            <tr>
                                <td>Service Charge({{ $restaurant_data->service_charge }}%)</td>
                                <td class="cart_service_charge">0</td>
                            </tr>


                            <tr>
                                <th>Net Amount</th>
                                <th class="cart_net_amaount">0</th>
                            </tr>

                            </tbody>

                        </table>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="print_cart_button" data-dismiss="modal">Print</button>
                    <button type="button" id="modify_order_button" class="btn btn-success" data-dismiss="modal" disabled>Modify Order</button>
                    <button type="button" id="place_order_button" class="btn btn-success" data-dismiss="modal" disabled>Place Order</button>
                </div>
            </div>

        </div>
    </div>
    <!-- end Modal -->

    <!--add customer Modal -->
    <div class="modal fade" id="add_customer_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content" style="margin-top: 200px; ">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Add Customer</h4>
                </div>
                <div class="modal-body">


                    <div class="form-group">
                        <label for="course_name">Customer Name</label>
                        <input type="text" class="form-control" id="modal_customer_name" placeholder="Javed Nayeem">
                    </div>

                    <div class="form-group">
                        <label for="description">Phone</label>
                        <input type="text" class="form-control" id="modal_customer_phone" placeholder="01715123456">
                    </div>

                    <div class="form-group">
                        <label for="description">Address</label>
                        <input type="text" class="form-control" id="modal_customer_address" placeholder="Dhanmondi">
                    </div>

                    <div class="form-group">
                        <label for="description">Email</label>
                        <input type="email" class="form-control" id="modal_customer_email" placeholder="customer@cupcake.com">
                    </div>



                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="add_customer_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                </div>
            </div>

        </div>
    </div>

    <!--payment Modal -->
    <div class="modal fade" id="order_payment_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content" id="order_payment_div">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                </div>
                <div class="modal-body">

                    <div class="x_title text-center">
                        <h4>{{ $restaurant_data->restaurants_name }}</h4>
                        <h5>{{ $restaurant_data->address }}</h5>
                        <h5>VAT Reg. No : {{ $restaurant_data->vat_no }}</h5>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_title form-group font-bold">
                        <h4>Customer : <a id="payment_modal_customer_name"></a></h4>
                        <h4>Served By : <a id="payment_modal_served_by"></a></h4>
                        <h4>Time : <script>document.write(new Date().toLocaleString())</script></h4>
                        <h4>Cashier : {{ Auth::user()->name }}</h4>
                        <h4>Table : <a id="payment_modal_table"></a></h4>
                    </div>


                    <div class="x_title table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th width="5%">#</th>
                                <th width="50%">Item Name</th>
                                <th>U.Price</th>
                                <th>Qty</th>
                                <th>Total</th>
                            </tr>
                            </thead>
                            <tbody id="payment_modal_order_table">

                            </tbody>
                        </table>
                    </div>

                    <div class="x_title form-group font-bold">
                        <a>Order Total : <a id="payment_modal_order_total">0</a></a><br>
                        <a>Service Charge ({{$restaurant_data->service_charge}}%) :<a id="payment_modal_service_charge">0</a></a><br>
                        <a>VAT Total ({{$restaurant_data->vat_percentage}}%): <a id="payment_modal_vat_total">0</a></a><br>
                        <p>Total Amount : <a id="payment_modal_total_amount">0</a></p>
                    </div>

                    <div class="x_title form-group font-bold">
                        <a>Payment Method : <a id="payment_modal_payment_method"></a></a><br>
                        <div id="paid_amount_change_div" class="div-hide">
                            <a>Paid Amount : <input id="payment_modal_paid_amount_input" type="number" style="border-radius: 6px;"></a><br>
                            <a>Change Amount : <a id="payment_modal_change_amount">0</a></a>
                        </div>
                    </div>

                    <h6 class="text-center">Thank You, Please Come Again</h6>



                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="confirm_payment_button" class="btn btn-success" data-dismiss="modal">OK</button>
                </div>
            </div>

        </div>
    </div>



@endsection
