@extends('layouts.main')

@section('title', 'Order New Version')

@section('content')


    <div class="row">

        @if (Auth::user()->license == 0)
            @include('cupcake.partials.expired-license')
        @else

            @if ($restaurant_data->work_period_status == 0)

                <div class="col-md-12 col-sm-12 col-xs-12 hidden-print">
                    <div class="x_panel">
                        <div class="x_content">

                            <div class="bs-example top-margin-30">
                                <div class="jumbotron">
                                    <h1>Work Period Not Started!</h1>
                                    <p>Please Contact Your Restaurant Administrator</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            @else

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">

                        <div class="x_title">
                            <h2>Select Table</h2>
                            <div class="clearfix"></div>
                        </div>

                        <div class="x_content" id="table_div_x_content">

                            @foreach($table_data as $table)

                                <a href="/table-order/{{ $table->table_id }}">

                                    <div class="col-sm-2 col-xs-4">
                                        <div class="tableList">

                                            @if($table->order_status == "guest_printed")
                                                <p>{{ date('g:i A', strtotime( $table->order_created )) }} / {{timeDifference($table->order_created)}}</p>
                                                <img src="/images/table_guest_printed.png" class="table-box-100 pointer">

                                            @elseif($table->order_status != "placed")
                                                <p>&nbsp;</p>
                                                @if($table->reserved == 1)
                                                    <img src="/images/table_reserved.png" class="table-box-100 pointer" >
                                                @else
                                                    <img src="/images/table_empty.png" class="table-box-100 pointer">
                                                @endif

                                            @else
                                                <p>{{ date('g:i A', strtotime( $table->order_created )) }} / {{timeDifference($table->order_created)}}</p>
                                                <img src="/images/table_guest.png" class="table-box-100 pointer">
                                            @endif

                                            <h2> Table-{{ $table->table_name }}</h2>
                                        </div>
                                    </div>

                                </a>
                            @endforeach

                        </div>
                    </div>
                </div>

            @endif

        @endif


    </div>








@endsection
