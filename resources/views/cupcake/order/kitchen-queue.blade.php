@extends('layouts.main')
@section('title', 'Kitchen Queue')

@section('content')

    @if($settings['refresh_timeout']>0)
        <meta http-equiv="refresh" content="{{ $settings['refresh_timeout'] }}" />
    @endif

    <script src="/js/queue.js"></script>

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a href="/view-queue" target="_blank" style="padding-top: 0px"><button type="button" class="btn btn-primary">View Queue</button></a></li>
                        <li><button type="button" class="btn btn-danger" id="clear_queue">Clear Queue</button></li>
                        <li><button type="button" class="btn btn-primary" onclick="reloadCurrentPage();">Refresh Queue</button></li>
                    </ul>

                    @if($order_data['count'] == 0)
                        <h1 class="text-center">No order is in queue</h1>
                    @else
                        <h1 class="text-center">Order #{{ $order_data['order_id'] }} in queue</h1>
                    @endif


                    <div class="clearfix"></div>
                </div>

                <div class="x_content">

                    <div class="table-responsive">
                        <table class="table table-striped jambo_table">
                            <thead>
                            <tr class="headings">
                                <th>#</th>
                                <th>Order Number</th>
                                <th>Table</th>
                                <th>Waiter</th>
                                <th>Order Type</th>
                                <th>Time</th>
                                <th class="text-center" width="30%">Order Items</th>
                                <th class="text-center" width="10%">Action</th>
                            </tr>
                            </thead>

                            <tbody>

                            @php $count=1; @endphp

                            @foreach($orders as $order)

                                @php
                                    $order_details = DB::table('order_details')->where('order_id', $order->order_id)->get();
                                @endphp

                                <tr id="order_{{ $order->order_id }}">
                                    <td scope="row">{{ $count++ }}</td>
                                    <td>{{ $order->order_id }}</td>
                                    <td>Table-{{ $order->table_name }}</td>
                                    <td>{{ $order->name }}</td>
                                    <td>{{ ucwords($order->order_type) }}</td>
                                    <td>{{ duration($order->created_at, date("Y-m-d H:i:s"), '%i Minutes') }}</td>
                                    <td>
                                        @foreach($order_details as $detail)
                                            <li><b>{{ $detail->menu_item_name }}: {{ $detail->item_quantity }}</b></li>
                                        @endforeach
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-primary" onclick="order_served({{ $order->order_id }})">Order Served</button>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>

                        </table>

                        {{ $orders->links() }}
                    </div>


                </div>
            </div>
        </div>
    </div>


@endsection
