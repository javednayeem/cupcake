@extends('layouts.main')

@section('title', 'Order History')

@section('content')
    <script src="/js/order.js"></script>

    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

    <div class="row">

        @foreach($menu_items as $item)
            <script type="application/javascript">
                <?php echo 'pushToItemArray("'.$item->menu_item_id.'", "'.$item->menu_id.'", "'.$item->item_name.'", "'.$item->ratio.'", "'.$item->price.'", "'.$item->item_discount.'");'; ?>
            </script>
        @endforeach

        <script type="application/javascript">

            pushChargesInfo("{{$restaurant_data->vat_no}}", '{{$restaurant_data->vat_percentage}}', '{{$restaurant_data->service_charge}}');

        </script>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Table design</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>

                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">

                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action" id="order_table">
                            <thead>
                            <tr class="headings">
                                <th class="column-title">Order #</th>
                                <th class="column-title">Waiter</th>
                                <th class="column-title">Table #</th>
                                <th class="column-title">Customer</th>
                                <th class="column-title">Order Type</th>
                                <th class="column-title">Amount</th>
                                <th class="column-title">Vat</th>
                                <th class="column-title">Service Charge</th>
                                <th class="column-title">Discount</th>
                                <th class="column-title">Total</th>
                                <th class="column-title">Status</th>
                                <th class="column-title">Time</th>
                                <th class="column-title no-link last"><span class="nobr">Action</span>
                                </th>
                            </tr>
                            </thead>

                            <tbody>

                            @foreach($orders as $order)
                                <tr class="odd pointer">
                                    <td>{{ $order->order_id }}</td>
                                    <td>{{ $order->name }}</td>
                                    <td>Table {{ $order->table_name }}</td>
                                    <td>{{ $order->customer_id==0? "Walk in Customer" : $order->customer_name }}</td>
                                    <td>{{ strtoupper($order->order_type) }}</td>
                                    <td>{{ $order->bill }}</td>
                                    <td>{{ $order->vat }}</td>
                                    <td>{{ $order->service_charge }}</td>
                                    <td>{{ $order->discount }}</td>
                                    <td>{{ $order->total_bill }}</td>
                                    <td>{{ ucfirst($order->order_status) }}</td>
                                    <td>{{   date('F j, Y \a\t g:ia', strtotime( $order->updated_at )) }}</td>
                                    <td><span class="pointer" onclick="get_order_details({{ $order->order_id }})">View</span> &nbsp; &nbsp; <span class="pointer">Request Void</span></td>
                                </tr>
                            @endforeach



                            </tbody>
                        </table>
                    </div>


                </div>
            </div>
        </div>

        <div class="pagination">
            {{ $orders->appends(Request::only('filter', 'search'))->links() }}
        </div>


        <!--view order details Modal -->
        <div class="modal fade" id="view_order_modal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content" style="margin-top: 200px;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">x</button>
                        <h4 class="modal-title" id="order_number">Order #0</h4>
                    </div>
                    <div class="modal-body">


                        <div class="form-group">
                            <h5><strong>Served By</strong></h5>
                            <p id="cart_served_by_name"></p>
                        </div>

                        <div class="form-group top-padding-10">
                            <h5><strong>Order Details</strong></h5>

                            <table class="table table-bordered" id="cart_table_modal">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Item Name</th>
                                    <th>Price</th>
                                    <th>Qty</th>
                                    <th>Total</th>
                                </tr>
                                </thead>

                                <tbody id="cart_item_modal">

                                </tbody>
                            </table>

                        </div>


                        <div class="form-group top-padding-10">
                            <h5><strong>Billing Details</strong></h5>
                            <table class="table table-striped" style="width: 50%;">
                                <tbody>
                                <tr>
                                    <td>Total</td>
                                    <td class="cart_bill">0</td>
                                </tr>

                                <tr>
                                    <td>Discount</td>
                                    <td class="cart_discount">0</td>
                                </tr>

                                <tr>
                                    <td>VAT({{ $restaurant_data->vat_percentage }}%)</td>
                                    <td class="cart_vat">0</td>
                                </tr>

                                <tr>
                                    <td>Service Charge({{ $restaurant_data->service_charge }}%)</td>
                                    <td class="cart_service_charge">0</td>
                                </tr>


                                <tr>
                                    <th>Net Amount</th>
                                    <th class="cart_net_amaount">0</th>
                                </tr>

                                </tbody>

                            </table>
                        </div>



                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
                    </div>
                </div>

            </div>
        </div>
        <!-- end Modal -->


@endsection
