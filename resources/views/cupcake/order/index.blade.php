@extends('layouts.main')

@section('title', 'Order')

@section('content')

    <link href="/css/order.css" rel="stylesheet">
    <script src="/js/order.js"></script>


    <input type="hidden" id="order_id" value="0">
    <input type="hidden" id="select_table_number" value="0">


    <div class="row text-center">
        <h3 style="font-family: 'Kaushan Script', cursive;" id="table_header"></h3>
    </div>


    <div class="row div-hide" id="customer_details_div">

        <div class="form-group" style="margin-top: 20px">
            <label class="control-label col-md-1 col-sm-1 col-xs-1 text-left" style="width: 50px;margin-top: 5px; margin-bottom: 40px">Waiter</label>
            <div class="col-md-1 col-sm-1 col-xs-12" style="width: 120px">
                <select class="form-control radius-10" id="waiter_id">

                    @if($restaurant_data->default_waiter=='1')
                        <option value="0">Select Waiter</option>
                    @endif

                    @foreach($user_data as $user)
                        <option value="{{ $user->id }}"
                                {{ Auth::user()->id == $user->id? 'selected': '' }} >{{ $user->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>


        <div class="form-group" style="margin-top: 20px">

            <div class="col-md-1 col-sm-1 col-xs-12">

                <button data-toggle="modal" data-target="#change_table_modal" class="btn btn-primary m-b-20">
                    <i class="fa fa-plus"></i> Change Table
                </button>

            </div>
        </div>


        <div class="form-group" style="margin-top: 20px">
            <label class="control-label col-md-2 col-sm-2 col-xs-12 text-right" style="margin-top: 5px; margin-bottom: 40px">Guest</label>
            <div class="col-md-4 col-sm-4 col-xs-12">

                <div class="col-md-8 col-sm-8 col-xs-8">
                    <select class="form-control radius-10" id="customer_id" style="width: 60%">
                        @foreach($customer_data as $customer)
                            <option value="{{ $customer->customer_id }}">{{ $customer->customer_phone . ' - '. $customer->customer_name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-md-4 col-sm-4 col-xs-4">
                    <button data-toggle="modal" data-target="#add_customer_modal" class="btn btn-primary m-b-20">
                        <i class="fa fa-plus"></i> Add Guest
                    </button>
                </div>

            </div>
        </div>


        <div class="form-group" style="margin-top: 20px">
            <label class="control-label col-md-1 col-sm-1 col-xs-1 text-right" style="margin-top: 5px; margin-bottom: 40px">Order Type</label>
            <div class="col-md-2 col-sm-2 col-xs-2">
                <select class="form-control" id="order_type">
                    @foreach($order_types as $type)
                        <option value="{{ $type->type_name }}">{{ ucwords($type->type_name) }}</option>
                    @endforeach
                </select>
            </div>
        </div>

    </div>


    <div class="row">

        @if (Auth::user()->license == 0)
            @include('cupcake.partials.expired-license')
        @else

            @foreach($setmenu_items as $item)
                <script type="application/javascript">
                    @php echo 'pushToSetMenuItemArray("'.$item->item_id.'", "'.$item->item_name.'", "'.$item->menu_item_id.'");' @endphp
                </script>
            @endforeach


            @foreach($menu_items as $item)
                <script type="application/javascript">
                    @php echo 'pushToItemArray("'.$item->menu_item_id.'", "'.$item->menu_id.'", "'.$item->item_name.'", "'.$item->ratio.'", "'.$item->price.'", "'.$item->item_discount.'", "'.$item->item_vat.'", "0", "0", "'.$item->set_menu.'", "'.$item->printer_id.'");' @endphp
                </script>
            @endforeach


            @foreach($modifiers as $modifier)
                <script type="application/javascript">
                    @php echo 'pushToModifiersArray("'.$modifier->modifier_id.'", "'.$modifier->modifier_name.'", "'.$modifier->price.'", "'.$modifier->modifier_vat.'");' @endphp
                </script>
            @endforeach


            @foreach($printers as $printer)
                <script type="application/javascript">
                    @php echo 'pushToKitchenToken("'.$printer->printer_id.'");' @endphp
                </script>
            @endforeach

            <script type="application/javascript">
                pushChargesInfo('{{$restaurant_data->vat_no}}', '{{$restaurant_data->vat_percentage}}', '{{$restaurant_data->service_charge}}', '{{$restaurant_data->guest_bill}}', '{{$restaurant_data->price_including_vat}}' ,'{{$restaurant_data->vat_after_discount}}', '{{$restaurant_data->sd_percentage}}', '{{$restaurant_data->service_charge_vat_percentage}}', '{{$restaurant_data->sd_vat_percentage}}', '{{$restaurant_data->kitchen_type}}', '{{$restaurant_data->pay_first}}', '{{$restaurant_data->item_box_size}}');
            </script>

            @if ($restaurant_data->work_period_status == 0)

                <div class="col-md-12 col-sm-12 col-xs-12 hidden-print">
                    <div class="x_panel">
                        <div class="x_content">

                            <div class="bs-example top-margin-30" data-example-id="simple-jumbotron">
                                <div class="jumbotron">
                                    <h1>Work Period Not Started!</h1>
                                    <p>Please Contact Your Restaurant Administrator</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            @else

                <div class="col-md-12 col-sm-12 col-xs-12 hidden-print" id="table_div">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Select Table</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content" id="table_div_x_content">

                            @foreach($table_data as $table)

                                <div class="col-sm-2 col-xs-4" >
                                    <div class="tableList">

                                        @if($table->order_status == "guest_printed")
                                            <p>{{ date('g:i A', strtotime( $table->order_created )) }} / {{timeDifference($table->order_created)}}</p>
                                            <img src="/images/table_guest_printed.png" class="table-box-100 pointer" onclick="select_table_order('{{ $table->order_id }}', '{{ $table->table_id }}', '{{ $table->customer_id }}', '{{ $table->waiter_id }}', '{{ $table->order_type }}', '{{ $table->discount }}', '{{ $table->discount_percentage }}', '{{ $table->discount_amount_type }}', '{{ $table->discount_reference }}', '{{ $table->table_name }}', '{{ $table->order_created }}')">

                                        @elseif($table->order_status != "placed")
                                            <p>&nbsp;</p>
                                            @if($table->reserved == 1)
                                                <img src="/images/table_reserved.png" class="table-box-100 pointer" onclick="select_table('{{ $table->table_id }}', '{{ $table->table_name }}')">
                                            @else
                                                <img src="/images/table_empty.png" class="table-box-100 pointer" onclick="select_table('{{ $table->table_id }}', '{{ $table->table_name }}')">
                                            @endif

                                        @else
                                            <p>{{ date('g:i A', strtotime( $table->order_created )) }} / {{timeDifference($table->order_created)}}</p>
                                            <img src="/images/table_guest.png" class="table-box-100 pointer" onclick="select_table_order('{{ $table->order_id }}', '{{ $table->table_id }}', '{{ $table->customer_id }}', '{{ $table->waiter_id }}', '{{ $table->order_type }}', '{{ $table->discount }}', '{{ $table->discount_percentage }}', '{{ $table->discount_amount_type }}', '{{ $table->discount_reference }}', '{{ $table->table_name }}', '{{ $table->order_created }}')">
                                        @endif

                                        <h2> Table-{{ $table->table_name }}</h2>
                                    </div>
                                </div>

                            @endforeach


                        </div>
                    </div>
                </div>


                <div class="col-md-2 col-sm-2 col-xs-12 div-hide hidden-print" id="menu_div">
                    <div class="x_panel" style="padding: 0px; border: none">
                        <div class="x_title top-margin-10">
                            <h2>Menu</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">


                            <table class="table text-center">

                                @php $color=0; @endphp

                                <div class="ranges">
                                    <ul id="ranges_nav">
                                        @foreach($menu_data as $menu)
                                            <li style="font-size: {{ $restaurant_data->menu_font_size }}px;background-color: {{ randomColorGenerate() }}" onclick="loadItems2('{{ $menu->menu_id }}', '{{ $menu->menu_name }}')">
                                                {{ $menu->menu_name }}
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>

                            </table>

                        </div>
                    </div>
                </div>


                <div class="col-md-6 col-sm-6 col-xs-12 div-hide hidden-print" id="item_div">
                    <div class="x_panel">

                        <div class="x_title">
                            <h2 id="menu_item_header">Menu Items</h2>
                            <div class="clearfix"></div>
                        </div>

                        <div id="item_div_span">
                        </div>
                    </div>

                </div>


                <div class="col-md-4 col-sm-4 col-xs-12 div-hide hidden-print" id="cart_div">


                    <div class="x_panel hidden-print">

                        <div class="x_content">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <td class="active">Total</td>
                                    <td class="whiteBg grand_total">0</td>
                                </tr>

                                @if($restaurant_data->vat_percentage > 0)
                                    <tr>
                                        <td class="active">VAT ({{ $restaurant_data->vat_percentage }}%)</td>
                                        <td class="whiteBg cart_vat">0</td>
                                    </tr>
                                @endif

                                @if($restaurant_data->service_charge > 0)
                                    <tr>
                                        <td class="active">Service Charge ({{ $restaurant_data->service_charge }}%)</td>
                                        <td class="whiteBg cart_service_charge">0</td>
                                    </tr>
                                @endif

                                @if($restaurant_data->sd_percentage > 0)
                                    <tr>
                                        <td class="active">SD ({{ $restaurant_data->sd_percentage }}%)</td>
                                        <td class="whiteBg cart_sd_percentage">0</td>
                                    </tr>
                                @endif

                                @if($restaurant_data->service_charge_vat_percentage > 0)
                                    <tr>
                                        <td class="active">Service Charge VAT ({{ $restaurant_data->service_charge_vat_percentage }}%)</td>
                                        <td class="whiteBg cart_service_charge_vat">0</td>
                                    </tr>
                                @endif


                                <tr>
                                    <td class="active">Grand Total</td>
                                    <td class="whiteBg"><b class="cart_net_amaount">0</b></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="">
                            <td><button type="button" class="btn btn-warning" id="history_back_button">Cancel</button></td>

                            <span class="div-hide" id="order_1_view">
                                <td><button type="button" class="btn btn-success place_order_button">Send KOT</button></td>

                                {{--@if($restaurant_data->guest_bill=='1')--}}
                                <td><button type="button" class="btn btn-dark" id="guest_print_button" onclick="printOrderPaymentReceipt('guest_printed')" disabled>Guest Print</button></td>
                                <td><button type="button" class="btn btn-success order_payment_button" disabled>Settle</button></td>

                                {{--@endif--}}
                            </span>



                            <span class="div-hide" id="order_2_view">
                                @if($restaurant_data->waiter_order_void=='1' || Auth::user()->role == 'superadmin' || Auth::user()->role == 'admin')
                                    <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#void_modal">Void</button></td>
                                @endif
                                <td><button type="button" id="modify_order_button" class="btn btn-success" disabled>Send KOT</button></td>



                                {{--@if($restaurant_data->guest_bill=='1')--}}
                                <td><button type="button" class="btn btn-dark" id="guest_print_button" onclick="printOrderPaymentReceipt('guest_printed')">Guest Print</button></td>
                                {{--@endif--}}

                                <td><button type="button" class="btn btn-success order_payment_button">Settle</button></td>
                                {{--<td><a type="button" href="" class="btn btn-success" id="order_settle">Settle</a></td>--}}

                            </span>

                            <td>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_discount_modal">
                                    Add Discount
                                </button>
                            </td>

                            <td><button type="button" class="btn btn-warning" id="order_instruction_button" disabled>Cooking Instruction</button></td>




                        </div>

                    </div>

                    <div class="x_panel hidden-print">
                        <div class="x_title">
                            <h2>Cart</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li>
                                    <button type="button" class="btn btn-danger" onclick="clearCart();">
                                        Clear Cart
                                    </button>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>

                        <div class="x_content">
                            <div class="table-responsive">
                                <table class="table table-striped jambo_table bulk_action" id="cart_table" >
                                    <thead>
                                    <tr class="headings">
                                        <th class="column-title">#</th>
                                        <th class="column-title">Item Name</th>
                                        <th class="column-title">Unit Price</th>
                                        <th class="column-title" width="20%">Quantity</th>
                                        <th class="column-title">Item Price</th>
                                        <th class="column-title no-link last" width="23%"><span class="nobr">Action</span>
                                        </th>
                                    </tr>
                                    </thead>

                                    <tbody id="cart_item_div" style="height: 100px;overflow-y: auto;">

                                    </tbody>
                                </table>
                            </div>

                            {{--<div class="form-group">--}}
                            {{--<div class="col-md-12 col-sm-12 col-xs-12">--}}
                            {{--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#view_cart_modal">View Cart</button>--}}

                            {{--</div>--}}
                            {{--</div>--}}
                        </div>
                    </div>

                    <div class="x_panel div-hide hidden-print" id="previous_cart_div">
                        <div class="x_title">
                            <h2>Previous Orders</h2>
                            <div class="clearfix"></div>
                        </div>

                        <div class="x_content">
                            <div class="table-responsive">
                                <table class="table table-striped jambo_table bulk_action">
                                    <thead>
                                    <tr class="headings">
                                        <th>Item Name</th>
                                        <th>Qty</th>
                                        <th>Action</th>
                                        </th>

                                    </tr>
                                    </thead>

                                    <tbody id="previous_cart_table">


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                </div>


                <div class="clearfix"></div>

                <div class="{{ $restaurant_data->order_receipt_debug==0?'div-hide':'' }}" id="order_receipt_div">

                    <div class="row">

                        <div class="div-hide" id="token_div">

                            @foreach($printers as $printer)
                                <div class="col-md-3 col-sm-2 col-xs-12" id="kitchen_token">

                                    <div class="x_panel">

                                        <div class="x_title text-center">
                                            <h4>{{ $restaurant_data->restaurants_name==''? "Cupcake":$restaurant_data->restaurants_name }}</h4>
                                            <h2 class="new_order_id"></h2>
                                            <h3 class="new_table_id"></h3>
                                            <h4 id="new_table_time">Time : @php echo date('g:i a, F j', time()); @endphp</h4>
                                            <h4>Waiter: <a class="token_waiter_name"></a></h4>
                                            <h4>{{ $printer->printer_name }}</h4>

                                            {{--<div class="print_button_div div-hide">--}}

                                            {{--<button type="button" class="btn btn-primary hidden-print"--}}
                                            {{--id="printTokenButton_{{ $printer->printer_id }}"--}}
                                            {{--onclick="printToken({{ $printer->printer_id }})">--}}
                                            {{--Print Token--}}
                                            {{--</button>--}}
                                            {{--</div>--}}

                                            <div class="clearfix"></div>
                                        </div>

                                        =========================

                                        <div class="x_content">
                                            <div class="table-responsive">
                                                <table class="table table-striped jambo_table bulk_action">
                                                    <thead>
                                                    <tr class="headings">
                                                        <th class="">Item Name</th>
                                                        <th class="text-right">Qty</th>
                                                    </tr>
                                                    </thead>

                                                    <tbody id="token_div_table_{{ $printer->printer_id }}" style="height: 100px;overflow-y: auto;">

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                        <div class="x_content">
                                            <div class="dashboard-widget-content top-margin-10 m-b-20">

                                                <b>Instructions</b>

                                                <ul class="" id="token_instruction_{{ $printer->printer_id }}">

                                                </ul>
                                            </div>
                                        </div>


                                    </div>

                                </div>
                            @endforeach






                        </div>

                    </div>


                    <div class="row">

                        <div class="col-md-3 col-sm-2 col-xs-12 div-hide" id="settle_div">
                            <div class="x_panel">

                                @include('cupcake.order.receipt')

                            </div>
                        </div>

                    </div>

                </div>


            @endif

        @endif


    </div>





    @include('cupcake.customers.add-customer')


    <div class="modal fade" id="order_payment_modal">
        <div class="modal-dialog">
            <div class="modal-content" id="order_payment_div" style="margin-top: 50px; left: 30%">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                </div>
                <div class="modal-body">

                    @if($restaurant_data->view_settle_receipt)
                        @include('cupcake.order.receipt')
                    @endif

                    <div class="form-group font-bold">
                        <a>Payment Method :
                            <select id="payment_method" class="form-control">
                                @foreach($payment_methods as $method)
                                    <option value="{{ $method->method_name }}">{{ ucfirst($method->method_name) }}</option>
                                @endforeach
                            </select>
                        </a>

                        <div class="row top-margin-20">

                            <div>
                                <div class="col-md-3 col-sm-3 col-xs-3 div-hide" id="bank_card_span">
                                    <div class="form-group">
                                        <label>Select Bank</label>
                                        <select class="form-control" id="bank_card">
                                            @foreach($bank_cards as $card)
                                                <option value="{{ $card->card_id }}">{{ $card->card_name }}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-3 col-xs-3 div-hide" id="card_number_span">
                                    <div class="form-group">
                                        <label id="card_no_label">Card No.</label>
                                        <input class="form-control" placeholder="6789" id="card_number">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label>Amount</label>
                                    <input type="number" min="0" class="form-control" placeholder="5500" id="paid_amount">
                                </div>
                            </div>

                        </div>


                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 top-margin-10">
                                <div class="form-group text-center">
                                    <button type="button" onclick="addPayment()" class="btn btn-success">Add Payment</button>
                                </div>
                            </div>

                        </div>

                        <div class="row">

                            <div>
                                <table class="table table-striped jambo_table">
                                    <thead>
                                    <tr>
                                        <th>Type</th>
                                        <th>Bank</th>
                                        <th>Card/Ref No.</th>
                                        <th class="text-right">Amount</th>
                                        <th width="10%">Action</th>
                                    </tr>
                                    </thead>

                                    <tbody id="payment_table">

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td class="text-right">
                                            Total: <b id="payment_total_amount">0</b>
                                        </td>
                                        <td></td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>

                        </div>

                        <hr>

                        <div id="paid_amount_change_div" class="top-margin-10">
                            <a>Paid Amount : <input id="total_paid_amount" type="number" style="border-radius: 6px;" value="0" disabled></a><br>
                            <a>Change Amount : <a id="change_amount">0</a></a>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="confirm_payment_button" class="btn btn-success" data-dismiss="modal" disabled>Settle</button>
                </div>
            </div>

        </div>
    </div>


    <div class="modal fade hidden-print" id="add_discount_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Add Discount</h4>
                </div>

                <div class="modal-body">

                    <input type="hidden" id="discount_amount_type" value="cash">

                    <div class="form-group">
                        <label for="course_name">Discount Amount</label>
                        <input type="text" class="form-control" id="discount_amount" value="0">
                    </div>

                    <div class="form-group">
                        <label for="course_name">Discount Amount in %</label>
                        <input type="text" class="form-control" id="discount_amount_percentage" value="0">
                    </div>

                    <div class="form-group">
                        <label for="description">Discount Reference</label>
                        <input type="text" class="form-control" id="discount_reference" placeholder="Victory Day">
                    </div>


                    <div class="form-group top-padding-10 div-hide">
                        <h5><strong>Billing Details</strong></h5>
                        <table class="table table-striped" style="width: 100%;">
                            <tbody>
                            <tr>
                                <td>Total</td>
                                <td id="order_amount_total">0</td>
                            </tr>

                            <tr>
                                <td>Discount</td>
                                <td class="discount_amount">0</td>
                            </tr>

                            <tr>
                                <th>Net Amount</th>
                                <th class="order_net_amaount">0</th>
                            </tr>

                            </tbody>

                        </table>
                    </div>

                </div>




                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="add_discount_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                </div>
            </div>

        </div>
    </div>


    <div class="modal fade" id="change_table_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Change Table</h4>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label for="course_name">Table Name</label>
                        <select class="form-control radius-10" id="select_table">

                            @foreach($table_data as $table)

                                <option value="{{ $table->table_id }}"
                                        {{ $table->order_status=="placed"?'disabled':'' }}>Table-{{ $table->table_name }}
                                </option>

                            @endforeach
                        </select>
                    </div>




                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="change_table_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                </div>
            </div>

        </div>
    </div>


    <div class="modal fade" id="add_modifier_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Add Modifier</h4>
                </div>
                <div class="modal-body">

                    <input type="hidden" id="modifier_item_id">

                    <div class="form-group">
                        <label for="course_name">Modifier Name</label>
                        <select class="form-control radius-10" id="modifier_id">

                            @foreach($modifiers as $modifier)
                                <option value="{{ $modifier->modifier_id }}">{{ $modifier->modifier_name }}</option>
                            @endforeach
                        </select>
                    </div>



                    <div class="row">

                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="form-group">
                                <label for="course_name">Quantity</label>
                                <input type="number" class="form-control" id="modifier_quantity" value="1" min="1">
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="form-group">
                                <label for="course_name">Unit Price</label>
                                <input type="text" class="form-control" id="modifier_unit_price" readonly>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="form-group">
                                <label for="course_name">Total Price</label>
                                <input type="text" class="form-control" id="modifier_total_price" readonly>
                            </div>
                        </div>

                    </div>



                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="add_modifier_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                </div>
            </div>

        </div>
    </div>


    <div class="modal fade" id="order_instruction_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Cooking Instructions</h4>
                </div>
                <div class="modal-body">

                    <input type="hidden" id="order_notes">
                    {{--<input type="hidden" id="edit_instruction_id" value="0">--}}

                    <div class="row">

                        <div class="col-md-7 col-sm-7 col-xs-7">
                            <div class="form-group">
                                <label for="instruction_id">Select Printer</label>
                                <select class="form-control radius-10" id="instruction_printer_id">

                                    @foreach($printers as $printer)
                                        <option value="{{ $printer->printer_id }}">{{ $printer->printer_name }}</option>
                                    @endforeach
                                </select>

                            </div>
                        </div>



                    </div>

                    <div class="row">

                        <div class="col-md-7 col-sm-7 col-xs-7">
                            <div class="form-group">
                                <label for="instruction_id">Instruction</label>
                                <select class="form-control radius-10" id="instruction_id">

                                    @foreach($order_instructions as $instruction)
                                        <option value="{{ $instruction->instruction_id }}">{{ $instruction->instruction }}</option>
                                    @endforeach
                                </select>

                            </div>
                        </div>

                        <div class="col-md-3 col-sm-3 col-xs-3" style="margin-top: 24px;">
                            <div class="form-group">
                                <button type="button" id="add_instruction_button" class="btn btn-success">Add Instruction</button>
                            </div>
                        </div>

                    </div>


                    <div class="row">

                        <div class="col-md-7 col-sm-7 col-xs-7">
                            <div class="form-group">
                                <label for="course_name">Text Instruction</label>
                                <textarea class="form-control radius-5" id="textarea_instruction"></textarea>
                            </div>
                        </div>



                        <div class="col-md-3 col-sm-3 col-xs-3 top-margin-30">
                            <div class="form-group">
                                <button type="button" id="textarea_instruction_button" class="btn btn-success">Add Instruction</button>
                            </div>
                        </div>

                    </div>


                    <div class="row top-margin-40">
                        <div class="x_content">
                            <div class="">
                                <table class="table table-striped jambo_table">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Instruction</th>
                                        <th width="20%">Action</th>
                                    </tr>
                                    </thead>

                                    <tbody id="instruction_table">



                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
                </div>
            </div>

        </div>
    </div>


    <div class="modal fade" id="void_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Order Void</h4>
                </div>

                <div class="modal-body">
                    <form autocomplete="off">

                        <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text" class="form-control" id="username">
                        </div>
                    </form>

                    <form autocomplete="off">

                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password">
                        </div>
                    </form>

                    <div class="form-group">
                        <label for="reason">Void Reason</label>
                        <textarea class="form-control radius-5" id="reason"></textarea>
                    </div>


                    <div class="form-group">
                        <label for="notes">Void Notes</label>
                        <textarea class="form-control radius-5" id="notes"></textarea>
                    </div>



                </div>




                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="request_void_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                </div>
            </div>

        </div>
    </div>


    <div class="modal fade" id="void_item_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Item Void</h4>
                </div>

                <div class="modal-body">

                    <input type="hidden" id="void_order_details_id">
                    <input type="hidden" id="void_item_quantity_max">

                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" class="form-control" id="item_username">
                    </div>

                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="item_password">
                    </div>


                    <div class="form-group">
                        <label for="username">Void Item Quantity</label>
                        <input type="number" class="form-control" id="void_item_quantity">
                    </div>


                    <div class="form-group">
                        <label for="reason">Void Reason</label>
                        <textarea class="form-control radius-5" id="item_reason"></textarea>
                    </div>


                    <div class="form-group">
                        <label for="notes">Void Notes</label>
                        <textarea class="form-control radius-5" id="item_notes"></textarea>
                    </div>

                </div>




                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="request_void_item_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                </div>
            </div>

        </div>
    </div>


    <div class="modal fade" id="item_quantity_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Item Quantity</h4>
                </div>

                <div class="modal-body">

                    <input type="hidden" id="item_id">

                    <div class="form-group">
                        <label for="username">Item Quantity</label>
                        <input type="number" class="form-control" id="item_amount">
                    </div>

                </div>




                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="set_item_quantity" class="btn btn-success" data-dismiss="modal">Ok</button>
                </div>
            </div>

        </div>
    </div>


    <div class="modal fade" id="add_item_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Open Food Item Add</h4>
                </div>

                <div class="modal-body">

                    <input type="hidden" id="open_food_menu_id">

                    <div class="form-group">
                        <label for="username">Item Name</label>
                        <input type="text" class="form-control" id="open_food_item_name">
                    </div>

                    <div class="form-group">
                        <label for="username">Price</label>
                        <input type="text" class="form-control" id="open_food_price">
                    </div>


                    <div class="form-group">
                        <label for="username">Item VAT</label>
                        <input type="text" class="form-control" id="open_food_item_vat">
                    </div>


                    <div class="form-group">
                        <label for="username">Select Printer</label>
                        <select class="form-control" id="open_food_printer_id">
                            @foreach($printers as $printer)
                                <option value="{{ $printer->printer_id }}">{{ $printer->printer_name }}</option>
                            @endforeach
                        </select>
                    </div>

                </div>




                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="add_item_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                </div>
            </div>

        </div>
    </div>


    <div class="modal fade" id="complimentary_item_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Complimentary Item</h4>
                </div>

                <div class="modal-body">

                    <input type="hidden" id="complimentary_order_details_id">
                    <input type="hidden" id="complimentary_item_quantity_max">


                    <div class="form-group">
                        <label for="complimentary_item_quantity">Complimentary Item Quantity <span class="text-danger">*</span></label>
                        <input type="number" class="form-control" id="complimentary_item_quantity">
                    </div>


                    <div class="form-group">
                        <label for="complimentary_notes">Complimentary Notes <span class="text-danger">*</span></label>
                        <textarea class="form-control radius-5" id="complimentary_notes"></textarea>
                    </div>

                </div>




                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="request_complimentary_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                </div>
            </div>

        </div>
    </div>



@endsection
