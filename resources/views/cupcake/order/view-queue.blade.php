@extends('layouts.app')
@section('title', 'Kitchen Queue')

@section('content')

    <script src="/js/queue.js"></script>

    <div class="row">
        <div class="col-md-12">
            <div class="col-md-12 col-lg-12 col-sm-12 text-center" style="text-align: center">
                @if($order_data['count']>0)
                    <h1 style="font-size: 320px" id="queue_span">Token #{{ $order_data['order_id'] }}</h1>
                @else
                    <h1 style="font-size: 320px" id="queue_span">No Order In Queue</h1>
                @endif
                {{--<h1>Please collect your order from the counter</h1>--}}
            </div>
        </div>
    </div>


    <script>
        queueTimer();
    </script>

@endsection
