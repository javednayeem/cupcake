@extends('layouts.main')

@section('title', 'Settle')

@section('content')

    <link href="/css/order.css" rel="stylesheet">
    <script src="/js/settle.js?v={{ time() }}"></script>


    <input type="hidden" id="order_id" value="0">


    <div class="row">

        @if (Auth::user()->license == 0)
            @include('cupcake.partials.expired-license')
        @else


            <div class="row">

                <div class="col-md-2 col-sm-2 col-xs-12 hidden-print">
                    <div class="x_panel" style="padding: 0px; border: none">

                        <div class="x_content">

                            <table class="table text-center">

                                <div class="ranges">
                                    <ul>

                                        <li style="font-size: {{ $restaurant_data->menu_font_size }}px; background-color: {{ randomColorGenerate() }}">
                                            Cash
                                        </li>

                                        <li style="font-size: {{ $restaurant_data->menu_font_size }}px; background-color: {{ randomColorGenerate() }}">
                                            Admin A/C
                                        </li>

                                        <li style="font-size: {{ $restaurant_data->menu_font_size }}px; background-color: {{ randomColorGenerate() }}">
                                            Visa Card
                                        </li>

                                        <li style="font-size: {{ $restaurant_data->menu_font_size }}px; background-color: {{ randomColorGenerate() }}">
                                            Master Card
                                        </li>

                                        <li style="font-size: {{ $restaurant_data->menu_font_size }}px; background-color: {{ randomColorGenerate() }}">
                                            AMEX Card
                                        </li>

                                    </ul>
                                </div>

                            </table>

                        </div>
                    </div>
                </div>


                <div class="col-md-7 col-sm-7 col-xs-12 hidden-print">
                    <div class="x_panel" style="padding: 0px; border: none">

                        <div class="x_content">

                            <div class="col-md-3 col-sm-3 col-xs-3 hidden-print">

                                <table class="table text-center">

                                    <div class="ranges">
                                        <ul>

                                            <li style="font-size: {{ $restaurant_data->menu_font_size }}px; background-color: #26B99A" onclick="giveNote(1)">
                                                1
                                            </li>

                                            <li style="font-size: {{ $restaurant_data->menu_font_size }}px; background-color: #26B99A" onclick="giveNote(2)">
                                                2
                                            </li>

                                            <li style="font-size: {{ $restaurant_data->menu_font_size }}px; background-color: #26B99A" onclick="giveNote(5)">
                                                5
                                            </li>

                                            <li style="font-size: {{ $restaurant_data->menu_font_size }}px; background-color: #26B99A" onclick="giveNote(10)">
                                                10
                                            </li>

                                            <li style="font-size: {{ $restaurant_data->menu_font_size }}px; background-color: #26B99A" onclick="giveNote(20)">
                                                20
                                            </li>

                                            <li style="font-size: {{ $restaurant_data->menu_font_size }}px; background-color: #26B99A" onclick="giveNote(50)">
                                                50
                                            </li>

                                            <li style="font-size: {{ $restaurant_data->menu_font_size }}px; background-color: #26B99A" onclick="giveNote(100)">
                                                100
                                            </li>

                                            <li style="font-size: {{ $restaurant_data->menu_font_size }}px; background-color: #26B99A" onclick="giveNote(500)">
                                                500
                                            </li>

                                            <li style="font-size: {{ $restaurant_data->menu_font_size }}px; background-color: #26B99A" onclick="giveNote(1000)">
                                                1000
                                            </li>

                                        </ul>
                                    </div>

                                </table>

                            </div>


                            <div class="col-md-9 col-sm-9 col-xs-9 hidden-print">

                                <div class="x_panel">

                                    <div class="x_content">

                                        <form class="form-horizontal form-label-left">

                                            <div class="form-group">

                                                <div class="col-sm-12">
                                                    <div class="input-group">
                                                        <span class="input-group-btn">
                                                            <button type="button" class="btn btn-primary">Total Amount</button>
                                                        </span>
                                                        <input type="text" id="total_amount" class="form-control" style="border-radius: 5px" value="{{ $order->total_bill }}" disabled>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12">
                                                    <div class="input-group">
                                                        <span class="input-group-btn">
                                                            <button type="button" class="btn btn-primary">Paid Amount</button>
                                                        </span>
                                                        <input type="text" class="form-control" style="border-radius: 5px" id="paid_amount">
                                                    </div>
                                                </div>

                                                <div class="col-sm-12">
                                                    <div class="input-group">
                                                        <span class="input-group-btn">
                                                            <button type="button" class="btn btn-primary">Change Amount</button>
                                                        </span>
                                                        <input type="text" id="change_amount" class="form-control" style="border-radius: 5px">
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="divider-dashed"></div>


                                        </form>
                                    </div>

                                    <div id="item_div_span">

                                        <div class="col-md-4 pointer" onclick="addNumber(1)">
                                            <div class="panel panel-default text-center text-white slider-bg m-b-0" style="height: 100px; background-color: white">
                                                <div class="panel-body p-0">
                                                    <div class="owl-carousel owl-loaded owl-drag">
                                                        <div class="owl-nav" style="color: black">
                                                            <h1 class="top-margin-10"><strong>1</strong></h1>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4 pointer" onclick="addNumber(2)">
                                            <div class="panel panel-default text-center text-white slider-bg m-b-0" style="height: 100px; background-color: white">
                                                <div class="panel-body p-0">
                                                    <div class="owl-carousel owl-loaded owl-drag">
                                                        <div class="owl-nav" style="color: black">
                                                            <h1 class="top-margin-10"><strong>2</strong></h1>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4 pointer" onclick="addNumber(3)">
                                            <div class="panel panel-default text-center text-white slider-bg m-b-0" style="height: 100px; background-color: white">
                                                <div class="panel-body p-0">
                                                    <div class="owl-carousel owl-loaded owl-drag">
                                                        <div class="owl-nav" style="color: black">
                                                            <h1 class="top-margin-10"><strong>3</strong></h1>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-4 pointer" onclick="addNumber(4)">
                                            <div class="panel panel-default text-center text-white slider-bg m-b-0" style="height: 100px; background-color: white">
                                                <div class="panel-body p-0">
                                                    <div class="owl-carousel owl-loaded owl-drag">
                                                        <div class="owl-nav" style="color: black">
                                                            <h1 class="top-margin-10"><strong>4</strong></h1>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4 pointer" onclick="addNumber(5)">
                                            <div class="panel panel-default text-center text-white slider-bg m-b-0" style="height: 100px; background-color: white">
                                                <div class="panel-body p-0">
                                                    <div class="owl-carousel owl-loaded owl-drag">
                                                        <div class="owl-nav" style="color: black">
                                                            <h1 class="top-margin-10"><strong>5</strong></h1>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4 pointer" onclick="addNumber(6)">
                                            <div class="panel panel-default text-center text-white slider-bg m-b-0" style="height: 100px; background-color: white">
                                                <div class="panel-body p-0">
                                                    <div class="owl-carousel owl-loaded owl-drag">
                                                        <div class="owl-nav" style="color: black">
                                                            <h1 class="top-margin-10"><strong>6</strong></h1>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-4 pointer" onclick="addNumber(7)">
                                            <div class="panel panel-default text-center text-white slider-bg m-b-0" style="height: 100px; background-color: white">
                                                <div class="panel-body p-0">
                                                    <div class="owl-carousel owl-loaded owl-drag">
                                                        <div class="owl-nav" style="color: black">
                                                            <h1 class="top-margin-10"><strong>7</strong></h1>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-4 pointer" onclick="addNumber(8)">
                                            <div class="panel panel-default text-center text-white slider-bg m-b-0" style="height: 100px; background-color: white">
                                                <div class="panel-body p-0">
                                                    <div class="owl-carousel owl-loaded owl-drag">
                                                        <div class="owl-nav" style="color: black">
                                                            <h1 class="top-margin-10"><strong>8</strong></h1>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>



                                        <div class="col-md-4 pointer" onclick="addNumber(9)">
                                            <div class="panel panel-default text-center text-white slider-bg m-b-0" style="height: 100px; background-color: white">
                                                <div class="panel-body p-0">
                                                    <div class="owl-carousel owl-loaded owl-drag">
                                                        <div class="owl-nav" style="color: black">
                                                            <h1 class="top-margin-10"><strong>9</strong></h1>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-4 pointer" >
                                            <div class="panel panel-default text-center text-white slider-bg m-b-0" style="height: 100px; background-color: white">
                                                <div class="panel-body p-0">
                                                    <div class="owl-carousel owl-loaded owl-drag">
                                                        <div class="owl-nav" style="color: black">
                                                            <h1 class="top-margin-10"><strong>.</strong></h1>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-4 pointer" onclick="addNumber(0)">
                                            <div class="panel panel-default text-center text-white slider-bg m-b-0" style="height: 100px; background-color: white">
                                                <div class="panel-body p-0">
                                                    <div class="owl-carousel owl-loaded owl-drag">
                                                        <div class="owl-nav" style="color: black">
                                                            <h1 class="top-margin-10"><strong>0</strong></h1>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-4 pointer" onclick="clearPaidAmount()">
                                            <div class="panel panel-default text-center text-white slider-bg m-b-0" style="height: 100px; background-color: white">
                                                <div class="panel-body p-0">
                                                    <div class="owl-carousel owl-loaded owl-drag">
                                                        <div class="owl-nav" style="color: black">
                                                            <h1 class="top-margin-10"><strong>C</strong></h1>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>



                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>


                {{--<div class="col-md-3 col-sm-3 col-xs-12 hidden-print" >--}}

                    {{--<div class="x_panel hidden-print">--}}

                        {{--<div class="x_content">--}}

                            {{--<table class="table">--}}
                                {{--<tbody>--}}
                                {{--<tr>--}}
                                    {{--<td class="active">Total</td>--}}
                                    {{--<td class="whiteBg">{{ $order->bill }}</td>--}}
                                {{--</tr>--}}

                                {{--@if($restaurant_data->vat_percentage > 0)--}}
                                    {{--<tr>--}}
                                        {{--<td class="active">VAT ({{ $restaurant_data->vat_percentage }}%)</td>--}}
                                        {{--<td class="whiteBg">{{ $order->vat }}</td>--}}
                                    {{--</tr>--}}
                                {{--@endif--}}

                                {{--@if($restaurant_data->service_charge > 0)--}}
                                    {{--<tr>--}}
                                        {{--<td class="active">Service Charge ({{ $restaurant_data->service_charge }}%)</td>--}}
                                        {{--<td class="whiteBg">{{ $order->service_charge }}</td>--}}
                                    {{--</tr>--}}
                                {{--@endif--}}

                                {{--@if($restaurant_data->sd_percentage > 0)--}}
                                    {{--<tr>--}}
                                        {{--<td class="active">SD ({{ $restaurant_data->sd_percentage }}%)</td>--}}
                                        {{--<td class="whiteBg">{{ $order->sd_charge }}</td>--}}
                                    {{--</tr>--}}
                                {{--@endif--}}

                                {{--@if($restaurant_data->service_charge_vat_percentage > 0)--}}
                                    {{--<tr>--}}
                                        {{--<td class="active">Service Charge VAT ({{ $restaurant_data->service_charge_vat_percentage }}%)</td>--}}
                                        {{--<td class="whiteBg">{{ $order->service_charge_vat }}</td>--}}
                                    {{--</tr>--}}
                                {{--@endif--}}


                                {{--<tr>--}}
                                    {{--<td class="active">Grand Total</td>--}}
                                    {{--<td class="whiteBg"><b>{{ $order->total_bill }}</b></td>--}}
                                {{--</tr>--}}

                                {{--</tbody>--}}
                            {{--</table>--}}

                            {{--<span class="text-center">--}}
                                {{--<button type="button" class="btn btn-success text-custom">Settle Payment</button>--}}
                            {{--</span>--}}

                            {{--<div class="table-responsive">--}}
                                {{--<hr>--}}
                                {{--<table class="table">--}}
                                    {{--<thead>--}}
                                    {{--<tr id="receipt_table_header">--}}
                                        {{--<th width="5%">#</th>--}}
                                        {{--<th width="50%">Item Name</th>--}}
                                        {{--<th>U.Price</th>--}}
                                        {{--<th>Qty</th>--}}
                                        {{--<th class="text-right">Total</th>--}}
                                    {{--</tr>--}}
                                    {{--</thead>--}}
                                    {{--<tbody class="receipt_order_table">--}}

                                    {{--</tbody>--}}
                                {{--</table>--}}
                            {{--</div>--}}

                        {{--</div>--}}
                    {{--</div>--}}

                {{--</div>--}}

                <div class="col-md-3 col-sm-3 col-xs-12" id="settle_div">
                    <div class="x_panel">

                        @include('cupcake.order.receipt')

                    </div>
                </div>
            </div>




            {{--<div class="row">--}}

                {{--<div class="col-md-3 col-sm-3 col-xs-12" id="settle_div">--}}
                    {{--<div class="x_panel">--}}

                        {{--@include('cupcake.order.receipt')--}}

                    {{--</div>--}}
                {{--</div>--}}

            {{--</div>--}}



            {{--<div class="row">--}}

                {{--<div class="col-md-3 col-sm-3 col-xs-12" id="settle_div">--}}
                    {{--<div class="x_panel">--}}

                        {{--<div class="text-center">--}}
                            {{--<img src="/images/restaurant-logo/{{ getRstaurantLogo() }}" id="logo-icon" width="20%">--}}
                            {{--<h4><b>{{ $restaurant_data->restaurants_name==''? "Cupcake":$restaurant_data->restaurants_name }}</b></h4>--}}
                            {{--<h5 id="receipt_address_div">--}}
                                {{--<b>--}}
                                    {{--{{ $restaurant_data->address}}--}}
                                    {{--{{ $restaurant_data->area==null? '':', '.$restaurant_data->area }}--}}
                                    {{--{{ $restaurant_data->city==null? '':', '.$restaurant_data->city }}--}}
                                {{--</b>--}}
                            {{--</h5>--}}
                            {{--<h5><b>Phone : {{ $restaurant_data->phone_number==null? '':$restaurant_data->phone_number }}</b></h5>--}}
                            {{--<h5 id="receipt_vat_div"><b>VAT Reg. No : {{ $restaurant_data->vat_no==null? 'N/A':$restaurant_data->vat_no }}</b></h5>--}}
                            {{--<br>--}}
                            {{--<h3 style=""><b class="bill_type"></b></h3>--}}
                        {{--</div>--}}

                        {{--<br>--}}
                        {{--<div class="form-group font-bold">--}}
                            {{--<table class="table">--}}
                                {{--<tbody>--}}
                                {{--<tr class="text-center"><td><b>Table No: <a class="receipt_table_no"></a></b></td></tr>--}}
                                {{--<tr><td><strong>Guest : <a class="receipt_customer_name"></a></strong></td></tr>--}}
                                {{--<tr><td><strong>Served By : <a class="receipt_waiter_name"></a></strong></td></tr>--}}
                                {{--<tr><td><strong>Time : <a class="receipt_created_at">@php echo date('F j, Y, g:i a', time()); @endphp</a></strong></td></tr>--}}
                                {{--<tr><td><strong><a id="tr_receipt_kot">Invoice No</a> : <a class="receipt_order_no"></a></strong></td></tr>--}}
                                {{--<tr><td><strong>Order Type : <a class="receipt_order_type"></a></strong></td></tr>--}}
                                {{--</tbody>--}}
                            {{--</table>--}}
                        {{--</div>--}}


                        {{--<div class="table-responsive">--}}
                            {{--<hr>--}}
                            {{--<table class="table">--}}
                                {{--<thead>--}}
                                {{--<tr id="receipt_table_header">--}}
                                    {{--<th width="5%">#</th>--}}
                                    {{--<th width="50%">Item Name</th>--}}
                                    {{--<th>U.Price</th>--}}
                                    {{--<th>Qty</th>--}}
                                    {{--<th class="text-right">Total</th>--}}
                                {{--</tr>--}}
                                {{--</thead>--}}
                                {{--<tbody class="receipt_order_table">--}}

                                {{--</tbody>--}}
                            {{--</table>--}}
                        {{--</div>--}}


                        {{--<div class="form-group font-bold" id="receipt_charges_div">--}}
                            {{--<hr>--}}
                            {{--<table class="table">--}}
                                {{--<thead>--}}
                                {{--<tr>--}}
                                    {{--<th width="85%"></th>--}}
                                    {{--<th width="15%"></th>--}}
                                {{--</tr>--}}
                                {{--</thead>--}}
                                {{--<tbody>--}}
                                {{--<tr>--}}
                                    {{--<td>SubTotal : </td>--}}
                                    {{--<td class="text-right"><a class="receipt_subtotal">0</a></td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                    {{--<td>VAT ({{$restaurant_data->vat_percentage}}%):<h6 id="receipt_vat_header"></h6></td>--}}
                                    {{--<td class="text-right"><a class="receipt_vat_total">0</a></td>--}}
                                {{--</tr>--}}

                                {{--@if($restaurant_data->service_charge>0)--}}
                                    {{--<tr>--}}
                                        {{--<td>Service Charge ({{$restaurant_data->service_charge}}%) : </td>--}}
                                        {{--<td class="text-right"><a class="receipt_service_charge">0</a></td>--}}
                                    {{--</tr>--}}
                                {{--@endif--}}


                                {{--@if($restaurant_data->sd_percentage>0)--}}
                                    {{--<tr>--}}
                                        {{--<td>SD ({{$restaurant_data->sd_percentage}}%) : </td>--}}
                                        {{--<td class="text-right"><a class="receipt_sd_percentage">0</a></td>--}}
                                    {{--</tr>--}}
                                {{--@endif--}}


                                {{--@if($restaurant_data->service_charge_vat_percentage>0)--}}
                                    {{--<tr>--}}
                                        {{--<td>Service Charge VAT ({{$restaurant_data->service_charge_vat_percentage}}%) : </td>--}}
                                        {{--<td class="text-right"><a class="receipt_service_charge_vat_percentage">0</a></td>--}}
                                    {{--</tr>--}}
                                {{--@endif--}}


                                {{--@if($restaurant_data->sd_vat_percentage>0)--}}
                                    {{--<tr>--}}
                                        {{--<td>SD VAT ({{$restaurant_data->sd_vat_percentage}}%) : </td>--}}
                                        {{--<td class="text-right"><a class="receipt_sd_vat_percentage">0</a></td>--}}
                                    {{--</tr>--}}
                                {{--@endif--}}



                                {{--<tr>--}}
                                    {{--<td>Discount (<a class="receipt_discount_percentage">0</a>) : </td>--}}
                                    {{--<td class="text-right"><a class="receipt_discount_amount">0</a></td>--}}
                                {{--</tr>--}}

                                {{--<tr>--}}
                                    {{--<td>Total Amount : <h6>(Rounded)</h6></td>--}}
                                    {{--<td class="text-right"><a class="receipt_total_amount">0</a></td>--}}
                                {{--</tr>--}}
                                {{--<hr>--}}

                                {{--<tr class="cash_given_return">--}}
                                    {{--<td>Given Amount :</td>--}}
                                    {{--<td class="text-right"><a class="receipt_given_amount">0</a></td>--}}
                                {{--</tr>--}}

                                {{--<tr class="cash_given_return">--}}
                                    {{--<td>Return Amount :</td>--}}
                                    {{--<td class="text-right"><a class="receipt_return_amount">0</a></td>--}}
                                {{--</tr>--}}

                                {{--</tbody>--}}
                            {{--</table>--}}

                        {{--</div>--}}

                        {{--<br><br>--}}

                        {{--<div class="table-responsive top-margin-10" id="receipt_payment_div">--}}
                            {{--<hr>--}}
                            {{--<h6 class="text-center">Payment Methods</h6>--}}
                            {{--<table class="table">--}}
                                {{--<thead>--}}
                                {{--<tr>--}}
                                    {{--<th>Type</th>--}}
                                    {{--<th>Bank</th>--}}
                                    {{--<th>Card No.</th>--}}
                                    {{--<th class="text-right">Amount</th>--}}
                                {{--</tr>--}}
                                {{--</thead>--}}
                                {{--<tbody class="payment_table">--}}

                                {{--</tbody>--}}
                            {{--</table>--}}
                        {{--</div>--}}


                        {{--<div class="text-center" id="receipt_footer_div">--}}
                            {{--<hr>--}}
                            {{--<h6 class="text-center">{{ $restaurant_data->receipt_footer }}</h6>--}}
                            {{--<h6 class="text-center">{{ $restaurant_data->receipt_company }}</h6>--}}
                            {{--<h6 class="text-center">{{ $restaurant_data->receipt_phone }}</h6>--}}
                        {{--</div>--}}



                    {{--</div>--}}
                {{--</div>--}}

            {{--</div>--}}

        @endif


        <script type="application/javascript">
            generateReceipt({{ $order_id }});
        </script>




    </div>






    <div class="modal fade hidden-print" id="order_payment_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content" id="order_payment_div">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                </div>
                <div class="modal-body" id="print_order_payment_modal">

                    @include('cupcake.order.receipt')

                    <div class="form-group font-bold">
                        {{--<a>Payment Method : <a id="payment_modal_payment_method"></a></a><br>--}}

                        <a>Payment Method :
                            <select id="payment_modal_payment_method" class="form-control">
                                <option value="cash" selected>Cash</option>
                                <option value="card">Card</option>
                                <option value="cash_card">Cash & Card</option>
                            </select>
                        </a>

                        <div class="text-center top-margin-10 div-hide" id="payment_button_div">
                            <button type="button" class="btn btn-success btn-sm" onclick="addPaymentMethod()">Add Payment Method</button>
                        </div>

                        <div class="div-hide" id="select_bank_card_div">

                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <select id="select_bank_card" class="form-control top-margin-10">
                                    @foreach($bank_cards as $card)
                                        <option value="{{ $card->card_id }}">{{ $card->card_name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <input type="number" class="form-control top-margin-10" id="card_number" style="border-radius: 6px;">
                            </div>

                        </div>

                        <hr>

                        <div id="paid_amount_change_div" class="top-margin-10">
                            <a>Paid Amount : <input id="payment_modal_paid_amount_input" type="number" style="border-radius: 6px;"></a><br>
                            <a>Change Amount : <a id="payment_modal_change_amount">0</a></a>
                        </div>
                    </div>





                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="confirm_payment_button" class="btn btn-success" data-dismiss="modal" disabled>OK</button>
                </div>
            </div>

        </div>
    </div>


    <div class="modal fade hidden-print" id="payment_method_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Add Payment</h4>
                </div>

                <div class="modal-body">

                    <div class="row">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="instruction_id">Select Payment Method</label>
                                <select class="form-control radius-10" id="modal_payment_method">
                                    <option value="cash" selected>Cash</option>
                                    <option value="card">Card</option>
                                </select>

                            </div>
                        </div>

                    </div>


                    <div class="row">

                        <div class="div-hide" id="modal_bank_div">

                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <div class="form-group">
                                    <label for="modal_bank_card">Select Bank</label>
                                    <select id="modal_bank_card" class="form-control">
                                        @foreach($bank_cards as $card)
                                            <option value="{{ $card->card_id }}">{{ $card->card_name }}</option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>

                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <div class="form-group">
                                    <label for="modal_bank_card">Card No.</label>
                                    <input class="form-control" id="modal_card_number" placeholder="6789">
                                </div>
                            </div>

                        </div>


                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="form-group">
                                <label for="modal_bank_card">Amount</label>
                                <input type="number" min="0" class="form-control" id="modal_amount" placeholder="5500">
                            </div>
                        </div>

                    </div>


                    <div class="row">

                        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 24px;">
                            <div class="form-group text-center">
                                <button type="button" onclick="addPayment()" class="btn btn-success">Add Payment</button>
                            </div>
                        </div>

                    </div>



                    <div class="row top-margin-40">
                        <div class="x_content">
                            <div class="">
                                <table class="table table-striped jambo_table">
                                    <thead>
                                    <tr>
                                        <th>Type</th>
                                        <th>Bank</th>
                                        <th>Card No.</th>
                                        <th class="text-right">Amount</th>
                                        <th width="10%">Action</th>
                                    </tr>
                                    </thead>

                                    <tbody id="payment_table">

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td class="text-right">
                                            Total: <b id="payment_total_amount">0</b>
                                        </td>
                                        <td></td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
                </div>
            </div>

        </div>
    </div>



@endsection
