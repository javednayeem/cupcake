@extends('layouts.main')

@section('title', 'Order')

@section('content')

    {{--<link href="/css/order.css" rel="stylesheet">--}}
    <script src="/js/table.js?v={{ time() }}"></script>

    <style>

        @media print {

            body * {
                visibility: hidden;
                margin: 0 !important;
                padding: 0 !important;
                overflow: hidden;

            }

            #receipt_span, #receipt_span * {
                visibility: visible !important;
                margin: 0px auto;
                padding: 0px auto;
                font-size: 12px;
            }

            #receipt_span {
                position: absolute;
                left: 0px;
                top: 0px;
                bottom: 0px;
            }

            /*#receipt_span {*/
            /*visibility: visible !important;*/
            /*overflow: visible !important;*/
            /*float:none !important;*/
            /*position: fixed;*/
            /*left: 0px;*/
            /*top: 0px;*/
            /*display:block !important;*/

            /*}*/

        }




    </style>


    <input type="hidden" id="order_id" value="{{ $order_id }}">
    <input type="hidden" id="table_id" value="{{ $table_data->table_id }}">

    @if (Auth::user()->license == 0)

        @include('cupcake.partials.expired-license')

    @elseif($restaurant_data->work_period_status == 0)

        <div class="col-md-12 col-sm-12 col-xs-12 hidden-print">
            <div class="x_panel hidden-print">
                <div class="x_content hidden-print">
                    <div class="bs-example top-margin-30 hidden-print">
                        <div class="jumbotron hidden-print">
                            <h1>Work Period Not Started!</h1>
                            <p>Please Contact Your Restaurant Administrator</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    @else

        <div class="hidden-print" id="order_span">

            <div class="row text-center hidden-print">
                <h3 style="font-family: 'Kaushan Script', cursive;" id="table_header">Table - {{ $table_data->table_name }}</h3>
            </div>


            <div class="row hidden-print">
                <div class="form-group hidden-print" style="margin-top: 20px">
                    <label class="control-label col-md-1 col-sm-1 col-xs-1 text-left" style="width: 50px;margin-top: 5px; margin-bottom: 40px">Waiter</label>
                    <div class="col-md-1 col-sm-1 col-xs-12" style="width: 120px">

                        <select class="form-control radius-10" id="waiter_id">
                            @if($restaurant_data->default_waiter=='1')
                                <option value="0">Select Waiter</option>
                            @endif

                            @foreach($user_data as $user)
                                <option value="{{ $user->id }}"
                                        {{ Auth::user()->id == $user->id? 'selected': '' }} >{{ $user->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>


                <div class="form-group" style="margin-top: 20px">
                    <div class="col-md-1 col-sm-1 col-xs-12">

                        <button data-toggle="modal" data-target="#change_table_modal" class="btn btn-primary m-b-20">
                            <i class="fa fa-plus"></i> Change Table
                        </button>

                    </div>
                </div>


                <div class="form-group hidden-print" style="margin-top: 20px">
                    <label class="control-label col-md-2 col-sm-2 col-xs-12 text-right" style="margin-top: 5px; margin-bottom: 40px">Guest</label>
                    <div class="col-md-4 col-sm-4 col-xs-12">

                        <div class="col-md-8 col-sm-8 col-xs-8">
                            <select class="form-control radius-10" id="customer_id" style="width: 60%">
                                @foreach($customer_data as $customer)
                                    <option value="{{ $customer->customer_id }}">{{ $customer->customer_phone . ' - '. $customer->customer_name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-4 hidden-print">
                            <button data-toggle="modal" data-target="#add_customer_modal" class="btn btn-primary m-b-20">
                                <i class="fa fa-plus"></i> Add Guest
                            </button>
                        </div>

                    </div>
                </div>


                <div class="form-group" style="margin-top: 20px">
                    <label class="control-label col-md-1 col-sm-1 col-xs-1 text-right" style="margin-top: 5px; margin-bottom: 40px">Order Type</label>
                    <div class="col-md-2 col-sm-2 col-xs-2">
                        <select class="form-control" id="order_type">
                            <option value="dine-in">Dine In</option>
                            <option value="take-away">Take Away</option>
                        </select>
                    </div>
                </div>

            </div>


            <div class="row hidden-print">

                @foreach($setmenu_items as $item)
                    <script type="application/javascript">
                        @php echo 'pushToSetMenuItemArray("'.$item->item_id.'", "'.$item->item_name.'", "'.$item->menu_item_id.'");' @endphp
                    </script>
                @endforeach


                @foreach($menu_items as $item)
                    <script type="application/javascript">
                        @php echo 'pushToItemArray("'.$item->menu_item_id.'", "'.$item->menu_id.'", "'.$item->item_name.'", "'.$item->ratio.'", "'.$item->price.'", "'.$item->item_discount.'", "'.$item->item_vat.'", "'.$item->set_menu.'");' @endphp
                    </script>
                @endforeach


                @foreach($modifiers as $modifier)
                    <script type="application/javascript">
                        @php echo 'pushToModifiersArray("'.$modifier->modifier_id.'", "'.$modifier->modifier_name.'", "'.$modifier->price.'", "'.$modifier->modifier_vat.'");' @endphp
                    </script>
                @endforeach


                <script type="application/javascript">
                    pushChargesInfo('{{$restaurant_data->vat_no}}', '{{$restaurant_data->vat_percentage}}', '{{$restaurant_data->service_charge}}', '{{$restaurant_data->guest_bill}}', '{{$restaurant_data->price_including_vat}}' ,'{{$restaurant_data->vat_after_discount}}', '{{$restaurant_data->sd_percentage}}', '{{$restaurant_data->service_charge_vat_percentage}}', '{{$restaurant_data->sd_vat_percentage}}', '{{$restaurant_data->kitchen_type}}');
                </script>



                <div class="col-md-2 col-sm-2 col-xs-12 hidden-print">
                    <div class="x_panel" style="padding: 0px; border: none">
                        <div class="x_title top-margin-10">
                            <h2>Menu</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <table class="table text-center">

                                @php $color=0; @endphp

                                <div class="ranges">
                                    <ul id="ranges_nav">
                                        @foreach($menu_data as $menu)
                                            <li style="font-size: {{ $restaurant_data->menu_font_size }}px;background-color: {{ randomColorGenerate() }}" onclick="loadItems('{{ $menu->menu_id }}', '{{ $menu->menu_name }}')">
                                                {{ $menu->menu_name }}
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>

                            </table>

                        </div>
                    </div>
                </div>


                <div class="col-md-6 col-sm-6 col-xs-12 hidden-print" id="item_div">
                    <div class="x_panel">

                        <div class="x_title">
                            <h2 id="menu_item_header">Menu Items</h2>
                            <div class="clearfix"></div>
                        </div>

                        <div id="item_div_span">
                        </div>
                    </div>

                </div>


                <div class="col-md-4 col-sm-4 col-xs-12 hidden-print" id="cart_div">

                    <div class="x_panel hidden-print">

                        <div class="x_content">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <td class="active">Total</td>
                                    <td class="whiteBg grand_total">0</td>
                                </tr>

                                @if($restaurant_data->vat_percentage > 0)
                                    <tr>
                                        <td class="active">VAT ({{ $restaurant_data->vat_percentage }}%)</td>
                                        <td class="whiteBg cart_vat">0</td>
                                    </tr>
                                @endif

                                @if($restaurant_data->service_charge > 0)
                                    <tr>
                                        <td class="active">Service Charge ({{ $restaurant_data->service_charge }}%)</td>
                                        <td class="whiteBg cart_service_charge">0</td>
                                    </tr>
                                @endif

                                @if($restaurant_data->sd_percentage > 0)
                                    <tr>
                                        <td class="active">SD ({{ $restaurant_data->sd_percentage }}%)</td>
                                        <td class="whiteBg cart_sd_percentage">0</td>
                                    </tr>
                                @endif

                                @if($restaurant_data->service_charge_vat_percentage > 0)
                                    <tr>
                                        <td class="active">Service Charge VAT ({{ $restaurant_data->service_charge_vat_percentage }}%)</td>
                                        <td class="whiteBg cart_service_charge_vat">0</td>
                                    </tr>
                                @endif


                                <tr>
                                    <td class="active">Grand Total</td>
                                    <td class="whiteBg"><b class="cart_net_amaount">0</b></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                        <div>
                            <td><a type="button" href="/table-order" class="btn btn-warning">Cancel</a></td>

                            <td><button type="button" class="btn btn-success" id="send_kot" disabled>Send KOT</button></td>

                            @if($order_status == 'placed' || $order_status == 'guest_printed')
                                <td><button type="button" class="btn btn-dark" id="guest_print_button" onclick="printOrderPaymentReceipt('guest_printed')">Guest Print</button></td>
                                <td><a href="/settle/1" type="button" class="btn btn-success order_payment_button">Settle</a></td>
                            @endif

                            @if(($order_status == 'placed' || $order_status == 'guest_printed') && ($restaurant_data->waiter_order_void=='1' || Auth::user()->role == 'superadmin' || Auth::user()->role == 'admin'))
                                <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#void_modal">Void</button></td>
                            @endif

                            <td>
                                <button type="button" class="btn btn-primary" data-toggle="modal" id="add_discount_button" data-target="#add_discount_modal" disabled>
                                    Add Discount
                                </button>
                            </td>

                            <td><button type="button" class="btn btn-warning" id="order_instruction_button" disabled>Cooking Instruction</button></td>

                        </div>

                    </div>

                    <div class="x_panel hidden-print">
                        <div class="x_title">
                            <h2>Cart</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li>
                                    <button type="button" class="btn btn-danger" onclick="clearCart();">
                                        Clear Cart
                                    </button>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>

                        <div class="x_content">
                            <div class="table-responsive">
                                <table class="table table-striped jambo_table bulk_action" id="cart_table">
                                    <thead>
                                    <tr class="headings">
                                        <th class="column-title">#</th>
                                        <th class="column-title">Item Name</th>
                                        <th class="column-title">Unit Price</th>
                                        <th class="column-title" width="20%">Quantity</th>
                                        <th class="column-title">Item Price</th>
                                        <th class="column-title no-link last" width="23%"><span class="nobr">Action</span>
                                        </th>
                                    </tr>
                                    </thead>

                                    <tbody id="cart_item_div" style="height: 100px;overflow-y: auto;">

                                    </tbody>
                                </table>
                            </div>

                            {{--<div class="form-group">--}}
                            {{--<div class="col-md-12 col-sm-12 col-xs-12">--}}
                            {{--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#view_cart_modal">View Cart</button>--}}

                            {{--</div>--}}
                            {{--</div>--}}
                        </div>
                    </div>

                    @if($order_id>0)
                        <div class="x_panel hidden-print">
                            <div class="x_title">
                                <h2>Previous Orders</h2>
                                <div class="clearfix"></div>
                            </div>

                            <div class="x_content">
                                <div class="table-responsive">
                                    <table class="table table-striped jambo_table bulk_action">
                                        <thead>
                                        <tr class="headings">
                                            <th>Item Name</th>
                                            <th>Qty</th>
                                            <th>Action</th>
                                            </th>

                                        </tr>
                                        </thead>

                                        <tbody id="previous_cart_table">


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>

            </div>


            <div class="modal fade hidden-print" id="add_discount_modal">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">x</button>
                            <h4 class="modal-title">Add Discount</h4>
                        </div>

                        <div class="modal-body">

                            <input type="hidden" id="discount_amount_type" value="cash">

                            <div class="form-group">
                                <label for="course_name">Discount Amount</label>
                                <input type="text" class="form-control" id="discount_amount" value="0">
                            </div>

                            <div class="form-group">
                                <label for="course_name">Discount Amount in %</label>
                                <input type="text" class="form-control" id="discount_amount_percentage" value="0">
                            </div>

                            <div class="form-group">
                                <label for="description">Discount Reference</label>
                                <input type="text" class="form-control" id="discount_reference" placeholder="Victory Day">
                            </div>


                            <div class="form-group top-padding-10 div-hide">
                                <h5><strong>Billing Details</strong></h5>
                                <table class="table table-striped" style="width: 100%;">
                                    <tbody>
                                    <tr>
                                        <td>Total</td>
                                        <td id="order_amount_total">0</td>
                                    </tr>

                                    <tr>
                                        <td>Discount</td>
                                        <td class="discount_amount">0</td>
                                    </tr>

                                    <tr>
                                        <th>Net Amount</th>
                                        <th class="order_net_amaount">0</th>
                                    </tr>

                                    </tbody>

                                </table>
                            </div>

                        </div>




                        <div class="modal-footer">
                            <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                            <button type="button" id="add_discount_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                        </div>
                    </div>

                </div>
            </div>


            <div class="modal fade" id="order_instruction_modal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">x</button>
                            <h4 class="modal-title">Cooking Instructions</h4>
                        </div>
                        <div class="modal-body">

                            <input type="hidden" id="order_notes">
                            {{--<input type="hidden" id="edit_instruction_id" value="0">--}}


                            <div class="row">

                                <div class="col-md-7 col-sm-7 col-xs-7">
                                    <div class="form-group">
                                        <label for="instruction_id">Instruction</label>
                                        <select class="form-control radius-10" id="instruction_id">

                                            @foreach($order_instructions as $instruction)
                                                <option value="{{ $instruction->instruction_id }}">{{ $instruction->instruction }}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-3 col-xs-3" style="margin-top: 24px;">
                                    <div class="form-group">
                                        <button type="button" id="add_instruction_button" class="btn btn-success">Add Instruction</button>
                                    </div>
                                </div>

                            </div>


                            <div class="row">

                                <div class="col-md-7 col-sm-7 col-xs-7">
                                    <div class="form-group">
                                        <label for="course_name">Text Instruction</label>
                                        <textarea class="form-control radius-5" id="textarea_instruction"></textarea>
                                    </div>
                                </div>



                                <div class="col-md-3 col-sm-3 col-xs-3 top-margin-30">
                                    <div class="form-group">
                                        <button type="button" id="textarea_instruction_button" class="btn btn-success">Add Instruction</button>
                                    </div>
                                </div>

                            </div>


                            <div class="row top-margin-40">
                                <div class="x_content">
                                    <div class="">
                                        <table class="table table-striped jambo_table">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Instruction</th>
                                                <th width="20%">Action</th>
                                            </tr>
                                            </thead>

                                            <tbody id="instruction_table">



                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
                        </div>
                    </div>

                </div>
            </div>

        </div>


        <div class="{{ $restaurant_data->order_receipt_debug==0?'div-hide':'lala' }}">

            <div class="row">
                <div class="col-md-3 col-sm-2 col-xs-12">

                    <div class="x_panel" id="kitchen_token_receipt">

                        <div class="x_title text-center">
                            <h4>{{ $restaurant_data->restaurants_name==''? "Cupcake":$restaurant_data->restaurants_name }}</h4>
                            <h2 class="token_order_id"></h2>
                            <h3 class="token_table_id"></h3>
                            @php date_default_timezone_set("Asia/Dhaka") @endphp
                            <h4>Time : @php echo date('g:i a, F j', time()); @endphp</h4>
                            <h4>Waiter: <a class="token_waiter_name"></a></h4>
                            <div class="clearfix"></div>
                        </div>

                        =========================

                        <div class="x_content">
                            <div class="table-responsive">
                                <table class="table table-striped jambo_table">
                                    <thead>
                                    <tr class="headings">
                                        <th class="">Item Name</th>
                                        <th class="text-right">Qty</th>
                                    </tr>
                                    </thead>

                                    <tbody id="kitchen_token" style="height: 100px;overflow-y: auto;">

                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="x_content">
                            <div class="dashboard-widget-content top-margin-10 m-b-20">
                                <b>Instructions</b>
                                <ul id="token_instruction"></ul>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

            @if($order_id>0)
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" id="settle_div">
                        <div class="x_panel">
                            @include('cupcake.order.receipt')
                        </div>
                    </div>
                </div>
            @endif
        </div>

    @endif


@endsection
