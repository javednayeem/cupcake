<div class="text-center">
    <img src="/images/restaurant-logo/{{ getRstaurantLogo() }}" id="logo-icon" width="5%">
    <h4><b>{{ $restaurant_data->restaurants_name==''? "Cupcake":$restaurant_data->restaurants_name }}</b></h4>
    <h5 id="receipt_address_div">
        <b>
            {{ $restaurant_data->address}}
            {{ $restaurant_data->area==null? '':', '.$restaurant_data->area }}
            {{ $restaurant_data->city==null? '':', '.$restaurant_data->city }}
        </b>
    </h5>
    <h5><b>Phone : {{ $restaurant_data->phone_number==null? '':$restaurant_data->phone_number }}</b></h5>
    <h5 id="receipt_vat_div"><b>VAT Reg. No : {{ $restaurant_data->vat_no==null? 'N/A':$restaurant_data->vat_no }}</b></h5>
    <br>
    <h3 style=""><b class="bill_type"></b></h3>
</div>
<br>

<div class="form-group font-bold">
    <table class="table top-margin-40">
        <tbody>
        <tr id="tr_receipt_table_no" class="text-center"><td><b>Table No: <a class="receipt_table_no"></a></b></td></tr>
        <tr id="tr_receipt_customer_name"><td><strong>Guest : <a class="receipt_customer_name"></a></strong></td></tr>
        <tr id="tr_receipt_waiter_name"><td><strong>Served By : <a class="receipt_waiter_name"></a></strong></td></tr>
        <tr id="tr_receipt_created_at"><td><strong>Time : <a class="receipt_created_at">@php echo date('F j, Y, g:i a', time()); @endphp</a></strong></td></tr>
        <tr id="tr_receipt_order_no"><td><strong><a id="tr_receipt_kot">Invoice No</a> : <a class="receipt_order_no"></a></strong></td></tr>
        <tr><td><strong><a>Token No</a> : <a class="receipt_token_no"></a></strong></td></tr>
        <tr id="tr_receipt_order_type"><td><strong>Order Type : <a class="receipt_order_type"></a></strong></td></tr>
        </tbody>
    </table>
</div>

<b>==========================================</b>


<div class="table-responsive">
    <table class="table">
        <thead>
        <tr id="receipt_table_header">
            <th width="5%">#</th>
            <th width="50%">Item Name</th>
            <th>U.Price</th>
            <th>Qty</th>
            <th class="text-right">Total</th>
        </tr>
        </thead>

        <tbody class="receipt_order_table">

        </tbody>
    </table>
</div>

<b>==========================================</b>

<div class="form-group font-bold" id="receipt_charges_div">
    <table class="table">
        <thead>
        <tr>
            <th width="85%"></th>
            <th width="15%"></th>
        </tr>
        </thead>
        <tbody>

        <tr>
            <td>SubTotal : </td>
            <td class="text-right"><a class="receipt_subtotal">0</a></td>
        </tr>
        <tr>
            <td>VAT ({{$restaurant_data->vat_percentage}}%):<h6 id="receipt_vat_header"></h6></td>
            <td class="text-right"><a class="receipt_vat_total">0</a></td>
        </tr>

        @if($restaurant_data->service_charge>0)
            <tr>
                <td>Service Charge ({{$restaurant_data->service_charge}}%) : </td>
                <td class="text-right"><a class="receipt_service_charge">0</a></td>
            </tr>
        @endif


        @if($restaurant_data->sd_percentage>0)
            <tr>
                <td>SD ({{$restaurant_data->sd_percentage}}%) : </td>
                <td class="text-right"><a class="receipt_sd_percentage">0</a></td>
            </tr>
        @endif


        @if($restaurant_data->service_charge_vat_percentage>0)
            <tr>
                <td>Service Charge VAT ({{$restaurant_data->service_charge_vat_percentage}}%) : </td>
                <td class="text-right"><a class="receipt_service_charge_vat_percentage">0</a></td>
            </tr>
        @endif


        @if($restaurant_data->sd_vat_percentage>0)
            <tr>
                <td>SD VAT ({{$restaurant_data->sd_vat_percentage}}%) : </td>
                <td class="text-right"><a class="receipt_sd_vat_percentage">0</a></td>
            </tr>
        @endif



        <tr>
            <td>Discount (<a class="receipt_discount_percentage">0</a>) : </td>
            <td class="text-right"><a class="receipt_discount_amount">0</a></td>
        </tr>



        <tr>
            <td>Total Amount : <h6>(Rounded)</h6></td>
            <td class="text-right"><a class="receipt_total_amount">0</a></td>
        </tr>
        <hr>

        <tr class="cash_given_return">
            <td>Given Amount :</td>
            <td class="text-right"><a class="receipt_given_amount">0</a></td>
        </tr>

        <tr class="cash_given_return">
            <td>Return Amount :</td>
            <td class="text-right"><a class="receipt_return_amount">0</a></td>
        </tr>

        </tbody>
    </table>

</div>

<b>==========================================</b>


<div class="table-responsive top-margin-10" id="receipt_payment_div" >
    <h6 class="text-center"><b>Payment Methods</b></h6>
    <table class="table">
        <thead>
        <tr>
            <th>Type</th>
            <th>Bank</th>
            <th>Card No.</th>
            <th class="text-right">Amount</th>
        </tr>
        </thead>
        <tbody class="payment_table">

        </tbody>
    </table>
</div>

<b>==========================================</b>

<div class="text-center top-margin-30" id="receipt_footer_div">
    <h6 class="text-center">{{ $restaurant_data->receipt_footer }}</h6>
    <h6 class="text-center">{{ $restaurant_data->receipt_company }}</h6>
    <h6 class="text-center">{{ $restaurant_data->receipt_phone }}</h6>
</div>

