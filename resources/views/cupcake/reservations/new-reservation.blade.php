@extends('layouts.main')

@section('title', 'Create New Reservation')

@section('content')

    <script src="/js/reservation.js"></script>

    @if (Auth::user()->license == 0)
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">

                        <div class="bs-example top-margin-30">
                            <div class="jumbotron">
                                <h1>License Has Been Expired</h1>
                                <p>Please Contact Your System Administrator For Further Information</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    @else


        <div class="row top-margin-30">

            <div class="col-md-8 col-sm-8 col-xs-8">
                <div class="x_panel">

                    <div class="form-horizontal top-margin-30">

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-3">Customer Name</label>
                            <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback">
                                <select class="form-control" id="customer_id">
                                    @foreach($customers as $customer)
                                        <option value="{{ $customer->customer_id }}">{{ $customer->customer_name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-3 col-sm-3 col-xs-3 form-group has-feedback">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_customer_modal">Add Customer</button>
                            </div>
                        </div>



                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Reservation Date</label>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="text" class="form-control has-feedback-left" id="single_cal2">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                <span id="inputSuccess2Status2" class="sr-only">(success)</span>
                            </div>

                            <div class="col-md-3 col-sm-3 col-xs-3 form-group has-feedback">
                                <button type="button" class="btn btn-primary check_availablity">Check Availablity</button>
                            </div>
                        </div>



                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Start Time</label>
                            <div class="col-md-7 col-sm-7 col-xs-12 form-group has-feedback">
                                <div class='input-group date'>
                                    <input type='text' class="form-control" id="start_time"/>
                                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">End Time</label>
                            <div class="col-md-7 col-sm-7 col-xs-12 form-group has-feedback">
                                <div class='input-group date' >
                                    <input type='text' class="form-control" id="end_time"/>
                                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                                </div>
                            </div>
                        </div>



                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <textarea class="form-control" placeholder="Birthday Party" id="description"></textarea>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-3">Food Menu</label>
                            <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback">
                                <textarea class="form-control" id="food_menu"></textarea>
                            </div>

                            <div class="col-md-3 col-sm-3 col-xs-3 form-group has-feedback">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_menu_modal">Add From Menu</button>
                            </div>
                        </div>


                        {{--<div class="form-group">--}}
                        {{--<label class="control-label col-md-3 col-sm-3 col-xs-12">Food Menu</label>--}}
                        {{--<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">--}}
                        {{--<textarea class="form-control" id="food_menu"></textarea>--}}
                        {{--</div>--}}
                        {{--</div>--}}


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Total Amount</label>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="number" class="form-control has-feedback-left" id="subtotal" min="0" value="0">
                                <span class="form-control-feedback left" aria-hidden="true">&#2547;</span>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Advance Payment</label>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="number" class="form-control has-feedback-left" id="advance" min="0" value="0">
                                <span class="form-control-feedback left" aria-hidden="true">&#2547;</span>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Number Of Guest</label>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="number" class="form-control has-feedback-left" id="guest" min="1" value="1">
                                <span class="fa fa-gg-circle form-control-feedback left" aria-hidden="true"></span>
                            </div>
                        </div>


                        <div class="form-group">

                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Tables</label>

                            <div class="col-md-6 col-sm-6 col-xs-12">


                                @foreach($tables as $table)
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="table_id" value="{{ $table->table_name }}">
                                            Table {{ $table->table_name }} - Capacity: {{ $table->capacity }}
                                        </label>
                                    </div>
                                @endforeach


                            </div>
                        </div>



                        <div class="form-group top-margin-30 text-center">

                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <button class="btn btn-primary btn-lg check_availablity">Check Availablity</button>
                            </div>


                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <button class="btn btn-success btn-lg" id="create_reservation_button" disabled>Create Reservation</button>
                            </div>
                        </div>


                    </div>
                </div>
            </div>


            <div class="col-md-4 col-sm-4 col-xs-4">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Availablity Status</h2>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content bs-example-popovers" id="availablity_status_span">


                    </div>



                    <div class="x_content" >

                        <ul class="list-unstyled timeline" id="reservations_table">



                        </ul>
                    </div>




                </div>
            </div>




        </div>


        <div class="modal fade" id="add_customer_modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">x</button>
                        <h4 class="modal-title">Add Customer</h4>
                    </div>
                    <div class="modal-body">


                        <div class="form-group">
                            <label for="customer_name">Customer Name</label>
                            <input type="text" class="form-control" id="customer_name" placeholder="Javed Nayeem">
                        </div>

                        <div class="form-group">
                            <label for="customer_phone">Phone</label>
                            <input type="text" class="form-control" id="customer_phone" placeholder="01715123456">
                        </div>

                        <div class="form-group">
                            <label for="customer_address">Address</label>
                            <input type="text" class="form-control" id="customer_address" placeholder="Dhanmondi">
                        </div>

                        <div class="form-group">
                            <label for="customer_email">Email</label>
                            <input type="email" class="form-control" id="customer_email" placeholder="customer@cupcake.com">
                        </div>

                        <div class="form-group">
                            <label for="card_no">Card No</label>
                            <input type="text" class="form-control" id="card_no">
                        </div>


                        <div class="form-group">
                            <label for="discount_percentage">Discount Percentage</label>
                            <input type="number" class="form-control" id="discount_percentage" value="0" min="0">
                        </div>



                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                        <button type="button" id="add_customer_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                    </div>
                </div>

            </div>
        </div>


        <div class="modal fade" id="add_menu_modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">x</button>
                        <h4 class="modal-title">Add Menu</h4>
                    </div>
                    <div class="modal-body">


                        <div class="form-group">
                            <label for="customer_name">Menu Item Name</label>
                            <select class="form-control" id="item_id" style="width: 70%">
                                @foreach($menu_items as $item)
                                    <option value="{{ $item->menu_item_id }}">
                                        {{ $item->item_name . ' ' .$item->ratio}} ({{ $item->menu_name }})
                                    </option>
                                @endforeach
                            </select>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                        <button type="button" id="add_menu_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                    </div>
                </div>

            </div>
        </div>


    @endif

@endsection
