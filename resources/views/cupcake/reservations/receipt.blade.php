@extends('layouts.main')

@section('title', 'Reservation Receipt')

@section('content')

    <link href="/css/order.css" rel="stylesheet">

    <script src="/js/reservation.js"></script>

    @if (Auth::user()->license == 0)
        <div class="row hidden-print">
            <div class="col-md-12 col-sm-12 col-xs-12 hidden-print">
                <div class="x_panel">
                    <div class="x_content">

                        <div class="bs-example top-margin-30">
                            <div class="jumbotron">
                                <h1>License Has Been Expired</h1>
                                <p>Please Contact Your System Administrator For Further Information</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    @else

        <link href="/css/order.css" rel="stylesheet">

        <div class="row">
            <div class="col-md-3 col-lg-offset-4" id="settle_div">
                <div class="x_panel" >

                    <div class="x_content">

                        <section class="content invoice">

                            <div class="row">

                                <div class="text-center">
                                    <img src="/images/restaurant-logo/{{ getRstaurantLogo() }}" id="logo-icon" width="20%">
                                    <h4><b>{{ $restaurant_data->restaurants_name==''? "Cupcake":$restaurant_data->restaurants_name }}</b></h4>
                                    <h5>
                                        <b>
                                            {{ $restaurant_data->address}}
                                            {{ $restaurant_data->area==null? '':', '.$restaurant_data->area }}
                                            {{ $restaurant_data->city==null? '':', '.$restaurant_data->city }}
                                        </b>
                                    </h5>
                                    <h5><b>VAT Reg. No : {{ $restaurant_data->vat_no==null? 'N/A':$restaurant_data->vat_no }}</b></h5>
                                    <br>
                                    <br>

                                </div>

                                <div class="col-xs-12 invoice-header">
                                    <h2>
                                        <b>
                                            <i class="fa fa-globe"></i>
                                            Invoice # {{ generateInvoiceNumber($reservation->reservation_id,$reservation->created_at) }}
                                        </b>
                                    </h2>

                                    <h2><b>Created At: {{ date('F j, Y', strtotime( $reservation->created_at )) }}</b></h2>
                                    <h2><b>Reservation Date: {{ date('F j, Y', strtotime( $reservation->reservation_date )) }}</b></h2>
                                    <h2>Reservation Start Time: {{ date('h:i a', strtotime( $reservation->start_time )) }}</h2>
                                    <h2>Reservation End Time: {{ date('h:i a', strtotime( $reservation->end_time )) }}</h2>

                                    <br>

                                </div>



                            </div>

                            <div class="row invoice-info">
                                <div class="col-sm-12 invoice-col">
                                    <strong>Guest Details</strong>
                                    <address>
                                        <strong>{{ $reservation->customer_name }}</strong>
                                        <br>{{ $reservation->customer_address }}
                                        <br>Phone: {{ $reservation->customer_phone }}
                                        <br>Email: {{ $reservation->customer_email }}
                                    </address>

                                    <br>
                                    <br>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-xs-12 table">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th width="70%">Description</th>
                                            <th>Food Menu</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>{{ $reservation->description }}</td>
                                            <td>{{ $reservation->food_menu }}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>


                            <div class="row">

                                <div class="col-xs-8 pull-right">
                                    {{--<p class="lead">Amount Due 2/22/2014</p>--}}

                                    <br>
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tbody>
                                            <tr>
                                                <th style="width:50%">Subtotal:</th>
                                                <th>&#2547;{{ $reservation->subtotal }}</th>
                                            </tr>

                                            <tr>
                                                <th>Advance:</th>
                                                <th>&#2547;{{ $reservation->advance }}</th>
                                            </tr>
                                            <tr>
                                                <th>Payment Due:</th>
                                                <th>&#2547;{{ $reservation->subtotal - $reservation->advance }}</th>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>



                                <div class="col-xs-12 text-center">
                                    <b class="text-center">{{ $restaurant_data->receipt_footer }}</b><br>
                                    <b class="text-center">{{ $restaurant_data->receipt_company }}</b><br>
                                    <b class="text-center">{{ $restaurant_data->receipt_phone }}</b>
                                </div>

                            </div>


                            <div class="row no-print hidden-print">
                                <div class="col-xs-12">
                                    <button class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>

    @endif








@endsection
