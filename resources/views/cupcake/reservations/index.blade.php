@extends('layouts.main')

@section('title', 'Reservations')

@section('content')

    <script src="/js/reservation.js"></script>

    @if (Auth::user()->license == 0)
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">

                        <div class="bs-example top-margin-30">
                            <div class="jumbotron">
                                <h1>License Has Been Expired</h1>
                                <p>Please Contact Your System Administrator For Further Information</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    @else



        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12 top-margin-30">
                <div class="x_panel">
                    <div class="top-margin-10">

                        <ul class="nav navbar-right panel_toolbox">
                            <li>
                                <a href="/new-reservation"><button class="btn btn-primary">Create New Reservation</button></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">

                        <div class="table-responsive top-margin-20">
                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                <tr class="headings">
                                    <th class="column-title">#</th>
                                    <th class="column-title">Customer Name</th>
                                    <th class="column-title">Description</th>
                                    <th class="column-title">Reservation Date</th>
                                    <th class="column-title">No. Of Guest</th>
                                    <th class="column-title">Start Time</th>
                                    <th class="column-title">End Time</th>
                                    <th class="column-title">Created By</th>
                                    <th class="column-title">Created At</th>
                                    <th class="column-title no-link last" width="11%">
                                        <span class="nobr">Action</span>
                                    </th>
                                </tr>
                                </thead>

                                <tbody id="reservation_table">

                                @php $reservation_count=1; @endphp

                                @foreach($reservations as $reservation)

                                    <tr class="even" id="reservation_{{ $reservation->reservation_id }}">
                                        <td> {{ $reservation_count++ }}</td>
                                        <td>{{ $reservation->customer_name }}</td>
                                        <td>{{ $reservation->description }}</td>
                                        <td>{{ date('F j, Y', strtotime( $reservation->reservation_date )) }}</td>
                                        <td>{{ $reservation->guest }}</td>
                                        <td>{{ date('g:i a', strtotime( $reservation->start_time )) }}</td>
                                        <td>{{ date('g:i a', strtotime( $reservation->end_time )) }}</td>
                                        <td>{{ $reservation->name }}</td>
                                        <td>{{ date('F j, Y', strtotime( $reservation->created_at )) }}</td>

                                        <td>
                                            <a href="/reservation/receipt/{{ $reservation->reservation_id }}" class="btn btn-primary">
                                                <i class="glyphicon glyphicon-print"></i>
                                            </a>

                                            <button type="button" class="btn btn-dark" onclick="editReservation()">
                                                <i class="glyphicon glyphicon-pencil"></i>
                                            </button>

                                            <button type="button" class="btn btn-danger" onclick="deleteReservation({{ $reservation->reservation_id }})">
                                                <i class="glyphicon glyphicon-remove"></i>
                                            </button>
                                        </td>
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>


                    </div>
                </div>
            </div>

        </div>


        <div class="modal fade" id="add_modifier_modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">x</button>
                        <h4 class="modal-title" id="edit_table_title">Add Modifier</h4>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label>Modifier Name</label>
                            <input type="text" class="form-control" id="modifier_name">
                        </div>

                        <div class="form-group">
                            <label>Price</label>
                            <input type="number" class="form-control" id="price" min="0" value="0">
                        </div>

                        <div class="form-group">
                            <label>VAT</label>
                            <input type="number" class="form-control" id="modifier_vat" value="{{ getRstaurantVAT() }}" min="0">
                        </div>

                        <div class="form-group">
                            <input type="checkbox" id="status" checked>&nbsp; Tick If Active
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                        <button type="button" id="add_modifier_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                    </div>
                </div>

            </div>
        </div>



    @endif








@endsection
