@extends('layouts.main')

@section('title', 'Loans Credit')

@section('content')

    <script src="/js/accounts.js"></script>


    <div class="row">

        <div class="col-md-6 col-sm-6 col-xs-6 top-margin-30">
            <div class="x_panel">
                <div class="top-margin-10">

                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_owner_modal">Add Name</button>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">

                    <div class="table-responsive top-margin-20">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                            <tr class="headings">
                                <th class="column-title">#</th>
                                <th class="column-title">Name</th>
                                <th class="column-title">Ac Number</th>
                                <th class="column-title">Accounts Payable</th>
                                <th class="column-title">Accounts Receivable</th>
                                <th class="column-title">Joined</th>
                            </tr>
                            </thead>

                            <tbody id="owner_table">

                            @php $owner_count=1; @endphp

                            @foreach($loans_from as $item)

                                <tr class="even" id="owner_{{ $item->owner_id }}">
                                    <td> {{ $owner_count++ }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->ac }}</td>
                                    <td>{{ $item->accounts_payable }}</td>
                                    <td>{{ $item->accounts_receivable }}</td>
                                    <td>{{ date('F j, Y', strtotime( $item->created_at )) }}</td>
                                </tr>

                            @endforeach

                            </tbody>
                        </table>
                    </div>


                </div>
            </div>
        </div>

        <div class="col-md-6 col-sm-6 col-xs-6 top-margin-30">
            <div class="x_panel">

                <div class="x_content">
                    <div data-parsley-validate class="form-horizontal form-label-left">


                        <div class="x_title">
                            <h2>Add New Loan</h2>
                            <div class="clearfix"></div>
                        </div>

                        <div class="form-horizontal form-label-left top-margin-20">

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Loan From</label>
                                <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                    <select class="form-control" id="owner_id">

                                        @foreach($loans_from as $item)
                                            <option value="{{ $item->owner_id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Loan Amount</label>
                                <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                    <input type="number" class="form-control" id="amount">
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5 ">
                                    <button class="btn btn-success btn-lg" id="add_loan_button">Add Loan</button>
                                </div>
                            </div>




                        </div>


                    </div>
                </div>
            </div>
        </div>

    </div>


    <div class="row">

        <div class="col-lg-12 col-md-12 top-margin-20">



            <div class="x_panel">


                <div class="x_content">

                    <div class="x_title">
                        <h2>Loan History</h2>
                        <div class="clearfix"></div>
                    </div>

                    <div class="form-horizontal">

                        <div class="top-margin-20">
                            <table class="table table-striped jambo_table bulk_action">
                                <thead>

                                <tr>
                                    <th>#</th>
                                    <th>Loan From</th>
                                    <th>Amount</th>
                                    <th>Type</th>
                                    <th>Created By</th>
                                    <th width="20%">Created At</th>
                                </tr>

                                </thead>

                                <tbody id="loan_table">

                                @php $loan_count=1; @endphp

                                @foreach($loans_history as $history)

                                    <tr>
                                        <th scope="row"> {{ $loan_count++ }}</th>
                                        <td>{{ $history->owner_name }}</td>
                                        <td>{{ $history->amount }}</td>
                                        <td>{{ ucfirst($history->transaction_type) }}</td>
                                        <td>{{ $history->name }}</td>
                                        <td>{{ date('F j, Y h:i a', strtotime( $history->created_at )) }}</td>
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>

                        </div>


                    </div>
                </div>
            </div>
        </div>


    </div>


    <div class="modal fade" id="add_owner_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title" id="edit_table_title">Add Name</h4>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label>Person Name</label>
                        <input type="text" class="form-control" id="name">
                    </div>

                    <div class="form-group">
                        <label>Address</label>
                        <input type="text" class="form-control" id="address">
                    </div>

                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" class="form-control" id="email">
                    </div>

                    <div class="form-group">
                        <label>Phone</label>
                        <input type="text" class="form-control" id="phone">
                    </div>

                    <div class="form-group">
                        <label>Ac Number</label>
                        <input type="text" class="form-control" id="ac">
                    </div>


                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="add_owner_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                </div>
            </div>

        </div>
    </div>




@endsection
