@extends('layouts.main')

@section('title', 'Daily Expenses')

@section('content')

    <script src="/js/accounts.js?v={{ time() }}"></script>


    <div class="row">
        <div class="col-md-12 top-margin-20">
            <div class="x_panel">

                <div class="x_content">
                    <div data-parsley-validate class="form-horizontal form-label-left">


                        <div class="x_title">
                            <h2>Add New Expense</h2>
                            <div class="clearfix"></div>
                        </div>

                        <div class="form-horizontal form-label-left top-margin-20">

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Expense Particular</label>
                                <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                    <input type="text" class="form-control" id="purpose">
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Debit From</label>
                                <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                    <select class="form-control" id="debit_from">
                                        <option value="sales">Sales</option>
                                        <option value="petty_cash">Petty Cash</option>
                                        <option value="loan">Loan</option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Expense Amount</label>
                                <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                    <input type="number" class="form-control" id="amount">
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5 ">
                                    <button class="btn btn-success btn-lg" id="add_expense_button">Add Expense</button>
                                </div>
                            </div>




                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row">

        <div class="col-lg-12 col-md-12 top-margin-20">


            @php
                $cash_sale = $expenses_details->cash_sale!=''?$expenses_details->cash_sale:0;
                $current_cash_sale = $cash_sale + $expenses_details->transfer_sales - $expenses_details->sales_expense;

                $card_sale = $expenses_details->card_sale!=''?$expenses_details->card_sale:0;
                $sale = $expenses_details->today_sale + $expenses_details->transfer_sales - $expenses_details->sales_expense;
                $petty_cash = $expenses_details->cash_opening_total_amount + $expenses_details->transfer_petty_cash -  $expenses_details->petty_cash_expense;
                $loan = $expenses_details->loan_amount;
                $total = $current_cash_sale + $petty_cash;

                $total_loan = $expenses_details->total_loan!=''?$expenses_details->total_loan:0;
            @endphp


            <div class="x_panel">

                <div class="row top_tiles">
                    <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="tile-stats">
                            <div class="count text-center text-danger">Total Loan To Be Paid : {{ $total_loan }}</div>
                            {{--<h3 class="text-center">Total Loan To Be Paid</h3>--}}
                        </div>
                    </div>



                </div>


                <div class="row top_tiles">

                    {{--<div class="animated flipInY col-lg-4 col-md-4 col-sm-4 col-xs-4">--}}
                        {{--<div class="tile-stats">--}}
                            {{--<div class="count">Cash Sale: <span id="cash_sale_amount">{{ $cash_sale + $expenses_details->transfer_sales - $expenses_details->sales_expense }}</span></div>--}}
                            {{--<h3></h3>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="animated flipInY col-lg-4 col-md-4 col-sm-4 col-xs-4">--}}
                        {{--<div class="tile-stats">--}}
                            {{--<div class="count">Card Sale: {{ $card_sale }}</div>--}}
                            {{--<h3></h3>--}}
                        {{--</div>--}}
                    {{--</div>--}}




                    @foreach($payment_methods as $payment)

                        <div class="animated flipInY col-lg-4 col-md-4 col-sm-4 col-xs-4">
                            <div class="tile-stats">
                                <div class="count">{{ $payment->amount!=null?$payment->amount:0 }}<span class="taka-icon">&#2547;</span></div>
                                <h3>{{ ucwords($payment->method_name) }} Sale</h3>
                                <p></p>
                            </div>
                        </div>

                    @endforeach

                    {{--<div class="animated flipInY col-lg-4 col-md-4 col-sm-4 col-xs-4">--}}
                        {{--<div class="tile-stats">--}}
                            {{--<div class="count">Loan: <span id="loan_amount">{{ $expenses_details->loan_amount }}</span></div>--}}
                            {{--<h3></h3>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    <div class="animated flipInY col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <div class="tile-stats">
                            <div class="count">{{ $expenses_details->loan_amount }}<span class="taka-icon">&#2547;</span></div>
                            <h3>Loan</h3>
                            <p></p>
                        </div>
                    </div>


                </div>


                <div class="row top_tiles">

                    {{--<div class="animated flipInY col-lg-4 col-md-4 col-sm-4 col-xs-4">--}}
                        {{--<div class="tile-stats">--}}
                            {{--<div class="count">Cash: <span id="petty_cash_amount">{{ $expenses_details->cash_opening_total_amount + $expenses_details->transfer_petty_cash -  $expenses_details->petty_cash_expense}}</span></div>--}}
                            {{--<h3></h3>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    <div class="animated flipInY col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <div class="tile-stats">
                            <div class="count">{{  $expenses_details->cash_opening_total_amount + $expenses_details->transfer_petty_cash -  $expenses_details->petty_cash_expense }}<span class="taka-icon">&#2547;</span></div>
                            <h3>Cash</h3>
                            <p></p>
                        </div>
                    </div>

                    {{--<div class="animated flipInY col-lg-4 col-md-4 col-sm-4 col-xs-4">--}}
                        {{--<div class="tile-stats">--}}
                            {{--<div class="count">Payable: {{ $expenses_details->total_payable }}</div>--}}
                            {{--<h3></h3>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    <div class="animated flipInY col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <div class="tile-stats">
                            <div class="count">{{ $expenses_details->total_payable!=null?$expenses_details->total_payable:0 }}<span class="taka-icon">&#2547;</span></div>
                            <h3>Payable</h3>
                            <p></p>
                        </div>
                    </div>

                    {{--<div class="animated flipInY col-lg-4 col-md-4 col-sm-4 col-xs-4">--}}
                        {{--<div class="tile-stats">--}}
                            {{--<div class="count">Cash In Hand: {{ $total }}</div>--}}
                            {{--<h3></h3>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    <div class="animated flipInY col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <div class="tile-stats">
                            <div class="count">{{ $total }}<span class="taka-icon">&#2547;</span></div>
                            <h3>Cash In Hand</h3>
                            <p></p>
                        </div>
                    </div>

                </div>


                <div class="x_content">
                    <div class="form-horizontal">

                        <div class="top-margin-20">
                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Particular</th>
                                    <th>Amount</th>
                                    <th>Debit From</th>
                                    <th>Created By</th>
                                    <th width="">Created At</th>
                                    <th width="11%">Action</th>
                                </tr>
                                </thead>
                                <tbody id="expense_table">

                                @php $expense_count=1; @endphp
                                @php $amount_total=0; @endphp

                                @foreach($daily_expense as $expense)

                                    @php $amount_total += $expense->amount; @endphp

                                    <tr id="expense_{{ $expense->expense_id }}">
                                        <td> {{ $expense_count++ }}</td>
                                        <td>{{ $expense->purpose }}</td>
                                        <td>{{ $expense->amount }}</td>
                                        <td>{{ $expense->debit_from }}</td>
                                        <td>{{ $expense->name }}</td>
                                        <td>{{ date('h:i a', strtotime( $expense->created_at )) }}</td>


                                        <td>
                                            <button type="button" class="btn btn-dark" onclick="editExpense('{{ $expense->expense_id }}', '{{ $expense->purpose }}', '{{ $expense->amount }}', '{{ $expense->debit_from }}')"
                                                    @if(Auth::user()->role == "superadmin" || Auth::user()->role == "admin")
                                                    @else
                                                    disabled
                                                    @endif>
                                                <i class="glyphicon glyphicon-pencil"></i>
                                            </button>
                                            <button type="button" class="btn btn-danger" onclick="deleteExpense({{ $expense->expense_id }})"
                                                    @if(Auth::user()->role == "superadmin" || Auth::user()->role == "admin")
                                                    @else
                                                    disabled
                                                    @endif>
                                                <i class="glyphicon glyphicon-remove"></i>
                                            </button>
                                        </td>
                                    </tr>

                                @endforeach

                                <tr>
                                    <td></td>
                                    <td><b>Total</b></td>
                                    <td><b>{{ $amount_total }}</b></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>

                                </tbody>
                            </table>

                        </div>


                    </div>
                </div>
            </div>
        </div>


    </div>


    <div class="modal fade" id="edit_expense_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Edit Expense</h4>
                </div>
                <div class="modal-body">

                    <input type="hidden" id="expense_id">

                    <div class="form-group">
                        <label for="edit_purpose">Expense Particular</label>
                        <input type="text" class="form-control" id="edit_purpose">
                    </div>

                    <div class="form-group">
                        <label for="edit_debit_from">Debit From</label>
                        <select class="form-control" id="edit_debit_from">
                            <option value="sales">Sales</option>
                            <option value="petty_cash">Petty Cash</option>
                            <option value="loan">Loan</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="edit_amount">Expense Amount</label>
                        <input type="number" class="form-control" id="edit_amount">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="edit_expense_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                </div>
            </div>

        </div>
    </div>



@endsection
