@extends('layouts.main')

@section('title', 'Loan Debit')

@section('content')

    <script src="/js/accounts.js"></script>

    <div class="row">

        <div class="col-lg-12 col-md-12 top-margin-20">
            <div class="x_panel">

                <div class="x_content">
                    <div class="form-horizontal form-label-left">


                        <div class="form-horizontal form-label-left top-margin-20">

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Return To</label>
                                <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                    <select class="form-control" id="owner_id">
                                        @foreach($loans_from as $item)
                                            <option value="{{ $item->owner_id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Debit From</label>
                                <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                    <select class="form-control" id="debit_from">
                                        <option value="sales">Sales</option>
                                        <option value="petty_cash">Petty Cash</option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Amount</label>
                                <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                    <input type="number" class="form-control" id="amount">
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5 ">
                                    <button class="btn btn-success btn-lg" id="add_loan_debit_button">Loan Debit</button>
                                </div>
                            </div>




                        </div>


                    </div>
                </div>
            </div>
        </div>

    </div>





@endsection
