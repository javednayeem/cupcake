@extends('layouts.main')

@section('title', 'Cash Closing')

@section('content')

    <script src="/js/accounts.js"></script>

    <div class="row">

        <div class="col-lg-6 col-md-6 top-margin-20">
            <div class="x_panel">

                <div class="x_content">

                    <div class="form-horizontal form-label-left top-margin-20">

                        <div class="form-group">
                            <label class="control-label col-md-3">1000 x </label>
                            <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback">
                                <input type="number" class="form-control" id="note_1000" value="{{ $cash_closing->note_1000 }}">
                            </div>
                            <div class="col-md-3">
                                <label class="control-label text-left">
                                    <span id="note_1000_total">{{ $cash_closing->note_1000 * 1000 }}</span> &#2547;
                                </label>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3">500 x </label>
                            <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback">
                                <input type="number" class="form-control" id="note_500" value="{{ $cash_closing->note_500 }}">
                            </div>
                            <div class="col-md-3">
                                <label class="control-label text-left">
                                    <span id="note_500_total">{{ $cash_closing->note_500 * 500 }}</span> &#2547;
                                </label>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3">100 x </label>
                            <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback">
                                <input type="number" class="form-control" id="note_100" value="{{ $cash_closing->note_100 }}">
                            </div>
                            <div class="col-md-3">
                                <label class="control-label text-left">
                                    <span id="note_100_total">{{ $cash_closing->note_100 * 100 }}</span> &#2547;
                                </label>
                            </div>
                        </div>



                        <div class="form-group">
                            <label class="control-label col-md-3">50 x </label>
                            <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback">
                                <input type="number" class="form-control" id="note_50" value="{{ $cash_closing->note_50 }}">
                            </div>
                            <div class="col-md-3">
                                <label class="control-label text-left">
                                    <span id="note_50_total">{{ $cash_closing->note_50 * 50 }}</span> &#2547;
                                </label>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3">20 x </label>
                            <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback">
                                <input type="number" class="form-control" id="note_20" value="{{ $cash_closing->note_20 }}">
                            </div>
                            <div class="col-md-3">
                                <label class="control-label text-left">
                                    <span id="note_20_total">{{ $cash_closing->note_20 * 20 }}</span> &#2547;
                                </label>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3">10 x </label>
                            <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback">
                                <input type="number" class="form-control" id="note_10" value="{{ $cash_closing->note_10 }}">
                            </div>
                            <div class="col-md-3">
                                <label class="control-label text-left">
                                    <span id="note_10_total">{{ $cash_closing->note_10 * 10 }}</span> &#2547;
                                </label>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3">5 x </label>
                            <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback">
                                <input type="number" class="form-control" id="note_5" value="{{ $cash_closing->note_5 }}">
                            </div>
                            <div class="col-md-3">
                                <label class="control-label text-left">
                                    <span id="note_5_total">{{ $cash_closing->note_5 * 5 }}</span> &#2547;
                                </label>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3">2 x </label>
                            <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback">
                                <input type="number" class="form-control" id="note_2" value="{{ $cash_closing->note_2 }}">
                            </div>
                            <div class="col-md-3">
                                <label class="control-label text-left">
                                    <span id="note_2_total">{{ $cash_closing->note_2 * 2 }}</span> &#2547;
                                </label>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3">1 x </label>
                            <div class="col-md-6 col-sm-6 col-xs-6 form-group has-feedback">
                                <input type="number" class="form-control" id="note_1" value="{{ $cash_closing->note_1 }}">
                            </div>
                            <div class="col-md-3">
                                <label class="control-label text-left">
                                    <span id="note_1_total">{{ $cash_closing->note_1 * 1 }}</span> &#2547;
                                </label>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-9">Total</label>
                            <div class="col-md-3">
                                <label class="control-label text-left">
                                    <span id="note_total">{{ $cash_closing->total_amount }}</span> &#2547;
                                </label>
                            </div>
                        </div>



                        @if(!$cash_closing->set || Auth::user()->role == "superadmin")

                            <div class="form-group top-margin-30">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
                                    <button class="btn btn-success btn-lg" id="save_cash_closing">Save Cash Closing</button>
                                </div>
                            </div>

                        @endif

                    </div>
                </div>
            </div>
        </div>


        <div class="col-lg-6 col-md-6 top-margin-20">
            <div class="x_panel">

                <div class="x_content">
                    <div class="form-horizontal form-label-left">

                        <div class="x_title">
                            <h2>Cash Transfer</h2>
                            <div class="clearfix"></div>
                        </div>

                        <div class="form-horizontal form-label-left top-margin-20">

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Transfer To</label>
                                <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                    <select class="form-control" id="transfer_to">
                                        <option value="sales">Sales</option>
                                        <option value="petty_cash">Petty Cash</option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Expense Amount</label>
                                <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                    <input type="number" class="form-control" id="amount">
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5 ">
                                    <button class="btn btn-success btn-lg" id="add_transfer_button">Transfer</button>
                                </div>
                            </div>




                        </div>


                    </div>
                </div>
            </div>
        </div>

    </div>


@endsection
