@extends('layouts.main')

@section('title', 'Guest')

@section('content')

    <script src="/js/customer.js"></script>

    @if (Auth::user()->license == 0)
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">

                        <div class="bs-example top-margin-30">
                            <div class="jumbotron">
                                <h1>License Has Been Expired</h1>
                                <p>Please Contact Your System Administrator For Further Information</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    @else



        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12 top-margin-30">
                <div class="x_panel">
                    <div class="top-margin-10">

                        <ul class="nav navbar-right panel_toolbox">
                            <li><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_customer_modal">Add Guest</button></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">

                        <div class="table-responsive top-margin-20">
                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                <tr class="headings">
                                    <th class="column-title">#</th>
                                    <th class="column-title">Guest Name</th>
                                    <th class="column-title">Email</th>
                                    <th class="column-title">Address</th>
                                    <th class="column-title">Phone No</th>
                                    <th class="column-title">Card No</th>
                                    <th class="column-title">Discount %</th>
                                    <th class="column-title">Joined</th>
                                    <th class="column-title no-link last" width="15%">
                                        <span class="nobr">Action</span>
                                    </th>
                                </tr>
                                </thead>

                                <tbody id="customer_table">

                                @php $customer_count=1; @endphp

                                @foreach($customers as $customer)

                                    <tr class="even" id="customer_{{ $customer->customer_id }}">
                                        <td> {{ $customer_count++ }}</td>
                                        <td id="customer_name_{{ $customer->customer_id }}">{{ $customer->customer_name }}</td>
                                        <td id="customer_email_{{ $customer->customer_id }}">{{ $customer->customer_email }}</td>
                                        <td id="customer_address_{{ $customer->customer_id }}">{{ $customer->customer_address }}</td>
                                        <td id="customer_phone_{{ $customer->customer_id }}">{{ $customer->customer_phone }}</td>
                                        <td id="card_no_{{ $customer->customer_id }}">{{ $customer->card_no }}</td>
                                        <td id="discount_percentage_{{ $customer->customer_id }}">{{ $customer->discount_percentage }}%</td>
                                        <td>{{ date('F j, Y', strtotime( $customer->created_at )) }}</td>
                                        <td>
                                            <button type="button" class="btn btn-dark" onclick="editCustomer('{{ $customer->customer_id }}', '{{ $customer->customer_name }}', '{{ $customer->customer_email }}', '{{ $customer->customer_address }}', '{{ $customer->customer_phone }}', '{{ $customer->card_no }}', '{{ $customer->discount_percentage }}')">
                                                <i class="glyphicon glyphicon-pencil"></i>
                                            </button>
                                            <button type="button" class="btn btn-danger" onclick="deleteCustomer({{ $customer->customer_id }})">
                                                <i class="glyphicon glyphicon-remove"></i>
                                            </button>
                                        </td>
                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>


                    </div>
                </div>
            </div>

        </div>


        @include('cupcake.customers.add-customer')


        <div class="modal fade" id="edit_customer_modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">x</button>
                        <h4 class="modal-title">Edit Guest</h4>
                    </div>
                    <div class="modal-body">

                        <input type="hidden" id="edit_customer_id">


                        <div class="form-group">
                            <label for="customer_name">Guest Name</label>
                            <input type="text" class="form-control" id="edit_customer_name" placeholder="Javed Nayeem">
                        </div>

                        <div class="form-group">
                            <label for="customer_phone">Phone</label>
                            <input type="text" class="form-control" id="edit_customer_phone" placeholder="01715123456">
                        </div>

                        <div class="form-group">
                            <label for="customer_address">Address</label>
                            <input type="text" class="form-control" id="edit_customer_address" placeholder="Dhanmondi">
                        </div>

                        <div class="form-group">
                            <label for="customer_email">Email</label>
                            <input type="email" class="form-control" id="edit_customer_email" placeholder="customer@cupcake.com">
                        </div>

                        <div class="form-group">
                            <label for="card_no">Card No</label>
                            <input type="text" class="form-control" id="edit_card_no">
                        </div>


                        <div class="form-group">
                            <label for="discount_percentage">Discount Percentage</label>
                            <input type="number" class="form-control" id="edit_discount_percentage" value="0" min="0">
                        </div>



                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                        <button type="button" id="edit_customer_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                    </div>
                </div>

            </div>
        </div>




    @endif








@endsection
