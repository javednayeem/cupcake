<div class="modal fade" id="add_customer_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">x</button>
                <h4 class="modal-title">Add Guest</h4>
            </div>
            <div class="modal-body">


                <div class="form-group">
                    <label for="course_name">Guest Name</label>
                    <input type="text" class="form-control" id="customer_name" placeholder="Javed Nayeem">
                </div>

                <div class="form-group">
                    <label for="description">Phone</label>
                    <input type="text" class="form-control" id="customer_phone" placeholder="01715123456">
                </div>

                <div class="form-group">
                    <label for="description">Address</label>
                    <input type="text" class="form-control" id="customer_address" placeholder="Dhanmondi">
                </div>

                <div class="form-group">
                    <label for="description">Email</label>
                    <input type="email" class="form-control" id="customer_email" placeholder="customer@cupcake.com">
                </div>

                <div class="form-group">
                    <label for="card_no">Card No</label>
                    <input type="text" class="form-control" id="card_no">
                </div>


                <div class="form-group">
                    <label for="discount_percentage">Discount Percentage</label>
                    <input type="number" class="form-control" id="discount_percentage" value="0" min="0">
                </div>



            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                <button type="button" id="add_customer_button" class="btn btn-success" data-dismiss="modal">Ok</button>
            </div>
        </div>

    </div>
</div>