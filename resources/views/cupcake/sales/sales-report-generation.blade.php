@extends('layouts.main')

@section('title', 'Sales Report')

@section('content')

    <script src="/js/salesReportGeneration.js"></script>

    <style>
        @media print {
            body * {
                visibility: hidden;
            }
            #order_details_modal_print, #order_details_modal_print * {
                visibility: visible;
                margin: 0;
                font-size: 10px;
                margin-top: 0px;
                padding-top: 0px;
            }
            #order_details_modal_print {
                position: absolute;
                left: 0;
                top: 0;
            }
        }
    </style>

    <div class="row">

        @if (Auth::user()->license == 0)
            @include('cupcake.partials.expired-license')
        @else

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Sales Report Generation</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br>
                        <div class="form-horizontal form-label-left">


                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Select Report Type</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select class="form-control" id="select_report_type">
                                        <option value="0">Choose option</option>
                                        {{--<option value="1">Sales By Item</option>--}}
                                        <option value="month_wise_sale">Monthly Sale</option>
                                        <option value="invoice_wise_sale">Sales By Invoice</option>
                                        <option value="order_summary">Order Summary</option>
                                        <option value="waiter_wise_sale">Sales By Waiter</option>
                                        <option value="table_wise_sale">Sales By Table</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group div-hide" id="select_waiter_div">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Waiter</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select class="form-control" id="select_waiter">
                                        @foreach($user_data as $user)
                                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group div-hide" id="select_table_div">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Table</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select class="form-control" id="select_table">
                                        @foreach($table_data as $table)
                                            <option value="{{ $table->table_id }}">{{ $table->table_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group div-hide" id="select_order_id">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Order Number</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="order_id_input" class="form-control" placeholder="">
                                </div>
                            </div>


                            <div class="form-group div-hide" id="select_start_date">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Start Date</label>
                                <div class="col-md-6 col-sm-6 col-xs-12 input-group date" id='myDatepicker2' >
                                    <input type='text' class="form-control" id="start_date"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>

                            <div class="form-group div-hide" id="select_end_date">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">End Date</label>
                                <div class="col-md-6 col-sm-6 col-xs-12 input-group date" id='myDatepicker3' >
                                    <input type='text' class="form-control" id="end_date"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>



                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">
                                    <button class="btn btn-success" id="generate_report_button">Generate Report</button>
                                </div>
                            </div>
                            <div id="printDiv"></div>

                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content form-group">
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="count" id="total_sales">0 &#2547;</div>
                                <h3>Sales</h3>
                                <p></p>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="count" id="total_vat">0 &#2547;</div>
                                <h3>VAT Collected</h3>
                                <p></p>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="count" id="total_service_charge">0 &#2547;</div>
                                <h3>Service Charge</h3>
                                <p></p>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="count" id="total_discount">0 &#2547;</div>
                                <h3>Discounts</h3>
                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Report</h2>

                        <ul class="nav navbar-right panel_toolbox">
                            <li>

                                <select id="payment_method" class="form-control">
                                    <option value="all">All</option>
                                    <option value="cash">Cash</option>
                                    <option value="card">Card</option>
                                </select>

                            </li>
                        </ul>

                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">

                        <div class="table-responsive">
                            <table class="table table-striped jambo_table bulk_action" id="report_table">
                                <thead id="report_table_thead">
                                <tr class="headings">
                                    <th class="column-title">Order #</th>
                                    <th class="column-title">Date</th>
                                    <th class="column-title">Served By</th>
                                    <th class="column-title">Item</th>
                                    <th class="column-title">Total Amount</th>
                                    <th class="column-title">Service Charge</th>
                                    <th class="column-title">VAT</th>
                                    <th class="column-title">Discount</th>
                                    <th class="column-title">Discount Ref.</th>
                                    <th class="column-title">Total</th>
                                    <th class="column-title">Status</th>
                                </tr>
                                </thead>

                                <tbody id="report_table_tbody">
                                </tbody>

                            </table>
                        </div>


                    </div>
                </div>
            </div>

        @endif

    </div>




    <div class="modal fade" id="order_details_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                </div>
                <div class="modal-body" id="order_details_modal_print">

                    <div class="x_title text-center">
                        <img src="/images/restaurant-logo/{{ getRstaurantLogo() }}" id="logo-icon" >
                        <h4>{{ $restaurant_data->restaurants_name==''? "Cupcake":$restaurant_data->restaurants_name }}</h4>
                        <h5>Address :
                            {{ $restaurant_data->address}}
                            {{ $restaurant_data->area==null? '':', '.$restaurant_data->area }}
                            {{ $restaurant_data->city==null? '':', '.$restaurant_data->city }}
                        </h5>
                        <h5>VAT Reg. No : {{ $restaurant_data->vat_no==null? 'N/A':$restaurant_data->vat_no }}</h5>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_title form-group">
                        <h5>Customer : <a id="payment_modal_customer_name"></a></h5>
                        <h5>Served By : <a id="payment_modal_served_by"></a></h5>
                        <h5>Time : <a id="payment_modal_time"></a></h5>
                        <h5>Order No : <a id="payment_modal_order_no"></a></h5>
                        <h5>Table : <a id="payment_modal_table"></a></h5>
                    </div>


                    <div class="x_title table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th width="5%">#</th>
                                <th width="50%">Item Name</th>
                                <th>U.Price</th>
                                <th>Qty</th>
                                <th>Total</th>
                            </tr>
                            </thead>
                            <tbody id="payment_modal_order_table">

                            </tbody>
                        </table>
                    </div>

                    <div class="x_title form-group font-bold">
                        <a>Order Total : <a id="payment_modal_order_total">0</a></a><br>
                        <a>Service Charge ({{$restaurant_data->service_charge}}%) : <a id="payment_modal_service_charge">0</a></a><br>
                        <a>VAT Total ({{$restaurant_data->vat_percentage}}%): <a id="payment_modal_vat_total">0</a></a><br>
                        <p>Total Amount : <a id="payment_modal_total_amount">0</a></p>
                    </div>

                    <div class="x_title form-group font-bold">
                        <a>Payment Method : <a id="payment_modal_payment_method"></a></a><br>
                        <a>Paid Amount : <a id="payment_modal_paid_amount"></a></a><br>
                    </div>

                    <h6 class="text-center">Thank You, Please Come Again</h6>



                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-success" data-dismiss="modal" onclick="window.print();">Print</button>
                </div>
            </div>

        </div>
    </div>





@endsection
