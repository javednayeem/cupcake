@extends('layouts.main')

@section('title', 'Sales Report')

@section('content')

    <script src="/js/sales.js"></script>

    @foreach($user_data as $user)
        <script type="application/javascript">
            <?php echo 'sales_array_push("'.$user->name.'", "'.$user->order_count.'");'; ?>
        </script>
    @endforeach

    @foreach($reports as $report)
        <script type="application/javascript">
            <?php echo 'sales_by_month_push('.$report->MONTH.', '.$report->COUNT.', '.$report->BILL.');'; ?>
        </script>
    @endforeach

    @foreach($week_reports as $report)
        <script type="application/javascript">
            <?php echo 'sales_by_week_push('.$report->WEEKDAY.', '.$report->COUNT.', '.$report->BILL.');'; ?>
        </script>
    @endforeach

    @foreach($table_reports as $report)
        <script type="application/javascript">
            <?php echo 'sales_by_table_array_push("'.$report->table_name.'", "'.$report->table_order.'");'; ?>
        </script>
    @endforeach


    @foreach($menu_analytics as $menu_analytic)
        <script type="application/javascript">
            <?php echo 'sales_by_menu_push("'.$menu_analytic['menu_name'].'", "'.$menu_analytic['menu_count'].'");'; ?>
        </script>
    @endforeach



    <div class="row">

        @if (Auth::user()->license == 0)

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">

                        <div class="bs-example top-margin-30">
                            <div class="jumbotron">
                                <h1>License Has Been Expired</h1>
                                <p>Please Contact Your System Administrator For Further Information</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        @else

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Orders</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <canvas id="sales_by_month_orders"></canvas>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Sales By Month</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <canvas id="sales_by_month_bills"></canvas>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Sales By Waiter</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        {{--<canvas id="canvasDoughnut"></canvas>--}}
                        <canvas id="chart_sales_by_waiter" ></canvas>

                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Customers By Weeks</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <canvas id="sales_by_week_orders"></canvas>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Sales By Table</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <canvas id="sales_by_table_orders"></canvas>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Sales By Menu</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <canvas id="sales_by_menu_orders"></canvas>
                    </div>
                </div>
            </div>

        @endif


    </div>

    <div class="modal fade" id="order_details_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                </div>
                <div class="modal-body">

                    <div class="x_title text-center">
                        <img src="/images/restaurant-logo/{{ getRstaurantLogo() }}" id="logo-icon" >
                        <h4>{{ $restaurant_data->restaurants_name==''? "Cupcake":$restaurant_data->restaurants_name }}</h4>
                        <h5>Address :
                            {{ $restaurant_data->address}}
                            {{ $restaurant_data->area==null? '':', '.$restaurant_data->area }}
                            {{ $restaurant_data->city==null? '':', '.$restaurant_data->city }}
                        </h5>
                        <h5>VAT Reg. No : {{ $restaurant_data->vat_no==null? 'N/A':', '.$restaurant_data->vat_no }}</h5>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_title form-group">
                        <h5>Customer : <a id="payment_modal_customer_name"></a></h5>
                        <h5>Served By : <a id="payment_modal_served_by"></a></h5>
                        <h5>Time : <a id="payment_modal_time"></a></h5>
                        <h5>Order No : <a id="payment_modal_order_no"></a></h5>
                        <h5>Table : <a id="payment_modal_table"></a></h5>
                    </div>


                    <div class="x_title table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th width="5%">#</th>
                                <th width="50%">Item Name</th>
                                <th>U.Price</th>
                                <th>Qty</th>
                                <th>Total</th>
                            </tr>
                            </thead>
                            <tbody id="payment_modal_order_table">

                            </tbody>
                        </table>
                    </div>

                    <div class="x_title form-group font-bold">
                        <a>Order Total : <a id="payment_modal_order_total">0</a></a><br>
                        <a>Service Charge ({{$restaurant_data->service_charge}}%) : <a id="payment_modal_service_charge">0</a></a><br>
                        <a>VAT Total ({{$restaurant_data->vat_percentage}}%): <a id="payment_modal_vat_total">0</a></a><br>
                        <p>Total Amount : <a id="payment_modal_total_amount">0</a></p>
                    </div>

                    <div class="x_title form-group font-bold">
                        <a>Payment Method : <a id="payment_modal_payment_method"></a></a><br>
                        <a>Paid Amount : <a id="payment_modal_paid_amount"></a></a><br>
                    </div>

                    <h6 class="text-center">Thank You, Please Come Again</h6>



                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-success" data-dismiss="modal">Print</button>
                </div>
            </div>

        </div>
    </div>





@endsection
