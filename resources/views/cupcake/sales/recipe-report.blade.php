@extends('layouts.main')
@section('title', 'Recipe Report')

@section('content')

    <script src="/js/revenueReportGeneration.js"></script>
    <link href="/css/report.css" rel="stylesheet">


    @if (Auth::user()->license == 0)
        @include('cupcake.partials.expired-license')
    @else

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 print-hidden">
                <div class="x_panel">

                    <div class="x_content">
                        <br>
                        <form class="form-horizontal form-label-left" action="/recipe-report" method="POST">

                            <div class="form-group top-margin-20">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Menu</label>
                                <div class="col-md-6 col-sm-6 col-xs-12 input-group">
                                    <select class="form-control" name="menu_id" id="menu_id">
                                        <option value="0">All Menus</option>
                                        @foreach($menus as $menu)
                                            <option value="{{ $menu->menu_id }}">{{ $menu->menu_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group top-margin-20">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Item</label>
                                <div class="col-md-6 col-sm-6 col-xs-12 input-group">
                                    <select class="form-control" name="menu_item_id" id="menu_item_id">
                                        <option value="0">All Items</option>
                                        @foreach($menu_items as $item)
                                            <option value="{{ $item->menu_item_id }}">{{ $item->item_name }} {{ $item->ratio }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">
                                    <button class="btn btn-success">Generate Report</button>
                                </div>
                            </div>
                            <div id="printDiv"></div>

                        </form>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12" id="report_div">
                <div class="x_panel">

                    @include('cupcake.sales.report-header')


                    <div class="x_content">

                        <div class="table-responsive">
                            <table class="table table-striped jambo_table bulk_action" id="report_table">
                                <thead>

                                <tr class="headings">
                                    <th>#</th>
                                    <th>Category</th>
                                    <th>Menu Item</th>
                                    <th>Item Price</th>
                                    <th>Ingredients</th>
                                    <th>Total Item Cost</th>
                                </tr>

                                </thead>

                                <tbody id="report_table">

                                @php $count=1; @endphp
                                @php $total_item_cost=0; @endphp

                                @foreach($menu_items as $item)

                                    @php $this_item_cost=0; @endphp

                                    @php
                                        $recipes = DB::table('recipes')
                                        ->select('recipes.quantity', 'inv_product.product_name', 'inv_product.avg_price', 'inv_units.unit_name')
                                        ->where('item_id', $item->menu_item_id)
                                        ->join('inv_product', 'inv_product.product_id', 'recipes.product_id')
                                        ->join('inv_units', 'inv_units.unit_id', 'inv_product.unit_id')
                                        ->get();
                                    @endphp

                                    <tr>
                                        <td> {{ $count++ }}</td>
                                        <td> {{ $item->menu_name }}</td>
                                        <td> {{ $item->item_name . ' ' . $item->ratio }}</td>
                                        <td> {{ $item->price }}Tk</td>
                                        <td>
                                            @foreach($recipes as $recipe)
                                                @php
                                                    $item_cost=($recipe->avg_price * $recipe->quantity);
                                                    $this_item_cost += $item_cost;
                                                @endphp
                                                <li><b>{{ $recipe->product_name }}: {{ $recipe->quantity }}{{ $recipe->unit_name }} - {{ $item_cost }}Tk</b></li>
                                            @endforeach
                                        </td>
                                        <td> {{ $this_item_cost }}Tk</td>

                                    </tr>

                                @endforeach

                                </tbody>

                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>



        <script>
            $('#report_header').text('Recipe Report');
        </script>
    @endif








@endsection
