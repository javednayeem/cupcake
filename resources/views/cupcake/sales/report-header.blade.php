<div class="x_content form-group">
    <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="tile-stats text-center">
            <img src="/images/restaurant-logo/{{ $restaurant_data->restaurant_img }}" width="5%">
            <div class="count">{{ $restaurant_data->restaurants_name==''? "Cupcake":$restaurant_data->restaurants_name }}</div>
            <h3>
                <b>
                    {{ $restaurant_data->address}}
                    {{ $restaurant_data->area==null? '':', '.$restaurant_data->area }}
                    {{ $restaurant_data->city==null? '':', '.$restaurant_data->city }}
                </b>
            </h3>
            <div class="count" id="report_header"></div>
            <h3 id="date_header"></h3>
            <div class="form-group text-center top-margin-20">
                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 20px">
                    <button class="btn btn-primary hidden-print" onclick="window.print()">Print Report</button>
                </div>
            </div>
        </div>
    </div>
</div>