@extends('layouts.main')

@section('title', 'Revenue Report')

@section('content')

    <script src="/js/revenueReportGeneration.js"></script>
    <link href="/css/report.css" rel="stylesheet">


    @if (Auth::user()->license == 0)
        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12 print-hidden">
                <div class="x_panel">
                    <div class="x_content">

                        <div class="bs-example top-margin-30">
                            <div class="jumbotron">
                                <h1>License Has Been Expired</h1>
                                <p>Please Contact Your System Administrator For Further Information</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    @else

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 print-hidden">
                <div class="x_panel">

                    <div class="x_content">
                        <br>
                        <div class="form-horizontal form-label-left">

                            <div class="form-group top-margin-20" id="select_start_date">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Start Date</label>
                                <div class="col-md-6 col-sm-6 col-xs-12 input-group date" id='myDatepicker2'>
                                    <input type='text' class="form-control" id="start_date" value="{{ date("Y-m-01") }}"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>


                            <div class="form-group" id="select_end_date">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">End Date</label>
                                <div class="col-md-6 col-sm-6 col-xs-12 input-group date" id='myDatepicker3'>
                                    <input type='text' class="form-control" id="end_date" value="{{ date("Y-m-d") }}"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>




                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">
                                    <button class="btn btn-success" id="generate_report_button">Generate Report</button>
                                </div>
                            </div>
                            <div id="printDiv"></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12" id="report_div">
                <div class="x_panel">

                    <div class="x_content form-group">
                        <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="tile-stats text-center">

                                <img src="/images/restaurant-logo/{{ getRstaurantLogo() }}" width="5%">
                                <div class="count">{{ $restaurant_data->restaurants_name==''? "Cupcake":$restaurant_data->restaurants_name }}</div>
                                <h3>
                                    <b>
                                        {{ $restaurant_data->address}}
                                        {{ $restaurant_data->area==null? '':', '.$restaurant_data->area }}
                                        {{ $restaurant_data->city==null? '':', '.$restaurant_data->city }}
                                    </b>
                                </h3>

                                <div class="count" id="report_header">Revenue Report</div>
                                <h3 id="date_header"></h3>
                                <div class="form-group text-center top-margin-20">
                                    <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 20px">
                                        <button class="btn btn-primary hidden-print" onclick="window.print()">Print Report</button>
                                    </div>
                                </div>

                            </div>





                        </div>
                    </div>


                    <div class="x_content">

                        <div class="table-responsive">
                            <table class="table table-striped jambo_table bulk_action" id="report_table">
                                <thead>

                                <tr class="headings">
                                    <th>#</th>
                                    <th>Date</th>
                                    <th>Cash Sale</th>
                                    <th>Card Sale</th>
                                    <th>Total Expense</th>
                                    <th>Cash In Hand</th>
                                </tr>

                                </thead>

                                <tbody id="report_table_tbody">

                                @php $count=1; @endphp
                                @php $cash_sale=0; @endphp
                                @php $card_sale=0; @endphp
                                @php $total_expense=0; @endphp
                                @php $cash_in_hand=0; @endphp

                                @foreach($work_period as $period)

                                    @php
                                        $cash = $period->cash_sale==null?0:$period->cash_sale;
                                        $card = $period->card_sale==null?0:$period->card_sale;
                                        $expense = $period->total_expense==null?0:$period->total_expense;
                                        $cash_in_hand_temp = $cash - $expense;

                                        $cash_sale += $cash;
                                        $card_sale += $card;
                                        $total_expense += $expense;
                                        $cash_in_hand += $cash_in_hand_temp;
                                    @endphp


                                    <tr>
                                        <td> {{ $count++ }}</td>
                                        <td>{{ date('F j, Y', strtotime( $period->start_time )) }}</td>
                                        <td>{{ $cash }}</td>
                                        <td>{{ $card }}</td>
                                        <td>{{ $expense }}</td>
                                        <td>{{ $cash_in_hand_temp }}</td>
                                    </tr>

                                @endforeach

                                <tr>
                                    <th></th>
                                    <td><b>Total</b></td>
                                    <td><b>{{ $cash_sale }}</b></td>
                                    <td><b>{{ $card_sale }}</b></td>
                                    <td><b>{{ $total_expense }}</b></td>
                                    <td><b>{{ $cash_in_hand }}</b></td>
                                </tr>

                                </tbody>

                            </table>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    @endif








@endsection
