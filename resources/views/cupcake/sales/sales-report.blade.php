@extends('layouts.main')

@section('title', 'Sales Report')

@section('content')

    <script src="/js/salesReportGeneration.js"></script>
    <link href="/css/report.css" rel="stylesheet">


    @if (Auth::user()->license == 0)
        @include('cupcake.partials.expired-license')
    @else


        <div class="row">

            <script type="application/javascript">
                pushChargesInfo('{{$restaurant_data->price_including_vat}}', '{{$restaurant_data->vat_percentage}}');
            </script>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Sales Report Generation</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br>
                        <div class="form-horizontal form-label-left">


                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Select Report Type</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select class="form-control" id="select_report_type">
                                        <option value="0">Choose option</option>

                                        @foreach($report_type as $type)
                                            <option value="{{ $type->report_value }}">{{ $type->report_name }}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>


                            <div class="form-group top-margin-20" id="select_start_date">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Start Date</label>
                                <div class="col-md-6 col-sm-6 col-xs-12 input-group date" id='myDatepicker2'>
                                    <input type='text' class="form-control" id="start_date" value="{{ date("Y-m-01") }}"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>


                            <div class="form-group" id="select_end_date">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">End Date</label>
                                <div class="col-md-6 col-sm-6 col-xs-12 input-group date" id='myDatepicker3'>
                                    <input type='text' class="form-control" id="end_date" value="{{ date("Y-m-d") }}"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>


                            <div class="form-group div-hide" id="select_waiter_div">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Waiter</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select class="form-control" id="select_waiter">
                                        <option value="0">All Waiter</option>
                                        @foreach($user_data as $user)
                                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                            <div class="form-group div-hide" id="select_item_div">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Item</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select class="form-control" id="select_item">
                                        <option value="0">All Items</option>
                                        @foreach($menu_items as $items)
                                            <option value="{{ $items->menu_item_id }}">{{ $items->item_name }} {{ $items->ratio }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group div-hide" id="select_menu_div">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Menu</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select class="form-control" id="menu_id">
                                        <option value="0">All Menu</option>
                                        @foreach($menus as $menu)
                                            <option value="{{ $menu->menu_id }}">{{ $menu->menu_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group div-hide" id="select_table_div">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Table</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select class="form-control" id="select_table">
                                        @foreach($table_data as $table)
                                            <option value="{{ $table->table_id }}">{{ $table->table_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group div-hide" id="select_order_id">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Order Number</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="order_id_input" class="form-control">
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">
                                    <button class="btn btn-success" id="generate_report_button">Generate Report</button>
                                </div>
                            </div>
                            <div id="printDiv"></div>

                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content form-group">
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="count" id="total_sales">0 &#2547;</div>
                                <h3>Sales</h3>
                                <p></p>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="count" id="total_vat">0 &#2547;</div>
                                <h3>VAT Collected</h3>
                                <p></p>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="count" id="total_service_charge">0 &#2547;</div>
                                <h3>Service Charge</h3>
                                <p></p>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="count" id="total_discount">0 &#2547;</div>
                                <h3>Discounts</h3>
                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-12 col-sm-12 col-xs-12" id="report_div">
                <div class="x_panel">

                    <div class="x_content form-group">
                        <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="tile-stats text-center">
                                <img src="/images/restaurant-logo/{{ getRstaurantLogo() }}" width="5%">
                                <div class="count">{{ $restaurant_data->restaurants_name==''? "Cupcake":$restaurant_data->restaurants_name }}</div>
                                <h3>
                                    <b>
                                        {{ $restaurant_data->address}}
                                        {{ $restaurant_data->area==null? '':', '.$restaurant_data->area }}
                                        {{ $restaurant_data->city==null? '':', '.$restaurant_data->city }}
                                    </b>
                                </h3>
                                <div class="count" id="report_header">Report</div>
                                <h3 id="date_header"></h3>
                                <div class="form-group text-center top-margin-20 ">
                                    <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 20px">
                                        <button class="btn btn-primary hidden-print" onclick="window.print()">Print Report</button>
                                        <button class="btn btn-dark hidden-print" id="email_report_button">Email Report</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>



                    <div class="x_content">

                        <div class="table-responsive">
                            <table class="table table-striped jambo_table bulk_action" id="report_table">
                                <thead>
                                <tr class="headings" id="report_table_header">

                                </tr>
                                </thead>

                                <tbody id="report_table_tbody">
                                </tbody>

                            </table>
                        </div>


                    </div>
                </div>
            </div>

        </div>


        <div class="row">
            <div class="col-md-3 col-sm-2 col-xs-12" id="settle_div">
                <div class="x_panel">
                    @include('cupcake.order.receipt')
                </div>
            </div>
        </div>


        <div class="modal fade hidden-print" id="order_details_modal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content" id="order_payment_div">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">x</button>
                    </div>
                    <div class="modal-body" id="print_order_payment_modal">

                        @include('cupcake.order.receipt')


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                        <button type="button" id="print_receipt_button" class="btn btn-success" data-dismiss="modal">Print</button>
                    </div>
                </div>

            </div>
        </div>

    @endif


@endsection
