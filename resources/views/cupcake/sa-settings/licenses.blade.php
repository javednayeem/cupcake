@extends('layouts.main')

@section('title', 'Licenses')

@section('content')

    <script src="/js/superadmin.js"></script>

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_content form-group">
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-edit"></i>
                            </div>
                            <div class="count">{{ count($licenses) }}</div>
                            <h3>Total Licenses</h3>
                            <p></p>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-money"></i>
                            </div>
                            <div class="count">{{ $license_data['paid_license_count']==null?0:$license_data['paid_license_count'] }}</div>
                            <h3>Paid Licenses</h3>
                            <p></p>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-user"></i>
                            </div>
                            <div class="count">{{ $license_data['trial_license_count']==null?0:$license_data['trial_license_count'] }}</div>
                            <h3>Trial Licenses</h3>
                            <p></p>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-users"></i>
                            </div>
                            <div class="count">{{ 0 }}</div>
                            <h3>Demo</h3>
                            <p></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Restaurants</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br>
                    <div data-parsley-validate="" class="form-horizontal form-label-left">


                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4 col-xs-12">Select Restaurant</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control" id="select_restaurant_id">
                                    <option value="0">View All Licenses</option>

                                    @foreach($restaurants as $restaurant)
                                        <option value="{{ $restaurant->restaurants_id}}">{{ $restaurant->restaurants_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">
                                <button class="btn btn-success" id="view_license_button">
                                    View
                                </button>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Licenses</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#restaurant_license_modal">Create License</button>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">

                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                            <tr class="headings">
                                <th class="column-title">#</th>
                                <th class="column-title">Restaurant</th>
                                <th class="column-title">License Key</th>
                                <th class="column-title">License Start Date</th>
                                <th class="column-title">License End Date</th>
                                <th class="column-title">Duration</th>
                                <th class="column-title">Status</th>
                                <th class="column-title">License Type</th>
                                <th class="column-title">Created By</th>
                                <th class="column-title">Created At</th>
                                <th class="column-title">Action</th>
                            </tr>
                            </thead>

                            <tbody>

                            @php
                                $license_count=1;
                                $today = date("Y-m-d");
                            @endphp

                            @foreach($licenses as $license)
                                <tr id="license_{{ $license->license_id }}">
                                    <td scope="row"> {{ $license_count++ }}</td>
                                    <td>{{ $license->restaurants_name }}</td>
                                    <td>{{ $license->license_key }}</td>
                                    <td>{{ date('F j, Y', strtotime( $license->license_start_date )) }}</td>
                                    <td>{{ date('F j, Y', strtotime( $license->license_end_date )) }}</td>
                                    <td>{{ duration($license->license_start_date, $license->license_end_date) }}</td>
                                    <td>
                                        @if($license->status==1)

                                            @php $reminder = checkLicenseDuration($today, $license->license_end_date); @endphp

                                            {{  $reminder==0?'License Expired':$reminder.' Remaining' }}
                                        @else
                                            <button type="button" class="btn btn-primary" onclick="activateLicense({{ $license->license_id }})">
                                                Activate
                                            </button>
                                        @endif

                                    </td>
                                    <td>{{ ucfirst($license->license_type) }}</td>
                                    <td>{{ $license->name }}</td>
                                    <td>{{ date('F j, Y', strtotime( $license->created_at )) }}</td>
                                    <td>
                                        <button type="button" class="btn btn-danger" onclick="deleteLicense({{ $license->license_id }})">
                                            <i class="glyphicon glyphicon-remove"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>

                        </table>
                    </div>


                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="restaurant_license_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content" style="margin-top: 200px; ">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Create New License</h4>
                </div>
                <div class="modal-body">

                    <div class="form-group" style="margin-left: 10px;">
                        <label>Restaurant</label>
                        <select class="form-control" id="restaurants_id">
                            @foreach($restaurants as $restaurant)
                                <option value="{{ $restaurant->restaurants_id}}">{{ $restaurant->restaurants_name }}</option>
                            @endforeach
                        </select>
                    </div>


                    <fieldset>
                        <div class="control-group">
                            <div class="controls">
                                <div class="col-md-11 xdisplay_inputx form-group has-feedback">
                                    <label>License Start date</label>
                                    <input type="text" class="form-control has-feedback-left" id="single_cal1" placeholder="License Start date" aria-describedby="inputSuccess2Status">
                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                    <span id="inputSuccess2Status" class="sr-only">(success)</span>
                                </div>
                            </div>
                        </div>
                    </fieldset>

                    <div class="form-group" style="margin-left: 10px;">
                        <label>Duration</label>
                        <select class="form-control" id="license_duration">
                            <option value='0'>Select </option>
                            @foreach($license_periods as $period)
                                <option value='{{ $period->days }}'>{{ $period->duration_str }}</option>
                            @endforeach
                        </select>
                    </div>


                    <fieldset>
                        <div class="control-group">
                            <div class="controls">
                                <div class="col-md-11 xdisplay_inputx form-group has-feedback">
                                    <label>License End date</label>
                                    <input type="text" disabled class="form-control has-feedback-left" id="single_cal2" placeholder="License End date" aria-describedby="inputSuccess2Status_2">
                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                    <span id="inputSuccess2Status_2" class="sr-only">(success)</span>
                                </div>
                            </div>
                        </div>
                    </fieldset>


                    <div class="form-group" style="margin-left: 10px;">
                        <label>License Type</label>
                        <select class="form-control" id="license_type">
                            <option value='paid' selected>Paid License</option>
                            <option value='trial'>Trial License</option>
                        </select>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="assign_license_button" class="btn btn-success" data-dismiss="modal">Assign</button>
                </div>
            </div>

        </div>
    </div>


@endsection
