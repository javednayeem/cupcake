@extends('layouts.main')

@section('title', 'View Orders')

@section('content')

    <script src="/js/superadmin.js"></script>


    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">

                    <ul class="nav navbar-right panel_toolbox">
                        <li><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#fix_date_modal">Fix Order Date</button></li>
                    </ul>


                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br>
                    <form class="form-horizontal form-label-left" action="/view-order" method="post" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4 col-xs-12">Select Report Type</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control" name="order_status">
                                    <option value="0">Select All</option>
                                    <option value="paid">Paid</option>
                                    <option value="placed">Placed</option>
                                    <option value="accepted_void">accepted_void</option>
                                </select>
                            </div>
                        </div>


                        <div class="form-group top-margin-20">
                            <label class="control-label col-md-4 col-sm-4 col-xs-12">Start Date</label>
                            <div class="col-md-6 col-sm-6 col-xs-12 input-group date" id='myDatepicker2'>
                                <input type='text' class="form-control" name="start_date" value="{{ date("Y-m-01") }}"/>
                                <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                            </div>
                        </div>


                        <div class="form-group" id="select_end_date">
                            <label class="control-label col-md-4 col-sm-4 col-xs-12">End Date</label>
                            <div class="col-md-6 col-sm-6 col-xs-12 input-group date" id='myDatepicker3'>
                                <input type='text' class="form-control" name="end_date" value="{{ date("Y-m-d") }}"/>
                                <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                            </div>
                        </div>


                        {{--<div class="form-group div-hide" id="select_waiter_div">--}}
                        {{--<label class="control-label col-md-4 col-sm-4 col-xs-12">Waiter</label>--}}
                        {{--<div class="col-md-6 col-sm-6 col-xs-12">--}}
                        {{--<select class="form-control" id="select_waiter">--}}
                        {{--<option value="0">All Waiter</option>--}}
                        {{--@foreach($user_data as $user)--}}
                        {{--<option value="{{ $user->id }}">{{ $user->name }}</option>--}}
                        {{--@endforeach--}}
                        {{--</select>--}}
                        {{--</div>--}}
                        {{--</div>--}}


                        {{--<div class="form-group div-hide" id="select_order_id">--}}
                        {{--<label class="control-label col-md-4 col-sm-4 col-xs-12">Order Number</label>--}}
                        {{--<div class="col-md-6 col-sm-6 col-xs-12">--}}
                        {{--<input type="text" id="order_id_input" class="form-control">--}}
                        {{--</div>--}}
                        {{--</div>--}}


                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">
                                <button type="submit" class="btn btn-success">View Order</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12 top-margin-20">
            <div class="x_panel">
                <div class="x_content form-group">
                    <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="tile-stats text-center">
                            <img src="/images/restaurant-logo/{{ getRstaurantLogo() }}" width="5%">
                            <div class="count">CupCake</div>
                            <div class="count" id="report_header">Number of Orders: {{ count($orders) }}</div>
                            <h3 id="date_header"></h3>
                        </div>
                    </div>
                </div>

                <div class="top-margin-20">

                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                        <tr class="headings">
                            <th class="text-center">Invoice #</th>
                            <th class="text-center">Restaurant</th>
                            <th class="text-center">Served By</th>
                            <th class="text-center">Item Qty</th>
                            <th class="text-center">Item Amount</th>
                            <th class="text-center">VAT</th>
                            <th class="text-center">Service Charge</th>
                            <th class="text-center">Discount</th>
                            <th class="text-center">Net Amount</th>
                            <th class="text-center">Order Type</th>
                            <th class="text-center">Order Status</th>
                            <th class="text-center">Date</th>
                            <th class="column-title" width="11%">Action</th>
                        </tr>
                        </thead>

                        <tbody>

                        @php
                            $total_item = 0;
                            $total_bill = 0;
                            $total_vat = 0;
                            $total_service_charge = 0;
                            $total_discount = 0;
                            $total_total_bill = 0;
                        @endphp

                        @foreach($orders as $order)

                            @php
                                $total_item += $order->total_item;
                                $total_bill += $order->bill;
                                $total_vat += $order->vat;
                                $total_service_charge += $order->service_charge;
                                $total_discount += $order->discount;
                                $total_total_bill += $order->total_bill;
                            @endphp



                            <tr class="even pointer">
                                <td class="text-center">{{ generateInvoiceNumber($order->order_id, $order->created_at) }}</td>
                                <td class="text-center">{{ $order->restaurants_name }}</td>
                                <td class="text-center">{{ $order->waiter_name }}</td>
                                <td class="text-center">{{ $order->total_item }}</td>
                                <td class="text-center">{{ $order->bill }}</td>
                                <td class="text-center">{{ $order->vat }}</td>
                                <td class="text-center">{{ $order->service_charge }}</td>
                                <td class="text-center">{{ $order->discount }}</td>
                                <td class="text-center">{{ $order->total_bill }}</td>
                                <td class="text-center">{{ $order->order_type }}</td>
                                <td class="text-center">{{ $order->order_status }}</td>
                                <td class="text-center">{{ date('F j, Y', strtotime( $order->created_at )) }}</td>
                                <td>
                                    <a type="button" href="/edit-order/{{ $order->order_id }}" class="btn btn-dark" title="Edit User">
                                        <i class="glyphicon glyphicon-pencil"></i>
                                    </a>

                                    <button type="button" class="btn btn-danger" title="Delete User" onclick="deleteUser({{ $order->order_id }})">
                                        <i class="glyphicon glyphicon-remove"></i>
                                    </button>
                                </td>
                            </tr>

                        @endforeach

                        <tr class="even pointer">
                            <th class="text-center">Total</th>
                            <th></th>
                            <th></th>
                            <th class="text-center">{{ $total_item }}</th>
                            <th class="text-center">{{ $total_bill }}</th>
                            <th class="text-center">{{ $total_vat }}</th>
                            <th class="text-center">{{ $total_service_charge }}</th>
                            <th class="text-center">{{ $total_discount }}</th>
                            <th class="text-center">{{ $total_total_bill }}</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>


                        </tbody>
                    </table>

                    {{ $orders->links() }}

                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="fix_date_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Fix Order Date</h4>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label>Starting Order Id</label>
                        <input type="text" class="form-control" id="start_order_id">
                    </div>

                    <div class="form-group">
                        <label>Ending Order Id</label>
                        <input type="text" class="form-control" id="end_order_id">
                    </div>


                    <div class="form-group">
                        <label>New Order Date</label>
                        <div class="input-group date">
                            <input type="text" class="form-control" id="created_at">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="fix_date_button" class="btn btn-success" data-dismiss="modal">Save Changes</button>
                </div>
            </div>

        </div>
    </div>




@endsection
