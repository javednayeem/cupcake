@extends('layouts.main')

@section('title', 'Restaurant List')

@section('content')

    <script src="/js/superadmin.js"></script>

    <div class="row">

        <div class="animated pulse col-md-4 col-sm-4 col-xs-12 top-margin-20">
            <div class="x_panel">
                <div class="">
                    <h2>All Restaurants</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="top-margin-20">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Restaurants Name</th>
                            <th>Restaurants Code</th>
                            <th>Status</th>
                            <th width="25%">Action</th>
                        </tr>
                        </thead>
                        <tbody id="restaurants_table">
                        @php $restaurant_count = 1; @endphp

                        @foreach($restaurants as $restaurant)
                            <tr id="restaurant_{{ $restaurant->restaurants_id }}">
                                <td scope="row"> {{ $restaurant_count++ }}</td>
                                <td>{{ $restaurant->restaurants_name }}</td>
                                <td>{{ $restaurant->restaurants_code }}</td>
                                <td>{{ $restaurant->status }}</td>
                                <td>
                                    <button type="button" class="btn btn-dark" title="View" onclick="viewRestaurant({{ $restaurant->restaurants_id }})">
                                        <i class="glyphicon glyphicon-eye-open"></i>
                                    </button>

                                    <button type="button" class="btn btn-danger" title="Delete Restaurant" onclick="deleteRestaurant({{$restaurant->restaurants_id}})">
                                        <i class="glyphicon glyphicon-remove"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>




@endsection
