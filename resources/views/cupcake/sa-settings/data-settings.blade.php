@extends('layouts.main')

@section('title', 'Data Settings')

@section('content')

    <script src="/js/superadmin.js"></script>


    <div class="row">

        <div class="col-md-6 col-sm-6 col-xs-12 top-margin-20">
            <div class="x_panel">
                <div class="x_title top-margin-20">
                    <h2>Truncate</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <button type="button" class="btn btn-warning" id="table_truncate_toggle">Check/Uncheck All</button>
                            <button type="button" class="btn btn-danger" id="table_truncate_button">Truncate</button>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">

                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">

                            <thead>
                            <tr class="headings">
                                <th class="column-title">#</th>
                                <th class="column-title"></th>
                                <th class="column-title">Table Name</th>
                            </tr>
                            </thead>

                            <tbody>

                            @php $table_count=1; @endphp

                            @foreach($tables as $table)
                                @if($table->$db_name != 'migrations')
                                    <tr>
                                        <td scope="row"> {{ $table_count++ }}</td>
                                        <td>
                                            <input class="table_truncate" value="{{ $table->$db_name }}" type="checkbox">
                                        </td>
                                        <td>{{ $table->$db_name }}</td>
                                    </tr>
                                @endif

                            @endforeach

                            </tbody>

                        </table>
                    </div>


                </div>

            </div>
        </div>

        <div class="col-md-6 col-sm-6 col-xs-12 top-margin-20">
            <div class="x_panel">
                <div class="x_title top-margin-20">
                    <h2>Drop</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <button type="button" class="btn btn-warning" id="table_drop_toggle">Check/Uncheck All</button>
                            <button type="button" class="btn btn-danger" id="table_drop_button">Drop</button>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">

                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">

                            <thead>
                            <tr class="headings">
                                <th class="column-title">#</th>
                                <th class="column-title"></th>
                                <th class="column-title">Table Name</th>
                            </tr>
                            </thead>

                            <tbody>

                            @php $table_count=1; @endphp

                            @foreach($tables as $table)
                                @if($table->$db_name != 'migrations')
                                    <tr>
                                        <td scope="row"> {{ $table_count++ }}</td>
                                        <td>
                                            <input class="table_drop" value="{{ $table->$db_name }}" type="checkbox">
                                        </td>
                                        <td>{{ $table->$db_name }}</td>
                                    </tr>
                                @endif

                            @endforeach

                            </tbody>

                        </table>
                    </div>


                </div>

            </div>
        </div>

    </div>


    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="">
                    <h2>Users</h2>

                    <div class="clearfix"></div>
                </div>

                <div class="x_content">

                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                            <tr class="headings">
                                <th class="column-title">#</th>
                                <th class="column-title">name</th>
                                <th class="column-title">email</th>
                                <th class="column-title">role</th>
                                <th class="column-title">status</th>
                                <th class="column-title">restaurant_id</th>
                                <th class="column-title">creator_id</th>
                                <th class="column-title">inventory</th>
                                <th class="column-title">license</th>
                            </tr>
                            </thead>

                            <tbody>



                            @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->role }}</td>
                                    <td>{{ $user->status }}</td>
                                    <td>{{ $user->restaurant_id }}</td>
                                    <td>{{ $user->creator_id }}</td>
                                    <td>{{ $user->inventory }}</td>
                                    <td>{{ $user->license }}</td>
                                </tr>
                            @endforeach

                            </tbody>

                        </table>
                    </div>


                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="">
                    <h2>Restaurants</h2>

                    <div class="clearfix"></div>
                </div>

                <div class="x_content">

                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                            <tr class="headings">
                                <th class="column-title">#</th>
                                <th class="column-title">restaurants_name</th>
                                <th class="column-title">restaurants_code</th>
                                <th class="column-title">status</th>
                                <th class="column-title">inventory</th>
                                <th class="column-title">license</th>
                                <th class="column-title">license_id</th>
                            </tr>
                            </thead>

                            <tbody>



                            @foreach($restaurants as $restaurant)
                                <tr>
                                    <td>{{ $restaurant->restaurants_id }}</td>
                                    <td>{{ $restaurant->restaurants_name }}</td>
                                    <td>{{ $restaurant->restaurants_code }}</td>
                                    <td>{{ $restaurant->status }}</td>
                                    <td>{{ $restaurant->inventory }}</td>
                                    <td>{{ $restaurant->license }}</td>
                                    <td>{{ $restaurant->license_id }}</td>

                                </tr>
                            @endforeach

                            </tbody>

                        </table>
                    </div>


                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="">
                    <h2>Licenses</h2>

                    <div class="clearfix"></div>
                </div>

                <div class="x_content">

                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                            <tr class="headings">
                                <th class="column-title">license_id</th>
                                <th class="column-title">restaurants_id</th>
                                <th class="column-title">license_key</th>
                                <th class="column-title">license_start_date</th>
                                <th class="column-title">license_end_date</th>
                                <th class="column-title">status</th>
                                <th class="column-title">license_type</th>
                                <th class="column-title">created_by</th>
                            </tr>
                            </thead>

                            <tbody>



                            @foreach($licenses as $license)
                                <tr>
                                    <td>{{ $license->license_id }}</td>
                                    <td>{{ $license->restaurants_id }}</td>
                                    <td>{{ $license->license_key }}</td>
                                    <td>{{ $license->license_start_date }}</td>
                                    <td>{{ $license->license_end_date }}</td>
                                    <td>{{ $license->status }}</td>
                                    <td>{{ $license->license_type }}</td>
                                    <td>{{ $license->created_by }}</td>

                                </tr>
                            @endforeach

                            </tbody>

                        </table>
                    </div>


                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="">
                    <h2>Menu Items</h2>

                    <div class="clearfix"></div>
                </div>

                <div class="x_content">

                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                            <tr class="headings">
                                <th class="column-title">menu_item_id</th>
                                <th class="column-title">menu_id</th>
                                <th class="column-title">item_name</th>
                                <th class="column-title">price</th>
                                <th class="column-title">item_discount</th>
                                <th class="column-title">status</th>
                                <th class="column-title">item_vat</th>
                            </tr>
                            </thead>

                            <tbody>



                            @foreach($menu_items as $item)
                                <tr>
                                    <td>{{ $item->menu_item_id }}</td>
                                    <td>{{ $item->menu_id }}</td>
                                    <td>{{ $item->item_name }}</td>
                                    <td>{{ $item->price }}</td>
                                    <td>{{ $item->item_discount }}</td>
                                    <td>{{ $item->status }}</td>
                                    <td>{{ $item->item_vat }}</td>
                                </tr>
                            @endforeach

                            </tbody>

                        </table>
                    </div>


                </div>
            </div>
        </div>
    </div>






@endsection
