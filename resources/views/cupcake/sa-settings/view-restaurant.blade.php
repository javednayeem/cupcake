@extends('layouts.main')
@section('title', 'View Restaurant: ' . $restaurant_data->restaurants_name)

@section('content')

    <script src="/js/superadmin.js"></script>

    <input type="hidden" id="restaurants_id" value="{{ $restaurant_data->restaurants_id }}">

    <div class="row">

        <div class="animated pulse col-md-4 col-sm-4 col-xs-12 top-margin-20">
            <div class="x_panel">
                <div class="">
                    <h2>All Restaurants</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="top-margin-20">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Restaurants Name</th>
                            <th>Restaurants Code</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody id="restaurants_table">
                        @php $restaurant_count = 1; @endphp

                        @foreach($restaurants as $restaurant)
                            <tr id="restaurant_{{ $restaurant->restaurants_id }}">
                                <td scope="row"> {{ $restaurant_count++ }}</td>
                                <td>{{ $restaurant->restaurants_name }}</td>
                                <td>{{ $restaurant->restaurants_code }}</td>
                                <td>
                                    <button type="button" class="btn btn-dark" title="View" onclick="viewRestaurant({{ $restaurant->restaurants_id }})">
                                        <i class="glyphicon glyphicon-eye-open"></i>
                                    </button>

                                    <button type="button" class="btn btn-danger" title="Delete Restaurant" onclick="deleteRestaurant({{$restaurant->restaurants_id}})">
                                        <i class="glyphicon glyphicon-remove"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                </div>
            </div>
        </div>

        <div class="animated pulse col-md-8 col-sm-8 col-xs-12 top-margin-20">
            <div class="x_panel">

                <div class="x_content">

                    <div class="col-xs-9">
                        <div class="tab-content">

                            <div class="tab-pane active" id="restaurant_details">
                                <div class="x_content">
                                    <div class="alert alert-info alert-dismissible fade in" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                                        <h3 class="name_title">{{ $restaurant_data->restaurants_name }}</h3>
                                    </div>
                                    <p>Code : {{ $restaurant_data->restaurants_name }}</p>

                                    <ul class="list-inline " style="width: 30%;">
                                        <li><img id="restaurant_img" src="/images/restaurant-logo/{{ $restaurant_data->restaurant_img }}" class="img-circle profile_img"></li>
                                    </ul>

                                    <div class="divider"></div>
                                    <div class="form-horizontal">

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Restaurant Name</label>
                                            <div class="col-md-8 col-sm-8 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left" id="restaurant_name" placeholder="Chef" value="{{ $restaurant_data->restaurants_name }}">
                                                <span class="fa fa-home form-control-feedback left" aria-hidden="true"></span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Restaurant Address</label>
                                            <div class="col-md-8 col-sm-8 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left" id="restaurant_address" placeholder="House#133, Road#4" value="{{ $restaurant_data->address }}">
                                                <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Area</label>
                                            <div class="col-md-8 col-sm-8 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left" id="restaurant_area" placeholder="Dhanmondi" value="{{ $restaurant_data->area }}" >
                                                <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">City</label>
                                            <div class="col-md-8 col-sm-8 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control has-feedback-left" id="restaurant_city" placeholder="Dhaka" value="{{ $restaurant_data->city }}">
                                                <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Restaurant Phone</label>
                                            <div class="col-md-8 col-sm-8 col-xs-12 form-group has-feedback">
                                                <input type="number" class="form-control has-feedback-left" id="restaurant_phone" placeholder="01715839494" value="{{ $restaurant_data->phone_number }}">
                                                <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Restaurant Email</label>
                                            <div class="col-md-8 col-sm-8 col-xs-12 form-group has-feedback">
                                                <input type="email" class="form-control has-feedback-left" id="restaurant_email" value="{{ $restaurant_data->email }}">
                                                <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Restaurant Website</label>
                                            <div class="col-md-8 col-sm-8 col-xs-12 form-group has-feedback">
                                                <input type="email" class="form-control has-feedback-left" id="restaurant_website" value="{{ $restaurant_data->website }}">
                                                <span class="fa fa-globe form-control-feedback left" aria-hidden="true"></span>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Petty Cash</label>
                                            <div class="col-md-8 col-sm-8 col-xs-12 form-group has-feedback">
                                                <input type="email" class="form-control has-feedback-left" id="petty_cash" value="{{ $restaurant_data->petty_cash }}">
                                                <span class="fa fa-money form-control-feedback left" aria-hidden="true"></span>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5 ">
                                                <button class="btn btn-success btn-lg" id="save_restaurant_info_button">Save Restaurant Info</button>
                                            </div>
                                        </div>


                                    </div>
                                </div>

                            </div>


                            <div class="tab-pane" id="restaurant_feature">

                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Restaurant Features</h2>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <br>
                                        <div class="form-horizontal form-label-left">

                                            <div class="form-group">
                                                <label class="col-md-3 col-sm-3 col-xs-12 control-label">Accounts</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <div class="checkbox">
                                                        <label><input type="checkbox" id="feature_accounts" {{ $restaurant_data->accounts==1?'checked':''  }}> Check this, if you need to active accounts feature</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 col-sm-3 col-xs-12 control-label">Inventory</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <div class="checkbox">
                                                        <label><input type="checkbox" id="feature_inventory" {{ $restaurant_data->inventory==1?'checked':''  }}> Check this, if you need to active inventory feature</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 col-sm-3 col-xs-12 control-label">Kitchen Queue</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <div class="checkbox">
                                                        <label><input type="checkbox" id="feature_kitchen_queue" {{ $restaurant_data->kitchen_queue==1?'checked':''  }}> Check this, if you need to active kitchen queue feature</label>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="col-md-3 col-sm-3 col-xs-12 control-label">Online Order</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <div class="checkbox">
                                                        <label><input type="checkbox" id="feature_online_order" {{ $restaurant_data->online_order==1?'checked':''  }}> Check this, if you need to active online order feature</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="ln_solid"></div>
                                            <div class="form-group">
                                                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                                    <button type="button" class="btn btn-primary">Cancel</button>
                                                    <button type="reset" class="btn btn-primary">Reset</button>
                                                    <button type="submit" class="btn btn-success" id="save_features_button">Save Changes</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>


                            <div class="tab-pane" id="restaurant_license">

                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Licenses</h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li>
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#restaurant_license_modal">Create License</button>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>

                                    <div class="x_content">

                                        <div class="table-responsive">
                                            <table class="table table-striped jambo_table bulk_action">
                                                <thead>
                                                <tr class="headings">
                                                    <th class="column-title">#</th>
                                                    <th class="column-title">License Key</th>
                                                    <th class="column-title">License Start Date</th>
                                                    <th class="column-title">License End Date</th>
                                                    <th class="column-title">Duration</th>
                                                    <th class="column-title">Status</th>
                                                    <th class="column-title">License Type</th>
                                                    <th class="column-title">Created By</th>
                                                    <th class="column-title">Created At</th>
                                                    <th class="column-title">Action</th>
                                                </tr>
                                                </thead>

                                                <tbody>

                                                @php
                                                    $license_count=1;
                                                    $today = date("Y-m-d");
                                                @endphp

                                                @foreach($licenses as $license)
                                                    <tr id="license_{{ $license->license_id }}">
                                                        <td scope="row"> {{ $license_count++ }}</td>
                                                        <td>{{ $license->license_key }}</td>
                                                        <td>{{ date('F j, Y', strtotime( $license->license_start_date )) }}</td>
                                                        <td>{{ date('F j, Y', strtotime( $license->license_end_date )) }}</td>
                                                        <td>{{ duration($license->license_start_date, $license->license_end_date) }}</td>
                                                        <td>
                                                            @if($license->status==1)

                                                                @php $reminder = checkLicenseDuration($today, $license->license_end_date); @endphp

                                                                {{  $reminder==0?'License Expired':$reminder.' Remaining' }}
                                                            @else
                                                                <button type="button" class="btn btn-primary" onclick="activateLicense({{ $license->license_id }})">
                                                                    Activate
                                                                </button>
                                                            @endif

                                                        </td>
                                                        <td>{{ ucfirst($license->license_type) }}</td>
                                                        <td>{{ $license->name }}</td>
                                                        <td>{{ date('F j, Y', strtotime( $license->created_at )) }}</td>
                                                        <td>
                                                            <button type="button" class="btn btn-danger" onclick="deleteLicense({{ $license->license_id }})">
                                                                <i class="glyphicon glyphicon-remove"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                @endforeach

                                                </tbody>

                                            </table>
                                        </div>


                                    </div>
                                </div>

                            </div>


                            <div class="tab-pane" id="restaurant_settings">

                                <div class="x_panel">

                                    <div class="x_title">
                                        <h2>Restaurant Settings</h2>
                                        <div class="clearfix"></div>
                                    </div>

                                    @include('cupcake.settings.restaurant-settings')
                                </div>

                            </div>


                            <div class="tab-pane" id="restaurant_messages">Messages Tab.</div>
                        </div>
                    </div>

                    <div class="col-xs-3">
                        <ul class="nav nav-tabs tabs-right">
                            <li class="active"><a href="#restaurant_details" data-toggle="tab" aria-expanded="true">Details</a></li>
                            <li class=""><a href="#restaurant_feature" data-toggle="tab" aria-expanded="false">Restaurant Features</a></li>
                            <li class=""><a href="#restaurant_license" data-toggle="tab" aria-expanded="false">License</a></li>
                            <li class=""><a href="#restaurant_settings" data-toggle="tab" aria-expanded="false">Restaurant Settings</a></li>
                        </ul>
                    </div>

                </div>

            </div>
        </div>

    </div>


    <div class="modal fade" id="restaurant_license_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content" style="margin-top: 200px; ">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Create New License For {{ $restaurant_data->restaurants_name }}</h4>
                </div>
                <div class="modal-body">


                    <fieldset>
                        <div class="control-group">
                            <div class="controls">
                                <div class="col-md-11 xdisplay_inputx form-group has-feedback">
                                    <label>License Start date</label>
                                    <input type="text" class="form-control has-feedback-left" id="single_cal1" placeholder="License Start date" aria-describedby="inputSuccess2Status">
                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                    <span id="inputSuccess2Status" class="sr-only">(success)</span>
                                </div>
                            </div>
                        </div>
                    </fieldset>

                    <div class="form-group" style="margin-left: 10px;">
                        <label>Duration</label>
                        <select class="form-control" id="license_duration">
                            <option value='0'>Select </option>
                            @foreach($license_periods as $period)
                                <option value='{{ $period->days }}'>{{ $period->duration_str }}</option>
                            @endforeach
                        </select>
                    </div>


                    <fieldset>
                        <div class="control-group">
                            <div class="controls">
                                <div class="col-md-11 xdisplay_inputx form-group has-feedback">
                                    <label>License End date</label>
                                    <input type="text" disabled class="form-control has-feedback-left" id="single_cal2" placeholder="License End date" aria-describedby="inputSuccess2Status_2">
                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                    <span id="inputSuccess2Status_2" class="sr-only">(success)</span>
                                </div>
                            </div>
                        </div>
                    </fieldset>


                    <div class="form-group" style="margin-left: 10px;">
                        <label>License Type</label>
                        <select class="form-control" id="license_type">
                            <option value='paid' selected>Paid License</option>
                            <option value='trial'>Trial License</option>
                        </select>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="assign_license_button" class="btn btn-success" data-dismiss="modal">Assign</button>
                </div>
            </div>

        </div>
    </div>



@endsection
