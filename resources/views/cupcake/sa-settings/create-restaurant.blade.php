@extends('layouts.main')

@section('title', 'Create New Restaurant')

@section('content')

    <script src="/js/superadmin.js"></script>

    <div class="row">
        <div class="col-md-12 top-margin-20">
            <div class="x_panel">

                <div class="x_content">
                    <br />
                    <div id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Restaurant Name</label>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="text" class="form-control has-feedback-left" id="restaurant_name" placeholder="Chef">
                                <span class="fa fa-home form-control-feedback left" aria-hidden="true"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Restaurant Address</label>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="text" class="form-control has-feedback-left" id="restaurant_address" placeholder="House#133, Road#4" >
                                <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Area</label>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="text" class="form-control has-feedback-left" id="restaurant_area" placeholder="Dhanmondi" >
                                <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">City</label>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="text" class="form-control has-feedback-left" id="restaurant_city" placeholder="Dhaka">
                                <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Restaurant Phone</label>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="number" class="form-control has-feedback-left" id="restaurant_phone" placeholder="01715839494">
                                <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Restaurant Email</label>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="email" class="form-control has-feedback-left" id="restaurant_email">
                                <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Restaurant Website</label>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="email" class="form-control has-feedback-left" id="restaurant_website">
                                <span class="fa fa-globe form-control-feedback left" aria-hidden="true"></span>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5 ">
                                <button class="btn btn-success btn-lg" id="create_restaurant_button">Create Restaurant</button>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
