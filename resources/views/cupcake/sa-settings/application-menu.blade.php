@extends('layouts.main')

@section('title', 'Application Menu')

@section('content')

    <script src="/js/superadmin.js"></script>


    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12 top-margin-30">
            <div class="x_panel">

                <div class="x_title top-margin-10">
                    {{--<h2>Application Menu</h2>--}}
                    <ul class="nav navbar-right panel_toolbox">
                        <li><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_application_menu_modal">Add Application Menu</button></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">

                    <div class="table-responsive top-margin-20">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                            <tr class="headings">
                                <th>#</th>
                                <th>Menu Name</th>
                                <th>Parent</th>
                                <th>Parent Count</th>
                                <th width="15%">Action</th>
                            </tr>
                            </thead>

                            <tbody id="order_type_table">

                            @php $count=1; @endphp


                            @foreach($application_menu as $menu)
                                <tr class="even pointer" id="menu_{{ $menu->menu_id }}">
                                    <td>{{ $count++ }}</td>
                                    <td>{{ $menu->menu_name }}</td>
                                    <td>{{ $menu->parent_menu_name }}</td>
                                    <td>{{ $menu->parent_count }}</td>

                                    <td>
                                        <button type="button" class="btn btn-dark" onclick="editMenu('{{ $menu->menu_id }}', '{{ $menu->menu_name }}', '{{ $menu->parent_menu }}')">
                                            <i class="glyphicon glyphicon-pencil"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger" onclick="deleteMenu({{ $menu->menu_id }})">
                                            <i class="glyphicon glyphicon-remove"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach



                            </tbody>
                        </table>
                    </div>


                </div>
            </div>
        </div>

    </div>


    <div class="modal fade" id="add_application_menu_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Add Application Menu</h4>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label class="control-label col-md-3">Parent Menu <span class="red-text">*</span></label>

                        <select class="form-control" id="parent_menu">
                            <option value="0">No Parent</option>
                            @foreach($application_menu as $menu)
                                <option value="{{ $menu->menu_id }}">{{ $menu->menu_name }}</option>
                            @endforeach
                        </select>

                    </div>


                    <div class="form-group">
                        <label for="menu_name">Menu Name</label>
                        <input type="text" class="form-control" id="menu_name">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="add_app_menu_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                </div>
            </div>

        </div>
    </div>


    <div class="modal fade" id="edit_application_menu_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Edit Application Menu</h4>
                </div>
                <div class="modal-body">

                    <input type="hidden" id="menu_id" value="0">

                    <div class="form-group">
                        <label class="control-label col-md-3">Parent Menu <span class="red-text">*</span></label>

                        <select class="form-control" id="edit_parent_menu">
                            <option value="0">No Parent</option>
                            @foreach($application_menu as $menu)
                                <option value="{{ $menu->menu_id }}">{{ $menu->menu_name }}</option>
                            @endforeach
                        </select>

                    </div>


                    <div class="form-group">
                        <label for="menu_name">Menu Name</label>
                        <input type="text" class="form-control" id="edit_menu_name">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="edit_app_menu_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                </div>
            </div>

        </div>
    </div>


@endsection
