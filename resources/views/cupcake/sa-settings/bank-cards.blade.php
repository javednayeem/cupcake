@extends('layouts.main')

@section('title', 'Bank Cards')

@section('content')

    <script src="/js/superadmin.js"></script>


    <div class="row">

        <div class="col-md-6 top-margin-20">
            <div class="x_panel">

                <div class="x_content">
                    <div class="form-horizontal">

                        <div class="x_title top-margin-10">
                            <h2>Bank Card Lists</h2>

                            <ul class="nav navbar-right panel_toolbox">
                                <li><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_card_modal">Add Bank Card</button></li>
                            </ul>

                            <div class="clearfix"></div>
                        </div>

                        <div class="top-margin-20">
                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Card Name</th>
                                    <th>Created At</th>
                                    <th width="15%">Action</th>
                                </tr>
                                </thead>
                                <tbody id="card_table">

                                @php $card_count=1; @endphp

                                @foreach($bank_cards as $bank_card)
                                    <tr id="bank_card_{{ $bank_card->card_id }}">
                                        <th scope="row"> {{ $card_count++ }}</th>
                                        <td>{{ $bank_card->card_name }}</td>
                                        <td>{{ date('F j, Y', strtotime( $bank_card->created_at )) }}</td>
                                        <td>
                                            <button type="button" class="btn btn-dark" title="Edit Card" onclick="editCard('{{ $bank_card->card_id }}', '{{ $bank_card->card_name }}')">
                                                <i class="glyphicon glyphicon-pencil"></i>
                                            </button>

                                            <button type="button" class="btn btn-danger" title="Delete Card" onclick="deleteCard('{{ $bank_card->card_id }}')">
                                                <i class="glyphicon glyphicon-remove"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>

                        </div>


                    </div>
                </div>
            </div>
        </div>


        <div class="col-md-6 top-margin-20">
            <div class="x_panel">

                <div class="x_content">
                    <div class="form-horizontal">

                        <div class="x_title top-margin-10">
                            <h2>Payment Methods</h2>

                            <ul class="nav navbar-right panel_toolbox">
                                <li><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_method_modal">Add New Payment Method</button></li>
                            </ul>

                            <div class="clearfix"></div>
                        </div>

                        <div class="top-margin-20">
                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Method Name</th>
                                    <th>Created At</th>
                                    <th width="15%">Action</th>
                                </tr>
                                </thead>
                                <tbody id="payment_methods_table">

                                @php $count=1; @endphp

                                @foreach($payment_methods as $method)
                                    <tr id="method_{{ $method->method_id }}">
                                        <th> {{ $count++ }}</th>
                                        <td>{{ ucfirst($method->method_name) }}</td>
                                        <td>{{ date('F j, Y', strtotime( $method->created_at )) }}</td>
                                        <td>
                                            <button type="button" class="btn btn-dark" title="Edit Card" onclick="editPaymentMethod('{{ $method->method_id }}', '{{ $method->method_name }}')">
                                                <i class="glyphicon glyphicon-pencil"></i>
                                            </button>

                                            <button type="button" class="btn btn-danger" title="Delete Card" onclick="deletePaymentMethod('{{ $method->method_id }}')">
                                                <i class="glyphicon glyphicon-remove"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>

                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="add_card_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Add Bank Card</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="card_name">Card Name</label>
                        <input type="text" class="form-control" id="card_name">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="add_card_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                </div>
            </div>

        </div>
    </div>


    <div class="modal fade" id="edit_card_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Edit Bank Card</h4>
                </div>
                <div class="modal-body">

                    <input type="hidden" id="card_id" value="0">

                    <div class="form-group">
                        <label for="card_name">Card Name</label>
                        <input type="text" class="form-control" id="edit_card_name">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="edit_card_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                </div>
            </div>

        </div>
    </div>


    <div class="modal fade" id="add_method_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Add New Payment Method</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="method_name">Method Name</label>
                        <input type="text" class="form-control" id="method_name">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="add_payment_method_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                </div>
            </div>

        </div>
    </div>


    <div class="modal fade" id="edit_method_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Edit Payment Method</h4>
                </div>
                <div class="modal-body">

                    <input type="hidden" id="method_id" value="0">

                    <div class="form-group">
                        <label for="method_name">Method Name</label>
                        <input type="text" class="form-control" id="edit_method_name">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="edit_payment_method_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                </div>
            </div>

        </div>
    </div>



@endsection
