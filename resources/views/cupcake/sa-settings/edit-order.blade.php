@extends('layouts.main')

@section('title', '')

@section('content')

    <script src="/js/superadmin.js"></script>

    <input type="hidden" id="order_id" value="{{ $order->order_id }}">
    <input type="hidden" id="total_bill" value="{{ $order->total_bill }}">

    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">

                <div class="x_content">

                    <section class="content invoice">
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <h1><i class="fa fa-globe"></i> Invoice #{{ $order->order_id }}</h1>
                                <h2><i class="fa fa-globe"></i> Date: {{ date('F j, Y h:i a', strtotime( $order->created_at )) }}</h2>
                                <h2><i class="fa fa-university"></i> Table #{{ $order->table_name }}</h2>
                                <h2><i class="fa fa-university"></i> Waiter #{{ $order->waiter_name }}</h2>
                                <h2><i class="fa fa-university"></i> Customer #{{ $order->customer_name }}</h2>
                                <h2><i class="fa fa-university"></i> Order Type #{{ $order->order_type }}</h2>
                                <h2><i class="fa fa-university"></i> Order Status #{{ $order->order_status }}</h2>
                                {{--<h2><i class="fa fa-university"></i> Payment Method #{{ $order->payment_method }}</h2>--}}
                                <h2><i class="fa fa-university"></i> Bill &#2547;{{ $order->total_bill }}</h2>
                                <br>

                                <button type="button" class="btn btn-lg btn-warning" id="recalculate_order_button">
                                    <i class="fa fa-recycle"></i> Re-Calculate Bill
                                </button>

                                <button type="button" class="btn btn-lg btn-danger" id="delete_order_button">
                                    <i class="fa fa-trash-o"></i> Delete This Order
                                </button>
                            </div>
                        </div>


                        <div class="row">

                            <div class="col-xl-12 col-md-12 col-xs-12 top-margin-20">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Order Information</h2>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <br>
                                        <div class="form-horizontal form-label-left">

                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Waiter</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <select class="form-control" id="waiter_id">

                                                        @foreach($user_data as $user)
                                                            <option value="{{ $user->id }}"
                                                                    {{ $order->waiter_id == $user->id? 'selected': '' }} >{{ $user->name }}</option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Customer</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <select class="form-control" id="customer_id">

                                                        @foreach($customer_data as $customer)
                                                            <option value="{{ $customer->customer_id }}"
                                                                    {{ $order->customer_id == $customer->customer_id? 'selected': '' }}>{{ $customer->customer_phone . ' - '. $customer->customer_name }}</option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Table</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <select class="form-control" id="table_id">

                                                        @foreach($table_data as $table)
                                                            <option value="{{ $table->table_id }}"
                                                                    {{ $order->table_id == $table->table_id? 'selected': '' }}>Table #{{ $table->table_name }}</option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Order Type</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <select class="form-control" id="order_type">
                                                        @foreach($order_types as $type)
                                                            <option value="{{ $type->type_name }}" {{ $order->order_type == $type->type_name?'selected':'' }}>
                                                                {{ ucfirst($type->type_name) }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Order Status</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <select class="form-control" id="order_status">
                                                        <option value="placed" {{ $order->order_status == 'placed'?'selected':'' }}>Placed</option>
                                                        <option value="paid" {{ $order->order_status == 'paid'? 'selected': '' }}>Paid</option>
                                                        <option value="accepted_void" {{ $order->order_status == 'accepted_void'? 'selected': '' }}>Accepted Void</option>
                                                    </select>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Discount Amount Type</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <select class="form-control" id="discount_amount_type">
                                                        <option value="cash" {{ $order->discount_amount_type == 'cash'?'selected':'' }}>Cash</option>
                                                        <option value="percentage" {{ $order->discount_amount_type == 'percentage'? 'selected': '' }}>Percentage</option>
                                                    </select>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Discount</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <input class="form-control" id="discount" value="{{ $order->discount }}">
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Discount Percentage</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <input class="form-control" id="discount_percentage" value="{{ $order->discount_percentage }}">
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Discount Reference</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <textarea class="form-control" id="discount_reference">{{ $order->discount_reference }}</textarea>
                                                </div>
                                            </div>


                                            {{--<div class="form-group">--}}
                                            {{--<label class="control-label col-md-3 col-sm-3 col-xs-12">Payment Method</label>--}}
                                            {{--<div class="col-md-9 col-sm-9 col-xs-12">--}}
                                            {{--<select class="form-control" id="payment_method">--}}
                                            {{--<option value="cash">Cash</option>--}}
                                            {{--<option value="card">Card</option>--}}
                                            {{--<option value="cash_card">Cash & Card</option>--}}
                                            {{--</select>--}}
                                            {{--</div>--}}
                                            {{--</div>--}}


                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Order Date</label>
                                                <div class="col-md-2 col-sm-2 col-xs-12 input-group date" id="myDatepicker2">
                                                    <input type="text" class="form-control" id="created_at" value="{{ date('Y-m-d', strtotime( $order->created_at )) }}" style="margin-left: 10px">
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Order Time</label>
                                                <div class="col-md-2 col-sm-2 col-xs-12">
                                                    <input type="text" class="form-control" id="created_time" value="{{ date('H:i:s', strtotime( $order->created_at )) }}">
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="col-md-3 col-sm-3 col-xs-12 control-label">Re-Calculate Bill

                                                </label>

                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" id="recalculate_bill"> Tick, if you need to recalculate the billing calculation
                                                        </label>
                                                    </div>


                                                </div>
                                            </div>





                                            <div class="ln_solid"></div>
                                            <div class="form-group">
                                                <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                                                    <button type="button" id="edit_order_button" class="btn btn-success">Save Changes</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>


                        <div class="row">

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Order Items</h2>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">

                                        <table class="table table-striped table-hover">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Item Name</th>
                                                <th>Qty</th>
                                                <th>Unit Price</th>
                                                <th>Subtotal</th>
                                                <th>Item Status</th>
                                                <th width="13%">Action</th>
                                            </tr>
                                            </thead>

                                            <tbody>

                                            @php $item_count=1; @endphp

                                            @foreach($order_details as $item)

                                                @php $unit_price = intval($item->item_price) - intval($item->item_discount) ;@endphp

                                                <tr>
                                                    <td>{{ $item_count++ }}</td>
                                                    <td>{{ $item->menu_item_name }}</td>
                                                    <td>{{ $item->item_quantity }}</td>
                                                    <td>{{ $unit_price }}</td>
                                                    <td>{{ $unit_price * $item->item_quantity }}</td>
                                                    <td>{{ $item->item_status==1? 'Active':'Inactive' }}</td>
                                                    <td>
                                                        <button type="button" class="btn btn-dark">
                                                            <i class="glyphicon glyphicon-pencil"></i>
                                                        </button>

                                                        <button type="button" class="btn btn-danger">
                                                            <i class="glyphicon glyphicon-remove"></i>
                                                        </button>
                                                    </td>
                                                </tr>

                                            @endforeach

                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>


                        </div>


                        <div class="row">

                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Modifiers</h2>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">

                                        <table class="table table-striped table-hover">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Modifier Name</th>
                                                <th>Qty</th>
                                                <th>Unit Price</th>
                                                <th>Subtotal</th>
                                                <th width="24%">Action</th>
                                            </tr>
                                            </thead>

                                            <tbody>

                                            @php $modifier_count=1; @endphp

                                            @foreach($order_modifiers as $modifier)
                                                <tr>
                                                    <th scope="row">{{ $modifier_count++ }}</th>
                                                    <td>{{ $modifier->modifier_name }}</td>
                                                    <td>{{ $modifier->quantity }}</td>
                                                    <td>{{ $modifier->price }}</td>
                                                    <td>{{ $modifier->quantity * $modifier->price }}</td>
                                                    <td>
                                                        <button type="button" class="btn btn-dark">
                                                            <i class="glyphicon glyphicon-pencil"></i>
                                                        </button>

                                                        <button type="button" class="btn btn-danger">
                                                            <i class="glyphicon glyphicon-remove"></i>
                                                        </button>
                                                    </td>

                                                </tr>
                                            @endforeach



                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>


                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Order Instructions</h2>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">

                                        <table class="table table-striped table-hover">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Instruction</th>
                                                <th width="25%">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @php
                                                $instruction_count=1;
                                                $instructions = explode("|", $order->order_notes);
                                            @endphp

                                            @foreach($instructions as $instruction)

                                                @php $note = explode("_", $instruction); @endphp

                                                <tr>
                                                    <th scope="row">{{ $instruction_count++ }}</th>
                                                    <td>{{ $note[0] }}</td>
                                                    <td>
                                                        <button type="button" class="btn btn-dark">
                                                            <i class="glyphicon glyphicon-pencil"></i>
                                                        </button>

                                                        <button type="button" class="btn btn-danger">
                                                            <i class="glyphicon glyphicon-remove"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                            @endforeach


                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>

                        </div>


                        <div class="row">

                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="x_panel">

                                    <div class="x_title">
                                        <div class="">
                                            <h2>Payment Methods</h2>
                                            <ul class="nav navbar-right">
                                                <li>
                                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#payment_method_modal">
                                                        <i class="fa fa-plus"></i> Add Payment Methods
                                                    </button>
                                                </li>
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>

                                    <div class="x_content">

                                        <table class="table table-striped table-hover">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Method</th>
                                                <th>Amount</th>
                                                <th>Bank</th>
                                                <th>Card No.</th>
                                                <th width="24%">Action</th>
                                            </tr>
                                            </thead>

                                            <tbody>

                                            @php $payment_count=1; @endphp

                                            @foreach($order_payments as $payment)

                                                <tr>
                                                    <td>{{ $payment_count++ }}</td>
                                                    <td>{{ $payment->payment_method }}</td>
                                                    <td>{{ $payment->amount }}</td>
                                                    <td>{{ $payment->card_name }}</td>
                                                    <td>{{ $payment->card_number }}</td>

                                                    <td>
                                                        <button type="button" class="btn btn-dark">
                                                            <i class="glyphicon glyphicon-pencil"></i>
                                                        </button>

                                                        <button type="button" class="btn btn-danger">
                                                            <i class="glyphicon glyphicon-remove"></i>
                                                        </button>
                                                    </td>
                                                </tr>

                                            @endforeach

                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>


                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Billing Details</h2>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">

                                        <table class="table">
                                            <tbody>
                                            <tr>
                                                <th style="width:50%">Subtotal:</th>
                                                <th>&#2547; {{ $order->bill }}</th>
                                            </tr>

                                            @if($restaurant_data->vat_percentage > 0)
                                                <tr>
                                                    <th>VAT ({{ $restaurant_data->vat_percentage }}%)</th>
                                                    <th>&#2547; {{ $order->vat }}</th>
                                                </tr>
                                            @endif

                                            @if($restaurant_data->service_charge > 0)
                                                <tr>
                                                    <th>Service Charge ({{ $restaurant_data->service_charge }}%):</th>
                                                    <th>&#2547; {{ $order->service_charge }}</th>
                                                </tr>
                                            @endif

                                            @if($restaurant_data->sd_percentage > 0)
                                                <tr>
                                                    <th>SD ({{ $restaurant_data->sd_percentage }}%):</th>
                                                    <th>&#2547; {{ $order->sd_charge }}</th>
                                                </tr>
                                            @endif

                                            @if($restaurant_data->service_charge_vat_percentage > 0)
                                                <tr>
                                                    <th>SD ({{ $restaurant_data->service_charge_vat_percentage }}%):</th>
                                                    <th>&#2547; {{ $order->service_charge_vat }}</th>
                                                </tr>
                                            @endif

                                            @if($order->discount > 0)
                                                <tr>
                                                    <th>Discount
                                                        ({{ $order->discount_amount_type=='cash'? $order->discount.'Tk':$order->discount_percentage.'%'  }}):
                                                    </th>
                                                    <th>-&#2547; {{ $order->discount }}</th>
                                                </tr>
                                            @endif

                                            <tr>
                                                <th>Total:</th>
                                                <th>&#2547; {{ $order->total_bill }}</th>
                                            </tr>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>

                        </div>


                        <div class="row no-print">
                            <div class="col-xs-12">
                                <button class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
                                <button class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment</button>
                                <button class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>



    <div class="modal fade" id="payment_method_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Add Payment</h4>
                </div>

                <div class="modal-body">

                    <div class="row">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="instruction_id">Select Payment Method</label>
                                <select class="form-control radius-10" id="modal_payment_method">
                                    @foreach($payment_methods as $method)
                                        <option value="{{ $method->method_name }}">{{ ucfirst($method->method_name) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                    </div>


                    <div class="row">

                        <div class="div-hide" id="modal_bank_div">

                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <div class="form-group">
                                    <label for="modal_bank_card">Select Bank</label>
                                    <select id="modal_bank_card" class="form-control">
                                        @foreach($bank_cards as $card)
                                            <option value="{{ $card->card_id }}">{{ $card->card_name }}</option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>

                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <div class="form-group">
                                    <label for="modal_bank_card">Card No.</label>
                                    <input class="form-control" id="modal_card_number" placeholder="6789">
                                </div>
                            </div>

                        </div>


                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="form-group">
                                <label for="modal_bank_card">Amount</label>
                                <input type="number" min="0" class="form-control" id="modal_amount" placeholder="5500">
                            </div>
                        </div>

                    </div>


                    <div class="row">

                        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 24px;">
                            <div class="form-group text-center">
                                <button type="button" onclick="addPayment()" class="btn btn-success">Add Payment</button>
                            </div>
                        </div>

                    </div>



                    <div class="row top-margin-40">
                        <div class="x_content">
                            <div class="">
                                <table class="table table-striped jambo_table">
                                    <thead>
                                    <tr>
                                        <th>Type</th>
                                        <th>Bank</th>
                                        <th>Card No.</th>
                                        <th class="text-right">Amount</th>
                                        <th width="10%">Action</th>
                                    </tr>
                                    </thead>

                                    <tbody id="payment_table">

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td class="text-right">
                                            Total: <b id="payment_total_amount">0</b>
                                        </td>
                                        <td></td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-success" data-dismiss="modal" id="edit_order_payment">OK</button>
                </div>
            </div>

        </div>
    </div>




@endsection
