@extends('layouts.main')

@section('title', 'Create New User')

@section('content')

    <script src="/js/superadmin.js"></script>

    <div class="row">
        <div class="col-md-12 top-margin-20">
            <div class="x_panel">

                <div class="x_content">
                    <div class="form-horizontal form-label-left top-margin-20">

                        <div class="form-group">
                            <label class="control-label col-md-3">Name <span class="red-text">*</span>
                            </label>
                            <div class="col-md-6">
                                <input type="text" id="new_user_name" required="required" class="form-control" placeholder="John Snow">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Restaurant <span class="red-text">*</span></label>
                            <div class="col-md-6">
                                <select class="form-control" id="select_restaurant">
                                    @foreach($restaurant_data as $restaurant)
                                        <option value="{{ $restaurant->restaurants_id }}">{{ $restaurant->restaurants_code }}</option>
                                    @endforeach

                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Role <span class="red-text">*</span></label>
                            <div class="col-md-6">
                                <select class="form-control" id="select_user_role">
                                    <option value="0">Choose option</option>

                                    @foreach($role_lookup as $role)
                                        <option value="{{ $role->role_name }}">
                                            {{ ucfirst($role->role_name) }}
                                        </option>
                                    @endforeach

                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3">Username <span class="red-text">*</span></label>
                            <div class="col-md-3">
                                <input type="text" id="user_email" class="form-control">
                            </div>
                            {{--<div class="col-md-3">--}}
                                {{--<label class="control-label col-md-3">&#64;<span id='span_rstaurantCode'>{{ getRstaurantCode() }}</span>.com</label>--}}
                                {{--<input type="hidden" value="&#64;{{ getRstaurantCode() }}.com" id="rstaurantCode">--}}
                            {{--</div>--}}
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Password</label>
                            <div class="col-md-3">
                                <input type="text" id="user_password" class="form-control">
                            </div>
                            <div class="col-md-3">
                                <button class="btn btn-success" onclick="generatePassword()">Generate Password</button>
                            </div>
                        </div>

                        <div class="form-group top-margin-50">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5">
                                <button class="btn btn-success" id="add_user_button">Add User</button>
                            </div>
                        </div>



                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 top-margin-20">
            <div class="x_panel">
                <div class="">
                    <h2>Users</h2>
                    <div class="clearfix"></div>
                </div>

                <div class="top-margin-20">

                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                        <tr class="headings">
                            <th class="column-title">#</th>
                            <th class="column-title">Name</th>
                            <th class="column-title">Username</th>
                            <th class="column-title">Restaurant</th>
                            <th class="column-title">Role</th>
                            <th class="column-title">Joined</th>
                            <th class="column-title" width="11%">Action</th>
                        </tr>
                        </thead>

                        <tbody>

                        @php $user_count=1; @endphp

                        @foreach($users as $user)
                            <tr class="even pointer" id="user_{{ $user->id }}">
                                <td>{{ $user_count++ }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>
                                    <select class="form-control" id="restaurant_{{ $user->id }}" onchange="changeRestaurant({{ $user->id }})">
                                        @foreach($restaurant_data as $restaurant)
                                            <option value="{{$restaurant->restaurants_id }}" {{ $restaurant->restaurants_id==$user->restaurant_id?'selected':'' }}>{{ $restaurant->restaurants_code }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control user-role" {{ $user->email=='superuser@cupcake.com'?'disabled':'' }}>
                                        @foreach($role_lookup as $role)
                                            <option value="{{$role->role_name.'-'.$user->id}}" {{ $role->role_name==$user->role?'selected':'' }}>{{ ucfirst($role->role_name) }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>{{ date('F j, Y', strtotime( $user->created_at )) }}</td>
                                <td>
                                    <button type="button" class="btn btn-dark" title="Edit User" onclick="editUser('{{ $user->id }}', '{{ $user->name }}', '{{ $user->role }}', '{{ $user->phone }}')"
                                            {{ $user->email=='superuser@cupcake.com'?'disabled':'' }}>
                                        <i class="glyphicon glyphicon-pencil"></i>
                                    </button>

                                    <button type="button" class="btn btn-danger" title="Delete User" onclick="deleteUser({{ $user->id }})" {{ $user->email=='superuser@cupcake.com'?'disabled':'' }}>
                                        <i class="glyphicon glyphicon-remove"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach


                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="edit_user_modal">
        <div class="modal-dialog">
            <div class="modal-content" style="margin-top: 200px; ">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title" id="edit_table_title">Edit User</h4>
                    <input type="hidden" id="edit_user_id" value="0">
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label>User Name</label>
                        <input type="text" class="form-control" id="modal_user_name">
                    </div>

                    <div class="form-group">
                        <label>Role</label>
                        <select class="form-control" id="modal_user_role">
                            <option value="0">Choose option</option>
                            @foreach($role_lookup as $role)
                                <option value="{{ $role->role_name }}">{{ ucfirst($role->role_name) }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Phone Number</label>
                        <input type="text" class="form-control" id="modal_user_phone">
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="edit_user_button" class="btn btn-success" data-dismiss="modal">Done</button>
                </div>
            </div>

        </div>
    </div>



@endsection
