@extends('layouts.main')

@section('title', 'Application Lookup')

@section('content')

    <script src="/js/superadmin.js"></script>


    <div class="row">

        <div class="col-md-6 col-sm-6 col-xs-6 top-margin-30">
            <div class="x_panel">

                <div class="x_title top-margin-10">
                    <h2>Order Types</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_order_type_modal">Add Order Type</button></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">

                    <div class="table-responsive top-margin-20">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                            <tr class="headings">
                                <th>#</th>
                                <th>Type Name</th>
                                <th width="15%">Action</th>
                            </tr>
                            </thead>

                            <tbody id="order_type_table">

                            @php $count=1; @endphp

                            @foreach($order_types as $type)

                                <tr id="type_{{ $type->type_id }}">
                                    <td> {{ $count++ }}</td>
                                    <td id="type_name_{{ $type->type_id }}">{{ $type->type_name }}</td>
                                    <td>
                                        <button type="button" class="btn btn-dark" onclick="editOrderType('{{ $type->type_id }}', '{{ $type->type_name }}')">
                                            <i class="glyphicon glyphicon-pencil"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger" onclick="deleteOrderType({{ $type->type_id }})">
                                            <i class="glyphicon glyphicon-remove"></i>
                                        </button>
                                    </td>
                                </tr>

                            @endforeach

                            </tbody>
                        </table>
                    </div>


                </div>
            </div>
        </div>

    </div>


    <div class="row">
        <div class="col-md-12 top-margin-30">
            <div class="x_panel">

                <div class="x_content">

                    <div class="form-horizontal">

                        <div class="animated pulse col-md-6 col-sm-6 col-xs-12">
                            <div class="">
                                <div class="x_title">
                                    <h2>Add New Lookup</h2>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Lookup Name</label>
                                        <div class="col-md-4">
                                            <input type="text" id="lookup_name" class="form-control">
                                        </div>
                                        <div class="col-md-3">
                                            <button class="btn btn-success" id="add_lookup_button">Add Lookup</button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="animated pulse col-md-6 col-sm-6 col-xs-12">
                            <div class="">
                                <div class="x_title">
                                    <h2>Lookup List</h2>
                                    <div class="clearfix"></div>
                                </div>



                                <div class="x_content">
                                    <table class="table table-striped jambo_table bulk_action">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>LookUp Name</th>
                                            <th>Created At</th>
                                            <th width="25%">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody id="lookup_table">



                                        @foreach($lookups as $lookup)
                                            <tr id="lookup_{{ $lookup->lookup_id }}">
                                                <td scope="row"> {{ $lookup->lookup_id }}</td>
                                                <td>{{ $lookup->lookup_name }}</td>
                                                <td>{{ date('F j, Y', strtotime( $lookup->created_at )) }}</td>
                                                <td>
                                                    <button type="button" class="btn btn-dark">
                                                        <i class="glyphicon glyphicon-pencil"></i>
                                                    </button>

                                                    <button type="button" class="btn btn-danger">
                                                        <i class="glyphicon glyphicon-remove"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12 top-margin-30">
            <div class="x_panel">

                <div class="x_content">

                    <div class="form-horizontal">

                        <div class="animated pulse col-md-6 col-sm-6 col-xs-12">
                            <div class="">
                                <div class="x_title">
                                    <h2>Add New Role</h2>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Role Name</label>
                                        <div class="col-md-4">
                                            <input type="text" id="lookup_name" class="form-control">
                                        </div>
                                        <div class="col-md-3">
                                            <button class="btn btn-success" id="add_lookup_button">Add Role Lookup</button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="animated pulse col-md-6 col-sm-6 col-xs-12">
                            <div class="">
                                <div class="x_title">
                                    <h2>Role Lookup List</h2>
                                    <div class="clearfix"></div>
                                </div>



                                <div class="x_content">
                                    <table class="table table-striped jambo_table bulk_action">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Role Name</th>
                                            <th>Created At</th>
                                            <th width="25%">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody id="role_lookup_table">

                                        @foreach($role_lookup as $role)
                                            <tr id="role_{{ $role->role_id }}">
                                                <td scope="row"> {{ $role->role_id }}</td>
                                                <td>{{ $role->role_name }}</td>
                                                <td>{{ date('F j, Y', strtotime( $role->created_at )) }}</td>
                                                <td>
                                                    <button type="button" class="btn btn-dark">
                                                        <i class="glyphicon glyphicon-pencil"></i>
                                                    </button>

                                                    <button type="button" class="btn btn-danger">
                                                        <i class="glyphicon glyphicon-remove"></i>
                                                    </button>
                                                </td>
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12 top-margin-30">
            <div class="x_panel">

                <div class="x_content">

                    <div class="form-horizontal">

                        <div class="animated pulse col-md-6 col-sm-6 col-xs-12">
                            <div class="">
                                <div class="x_title">
                                    <h2>Add New Report Type</h2>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">Report Type</label>
                                    <div class="col-md-9 col-sm-9 col-xs-9 form-group has-feedback">
                                        <select class="form-control" id="report_type">
                                            <option value="sales">Sales</option>
                                            <option value="void">Void</option>
                                            <option value="inventory">Inventory</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">Report Name</label>
                                    <div class="col-md-9 col-sm-9 col-xs-9 form-group has-feedback">
                                        <input type="text" class="form-control" id="report_name">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">Report Value</label>
                                    <div class="col-md-9 col-sm-9 col-xs-9 form-group has-feedback">
                                        <input type="text" class="form-control" id="report_value">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5 ">
                                        <button class="btn btn-success" id="add_reportType_button">Add Report Type</button>
                                    </div>
                                </div>


                            </div>
                        </div>

                        <div class="animated pulse col-md-6 col-sm-6 col-xs-12">
                            <div class="">
                                <div class="x_title">
                                    <h2>Report Type Lookup</h2>
                                    <div class="clearfix"></div>
                                </div>



                                <div class="x_content">
                                    <table class="table table-striped jambo_table bulk_action">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Report Type</th>
                                            <th>Name</th>
                                            <th>Value</th>
                                            <th width="25%">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody id="role_lookup_table">

                                        @foreach($report_type as $type)
                                            <tr id="reportType_{{ $type->reportType_id }}">
                                                <td scope="row"> {{ $type->reportType_id }}</td>
                                                <td>{{ $type->report_type }}</td>
                                                <td>{{ $type->report_name }}</td>
                                                <td>{{ $type->report_value }}</td>

                                                <td>
                                                    <button type="button" class="btn btn-dark">
                                                        <i class="glyphicon glyphicon-pencil"></i>
                                                    </button>

                                                    <button type="button" class="btn btn-danger">
                                                        <i class="glyphicon glyphicon-remove"></i>
                                                    </button>
                                                </td>
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="modal fade" id="add_order_type_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Add Order Type</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="type_name">Type Name</label>
                        <input type="text" class="form-control" id="type_name">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="add_order_type_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                </div>
            </div>

        </div>
    </div>


    <div class="modal fade" id="edit_order_type_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title">Add Order Type</h4>
                </div>
                <div class="modal-body">

                    <input type="hidden" id="edit_type_id" value="0">

                    <div class="form-group">
                        <label for="type_name">Type Name</label>
                        <input type="text" class="form-control" id="edit_type_name">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="edit_order_type_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                </div>
            </div>

        </div>
    </div>



@endsection
