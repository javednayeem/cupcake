@extends('layouts.main')

@section('title', 'Scheduled Scripts')

@section('content')

    <script src="/js/schedule-scripts.js"></script>


    <div class="row">
        <div class="col-md-12 top-margin-20">
            <div class="x_panel">

                <div class="top-margin-20">
                    <ul class="nav navbar-right panel_toolbox">
                        <li><button type="button" class="btn btn-dark" id="demo_data_button">Load Demo Data</button></li>
                        <li><button type="button" class="btn btn-info" id="fix_order_button">Fix Order Data</button></li>
                        <li><button type="button" class="btn btn-danger" id="db_fix_scripts_button">DB Fixing Script</button></li>
                        <li><button type="button" class="btn btn-primary" id="run_scripts_button">Run Scripts</button></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>


                <div class="x_content">
                    <div data-parsley-validate class="form-horizontal">

                        <div class="top-margin-20">
                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                <tr>
                                    <th>Job Id</th>
                                    <th>Script Name</th>
                                    <th>Scheduled</th>
                                    <th>Executed At</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($schedule_scripts as $script)
                                    <tr>
                                        <td>{{ $script->job_id }}</td>
                                        <td>{{ $script->script_name }}</td>
                                        <td>{{ $script->schedule_date != null ? date('F j, Y', strtotime( $script->schedule_date )):"" }}</td>
                                        <td>{{ date('F j, Y h:i:s A', strtotime( $script->created_at )) }}</td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>

                            {{ $schedule_scripts->links() }}

                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
