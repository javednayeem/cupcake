@extends('layouts.main')

@section('title', 'App Settings')

@section('content')

    <script src="/js/superadmin.js"></script>

    <div class="row">
        <div class="col-md-12 top-margin-20">
            <div class="x_panel">

                <div class="x_content">

                    <form class="form-horizontal form-label-left top-margin-20" action="/save/app/settings" method="post" enctype="multipart/form-data">

                        @csrf


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">App Name 1</label>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="text" class="form-control has-feedback-left" name="app_name_1"
                                       value="{{ $settings['app_name_1'] }}">
                                <span class="fa fa-home form-control-feedback left" aria-hidden="true"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">App Name 2</label>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="text" class="form-control has-feedback-left" name="app_name_2"
                                       value="{{ $settings['app_name_2'] }}">
                                <span class="fa fa-home form-control-feedback left" aria-hidden="true"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">App Title</label>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="text" class="form-control has-feedback-left" name="app_title"
                                       value="{{ $settings['app_title'] }}">
                                <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Login Footer</label>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="text" class="form-control has-feedback-left" name="app_login_footer"
                                       value="{{ $settings['app_login_footer'] }}">
                                <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Receipt Company Name</label>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="text" class="form-control has-feedback-left" name="receipt_company"
                                       value="{{ $settings['receipt_company'] }}">
                                <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Receipt Phone</label>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="text" class="form-control has-feedback-left" name="receipt_phone"
                                       value="{{ $settings['receipt_phone'] }}">
                                <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Schedule Backup Interval (Hours)</label>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="text" class="form-control has-feedback-left" name="backup_interval"
                                       value="{{ $settings['backup_interval'] }}">
                                <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Background Type</label>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <select class="form-control" name="bg_type">
                                    <option value="color" {{ $settings['bg_type'] == 'color' ? 'selected':'' }}>Color</option>
                                    <option value="image" {{ $settings['bg_type'] == 'image' ? 'selected':'' }}>Image</option>
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Background Color</label>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="text" class="form-control has-feedback-left" name="bg_color"
                                       value="{{ $settings['bg_color'] }}">
                                <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Background Image</label>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                <input type="file" class="form-control" name="bg_image" accept="image/*">
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5 ">
                                <button class="btn btn-success btn-lg" type="submit">Save App Settings</button>
                            </div>
                        </div>




                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 top-margin-30">
            <div class="x_panel">

                <div class="x_content">

                    <div class="form-horizontal">

                        <div class="animated pulse col-md-6 col-sm-6 col-xs-12">
                            <div class="">
                                <div class="x_title">
                                    <h2>Add New Lookup</h2>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Lookup Name</label>
                                        <div class="col-md-4">
                                            <input type="text" id="lookup_name" class="form-control">
                                        </div>
                                        <div class="col-md-3">
                                            <button class="btn btn-success" id="add_lookup_button">Add Lookup</button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="animated pulse col-md-6 col-sm-6 col-xs-12">
                            <div class="">
                                <div class="x_title">
                                    <h2>Lookup List</h2>
                                    <div class="clearfix"></div>
                                </div>



                                <div class="x_content">
                                    <table class="table table-striped jambo_table bulk_action">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>LookUp Name</th>
                                            <th>Created At</th>
                                            <th width="25%">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody id="lookup_table">



                                        @foreach($lookups as $lookup)
                                            <tr id="lookup_{{ $lookup->lookup_id }}">
                                                <td scope="row"> {{ $lookup->lookup_id }}</td>
                                                <td>{{ $lookup->lookup_name }}</td>
                                                <td>{{ date('F j, Y', strtotime( $lookup->created_at )) }}</td>
                                                <td>
                                                    <button type="button" class="btn btn-dark">
                                                        <i class="glyphicon glyphicon-pencil"></i>
                                                    </button>

                                                    <button type="button" class="btn btn-danger">
                                                        <i class="glyphicon glyphicon-remove"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 top-margin-30">
            <div class="x_panel">

                <div class="x_content">

                    <div class="form-horizontal">

                        <div class="animated pulse col-md-6 col-sm-6 col-xs-12">
                            <div class="">
                                <div class="x_title">
                                    <h2>Add New Role</h2>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Role Name</label>
                                        <div class="col-md-4">
                                            <input type="text" id="lookup_name" class="form-control">
                                        </div>
                                        <div class="col-md-3">
                                            <button class="btn btn-success" id="add_lookup_button">Add Role Lookup</button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="animated pulse col-md-6 col-sm-6 col-xs-12">
                            <div class="">
                                <div class="x_title">
                                    <h2>Role Lookup List</h2>
                                    <div class="clearfix"></div>
                                </div>



                                <div class="x_content">
                                    <table class="table table-striped jambo_table bulk_action">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Role Name</th>
                                            <th>Created At</th>
                                            <th width="25%">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody id="role_lookup_table">

                                        @foreach($role_lookup as $role)
                                            <tr id="role_{{ $role->role_id }}">
                                                <td scope="row"> {{ $role->role_id }}</td>
                                                <td>{{ $role->role_name }}</td>
                                                <td>{{ date('F j, Y', strtotime( $role->created_at )) }}</td>
                                                <td>
                                                    <button type="button" class="btn btn-dark">
                                                        <i class="glyphicon glyphicon-pencil"></i>
                                                    </button>

                                                    <button type="button" class="btn btn-danger">
                                                        <i class="glyphicon glyphicon-remove"></i>
                                                    </button>
                                                </td>
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12 top-margin-30">
            <div class="x_panel">

                <div class="x_content">

                    <div class="form-horizontal">

                        <div class="animated pulse col-md-6 col-sm-6 col-xs-12">
                            <div class="">
                                <div class="x_title">
                                    <h2>Add New Report Type</h2>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">Report Type</label>
                                    <div class="col-md-9 col-sm-9 col-xs-9 form-group has-feedback">
                                        <select class="form-control" id="report_type">
                                            <option value="sales">Sales</option>
                                            <option value="void">Void</option>
                                            <option value="inventory">Inventory</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">Report Name</label>
                                    <div class="col-md-9 col-sm-9 col-xs-9 form-group has-feedback">
                                        <input type="text" class="form-control" id="report_name">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">Report Value</label>
                                    <div class="col-md-9 col-sm-9 col-xs-9 form-group has-feedback">
                                        <input type="text" class="form-control" id="report_value">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5 ">
                                        <button class="btn btn-success" id="add_reportType_button">Add Report Type</button>
                                    </div>
                                </div>


                            </div>
                        </div>

                        <div class="animated pulse col-md-6 col-sm-6 col-xs-12">
                            <div class="">
                                <div class="x_title">
                                    <h2>Report Type Lookup</h2>
                                    <div class="clearfix"></div>
                                </div>



                                <div class="x_content">
                                    <table class="table table-striped jambo_table bulk_action">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Report Type</th>
                                            <th>Name</th>
                                            <th>Value</th>
                                            <th width="25%">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody id="role_lookup_table">

                                        @foreach($report_type as $type)
                                            <tr id="reportType_{{ $type->reportType_id }}">
                                                <td scope="row"> {{ $type->reportType_id }}</td>
                                                <td>{{ $type->report_type }}</td>
                                                <td>{{ $type->report_name }}</td>
                                                <td>{{ $type->report_value }}</td>

                                                <td>
                                                    <button type="button" class="btn btn-dark">
                                                        <i class="glyphicon glyphicon-pencil"></i>
                                                    </button>

                                                    <button type="button" class="btn btn-danger">
                                                        <i class="glyphicon glyphicon-remove"></i>
                                                    </button>
                                                </td>
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
