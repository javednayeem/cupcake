@extends('layouts.main')

@section('title', 'Feature Permissions')

@section('content')

    <script src="/js/superadmin.js"></script>

    <div class="row">

        <div class="animated pulse col-md-12 col-sm-12 col-xs-12 top-margin-20">
            <div class="x_panel">

                <div class="top-margin-20">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Restaurants Name</th>
                            <th>Restaurants Code</th>
                            <th>Status</th>
                            <th width="11%">Accounts</th>
                            <th width="11%">Inventory</th>
                            <th width="11%">Kitchen Queue</th>
                        </tr>
                        </thead>
                        <tbody id="restaurant_inv_table">
                        @php $restaurant_count = 1; @endphp

                        @foreach($restaurant_data as $restaurant)
                            <tr id="restaurant_{{ $restaurant->restaurants_id }}">
                                <td scope="row"> {{ $restaurant_count++ }}</td>
                                <td>{{ $restaurant->restaurants_name }}</td>
                                <td>{{ $restaurant->restaurants_code }}</td>
                                <td>
                                    {{ $restaurant->accounts==1? 'Active Accounts':'Inactive Accounts' }} <br>
                                    {{ $restaurant->inventory==1? 'Active Inventory':'Inactive Inventory' }}</td>
                                <td>
                                    <input class="acc_permission" value="{{ $restaurant->restaurants_id }}" type="checkbox" {{ $restaurant->accounts==1?'checked':''  }}>&nbsp; Tick if active
                                </td>
                                <td>
                                    <input class="inv_permission" value="{{ $restaurant->restaurants_id }}" type="checkbox" {{ $restaurant->inventory==1?'checked':''  }}>&nbsp; Tick if active
                                </td>
                                <td>
                                    <input class="kitchen_queue_permission" value="{{ $restaurant->restaurants_id }}" type="checkbox" {{ $restaurant->kitchen_queue==1?'checked':''  }}>&nbsp; Tick if active
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                </div>
            </div>
        </div>


    </div>



@endsection
