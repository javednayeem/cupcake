<link href="/css/style.css" rel="stylesheet">

<link href="/css/flatpickr.min.css" rel="stylesheet" type="text/css" />
{{--<link href="/css/bootstrap-clockpicker.min.css" rel="stylesheet" type="text/css" />--}}
{{--<link href="/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />--}}

<!-- Bootstrap -->
<link href="/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="/css/font-awesome.min.css" rel="stylesheet">
<!-- NProgress -->
<link href="/css/nprogress.css" rel="stylesheet">

<link href="/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">

<link href="/css/daterangepicker.css" rel="stylesheet">
<!-- bootstrap-datetimepicker -->
<link href="/css/bootstrap-datetimepicker.css" rel="stylesheet">

<link href="/css/ion.rangeSlider.css" rel="stylesheet">
<link href="/css/ion.rangeSlider.skinFlat.css" rel="stylesheet">
<link href="/css/bootstrap-colorpicker.min.css" rel="stylesheet">



<script src="/js/jquery.min.js"></script>
<script src="/js/echarts.min.js"></script>
<script src="/js/Chart.min.js"></script>

{{--<!-- PNotify -->--}}
{{--<link href="/css/pnotify.css" rel="stylesheet">--}}
{{--<link href="/css/pnotify.buttons.css" rel="stylesheet">--}}
{{--<link href="/css/pnotify.nonblock.css" rel="stylesheet">--}}



<!-- Sweet Alert css -->
<link href="/css/sweetalert2.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="/css/animate.min.css">

<link rel="stylesheet" href="/css/jquery.dataTables.min.css" type="text/css"/>

<!-- Custom Theme Style -->
<link href="/css/custom.min.css" rel="stylesheet">

<script src="/js/jquery311.min.js"></script>
<script src="/js/application.js"></script>


<link href="/css/select2.min.css" rel="stylesheet" />