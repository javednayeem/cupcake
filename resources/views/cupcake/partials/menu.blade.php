<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">

        <ul class="nav side-menu">

            @if(Auth::user()->role == "superadmin")
                <li><a href="/sa-dashboard"><i class="fa fa-home"></i>@lang('lang.superadmin_dashboard')</a></li>

                <li>
                    <a><i class="fa fa-users"></i>@lang('lang.superadmin_settings')<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="/create-restaurant">@lang('lang.create_new_restaurant')</a></li>
                        <li><a href="/restaurants">@lang('lang.restaurants')</a></li>
                        <li><a href="/create-user">@lang('lang.create_user')</a></li>
                        <li><a href="/sa-licenses">@lang('lang.licenses')</a></li>
                        <li><a href="/bank-cards">@lang('lang.bank_cards')</a></li>
                        <li><a href="/app-settings">@lang('lang.app')</a></li>
                        <li><a href="/look-up">@lang('lang.lookup')</a></li>
                        <li><a href="/inv-settings">@lang('lang.inventory')</a></li>
                        <li><a href="/data-settings">@lang('lang.data')</a></li>
                        <li><a href="/schedule-scripts">@lang('lang.schedule_scripts')</a></li>
                        <li><a href="/application-menu">@lang('lang.application_menu')</a></li>
                        @if(Auth::user()->email == "superuser@cupcake.com")
                            <li><a href="/view-order">@lang('lang.view_orders')</a></li>
                        @endif

                    </ul>
                </li>

            @endif

            @php

                $user_id = Auth::user()->id;

                $application_menu = DB::table('application_menu as a')
                    ->select('a.*',
                        DB::raw("(SELECT menu_name FROM application_menu as b WHERE b.menu_id=a.parent_menu) as parent_menu_name"),
                        DB::raw("(SELECT COUNT(menu_id) FROM application_menu as c WHERE a.menu_id=c.parent_menu) as parent_count"),
                        DB::raw("(SELECT permission FROM application_menu_permission as d WHERE a.menu_id=d.menu_id AND d.user_id=$user_id) as permission")
                    )
                    ->get();

            @endphp



            @if(Auth::user()->role == "superadmin" || searchMenu('Dashboard', $application_menu))
                <li><a href="/dashboard"><i class="fa fa-home"></i> @lang('lang.dashboard')</a></li>
            @endif


            @if(Auth::user()->role == "superadmin" || searchMenu('Accounts', $application_menu))
                @if(Auth::user()->accounts == 1)
                    <li>
                        <a><i class="fa fa-bar-chart-o"></i>@lang('lang.accounts')<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">

                            @if(Auth::user()->role == "superadmin" || searchMenu('Cash Opening', $application_menu))
                                <li><a href="/cash-opening">@lang('lang.cash_opening')</a></li>
                            @endif

                            @if(Auth::user()->role == "superadmin" || searchMenu('Daily Expense', $application_menu))
                                <li><a href="/daily-expense">@lang('lang.daily_expense')</a></li>
                            @endif

                            @if(Auth::user()->role == "superadmin" || searchMenu('Cash Closing', $application_menu))
                                <li><a href="/cash-closing">@lang('lang.cash_closing')</a></li>
                            @endif

                            @if(Auth::user()->role == "superadmin" || searchMenu('Loan Credit', $application_menu))
                                <li><a href="/loan-credit">@lang('lang.loan_credit')</a></li>
                            @endif

                            @if(Auth::user()->role == "superadmin" || searchMenu('Loan Debit', $application_menu))
                                <li><a href="/loan-debit">@lang('lang.loan_debit')</a></li>
                            @endif
                        </ul>
                    </li>
                @endif
            @endif


            @if(Auth::user()->role == "superadmin" || searchMenu('Guest', $application_menu))
                <li><a href="/guests"><i class="fa fa-user"></i>@lang('lang.guest')</a></li>
            @endif


            @if(Auth::user()->role == "superadmin" || searchMenu('Inventory', $application_menu))

                @if(Auth::user()->inventory == 1)
                    <li>
                        <a><i class="fa fa-sitemap"></i>@lang('lang.inventory')<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">

                            @if(Auth::user()->role == "superadmin" || searchMenu('Supplier', $application_menu))
                                <li><a href="/supplier">@lang('lang.supplier')</a></li>
                            @endif

                            @if(Auth::user()->role == "superadmin" || searchMenu('Manage Product', $application_menu))
                                <li><a>@lang('lang.manage_product')<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <li><a href="/product-category">@lang('lang.category')</a></li>
                                        <li><a href="/products">@lang('lang.products')</a></li>
                                        <li><a href="/product-unit">@lang('lang.unit')</a></li>
                                    </ul>
                                </li>
                            @endif

                            @if(Auth::user()->role == "superadmin" || searchMenu('Purchase', $application_menu))
                                <li><a>@lang('lang.purchase')<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <li class="sub_menu"><a href="/new-purchase">@lang('lang.new_purchase')</a></li>
                                        <li><a href="/purchase-history">@lang('lang.purchase_history')</a></li>
                                    </ul>
                                </li>
                            @endif

                            @if(Auth::user()->role == "superadmin" || searchMenu('Manage Stock', $application_menu))
                                <li><a>@lang('lang.manage_stock')<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <li><a href="/current-stock">@lang('lang.current_stock')</a></li>
                                        <li><a href="/transfer-stock">@lang('lang.transfer_stock')</a></li>
                                        <li><a href="/receive-stock">@lang('lang.receive_stock')</a></li>
                                        <li><a href="/transfer-history">@lang('lang.transfer_history')</a></li>
                                        <li><a href="/damage-entry">@lang('lang.damage_entry')</a></li>
                                        <li><a href="/damage-history">@lang('lang.damage_history')</a></li>
                                    </ul>
                                </li>
                            @endif

                            @if(Auth::user()->role == "superadmin" || searchMenu('Store', $application_menu))
                                <li><a href="/store">@lang('lang.store')</a></li>
                            @endif

                        </ul>
                    </li>
                @endif

            @endif


            @if(Auth::user()->role == "superadmin" || searchMenu('Menu', $application_menu))
                <li>
                    <a><i class="fa fa-bars"></i> @lang('lang.menu')<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">

                        @if(Auth::user()->role == "superadmin" || searchMenu('Create Menu', $application_menu))
                            <li><a href="/menu">@lang('lang.create_menu')</a></li>
                        @endif

                        @if(Auth::user()->role == "superadmin" || searchMenu('Modifiers', $application_menu))
                            <li><a href="/modifiers">@lang('lang.modifiers')</a></li>
                        @endif

                        @if(Auth::user()->role == "superadmin" || searchMenu('Cooking Instructions', $application_menu))
                            <li><a href="/order-instruction">@lang('lang.cooking_instructions')</a></li>
                        @endif

                        @if(Auth::user()->role == "superadmin" || searchMenu('Discount Circular', $application_menu))
                            <li><a href="/discount-circular">@lang('lang.discount_circular')</a></li>
                        @endif

                        @if(Auth::user()->role == "superadmin" || searchMenu('Recipe', $application_menu))
                            @if(Auth::user()->inventory == 1)
                                <li><a href="/recipes">@lang('lang.recipe')</a></li>
                            @endif
                        @endif
                    </ul>
                </li>
            @endif

            @if(Auth::user()->role == "customer")
                <li><a href="/online-order"><i class="fa fa-edit"></i>@lang('lang.order')</a></li>
            @else
                <li><a href="/order"><i class="fa fa-edit"></i>@lang('lang.order')</a></li>
            @endif


            @if(Auth::user()->role == "superadmin" || searchMenu('Online Orders', $application_menu))
                @if(Auth::user()->online_order == 1)
                    <li><a href="/online/orders"><i class="fa fa-list"></i>@lang('lang.online_order')</a></li>
                @endif
            @endif



            @if(Auth::user()->role == "superadmin")
                <li><a href="/table-order"><i class="fa fa-edit"></i>@lang('lang.table_order')</a></li>
            @endif

            @if(Auth::user()->role == "superadmin" || searchMenu('Kitchen Queue', $application_menu))
                @if(Auth::user()->kitchen_queue == 1)
                    <li><a href="/kitchen-queue"><i class="fa fa-list"></i>@lang('lang.kitchen_queue')</a></li>
                @endif
            @endif


            @if(Auth::user()->role == "superadmin" || searchMenu('Report', $application_menu))

                <li>
                    <a><i class="fa fa-bar-chart-o"></i>@lang('lang.report')<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">

                        @if(Auth::user()->role == "superadmin" || searchMenu('Report Summary', $application_menu))
                            <li><a href="/sales-report">@lang('lang.report_summary')</a></li>
                        @endif

                        @if(Auth::user()->role == "superadmin" || searchMenu('Sales Report', $application_menu))
                            <li><a href="/report">@lang('lang.sales_report')</a></li>
                        @endif

                        @if(Auth::user()->role == "superadmin" || searchMenu('Void Report', $application_menu))
                            <li><a href="/void-report">@lang('lang.void_report')</a></li>
                        @endif

                        @if(Auth::user()->role == "superadmin" || searchMenu('Expense Report', $application_menu))
                            <li><a href="/expense-report">@lang('lang.expense_report')</a></li>
                        @endif

                        @if(Auth::user()->role == "superadmin" || searchMenu('Revenue Report', $application_menu))
                            <li><a href="/revenue-report">@lang('lang.revenue_report')</a></li>
                        @endif

                        @if(Auth::user()->role == "superadmin" || searchMenu('Reservation Report', $application_menu))
                            <li><a href="/reservation-report">@lang('lang.reservation_report')</a></li>
                        @endif

                        @if(Auth::user()->role == "superadmin" || searchMenu('Recipe Report', $application_menu))
                            <li><a href="/recipe-report">@lang('lang.recipe_report')</a></li>
                        @endif
                    </ul>
                </li>

            @endif

            @if(Auth::user()->role == "superadmin" || searchMenu('Reservation', $application_menu))
                <li>
                    <a><i class="fa fa-ticket"></i> @lang('lang.reservation')<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">

                        @if(Auth::user()->role == "superadmin" || searchMenu('Create New Reservation', $application_menu))
                            <li><a href="/new-reservation">@lang('lang.create_new_reservation')</a></li>
                        @endif

                        @if(Auth::user()->role == "superadmin" || searchMenu('Reservation List', $application_menu))
                            <li><a href="/reservations">@lang('lang.reservation_list')</a></li>
                        @endif
                    </ul>
                </li>
            @endif


            @if(Auth::user()->role == "superadmin" || searchMenu('Settings', $application_menu))
                <li>
                    <a><i class="fa fa-users"></i>@lang('lang.settings')<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">

                        @if(Auth::user()->role == "superadmin" || searchMenu('Users', $application_menu))
                            <li><a href="/user-setting">@lang('lang.users')</a></li>
                        @endif

                        @if(Auth::user()->role == "superadmin" || searchMenu('Company Info', $application_menu))
                            <li><a href="/restaurant-settings">@lang('lang.company_info')</a></li>
                        @endif

                        @if(Auth::user()->role == "superadmin" || searchMenu('Tables', $application_menu))
                            <li><a href="/table-settings">@lang('lang.tables')</a></li>
                        @endif

                        @if(Auth::user()->role == "superadmin" || searchMenu('App Settings', $application_menu))
                            <li><a href="/application-setting">@lang('lang.app')</a></li>
                        @endif

                        @if(Auth::user()->role == "superadmin" || searchMenu('Permissions', $application_menu))
                            <li><a href="/permission-settings">@lang('lang.permissions')</a></li>
                        @endif

                        @if(Auth::user()->role == "superadmin" || searchMenu('Printers', $application_menu))
                            <li><a href="/printers">@lang('lang.printers')</a></li>
                        @endif

                        @if(Auth::user()->role == "superadmin")
                            <li><a href="/back-up">@lang('lang.back_up')</a></li>
                        @endif

                        @if(Auth::user()->role == "superadmin" || searchMenu('Team', $application_menu))
                            <li><a href="/team">@lang('lang.team')</a></li>
                        @endif

                    </ul>
                </li>
            @endif

            {{--<li><a href="/activity"><i class="fa fa-eye"></i>Activity</a></li>--}}



        </ul>
    </div>


</div>