<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
                <div class="bs-example top-margin-30">
                    <div class="jumbotron">
                        <h1>License Has Been Expired</h1>
                        <p>Please Contact Your System Administrator For Further Information</p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>