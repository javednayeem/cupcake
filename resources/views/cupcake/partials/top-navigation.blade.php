@php $role=Auth::user()->role; @endphp

<div class="top_nav">
  <div class="nav_menu">
    <nav>
      <div class="nav toggle">
        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
      </div>

      <ul class="nav navbar-nav navbar-right">
        <li class="">
          <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <img src="/images/users/{{ Auth::user()->user_img }}">
            {{ Auth::user()->name }}
            <span class=" fa fa-angle-down"></span>
          </a>
          <ul class="dropdown-menu dropdown-usermenu pull-right">
            <li><a href="/profile"> Profile</a></li>
            @if($role == 'customer')
              <li><a href="/my-orders">My Orders</a></li>
            @endif
            <li><a href="/logout" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
          </ul>
        </li>

        <li role="presentation">

          <a href="javascript:void(0);" onclick="reloadCurrentPage();">
            <i class="fa fa-refresh"></i>
          </a>

        </li>

        @if(Auth::user()->inventory == 1 && $role != 'customer')
          <li role="presentation" class="dropdown">

            <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
              <i class="fa fa-sitemap"></i>
              @php
                $restaurant_id = Auth::user()->restaurant_id;

                $inv_product_store = DB::table('inv_product_store')
                  ->select('inv_product_store.*', 'inv_product.product_name','inv_units.unit_name')
                  ->where('inv_stores.restaurant_id', $restaurant_id)
                  ->where('inv_product_store.quantity', '<', 5)
                  ->join('inv_stores', 'inv_stores.store_id', 'inv_product_store.store_id')
                  ->join('inv_product', 'inv_product.product_id', 'inv_product_store.product_id')
                  ->join('inv_units', 'inv_units.unit_id', 'inv_product.unit_id')
                  ->get();
              @endphp

              @if(count($inv_product_store)>0)
                <span class="badge bg-red">{{ count($inv_product_store) }}</span>
              @endif
            </a>

            <ul class="dropdown-menu list-unstyled msg_list" role="menu" style="height:500px;overflow:hidden; overflow-y:scroll;">

              @foreach($inv_product_store as $product)

                <li>
                  <a href="javascript:void(0);">
                <span>
                  <b><h4>{{ $product->product_name }}</h4></b>
                </span>
                    <b class="message text-danger">Remaining: {{ $product->quantity . $product->unit_name}}</b>
                  </a>
                </li>

              @endforeach

            </ul>

          </li>
        @endif




        <li class="dropdown">
          <a href="#" class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown"
             role="button" aria-haspopup="true" aria-expanded="false">
            {{ __('lang.language') }}
            <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="javascript:void(0)" id="select_en">{{ __('lang.english') }}</a></li>
            <li><a href="javascript:void(0)" id="select_bn">{{ __('lang.bangla') }}</a></li>
          </ul>
        </li>






        <li role="presentation" style="margin-top: 10px">
          <h4>
            <time id="clock_span" ></time>
          </h4>
        </li>


      </ul>
    </nav>
  </div>
</div>