@extends('layouts.main')

@section('title', 'Settings')

@section('content')

    <div class="row">

        @if (count($table_data) == 0)
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Table Settings</h2>
                        <div class="clearfix"></div>
                    </div>

                    <div class="form-horizontal">

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Number of Tables</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" id="table_no" class="form-control col-md-7 col-xs-12" placeholder="15">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Table</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control" id="select_table_capacity">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4" selected>4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>

                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5 ">
                                <button class="btn btn-success btn-lg" id="create_table_button">Create Tables</button>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        @else
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Tables</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_table_modal">Add Table</button></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">

                        <div class="table-responsive">
                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                <tr class="headings">
                                    <th class="column-title">#</th>
                                    <th class="column-title">Table Name</th>
                                    <th class="column-title">Capacity</th>
                                    <th class="column-title">Created At</th>
                                    <th class="column-title">Updated At</th>
                                    <th class="column-title no-link last" width="11%"><span class="nobr">Action</span>
                                    </th>
                                </tr>
                                </thead>

                                <tbody id="restaurant_table_list">

                                @php $table_count=0; @endphp

                                @foreach($table_data as $table)
                                    <tr class="even pointer" id="table_{{ $table->table_id }}">
                                        <td>{{ ++$table_count }}</td>
                                        <td id="table_name_{{ $table->table_id }}">{{ $table->table_name }}</td>
                                        <td id="table_capacity_{{ $table->table_id }}">{{ $table->capacity }}</td>
                                        <td>{{ date('F j, Y', strtotime( $table->created_at )) }}</td>
                                        <td>{{ date('F j, Y', strtotime( $table->updated_at )) }}</td>
                                        <td>

                                            <button type="button" title="Edit Table" class="btn btn-dark pointer" onclick="editTable('{{ $table->table_id }}', '{{ $table->table_name }}', '{{ $table->capacity }}')">
                                                <i class="glyphicon glyphicon-pencil"></i>
                                            </button>

                                            <button type="button" class="btn btn-danger pointer" title="Delete Table" onclick="deleteTable({{ $table->table_id }})">
                                                <i class="glyphicon glyphicon-remove"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>


                    </div>
                </div>
            </div>
        @endif


    </div>


    <!--add table Modal -->
    <div class="modal fade" id="add_table_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title" id="edit_table_title">Add Table</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Number of Tables</label>
                        <input type="number" class="form-control" id="modal_table_number" placeholder="5" min="1">
                    </div>

                    <div class="form-group">
                        <label>Capacity</label>
                        <input type="number" class="form-control" id="modal_new_table_capacity" placeholder="4" min="1">
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="add_table_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                </div>
            </div>

        </div>
    </div>
    <!-- end Modal -->


    <!--edit table Modal -->
    <div class="modal fade" id="edit_table_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title" id="edit_table_title">Edit Table</h4>
                    <input type="hidden" id="edit_table_id" value="0">
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label>Table Name</label>
                        <input type="text" class="form-control" id="modal_table_name" placeholder="12">
                    </div>

                    <div class="form-group">
                        <label>Capacity</label>
                        <input type="number" class="form-control" id="modal_table_capacity" placeholder="4">
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="edit_table_button" class="btn btn-success" data-dismiss="modal">Edit</button>
                </div>
            </div>

        </div>
    </div>
    <!-- end Modal -->

    <script src="/js/settings.js"></script>

@endsection
