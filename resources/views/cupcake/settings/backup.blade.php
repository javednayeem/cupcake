@extends('layouts.main')

@section('title', 'Back-Up')

@section('content')

    <script src="/js/backup.js"></script>


    <div class="row">
        <div class="col-md-12 top-margin-20">
            <div class="x_panel">

                <div class="top-margin-20">

                    <ul class="nav navbar-right panel_toolbox">
                        <li><button type="button" class="btn btn-primary" id="db_backup_button">Create Database Back-Up</button></li>
                    </ul>

                    @if (isset($backup_schedule->next_schedule))
                        <h2>Next Backup Schedule At: {{ date('F j, Y h:i:s A', strtotime( $backup_schedule->next_schedule )) }}</h2>
                    @endif

                    <div class="clearfix"></div>
                </div>


                <div class="x_content">
                    <div data-parsley-validate class="form-horizontal">

                        <div class="top-margin-20">
                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                <tr>
                                    <th>Log Id</th>
                                    <th class="text-center">Executed At</th>
                                    <th width="33%">Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($backup_log as $log)
                                    <tr id="log_{{ $log->log_id }}">
                                        <td>{{ $log->log_id }}</td>
                                        <td class="text-center">{{ date('F j, Y h:i:s A', strtotime( $log->created_at )) }}</td>
                                        <td>

                                            <a download="{{ $log->file_name }}" href="/download/back-up/{{ $log->file_name }}" title="{{ $log->file_name }}" type="button" class="btn btn-primary" {{ Auth::user()->role != "superadmin"?'disabled':'' }}>
                                                <i class="glyphicon glyphicon-download-alt"></i> Download
                                            </a>

                                            <button type="button" class="btn btn-dark" {{ Auth::user()->role != "superadmin"?'disabled':'' }}>
                                                <i class="glyphicon glyphicon-refresh"></i> Restore
                                            </button>

                                            <button type="button" onclick="deleteBackup({{ $log->log_id }})" class="btn btn-danger" {{ Auth::user()->role != "superadmin"?'disabled':'' }}>
                                                <i class="glyphicon glyphicon-remove"></i> Delete
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>

                            {{ $backup_log->links() }}

                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
