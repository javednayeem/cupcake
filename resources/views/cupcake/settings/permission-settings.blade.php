@extends('layouts.main')

@section('title', 'Permissions')

@section('content')

    <script src="/js/settings.js"></script>


    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12 top-margin-20">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Users</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br>
                    <div data-parsley-validate="" class="form-horizontal form-label-left">


                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4 col-xs-12">Select Any User</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control" id="select_user_id">
                                    <option value="0">Select User</option>

                                    @foreach($users as $user)
                                        <option value="{{ $user->id}}">{{ $user->name }} ({{ ucfirst($user->role) }})</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>


    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">

                <div class="x_title top-margin-20">
                    <h2>Menu List</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <button type="button" onclick="unCheckAllMenu()" class="btn btn-dark waves-effect waves-light float-right ml-2">Clear All</button>
                            <button type="button" onclick="checkAllMenu()" class="btn btn-dark waves-effect waves-light float-right ml-2">Select All</button>
                            <button type="button" class="btn btn-success waves-effect waves-light float-right save_permission">Save</button>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <form id="demo-form" class="mt-3">

                    @foreach($application_menu as $menu)

                        @if ($menu->parent_menu == 0)

                            <ul style="list-style: none;" class="mt-3">
                                <li>

                                    <div class="checkbox checkbox-pink mb-1">
                                        <input type="checkbox" name="menu[]" id="menu_{{ $menu->menu_id }}" value="{{ $menu->menu_id }}">
                                        <label for="{{ $menu->menu_id }}"> {{ $menu->menu_name }}</label>
                                    </div>

                                    <ul style="list-style: none;">

                                        @foreach($application_menu as $submenu)
                                            @if ($submenu->parent_menu == $menu->menu_id)
                                                <li>
                                                    <div class="checkbox checkbox-pink mb-1">
                                                        <input type="checkbox" name="menu[]" id="menu_{{ $submenu->menu_id }}" value="{{ $submenu->menu_id }}">
                                                        <label for="{{ $submenu->menu_id }}"> {{ $submenu->menu_name }}</label>
                                                    </div>
                                                </li>
                                            @endif
                                        @endforeach

                                    </ul>
                                </li>
                            </ul>

                        @endif

                    @endforeach

                    <div class="row text-center">
                        <button type="button" class="btn btn-success waves-effect waves-light float-right save_permission">Save </button>
                        <button type="button" onclick="checkAllMenu()" class="btn btn-dark waves-effect waves-light float-right ml-2">Select All</button>
                        <button type="button" onclick="unCheckAllMenu()" class="btn btn-dark waves-effect waves-light float-right ml-2">Clear All</button>
                    </div>

                    {{--<div class="button-list pr-xl-4 mt-3">--}}
                    {{--<button type="button" class="btn btn-block btn-lg btn-success waves-effect waves-light save_permission">Change Permission</button>--}}
                    {{--</div>--}}

                </form>
            </div>
        </div>
    </div>


    <div class="row">

        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="x_panel">
                <div class="x_title top-margin-20">
                    <h2>Sales Report</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <button type="button" class="btn btn-warning" id="table_sales_toggle">Check/Uncheck All</button>
                            <button type="button" class="btn btn-success" id="change_sales_permission">Change Permission</button>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">

                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">

                            <thead>
                            <tr class="headings">
                                <th class="column-title">#</th>
                                <th class="column-title"></th>
                                <th class="column-title">Report Name</th>
                            </tr>
                            </thead>

                            <tbody>

                            @php $table_count=1; @endphp

                            @foreach($report_type as $type)
                                @if($type->report_type == 'sales')
                                    <tr>
                                        <td scope="row"> {{ $table_count++ }}</td>
                                        <td>
                                            <input class="table_sales" id="reportType_{{ $type->reportType_id }}" value="{{ $type->reportType_id }}" type="checkbox">
                                        </td>
                                        <td>{{ $type->report_name }}</td>
                                    </tr>
                                @endif

                            @endforeach

                            </tbody>

                        </table>
                    </div>


                </div>

            </div>
        </div>


        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="x_panel">
                <div class="x_title top-margin-20">
                    <h2>Void Report</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <button type="button" class="btn btn-warning" id="table_void_toggle">Check/Uncheck All</button>
                            <button type="button" class="btn btn-success" id="change_void_report_permission">Change Permission</button>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">

                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">

                            <thead>
                            <tr class="headings">
                                <th class="column-title">#</th>
                                <th class="column-title"></th>
                                <th class="column-title">Report Name</th>
                            </tr>
                            </thead>

                            <tbody>

                            @php $table_count=1; @endphp

                            @foreach($report_type as $type)
                                @if($type->report_type == 'void')
                                    <tr>
                                        <td scope="row"> {{ $table_count++ }}</td>
                                        <td>
                                            <input class="table_void" id="reportType_{{ $type->reportType_id }}" value="{{ $type->reportType_id }}" type="checkbox">
                                        </td>
                                        <td>{{ $type->report_name }}</td>
                                    </tr>
                                @endif

                            @endforeach

                            </tbody>

                        </table>
                    </div>


                </div>

            </div>
        </div>


    </div>


    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title top-margin-20">
                    <h2>Void Permission</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <button type="button" class="btn btn-warning" id="table_void_permission_toggle">Check/Uncheck All</button>
                            <button type="button" class="btn btn-success" id="change_void_permission">Change Permission</button>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">

                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">

                            <thead>
                            <tr class="headings">
                                <th class="column-title">#</th>
                                <th class="column-title"></th>
                                <th class="column-title">User Name</th>
                            </tr>
                            </thead>

                            <tbody>

                            @php $table_count=1; @endphp

                            @foreach($users as $user)
                                <tr>
                                    <td scope="row"> {{ $table_count++ }}</td>
                                    <td>
                                        <input class="users" id="void_permission_{{ $user->id }}" value="{{ $user->id }}" type="checkbox" {{ $user->void_permission=='1'?'checked':'' }}>
                                    </td>
                                    <td>{{ $user->name }} ({{ ucfirst($user->role) }})</td>
                                </tr>
                            @endforeach

                            </tbody>

                        </table>
                    </div>


                </div>

            </div>
        </div>

    </div>



@endsection
