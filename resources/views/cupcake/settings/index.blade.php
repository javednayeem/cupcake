@extends('layouts.main')

@section('title', 'Settings')

@section('content')

    <div class="row">

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Restaurant Details</h2>
                    <div class="clearfix"></div>
                </div>

                <div class="form-horizontal">

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Restaurant Name</label>
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                            <input type="text" class="form-control has-feedback-left" id="restaurant_name" placeholder="Chef" value="{{ $restaurant->restaurants_name }}">
                            <span class="fa fa-home form-control-feedback left" aria-hidden="true"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Restaurant Address</label>
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                            <input type="text" class="form-control has-feedback-left" id="restaurant_address" placeholder="House#133, Road#4" value="{{ $restaurant->address }}">
                            <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Area</label>
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                            <input type="text" class="form-control has-feedback-left" id="restaurant_area" placeholder="Dhanmondi" value="{{ $restaurant->area }}" >
                            <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">City</label>
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                            <input type="text" class="form-control has-feedback-left" id="restaurant_city" placeholder="Dhaka" value="{{ $restaurant->city }}">
                            <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Restaurant Phone</label>
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                            <input type="number" class="form-control has-feedback-left" id="restaurant_phone" placeholder="01715839494" value="{{ $restaurant->phone_number }}">
                            <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Restaurant Email</label>
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                            <input type="email" class="form-control has-feedback-left" id="restaurant_email" value="{{ $restaurant->email }}">
                            <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Restaurant Website</label>
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                            <input type="email" class="form-control has-feedback-left" id="restaurant_website" value="{{ $restaurant->website }}">
                            <span class="fa fa-globe form-control-feedback left" aria-hidden="true"></span>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Petty Cash</label>
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                            <input type="email" class="form-control has-feedback-left" id="petty_cash" value="{{ $restaurant->petty_cash }}">
                            <span class="fa fa-money form-control-feedback left" aria-hidden="true"></span>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5 ">
                            <button class="btn btn-success btn-lg" id="save_restaurant_info_button">Save Restaurant Info</button>
                        </div>
                    </div>


                </div>
            </div>
        </div>


        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Charges Details</h2>
                    <div class="clearfix"></div>
                </div>

                <div class="form-horizontal">

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">VAT Reg. No / BIN</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" id="vat_no" placeholder="181******48" value="{{ $restaurant->vat_no }}">
                            <span class="fa fa-registered form-control-feedback right" aria-hidden="true"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Price Including VAT</label>
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group" style="margin-top: 7px">
                            <input type="checkbox" class="" id="price_including_vat"
                                    {{ $restaurant->price_including_vat==1?'checked':'' }}>

                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">VAT After Discount</label>
                        <div class="col-md-6 col-sm-6 col-xs-12 form-group" style="margin-top: 7px">
                            <input type="checkbox" class="" id="vat_after_discount"
                                    {{ $restaurant->vat_after_discount==1?'checked':'' }}>

                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">VAT</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" id="vat_percentage" placeholder="15" value="{{ $restaurant->vat_percentage }}">
                            <span class="fa fa-percent form-control-feedback right" aria-hidden="true"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Service Charge</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" id="service_charge" placeholder="10" value="{{ $restaurant->service_charge }}">
                            <span class="fa fa-percent form-control-feedback right" aria-hidden="true"></span>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">SD Percentage</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" id="sd_percentage" placeholder="10" value="{{ $restaurant->sd_percentage }}">
                            <span class="fa fa-percent form-control-feedback right" aria-hidden="true"></span>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Service Charge VAT Percentage</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" id="service_charge_vat_percentage" placeholder="10" value="{{ $restaurant->service_charge_vat_percentage }}">
                            <span class="fa fa-percent form-control-feedback right" aria-hidden="true"></span>
                        </div>
                    </div>


                    <div class="form-group hidden">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">SD VAT Percentage</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" class="form-control" id="sd_vat_percentage" placeholder="10" value="{{ $restaurant->sd_vat_percentage }}">
                            <span class="fa fa-percent form-control-feedback right" aria-hidden="true"></span>
                        </div>
                    </div>



                    {{--<div class="form-group">--}}
                    {{--<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5 ">--}}
                    {{--<button class="btn btn-success btn-lg" id="save_charge_button">Save Charges</button>--}}
                    {{--</div>--}}
                    {{--</div>--}}


                </div>
            </div>
        </div>


    </div>

    <div class="row">

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Reporting Emails</h2>

                    <ul class="nav navbar-right panel_toolbox">
                        <li><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_email_modal">Add Email</button></li>

                    </ul>

                    <div class="clearfix"></div>
                </div>

                <div class="x_content">

                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                            <tr class="headings">
                                <th class="column-title">#</th>
                                <th class="column-title">Email</th>
                                <th class="column-title" width="25%"><span class="nobr">Action</span>
                                </th>
                            </tr>
                            </thead>

                            <tbody id="emails_table">

                            @php $email_count=0; @endphp

                            @foreach($emails as $email)
                                <tr class="even pointer" id="email_{{ $email->email_id }}">
                                    <td>{{ ++$email_count }}</td>
                                    <td id="email_address_{{ $email->email_id }}">{{ $email->email }}</td>

                                    <td>
                                        <button type="button" class="btn btn-dark pointer" onclick="editEmail('{{ $email->email_id }}', '{{ $email->email }}')">
                                            <i class="glyphicon glyphicon-pencil"></i>
                                        </button>

                                        <button type="button" class="btn btn-danger pointer" onclick="deleteEmail({{ $email->email_id }})">
                                            <i class="glyphicon glyphicon-remove"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>


                </div>
            </div>
        </div>


        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Restaurant Logo</h2>
                    <div class="clearfix"></div>
                </div>

                <form class="form-horizontal" action="restaurant-settings" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" value="{{ Auth::user()->restaurant_id }}" name="restaurant_id">
                    <div class="col-md-6 col-sm-12 col-xs-12 form-group">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <label class="">Image</label>
                            <input type="file" class="form-control" name="image" accept="image/*">
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12 form-group" >
                        <img src="/images/restaurant-logo/{{ getRstaurantLogo() }}" class="logo-icon-preview" >
                    </div>



                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5 ">
                            <button class="btn btn-success btn-lg" id="upload_button" type="submit">Upload Logo</button>
                        </div>
                    </div>


                </form>
            </div>
        </div>

    </div>



    <div class="modal fade" id="add_email_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title" id="edit_table_title">Add New Email</h4>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label>Email Address</label>
                        <input type="email" class="form-control" id="email">
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="add_email_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                </div>
            </div>

        </div>
    </div>


    <div class="modal fade" id="edit_email_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">x</button>
                    <h4 class="modal-title" id="edit_table_title">Edit Email</h4>
                </div>
                <div class="modal-body">

                    <input type="hidden" id="email_id">

                    <div class="form-group">
                        <label>Email Address</label>
                        <input type="email" class="form-control" id="edit_email">
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    <button type="button" id="edit_email_button" class="btn btn-success" data-dismiss="modal">Ok</button>
                </div>
            </div>

        </div>
    </div>




    <script src="/js/settings.js"></script>

@endsection
