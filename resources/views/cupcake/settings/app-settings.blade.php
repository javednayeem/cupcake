@extends('layouts.main')

@section('title', 'App Settings')

@section('content')

    <script src="/js/settings.js"></script>


    <div class="row">

        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 top-margin-20">
            <div class="x_panel">
                @include('cupcake.settings.restaurant-settings')
            </div>
        </div>


        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 top-margin-20">
            <div class="x_panel">

                <div class="x_title top-margin-20">
                    <h2>Order Types</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <button type="button" onclick="unCheckAllOrderTypes()" class="btn btn-dark waves-effect waves-light float-right ml-2">Clear All</button>
                            <button type="button" onclick="checkAllOrderTypes()" class="btn btn-dark waves-effect waves-light float-right ml-2">Select All</button>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>


                @foreach($order_types as $type)

                    <ul style="list-style: none;" class="mt-3">
                        <li>
                            <div class="checkbox mb-1">
                                <input type="checkbox" name="order_types[]" id="menu_{{ $type->type_id }}" value="{{ $type->type_id }}" @if($type->permission==1) checked @endif>
                                <label for="{{ $type->type_id }}"> {{ $type->type_name }}</label>
                            </div>
                        </li>
                    </ul>

                @endforeach



                <div class="button-list pr-xl-4 top-margin-20">
                    <button type="button" class="btn btn-block btn-lg btn-success waves-effect waves-light" id="save_order_types_permission">Change Permission</button>
                </div>


            </div>
        </div>

    </div>

@endsection
