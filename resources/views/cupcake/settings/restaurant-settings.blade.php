<div class="x_content">

    <div class="form-horizontal form-label-left top-margin-20">

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Guest Bill</label>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group" style="margin-top: 7px">
                <input type="checkbox" class="" id="guest_bill"
                        {{ $settings['guest_bill']==1?'checked':'' }}>

            </div>
        </div>


        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Waiter Order Void</label>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group" style="margin-top: 7px">
                <input type="checkbox" class="" id="waiter_order_void"
                        {{ $settings['waiter_order_void']==1?'checked':'' }}>

            </div>
        </div>


        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Order Receipt Debug</label>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group" style="margin-top: 7px">
                <input type="checkbox" class="" id="order_receipt_debug"
                        {{ $settings['order_receipt_debug']==1?'checked':'' }}>

            </div>
        </div>


        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Pay First</label>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group" style="margin-top: 7px">
                <input type="checkbox" class="" id="pay_first"
                        {{ $settings['pay_first']==1?'checked':'' }}>

            </div>
        </div>


        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Kitchen Type</label>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                <select class="form-control" id="kitchen_type">
                    <option value="single" {{ $settings['kitchen_type']=='single'?'selected':'' }}>Single Kitchen</option>
                    <option value="multiple" {{ $settings['kitchen_type']=='multiple'?'selected':'' }}>Multiple Kitchen</option>
                </select>
            </div>
        </div>


        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">View Settle Receipt</label>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                <select class="form-control" id="view_settle_receipt">
                    <option value="1" {{ $settings['view_settle_receipt']=='1'?'selected':'' }}>Yes</option>
                    <option value="0" {{ $settings['view_settle_receipt']=='0'?'selected':'' }}>No</option>
                </select>
            </div>
        </div>


        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Receipt Footer</label>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                <input type="text" class="form-control has-feedback-left" id="receipt_footer"
                       value="{{ $settings['receipt_footer'] }}">
                <span class="fa fa-coffee form-control-feedback left" aria-hidden="true"></span>
            </div>
        </div>


        <div class="form-group">
            <label class="control-label col-md-3">Menu Font Size</label>
            <div class="col-md-4 col-sm-4 col-xs-4 form-group has-feedback">
                <input type="number" class="form-control has-feedback-left" id="menu_font_size"
                       value="{{ $settings['menu_font_size'] }}">
                <span class="glyphicon glyphicon-text-size form-control-feedback left" aria-hidden="true"></span>
            </div>
            <div class="col-md-3">
                <label class="control-label col-md-3 text-left">px</label>
            </div>
        </div>


        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Default Waiter</label>
            <div class="col-md-6 col-sm-6 col-xs-12 form-group" style="margin-top: 7px">
                <input type="checkbox" class="" id="default_waiter"
                        {{ $settings['default_waiter']==1?'checked':'' }}>

            </div>
        </div>


        <div class="form-group">
            <label class="control-label col-md-3">Item Box Height</label>
            <div class="col-md-4 col-sm-4 col-xs-4 form-group has-feedback">
                <input type="number" class="form-control has-feedback-left" id="item_box_size"
                       value="{{ $settings['item_box_size'] }}">
                <span class="glyphicon glyphicon-text-size form-control-feedback left" aria-hidden="true"></span>
            </div>
            <div class="col-md-3">
                <label class="control-label col-md-3 text-left">px</label>
            </div>
        </div>


        <div class="@if(Auth::user()->role != "superadmin") div-hide @endif">

            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Order Timestamp</label>
                <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                    <select class="form-control" id="order_timestamp">
                        <option value="current_timestamp" {{ $settings['order_timestamp']=='current_timestamp'?'selected':'' }}>Current Timestamp</option>
                        <option value="workperiod_timestamp" {{ $settings['order_timestamp']=='workperiod_timestamp'?'selected':'' }}>WorkPeriod Timestamp</option>
                        <option value="nighthour_timestamp" {{ $settings['order_timestamp']=='nighthour_timestamp'?'selected':'' }}>Night Hour Timestamp</option>
                    </select>
                </div>
            </div>


            <div class="form-group">
                <label class="control-label col-md-3">Night Hour</label>
                <div class="col-md-4 col-sm-4 col-xs-4 form-group has-feedback">
                    <input type="text" class="form-control has-feedback-left" id="night_hour"
                           value="{{ $settings['night_hour'] }}">
                    <span class="glyphicon glyphicon-text-size form-control-feedback left" aria-hidden="true"></span>
                </div>
                <div class="col-md-3">
                    <label class="control-label col-md-3 text-left">Minutes</label>
                </div>
            </div>


            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Check Order Before Ending Work Period</label>
                <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                    <select class="form-control" id="check_order_before_end_workperiod">
                        <option value="1" {{ $settings['check_order_before_end_workperiod']=='1'?'selected':'' }}>Yes</option>
                        <option value="0" {{ $settings['check_order_before_end_workperiod']=='0'?'selected':'' }}>No</option>
                    </select>
                </div>
            </div>

        </div>


        <div class="form-group @if(Auth::user()->kitchen_queue == 0) div-hide @endif">
            <label class="control-label col-md-3">Refresh Timeout</label>
            <div class="col-md-4 col-sm-4 col-xs-4 form-group has-feedback">
                <input type="text" class="form-control has-feedback-left" id="refresh_timeout"
                       value="{{ $settings['refresh_timeout'] }}">
                <span class="glyphicon glyphicon-text-size form-control-feedback left" aria-hidden="true"></span>
            </div>
            <div class="col-md-3">
                <label class="control-label col-md-3 text-left">Seconds</label>
            </div>
        </div>





        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-5 ">
                <button class="btn btn-success btn-lg" id="restaurant_settings_button">Save App Settings</button>
            </div>
        </div>




    </div>
</div>