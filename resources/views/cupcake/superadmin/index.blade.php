@extends('layouts.main')

@section('title', 'Super Admin Dashboard')

@section('content')

    <script src="/js/superadmin.js"></script>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_content form-group">
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-edit"></i>
                            </div>
                            <div class="count">{{ $restaurants->total_restaurants - 1 }}</div>
                            <h3>Restaurants</h3>
                            <p></p>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-money"></i>
                            </div>
                            <div class="count">{{ $restaurants->total_revenue==Null? 0:$restaurants->total_revenue }}</div>
                            <h3>Total Revenue</h3>
                            <p></p>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-user"></i>
                            </div>
                            <div class="count">{{ $restaurants->customers }}</div>
                            <h3>Customers</h3>
                            <p></p>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-users"></i>
                            </div>
                            <div class="count">{{ $restaurants->stuffs }}</div>
                            <h3>Stuffs</h3>
                            <p></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>






@endsection
