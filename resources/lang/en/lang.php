<?php

return [

    /*
     * Menu
     */

    'language' => 'Language',
    'english' => 'English',
    'bangla' => 'Bangla',


    'dashboard' => 'Dashboard',
    'superadmin' => 'SuperAdmin',
    'superadmin_dashboard' => 'SuperAdmin Dashboard',
    'superadmin_settings' => 'SuperAdmin Settings',
    'create_new_restaurant' => 'Create New Restaurant',
    'restaurants' => 'Restaurants',
    'create_user' => 'Create User',
    'licenses' => 'Licenses',
    'bank_cards' => 'Bank Cards',
    'app' => 'App',
    'lookup' => 'Look Up',
    'inventory' => 'Inventory',
    'data' => 'Data',
    'schedule_scripts' => 'Schedule Scripts',
    'application_menu' => 'Application Menu',
    'view_orders' => 'View Orders',


    'accounts' => 'Accounts',
    'cash_opening' => 'Cash Opening',
    'daily_expense' => 'Daily Expense',
    'cash_closing' => 'Cash Closing',
    'loan_credit' => 'Loan Credit',
    'loan_debit' => 'Loan Debit',


    'guest' => 'Guest',


    'supplier' => 'Supplier',
    'manage_product' => 'Manage Product',
    'category' => 'Category',
    'products' => 'Products',
    'unit' => 'Unit',
    'purchase' => 'Purchase',
    'new_purchase' => 'New Purchase',
    'purchase_history' => 'Purchase History',
    'manage_stock' => 'Manage Stock',
    'current_stock' => 'Current Stock',
    'transfer_stock' => 'Transfer Stock',
    'receive_stock' => 'Receive Stock',
    'transfer_history' => 'Transfer History',
    'damage_entry' => 'Damage Entry',
    'damage_history' => 'Damage History',
    'store' => 'Store',


    'menu' => 'Menu',
    'create_menu' => 'Create Menu',
    'modifiers' => 'Modifiers',
    'cooking_instructions' => 'Cooking Instructions',
    'discount_circular' => 'Discount Circular',
    'recipe' => 'Recipe',


    'order' => 'Order',
    'online_order' => 'Online Orders',


    'table_order' => 'Table Order',


    'kitchen_queue' => 'Kitchen Queue',


    'report' => 'Report',
    'report_summary' => 'Report Summary',
    'sales_report' => 'Sales Report',
    'void_report' => 'Void Report',
    'expense_report' => 'Expense Report',
    'revenue_report' => 'Revenue Report',
    'reservation_report' => 'Reservation Report',
    'recipe_report' => 'Recipe Report',


    'reservation' => 'Reservation',
    'create_new_reservation' => 'Create New Reservation',
    'reservation_list' => 'Reservation List',


    'settings' => 'Settings',
    'users' => 'Users',
    'company_info' => 'Company Info',
    'tables' => 'Tables',
    'permissions' => 'Permissions',
    'printers' => 'Printers',
    'back_up' => 'Back-Up',
    'team' => 'Team',


];
