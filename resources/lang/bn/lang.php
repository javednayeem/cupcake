<?php

return [

    /*
     * Menu
     */

    'language' => 'ভাষা',
    'english' => 'ইংরেজি',
    'bangla' => 'বাংলা',


    'dashboard' => 'ড্যাশবোর্ড',
    'superadmin' => 'সুপার অ্যাডমিন',
    'superadmin_dashboard' => 'সুপার অ্যাডমিন ড্যাশবোর্ড',
    'superadmin_settings' => 'সুপার অ্যাডমিন সেটিংস',
    'create_new_restaurant' => 'রেস্টুরেন্ট তৈরী',
    'restaurants' => 'রেস্টুরেন্ট',
    'create_user' => 'ইউজার তৈরী',
    'licenses' => 'লাইসেন্স',
    'bank_cards' => 'ব্যাঙ্ক কার্ড',
    'app' => 'এপ্লিকেশন সেটিংস',
    'lookup' => 'লুক আপ',
    'inventory' => 'ইনভেন্টরি ',
    'data' => 'ডেটা',
    'schedule_scripts' => 'শিডিউল স্ক্রিপ্ট',
    'application_menu' => 'এপ্লিকেশন মেন্যু',
    'view_orders' => 'অর্ডার দেখুন',


    'accounts' => 'হিসাব',
    'cash_opening' => 'প্রারম্ভিক জমা',
    'daily_expense' => 'দৈনিক খরচ',
    'cash_closing' => 'সমাপনি জমা',
    'loan_credit' => 'ঋণ জমা',
    'loan_debit' => 'ঋণ খরচ',


    'guest' => 'অতিথি',


    'supplier' => 'সরবরাহকারী',
    'manage_product' => 'পণ্য পরিচালনা',
    'category' => 'শ্রেণী',
    'products' => 'পণ্য',
    'unit' => 'একক',
    'purchase' => 'ক্রয়',
    'new_purchase' => 'নতুন ক্রয়',
    'purchase_history' => 'ক্রয় তালিকা',
    'manage_stock' => 'স্টক পরিচালনা',
    'current_stock' => 'চলতি স্টক',
    'transfer_stock' => 'স্টক হস্তান্তর',
    'receive_stock' => 'স্টক গ্রহণ',
    'transfer_history' => 'হস্তান্তর তালিকা',
    'damage_entry' => 'ক্ষতি লিপিবদ্ধ',
    'damage_history' => 'ক্ষতি তালিকা',
    'store' => 'গুদাম',


    'menu' => 'মেন্যু',
    'create_menu' => 'মেন্যু তৈরী',
    'modifiers' => 'মডিফায়ার',
    'cooking_instructions' => 'রান্নার নির্দেশ',
    'discount_circular' => 'ছাড় বিজ্ঞপ্তি',
    'recipe' => 'প্রণালী',


    'order' => 'অর্ডার',
    'online_order' => 'অনলাইন অর্ডার',

    'table_order' => 'টেবিল অর্ডার',


    'kitchen_queue' => 'কিচেন কিউ',


    'report' => 'রিপোর্ট',
    'report_summary' => 'রিপোর্ট বিবরণী',
    'sales_report' => 'সেলস রিপোর্ট',
    'void_report' => 'ভয়েড রিপোর্ট',
    'expense_report' => 'ব্যয় রিপোর্ট',
    'revenue_report' => 'আয় রিপোর্ট',
    'reservation_report' => 'রিজারভেশন রিপোর্ট',
    'recipe_report' => 'রেসিপি রিপোর্ট',


    'reservation' => 'রিজারভেশন',
    'create_new_reservation' => 'রিজারভেশন তৈরী',
    'reservation_list' => 'রিজারভেশন তালিকা',


    'settings' => 'সেটিংস',
    'users' => 'ইউজার',
    'company_info' => 'প্রতিষ্ঠানের তথ্য',
    'tables' => 'টেবিল',
    'permissions' => 'অনুমতি',
    'printers' => 'প্রিন্টার',
    'back_up' => 'ব্যাকআপ',
    'team' => 'টীম',




];
