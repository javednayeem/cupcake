<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ScheduleScripts extends Migration {

    public function up() {

        Schema::defaultStringLength(191);

        Schema::create('schedule_scripts', function (Blueprint $table) {
            $table->increments('job_id')->unsigned();
            $table->string('script_name');
            $table->date('schedule_date')->nullable();
            $table->timestamp('created_at')->useCurrent();

        });

    }


    public function down() {
        Schema::dropIfExists('schedule_scripts');
    }
}
