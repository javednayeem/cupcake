<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InvProductStore extends Migration {

    public function up() {

        Schema::defaultStringLength(191);

        Schema::create('inv_product_store', function (Blueprint $table) {
            $table->increments('ps_id')->unsigned();

            $table->integer('product_id')->unsigned();
            $table->foreign('product_id')->references('product_id')->on('inv_product')->onDelete('cascade');

            $table->integer('store_id')->unsigned();
            $table->foreign('store_id')->references('store_id')->on('inv_stores')->onDelete('cascade');

            $table->double('quantity')->default(0);

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });


    }


    public function down() {
        Schema::dropIfExists('inv_product_store');
    }
}
