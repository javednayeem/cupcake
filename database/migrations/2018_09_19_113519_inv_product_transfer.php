<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InvProductTransfer extends Migration {

    public function up() {

        Schema::defaultStringLength(191);

        Schema::create('inv_product_transfer', function (Blueprint $table) {
            $table->increments('transfer_id')->unsigned();

            $table->integer('from_store_id')->unsigned();
            $table->foreign('from_store_id')->references('store_id')->on('inv_stores')->onDelete('cascade');

            $table->integer('to_store_id')->unsigned();
            $table->foreign('to_store_id')->references('store_id')->on('inv_stores')->onDelete('cascade');

            $table->date('transfer_date');

            $table->integer('created_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('users');

            $table->integer('updated_by')->unsigned();
            $table->foreign('updated_by')->references('id')->on('users');

            $table->integer('restaurant_id')->unsigned();
            $table->foreign('restaurant_id')->references('restaurants_id')->on('restaurants')->onDelete('cascade');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });


    }


    public function down() {
        Schema::dropIfExists('inv_product_transfer');
    }
}
