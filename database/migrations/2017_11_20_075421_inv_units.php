<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InvUnits extends Migration {

    public function up() {

        Schema::defaultStringLength(191);

        Schema::create('inv_units', function (Blueprint $table) {

            $table->increments('unit_id')->unsigned();
            $table->string('unit_name');
            $table->tinyInteger('status')->default(1);
            $table->string('description')->nullable();

            $table->integer('restaurant_id')->unsigned();
            $table->foreign('restaurant_id')->references('restaurants_id')->on('restaurants')->onDelete('cascade');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }


    public function down() {
        Schema::dropIfExists('inv_units');

    }
}
