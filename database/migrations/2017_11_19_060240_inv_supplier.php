<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InvSupplier extends Migration {

    public function up() {

        Schema::defaultStringLength(191);

        Schema::create('inv_supplier', function (Blueprint $table) {
            $table->increments('supplier_id')->unsigned();
            $table->string('company_name')->nullable();
            $table->string('supplier_name');
            $table->string('address')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('act_code')->nullable();

            $table->integer('accounts_payable')->default(0);
            $table->integer('accounts_receivable')->default(0);

            $table->integer('restaurant_id')->unsigned();
            $table->foreign('restaurant_id')->references('restaurants_id')->on('restaurants')->onDelete('cascade');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }


    public function down() {
        Schema::dropIfExists('inv_supplier');
    }
}
