<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Modifiers extends Migration {

    public function up() {

        Schema::defaultStringLength(191);

        Schema::create('modifiers', function (Blueprint $table) {

            $table->increments('modifier_id')->unsigned();

            $table->integer('restaurant_id')->unsigned();
            $table->foreign('restaurant_id')->references('restaurants_id')->on('restaurants')->onDelete('cascade');

            $table->string('modifier_name');
            $table->integer('price')->default(0);
            $table->tinyInteger('status')->default(1);
            $table->double('modifier_vat')->default(0.0);

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }


    public function down() {
        Schema::dropIfExists('modifiers');
    }
}
