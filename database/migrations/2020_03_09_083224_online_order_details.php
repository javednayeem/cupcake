<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OnlineOrderDetails extends Migration {

    public function up() {

        Schema::create('online_order_details', function (Blueprint $table) {

            $table->increments('id')->unsigned();

            $table->integer('order_id')->unsigned();
            $table->foreign('order_id')->references('order_id')->on('online_orders')->onDelete('cascade');

            $table->integer('menu_item_id')->unsigned();
            $table->foreign('menu_item_id')->references('menu_item_id')->on('menu_items')->onDelete('cascade');

            $table->string('menu_item_name');
            $table->integer('item_price');
            $table->integer('item_quantity');
            $table->double('item_vat');

            $table->integer('item_status')->default(1);

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }

    public function down() {
        Schema::dropIfExists('online_order_details');
    }
}
