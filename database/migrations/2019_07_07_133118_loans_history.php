<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LoansHistory extends Migration {

    public function up() {

        Schema::create('loans_history', function (Blueprint $table) {

            $table->increments('history_id')->unsigned();

            $table->integer('owner_id')->unsigned();
            $table->foreign('owner_id')->references('owner_id')->on('loans_from')->onDelete('cascade');

            $table->integer('amount')->default(0);
            $table->enum('transaction_type', ['debit', 'credit']);

            $table->integer('created_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('users');

            $table->integer('restaurant_id')->unsigned();
            $table->foreign('restaurant_id')->references('restaurants_id')->on('restaurants')->onDelete('cascade');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }

    public function down() {
        Schema::dropIfExists('loans_history');
    }
}
