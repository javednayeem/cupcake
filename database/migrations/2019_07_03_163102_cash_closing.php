<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CashClosing extends Migration {

    public function up() {

        Schema::create('cash_closing', function (Blueprint $table) {

            $table->increments('closing_id')->unsigned();

            $table->integer('restaurant_id')->unsigned();
            $table->foreign('restaurant_id')->references('restaurants_id')->on('restaurants')->onDelete('cascade');

            $table->integer('note_1000')->default(0);
            $table->integer('note_500')->default(0);
            $table->integer('note_100')->default(0);
            $table->integer('note_50')->default(0);
            $table->integer('note_20')->default(0);
            $table->integer('note_10')->default(0);
            $table->integer('note_5')->default(0);
            $table->integer('note_2')->default(0);
            $table->integer('note_1')->default(0);

            $table->date('closing_date');
            $table->integer('total_amount')->default(0);
            $table->tinyInteger('set')->default(0);

            $table->integer('created_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('users');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }

    public function down() {
        Schema::dropIfExists('cash_closing');
    }
}
