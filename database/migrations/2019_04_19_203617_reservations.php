<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Reservations extends Migration {

    public function up() {

        Schema::create('reservations', function (Blueprint $table) {

            $table->increments('reservation_id')->unsigned();

            $table->integer('restaurant_id')->unsigned();
            $table->foreign('restaurant_id')->references('restaurants_id')->on('restaurants')->onDelete('cascade');

            $table->integer('customer_id')->unsigned();
            $table->foreign('customer_id')->references('customer_id')->on('customers')->onDelete('cascade');

            $table->date('reservation_date');
            $table->time('start_time');
            $table->time('end_time');

            $table->longText('description');

            $table->longText('food_menu')->nullable();
            $table->integer('subtotal')->default(0);
            $table->integer('advance')->default(0);

            $table->integer('guest')->default(1);
            $table->longText('tables');

            $table->integer('created_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('users');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }

    public function down() {
        Schema::dropIfExists('reservations');
    }
}
