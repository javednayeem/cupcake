<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrderModifiers extends Migration {

    public function up() {

        Schema::create('order_modifiers', function (Blueprint $table) {
            $table->increments('order_modifier_id')->unsigned();

            $table->integer('order_id')->unsigned();
            $table->foreign('order_id')->references('order_id')->on('orders')->onDelete('cascade');

            $table->integer('modifier_id')->unsigned();
            $table->foreign('modifier_id')->references('modifier_id')->on('modifiers')->onDelete('cascade');

            $table->integer('order_details_id')->unsigned();
            $table->foreign('order_details_id')->references('order_details_id')->on('order_details')->onDelete('cascade');

            $table->integer('quantity')->default(1);

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }

    public function down() {
        Schema::dropIfExists('order_modifiers');
    }
}
