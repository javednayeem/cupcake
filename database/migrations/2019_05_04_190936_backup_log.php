<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BackupLog extends Migration {

    public function up() {

        Schema::create('backup_log', function (Blueprint $table) {

            $table->increments('log_id')->unsigned();
            $table->string('file_name');
            $table->timestamp('created_at')->useCurrent();

        });

    }

    public function down() {
        Schema::dropIfExists('backup_log');
    }
}
