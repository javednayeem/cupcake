<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Licenses extends Migration {

    public function up() {

        Schema::create('licenses', function (Blueprint $table) {
            $table->increments('license_id')->unsigned();

            $table->integer('restaurants_id');
            $table->string('license_key')->nullable();
            $table->date('license_start_date');
            $table->date('license_end_date');
            $table->integer('status')->default(1);
            $table->enum('license_type', ['paid', 'trial']);
            $table->integer('created_by');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }


    public function down() {
        Schema::dropIfExists('licenses');
    }
}
