<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetmenuItems extends Migration {

    public function up() {

        Schema::create('setmenu_items', function (Blueprint $table) {

            $table->increments('item_id')->unsigned();

            $table->string('item_name');

            $table->integer('menu_item_id')->unsigned();
            $table->foreign('menu_item_id')->references('menu_item_id')->on('menu_items')->onDelete('cascade');

            $table->integer('restaurant_id')->unsigned();
            $table->foreign('restaurant_id')->references('restaurants_id')->on('restaurants')->onDelete('cascade');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }


    public function down() {
        Schema::dropIfExists('setmenu_items');
    }
}
