<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InvDamageEntry extends Migration {

    public function up() {

        Schema::create('inv_damage_entry', function (Blueprint $table) {

            $table->increments('damage_id')->unsigned();

            $table->integer('store_id')->unsigned();
            $table->foreign('store_id')->references('store_id')->on('inv_stores')->onDelete('cascade');

            $table->double('damage_cost')->default(0);

            $table->integer('created_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');

            $table->integer('restaurant_id')->unsigned();
            $table->foreign('restaurant_id')->references('restaurants_id')->on('restaurants')->onDelete('cascade');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    public function down() {
        Schema::dropIfExists('inv_damage_entry');
    }

}
