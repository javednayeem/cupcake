<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Orders extends Migration {

    public function up() {

        Schema::defaultStringLength(191);

        Schema::create('orders', function (Blueprint $table) {
            $table->increments('order_id')->unsigned();

            $table->integer('restaurant_id')->unsigned();
            $table->foreign('restaurant_id')->references('restaurants_id')->on('restaurants')->onDelete('cascade');

            $table->integer('table_id')->nullable();

            $table->integer('waiter_id')->unsigned();
            $table->foreign('waiter_id')->references('id')->on('users');

            $table->integer('customer_id')->nullable();
            $table->string('order_type');
            $table->string('order_status')->default("placed");
            $table->double('bill')->default(0);
            $table->double('vat')->default(0);
            $table->double('service_charge')->default(0);
            $table->double('sd_charge')->default(0);
            $table->double('service_charge_vat')->default(0);
            $table->double('sd_vat')->default(0);
            $table->double('discount')->default(0);
            $table->double('discount_percentage')->default(0);
            $table->enum('discount_amount_type', ['cash', 'percentage']);
            $table->string('discount_reference')->nullable();

            $table->double('total_bill');
            $table->string('given_amount')->nullable();
            $table->longText('order_notes')->nullable();
            $table->tinyInteger('served')->default(0);

            $table->integer('created_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('users');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }


    public function down() {
        Schema::dropIfExists('orders');
    }
}
