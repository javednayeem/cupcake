<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RoleLookup extends Migration {

    public function up() {

        Schema::defaultStringLength(191);

        Schema::create('role_lookup', function (Blueprint $table) {
            $table->increments('role_id')->unsigned();
            $table->string('role_name');
            $table->integer('created_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('users');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }


    public function down() {
        Schema::dropIfExists('role_lookup');
    }
}
