<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OnlineOrders extends Migration {

    public function up() {

        Schema::create('online_orders', function (Blueprint $table) {

            $table->increments('order_id')->unsigned();

            $table->integer('restaurant_id')->unsigned();
            $table->foreign('restaurant_id')->references('restaurants_id')->on('restaurants')->onDelete('cascade');

            $table->integer('customer_id')->unsigned();
            $table->foreign('customer_id')->references('id')->on('users');

            $table->timestamp('scheduled_at')->nullable();

            $table->enum('order_status', ['placed', 'confirmed', 'processing', 'completed', 'cancelled'])->default('placed');

            $table->double('bill')->default(0);
            $table->double('vat')->default(0);
            $table->double('service_charge')->default(0);
            $table->double('sd_charge')->default(0);
            $table->double('service_charge_vat')->default(0);
            $table->double('sd_vat')->default(0);

            $table->double('discount')->default(0);
            $table->double('discount_percentage')->default(0);
            $table->enum('discount_amount_type', ['cash', 'percentage'])->default('cash');
            $table->string('discount_reference')->nullable();

            $table->double('total_bill')->default(0);

            $table->longText('order_notes')->nullable();

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });


    }

    public function down() {
        Schema::dropIfExists('online_orders');
    }
}
