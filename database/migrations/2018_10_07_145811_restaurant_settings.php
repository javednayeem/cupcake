<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RestaurantSettings extends Migration {

    public function up() {

        Schema::defaultStringLength(191);

        Schema::create('restaurant_settings', function (Blueprint $table) {
            $table->increments('setting_id')->unsigned();
            $table->string('setting_key');
            $table->string('setting_value')->nullable();

            $table->integer('restaurant_id')->unsigned();
            $table->foreign('restaurant_id')->references('restaurants_id')->on('restaurants')->onDelete('cascade');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }


    public function down() {
        Schema::dropIfExists('restaurant_settings');
    }
}