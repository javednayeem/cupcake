<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ApplicationMenu extends Migration {

    public function up() {

        Schema::create('application_menu', function (Blueprint $table) {

            $table->increments('menu_id')->unsigned();

            $table->string('menu_name');
            $table->string('url')->nullable();
            $table->integer('parent_menu')->default(0);
            $table->string('class')->nullable();

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }

    public function down() {
        Schema::dropIfExists('application_menu');
    }
}
