<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InvProduct extends Migration {

    public function up() {

        Schema::create('inv_product', function (Blueprint $table) {
            $table->increments('product_id')->unsigned();
            $table->string('product_code')->nullable();
            $table->string('product_name');

            $table->integer('unit_id')->unsigned();
            $table->integer('category_id')->unsigned();

            $table->double('fixed_price')->default(0);

            $table->double('first_price')->default(0);
            $table->double('last_price')->default(0);
            $table->double('avg_price')->default(0);

            $table->double('low_inv_alert')->default(5);

            $table->tinyInteger('status')->default(1);

            $table->integer('restaurant_id')->unsigned();
            $table->foreign('restaurant_id')->references('restaurants_id')->on('restaurants')->onDelete('cascade');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }


    public function down() {
        Schema::dropIfExists('inv_product');
    }
}
