<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReportPermission extends Migration {

    public function up() {

        Schema::create('report_permission', function (Blueprint $table) {
            $table->increments('permission_id')->unsigned();

            $table->integer('reportType_id')->unsigned();
            $table->foreign('reportType_id')->references('reportType_id')->on('report_type')->onDelete('cascade');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->tinyInteger('permission')->default(0);

            $table->integer('created_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }

    public function down() {
        Schema::dropIfExists('report_permission');
    }
}
