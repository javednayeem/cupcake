<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Tables extends Migration {

    public function up() {

        Schema::defaultStringLength(191);

        Schema::create('tables', function (Blueprint $table) {
            $table->increments('table_id')->unsigned();

            $table->integer('restaurant_id')->unsigned();
            $table->foreign('restaurant_id')->references('restaurants_id')->on('restaurants')->onDelete('cascade');

            $table->string('table_name');
            $table->integer('capacity')->default(4);
            $table->tinyInteger('reserved')->default(0);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }


    public function down() {
        Schema::dropIfExists('tables');

    }
}
