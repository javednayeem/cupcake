<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InvProductCategory extends Migration {

    public function up() {

        Schema::defaultStringLength(191);

        Schema::create('inv_product_category', function (Blueprint $table) {
            $table->increments('category_id')->unsigned();
            $table->string('category_name');
            $table->integer('restaurant_id')->unsigned();
            $table->foreign('restaurant_id')->references('restaurants_id')->on('restaurants')->onDelete('cascade');
            $table->tinyInteger('status')->default(1);

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }


    public function down() {
        Schema::dropIfExists('inv_product_category');
    }
}
