<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MenuItems extends Migration {

    public function up() {
        Schema::defaultStringLength(191);

        Schema::create('menu_items', function (Blueprint $table) {

            $table->increments('menu_item_id')->unsigned();

            $table->integer('menu_id')->unsigned();
            $table->foreign('menu_id')->references('menu_id')->on('menus')->onDelete('cascade');

            $table->string('item_name');
            $table->string('ratio')->nullable();
            $table->double('price');
            $table->double('cost_price')->default(0);
            $table->integer('item_discount')->default(0);
            $table->string('category')->nullable();
            $table->string('item_img')->default("default_item.png");
            $table->tinyInteger('status')->default(1);
            $table->double('item_vat')->default(0.0);
            $table->tinyInteger('set_menu')->default(0);
            $table->tinyInteger('discount_available')->default(1);
            $table->integer('printer_id')->default(0);

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }


    public function down() {
        Schema::dropIfExists('menu_items');
    }
}
