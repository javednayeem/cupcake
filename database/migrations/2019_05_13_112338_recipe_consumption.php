<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RecipeConsumption extends Migration {

    public function up() {

        Schema::create('recipe_consumption', function (Blueprint $table) {

            $table->increments('consumption_id')->unsigned();

            $table->integer('order_id')->nullable();

            $table->integer('item_id')->unsigned();
            $table->foreign('item_id')->references('menu_item_id')->on('menu_items')->onDelete('cascade');

            $table->integer('product_id')->unsigned();
            $table->foreign('product_id')->references('product_id')->on('inv_product')->onDelete('cascade');

            $table->double('quantity');

            $table->integer('store_id')->unsigned();
            $table->foreign('store_id')->references('store_id')->on('inv_stores')->onDelete('cascade');

            $table->tinyInteger('status')->default(1);

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }

    public function down() {
        Schema::dropIfExists('recipe_consumption');
    }
}
