<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Menus extends Migration {

    public function up() {
        Schema::defaultStringLength(191);

        Schema::create('menus', function (Blueprint $table) {
            $table->increments('menu_id')->unsigned();
            $table->string('restaurants_id');
            $table->string('menu_name')->nullable();
            $table->integer('creator_id');
            $table->tinyInteger('status')->default(1);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    public function down() {
        Schema::dropIfExists('menus');

    }
}
