<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VoidItem extends Migration {

    public function up() {

        Schema::create('void_item', function (Blueprint $table) {
            $table->increments('void_item_id')->unsigned();

            $table->integer('restaurant_id')->unsigned();
            $table->foreign('restaurant_id')->references('restaurants_id')->on('restaurants')->onDelete('cascade');

            $table->integer('order_details_id')->unsigned();
            $table->foreign('order_details_id')->references('order_details_id')->on('order_details')->onDelete('cascade');

            $table->integer('order_id')->unsigned();
            $table->foreign('order_id')->references('order_id')->on('orders')->onDelete('cascade');

            $table->integer('void_quantity');

            $table->integer('waiter_id')->unsigned();
            $table->foreign('waiter_id')->references('id')->on('users')->onDelete('cascade');

            $table->integer('cashier_id')->unsigned();
            $table->foreign('cashier_id')->references('id')->on('users')->onDelete('cascade');

            $table->integer('approved_admin_id')->nullable();
            $table->enum('void_status', ['requested_void', 'accepted_void'])->default("requested_void");

            $table->longText('reason');
            $table->longText('notes')->nullable();

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }


    public function down() {
        Schema::dropIfExists('void_item');
    }
}
