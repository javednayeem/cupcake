<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DailyExpense extends Migration {

    public function up() {

        Schema::create('daily_expense', function (Blueprint $table) {

            $table->increments('expense_id')->unsigned();

            $table->integer('restaurant_id')->unsigned();
            $table->foreign('restaurant_id')->references('restaurants_id')->on('restaurants')->onDelete('cascade');

            $table->longText('purpose')->nullable();
            $table->double('amount')->default(0);

            $table->enum('type', ['expense', 'transfer'])->default('expense');
            $table->enum('debit_from', ['sales', 'petty_cash', 'loan']);
            $table->string('transfer_to')->nullable();

            $table->integer('created_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('users');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }

    public function down() {
        Schema::dropIfExists('daily_expense');
    }
}
