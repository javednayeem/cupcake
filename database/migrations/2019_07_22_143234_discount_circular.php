<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DiscountCircular extends Migration {

    public function up() {

        Schema::create('discount_circular', function (Blueprint $table) {

            $table->increments('circular_id')->unsigned();

            $table->string('circular_name');

            $table->double('discount')->default(0);
            $table->enum('discount_type', ['cash', 'percentage']);

            $table->date('start_date');
            $table->date('end_date');

            $table->integer('status')->default(1);

            $table->integer('created_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('users');

            $table->integer('restaurant_id')->unsigned();
            $table->foreign('restaurant_id')->references('restaurants_id')->on('restaurants')->onDelete('cascade');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }

    public function down() {
        Schema::dropIfExists('discount_circular');
    }
}
