<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class WorkPeriods extends Migration {

    public function up() {
        Schema::defaultStringLength(191);

        Schema::create('work_period', function (Blueprint $table) {

            $table->increments('workperiod_id')->unsigned();
            $table->integer('restaurant_id');
            $table->timestamp('start_time')->useCurrent();
            $table->timestamp('end_time')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->integer('start_time_creator');
            $table->integer('end_time_creator')->nullable();
        });
    }


    public function down() {
        Schema::dropIfExists('work_period');
    }
}
