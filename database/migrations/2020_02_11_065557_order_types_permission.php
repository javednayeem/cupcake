<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrderTypesPermission extends Migration {

    public function up() {

        Schema::create('order_types_permission', function (Blueprint $table) {

            $table->increments('id')->unsigned();

            $table->integer('type_id')->unsigned();
            $table->foreign('type_id')->references('type_id')->on('order_types')->onDelete('cascade');

            $table->integer('restaurant_id')->unsigned();
            $table->foreign('restaurant_id')->references('restaurants_id')->on('restaurants')->onDelete('cascade');

            $table->tinyInteger('permission')->default(1);

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }

    public function down() {
        Schema::dropIfExists('order_types_permission');
    }
}
