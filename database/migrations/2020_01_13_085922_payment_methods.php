<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PaymentMethods extends Migration {

    public function up() {

        Schema::create('payment_methods', function (Blueprint $table) {

            $table->increments('method_id')->unsigned();

            $table->string('method_name');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }

    public function down() {
        Schema::dropIfExists('payment_methods');
    }
}
