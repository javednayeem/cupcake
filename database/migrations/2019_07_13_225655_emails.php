<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Emails extends Migration {

    public function up() {

        Schema::create('emails', function (Blueprint $table) {

            $table->increments('email_id')->unsigned();

            $table->string('email');

            $table->integer('created_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('users');

            $table->integer('restaurant_id')->unsigned();
            $table->foreign('restaurant_id')->references('restaurants_id')->on('restaurants')->onDelete('cascade');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }

    public function down() {
        Schema::dropIfExists('emails');
    }
}
