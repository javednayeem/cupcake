<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InvProductPurchaseIn extends Migration {

    public function up() {
        Schema::defaultStringLength(191);

        Schema::create('inv_product_purchase_in', function (Blueprint $table) {
            $table->increments('inv_product_purchase_in_id')->unsigned();
            $table->integer('purchase_invoice_id')->references('invoice_id')->on('inv_purchase_invoice')->onDelete('cascade');
            $table->integer('product_id')->references('product_id')->on('inv_product');
            $table->double('quantity');
            $table->double('unit_price')->default(0);
            $table->double('total_purchase_price')->default(0);
            $table->date('expire_date')->nullable();

            $table->integer('created_by')->references('id')->on('users');
            $table->integer('updated_by')->references('id')->on('users');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }


    public function down() {
        Schema::dropIfExists('inv_product_purchase_in');
    }
}
