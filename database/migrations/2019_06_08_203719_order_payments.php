<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrderPayments extends Migration {

    public function up() {

        Schema::create('order_payments', function (Blueprint $table) {
            $table->increments('payment_id')->unsigned();

            $table->integer('order_id')->unsigned();
            $table->foreign('order_id')->references('order_id')->on('orders')->onDelete('cascade');

            $table->string('payment_method');
            $table->double('amount');
            $table->integer('card_id')->nullable();
            $table->string('card_number')->nullable();

            $table->integer('restaurant_id')->unsigned();
            $table->foreign('restaurant_id')->references('restaurants_id')->on('restaurants')->onDelete('cascade');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }


    public function down() {
        Schema::dropIfExists('order_payments');
    }
}
