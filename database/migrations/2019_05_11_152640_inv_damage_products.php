<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InvDamageProducts extends Migration {

    public function up() {

        Schema::create('inv_damage_products', function (Blueprint $table) {

            $table->increments('id')->unsigned();

            $table->integer('damage_id')->unsigned();
            $table->foreign('damage_id')->references('damage_id')->on('inv_damage_entry')->onDelete('cascade');

            $table->integer('product_id')->unsigned();
            $table->foreign('product_id')->references('product_id')->on('inv_product')->onDelete('cascade');

            $table->double('quantity')->default(1);
            $table->double('unit_price')->default(0);

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }

    public function down() {
        Schema::dropIfExists('inv_damage_products');
    }
}
