<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InvProductStoreIn extends Migration {

    public function up() {

        Schema::defaultStringLength(191);

        Schema::create('inv_product_store_in', function (Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->integer('transfer_id')->unsigned();
            $table->foreign('transfer_id')->references('transfer_id')->on('inv_product_transfer')->onDelete('cascade');

            $table->integer('product_id');
            $table->double('quantity');

            $table->integer('created_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('users');

            $table->integer('updated_by')->unsigned();
            $table->foreign('updated_by')->references('id')->on('users');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }


    public function down() {
        Schema::dropIfExists('inv_product_store_in');
    }
}
