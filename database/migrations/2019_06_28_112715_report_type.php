<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReportType extends Migration {

    public function up() {

        Schema::create('report_type', function (Blueprint $table) {
            $table->increments('reportType_id')->unsigned();

            $table->enum('report_type', ['sales', 'void', 'inventory']);
            $table->string('report_value');
            $table->string('report_name');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }

    public function down() {
        Schema::dropIfExists('report_type');
    }
}
