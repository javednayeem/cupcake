<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

    public function up() {
        Schema::defaultStringLength(191);

        Schema::create('users', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('role');
            $table->string('phone')->nullable();
            $table->string('user_img')->default("default_user.png");
            $table->integer('status')->default(1);

            $table->integer('restaurant_id');
            $table->integer('creator_id');

            $table->tinyInteger('accounts')->default(1);
            $table->tinyInteger('inventory')->default(0);
            $table->tinyInteger('kitchen_queue')->default(0);
            $table->tinyInteger('online_order')->default(0);
            $table->tinyInteger('license')->default(0);

            $table->string('locale')->default("en");

            $table->rememberToken();
            $table->timestamps();
        });
    }


    public function down() {
        Schema::dropIfExists('users');
    }
}
