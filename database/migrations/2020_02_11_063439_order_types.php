<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrderTypes extends Migration {

    public function up() {

        Schema::create('order_types', function (Blueprint $table) {

            $table->increments('type_id')->unsigned();

            $table->string('type_name');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }

    public function down() {
        Schema::dropIfExists('order_types');
    }
}
