<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LicensePeriods extends Migration {

    public function up() {

      Schema::create('license_periods', function (Blueprint $table) {
          $table->increments('period_id')->unsigned();

          $table->integer('days');
          $table->string('duration_str');
          $table->integer('order')->default(1);
          
          $table->integer('created_by')->unsigned();
          $table->foreign('created_by')->references('id')->on('users');

          $table->timestamp('created_at')->useCurrent();
          $table->timestamp('updated_at')->useCurrent();
      });
    }


    public function down() {
        Schema::dropIfExists('license_periods');
    }
}
