<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UnitConversion extends Migration {

    public function up() {

        Schema::create('unit_conversion', function (Blueprint $table) {

            $table->increments('conversion_id')->unsigned();

            $table->integer('from_unit_id')->unsigned();
            $table->foreign('from_unit_id')->references('unit_id')->on('inv_units');

            $table->integer('to_unit_id')->unsigned();
            $table->foreign('to_unit_id')->references('unit_id')->on('inv_units');

            $table->double('conversion_rate')->default(0);

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }

    public function down() {
        Schema::dropIfExists('unit_conversion');
    }
}
