<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Customers extends Migration {

    public function up() {
        Schema::defaultStringLength(191);

        Schema::create('customers', function (Blueprint $table) {
            $table->increments('customer_id')->unsigned();

            $table->integer('restaurant_id')->unsigned();
            $table->foreign('restaurant_id')->references('restaurants_id')->on('restaurants')->onDelete('cascade');

            $table->string('customer_name');
            $table->string('customer_email')->nullable();
            $table->longText('customer_address')->nullable();
            $table->string('customer_phone')->nullable();
            $table->string('card_no')->nullable();
            $table->double('discount')->default(0);
            $table->double('discount_percentage')->default(0);

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    public function down() {
        Schema::dropIfExists('customers');
    }
}
