<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InvPurchaseInvoice extends Migration {

    public function up() {

        Schema::defaultStringLength(191);

        Schema::create('inv_purchase_invoice', function (Blueprint $table) {
            $table->increments('invoice_id')->unsigned();
            $table->string('invoice_no');
            $table->integer('supplier_id')->references('supplier_id')->on('inv_supplier');
            $table->timestamp('purchase_date')->useCurrent();
            $table->timestamp('delivery_date')->nullable();
            $table->double('sub_total');
            $table->double('discount')->default(0);
            $table->double('shipping')->default(0);
            $table->double('grand_total');
            $table->integer('created_by')->references('id')->on('users');
            $table->integer('updated_by')->references('id')->on('users');
            $table->integer('store_id')->nullable();
            $table->integer('restaurant_id');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }


    public function down() {
        Schema::dropIfExists('inv_purchase_invoice');

    }
}
