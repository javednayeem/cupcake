<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BackupSchedule extends Migration {

    public function up() {

        Schema::create('backup_schedule', function (Blueprint $table) {

            $table->increments('schedule_id')->unsigned();

            $table->timestamp('next_schedule');
            $table->tinyInteger('on_queue')->default(1);
            $table->tinyInteger('created_backup')->default(0);

            $table->timestamp('created_at')->useCurrent();

        });

    }

    public function down() {
        Schema::dropIfExists('backup_schedule');
    }
}
