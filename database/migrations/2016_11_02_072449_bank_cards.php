<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BankCards extends Migration {

    public function up() {
        Schema::defaultStringLength(191);

        Schema::create('bank_cards', function (Blueprint $table) {
            $table->increments('card_id')->unsigned();
            $table->string('card_name');
            $table->string('bank_name')->nullable();

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    public function down() {
        Schema::dropIfExists('bank_cards');
    }
}
