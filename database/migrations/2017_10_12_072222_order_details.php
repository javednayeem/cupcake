<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrderDetails extends Migration {

    public function up() {

        Schema::defaultStringLength(191);

        Schema::create('order_details', function (Blueprint $table) {
            $table->increments('order_details_id')->unsigned();

            $table->integer('order_id')->unsigned();
            $table->foreign('order_id')->references('order_id')->on('orders')->onDelete('cascade');

            $table->integer('menu_item_id')->unsigned();
            $table->foreign('menu_item_id')->references('menu_item_id')->on('menu_items')->onDelete('cascade');

            $table->string('menu_item_name');
            $table->integer('item_price');
            $table->integer('item_discount')->nullable();
            $table->integer('item_quantity');
            $table->double('item_vat');
            $table->tinyInteger('settle')->default(0);

            $table->integer('item_status')->default(1);

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }


    public function down() {
        Schema::dropIfExists('order_details');
    }
}
