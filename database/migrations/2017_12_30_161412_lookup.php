<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Lookup extends Migration {

    public function up() {
        Schema::defaultStringLength(191);

        Schema::create('lookup', function (Blueprint $table) {
            $table->increments('lookup_id')->unsigned();
            $table->string('lookup_name');
            $table->integer('created_by');
            $table->timestamp('created_at')->useCurrent();
        });
    }

    public function down() {
        Schema::dropIfExists('lookup');
    }
}
