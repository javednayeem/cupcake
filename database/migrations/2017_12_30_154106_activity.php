<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Activity extends Migration {

    public function up() {

        Schema::create('activity', function (Blueprint $table) {
            $table->increments('activity_id')->unsigned();

            $table->integer('restaurant_id')->unsigned();
            $table->foreign('restaurant_id')->references('restaurants_id')->on('restaurants')->onDelete('cascade');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->longText('activity');

            $table->timestamp('created_at')->useCurrent();
        });
    }

    public function down(){
        Schema::dropIfExists('activity');
    }
}
