<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Restaurants extends Migration {

    public function up() {
        Schema::defaultStringLength(191);

        Schema::create('restaurants', function (Blueprint $table) {
            $table->increments('restaurants_id')->unsigned();

            $table->string('restaurants_name');
            $table->string('restaurants_code')->unique();
            $table->string('address')->nullable();
            $table->string('area')->nullable();
            $table->string('city')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('email')->nullable();
            $table->string('website')->nullable();
            $table->string('restaurant_img')->default("default_restaurant.png");
            $table->integer('status')->default(1);
            $table->string('vat_no')->nullable();

            $table->tinyInteger('price_including_vat')->default(0);
            $table->tinyInteger('vat_after_discount')->default(0);

            $table->double('vat_percentage')->default(0.0);
            $table->double('service_charge')->default(0.0);
            $table->double('sd_percentage')->default(0.0);

            $table->double('service_charge_vat_percentage')->default(0.0);
            $table->double('sd_vat_percentage')->default(0.0);

            $table->integer('creator_id')->unsigned();
            $table->foreign('creator_id')->references('id')->on('users')->onDelete('cascade');

            $table->tinyInteger('accounts')->default(1);
            $table->tinyInteger('inventory')->default(0);
            $table->tinyInteger('kitchen_queue')->default(0);
            $table->tinyInteger('online_order')->default(0);
            $table->tinyInteger('license')->default(0);
            $table->integer('license_id');
            $table->integer('petty_cash')->default(0);
            $table->integer('loan')->default(0);

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }


    public function down() {
        Schema::dropIfExists('restaurants');

    }
}
